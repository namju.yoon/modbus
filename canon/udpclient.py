import socket
import struct
import threading
import logging
from logging.handlers import RotatingFileHandler
from ctypes import c_int16
import numpy as np

from .mcode import *
from .msg import MessageProtocol

class UDPClient(object):
    def __init__(self, ipAddress, sport):
        self.__udpTarget = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.__serverAddress = ipAddress
        self.__sport = sport
        self.__connected = False

        self.pbit_status = np.zeros((42, 4))
        self.status_info = tuple()

        # self.__udpTarget.settimeout(5)
        self.__udpTarget.connect((self.__serverAddress, self.__sport))
        logging.debug("*"*40)
        logging.debug("***** SERVER BIND %s :: %s", self.__udpTarget, "*"*10)
        logging.debug("*"*40)
        self.__connected = True
        self.__boot_udp = False

        self.__thread = threading.Thread(target=self.__listen, args=())
        self.__thread.start()

    def __listen(self):
        logging.debug("&"*10, "UDPClient :: Start Request .....")
        self.__stoplistening = False
        self.__receivedata = bytearray()
        try:
            while not self.__stoplistening:
                if len(self.__receivedata) == 0:
                    self.__receivedata = bytearray()
                    self.__timeout = 500
                    if self.__udpTarget is not None:
                        try:
                            self.__receivedata = self.__udpTarget.recv(BUFFERSIZE)
                            self.__athread = threading.Thread(target=self.__action_response, args=())
                            self.__athread.start()
                        except Exception as e:
                            pass
                        
        except socket.timeout:
            logging.debug("__listen :: socket.timeout :: event")
            self.__receivedata = None

    def close_server(self):
        self.__udpTarget.close()


    def __action_response(self):
        message = self.__receivedata
        msg = MessageProtocol()
        msg.make_header_from_received(message)

        if msg.checksum_test():
            checksum_pass = True
        else:
            checksum_pass = False

        ## self.__receivedata 를 비워야 새로운 데이터를 받을수 있음
        self.__receivedata = bytearray()

        if msg.code == PCS_TCC_BOOT_DONE_UPD:
            if checksum_pass:
                logging.debug("Success Connection")
                self.__boot_udp = True
            else:
                logging.debug("....")

        elif msg.code == PCS_TCC_PBIT_RESULT_REF:
            logging.debug("PCS_TCC_PBIT_RESULT_REF :: %s \n\n", message)
            self.pbit_status = self.make_data_pbit_result(message)
            return self.pbit_status

        elif msg.code == PCS_TCC_STATE_INFO_REF:
            if checksum_pass:
                logging.debug("PCS_TCC_STATE_INFO_REF CHECKSUM PASS ######")
            self.status_info = self.make_data_pcs_statusinfo(message)
            return self.status_info


    def make_data_pbit_result(self, message):
        data = list(message[6:-2])
        pbit_status = np.zeros((42, 4))

        temp0 = 0b00000011
        temp1 = 0b00001100
        temp2 = 0b00110000
        temp3 = 0b11000000

        for idx, ddd in enumerate(data):
            if ddd:
                d0 = (ddd & temp0)
                d1 = (ddd & temp1) >> 2
                d2 = (ddd & temp2) >> 4
                d3 = (ddd & temp3) >> 6

                pbit_status[idx][0] = d0
                pbit_status[idx][1] = d1
                pbit_status[idx][2] = d2
                pbit_status[idx][3] = d3

        # print("make_data_pbit_result :: ", pbit_status.shape)
        return pbit_status


    def make_data_pcs_statusinfo(self, message):
        data = message[6:-2]
        # print(data, len(data))
        return struct.unpack("<BHBBBBBBBBBBBB", data)


    def send(self, data):
        try:
            self.__udpTarget.send(data)
        except Exception as e:
            pass

    def loop_pcs_boot_signal(self):
        if not self.__boot_udp:
            msg = MessageProtocol(PCS_ID, TCC_ID, PCS_TCC_BOOT_DONE_UPD, SACK, 0)
            msg.make_encode()
            self.send(msg.message)

            timer = threading.Timer(BOOT_SIGNAL_REPEAT, self.loop_pcs_boot_signal)
            timer.start()

    def request_pbit_result(self):
        # TCC_PCS_PBIT_RESULT_REQ
        msg = MessageProtocol(TCC_ID, PCS_ID, TCC_PCS_PBIT_RESULT_REQ, PNACK, 0)
        msg.make_encode()
        self.send(msg.message)

        logging.debug("REQUEST :: TCC_PCS_PBIT_RESULT_REQ_REQ_NOACK :: %s :: %s\n", msg.message, msg)

        timer = threading.Timer(BOOT_SIGNAL_REPEAT, self.request_pbit_result)
        timer.start()

    def request_pcs_status_info(self):
        # TCC_PCS_PBIT_RESULT_REQ
        msg = MessageProtocol(TCC_ID, PCS_ID, TCC_PCS_STATE_INFO_REQ, PNACK, 0)
        msg.make_encode()
        self.send(msg.message)

        logging.debug("REQUEST :: TCC_PCS_STATE_INFO_REQ_NOACK :: %s :: %s\n", msg.message, msg)

        # timer = threading.Timer(BOOT_SIGNAL_REPEAT, self.request_pcs_status_info)
        # timer.start()


    def close(self):
        """
        Closes Serial port, or TCP-Socket connection
        """
        if self.__udpTarget is not None:
            self.__stoplistening = True
            self.__udpTarget.shutdown(socket.SHUT_RDWR)
            self.__udpTarget.close()
        self.__connected = False


    @property
    def ipaddress(self):
        """
        Gets the IP-Address of the Server to be connected
        """
        return self.__serverAddress

    @ipaddress.setter
    def ipaddress(self, ipAddress):
        """
        Sets the IP-Address of the Server to be connected
        """
        self.__serverAddress = ipAddress




    