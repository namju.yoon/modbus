## TIMER
BOOT_SIGNAL_REPEAT = 0.5
TPERIOD = 5


## ID
TCC_ID = 0x10 
TPC_ID = 0x40

PACK = 0x01
PNACK = 0xFF

CACK = 0x10
CNACK = 0x11

# TPC_HOST_IP = "192.168.100.130"
TPC_HOST_IP = "127.0.0.1"
TPC_HOST_PORT = 31000

# TCC_HOST_IP = "192.168.100.100"
TCC_HOST_IP = "127.0.0.1"
TCC_HOST_PORT = 11000

BUFFERSIZE = 512
BOOT_CHECK_REPEAT = 3
ACK_TIME = 1000
STATUS_TIME = 2000
TIME_WAIT = 0.2

NORMAL = 0
WARNING = 1
FAIL = 2


# 0x01 : Fault
# 0x02 : CPU
FAULT_RESET = 0x01
CPU_RESET = 0x02


# 0x01 : 저전원 ON
# 0x02 : 저전원 OFF"
ON = 0x01
OFF = 0x02

# 0x01 : 포탄제어기 (PCU)
# 0x02 : 장약제어기 (MCCU)
# 0x03 : 장전제어기
# 0x04 : 포/포탑 구동제어기 (GTCU)
# 0x05 : 무장제어기 (CCU)
# 0xFF : ALL

ALL = 0xFF
PCU_CTL = 0x01
MCCU_CTL = 0x02
SETUP_CTL = 0x03
GTCU_CTL = 0x04
CCU_CTL = 0x05

TPC_PARTS2 = {
    ALL: "ALL",
    PCU_CTL: "포탄제어기(PCU)",
    MCCU_CTL: "장약제어기(MCCU)",
    SETUP_CTL: "장전제어기",
    GTCU_CTL: "포/포탑 구동제어기(GTCU)",
    CCU_CTL: "무장제어기(CCU)",
}

ON_COMPLETE = 0x01
OFF_COMPLETE = 0x02
ON_FAIL = 0x03
OFF_FAIL = 0x04

HIGHPOWER_RESULT = {
    ON_COMPLETE: "고전원 ON 완료",
    OFF_COMPLETE: "고전원 OFF 완료",
    ON_FAIL: "고전원 ON 미완료(실패)",
    OFF_FAIL: "고전원 OFF 미완료(실패)",
}

ACK_CODE = 0xFF

# "0x01 : 완료
# 0x02 : 실패"
SUCCESS = 0x01
FAIL = 0x02

# "0x01 : 비상정지
# 0x02 : 비상정지 해제"
EMERGENCY_STOP = 0x01
EMERGENCY_STOP_END = 0x02

TCC_TPC_BOOT_DONE_UPD = 0x01
TCC_TPC_PBIT_RESULT_REQ = 0x10
TCC_TPC_CBIT_SET_UPD = 0x11
TCC_TPC_CBIT_RESULT_REQ = 0x12
TCC_TPC_IBIT_RUN_CMD = 0x13
TCC_TPC_IBIT_RESULT_REQ = 0x14
TCC_TPC_RESET_UPD = 0x20
TCC_TPC_SOFT_EMERGENCY_STOP_UPD	= 0x30
TCC_TPC_CTRL_POWER_CMD = 0x40
TCC_TPC_DRIVE_POWER_CMD = 0x41
TCC_TPC_STATE_INFO_REQ = 0xA0

TCC_CODE2 = {
    "1": "TCC_TPC_BOOT_DONE_UPD",
    "16": "TCC_TPC_PBIT_RESULT_REQ",
    "17": "TCC_TPC_CBIT_SET_UPD",
    "18": "TCC_TPC_CBIT_RESULT_REQ",
    "19": "TCC_TPC_IBIT_RUN_CMD   ",
    "20": "TCC_TPC_IBIT_RESULT_REQ",
    "32": "TCC_TPC_RESET_UPD",
    "48": "TCC_TPC_SOFT_EMERGENCY_STOP_UPD ",
    "64": "TCC_TPC_CTRL_POWER_CMD",
    "65": "TCC_TPC_DRIVE_POWER_CMD",
    "160": "TCC_TPC_STATE_INFO_REQ",
    "255": "ACK_CODE"
}

## K9A2_ICD PCS(SERVER) ==> TCC(CLIENT)
TPC_TCC_BOOT_DONE_UPD = 0x01        # 1
TPC_TCC_PBIT_RESULT_REF = 0x10      # 2
TPC_TCC_CBIT_SET_UPD = 0x11
TPC_TCC_CBIT_RESULT_REF = 0x12      # 3
TPC_TCC_IBIT_DONE_UPD = 0x13        # 4
TPC_TCC_IBIT_RESULT_REF = 0x14      # 5
TPC_TCC_RESET_UPD = 0x20
TPC_TCC_SOFT_EMERGENCY_STOP_UPD	= 0x30
TPC_TCC_HARD_EMERGENCY_STOP_UPD = 0x31  # 6
TPC_TCC_CTRL_POWER_UPD = 0x40       # 7
TPC_TCC_DRIVE_POWER_UPD = 0x41      # 8
TPC_TCC_STATE_INFO_REF = 0xA0       # 9

TPC_CODE2 = {
    "1": "TPC_TCC_BOOT_DONE_UPD",
    "16": "TPC_TCC_PBIT_RESULT_REF",
    "17": "TPC_TCC_CBIT_SET_UPD",
    "18": "TPC_TCC_CBIT_RESULT_REF",
    "19": "TPC_TCC_IBIT_DONE_UPD",
    "20": "TPC_TCC_IBIT_RESULT_REF",
    "32": "TPC_TCC_RESET_UPD",
    "48": "TPC_TCC_SOFT_EMERGENCY_STOP_UPD",
    "49": "TPC_TCC_HARD_EMERGENCY_STOP_UPD",
    "64": "TPC_TCC_CTRL_POWER_UPD",
    "65": "TPC_TCC_DRIVE_POWER_UPD",
    "65": "TPC_TCC_DRIVE_POWER_CMD",
    "160": "TPC_TCC_STATE_INFO_REF",
    "255": "ACK_CODE"
}

CONTROL_NAME = {
	'인터페이스 #1' : [
		{'상태정보상세' : [
            {'name' : '대기상태', 'ftype': 'OneStatus', 'value': 0},
            {'name' : '출력상태', 'ftype': 'OneStatus', 'value': 2},
            {'name' : '경고상태', 'ftype': 'OneStatus', 'value': 4},
            {'name' : '고장상태', 'ftype': 'OneStatus', 'value': 8},
        ]},
		{'경고정보상세1' : [
			{'name': '포탄제어기 (PCU) 출력 경고', 'ftype': 'Field'},            
            {'name': '장약제어기 (MCCU) 출력 경고', 'ftype': 'Field'},
            {'name': '장전제어기 출력 경고', 'ftype': 'Field'},
            {'name': '포포탑 구동제어기 (GTCU) 출력 경고', 'ftype': 'Field'},
            {'name': '무장제어기 (CCU) 출력 경고', 'ftype': 'Field'},
            {'name': '상시전원 출력 과전류 경고', 'ftype': 'Field'},
            {'name': '입력 저전압 경고', 'ftype': 'Field'},
            {'name': '입력 과전압 경고', 'ftype': 'Field'},
            {'name': '입력 과전류 경고', 'ftype': 'Field'},
            {'name': '출력 과전압 경고', 'ftype': 'Field'},
            {'name': '출력 저전압 경고', 'ftype': 'Field'},
            {'name': '커넥터 연결 불량 J3 경고', 'ftype': 'Field'},
            {'name': '커넥터 연결 불량 J4 경고', 'ftype': 'Field'},
            {'name': '전원분배기 커넥터 연결불량 1 경고', 'ftype': 'Field'},
            {'name': '전원분배기 커넥터 연결불량 2 경고', 'ftype': 'Field'},
            {'name': '모듈1 이상 경고', 'ftype': 'Field'},
        ]},
    ],
	'인터페이스 #2' : [
		{'경고정보상세2' : [
            {'name': '모듈2 이상 경고', 'ftype': 'Field'},
            {'name': '모듈3 이상 경고', 'ftype': 'Field'},
            {'name': '입력 저전압 경고 보조축전지 전환', 'ftype': 'Field'},
            {'name': '보조축전지 저전압', 'ftype': 'Field'},
            {'name': '승무원 안전경고', 'ftype': 'Field'},
            {'name': '조종수안전 경고', 'ftype': 'Field'},
            {'name': '비상정지1 경고', 'ftype': 'Field'},
            {'name': '비상정지2 경고', 'ftype': 'Field'},
        ]},
        {'차단정보상세' : [
			{'name': '포탄제어기 (PCU)', 'ftype': 'Field'},
            {'name': '장약제어기 (MCCU)', 'ftype': 'Field'},
            {'name': '장전제어기 ', 'ftype': 'Field'},
            {'name': '포포탑 구동제어기 (GTCU)', 'ftype': 'Field'},
            {'name': '무장제어기 (CCU)', 'ftype': 'Field'},
            {'name': '상시전원 출력 과전류', 'ftype': 'Field'},
            {'name': '입력 저전압 차단', 'ftype': 'Field'},
            {'name': '입력 좌전압 차단', 'ftype': 'Field'},
            {'name': '입력 과전류', 'ftype': 'Field'},
            {'name': '출력 과전압', 'ftype': 'Field'},
            {'name': '출력 저전압 차단', 'ftype': 'Field'},
            {'name': '모듈 1 차단', 'ftype': 'Field'},
            {'name': '모듈 2 차단', 'ftype': 'Field'},
            {'name': '모듈 3 차단', 'ftype': 'Field'},
        ]},
    ],
	'모듈 #1' : [
		{'상태정보상세' : [
            {'name' : '대기상태', 'ftype': 'OneStatus', 'value': 0},
            {'name' : '출력상태', 'ftype': 'OneStatus', 'value': 2},
            {'name' : '경고상태', 'ftype': 'OneStatus', 'value': 4},
            {'name' : '고장상태', 'ftype': 'OneStatus', 'value': 8},	
        ]},
		{'경고정보상세' : [
			{'name': '출력인덕터 과전류 1 경고', 'ftype': 'Field'},
            {'name': '출력인덕터 과전류 2 경고', 'ftype': 'Field'},
            {'name': '출력인덕터 과전류 3 경고', 'ftype': 'Field'},
            {'name': '출력인덕터 과전류 4 경고', 'ftype': 'Field'},
            {'name': '출력 과전압 경고', 'ftype': 'Field'},
            {'name': '출력 저전압 경고', 'ftype': 'Field'},
            {'name': '출력 과전류 경고', 'ftype': 'Field'},
            {'name': '입력 저전압 경고', 'ftype': 'Field'},
            {'name': '입력 과전압 경고', 'ftype': 'Field'},
            {'name': '과온도1 경고', 'ftype': 'Field'},
        ]},
		{'차단정보상세' : [
			{'name': '출력인덕터 과전류 1', 'ftype': 'Field'},
            {'name': '출력인덕터 과전류 2', 'ftype': 'Field'},
            {'name': '출력인덕터 과전류 3', 'ftype': 'Field'},
            {'name': '출력인덕터 과전류 4', 'ftype': 'Field'},
            {'name': '출력 과전압', 'ftype': 'Field'},
            {'name': '출력 저전압 차단', 'ftype': 'Field'},
            {'name': '출력 과전류', 'ftype': 'Field'},
            {'name': '입력 저전압 차단', 'ftype': 'Field'},
            {'name': '입력 과전압', 'ftype': 'Field'},
            {'name': '과온도1 차단', 'ftype': 'Field'},
            {'name': '전류 밸런스 이상 차단', 'ftype': 'Field'},
            {'name': '내부 통신 차단 차단', 'ftype': 'Field'},
        ]}
    ],
	'모듈 #2' : [
        {'상태정보상세' : [
            {'name' : '대기상태', 'ftype': 'OneStatus', 'value': 0},
            {'name' : '출력상태', 'ftype': 'OneStatus', 'value': 2},
            {'name' : '경고상태', 'ftype': 'OneStatus', 'value': 4},
            {'name' : '고장상태', 'ftype': 'OneStatus', 'value': 8},	
        ]},
        {'경고정보상세' : [
            {'name':'출력인덕터 과전류 1 경고', 'ftype': 'Field'},
            {'name':'출력인덕터 과전류 2 경고', 'ftype': 'Field'},
            {'name':'출력인덕터 과전류 3 경고', 'ftype': 'Field'},
            {'name':'출력인덕터 과전류 4 경고', 'ftype': 'Field'},
            {'name':'출력 과전압 경고', 'ftype': 'Field'},
            {'name':'출력 저전압 경고', 'ftype': 'Field'},
            {'name':'출력 과전류 경고', 'ftype': 'Field'},
            {'name':'입력 저전압 경고', 'ftype': 'Field'},
            {'name':'입력 과전압 경고', 'ftype': 'Field'},
            {'name':'과온도1 경고', 'ftype': 'Field'},
        ]},
        {'차단정보상세' : [
            {'name': '출력인덕터 과전류 1', 'ftype': 'Field'},
            {'name': '출력인덕터 과전류 2','ftype': 'Field'},
            {'name': '출력인덕터 과전류 3','ftype': 'Field'},
            {'name': '출력인덕터 과전류 4','ftype': 'Field'},
            {'name': '출력 과전압','ftype': 'Field'},
            {'name': '출력 저전압 차단','ftype': 'Field'},
            {'name': '출력 과전류','ftype': 'Field'},
            {'name': '입력 저전압 차단','ftype': 'Field'},
            {'name': '입력 과전압','ftype': 'Field'},
            {'name': '과온도1 차단','ftype': 'Field'},
            {'name': '전류 밸런스 이상 차단', 'ftype': 'Field'},
            {'name': '내부 통신 차단 차단', 'ftype': 'Field'},
        ]}
    ],
    '모듈 #3' : [
        {'상태정보상세' : [
            {'name' : '대기상태', 'ftype': 'OneStatus', 'value': 0},
            {'name' : '출력상태', 'ftype': 'OneStatus', 'value': 2},
            {'name' : '경고상태', 'ftype': 'OneStatus', 'value': 4},
            {'name' : '고장상태', 'ftype': 'OneStatus', 'value': 8},	
        ]},
        {'경고정보상세' : [
            {'name': '출력인덕터 과전류 1 경고', 'ftype': 'Field'},
            {'name': '출력인덕터 과전류 2 경고', 'ftype': 'Field'},
            {'name': '출력인덕터 과전류 3 경고', 'ftype': 'Field'},
            {'name': '출력인덕터 과전류 4 경고', 'ftype': 'Field'},
            {'name': '출력 과전압 경고', 'ftype': 'Field'},
            {'name': '출력 저전압 경고', 'ftype': 'Field'},
            {'name': '출력 과전류 경고', 'ftype': 'Field'},
            {'name': '입력 저전압 경고', 'ftype': 'Field'},
            {'name': '입력 과전압 경고', 'ftype': 'Field'},
            {'name': '과온도1 경고', 'ftype': 'Field'},
        ]},
        {'차단정보상세' : [
            {'name': '출력인덕터 과전류 1', 'ftype': 'Field'},
            {'name': '출력인덕터 과전류 2', 'ftype': 'Field'},
            {'name': '출력인덕터 과전류 3', 'ftype': 'Field'},
            {'name': '출력인덕터 과전류 4', 'ftype': 'Field'},
            {'name': '출력 과전압', 'ftype': 'Field'},
            {'name': '출력 저전압 차단', 'ftype': 'Field'},
            {'name': '출력 과전류', 'ftype': 'Field'},
            {'name': '입력 저전압 차단', 'ftype': 'Field'},
            {'name': '입력 과전압', 'ftype': 'Field'},
            {'name': '과온도1 차단', 'ftype': 'Field'},
            {'name': '전류 밸런스 이상 차단', 'ftype': 'Field'},
            {'name': '내부 통신 차단 차단', 'ftype': 'Field'},
        ]}
    ],
}

POWER_NAME = {
	'인터페이스 #1' : [
		{'상태정보상세' : [
            {'name' : '대기상태', 'ftype': 'OneStatus', 'value': 0},
            {'name' : '출력상태', 'ftype': 'OneStatus', 'value': 2},
            {'name' : '경고상태', 'ftype': 'OneStatus', 'value': 4},
            {'name' : '고장상태', 'ftype': 'OneStatus', 'value': 8},
        ]},
		{'경고정보상세1' : [
			{'name':'포탄제어기 출력 과전류 경고', 'ftype': 'Field'},
            {'name':'장약제어기 출력 과전류 경고', 'ftype': 'Field'},
            {'name':'장전제어기 출력 과전류 경고', 'ftype': 'Field'},
            {'name':'포구동제어기 출력 과전류 경고', 'ftype': 'Field'},
            {'name':'무장제어기 출력 과전류 경고', 'ftype': 'Field'},
            {'name':'회생저항 출력 과전류 경고', 'ftype': 'Field'},
            {'name':'출력 과전압 경고', 'ftype': 'Field'},
            {'name':'모듈1 경고', 'ftype': 'Field'},
            {'name':'모듈2 경고', 'ftype': 'Field'},
            {'name':'모듈3 경고', 'ftype': 'Field'},
            {'name':'모듈4 경고', 'ftype': 'Field'},
            {'name':'CABLE LOOP4 경고', 'ftype': 'Field'},
            {'name':'CABLE LOOP5 경고', 'ftype': 'Field'},
            {'name':'CABLE LOOP6 경고', 'ftype': 'Field'},
            {'name':'CABLE LOOP7 경고', 'ftype': 'Field'},
            {'name':'CABLE LOOP8 경고', 'ftype': 'Field'},
        ]},
    ],
    '인터페이스 #2' : [
		{'경고정보상세2': [
            {'name':'CABLE LOOP9 경고','ftype':'Field'},
            {'name':'CABLE LOOP10 경고','ftype':'Field'},
            {'name':'CABLE LOOP11 경고','ftype':'Field'},
            {'name':'조종수 안전 경고','ftype':'Field'},
            {'name':'비상정지1 경고','ftype':'Field'},
        ]},
		{'차단정보상세' : [
            {'name': '포탄제어기 출력 과전류', 'ftype':'Field'},
            {'name': '장약제어기 출력 과전류', 'ftype':'Field'},
            {'name': '장전제어기 출력 과전류', 'ftype':'Field'},
            {'name': '포구동제어기 출력 과전류', 'ftype':'Field'},
            {'name': '무장제어기 출력 과전류고장', 'ftype':'Field'},
            {'name': '회생저항 출력 과전류', 'ftype':'Field'},
            {'name': '출력 과전압', 'ftype':'Field'},
            {'name': '모듈1 차단', 'ftype':'Field'},
            {'name': '모듈2 차단', 'ftype':'Field'},
            {'name': '모듈3 차단', 'ftype':'Field'},
            {'name': '모듈4 차단', 'ftype':'Field'},
            {'name': '외부 통신 이상 차단', 'ftype':'Field'},
        ]},
    ],
	'모듈 #1 정보 1' : [
        {'상태정보상세' : [
            {'name' : '대기상태', 'ftype': 'OneStatus', 'value': 0},
            {'name' : '출력상태', 'ftype': 'OneStatus', 'value': 2},
            {'name' : '경고상태', 'ftype': 'OneStatus', 'value': 4},
            {'name' : '고장상태', 'ftype': 'OneStatus', 'value': 8},
        ]},
        {'경고정보상세 1' : [
            {'name': '1번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '2번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '3번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '4번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '5번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '6번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '7번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '8번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '9번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '10번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '11번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '12번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '과온도1 경고', 'ftype': 'Field'},
            {'name': '과온도2 경고', 'ftype': 'Field'},
            {'name': '입력 저전압 경고', 'ftype': 'Field'},
            {'name': '입력 과전압 경고', 'ftype': 'Field'},
        ]},
        {'경고정보상세 2' : [
            {'name':'입력 과전류 경고', 'ftype':'Field'},
            {'name':'출력 과전압 경고', 'ftype':'Field'},
            {'name':'출력 저전압 경고', 'ftype':'Field'},
            {'name':'출력 과전류 경고', 'ftype':'Field'},
        ]}
    ],
	'모듈 #1 정보 2' : [
        {'차단정보상세 1': [
            { 'name': '1번 출력인덕터 과전류', 'ftype':'Field'},
            { 'name': '2번 출력인덕터 과전류', 'ftype':'Field'},
            { 'name': '3번 출력인덕터 과전류', 'ftype':'Field'},
            { 'name': '4번 출력인덕터 과전류', 'ftype':'Field'},
            { 'name': '5번 출력인덕터 과전류', 'ftype':'Field'},
            { 'name': '6번 출력인덕터 과전류', 'ftype':'Field'},
            { 'name': '7번 출력인덕터 과전류', 'ftype':'Field'},
            { 'name': '8번 출력인덕터 과전류', 'ftype':'Field'},
            { 'name': '9번 출력인덕터 과전류', 'ftype':'Field'},
            { 'name': '10번 출력인덕터 과전류',  'ftype':'Field'},
            { 'name': '11번 출력인덕터 과전류',  'ftype':'Field'},
            { 'name': '12번 출력인덕터 과전류',  'ftype':'Field'},
            { 'name': '과온도1 차단', 'ftype':'Field'},
            { 'name': '과온도2 차단', 'ftype':'Field'},
            { 'name': '입력 저전압 차단', 'ftype':'Field'},
            { 'name': '입력 과전압', 'ftype':'Field'},
        ]},
        {'차단정보상세 2': [
            { 'name': '입력 저전압 차단', 'ftype':'Field'},
            { 'name': '입력 과전압', 'ftype':'Field'},
            { 'name': '입력 과전류', 'ftype':'Field'},
            { 'name': '출력 과전압', 'ftype':'Field'},
            { 'name': '출력 저전압 차단', 'ftype':'Field'},
            { 'name': '출력 과전류', 'ftype':'Field'},
            { 'name': '출력인덕터 불균형 차단', 'ftype':'Field'},
            { 'name': '내부 통신 차단 차단', 'ftype':'Field'},
        ]}
    ],
    '모듈 #2 정보 1' : [
        {'상태정보상세' : [
            {'name' : '대기상태', 'ftype': 'OneStatus', 'value': 0},
            {'name' : '출력상태', 'ftype': 'OneStatus', 'value': 2},
            {'name' : '경고상태', 'ftype': 'OneStatus', 'value': 4},
            {'name' : '고장상태', 'ftype': 'OneStatus', 'value': 8},
        ]},
        {'경고정보상세 1' : [
            {'name': '1번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '2번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '3번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '4번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '5번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '6번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '7번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '8번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '9번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '10번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '11번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '12번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '과온도1 경고', 'ftype':'Field'},
            {'name': '과온도2 경고', 'ftype':'Field'},
            {'name': '입력 저전압 경고', 'ftype':'Field'},
            {'name': '입력 과전압 경고', 'ftype':'Field'},
        ]},
        {'경고정보상세 2' : [
            {'name':'입력 과전류 경고', 'ftype':'Field'},
            {'name':'출력 과전압 경고', 'ftype':'Field'},
            {'name':'출력 저전압 경고', 'ftype':'Field'},
            {'name':'출력 과전류 경고', 'ftype':'Field'},
        ]},
    ],
    '모듈 #2 정보 2' : [
        {'차단정보상세 1': [
            {'name': '1번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '2번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '3번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '4번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '5번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '6번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '7번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '8번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '9번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '10번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '11번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '12번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '과온도1 차단','ftype':'Field'},
            {'name': '과온도2 차단','ftype':'Field'},
            {'name': '입력 저전압 차단','ftype':'Field'},
            {'name': '입력 과전압','ftype':'Field'},
        ]},
        {'차단정보상세 2': [
            {'name':'입력 저전압 차단', 'ftype':'Field'},
            {'name':'입력 과전압', 'ftype':'Field'},
            {'name':'입력 과전류', 'ftype':'Field'},
            {'name':'출력 과전압', 'ftype':'Field'},
            {'name':'출력 저전압 차단', 'ftype':'Field'},
            {'name':'출력 과전류', 'ftype':'Field'},
            {'name':'출력인덕터 불균형 차단', 'ftype':'Field'},
            {'name':'내부 통신 차단 차단', 'ftype':'Field'},
        ]}
    ],
    '모듈 #3 정보 1' : [
        {'상태정보상세' : [
            {'name' : '대기상태', 'ftype': 'OneStatus', 'value': 0},
            {'name' : '출력상태', 'ftype': 'OneStatus', 'value': 2},
            {'name' : '경고상태', 'ftype': 'OneStatus', 'value': 4},
            {'name' : '고장상태', 'ftype': 'OneStatus', 'value': 8},
        ]},
        {'경고정보상세 1' : [
            {'name': '1번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '2번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '3번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '4번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '5번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '6번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '7번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '8번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '9번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '10번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '11번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '12번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '과온도1 경고', 'ftype':'Field'},
            {'name': '과온도2 경고', 'ftype':'Field'},
            {'name': '입력 저전압 경고', 'ftype':'Field'},
            {'name': '입력 과전압 경고', 'ftype':'Field'},
        ]},
        {'경고정보상세 2' : [
            {'name': '입력 과전류 경고', 'ftype':'Field'},
            {'name': '출력 과전압 경고', 'ftype':'Field'},
            {'name': '출력 저전압 경고', 'ftype':'Field'},
            {'name': '출력 과전류 경고', 'ftype':'Field'},
        ]}
    ],
    '모듈 #3 정보 2' : [
        {'차단정보상세 1': [
            {'name': '1번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '2번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '3번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '4번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '5번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '6번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '7번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '8번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '9번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '10번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '11번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '12번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '과온도1 차단', 'ftype':'Field'},
            {'name': '과온도2 차단', 'ftype':'Field'},
            {'name': '입력 저전압 차단', 'ftype':'Field'},
            {'name': '입력 과전압', 'ftype':'Field'},
        ]},
        {'차단정보상세 2': [
            {'name': '입력 저전압 차단', 'ftype':'Field'},
            {'name': '입력 과전압', 'ftype':'Field'},
            {'name': '입력 과전류', 'ftype':'Field'},
            {'name': '출력 과전압', 'ftype':'Field'},
            {'name': '출력 저전압 차단', 'ftype':'Field'},
            {'name': '출력 과전류', 'ftype':'Field'},
            {'name': '출력인덕터 불균형 차단', 'ftype':'Field'},
            {'name': '내부 통신 차단 차단', 'ftype':'Field'},
        ]}
    ],
    '모듈 #4 정보 1' : [
        {'상태정보상세' : [
            {'name' : '대기상태', 'ftype': 'OneStatus', 'value': 0},
            {'name' : '출력상태', 'ftype': 'OneStatus', 'value': 2},
            {'name' : '경고상태', 'ftype': 'OneStatus', 'value': 4},
            {'name' : '고장상태', 'ftype': 'OneStatus', 'value': 8},
        ]},
        {'경고정보상세 1' : [
            {'name': '1번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '2번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '3번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '4번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '5번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '6번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '7번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '8번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '9번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '10번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '11번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '12번 출력인덕터 과전류 경고', 'ftype':'Field'},
            {'name': '과온도1 경고', 'ftype':'Field'},
            {'name': '과온도2 경고', 'ftype':'Field'},
            {'name': '입력 저전압 경고', 'ftype':'Field'},
            {'name': '입력 과전압 경고', 'ftype':'Field'},
        ]},
        {'경고정보상세 2' : [
            {'name': '입력 과전류 경고', 'ftype':'Field'},
            {'name': '출력 과전압 경고', 'ftype':'Field'},
            {'name': '출력 저전압 경고', 'ftype':'Field'},
            {'name': '출력 과전류 경고', 'ftype':'Field'},
        ]}
    ],
    '모듈 #4 정보 2' : [
        {'차단정보상세 1': [
            {'name': '1번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '2번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '3번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '4번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '5번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '6번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '7번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '8번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '9번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '10번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '11번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '12번 출력인덕터 과전류', 'ftype':'Field'},
            {'name': '과온도1 차단', 'ftype':'Field'},
            {'name': '과온도2 차단', 'ftype':'Field'},
            {'name': '입력 저전압 차단', 'ftype':'Field'},
            {'name': '입력 과전압', 'ftype':'Field'},
        ]},
        {'차단정보상세 2': [
            {'name': '입력 저전압 차단', 'ftype':'Field'},
            {'name': '입력 과전압', 'ftype':'Field'},
            {'name': '입력 과전류', 'ftype':'Field'},
            {'name': '출력 과전압', 'ftype':'Field'},
            {'name': '출력 저전압 차단', 'ftype':'Field'},
            {'name': '출력 과전류', 'ftype':'Field'},
            {'name': '출력인덕터 불균형 차단', 'ftype':'Field'},
            {'name': '내부 통신 차단 차단', 'ftype':'Field'},
        ]}
    ],
}

STATUS_INFO = {
    '전원조절기' : [
		{'전원조절기 인터페이스': [
            '상태정보상세',
            '경고정보상세1',
            '경고정보상세2',
            '차단정보상세',
            '포탄제어기 (PCU) 전류',
            '장약제어기 (MCCU) 전류',
            '장전제어기 전류',
            '포/포탑 구동제어기 (GTCU) 전류',
            '무장제어기 (CCU) 전류',
            '상시 전류',
            '입력 전압',
            '출력 전압',
        ]},
		{'전원조절기 모듈 #1' : [
            '상태정보상세',
            '경고정보상세',
            '차단정보상세',
            '출력 전압',
            '출력 전류',
            '입력 전압',
            '온도',
        ]},
		{'전원조절기 모듈 #2': [
            '상태정보상세',
            '경고정보상세',
            '차단정보상세',
            '출력 전압',
            '출력 전류',
            '입력 전압',
            '온도',
        ]},
		{'전원조절기 모듈 #3' : [
            '상태정보상세',
            '경고정보상세',
            '차단정보상세',
            '출력 전압',
            '출력 전류',
            '입력 전압',
            '온도',
        ]},
    ],
	'승압기' :[
		{'승압기 인터페이스': [
            '상태정보상세',
            '경고정보상세1',
            '경고정보상세2',
            '차단정보상세',
            '포탄제어기 전류',
            '장약제어기 전류',
            '장전제어기 전류',
            '포구동제어기 전류',
            '무장제어기 전류',
            '회생저항 전류',
            '출력 전압',
        ]},
		{'승압기 모듈 #1': [
            '상태정보상세',
            '경고정보상세1',
            '경고정보상세2',
            '차단정보상세 1',
            '차단정보상세2',
            '온도1',
            '온도2',
            '입력 전압',
            '입력 전류',
            '출력 전압',
            '출력 전류',
        ]},
		{'승압기 모듈 #2' : [
            '상태정보상세',
            '경고정보상세1',
            '경고정보상세2',
            '차단정보상세 1',
            '차단정보상세2',
            '온도1',
            '온도2',
            '입력 전압',
            '입력 전류',
            '출력 전압',
            '출력 전류',
        ]},
		{'승압기 모듈 #3': [
            '상태정보상세',
            '경고정보상세1',
            '경고정보상세2',
            '차단정보상세 1',
            '차단정보상세2',
            '온도1',
            '온도2',
            '입력 전압',
            '입력 전류',
            '출력 전압',
            '출력 전류',
        ]},
		{'승압기 모듈 #4': [
            '상태정보상세',
            '경고정보상세1',
            '경고정보상세2',
            '차단정보상세 1',
            '차단정보상세2',
            '온도1',
            '온도2',
            '입력 전압',
            '입력 전류',
            '출력 전압',
            '출력 전류',
        ]}
    ]
}


NAMES = {
    "포탑전원조절기 1" : [
        {
            "포탑전원조절기 모듈 1 MAIN B/D" : [
                "입력 과전류",
                "입력 과전압",
                "입력 저전압",
                "출력 과전류",
                "출력 과전압",
                "출력 저전압",
                "내부 과(공진)전류",
                "내부 과온도",
                "내부 통신 차단",
            ]
        }, {
            "포탑전원조절기 모듈 2 MAIN B/D": [
                "입력 과전류",
                "입력 과전압",
                "입력 저전압",
                "출력 과전류",
                "출력 과전압",
                "출력 저전압",
                "내부 과(공진)전류",
                "내부 과온도",
                "내부 통신 차단",
            ]
        }, {
            "포탑전원조절기 모듈 3 MAIN B/D": [
                "입력 과전류",
                "입력 과전압",
                "입력 저전압",
                "출력 과전류",
                "출력 과전압",
                "출력 저전압",
                "내부 과(공진)전류",
                "내부 과온도",
                "내부 통신 차단",
            ]
        }], 

    "포탑전원조절기 2" : [
        {
            "포탑전원조절기 모듈 4 MAIN B/D": [
                "입력 과전류",
                "입력 과전압",
                "입력 저전압",
                "출력 과전류",
                "출력 과전압",
                "출력 저전압",
                "내부 과(공진)전류",
                "내부 과온도",
                "내부 통신 차단",
            ]
        }, {
            "포탑전원조절기 모듈 5 MAIN B/D": [
                "입력 과전류",
                "입력 과전압",
                "입력 저전압",
                "출력 과전류",
                "출력 과전압",
                "출력 저전압",
                "내부 과(공진)전류",
                "내부 과온도",
                "내부 통신 차단",
            ]
        }],
    "포탑전원조절기 3" : [
        {
            "포탑전원조절기 인터페이스 MAIN B/D":[
                "보조축전지 전환",
                "보조축전지 저전압",
                "보조축전지 과 충전전류",
                "사수전시기(GDU) 출력 과전류",
                "사통컴퓨터(TCC) 출력 과전류",
                "포탄적재제어기(PTU) 출력 과전류",
                "장약적재이송제어기(CTU) 출력 과전류",
                "탄약장전제어기(ARU) 출력 과전류",
                "포탑승압기(TBU) 출력 과전류",
                "포탑구동제어기(TDU) 출력 과전류",
                "포미폐쇄제어기(GBU) 출력 과전류",
                "무전기set(VRC-947K) 출력 과전류",
                "항법장치(DRUH) 출력 과전류",
                "자동시한장입장치(AFSS) 출력 과전류",
                "부수장치(영상저장장치, 이더넷 스위치 등) 출력 과전류",
                "전원분배기 연결체(J3) 분리",
                "전원분배기 연결체(J4) 분리",
                "전원분배기 연결체(J5) 분리",
            ]
        }
    ],
    "연결/통신 포탑전원조절기 타구성품 연동 1" : [
        {"포탑 전원분배기_Cable Loop Group_1(연결)": ["전원분배기 연결체 분리(GDU)"]},
        {"포탑 전원분배기_Cable Loop Group_2(연결)": ["전원분배기 연결체 분리(TBU, TCC, 부수장치)"]},
        {"포탑 전원분배기_Cable Loop Group_3(연결)": ["전원분배기 연결체 분리(PTU/CTU, ARU)"]},
        {"포탑 전원분배기_Cable Loop Group_4(연결)": ["전원분배기 연결체 분리(TDC/GBU)"]},
        {"포탑 전원분배기_Cable Loop Group_5(연결)": ["전원분배기 연결체 분리(DRUM, AFSS)"]},
        {"사수전시기(GDU)(전원)":  ["사수전시기(GDU) 회로차단기 OFF"]},
        {"사통컴퓨터(TCC)(전원)":  ["사통컴퓨터(TCC) 회로차단기 OFF"]},
        {"포탄적재제어기(PTU)(전원)":  ["  포탄적재제어기(PTU) 회로차단기 OFF"]},
        {"장약적재이송제어기(CTU)(전원)":  ["장약적재이송제어기(CTU) 회로차단기 OFF"]},
    ],
    "연결/통신 포탑전원조절기 타구성품 연동 2" : [
        {"탄약장전제어기(ARU)(전원)":  ["탄약장전제어기(ARU) 회로차단기 OFF"]},
        {"포탑승압기(TBU)(전원)":  ["포탑승압기(TBU) 회로차단기 OFF"]},
        {"포탑구동제어기(TDU)(전원)":  ["포탑구동제어기(TDU) 회로차단기 OFF"]},
        {"포미폐쇄제어기(GBU)(전원)":  ["포미폐쇄제어기(GBU) 회로차단기 OFF"]},
        {"무전기set(VRC-947K)(전원)":  ["무전기set(VRC-947K) 회로차단기 OFF"]},
        {"항법장치(DRUH)(전원)":  ["항법장치(DRUH) 회로차단기 OFF"]},
        {"자동시한장입장치(AFSS)(전원)":  ["자동시한장입장치(AFSS) 회로차단기 OFF"]},
        {"부수장치(영상저장장치, 이더넷 스위치, 이더넷 익스텐더)(전원)": ["부수장치(영상저장장치, 이더넷 스위치, 이더넷 익스텐더) 회로차단기 OFF"]},
        {"사격통제컴퓨터(통신)": ["외부 통신 이상"]},
        {"포탑승압기(통신)" : ["외부 통신 이상"]},
        {"차제전원조절기(통신)": ["외부 통신 이상"]},
    ],
    "포탑승압기 1": [
        {
            "포탑승압기 모듈 1 POWER B/D": [
                "입력 과전류",
                "입력 과전압",
                "입력 저전압",
                "출력 과전류",
                "출력 과전압",
                "출력 저전압",
                "내부 과온도",
                "내부 과(공진)전류",
            ]
        }, {
            "포탑승압기 모듈 1 CONTROL B/D": ["내부 통신 차단"],
        }, {
            "포탑승압기 모듈 2 POWER B/D": [
                "입력 과전류",
                "입력 과전압",
                "입력 저전압",
                "출력 과전류",
                "출력 과전압",
                "출력 저전압",
                "내부 과온도",
                "내부 과(공진)전류",
            ]
        }, {
            "포탑승압기 모듈 2 CONTROL B/D": ["내부 통신 차단"],
        }], 
    "포탑승압기 2": [
        {
            "포탑승압기 모듈 3 POWER B/D": [
                "입력 과전류",
                "입력 과전압",
                "입력 저전압",
                "출력 과전류",
                "출력 과전압",
                "출력 저전압",
                "내부 과온도",
                "내부 과(공진)전류",
            ]
        }, {
            "포탑승압기 모듈 3 CONTROL B/D": ["내부 통신 차단"],
        }, {
            "포탑승압기 모듈 4 POWER B/D": [
                "입력 과전류",
                "입력 과전압",
                "입력 저전압",
                "출력 과전류",
                "출력 과전압",
                "출력 저전압",
                "내부 과온도",
                "내부 과(공진)전류",
            ]
        }, {
            "포탑승압기 모듈 4 CONTROL B/D": ["내부 통신 차단"],
        }], 
    "포탑승압기 3": [
        {
            "포탑승압기 모듈 5 POWER B/D": [
                "입력 과전류",
                "입력 과전압",
                "입력 저전압",
                "출력 과전류",
                "출력 과전압",
                "출력 저전압",
                "내부 과온도",
                "내부 과(공진)전류",
            ]
        }, {
            "포탑승압기 모듈 5 CONTROL B/D": ["내부 통신 차단"],
        }, {
            "포탑승압기 모듈 6 POWER B/D": [
                "입력 과전류",
                "입력 과전압",
                "입력 저전압",
                "출력 과전류",
                "출력 과전압",
                "출력 저전압",
                "내부 과온도",
                "내부 과(공진)전류",
            ]
        }, {
            "포탑승압기 모듈 6 CONTROL B/D": ["내부 통신 차단"],
        }, {
            "포탑승압기 인터페이스 CONTROL B/D": ["외부 통신 이상"]
        }, 
    ],
    "연결/통신 포탑승압기 타구성품 연동" : [
        {
            "연결/통신 포탑승압기 타구성품 연동": [
                "이더넷 스위치, 포탑 전원분배기(연결):커넥터 이상, 이더넷, 제어전원 동작 ON 안됨 -> 검출 안됨, ",
                "포탑 전원분배기(전원):커넥터 연결 이상, 승압기의 제어 전원 연결이 안됨 -> 검출 안됨",
                "포탄적재이송제어기(연결):커넥터 연결 이상",
                "포탄적재이송제어기(전원):커넥터 출력 과전류",
                "장약적재이송제어기(연결):커넥터 연결 이상",
                "장약적재이송제어기(전원):커넥터 출력 과전류",
                "복합장전제어기#3(연결):커넥터 연결 이상",
                "복합장전제어기#3(전원):커넥터 출력 과전류",
                "복합장전제어기#1(연결):커넥터 연결 이상",
                "복합장전제어기#1(전원):커넥터 출력 과전류",
                "복합장전제어기#2(연결):커넥터 연결 이상",
                "복합장전제어기#2(전원):커넥터 출력 과전류",
                "포/포탑구동제어기(연결):커넥터 연결 이상",
                "포/포탑구동제어기(전원):커넥터 출력 과전류",
                "포미개폐제어기(연결):커넥터 연결 이상",
                "포미개폐제어기(전원):커넥터 출력 과전류",
                "포탑전원조절기(통신):외부 통신 이상",
            ]
        }
    ],
    "차체전원조절기" : [
        {"차체전원조절기 모듈 MAIN B/D": [
            "입력 과전류",
            "입력 과전압",
            "입력 저전압",
            "출력 과전류",
            "출력 과전압",
            "출력 저전압",
            "내부 과(공진)전류",
            "내부 과온도",
            "내부 통신 차단",
        ]},
    ],
    "연결/통신 차체전원조절기 타구성품 연동": [
        {
            "연결/통신 차체전원조절기": [
                "조종수해치스위치, 포신잠금(연결) : 커넥터 연결 확인",
                "이더넷스위치(전원) : 커넥터 연결 확인",
                "이더넷스위치, 이더넷익스텐더(전원) : 커넥터 연결 확인",
                "차체벽(전원) : 커넥터 연결 확인",
                "포탑전원조절기(통신) : 외부 통신 이상",
                "이더넷 스위치, 익스텐더 과전류 : 출력 과전류",
                "차체벽 과전류 : 출력 과전류",
            ]
        }
    ]
}


PCSSTATUS = {
    '전원조절기 인터페이스' : [
        { 'name': '상태정보상세', 'divide': 1, 'unit': ''},
        { 'name': '경고정보 상세', 'divide': 1, 'unit': ''},
        { 'name': '경고정보2 상세', 'divide': 1, 'unit': ''},
        { 'name': '고장정보 상세', 'divide': 1, 'unit': ''},
        { 'name': '고장정보2 상세', 'divide': 1, 'unit': ''},
        { 'name': '배터리 전압(전원조절기 소스)', 'divide': 1, 'unit': 'V'},
        { 'name': '배터리 전압(사통 축전지)', 'divide': 1, 'unit': 'V'},
        { 'name': '입력전류', 'divide': 1, 'unit': 'A'},
        { 'name': 'Aux IN', 'divide': 1, 'unit': 'A'},
        { 'name': '배터리 충전 전류', 'divide': 1, 'unit': 'A'},
        { 'name': '출력 전류 1', 'divide': 1, 'unit': 'A'},
        { 'name': '출력 전류 2', 'divide': 1, 'unit': 'A'},
        { 'name': '출력 전류 3', 'divide': 1, 'unit': 'A'},
        { 'name': '출력 전류 4', 'divide': 1, 'unit': 'A'},
        { 'name': '출력 전류 5', 'divide': 1, 'unit': 'A'},
        { 'name': '출력 전류 6', 'divide': 1, 'unit': 'A'},
        { 'name': '출력 전류 7', 'divide': 1, 'unit': 'A'},
        { 'name': '출력 전류 8', 'divide': 1, 'unit': 'A'},
        { 'name': '출력 전류 9', 'divide': 1, 'unit': 'A'},
        { 'name': '출력 전류 10', 'divide': 1, 'unit': 'A'},
        { 'name': '출력 전류 11', 'divide': 1, 'unit': 'A'},
        { 'name': '출력 전류 12', 'divide': 1, 'unit': 'A'},
    ],
    '전원조절기 모듈 1' : [
        {'name': '상태정보상세', 'divide': 1, 'unit': ''},
        {'name': '경고정보 상세', 'divide': 1, 'unit': ''},
        {'name': '고장정보 상세', 'divide': 1, 'unit': ''},
        {'name': '입력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '출력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '입력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '출력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '소비 전력', 'divide': 1, 'unit': ''},
        {'name': 'SW 버전', 'divide': 1, 'unit': ''},
    ],
    '전원조절기 모듈 2' : [
        {'name': '상태정보상세', 'divide': 1, 'unit': ''},
        {'name': '경고정보 상세', 'divide': 1, 'unit': ''},
        {'name': '고장정보 상세', 'divide': 1, 'unit': ''},
        {'name': '입력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '출력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '입력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '출력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '소비 전력', 'divide': 1, 'unit': ''},
        {'name': 'SW 버전', 'divide': 1, 'unit': ''},
    ],
    '전원조절기 모듈 3' : [
        {'name': '상태정보상세', 'divide': 1, 'unit': ''},
        {'name': '경고정보 상세', 'divide': 1, 'unit': ''},
        {'name': '고장정보 상세', 'divide': 1, 'unit': ''},
        {'name': '입력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '출력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '입력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '출력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '소비 전력', 'divide': 1, 'unit': ''},
        {'name': 'SW 버전', 'divide': 1, 'unit': ''},
    ],
    '전원조절기 모듈 4' : [
        {'name': '상태정보상세', 'divide': 1, 'unit': ''},
        {'name': '경고정보 상세', 'divide': 1, 'unit': ''},
        {'name': '고장정보 상세', 'divide': 1, 'unit': ''},
        {'name': '입력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '출력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '입력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '출력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '소비 전력', 'divide': 1, 'unit': ''},
        {'name': 'SW 버전', 'divide': 1, 'unit': ''},
    ],
    '전원조절기 모듈 5' : [
        {'name': '상태정보상세', 'divide': 1, 'unit': ''},
        {'name': '경고정보 상세', 'divide': 1, 'unit': ''},
        {'name': '고장정보 상세', 'divide': 1, 'unit': ''},
        {'name': '입력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '출력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '입력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '출력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '소비 전력', 'divide': 1, 'unit': ''},
        {'name': 'SW 버전', 'divide': 1, 'unit': ''},
    ],
    ' 승압기 인터페이스': [
        {'name': '상태 정보상세', 'divide': 1, 'unit': ''},
        {'name': '경고 정보상세', 'divide': 1, 'unit': ''},
        {'name': '고장 정보상세', 'divide': 1, 'unit': ''},
        {'name': 'IOP1', 'divide': 1, 'unit': 'A'},
        {'name': 'IOP2', 'divide': 1, 'unit': 'A'},
        {'name': 'IOP3', 'divide': 1, 'unit': 'A'},
        {'name': 'IOP4', 'divide': 1, 'unit': 'A'},
        {'name': 'IOP5', 'divide': 1, 'unit': 'A'},
        {'name': 'IOP6', 'divide': 1, 'unit': 'A'},
        {'name': 'IOP7', 'divide': 1, 'unit': 'A'},
        {'name': 'VO', 'divide': 1, 'unit': 'V'},
    ],
    '승압기 모듈 1': [
        {'name': '상태정보상세', 'divide': 1, 'unit': ''},
        {'name': '경고정보 상세', 'divide': 1, 'unit': ''},
        {'name': '고장정보 상세', 'divide': 1, 'unit': ''},
        {'name': '입력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '입력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '온도', 'divide': 1, 'unit': '˚C'},
        {'name': '출력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '출력 전압', 'divide': 1, 'unit': 'V'},
        {'name': 'SW 버전', 'divide': 1, 'unit': ''},
    ],
    '승압기 모듈 2': [
        {'name': '상태정보상세', 'divide': 1, 'unit': ''},
        {'name': '경고정보 상세', 'divide': 1, 'unit': ''},
        {'name': '고장정보 상세', 'divide': 1, 'unit': ''},
        {'name': '입력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '입력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '온도', 'divide': 1, 'unit': '˚C'},
        {'name': '출력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '출력 전압', 'divide': 1, 'unit': 'V'},
        {'name': 'SW 버전', 'divide': 1, 'unit': ''},
    ],
    '승압기 모듈 3': [
        {'name': '상태정보상세', 'divide': 1, 'unit': ''},
        {'name': '경고정보 상세', 'divide': 1, 'unit': ''},
        {'name': '고장정보 상세', 'divide': 1, 'unit': ''},
        {'name': '입력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '입력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '온도', 'divide': 1, 'unit': '˚C'},
        {'name': '출력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '출력 전압', 'divide': 1, 'unit': 'V'},
        {'name': 'SW 버전', 'divide': 1, 'unit': ''},
    ],
    '승압기 모듈 4': [
        {'name': '상태정보상세', 'divide': 1, 'unit': ''},
        {'name': '경고정보 상세', 'divide': 1, 'unit': ''},
        {'name': '고장정보 상세', 'divide': 1, 'unit': ''},
        {'name': '입력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '입력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '온도', 'divide': 1, 'unit': '˚C'},
        {'name': '출력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '출력 전압', 'divide': 1, 'unit': 'V'},
        {'name': 'SW 버전', 'divide': 1, 'unit': ''},
    ],
    '승압기 모듈 5': [
        {'name': '상태정보상세', 'divide': 1, 'unit': ''},
        {'name': '경고정보 상세', 'divide': 1, 'unit': ''},
        {'name': '고장정보 상세', 'divide': 1, 'unit': ''},
        {'name': '입력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '입력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '온도', 'divide': 1, 'unit': '˚C'},
        {'name': '출력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '출력 전압', 'divide': 1, 'unit': 'V'},
        {'name': 'SW 버전', 'divide': 1, 'unit': ''},
    ],
    '승압기 모듈 6': [
        {'name': '상태정보상세', 'divide': 1, 'unit': ''},
        {'name': '경고정보 상세', 'divide': 1, 'unit': ''},
        {'name': '고장정보 상세', 'divide': 1, 'unit': ''},
        {'name': '입력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '입력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '온도', 'divide': 1, 'unit': '˚C'},
        {'name': '출력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '출력 전압', 'divide': 1, 'unit': 'V'},
        {'name': 'SW 버전', 'divide': 1, 'unit': ''},
    ],
    '차체전원조절기': [
        {'name': '상태정보상세', 'divide': 1, 'unit': ''},
        {'name': '경고정보상세', 'divide': 1, 'unit': ''},
        {'name': '차단정보상세', 'divide': 1, 'unit': ''},
        {'name': '입력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '입력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '출력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '출력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '온도', 'divide': 1, 'unit': '˚C'},
    ]
}


TPCSTATUS = {
    '전원조절기 인터페이스' : [
        {'name': '포탄제어기 (PCU)', 'ftype': 'Status'},
        {'name': '장약제어기 (MCCU)', 'ftype': 'Status'},
        {'name': '장전제어기', 'ftype': 'Status'},
        {'name': '포/포탑 구동제어기 (GTCU)', 'ftype': 'Status'},
        {'name': '무장제어기 (CCU)', 'ftype': 'Status'},
        {'name': '모듈1상태', 'ftype': 'Status'},
        {'name': '모듈2상태', 'ftype': 'Status'},
        {'name': '모듈3상태', 'ftype': 'Status'},
    ],
    '포탑 승압기 인터페이스' : [
        { 'name': '포탄제어기 (PCU)', 'ftype': 'Status'},
        { 'name': '장약제어기 (MCCU)', 'ftype': 'Status'},
        { 'name': '장전제어기', 'ftype': 'Status'},
        { 'name': '포/포탑 구동제어기 (GTCU)', 'ftype': 'Status'},
        { 'name': '무장제어기 (CCU)', 'ftype': 'Status'},
        { 'name': '구동전원 출력 상태', 'ftype': 'Status'},
        { 'name': '모듈1상태', 'ftype': 'Status'},
        { 'name': '모듈2상태', 'ftype': 'Status'},
        { 'name': '모듈3상태', 'ftype': 'Status'},
        { 'name': '모듈4상태', 'ftype': 'Status'},
    ],
    '기타 인터페이스' : [
        {'name': 'C-bit 전송 Flag', 'ftype': 'Status'},
        {'name': 'C-bit 전송 주기', 'ftype': 'PField'},
        {'name': 'I-bit 수행 상태', 'ftype': 'Status'},
        {'name': '포탑전원조절기 버전 정보', 'ftype': 'PField'},
        {'name': '포탑승압기 버전 정보', 'ftype': 'PField'},
    ],
}
