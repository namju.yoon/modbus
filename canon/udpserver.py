import socket
import struct
import threading
import logging
import numpy as np

from PyQt5.QtCore import pyqtSignal, pyqtSlot, QObject

from logging.handlers import RotatingFileHandler
from ctypes import c_int16

from .mcode import *
from .msg import MessageProtocol

class UDPServer(QObject):
    send_requestBox = pyqtSignal(str)
    send_responseBox = pyqtSignal(str)
    send_normalMsgBox = pyqtSignal(str)
    send_highMsgBox = pyqtSignal(str)

    def __init__(self, parent, ipAddress, port):
        super(UDPServer, self).__init__(parent)
        self.parent = parent
        self.__udpServer = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.__ipAddress = ipAddress
        self.__port = port
        self.__fromAddress = None
        self.__data = None
        self.__bootflag = False
        self.__timeout = 500

        # Waiting Event
        self.event = threading.Event()
        self.starttime = None

        ## PBIT
        self.pbit_status = np.zeros((44, 4), dtype=np.int8)
        self.cbit_status = np.zeros((44, 4), dtype=np.int8)
        self.ibit_status = np.zeros((44, 4), dtype=np.int8)

        self.pbit_flag = False
        self.pbit_received_flag = False

        ## STATUS
        self.status_info = None

        ## CBIT SETUP
        self.cbit_setup_flag = False

        # self.__udpServer.settimeout(5)
        self.__udpServer.bind((self.__ipAddress, self.__port))
        logging.debug("#"*40)
        logging.debug("###### SERVER BIND :: %s", "#"*10)
        logging.debug("#"*40)

        self.__thread = threading.Thread(target=self.__listen, args=())
        self.__thread.start()

    def __listen(self):
        logging.debug("@@@@@@. UDPServer :: Start Listening .....")
        self.__stoplistening = False
        self.__receivedata = bytearray()
        try:
            while not self.__stoplistening:
                if len(self.__receivedata) == 0:
                    self.__receivedata = bytearray()
                    self.__timeout = 500
                    if self.__udpServer is not None:
                        self.__receivedata, self.__fromAddress \
                                = self.__udpServer.recvfrom(BUFFERSIZE)

                        self.__athread = threading.Thread(target=self.__action_service, args=())
                        self.__athread.start()
        except socket.timeout:
            logging.debug("__listen :: socket.timeout :: event")
            self.__receivedata = None

    def check_bootflag(self):
        return self.__bootflag

    def close_server(self):
        try:
            self.__receivedata = None
        except Exception as e:
            logging.debug("subProcess close :: %s", e)
            # self.__udpServer.close()

    @pyqtSlot(str)
    def __action_service(self):
        message = self.__receivedata
        msg = MessageProtocol()
        msg.make_protocol_header_from_msg(message)
        logging.debug(" RECEIVED :: %s\n", msg)
        
        self.send_responseBox.emit(msg.make_msgbox())
        ## self.__receivedata 를 비워야 새로운 데이터를 받을수 있음
        self.__receivedata = bytearray()

        # PCS_TCC_BOOT_DONE_UPD
        if msg.code == PCS_TCC_BOOT_DONE_UPD:
            if msg.csum_flag:
                if msg.ack == PACK:
                    self.__bootflag = True
                    ## ACK 발송
                    sendmsg = MessageProtocol(TCC_ID, PCS_ID, PCS_TCC_BOOT_DONE_UPD, CACK, 0)
            else:
                sendmsg = MessageProtocol(TCC_ID, PCS_ID, PCS_TCC_BOOT_DONE_UPD, CNACK, 0)
                
            self.sendmsg_and_display(sendmsg, "PCS_TCC_BOOT_DONE_UPD")

        ## PCS_TCC_PBIT_RESULT_REF
        elif msg.code == PCS_TCC_PBIT_RESULT_REF:
            if msg.csum_flag:
                self.make_data_bit_result(msg.data, self.pbit_status)
                self.parent.update_Pbit_result()
            else:
                sendmsg = MessageProtocol(TCC_ID, PCS_ID, PCS_TCC_PBIT_RESULT_REF, CNACK, 0)
                self.sendmsg_and_display(sendmsg, "PCS_TCC_PBIT_RESULT_REF")

        ## CBIT REPLY : PCS_TCC_CBIT_RESULT_REF
        elif msg.code == PCS_TCC_CBIT_RESULT_REF:
            if msg.csum_flag and msg.data:
                self.make_data_bit_result(msg.data, self.cbit_status)
                self.parent.update_Cbit_result()
            else:
                sendmsg = MessageProtocol(TCC_ID, PCS_ID, PCS_TCC_CBIT_RESULT_REF, CNACK, 0)
                self.sendmsg_and_display(sendmsg, "PCS_TCC_CBIT_RESULT_REF")

        ## IBIT RESULT : PCS_TCC_IBIT_RESULT_REF
        elif msg.code == PCS_TCC_IBIT_RESULT_REF:
            if msg.csum_flag:
                self.parent.iflag = True
                self.make_data_bit_result(msg.data, self.cbit_status)
                self.parent.update_Ibit_result()

        ## IBIT PERIOD : PCS_TCC_IBIT_DONE_UPD
        elif msg.code == PCS_TCC_IBIT_DONE_UPD:
            if msg.csum_flag:
                self.ibit_setup_flag = True
                self.parent.update_ibit_flag()

                ## ACK 발송
                sendmsg = MessageProtocol(TCC_ID, PCS_ID, PCS_TCC_IBIT_DONE_UPD, CACK, 0)
            else:
                sendmsg = MessageProtocol(TCC_ID, PCS_ID, PCS_TCC_IBIT_DONE_UPD, CNACK, 0)

            self.sendmsg_and_display(sendmsg, "PCS_TCC_IBIT_DONE_UPD")

        ## TCC_PCS_CBIT_SET_UPD
        elif msg.code == TCC_PCS_CBIT_SET_UPD:
            if msg.csum_flag:
                if msg.ack == PACK:
                    self.cbit_setup_flag = True
                    msg = MessageProtocol(TCC_ID, PCS_ID, TCC_PCS_CBIT_SET_UPD, CACK, 0)
                    self.sendmsg_and_display(msg, "TCC_PCS_CBIT_SET_UPD_CACK")

        ## PCS_TCC_SHUTDOWN_READY_DONE_UPD
        elif msg.code == PCS_TCC_SHUTDOWN_READY_DONE_UPD:
            if msg.csum_flag:
                data = msg.data[0]
                if data == SUCCESS:
                    logging.debug("PCS_TCC_SHUTDOWN_READY_DONE_UPD :: %s :: %s ", data, 'SUCCESS')
                else:
                    logging.debug("PCS_TCC_SHUTDOWN_READY_DONE_UPD :: %s :: %s ", data, 'FAIL')
            else:
                logging.debug("PCS_TCC_SHUTDOWN_READY_DONE_UPD :: CHECKSUM ERROR")
                msg = MessageProtocol(TCC_ID, PCS_ID, PCS_TCC_SHUTDOWN_READY_DONE_UPD, CNACK, 0)
                ## SNACK 발송
                self.sendmsg_and_display(msg, "PCS_TCC_SHUTDOWN_READY_DONE_UPD")

    
        ## PCS_TCC_HARD_EMERGENCY_STOP_REF
        elif msg.code == PCS_TCC_HARD_EMERGENCY_STOP_REF:
            logging.debug("PCS_TCC_HARD_EMERGENCY_STOP_REF :: %s\n", msg)
            if msg.data[0] == EMERGENCY_STOP:
                print(data, 'EMERGENCY_STOP')
            else:
                print(data, 'EMERGENCY_STOP_END')
            self.send_requestBox.emit(msg.make_msgbox())

        ## PCS_TCC_POWER_CTRL_UPD
        elif msg.code == PCS_TCC_POWER_CTRL_UPD:
            if msg.csum_flag:
                ## ACK 체크 ACK에 대한 RESPONSE 인지?
                if msg.ack == PACK:
                    if msg.data[0] == ON:
                        message = f'{PCS_PARTS[msg.data[1]]} :: ON 완료'
                    else:
                        message = f'{PCS_PARTS[msg.data[1]]} :: OFF 완료'
                    self.send_normalMsgBox.emit(message)

                    sendmsg = MessageProtocol(TCC_ID, PCS_ID, PCS_TCC_POWER_CTRL_UPD, CACK, 0)
                    self.sendmsg_and_display(sendmsg, "PCS_TCC_POWER_CTRL_UPD")


        ## PCS_TCC_HIGH_POWER_CTRL_UPD
        elif msg.code == PCS_TCC_HIGH_POWER_CTRL_UPD:
            if msg.csum_flag:
                ## ACK 체크 ACK에 대한 RESPONSE 인지?
                if msg.ack == PACK:
                    message = f'{HIGHPOWER_RESULT[msg.data[0]]} :: ON 완료'
                    self.send_highMsgBox.emit(message)

                    sendmsg = MessageProtocol(TCC_ID, PCS_ID, PCS_TCC_HIGH_POWER_CTRL_UPD, CACK, 0)
                    self.sendmsg_and_display(sendmsg, "PCS_TCC_HIGH_POWER_CTRL_UPD")


        ## PCS_TCC_STATE_INFO_REF
        elif msg.code == PCS_TCC_STATE_INFO_REF:
            logging.debug("PCS DATA %s", msg.data)
            # if msg.csum_flag:
                ## ACK 체크 ACK에 대한 RESPONSE 인지?
            self.status_info = msg.data
            self.parent.update_pcs_stateinfo()


    #################################################################################
    #################################################################################
    # TCC_PCS_PBIT_RESULT_REQ
    @pyqtSlot()
    def request_Pbit_status(self):
        self.pbit_flag = True
        msg = MessageProtocol(TCC_ID, PCS_ID, TCC_PCS_PBIT_RESULT_REQ, PNACK, 0)
        self.sendmsg_and_display(msg, "TCC_PCS_PBIT_RESULT_REQ_NACK")

    # TCC_PCS_CBIT_SET_UPD
    @pyqtSlot()
    def setup_cbitperiod(self, period):
        self.cbit_setup_flag = False
        # arr_period = list((period).to_bytes(2, byteorder='little'))
        msg = MessageProtocol(TCC_ID, PCS_ID, TCC_PCS_CBIT_SET_UPD, PACK, 2, period)
        self.sendmsg_and_display(msg, "TCC_PCS_CBIT_SET_UPD")
        
        # 0.1초를 기다려 보고.
        self.event.wait(ACK_TIME)

        # if not self.cbit_setup_flag:
        #     logging.debug(f">>> RESENDING ::  TCC_PCS_CBIT_SET_UPD :: self.cbit_setup_flag = {self.cbit_setup_flag}\n")
        #     timer = threading.Timer(ACK_TIME, self.setup_cbitperiod)
        #     timer.start()

    # TCC_PCS_CBIT_RESULT_REQ START
    @pyqtSlot()
    def request_Cbit_status(self, data):
        msg = MessageProtocol(TCC_ID, PCS_ID, TCC_PCS_CBIT_RESULT_REQ, PNACK, 2, data)
        self.sendmsg_and_display(msg, "TCC_PCS_CBIT_RESULT_REQ")

    # TCC_PCS_IBIT_RUN_CMD
    @pyqtSlot()
    def setup_ibitperiod(self, period):
        self.ibit_setup_flag = False
        msg = MessageProtocol(TCC_ID, PCS_ID, TCC_PCS_IBIT_RUN_CMD, PACK, 2, period)
        self.sendmsg_and_display(msg, "TCC_PCS_IBIT_RUN_CMD")
        
        # 0.1초를 기다려 보고.
        self.event.wait(ACK_TIME)

        if not self.ibit_setup_flag:
            logging.debug(f">>> RESENDING ::  TCC_PCS_CBIT_SET_UPD :: self.ibit_setup_flag = {self.ibit_setup_flag}\n")
            timer = threading.Timer(ACK_TIME, self.setup_ibitperiod)
            timer.start()

    # TCC_PCS_IBIT_RESULT_REQ
    @pyqtSlot()
    def request_Ibit_status(self, data):
        msg = MessageProtocol(TCC_ID, PCS_ID, TCC_PCS_IBIT_RESULT_REQ, PNACK, 0, data)
        self.sendmsg_and_display(msg, "TCC_PCS_IBIT_RESULT_REQ")
        
    ## TCC_PCS_RESET_UPD
    @pyqtSlot()
    def request_pcs_reset(self, val):
        msg = MessageProtocol(TCC_ID, PCS_ID, TCC_PCS_RESET_UPD, PACK, 2, val)
        self.sendmsg_and_display(msg, "TCC_PCS_RESET_UPD") 
        
    # TCC_PCS_STATE_INFO_REQ
    @pyqtSlot()
    def request_pcs_status_info(self):
        msg = MessageProtocol(TCC_ID, PCS_ID, TCC_PCS_STATE_INFO_REQ, PNACK, 0)
        self.sendmsg_and_display(msg, "TCC_PCS_STATE_INFO_REQ_NOACK")
        
    # TCC_PCS_POWER_CTRL_CMD
    def power_control_cmd(self, onoff, part):
        msg = MessageProtocol(TCC_ID, PCS_ID, TCC_PCS_POWER_CTRL_CMD, PACK, 4, [onoff, part])
        self.sendmsg_and_display(msg, "TCC_PCS_POWER_CTRL_CMD")
      
    # TCC_PCS_HIGH_POWER_CTRL_CMD
    def high_power_control_cmd(self, onoff):
        msg = MessageProtocol(TCC_ID, PCS_ID, TCC_PCS_HIGH_POWER_CTRL_CMD, PACK, 2, [onoff])
        self.sendmsg_and_display(msg, "TCC_PCS_HIGH_POWER_CTRL_CMD")
    
    # TCC_PCS_SOFT_EMERGENCY_STOP_UPD
    def emergency_stop(self, data):
        # data = []
        msg = MessageProtocol(TCC_ID, PCS_ID, TCC_PCS_SOFT_EMERGENCY_STOP_UPD, PACK, 2, data)
        self.sendmsg_and_display(msg, "TCC_PCS_SOFT_EMERGENCY_STOP_UPD")

    ## TCC_PCS_SHUTDOWN_READY_REQ
    @pyqtSlot()
    def request_shutdown_ready(self):
        self.pbit_flag = True
        msg = MessageProtocol(TCC_ID, PCS_ID, TCC_PCS_SHUTDOWN_READY_REQ, PNACK, 0)
        self.sendmsg_and_display(msg, "TCC_PCS_SHUTDOWN_READY_REQ_NACK")
        

    #################################################################################
    #################################################################################
    def close(self):
        """
        Closes Serial port, or TCP-Socket connection
        """
        if self.__udpServer is not None:
            self.__stoplistening = True
            self.__udpServer.shutdown(socket.SHUT_RDWR)
            self.__udpServer.close()
        self.__connected = False


    def send(self, data, address):
        try:
            self.__udpServer.sendto(data, address)
        except Exception as e:
            pass
        
    def make_data_bit_result(self, data, bit_status):
        # data = list(message[6:-2])
        temp0 = 0b00000011
        temp1 = 0b00001100
        temp2 = 0b00110000
        temp3 = 0b11000000
        for idx, ddd in enumerate(data):
            try:
                if ddd:
                    d0 = (ddd & temp0)
                    d1 = (ddd & temp1) >> 2
                    d2 = (ddd & temp2) >> 4
                    d3 = (ddd & temp3) >> 6

                    bit_status[idx][0] = d0
                    bit_status[idx][1] = d1
                    bit_status[idx][2] = d2
                    bit_status[idx][3] = d3
                    logging.debug(f"BIT WARNING :: {idx} :: {data} :: d0 = {d0} :: d1 = {d1} :: d2 = {d2} :: d3 = {d3} ")
                else:
                    bit_status[idx][0] = 0
                    bit_status[idx][1] = 0
                    bit_status[idx][2] = 0
                    bit_status[idx][3] = 0
            except Exception as e:
                pass
                # logging.debug("ERROR :: %d :: %s\n", idx, ddd)


    ## COMMON 
    @pyqtSlot()
    def sendmsg_and_display(self, msg, ptype):
        msg.make_encode()
        self.send(msg.message, self.__fromAddress)
        self.send_requestBox.emit(msg.make_msgbox())

        logging.debug("REQUEST :: %s\n", msg)
