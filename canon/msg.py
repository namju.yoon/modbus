import struct
import random
import logging

from PyQt5.QtWidgets import QLabel, QPushButton, QCheckBox, QSizePolicy
from PyQt5.QtWidgets import QGroupBox, QVBoxLayout, QGridLayout, QHBoxLayout, QLineEdit
from PyQt5.QtCore import QObject

from .mcode import *

# import struct
# message = b'P\x01 \x10\x00\x00\x81\x00'
# sourceID, destinationID, code, ack, dataLength = \
#             struct.unpack("<BBBBH", message[:6])
# sourceID, destinationID, code, ack, dataLength
# pattern="<" + "H"*int(dataLength/2)
# pattern="<" + "B"*dataLength
# data = list(struct.unpack(pattern, message[6:-2]))
# csum = struct.unpack("<H", message[-2:])[0]

class MessageProtocol(QObject):
    """ Implement Protocol Header
    The request Header with function code 06 must be 5 bytes:
        ================    ===============
        Field               Length (bytes)
        ================    ===============
        HEADER              6
            Source ID           1
            Destination ID      1
            CODE                1     
            ACK 필요 여부         1
            DATA LENGTH         2
        CHECKSUM                2
        ================    ===============
    """
    def __init__(self, sourceID=None, destinationID=None, 
                code=None, ack=None, dataLength=0, data=None):
        self.sourceID = sourceID
        self.destinationID = destinationID
        self.code = code
        self.ack = ack
        self.dataLength = dataLength
        self.data = data
        self.csum = None
        self.csum_flag = False
        self.ptype = 1
        self.message = bytearray()

    def checksum(self):
        headsum = self.sourceID + self.destinationID + self.code + self.ack + self.dataLength
        if self.dataLength:
            for dt in self.data:
                bytes_val = (dt).to_bytes(2, byteorder='little')
                temp_val = list(bytes_val)
                headsum += sum(temp_val)
        return headsum

    def make_msgbox(self):
        return f"{self.sourceID}:{self.destinationID}:{self.code}:{self.ack}:{self.dataLength}:{self.data}"
    
    def make_encode(self):
        self.message = bytearray()
        self.message.append(self.sourceID)
        self.message.append(self.destinationID)
        self.message.append(self.code)
        self.message.append(self.ack)
        
        dlen = (self.dataLength).to_bytes(2, byteorder='little')
        self.message += dlen

        if self.dataLength:
            for val in self.data:
                self.message += (val).to_bytes(2, byteorder='little')

        self.csum = self.checksum()
        csum = (self.csum).to_bytes(2, byteorder='little')
        self.message += csum

    def make_protocol_header_from_msg(self, message):
        self.ptype = 2
        self.sourceID, self.destinationID, self.code, self.ack, self.dataLength = \
            struct.unpack("<BBBBH", message[:6])

        ## data가 있으면.
        try:
            if self.dataLength:
                if self.code in [PCS_TCC_PBIT_RESULT_REF, PCS_TCC_CBIT_RESULT_REF, 
                        PCS_TCC_IBIT_RESULT_REF]:

                    pattern="<" + "B"*self.dataLength
                    self.data = list(struct.unpack(pattern, message[6:-2]))

                else:
                    pattern="<" + "H"*int(self.dataLength/2)
                    self.data = list(struct.unpack(pattern, message[6:-2]))

            self.csum = struct.unpack("<H", message[-2:])[0]
            self.csum_flag = True if self.csum == self.checksum() else False

        except Exception as e:
            logging.debug("make_general_from_message Error :: %s", message)

    def __str__(self):
        if self.ptype == 1:
            pcode = TCC_CODE.get(str(self.code), 'NOT REGISTED OR RESPONSE')
        else:
            pcode = PCS_CODE.get(str(self.code), 'NOT REGISTED OR RESPONSE')

        return f"SID = {self.sourceID}, DID = {self.destinationID}, CODE = {pcode}, ACK = {self.ack}, DLEN = {self.dataLength}, DATA = {self.data}, CSUM = {self.csum}, ** FLAG = {self.csum_flag} "


def CLed(name, color):
    btn = QPushButton(name)
    style = f"border: 3px solid lightgray;border-radius: 10px;background-color: {color};color: black;font-size: 14px;font-weight: bold;"
    btn.setStyleSheet(style)
    btn.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
    return btn


class Status:
    grayfont = "color: #222;"
    bluefont = "color: #0000ff;"
    yellowfont = "color: #FFC300;font-weight: bold;"
    redfont = "color: #ff0000;font-weight: bold;"

    def __init__(self, name, st1, st2, st3=None, st4=None):
        self.name = name
        self.cb_st1 = QCheckBox(st1)
        self.cb_st1.setStyleSheet(Status.grayfont)

        self.cb_st2 = QCheckBox(st2)
        self.cb_st2.setStyleSheet(Status.grayfont)

        if st3:
            self.cb_st3 = QCheckBox(st3)
            self.cb_st3.setStyleSheet(Status.grayfont)
        else:
            self.cb_st3 = None

        if st4:
            self.cb_st4 = QCheckBox(st4)
            self.cb_st4.setStyleSheet(Status.grayfont)
        else:
            self.cb_st4 = None

    def display(self):
        group = QGroupBox(self.name)
        layout = QVBoxLayout()
        group.setLayout(layout)

        layout.addWidget(self.cb_st1)
        layout.addWidget(self.cb_st2)
        if self.cb_st3:
            layout.addWidget(self.cb_st3)
        if self.cb_st4:
            layout.addWidget(self.cb_st4)
        return group

    def display2row(self):
        group = QGroupBox(self.name)
        layout = QGridLayout()
        group.setLayout(layout)

        layout.addWidget(self.cb_st1, 0, 0)
        layout.addWidget(self.cb_st2, 0, 1)
        if self.cb_st3:
            layout.addWidget(self.cb_st3, 1, 0)
        if self.cb_st4:
            layout.addWidget(self.cb_st4, 1, 1)
        return group

    def change_status(self, val):
        try:
            if val == 0:
                self.cb_st1.setChecked(True)
                self.cb_st1.setStyleSheet(Status.grayfont)

                self.cb_st2.setChecked(False)
                self.cb_st2.setStyleSheet(Status.grayfont)

                if self.cb_st3:
                    self.cb_st3.setChecked(False)
                    self.cb_st3.setStyleSheet(Status.grayfont)

                if self.cb_st4:
                    self.cb_st4.setChecked(False)
                    self.cb_st4.setStyleSheet(Status.grayfont)

            elif val == 1:
                self.cb_st1.setChecked(False)
                self.cb_st1.setStyleSheet(Status.grayfont)

                self.cb_st2.setChecked(True)
                self.cb_st2.setStyleSheet(Status.bluefont)

                if self.cb_st3:
                    self.cb_st3.setChecked(False)
                    self.cb_st3.setStyleSheet(Status.grayfont)

                if self.cb_st4:
                    self.cb_st4.setChecked(False)
                    self.cb_st4.setStyleSheet(Status.grayfont)

            elif val == 2:
                self.cb_st1.setChecked(False)
                self.cb_st1.setStyleSheet(Status.grayfont)

                self.cb_st2.setChecked(False)
                self.cb_st2.setStyleSheet(Status.grayfont)

                self.cb_st3.setChecked(True)
                self.cb_st3.setStyleSheet(Status.redfont)

                if self.cb_st4:
                    self.cb_st4.setChecked(False)
                    self.cb_st4.setStyleSheet(Status.grayfont)

            elif val == 3:
                self.cb_st1.setChecked(False)
                self.cb_st1.setStyleSheet(Status.grayfont)

                self.cb_st2.setChecked(False)
                self.cb_st2.setStyleSheet(Status.grayfont)

                self.cb_st3.setChecked(False)
                self.cb_st3.setStyleSheet(Status.grayfont)

                self.cb_st4.setChecked(True)
                self.cb_st4.setStyleSheet(Status.yellowfont)
        except Exception as e:
            logging.debug(e)


class Field:
    grayfont = "color: #222;"
    bluefont = "color: #0000ff;"
    yellowfont = "color: #FFC300;font-weight: bold;"
    redfont = "color: #ff0000;font-weight: bold;"

    def __init__(self, mod, name, byte, nth, value, pbit=0, cbit=0, ibit=0):
        self.mod = mod
        self.name = name 
        self.byte = byte 
        self.nth = nth
        self.value = value

        self.pbit = pbit
        self.cbit = cbit
        self.ibit = ibit

        self.lbl_name = QLabel(self.name)
        self.rd_normal = QCheckBox("정상")
        self.rd_normal.setChecked(True)
        self.rd_normal.setStyleSheet(Field.bluefont)
        self.rd_warning = QCheckBox("경고")
        self.rd_fail = QCheckBox("고장")

    def set_value(self, value):
        self.value = value

    def change_normal(self):
        self.value = NORMAL
        self.rd_normal.setChecked(True)
        self.rd_normal.setStyleSheet(Field.bluefont)

        self.rd_warning.setChecked(False)
        self.rd_warning.setStyleSheet(Field.grayfont)

        self.rd_fail.setChecked(False)
        self.rd_fail.setStyleSheet(Field.grayfont)

        self.mod.update_normal()

    def change_warning(self):
        self.value = WARNING
        self.rd_normal.setChecked(False)
        self.rd_normal.setStyleSheet(Field.grayfont)

        self.rd_warning.setChecked(True)
        self.rd_warning.setStyleSheet(Field.yellowfont)

        self.rd_fail.setChecked(False)
        self.rd_fail.setStyleSheet(Field.grayfont)

        self.mod.update_warning()


    def change_fail(self):
        self.value = FAIL
        self.rd_normal.setChecked(False)
        self.rd_normal.setStyleSheet(Field.grayfont)

        self.rd_warning.setChecked(False)
        self.rd_warning.setStyleSheet(Field.grayfont)

        self.rd_fail.setChecked(True)
        self.rd_fail.setStyleSheet(Field.redfont)

        self.mod.update_fail()
        # print("change_fail :: ", self)

    def __str__(self):
        return f"## ({self.byte}, {self.nth}) :: {self.mod}::{self.name}"


class Module:
    cyanfont = "border: 3px solid lightgray;border-radius: 10px;background-color:#ccffff;color: black;font-size: 14px;font-weight: bold;"
    yellowfont = "border: 3px solid lightgray;border-radius: 10px;background-color: #FFC300;color: black;font-size: 14px;font-weight: bold;"
    redfont = "border: 3px solid lightgray;border-radius: 10px;background-color: #FF0000;color: black;font-size: 14px;font-weight: bold;"

    def __init__(self, name):
        self.name = name 
        self.fields = []
        self.value = NORMAL
        self.btn = CLed(name, "#ccffff")

    def update_normal(self):
        self.value = NORMAL
        self.btn.setStyleSheet(Module.cyanfont)

    def update_warning(self):
        self.value = WARNING
        self.btn.setStyleSheet(Module.yellowfont)

    def update_fail(self):
        self.value = FAIL
        self.btn.setStyleSheet(Module.redfont)

    def add_field(self, field):
        self.fields.append(field)

    def display(self):
        group = QGroupBox(self.name)
        # group.setStyleSheet("padding: 12px 0px 0px 0px")
        layout = QGridLayout()
        group.setLayout(layout)

        for idx, field in enumerate(self.fields):
            layout.addWidget(field.lbl_name, idx, 0)
            layout.addWidget(field.rd_normal, idx, 1)
            layout.addWidget(field.rd_warning, idx, 2)
            layout.addWidget(field.rd_fail, idx, 3)

        return group

    def __str__(self):
        return f"#### Module Name = {self.name}"


class SubSystem:
    def __init__(self, name):
        self.name = name 
        self.modules = []

        self.btn = CLed(name, "#ccff66")
        
    def add_module(self, module):
        self.modules.append(module)

    def display(self):
        group = QGroupBox(self.name)
        # group.setStyleSheet("margin: 0.2em;")
        layout = QVBoxLayout()
        group.setLayout(layout)

        for idx, module in enumerate(self.modules):
            layout.addWidget(module.display())

        return group

    def __str__(self):
        return f"### SubSystem Name = {self.name}"


class PField:
    def __init__(self, mod, name, divide=1, unit=""):
        self.mod = mod
        self.name = name 
        self.divide = divide
        self.value = 0

        self.lbl_name = QLabel(self.name)
        self.edit_value = QLineEdit("")
        self.unit = QLabel(unit)

    def set_value(self, val):
        self.value = round(val/self.divide, 0)
        self.edit_value.setText(str(self.value))

    def __str__(self):
        return f"## ({self.mod}, {self.name}) :: {self.divide}::{self.unit}"


class PModule:
    def __init__(self, name):
        self.name = name 
        self.fields = []

    def display(self):
        group = QGroupBox(self.name)
        # group.setStyleSheet("padding: 12px 0px 0px 0px")
        layout = QGridLayout()
        group.setLayout(layout)

        for idx, field in enumerate(self.fields):
            layout.addWidget(field.lbl_name, idx, 0)
            layout.addWidget(field.edit_value, idx, 1)
            layout.addWidget(field.unit, idx, 2)

        return group

    def __str__(self):
        return self.name



if __name__ == "__main__":
    data = make_random_pbit_result()

    msg = MessageProtocol(PCS_ID, TCC_ID, PCS_TCC_PBIT_RESULT_REF, PNACK, 42, data)
    msg.make_encode()
    msg.make_decode()

