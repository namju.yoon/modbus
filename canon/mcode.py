## ID
TCC_ID = 0x01 
PCS_ID = 0x50

PACK = 0x01
PNACK = 0xFF

CACK = 0x10
CNACK = 0x11

TCC_HOST_IP = "192.168.200.100"
# TCC_BOOT_IP = "127.0.0.1"
TCC_HOST_PORT = 15001

PCS_HOST_IP = "192.168.200.120"
# PCS_HOST_IP = "localhost"
PCS_HOST_PORT = 45000

BUFFERSIZE = 512
BOOT_CHECK_REPEAT = 3
ACK_TIME = 1000
STATUS_TIME = 2000
TIME_WAIT = 0.2

NORMAL = 0
WARNING = 1
FAIL = 2


# 0x01 : Fault
# 0x02 : CPU
FAULT_RESET = 0x01
CPU_RESET = 0x02


# 0x01 : 저전원 ON
# 0x02 : 저전원 OFF"
ON = 0x01
OFF = 0x02

# 0x01 : ALL
# 0x02 : 포/포탑구동장치
# 0x03 : 포탄적재이송장치
# 0x04 : 장약적재이송장치
# 0x05 : 탄약장전장치
# 0x06 : 포미개폐장치
# 0x07 : 항법장치
# 0x08 : 자동시한장입장치
# 0x0A : 통신장치(무전기)

ALL = 0x01
BULLET_CTL = 0x02
POWDER_CTL = 0x03
READY_CTL = 0x04
TURRET_CTL = 0x05
REAR_CTL = 0x06
NAVI_CTL = 0x07
AUTO_CTL = 0x08
COMMU_CTL = 0x0A

PCS_PARTS = {
    ALL: "ALL",
    BULLET_CTL: "포/포탑구동장치",
    POWDER_CTL: "포탄적재이송장치",
    READY_CTL: "장약적재이송장치",
    TURRET_CTL: "탄약장전장치",
    REAR_CTL: "포미개폐장치",
    NAVI_CTL: "항법장치",
    AUTO_CTL: "자동시한장입장치",
    COMMU_CTL: "통신장치(무전기)",
}

ON_COMPLETE = 0x01
OFF_COMPLETE = 0x02
ON_FAIL = 0x03
OFF_FAIL = 0x04

HIGHPOWER_RESULT = {
    ON_COMPLETE: "고전원 ON 완료",
    OFF_COMPLETE: "고전원 OFF 완료",
    ON_FAIL: "고전원 ON 미완료(실패)",
    OFF_FAIL: "고전원 OFF 미완료(실패)",
}


# "0x01 : 완료
# 0x02 : 실패"
SUCCESS = 0x01
FAIL = 0x02

# "0x01 : 비상정지
# 0x02 : 비상정지 해제"
EMERGENCY_STOP = 0x01
EMERGENCY_STOP_END = 0x02


## K9A2_ICD TCC(CLIENT) ==> PCS(SERVER)
TCC_PCS_SHUTDOWN_READY_REQ = 0x02
TCC_PCS_PBIT_RESULT_REQ = 0x10
TCC_PCS_CBIT_SET_UPD = 0x11
TCC_PCS_CBIT_RESULT_REQ = 0x12
TCC_PCS_IBIT_RUN_CMD    = 0x13
TCC_PCS_IBIT_RESULT_REQ = 0x14
TCC_PCS_RESET_UPD   = 0x20
TCC_PCS_POWER_CTRL_CMD  = 0x30
TCC_PCS_HIGH_POWER_CTRL_CMD = 0x31
TCC_PCS_SOFT_EMERGENCY_STOP_UPD = 0x40
TCC_PCS_STATE_INFO_REQ  = 0xA0

TCC_CODE = {
    "1": "TCC_PCS_BOOT_DONE_UPD",
    "2": "TCC_PCS_SHUTDOWN_READY_REQ",
    "16": "TCC_PCS_PBIT_RESULT_REQ",
    "17": "TCC_PCS_CBIT_SET_UPD",
    "18": "TCC_PCS_CBIT_RESULT_REQ",
    "19": "TCC_PCS_IBIT_RUN_CMD   ",
    "20": "TCC_PCS_IBIT_RESULT_REQ",
    "32": "TCC_PCS_RESET_UPD",
    "48": "TCC_PCS_POWER_CTRL_CMD ",
    "49": "TCC_PCS_HIGH_POWER_CTRL_CMD",
    "64": "TCC_PCS_SOFT_EMERGENCY_STOP_UPD",
    "160": "TCC_PCS_STATE_INFO_REQ",
}

## K9A2_ICD PCS(SERVER) ==> TCC(CLIENT)
PCS_TCC_BOOT_DONE_UPD = 0x01
PCS_TCC_SHUTDOWN_READY_DONE_UPD = 0x02
PCS_TCC_PBIT_RESULT_REF = 0x10
PCS_TCC_CBIT_RESULT_REF = 0x12
PCS_TCC_IBIT_DONE_UPD   = 0x13
PCS_TCC_IBIT_RESULT_REF = 0x14
PCS_TCC_HARD_EMERGENCY_STOP_REF  = 0x21
PCS_TCC_POWER_CTRL_UPD  = 0x30
PCS_TCC_HIGH_POWER_CTRL_UPD = 0x31
PCS_TCC_STATE_INFO_REF  = 0xA0

PCS_CODE = {
    "1": "PCS_TCC_BOOT_DONE_UPD",
    "2": "PCS_TCC_SHUTDOWN_READY_DONE_UPD",
    "16": "PCS_TCC_PBIT_RESULT_REF",
    "17": "PCS_TCC_CBIT_SET_UPD",
    "18": "PCS_TCC_CBIT_RESULT_REF",
    "19": "PCS_TCC_IBIT_DONE_UPD",
    "20": "PCS_TCC_IBIT_RESULT_REF",
    "32": "TCC_PCS_RESET_UPD",
    "33": "PCS_TCC_HARD_EMERGENCY_STOP_REF",
    "48": "PCS_TCC_POWER_CTRL_UPD",
    "49": "PCS_TCC_HIGH_POWER_CTRL_UPD",
    "160": "PCS_TCC_STATE_INFO_REF",
}

NAMES = {
    "포탑전원조절기 1" : [
        {
            "포탑전원조절기 모듈 1 MAIN B/D" : [
                "입력 과전류",
                "입력 과전압",
                "입력 저전압",
                "출력 과전류",
                "출력 과전압",
                "출력 저전압",
                "내부 과(공진)전류",
                "내부 과온도",
                "내부 통신 차단",
            ]
        }, {
            "포탑전원조절기 모듈 2 MAIN B/D": [
                "입력 과전류",
                "입력 과전압",
                "입력 저전압",
                "출력 과전류",
                "출력 과전압",
                "출력 저전압",
                "내부 과(공진)전류",
                "내부 과온도",
                "내부 통신 차단",
            ]
        }, {
            "포탑전원조절기 모듈 3 MAIN B/D": [
                "입력 과전류",
                "입력 과전압",
                "입력 저전압",
                "출력 과전류",
                "출력 과전압",
                "출력 저전압",
                "내부 과(공진)전류",
                "내부 과온도",
                "내부 통신 차단",
            ]
        }], 

    "포탑전원조절기 2" : [
        {
            "포탑전원조절기 모듈 4 MAIN B/D": [
                "입력 과전류",
                "입력 과전압",
                "입력 저전압",
                "출력 과전류",
                "출력 과전압",
                "출력 저전압",
                "내부 과(공진)전류",
                "내부 과온도",
                "내부 통신 차단",
            ]
        }, {
            "포탑전원조절기 모듈 5 MAIN B/D": [
                "입력 과전류",
                "입력 과전압",
                "입력 저전압",
                "출력 과전류",
                "출력 과전압",
                "출력 저전압",
                "내부 과(공진)전류",
                "내부 과온도",
                "내부 통신 차단",
            ]
        }],
    "포탑전원조절기 3" : [
        {
            "포탑전원조절기 인터페이스 MAIN B/D":[
                "보조축전지 전환",
                "보조축전지 저전압",
                "보조축전지 과 충전전류",
                "사수전시기(GDU) 출력 과전류",
                "사통컴퓨터(TCC) 출력 과전류",
                "포탄적재제어기(PTU) 출력 과전류",
                "장약적재이송제어기(CTU) 출력 과전류",
                "탄약장전제어기(ARU) 출력 과전류",
                "포탑승압기(TBU) 출력 과전류",
                "포탑구동제어기(TDU) 출력 과전류",
                "포미폐쇄제어기(GBU) 출력 과전류",
                "무전기set(VRC-947K) 출력 과전류",
                "항법장치(DRUH) 출력 과전류",
                "자동시한장입장치(AFSS) 출력 과전류",
                "부수장치(영상저장장치, 이더넷 스위치 등) 출력 과전류",
                "전원분배기 연결체(J3) 분리",
                "전원분배기 연결체(J4) 분리",
                "전원분배기 연결체(J5) 분리",
            ]
        }
    ],
    "연결/통신 포탑전원조절기 타구성품 연동 1" : [
        {"포탑 전원분배기_Cable Loop Group_1(연결)": ["전원분배기 연결체 분리(GDU)"]},
        {"포탑 전원분배기_Cable Loop Group_2(연결)": ["전원분배기 연결체 분리(TBU, TCC, 부수장치)"]},
        {"포탑 전원분배기_Cable Loop Group_3(연결)": ["전원분배기 연결체 분리(PTU/CTU, ARU)"]},
        {"포탑 전원분배기_Cable Loop Group_4(연결)": ["전원분배기 연결체 분리(TDC/GBU)"]},
        {"포탑 전원분배기_Cable Loop Group_5(연결)": ["전원분배기 연결체 분리(DRUM, AFSS)"]},
        {"사수전시기(GDU)(전원)":  ["사수전시기(GDU) 회로차단기 OFF"]},
        {"사통컴퓨터(TCC)(전원)":  ["사통컴퓨터(TCC) 회로차단기 OFF"]},
        {"포탄적재제어기(PTU)(전원)":  ["  포탄적재제어기(PTU) 회로차단기 OFF"]},
        {"장약적재이송제어기(CTU)(전원)":  ["장약적재이송제어기(CTU) 회로차단기 OFF"]},
    ],
    "연결/통신 포탑전원조절기 타구성품 연동 2" : [
        {"탄약장전제어기(ARU)(전원)":  ["탄약장전제어기(ARU) 회로차단기 OFF"]},
        {"포탑승압기(TBU)(전원)":  ["포탑승압기(TBU) 회로차단기 OFF"]},
        {"포탑구동제어기(TDU)(전원)":  ["포탑구동제어기(TDU) 회로차단기 OFF"]},
        {"포미폐쇄제어기(GBU)(전원)":  ["포미폐쇄제어기(GBU) 회로차단기 OFF"]},
        {"무전기set(VRC-947K)(전원)":  ["무전기set(VRC-947K) 회로차단기 OFF"]},
        {"항법장치(DRUH)(전원)":  ["항법장치(DRUH) 회로차단기 OFF"]},
        {"자동시한장입장치(AFSS)(전원)":  ["자동시한장입장치(AFSS) 회로차단기 OFF"]},
        {"부수장치(영상저장장치, 이더넷 스위치, 이더넷 익스텐더)(전원)": ["부수장치(영상저장장치, 이더넷 스위치, 이더넷 익스텐더) 회로차단기 OFF"]},
        {"사격통제컴퓨터(통신)": ["외부 통신 이상"]},
        {"포탑승압기(통신)" : ["외부 통신 이상"]},
        {"차제전원조절기(통신)": ["외부 통신 이상"]},
    ],
    "포탑승압기 1": [
        {
            "포탑승압기 모듈 1 POWER B/D": [
                "입력 과전류",
                "입력 과전압",
                "입력 저전압",
                "출력 과전류",
                "출력 과전압",
                "출력 저전압",
                "내부 과온도",
                "내부 과(공진)전류",
            ]
        }, {
            "포탑승압기 모듈 1 CONTROL B/D": ["내부 통신 차단"],
        }, {
            "포탑승압기 모듈 2 POWER B/D": [
                "입력 과전류",
                "입력 과전압",
                "입력 저전압",
                "출력 과전류",
                "출력 과전압",
                "출력 저전압",
                "내부 과온도",
                "내부 과(공진)전류",
            ]
        }, {
            "포탑승압기 모듈 2 CONTROL B/D": ["내부 통신 차단"],
        }], 
    "포탑승압기 2": [
        {
            "포탑승압기 모듈 3 POWER B/D": [
                "입력 과전류",
                "입력 과전압",
                "입력 저전압",
                "출력 과전류",
                "출력 과전압",
                "출력 저전압",
                "내부 과온도",
                "내부 과(공진)전류",
            ]
        }, {
            "포탑승압기 모듈 3 CONTROL B/D": ["내부 통신 차단"],
        }, {
            "포탑승압기 모듈 4 POWER B/D": [
                "입력 과전류",
                "입력 과전압",
                "입력 저전압",
                "출력 과전류",
                "출력 과전압",
                "출력 저전압",
                "내부 과온도",
                "내부 과(공진)전류",
            ]
        }, {
            "포탑승압기 모듈 4 CONTROL B/D": ["내부 통신 차단"],
        }], 
    "포탑승압기 3": [
        {
            "포탑승압기 모듈 5 POWER B/D": [
                "입력 과전류",
                "입력 과전압",
                "입력 저전압",
                "출력 과전류",
                "출력 과전압",
                "출력 저전압",
                "내부 과온도",
                "내부 과(공진)전류",
            ]
        }, {
            "포탑승압기 모듈 5 CONTROL B/D": ["내부 통신 차단"],
        }, {
            "포탑승압기 모듈 6 POWER B/D": [
                "입력 과전류",
                "입력 과전압",
                "입력 저전압",
                "출력 과전류",
                "출력 과전압",
                "출력 저전압",
                "내부 과온도",
                "내부 과(공진)전류",
            ]
        }, {
            "포탑승압기 모듈 6 CONTROL B/D": ["내부 통신 차단"],
        }, {
            "포탑승압기 인터페이스 CONTROL B/D": ["외부 통신 이상"]
        }, 
    ],
    "연결/통신 포탑승압기 타구성품 연동" : [
        {
            "연결/통신 포탑승압기 타구성품 연동": [
                "이더넷 스위치, 포탑 전원분배기(연결):커넥터 이상, 이더넷, 제어전원 동작 ON 안됨 -> 검출 안됨, ",
                "포탑 전원분배기(전원):커넥터 연결 이상, 승압기의 제어 전원 연결이 안됨 -> 검출 안됨",
                "포탄적재이송제어기(연결):커넥터 연결 이상",
                "포탄적재이송제어기(전원):커넥터 출력 과전류",
                "장약적재이송제어기(연결):커넥터 연결 이상",
                "장약적재이송제어기(전원):커넥터 출력 과전류",
                "복합장전제어기#3(연결):커넥터 연결 이상",
                "복합장전제어기#3(전원):커넥터 출력 과전류",
                "복합장전제어기#1(연결):커넥터 연결 이상",
                "복합장전제어기#1(전원):커넥터 출력 과전류",
                "복합장전제어기#2(연결):커넥터 연결 이상",
                "복합장전제어기#2(전원):커넥터 출력 과전류",
                "포/포탑구동제어기(연결):커넥터 연결 이상",
                "포/포탑구동제어기(전원):커넥터 출력 과전류",
                "포미개폐제어기(연결):커넥터 연결 이상",
                "포미개폐제어기(전원):커넥터 출력 과전류",
                "포탑전원조절기(통신):외부 통신 이상",
            ]
        }
    ],
    "차체전원조절기" : [
        {"차체전원조절기 모듈 MAIN B/D": [
            "입력 과전류",
            "입력 과전압",
            "입력 저전압",
            "출력 과전류",
            "출력 과전압",
            "출력 저전압",
            "내부 과(공진)전류",
            "내부 과온도",
            "내부 통신 차단",
        ]},
    ],
    "연결/통신 차체전원조절기 타구성품 연동": [
        {
            "연결/통신 차체전원조절기": [
                "조종수해치스위치, 포신잠금(연결) : 커넥터 연결 확인",
                "이더넷스위치(전원) : 커넥터 연결 확인",
                "이더넷스위치, 이더넷익스텐더(전원) : 커넥터 연결 확인",
                "차체벽(전원) : 커넥터 연결 확인",
                "포탑전원조절기(통신) : 외부 통신 이상",
                "이더넷 스위치, 익스텐더 과전류 : 출력 과전류",
                "차체벽 과전류 : 출력 과전류",
            ]
        }
    ]
}

PCSSTATUS = {
    '전원조절기 인터페이스' : [
        { 'name': '상태정보상세', 'divide': 1, 'unit': ''},
        { 'name': '경고정보 상세', 'divide': 1, 'unit': ''},
        { 'name': '경고정보2 상세', 'divide': 1, 'unit': ''},
        { 'name': '고장정보 상세', 'divide': 1, 'unit': ''},
        { 'name': '고장정보2 상세', 'divide': 1, 'unit': ''},
        { 'name': '배터리 전압(전원조절기 소스)', 'divide': 1, 'unit': 'V'},
        { 'name': '배터리 전압(사통 축전지)', 'divide': 1, 'unit': 'V'},
        { 'name': '입력전류', 'divide': 1, 'unit': 'A'},
        { 'name': 'Aux IN', 'divide': 1, 'unit': 'A'},
        { 'name': '배터리 충전 전류', 'divide': 1, 'unit': 'A'},
        { 'name': '출력 전류 1', 'divide': 1, 'unit': 'A'},
        { 'name': '출력 전류 2', 'divide': 1, 'unit': 'A'},
        { 'name': '출력 전류 3', 'divide': 1, 'unit': 'A'},
        { 'name': '출력 전류 4', 'divide': 1, 'unit': 'A'},
        { 'name': '출력 전류 5', 'divide': 1, 'unit': 'A'},
        { 'name': '출력 전류 6', 'divide': 1, 'unit': 'A'},
        { 'name': '출력 전류 7', 'divide': 1, 'unit': 'A'},
        { 'name': '출력 전류 8', 'divide': 1, 'unit': 'A'},
        { 'name': '출력 전류 9', 'divide': 1, 'unit': 'A'},
        { 'name': '출력 전류 10', 'divide': 1, 'unit': 'A'},
        { 'name': '출력 전류 11', 'divide': 1, 'unit': 'A'},
        { 'name': '출력 전류 12', 'divide': 1, 'unit': 'A'},
    ],
    '전원조절기 모듈 1' : [
        {'name': '상태정보상세', 'divide': 1, 'unit': ''},
        {'name': '경고정보 상세', 'divide': 1, 'unit': ''},
        {'name': '고장정보 상세', 'divide': 1, 'unit': ''},
        {'name': '입력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '출력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '입력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '출력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '소비 전력', 'divide': 1, 'unit': ''},
        {'name': 'SW 버전', 'divide': 1, 'unit': ''},
    ],
    '전원조절기 모듈 2' : [
        {'name': '상태정보상세', 'divide': 1, 'unit': ''},
        {'name': '경고정보 상세', 'divide': 1, 'unit': ''},
        {'name': '고장정보 상세', 'divide': 1, 'unit': ''},
        {'name': '입력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '출력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '입력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '출력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '소비 전력', 'divide': 1, 'unit': ''},
        {'name': 'SW 버전', 'divide': 1, 'unit': ''},
    ],
    '전원조절기 모듈 3' : [
        {'name': '상태정보상세', 'divide': 1, 'unit': ''},
        {'name': '경고정보 상세', 'divide': 1, 'unit': ''},
        {'name': '고장정보 상세', 'divide': 1, 'unit': ''},
        {'name': '입력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '출력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '입력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '출력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '소비 전력', 'divide': 1, 'unit': ''},
        {'name': 'SW 버전', 'divide': 1, 'unit': ''},
    ],
    '전원조절기 모듈 4' : [
        {'name': '상태정보상세', 'divide': 1, 'unit': ''},
        {'name': '경고정보 상세', 'divide': 1, 'unit': ''},
        {'name': '고장정보 상세', 'divide': 1, 'unit': ''},
        {'name': '입력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '출력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '입력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '출력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '소비 전력', 'divide': 1, 'unit': ''},
        {'name': 'SW 버전', 'divide': 1, 'unit': ''},
    ],
    '전원조절기 모듈 5' : [
        {'name': '상태정보상세', 'divide': 1, 'unit': ''},
        {'name': '경고정보 상세', 'divide': 1, 'unit': ''},
        {'name': '고장정보 상세', 'divide': 1, 'unit': ''},
        {'name': '입력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '출력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '입력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '출력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '소비 전력', 'divide': 1, 'unit': ''},
        {'name': 'SW 버전', 'divide': 1, 'unit': ''},
    ],
    ' 승압기 인터페이스': [
        {'name': '상태 정보상세', 'divide': 1, 'unit': ''},
        {'name': '경고 정보상세', 'divide': 1, 'unit': ''},
        {'name': '고장 정보상세', 'divide': 1, 'unit': ''},
        {'name': 'IOP1', 'divide': 1, 'unit': 'A'},
        {'name': 'IOP2', 'divide': 1, 'unit': 'A'},
        {'name': 'IOP3', 'divide': 1, 'unit': 'A'},
        {'name': 'IOP4', 'divide': 1, 'unit': 'A'},
        {'name': 'IOP5', 'divide': 1, 'unit': 'A'},
        {'name': 'IOP6', 'divide': 1, 'unit': 'A'},
        {'name': 'IOP7', 'divide': 1, 'unit': 'A'},
        {'name': 'VO', 'divide': 1, 'unit': 'V'},
    ],
    '승압기 모듈 1': [
        {'name': '상태정보상세', 'divide': 1, 'unit': ''},
        {'name': '경고정보 상세', 'divide': 1, 'unit': ''},
        {'name': '고장정보 상세', 'divide': 1, 'unit': ''},
        {'name': '입력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '입력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '온도', 'divide': 1, 'unit': '˚C'},
        {'name': '출력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '출력 전압', 'divide': 1, 'unit': 'V'},
        {'name': 'SW 버전', 'divide': 1, 'unit': ''},
    ],
    '승압기 모듈 2': [
        {'name': '상태정보상세', 'divide': 1, 'unit': ''},
        {'name': '경고정보 상세', 'divide': 1, 'unit': ''},
        {'name': '고장정보 상세', 'divide': 1, 'unit': ''},
        {'name': '입력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '입력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '온도', 'divide': 1, 'unit': '˚C'},
        {'name': '출력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '출력 전압', 'divide': 1, 'unit': 'V'},
        {'name': 'SW 버전', 'divide': 1, 'unit': ''},
    ],
    '승압기 모듈 3': [
        {'name': '상태정보상세', 'divide': 1, 'unit': ''},
        {'name': '경고정보 상세', 'divide': 1, 'unit': ''},
        {'name': '고장정보 상세', 'divide': 1, 'unit': ''},
        {'name': '입력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '입력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '온도', 'divide': 1, 'unit': '˚C'},
        {'name': '출력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '출력 전압', 'divide': 1, 'unit': 'V'},
        {'name': 'SW 버전', 'divide': 1, 'unit': ''},
    ],
    '승압기 모듈 4': [
        {'name': '상태정보상세', 'divide': 1, 'unit': ''},
        {'name': '경고정보 상세', 'divide': 1, 'unit': ''},
        {'name': '고장정보 상세', 'divide': 1, 'unit': ''},
        {'name': '입력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '입력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '온도', 'divide': 1, 'unit': '˚C'},
        {'name': '출력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '출력 전압', 'divide': 1, 'unit': 'V'},
        {'name': 'SW 버전', 'divide': 1, 'unit': ''},
    ],
    '승압기 모듈 5': [
        {'name': '상태정보상세', 'divide': 1, 'unit': ''},
        {'name': '경고정보 상세', 'divide': 1, 'unit': ''},
        {'name': '고장정보 상세', 'divide': 1, 'unit': ''},
        {'name': '입력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '입력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '온도', 'divide': 1, 'unit': '˚C'},
        {'name': '출력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '출력 전압', 'divide': 1, 'unit': 'V'},
        {'name': 'SW 버전', 'divide': 1, 'unit': ''},
    ],
    '승압기 모듈 6': [
        {'name': '상태정보상세', 'divide': 1, 'unit': ''},
        {'name': '경고정보 상세', 'divide': 1, 'unit': ''},
        {'name': '고장정보 상세', 'divide': 1, 'unit': ''},
        {'name': '입력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '입력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '온도', 'divide': 1, 'unit': '˚C'},
        {'name': '출력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '출력 전압', 'divide': 1, 'unit': 'V'},
        {'name': 'SW 버전', 'divide': 1, 'unit': ''},
    ],
    '차체전원조절기': [
        {'name': '상태정보상세', 'divide': 1, 'unit': ''},
        {'name': '경고정보상세', 'divide': 1, 'unit': ''},
        {'name': '고장정보상세', 'divide': 1, 'unit': ''},
        {'name': '입력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '입력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '출력 전류', 'divide': 1, 'unit': 'A'},
        {'name': '출력 전압', 'divide': 1, 'unit': 'V'},
        {'name': '온도', 'divide': 1, 'unit': '˚C'},
    ]
}

    