import struct

message = b'\x00\x00\x00\x00\x10@\xff\x10\x01\x01\x04\x00\x01\x00\x00\x00f\x01'
hex_string = ' '.join(f'{byte:02X}' for byte in message)
print(hex_string)

def analysis(message):
    hex_string = ' '.join(f'{byte:02X}' for byte in message)
    return hex_string


message = b'\x00\x00\x00\x00\x10@\xff\x10\x01\x01\x04\x00\x01\x00\x00\x00f\x01'
analysis(message)

## 내가 보낸 Boot Done Upd ACK 메시지 b'\x00\x00\x00\x00\x10@\xff\x10\x01\x01\x04\x00\x01\x00\x00\x00d\x01'


## 유차장이 보내준 c# Boot Done Upd ACK 메시지 
## 유광현 : 00 00 00 00 10 40 FF 10 01 01 04 00 01 00 00 00 66 01

## 윤남주 : 00 00 00 00 10 40 FF 10 01 01 04 00 01 00 00 00 64 01

1, TCC_TPC_PBIT_RESULT_REQ
2, TCC_TPC_CBIT_SET_UPD
3, TCC_TPC_CBIT_RESULT_REQ
4, TCC_TPC_IBIT_RUN_CMD
5, TCC_TPC_IBIT_RESULT_REQ
6, TCC_TPC_RESET_UPD
7, TCC_TPC_SOFT_EMERGENCY_STOP_UPD
8, TCC_TPC_CTRL_POWER_CMD
9, TCC_TPC_DRIVE_POWER_CMD
10, TCC_TPC_STATE_INFO_REQ



udpclient2: ### :: DATA RECEIVED :: PTYPE = 2 :: Time = 0 :: DID = 16, CODE = TPC_TCC_BOOT_DONE_UPD, ACK = 1, dataLength = 0, DATA = None :: b'\x00\x00\x00\x00@\x10\x01\x01\x01\x01\x00\x00T\x00'

udpclient2: SEND TO CLIENT :: ('192.168.100.130', 31000) :: TPC_TCC_BOOT_DONE_UPD :: PTYPE = 2 :: Time = 0 :: DID = 64, CODE = ACK_CODE, ACK = 16, dataLength = 4, DATA = ['TPC_TCC_BOOT_DONE_UPD', 0] :: b'\x00\x00\x00\x00\x10@\xff\x10\x01\x01\x04\x00\x01\x00\x00\x00f\x01'



k9a2      : run_pcs :: self.tccClient :: 1
udpclient2: SEND TO CLIENT :: ('192.168.100.130', 31000) :: TCC_TPC_PBIT_RESULT_REQ :: PTYPE = 1 :: Time = 2 :: DID = 64, CODE = TCC_TPC_PBIT_RESULT_REQ, ACK = 255, dataLength = 0, DATA = None :: b'\x02\x00\x00\x00\x10@\x10\xff\x01\x01\x00\x00a\x01'



udpclient2: ### :: DATA RECEIVED :: PTYPE = 2 :: Time = 0 :: DID = 16, CODE = TPC_TCC_BOOT_DONE_UPD, ACK = 1, dataLength = 0, DATA = None :: b'\x00\x00\x00\x00@\x10\x01\x01\x01\x01\x00\x00T\x00'

udpclient2: SEND TO CLIENT :: ('192.168.100.130', 31000) :: TPC_TCC_BOOT_DONE_UPD :: PTYPE = 2 :: Time = 0 :: DID = 64, CODE = ACK_CODE, ACK = 16, dataLength = 4, DATA = ['TPC_TCC_BOOT_DONE_UPD', 0] :: b'\x00\x00\x00\x00\x10@\xff\x10\x01\x01\x04\x00\x01\x00\x00\x00f\x01'



udpclient2: SEND TO CLIENT :: ('192.168.100.130', 31000) :: TCC_TPC_CBIT_SET_UPD :: PTYPE = 1 :: Time = 4 :: DID = 64, CODE = TCC_TPC_CBIT_SET_UPD, ACK = 1, dataLength = 2, DATA = [3000] :: b'\x04\x00\x00\x00\x10@\x11\x01\x01\x01\x02\x00\xb8\x0b)\x01'



udpclient2: ### :: DATA RECEIVED :: PTYPE = 2 :: Time = 4 :: DID = 16, CODE = ACK_CODE, ACK = 16, dataLength = 4, DATA = ['TPC_TCC_CBIT_SET_UPD', 0] :: b'\x04\x00\x00\x00@\x10\xff\x10\x01\x01\x04\x00\x11\x00\x00\x00z\x01'

udpclient2: ### :: DATA RECEIVED :: PTYPE = 2 :: Time = 0 :: DID = 16, CODE = TPC_TCC_BOOT_DONE_UPD, ACK = 1, dataLength = 0, DATA = None :: b'\x00\x00\x00\x00@\x10\x01\x01\x01\x01\x00\x00T\x00'

udpclient2: SEND TO CLIENT :: ('192.168.100.130', 31000) :: TPC_TCC_BOOT_DONE_UPD :: PTYPE = 2 :: Time = 0 :: DID = 64, CODE = ACK_CODE, ACK = 16, dataLength = 4, DATA = ['TPC_TCC_BOOT_DONE_UPD', 0] :: b'\x00\x00\x00\x00\x10@\xff\x10\x01\x01\x04\x00\x01\x00\x00\x00f\x01'



udpclient2: SEND TO CLIENT :: ('192.168.100.130', 31000) :: TCC_TPC_IBIT_RUN_CMD :: PTYPE = 1 :: Time = 6 :: DID = 64, CODE = TCC_TPC_IBIT_RUN_CMD, ACK = 1, dataLength = 2, DATA = [5000] :: b'\x06\x00\x00\x00\x10@\x13\x01\x01\x01\x02\x00\x88\x13\x03\x01'



udpclient2: ### :: DATA RECEIVED :: PTYPE = 2 :: Time = 6 :: DID = 16, CODE = ACK_CODE, ACK = 16, dataLength = 4, DATA = ['TPC_TCC_IBIT_RUN_CMD', 0] :: b'\x06\x00\x00\x00@\x10\xff\x10\x01\x01\x04\x00\x13\x00\x00\x00~\x01'

udpclient2: ### :: DATA RECEIVED :: PTYPE = 2 :: Time = 0 :: DID = 16, CODE = TPC_TCC_BOOT_DONE_UPD, ACK = 1, dataLength = 0, DATA = None :: b'\x00\x00\x00\x00@\x10\x01\x01\x01\x01\x00\x00T\x00'

udpclient2: SEND TO CLIENT :: ('192.168.100.130', 31000) :: TPC_TCC_BOOT_DONE_UPD :: PTYPE = 2 :: Time = 0 :: DID = 64, CODE = ACK_CODE, ACK = 16, dataLength = 4, DATA = ['TPC_TCC_BOOT_DONE_UPD', 0] :: b'\x00\x00\x00\x00\x10@\xff\x10\x01\x01\x04\x00\x01\x00\x00\x00f\x01'



udpclient2: ### :: DATA RECEIVED :: PTYPE = 2 :: Time = 0 :: DID = 16, CODE = TPC_TCC_BOOT_DONE_UPD, ACK = 1, dataLength = 0, DATA = None :: b'\x00\x00\x00\x00@\x10\x01\x01\x01\x01\x00\x00T\x00'

udpclient2: SEND TO CLIENT :: ('192.168.100.130', 31000) :: TPC_TCC_BOOT_DONE_UPD :: PTYPE = 2 :: Time = 0 :: DID = 64, CODE = ACK_CODE, ACK = 16, dataLength = 4, DATA = ['TPC_TCC_BOOT_DONE_UPD', 0] :: b'\x00\x00\x00\x00\x10@\xff\x10\x01\x01\x04\x00\x01\x00\x00\x00f\x01'



msg2      : SField :: setup_reset_device :: 1
k9a2      : setup_reset_device :: 1
udpclient2: SEND TO CLIENT :: ('192.168.100.130', 31000) :: TCC_TPC_RESET_UPD :: PTYPE = 1 :: Time = 9 :: DID = 64, CODE = TCC_TPC_RESET_UPD, ACK = 1, dataLength = 2, DATA = [1] :: b'\t\x00\x00\x00\x10@ \x01\x01\x01\x02\x00\x01\x00v\x00'



udpclient2: ### :: DATA RECEIVED :: PTYPE = 2 :: Time = 9 :: DID = 16, CODE = ACK_CODE, ACK = 16, dataLength = 4, DATA = ['TPC_TCC_RESET_UPD', 0] :: b'\t\x00\x00\x00@\x10\xff\x10\x01\x01\x04\x00 \x00\x00\x00\x8e\x01'

udpclient2: ### :: DATA RECEIVED :: PTYPE = 2 :: Time = 0 :: DID = 16, CODE = TPC_TCC_BOOT_DONE_UPD, ACK = 1, dataLength = 0, DATA = None :: b'\x00\x00\x00\x00@\x10\x01\x01\x01\x01\x00\x00T\x00'

udpclient2: SEND TO CLIENT :: ('192.168.100.130', 31000) :: TPC_TCC_BOOT_DONE_UPD :: PTYPE = 2 :: Time = 0 :: DID = 64, CODE = ACK_CODE, ACK = 16, dataLength = 4, DATA = ['TPC_TCC_BOOT_DONE_UPD', 0] :: b'\x00\x00\x00\x00\x10@\xff\x10\x01\x01\x04\x00\x01\x00\x00\x00f\x01'



k9a2      : control_onoff_cmd :: 1, 255
udpclient2: UDPClient :: control_onoff_cmd :: 1, 255
udpclient2: SEND TO CLIENT :: ('192.168.100.130', 31000) :: TCC_TPC_CTRL_POWER_CMD :: PTYPE = 1 :: Time = 11 :: DID = 64, CODE = TCC_TPC_CTRL_POWER_CMD, ACK = 1, dataLength = 4, DATA = [1, 255] :: b'\x0b\x00\x00\x00\x10@@\x01\x01\x01\x04\x00\x01\x00\xff\x00\x97\x01'



udpclient2: ### :: DATA RECEIVED :: PTYPE = 2 :: Time = 11 :: DID = 16, CODE = ACK_CODE, ACK = 16, dataLength = 4, DATA = ['TPC_TCC_CTRL_POWER_UPD', 0] :: b'\x0b\x00\x00\x00@\x10\xff\x10\x01\x01\x04\x00@\x00\x00\x00\xb0\x01'

udpclient2: ### :: DATA RECEIVED :: PTYPE = 2 :: Time = 0 :: DID = 16, CODE = TPC_TCC_BOOT_DONE_UPD, ACK = 1, dataLength = 0, DATA = None :: b'\x00\x00\x00\x00@\x10\x01\x01\x01\x01\x00\x00T\x00'

udpclient2: SEND TO CLIENT :: ('192.168.100.130', 31000) :: TPC_TCC_BOOT_DONE_UPD :: PTYPE = 2 :: Time = 0 :: DID = 64, CODE = ACK_CODE, ACK = 16, dataLength = 4, DATA = ['TPC_TCC_BOOT_DONE_UPD', 0] :: b'\x00\x00\x00\x00\x10@\xff\x10\x01\x01\x04\x00\x01\x00\x00\x00f\x01'



udpclient2: SEND TO CLIENT :: ('192.168.100.130', 31000) :: START :: TCC_TPC_CBIT_RESULT_REQ :: PTYPE = 1 :: Time = 13 :: DID = 64, CODE = TCC_TPC_CBIT_RESULT_REQ, ACK = 255, dataLength = 2, DATA = [255] :: b'\r\x00\x00\x00\x10@\x12\xff\x01\x01\x02\x00\xff\x00d\x02'



udpclient2: ### :: DATA RECEIVED :: PTYPE = 2 :: Time = 0 :: DID = 16, CODE = TPC_TCC_BOOT_DONE_UPD, ACK = 1, dataLength = 0, DATA = None :: b'\x00\x00\x00\x00@\x10\x01\x01\x01\x01\x00\x00T\x00'

udpclient2: SEND TO CLIENT :: ('192.168.100.130', 31000) :: TPC_TCC_BOOT_DONE_UPD :: PTYPE = 2 :: Time = 0 :: DID = 64, CODE = ACK_CODE, ACK = 16, dataLength = 4, DATA = ['TPC_TCC_BOOT_DONE_UPD', 0] :: b'\x00\x00\x00\x00\x10@\xff\x10\x01\x01\x04\x00\x01\x00\x00\x00f\x01'



import struct

message =  b'\x00\x00\x00\x00@\x10\x01\x01\x01\x01\x00\x00T\x00'

def analysis(message):
    timeStamp, sourceID, destinationID, code, ack, commCh, sendCount, dataLength = \
        struct.unpack("<iBBBBBBH", message[:12])
    pattern="<" + "H" * int(dataLength/2)
    data = list(struct.unpack(pattern, message[12:-2]))
    return f"timeStamp = {timeStamp}, sourceID = {sourceID}, destinationID = {destinationID}, code = {code}, ack = {ack}, commCh = {commCh}, sendCount = {sendCount}, dataLength = {dataLength}, data = {data}"

analysis(message)


pattern="<" + "H"*dataLength
data = list(struct.unpack(pattern, message[12:-2]))




##########3
timeStamp = 0
sourceID = 16
destinationID = 64
code = 0xFF
ack = 16
commCh = 1
sendCount = 1
dataLength = 4
data = [1, 0]

message = struct.pack('<iBBBBBB', timeStamp, sourceID, destinationID,
                        code, ack, commCh, sendCount)
dlen = (dataLength).to_bytes(2, byteorder='little')
message += dlen

if dataLength:
    for val in data:
        message += (val).to_bytes(2, byteorder='little')

csum = checksum()
csum = (csum).to_bytes(2, byteorder='little')
message += csum
logging.info(f"make_encode :: {self}")



# 2bit Check
def bit_check(x):
    data = []
    for idx in range(16):
        if (x & (1<<idx)):
            data.append(1)
        else:
            data.append(0)
    return data


bit_check(2903)
bit_check(2899)
