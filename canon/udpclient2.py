import socket
import struct
import threading
import logging
import numpy as np

from PyQt5.QtCore import pyqtSignal, pyqtSlot, QObject

from .mcode2 import *
from .msg2 import MsgProtocol2

class UDPClient(QObject):
    send_requestBox = pyqtSignal(str)
    send_responseBox = pyqtSignal(str)
    send_normalMsgBox = pyqtSignal(str)
    send_highMsgBox = pyqtSignal(str)

    def __init__(self, parent, ipAddress, port):
        super(UDPClient, self).__init__(parent)
        self.parent = parent
        self.__udpServer = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.__ipAddress = ipAddress
        self.__port = port
        self.__fromAddress = None
        self.__bootflag = False
        self.timeStamp = 0
        self.control_info = None
        self.power_info = None

        # Waiting Event
        self.event = threading.Event()
        self.starttime = None

        ## PBIT
        self.pbit_status = np.zeros((37, 16), dtype=np.int8)
        self.cbit_status = np.zeros((37, 16), dtype=np.int8)
        self.ibit_status = np.zeros((37, 16), dtype=np.int8)

        self.zeroarray = np.zeros(16, dtype=np.int8)

        self.pbit_flag = False
        self.pbit_received_flag = False

        ## STATUS
        self.tpc_status_info = None

        ## CBIT SETUP
        self.cbit_setup_flag = False

        # self.__udpServer.settimeout(5)
        logging.info(f"TCC HOST ::  {self.__ipAddress} :: {self.__port}")
        self.__udpServer.bind((self.__ipAddress, self.__port))
        logging.info("###################################################################")
        logging.info(f"###### SERVER BIND :: {self.__udpServer} :: ######")
        logging.info("###################################################################\n\n")

        self.__thread = threading.Thread(target=self.__listen, args=())
        self.__thread.start()

    def __listen(self):
        logging.info("@@@@@@. UDPClient :: Start Listening .....")
        self.__stoplistening = False
        self.__receivedata = bytearray()
        try:
            while not self.__stoplistening:
                if len(self.__receivedata) == 0:
                    self.__receivedata = bytearray()
                    self.__timeout = 500
                    if self.__udpServer is not None:
                        self.__receivedata, self.__fromAddress \
                                = self.__udpServer.recvfrom(BUFFERSIZE)

                        self.__athread = threading.Thread(target=self.__action_service, args=())
                        self.__athread.start()
        except socket.timeout:
            logging.info("__listen :: socket.timeout :: event")
            self.__receivedata = None

    def check_bootflag(self):
        return self.__bootflag

    def close_server(self):
        try:
            self.__receivedata = None
        except Exception as e:
            logging.info("subProcess close :: %s", e)
            # self.__udpServer.close()

    @pyqtSlot(str)
    def __action_service(self):
        message = self.__receivedata
        msg = MsgProtocol2(ptype=2)
        msg.make_protocol_header_from_msg(message)
        logging.info("      ** DATA RECEIVED :: %s\n", msg.code)
        
        self.send_responseBox.emit(msg.make_msgbox())
        ## self.__receivedata 를 비워야 새로운 데이터를 받을수 있음
        self.__receivedata = bytearray()

        ## FROM :: TPC_TCC_BOOT_DONE_UPD
        if msg.code == TPC_TCC_BOOT_DONE_UPD:
            if msg.ack == PACK:
                self.__bootflag = True
                ########################################################
                ## ## ACK 발송
                sendmsg = MsgProtocol2(2, msg.timeStamp, TCC_ID, TPC_ID, ACK_CODE, CACK, 1, 1, 4, [TPC_TCC_BOOT_DONE_UPD, 0])
                self.sendmsg_and_display(sendmsg, "ACK_CODE")
        
        ## FROM :: TPC_TCC_IBIT_DONE_UPD
        elif msg.code == TPC_TCC_IBIT_DONE_UPD:
            if msg.csum_flag:
                ########################################################
                ## ## ACK 발송
                sendmsg = MsgProtocol2(2, msg.timeStamp, TCC_ID, TPC_ID, ACK_CODE, CACK, 1, 1, 4, [TPC_TCC_IBIT_DONE_UPD, 0])
                self.sendmsg_and_display(sendmsg, "ACK_CODE")

        ## FROM :: TPC_TCC_PBIT_RESULT_REF
        elif msg.code == TPC_TCC_PBIT_RESULT_REF:
            if msg.csum_flag and msg.data:
                self.make_data_bit_result(msg.data, self.pbit_status)
                self.parent.update_Pbit_result()

        ## FROM :: TPC_TCC_CBIT_RESULT_REF
        elif msg.code == TPC_TCC_CBIT_RESULT_REF:
            if msg.csum_flag and msg.data:
                self.make_data_bit_result(msg.data, self.cbit_status)
                self.parent.update_Cbit_result()

        ## FROM :: TPC_TCC_IBIT_RESULT_REF
        elif msg.code == TPC_TCC_IBIT_RESULT_REF:
            if msg.csum_flag and msg.data:
                self.make_data_bit_result(msg.data, self.ibit_status)
                self.parent.update_Ibit_result()

        ## FROM :: TPC_TCC_HARD_EMERGENCY_STOP_UPD
        elif msg.code == TPC_TCC_HARD_EMERGENCY_STOP_UPD:
            ########################################################
            ## ## ACK 발송
            sendmsg = MsgProtocol2(2, msg.timeStamp, TCC_ID, TPC_ID, ACK_CODE, CACK, 1, 1, 4, [TPC_TCC_HARD_EMERGENCY_STOP_UPD, 0])
            self.sendmsg_and_display(sendmsg, "ACK_CODE")
            # ========================================================
            self.parent.update_hard_emergency_stop(msg.data)

        ## FROM :: TPC_TCC_CTRL_POWER_UPD
        elif msg.code == TPC_TCC_CTRL_POWER_UPD:
            ########################################################
            ## ## ACK 발송
            sendmsg = MsgProtocol2(2, msg.timeStamp, TCC_ID, TPC_ID, ACK_CODE, CACK, 1, 1, 4, [TPC_TCC_CTRL_POWER_UPD, 0])
            self.sendmsg_and_display(sendmsg, "ACK_CODE")
            # ========================================================
            self.parent.update_control_onoff_status(msg.data)

        ## FROM :: TPC_TCC_CBIT_SET_UPD
        elif msg.code == TPC_TCC_CBIT_SET_UPD:
            if msg.csum_flag:
                if msg.ack == PACK:
                    self.cbit_setup_flag = True

        ## FROM :: TPC_TCC_STATE_INFO_REF
        elif msg.code == TPC_TCC_STATE_INFO_REF:
            logging.info("PCS DATA %s", msg.data)
            # if msg.csum_flag:
                ## ACK 체크 ACK에 대한 RESPONSE 인지?
            self.tpc_status_info = msg.data
            self.parent.update_pcs_stateinfo()

        ## FROM :: TPC_TCC_DRIVE_POWER_UPD
        elif msg.code == TPC_TCC_DRIVE_POWER_UPD:
            ########################################################
            ## ## ACK 발송
            sendmsg = MsgProtocol2(2, msg.timeStamp, TCC_ID, TPC_ID, ACK_CODE, CACK, 1, 1, 4, [TPC_TCC_DRIVE_POWER_UPD, 0])
            self.sendmsg_and_display(sendmsg, "ACK_CODE")
            # ========================================================
            self.parent.update_upgrade_onoff_status(msg.data)

        elif msg.code == ACK_CODE:
            if msg.data:
                if msg.data[0] == TCC_TPC_PBIT_RESULT_REQ:
                    self.pbit_received_flag = True
                elif msg.data[0] == TCC_TPC_CBIT_RESULT_REQ:
                    self.cbit_setup_flag = True
                elif msg.data[0] == TCC_TPC_IBIT_RESULT_REQ:
                    self.ibit_setup_flag = True
            # logging.info("ACK RESPONSE DATA :: %s", msg)

    #################################################################################
    #################################################################################
    # TCC_TPC_PBIT_RESULT_REQ
    @pyqtSlot()
    def request_Pbit_status(self):
        self.pbit_flag = True
        self.timeStamp += 1
        sendmsg = MsgProtocol2(1, self.timeStamp, TCC_ID, TPC_ID, TCC_TPC_PBIT_RESULT_REQ, PNACK, 1, 1, 0)
        self.sendmsg_and_display(sendmsg, "TCC_TPC_PBIT_RESULT_REQ")

    ## CMD : TCC_TPC_CBIT_SET_UPD
    @pyqtSlot()
    def setup_cbitperiod(self, period):
        self.cbit_setup_flag = False
        self.timeStamp += 1
        dataLength = 2
        msg = MsgProtocol2(1, self.timeStamp, TCC_ID, TPC_ID, TCC_TPC_CBIT_SET_UPD, PACK, 1, 1, dataLength, period)
        self.sendmsg_and_display(msg, "TCC_TPC_CBIT_SET_UPD")
        
    ## CMD : TCC_TPC_CBIT_RESULT_REQ
    @pyqtSlot()
    def request_Cbit_status(self, data):
        self.timeStamp += 1
        dataLength = 2
        sendmsg = MsgProtocol2(1, self.timeStamp, TCC_ID, TPC_ID, TCC_TPC_CBIT_RESULT_REQ, PACK, 1, 1, dataLength, data)
        self.sendmsg_and_display(sendmsg, "TCC_TPC_CBIT_RESULT_REQ")

    ## CMD : TCC_TPC_IBIT_RUN_CMD
    @pyqtSlot()
    def setup_ibitperiod(self, period):
        self.ibit_setup_flag = False
        self.timeStamp += 1
        dataLength = 2
        sendmsg = MsgProtocol2(1, self.timeStamp, TCC_ID, TPC_ID, TCC_TPC_IBIT_RUN_CMD, PACK, 1, 1, dataLength, period)
        self.sendmsg_and_display(sendmsg, "TCC_TPC_IBIT_RUN_CMD")
        
    ## CMD : TCC_TPC_IBIT_RESULT_REQ
    @pyqtSlot()
    def request_Ibit_status(self):
        self.timeStamp += 1
        sendmsg = MsgProtocol2(1, self.timeStamp, TCC_ID, TPC_ID, TCC_TPC_IBIT_RESULT_REQ, PACK, 1, 1, 0)
        self.sendmsg_and_display(sendmsg, "TCC_TPC_IBIT_RESULT_REQ")
    
    ## CMD :: TCC_TPC_RESET_UPD
    @pyqtSlot()
    def setup_reset_device(self, data):
        self.timeStamp += 1
        dataLength = 2
        sendmsg = MsgProtocol2(1, self.timeStamp, TCC_ID, TPC_ID, TCC_TPC_RESET_UPD, PACK, 1, 1, dataLength, data)
        self.sendmsg_and_display(sendmsg, "TCC_TPC_RESET_UPD")

    ## CMD :: TCC_TPC_SOFT_EMERGENCY_STOP_UPD
    @pyqtSlot()
    def setup_stop_device(self, data):
        self.timeStamp += 1
        dataLength = 2
        sendmsg = MsgProtocol2(1, self.timeStamp, TCC_ID, TPC_ID, TCC_TPC_SOFT_EMERGENCY_STOP_UPD, PACK, 1, 1, dataLength, data)
        self.sendmsg_and_display(sendmsg, "TCC_TPC_SOFT_EMERGENCY_STOP_UPD")
     
    ## CMD :: TCC_TPC_CTRL_POWER_CMD
    def control_onoff_cmd(self, onoff, part):
        logging.info(f"UDPClient :: control_onoff_cmd :: {onoff}, {part}")
        self.timeStamp += 1
        dataLength = 4
        sendmsg = MsgProtocol2(1, self.timeStamp, TCC_ID, TPC_ID, TCC_TPC_CTRL_POWER_CMD, PACK, 1, 1, dataLength, [onoff, part])
        self.sendmsg_and_display(sendmsg, "TCC_TPC_CTRL_POWER_CMD")
    
    ## CMD :: TCC_TPC_DRIVE_POWER_CMD
    def upgrade_onoff_cmd(self, onoff, part):
        logging.info(f"UDPClient :: upgrade_onoff_cmd :: {onoff}, {part}")
        self.timeStamp += 1
        dataLength = 4
        sendmsg = MsgProtocol2(1, self.timeStamp, TCC_ID, TPC_ID, TCC_TPC_DRIVE_POWER_CMD, PACK, 1, 1, dataLength, [onoff, part])
        self.sendmsg_and_display(sendmsg, "TCC_TPC_DRIVE_POWER_CMD")
     
    # CMD :: TCC_TPC_STATE_INFO_REQ
    @pyqtSlot()
    def request_tpc_status_info(self):
        self.timeStamp += 1
        sendmsg = MsgProtocol2(1, self.timeStamp, TCC_ID, TPC_ID, TCC_TPC_STATE_INFO_REQ, PNACK, 1, 1, 0)
        self.sendmsg_and_display(sendmsg, "TCC_TPC_STATE_INFO_REQ")

    #################################################################################
    #################################################################################
    def close(self):
        """
        Closes Serial port, or TCP-Socket connection
        """
        if self.__udpServer is not None:
            self.__stoplistening = True
            self.__udpServer.shutdown(socket.SHUT_RDWR)
            self.__udpServer.close()
        self.__connected = False

    # Send Data
    def send(self, data, address):
        try:
            self.__udpServer.sendto(data, address)
        except Exception as e:
            pass
        
    # 2bit Check
    def bit_check(self, x):
        data = []
        for idx in range(16):
            if (x & (1<<idx)):
                data.append(1)
            else:
                data.append(0)
        return data

    def make_data_bit_result(self, data, bit_status):
        for index, val in enumerate(data):
            if val:
                bits = self.bit_check(val)
                bit_status[index] = bits
            else:
                bit_status[index] = self.zeroarray

    ## COMMON 
    @pyqtSlot()
    def sendmsg_and_display(self, msg, ptype):
        msg.make_encode()
        self.send(msg.message, self.__fromAddress)
        # if ptype != ACK_CODE:
        self.send_requestBox.emit(msg.make_msgbox())
        # logging.info("## REQUEST :: %s\n\n", msg)
