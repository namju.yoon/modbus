import struct
import random
import logging

from pymodbus.payload import BinaryPayloadBuilder
from pymodbus.constants import Endian


from PyQt5.QtWidgets import QLabel, QPushButton, QCheckBox, QSizePolicy, QRadioButton, QButtonGroup
from PyQt5.QtWidgets import QGroupBox, QVBoxLayout, QGridLayout, QLineEdit, QHBoxLayout
from PyQt5.QtCore import QObject

from .mcode2 import *

# import struct
# message = b'P\x01 \x10\x00\x00\x81\x00'
# sourceID, destinationID, code, ack, dataLength = \
#             struct.unpack("<BBBBH", message[:6])
# sourceID, destinationID, code, ack, dataLength
# pattern="<" + "H"*int(dataLength/2)
# pattern="<" + "B"*dataLength
# data = list(struct.unpack(pattern, message[6:-2]))
# csum = struct.unpack("<H", message[-2:])[0]

class MsgProtocol2(QObject):
    """ Implement Protocol Header
    The request Header with function code 06 must be 5 bytes:
        ================    ===============
        Field               Length (bytes)
        ================    ===============
        HEADER              12
            TimeStamp           4 # NEW
            Source ID           1
            Destination ID      1
            CODE                1     
            ACK 필요 여부         1 ## 0xFF 미요청, 0x01 요청
            COMM CH# INFO       1 ## 0x01 Ch1, 0x02 Ch2
            SEND COUNT          1 ## 1 ~ 4
            DATA LENGTH         2
        DATA                N        
        CHECKSUM            2
        ================    ===============
    """
    def __init__(self, ptype=1, timeStamp=0, sourceID=None, destinationID=None, 
                code=None, ack=None, commCh=1, sendCount=1, dataLength=0, data=None):
        super(MsgProtocol2, self).__init__()
        # logging.info(f"MsgProtocol2({ptype}, {sourceID}, {destinationID}, {code})")
        self.timeStamp = timeStamp ##
        self.sourceID = sourceID
        self.destinationID = destinationID
        self.code = code
        self.ack = ack
        self.commCh = commCh
        self.sendCount = sendCount
        self.dataLength = dataLength
        self.data = data
        self.csum_flag = False
        self.ptype = ptype ## ptype == 1 : TCC, ptype == 2: TPC
        self.message = bytearray()
        self.csum = self.checksum()

    def checksum(self):
        try:
            headsum = self.timeStamp + self.sourceID + self.destinationID + self.code + self.ack + self.dataLength + self.commCh + self.sendCount
            if self.dataLength:
                for dt in self.data:
                    bytes_val = (dt).to_bytes(2, byteorder='little')
                    temp_val = list(bytes_val)
                    headsum += sum(temp_val)
            return headsum
        except Exception as e:
            return None

    def make_msgbox(self):
        return f"{self.sourceID}:{self.destinationID}:{self.code}:{self.ack}:{self.dataLength}:{self.data}:{self.csum}"
    
    def make_encode(self):
        self.message = struct.pack('<iBBBBBB', self.timeStamp, self.sourceID, self.destinationID,
                                self.code, self.ack, self.commCh, self.sendCount)
        dlen = (self.dataLength).to_bytes(2, byteorder='little')
        self.message += dlen

        if self.dataLength:
            for val in self.data:
                self.message += (val).to_bytes(2, byteorder='little')

        self.csum = self.checksum()
        csum = (self.csum).to_bytes(2, byteorder='little')
        self.message += csum
        # logging.info(f"make_encode :: {self.csum} :: {list(self.message)}")

    def make_protocol_header_from_msg(self, message):
        ## data가 있으면.
        self.message = message
        try:
            self.timeStamp, self.sourceID, self.destinationID, self.code, \
            self.ack, self.commCh, self.sendCount, self.dataLength = \
                struct.unpack("<iBBBBBBH", message[:12])

            if self.dataLength:
                # if self.code in [TPC_TCC_PBIT_RESULT_REF, TPC_TCC_CBIT_RESULT_REF, TPC_TCC_IBIT_RESULT_REF]:
                #     pattern="<" + "H" * self.dataLength
                #     self.data = list(struct.unpack(pattern, message[12:-2]))
                # else:
                pattern="<" + "H" * int(self.dataLength/2)
                self.data = list(struct.unpack(pattern, message[12:-2]))

            self.csum = struct.unpack("<H", message[-2:])[0]
            self.csum_flag = True if self.csum == self.checksum() else False

        except Exception as e:
            logging.debug("make_general_from_message Error :: %s", message)

    def get_checksum(self):
        return self.csum_flag

    def __str__(self):
        if self.ptype == 1:
            pcode = TCC_CODE2.get(str(self.code), 'NOT REGISTED OR RESPONSE')
        else:
            pcode = TPC_CODE2.get(str(self.code), 'NOT REGISTED OR RESPONSE')

        if self.data:
            pdata = []
            if self.ptype == 1:
                pdata.append(TCC_CODE2.get(str(self.data[0]), self.data[0]))
                pdata.append(0)
            else:
                pdata.append(TPC_CODE2.get(str(self.data[0]), self.data[0]))
                pdata.append(0)
        else:
            pdata = []
        return f"Time = {self.timeStamp} :: DID = {self.destinationID}, CODE = {pcode}, ACK = {self.ack}, DLEN = {self.dataLength}, DATA = {self.data}"
        ## , RAW = {' '.join(f'{byte:02X}' for byte in self.message)}

def QLed(name, color):
    btn = QPushButton(name)
    style = f"border: 3px solid lightgray;border-radius: 10px;background-color: {color};color: black;font-size: 14px;font-weight: bold;"
    btn.setStyleSheet(style)
    btn.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
    return btn

def PLed(name, width, height, color):
    half = int(width/2)
    btn = QPushButton(name)
    btn.setFixedSize(width, height)
    style = f"border: 1px solid lightgray;border-radius: {half}px;background-color: {color};color: black;font-size: 14px;font-weight: bold;"
    btn.setStyleSheet(style)
    return btn


class Status:
    grayfont = "color: #222;"
    bluefont = "color: #0000ff;"
    yellowfont = "color: #FFC300;font-weight: bold;"
    redfont = "color: #ff0000;font-weight: bold;"

    def __init__(self, name, st1, st2, st3=None, st4=None):
        self.name = name
        self.cb_st1 = QCheckBox(st1)
        self.cb_st1.setStyleSheet(Status.grayfont)
        self.cb_st1.setChecked(True)

        self.cb_st2 = QCheckBox(st2)
        self.cb_st2.setStyleSheet(Status.grayfont)

        if st3:
            self.cb_st3 = QCheckBox(st3)
            self.cb_st3.setStyleSheet(Status.grayfont)
        else:
            self.cb_st3 = None

        if st4:
            self.cb_st4 = QCheckBox(st4)
            self.cb_st4.setStyleSheet(Status.grayfont)
        else:
            self.cb_st4 = None

    def display(self):
        group = QGroupBox(self.name)
        layout = QVBoxLayout()
        group.setLayout(layout)

        layout.addWidget(self.cb_st1)
        layout.addWidget(self.cb_st2)
        if self.cb_st3:
            layout.addWidget(self.cb_st3)
        if self.cb_st4:
            layout.addWidget(self.cb_st4)
        return group

    def display2row(self):
        group = QGroupBox(self.name)
        layout = QGridLayout()
        group.setLayout(layout)

        layout.addWidget(self.cb_st1, 0, 0)
        layout.addWidget(self.cb_st2, 0, 1)
        if self.cb_st3:
            layout.addWidget(self.cb_st3, 1, 0)
        if self.cb_st4:
            layout.addWidget(self.cb_st4, 1, 1)
        return group

    def change_status(self, val):
        try:
            if val == 0:
                self.cb_st1.setChecked(True)
                self.cb_st1.setStyleSheet(Status.grayfont)

                self.cb_st2.setChecked(False)
                self.cb_st2.setStyleSheet(Status.grayfont)

                if self.cb_st3:
                    self.cb_st3.setChecked(False)
                    self.cb_st3.setStyleSheet(Status.grayfont)

                if self.cb_st4:
                    self.cb_st4.setChecked(False)
                    self.cb_st4.setStyleSheet(Status.grayfont)

            elif val == 1:
                self.cb_st1.setChecked(False)
                self.cb_st1.setStyleSheet(Status.grayfont)

                self.cb_st2.setChecked(True)
                self.cb_st2.setStyleSheet(Status.bluefont)

                if self.cb_st3:
                    self.cb_st3.setChecked(False)
                    self.cb_st3.setStyleSheet(Status.grayfont)

                if self.cb_st4:
                    self.cb_st4.setChecked(False)
                    self.cb_st4.setStyleSheet(Status.grayfont)

            elif val == 2:
                self.cb_st1.setChecked(False)
                self.cb_st1.setStyleSheet(Status.grayfont)

                self.cb_st2.setChecked(False)
                self.cb_st2.setStyleSheet(Status.grayfont)

                self.cb_st3.setChecked(True)
                self.cb_st3.setStyleSheet(Status.redfont)

                if self.cb_st4:
                    self.cb_st4.setChecked(False)
                    self.cb_st4.setStyleSheet(Status.grayfont)

            elif val == 3:
                self.cb_st1.setChecked(False)
                self.cb_st1.setStyleSheet(Status.grayfont)

                self.cb_st2.setChecked(False)
                self.cb_st2.setStyleSheet(Status.grayfont)

                self.cb_st3.setChecked(False)
                self.cb_st3.setStyleSheet(Status.grayfont)

                self.cb_st4.setChecked(True)
                self.cb_st4.setStyleSheet(Status.yellowfont)
        except Exception as e:
            logging.debug(e)

    def __str__(self):
        return f"Status :: {self.name}"


class OneStatus:
    grayfont = "color: #222;"
    bluefont = "color: #0000ff;"
    yellowfont = "color: #FFC300;font-weight: bold;"
    redfont = "color: #ff0000;font-weight: bold;"

    def __init__(self, mod, name):
        self.mod = mod
        self.name = name
        self.lbl_name = QLabel(self.name)
        self.cb_st1 = QCheckBox(name)
        self.cb_st1.setStyleSheet(OneStatus.grayfont)

    def display(self):
        group = QGroupBox(self.name)
        layout = QVBoxLayout()
        group.setLayout(layout)

        layout.addWidget(self.cb_st1)
        return group

    def change_normal(self):
        try:
            self.cb_st1.setChecked(False)
            self.cb_st1.setStyleSheet(OneStatus.grayfont)
        except Exception as e:
            logging.debug(e)
    
    def change_warning(self):
        try:
            self.cb_st1.setChecked(True)
            self.cb_st1.setStyleSheet(OneStatus.bluefont)
        except Exception as e:
            logging.debug(e)

    def __str__(self):
        return f"Status :: {self.name}"


class Field:
    grayfont = "color: #222;"
    bluefont = "color: #0000ff;"
    yellowfont = "color: #FFC300;font-weight: bold;"
    redfont = "color: #ff0000;font-weight: bold;"

    def __init__(self, mod, name, value, op1="정상", op2="경고"):
        self.mod = mod
        self.name = name 
        self.value = "NO"
        try:
            self.lbl_name = QLabel(self.name)
            self.rd_normal = QCheckBox(op1)
            self.rd_normal.setChecked(True)
            self.rd_normal.setStyleSheet(Field.bluefont)
            
            self.rd_warning = QCheckBox(op2)
        except Exception as e:
            logging.debug("Field ERROR ::", mod, name, value, e)

    def set_value(self, value):
        self.value = "NO"

    def change_normal(self):
        if self.value == "FF":
            # logging.info(f"     ####    Field :: change_normal :: {self} :: {self.value}")
            self.value = "NO"
            self.rd_normal.setChecked(True)
            self.rd_normal.setStyleSheet(Field.bluefont)

            self.rd_warning.setChecked(False)
            self.rd_warning.setStyleSheet(Field.grayfont)
            self.mod.update_normal()

    def change_warning(self):
        if self.value == "NO":
            logging.info(f"Field :: change_warning :: {self} :: {self.value}")
            self.value = "FF"
            self.rd_normal.setChecked(False)
            self.rd_normal.setStyleSheet(Field.grayfont)

            self.rd_warning.setChecked(True)
            self.rd_warning.setStyleSheet(Field.yellowfont)
            self.mod.update_warning()

    def __str__(self):
        return f"## :: Field :: ({self.mod}::{self.name}"


## 선택하고 값 읽기
class SField:
    grayfont = "color: #222;"
    bluefont = "color: #0000ff;font-weight: bold;"
    yellowfont = "color: #FFC300;font-weight: bold;"
    redfont = "color: #ff0000;font-weight: bold;"
    cyanfont = "border: 1px solid lightgray;border-radius: 25px;background-color: #ccffff;color: black;font-size: 14px;font-weight: bold;"

    def __init__(self, parent, name, st1, val1, st2, val2):
        self.parent = parent
        self.name = name
        self.value = None
        self.st1 = st1
        self.st2 = st2
        self.val1 = val1
        self.val2 = val2

        self.stradio1 = QRadioButton(st1)
        self.stradio1.clicked.connect(lambda:self.radioSelectClicked(self.stradio1))
        self.stradio2 = QRadioButton(st2)
        self.stradio2.clicked.connect(lambda:self.radioSelectClicked(self.stradio2))

        self.submit = PLed("ON/OFF", 50, 50, "#ccc")
        # self.submit.setEnabled(False)
        self.submit.clicked.connect(self.set_function)

    def set_enable(self):
        self.submit.setEnabled(True)
        self.submit.setStyleSheet(SField.cyanfont)

    def set_value(self, value):
        self.value = value

    def radioSelectClicked(self, btn):
        result = btn.text()
        if result == self.st1:
            self.value = self.val1
            self.stradio1.setStyleSheet(Field.bluefont)
            self.stradio2.setStyleSheet(Field.grayfont)
        elif result == self.st2:
            self.value = self.val2 
            self.stradio1.setStyleSheet(Field.grayfont)
            self.stradio2.setStyleSheet(Field.bluefont)
        else:
            self.value = None 

    def display(self):
        group = QGroupBox(self.name)
        layout = QGridLayout()
        group.setLayout(layout)

        layout.addWidget(self.stradio1, 0, 0)
        layout.addWidget(self.stradio2, 0, 1)
        layout.addWidget(self.submit, 0, 2)

        return group

    def get_value(self):
        return self.value
    
    def set_function(self):
        if self.name == 'RESET':
            val = self.get_value()
            # logging.info(f"SField :: setup_reset_device :: {val}")
            self.parent.setup_reset_device(val)
        elif self.name == 'STOP':
            val = self.get_value()
            # logging.info(f"SField :: setup_stop_device :: {val}")
            self.parent.setup_stop_device(val)
        else:
            logging.info(f"SField :: setup_stop_device :: else")


class Module:
    cyanfont = "border: 3px solid lightgray;border-radius: 10px;background-color:#ccffff;color: black;font-size: 14px;font-weight: bold;"
    yellowfont = "border: 3px solid lightgray;border-radius: 10px;background-color: #FFC300;color: black;font-size: 14px;font-weight: bold;"
    redfont = "border: 3px solid lightgray;border-radius: 10px;background-color: #FF0000;color: black;font-size: 14px;font-weight: bold;"

    def __init__(self, name):
        self.name = name 
        self.fields = []
        self.length = 0
        self.value = NORMAL
        self.btn = QLed(name, "#ccffff")

    def update_normal(self):
        self.value = NORMAL
        self.btn.setStyleSheet(Module.cyanfont)

    def update_warning(self):
        self.value = WARNING
        self.btn.setStyleSheet(Module.yellowfont)

    def update_fail(self):
        self.value = FAIL
        self.btn.setStyleSheet(Module.redfont)

    def add_field(self, field):
        self.fields.append(field)
        self.length += 1

    def display(self):
        group = QGroupBox(self.name)
        layout = QGridLayout()
        group.setLayout(layout)

        for idx, field in enumerate(self.fields):
            try:
                if isinstance(field, Field):
                    layout.addWidget(field.lbl_name, idx, 0)
                    layout.addWidget(field.rd_normal, idx, 1)
                    layout.addWidget(field.rd_warning, idx, 2)
                else:
                    layout.addWidget(field.lbl_name, idx, 0)
                    layout.addWidget(field.cb_st1, idx, 1, 1, 2)
            except Exception as e:
                logging.info(f"Module :: display :: {field} :: {e}")
            
        return group
    
    def displayField(self):
        group = QGroupBox(self.name)
        layout = QGridLayout()
        group.setLayout(layout)

        for idx, field in enumerate(self.fields):
            layout.addWidget(field.lbl_name, idx, 0)
            layout.addWidget(field.edit_value, idx, 1)

        return group
    
    def update_mod(self, data):
        for index, val in enumerate(data):
            if index < self.length and val:
                try:
                    self.fields[index].change_warning()
                except Exception as e:
                    pass
                    # logging.info(f"update_mod :; {e}")
            elif index < self.length:
                try:
                    self.fields[index].change_normal()
                except Exception as e:
                    pass
            else:
                # logging.info(f"update_mod :: BREAK :: {self} :: {index} :: {self.length} :: {data}")
                break

    def __str__(self):
        return f"#### Module Name = {self.name}"


class SubSystem:
    def __init__(self, name):
        self.name = name 
        self.modules = []

        self.btn = QLed(name, "#ccff66")
        
    def add_module(self, module):
        self.modules.append(module)

    def display(self):
        group = QGroupBox(self.name)
        # group.setStyleSheet("margin: 0.2em;")
        layout = QVBoxLayout()
        group.setLayout(layout)

        for idx, module in enumerate(self.modules):
            layout.addWidget(module.display())

        return group
    
    def displayField(self):
        group = QGroupBox(self.name)
        layout = QHBoxLayout()
        group.setLayout(layout)

        for idx, module in enumerate(self.modules):
            layout.addWidget(module.displayField())

        return group

    def __str__(self):
        return f"### SubSystem Name = {self.name}"


class PField:
    def __init__(self, name, divide=1, unit=""):
        self.name = name 
        self.divide = divide
        self.value = 0

        self.lbl_name = QLabel(self.name)
        self.edit_value = QLineEdit("0")
        self.unit = QLabel(unit)

    def set_value(self, val):
        self.value = int(val/self.divide)
        self.edit_value.setText(str(self.value))
        if val:
            self.edit_value.setStyleSheet("background-color: #ccffff;color: black;font-weight: bold;")

    def change_status(self, val):
        if val:
            self.edit_value.setStyleSheet("background-color: #FFC300;color: black;font-weight: bold;")
            self.edit_value.setText(str(val))
        else:
            self.edit_value.setStyleSheet("background-color: #ff0000;color: black;font-weight: bold;")
            self.edit_value.setText("0")

    def __str__(self):
        return f"## ({self.mod}, {self.name})"


class PModule:
    def __init__(self, name):
        self.name = name 
        self.fields = []

    def display(self):
        group = QGroupBox(self.name)
        layout = QGridLayout()
        group.setLayout(layout)

        for idx, field in enumerate(self.fields):
            layout.addWidget(field.lbl_name, idx, 0)
            layout.addWidget(field.edit_value, idx, 1)
            layout.addWidget(field.unit, idx, 2)

        return group

    def __str__(self):
        return self.name


class SModule:
    def __init__(self, name):
        self.name = name 
        self.fields = []

    def display(self):
        group = QGroupBox(self.name)
        layout = QGridLayout()
        group.setLayout(layout)

        for idx, status in enumerate(self.fields):
            lbl_name = QLabel(status.name)
            # logging.info(f"SModule :: display :: {status.name}")
            layout.addWidget(lbl_name, idx, 0)
            if isinstance(status, Status):
                layout.addWidget(status.cb_st1, idx, 1)
                layout.addWidget(status.cb_st2, idx, 2)
            else:
                layout.addWidget(status.edit_value, idx, 1, 1, 2)

        return group

    def __str__(self):
        return self.name


class OnOffStatus:
    grayfont = "color: #ccc;"
    bluefont = "color: #0000ff;"

    def __init__(self, name, st1, st2, st3, st4):
        self.name = name
        self.value = None
        self.bt_group = QButtonGroup()

        self.cb_st1 = QRadioButton(st1)
        self.cb_st2 = QRadioButton(st2)
        self.cb_st3 = QRadioButton(st3)
        self.cb_st4 = QRadioButton(st4)

        self.bt_group.addButton(self.cb_st1)
        self.bt_group.addButton(self.cb_st2)
        self.bt_group.addButton(self.cb_st3)
        self.bt_group.addButton(self.cb_st4)

    def display(self):
        group = QGroupBox(self.name)
        layout = QVBoxLayout()
        group.setLayout(layout)

        layout.addWidget(self.cb_st1)
        layout.addWidget(self.cb_st2)
        layout.addWidget(self.cb_st3)
        layout.addWidget(self.cb_st4)

        return group
    
    def set_value(self, val):
        if val == 1:
            self.cb_st1.setChecked(True)
            self.cb_st1.setStyleSheet(Status.bluefont)
            self.cb_st2.setStyleSheet(Status.grayfont)
            self.cb_st3.setStyleSheet(Status.grayfont)
            self.cb_st4.setStyleSheet(Status.grayfont)
        elif val == 2:
            self.cb_st1.setStyleSheet(Status.grayfont)
            self.cb_st2.setChecked(True)
            self.cb_st2.setStyleSheet(Status.bluefont)
            self.cb_st3.setStyleSheet(Status.grayfont)
            self.cb_st4.setStyleSheet(Status.grayfont)
        elif val == 3:
            self.cb_st1.setStyleSheet(Status.grayfont)
            self.cb_st2.setStyleSheet(Status.grayfont)
            self.cb_st3.setChecked(True)
            self.cb_st3.setStyleSheet(Status.bluefont)
            self.cb_st4.setStyleSheet(Status.grayfont)
        elif val == 4:
            self.cb_st1.setStyleSheet(Status.grayfont)
            self.cb_st2.setStyleSheet(Status.grayfont)
            self.cb_st3.setStyleSheet(Status.grayfont)
            self.cb_st4.setChecked(True)
            self.cb_st4.setStyleSheet(Status.bluefont)


class OnOffPartStatus:
    grayfont = "color: #ccc;"
    bluefont = "color: #0000ff;"

    def __init__(self, name, st1, st2, st3, st4, st5):
        self.name = name
        self.value = None
        self.cb_st1 = QCheckBox(st1)
        self.cb_st2 = QCheckBox(st2)
        self.cb_st3 = QCheckBox(st3)
        self.cb_st4 = QCheckBox(st4)
        self.cb_st5 = QCheckBox(st5)

    def display(self):
        group = QGroupBox(self.name)
        layout = QVBoxLayout()
        group.setLayout(layout)

        layout.addWidget(self.cb_st1)
        layout.addWidget(self.cb_st2)
        layout.addWidget(self.cb_st3)
        layout.addWidget(self.cb_st4)
        layout.addWidget(self.cb_st5)

        return group

    def set_value(self, val):
        if val == 1:
            self.cb_st1.setChecked(True)
            self.cb_st1.setStyleSheet(Status.bluefont)

            self.cb_st2.setChecked(False)
            self.cb_st3.setChecked(False)
            self.cb_st4.setChecked(False)
            self.cb_st5.setChecked(False)

            self.cb_st2.setStyleSheet(Status.grayfont)
            self.cb_st3.setStyleSheet(Status.grayfont)
            self.cb_st4.setStyleSheet(Status.grayfont)
            self.cb_st5.setStyleSheet(Status.grayfont)
        elif val == 2:
            self.cb_st1.setStyleSheet(Status.grayfont)
            self.cb_st2.setChecked(True)
            self.cb_st2.setStyleSheet(Status.bluefont)
            self.cb_st3.setStyleSheet(Status.grayfont)
            self.cb_st4.setStyleSheet(Status.grayfont)
            self.cb_st5.setStyleSheet(Status.grayfont)

            self.cb_st1.setChecked(False)
            self.cb_st3.setChecked(False)
            self.cb_st4.setChecked(False)
            self.cb_st5.setChecked(False)

        elif val == 3:
            self.cb_st1.setStyleSheet(Status.grayfont)
            self.cb_st2.setStyleSheet(Status.grayfont)
            self.cb_st3.setChecked(True)
            self.cb_st3.setStyleSheet(Status.bluefont)
            self.cb_st4.setStyleSheet(Status.grayfont)
            self.cb_st5.setStyleSheet(Status.grayfont)

            self.cb_st1.setChecked(False)
            self.cb_st2.setChecked(False)
            self.cb_st4.setChecked(False)
            self.cb_st5.setChecked(False)


        elif val == 4:
            self.cb_st1.setStyleSheet(Status.grayfont)
            self.cb_st2.setStyleSheet(Status.grayfont)
            self.cb_st3.setStyleSheet(Status.grayfont)
            self.cb_st4.setChecked(True)
            self.cb_st4.setStyleSheet(Status.bluefont)
            self.cb_st5.setStyleSheet(Status.grayfont)

            self.cb_st1.setChecked(False)
            self.cb_st3.setChecked(False)
            self.cb_st2.setChecked(False)
            self.cb_st5.setChecked(False)


        elif val == 5:
            self.cb_st1.setStyleSheet(Status.grayfont)
            self.cb_st2.setStyleSheet(Status.grayfont)
            self.cb_st3.setStyleSheet(Status.grayfont)
            self.cb_st4.setStyleSheet(Status.grayfont)
            self.cb_st5.setChecked(True)
            self.cb_st5.setStyleSheet(Status.bluefont)
            
            self.cb_st1.setChecked(False)
            self.cb_st2.setChecked(False)
            self.cb_st3.setChecked(False)
            self.cb_st4.setChecked(False)


        elif val == 255:
            self.cb_st1.setChecked(True)
            self.cb_st1.setStyleSheet(Status.bluefont)
            self.cb_st2.setChecked(True)
            self.cb_st2.setStyleSheet(Status.bluefont)
            self.cb_st3.setChecked(True)
            self.cb_st3.setStyleSheet(Status.bluefont)
            self.cb_st4.setChecked(True)
            self.cb_st4.setStyleSheet(Status.bluefont)
            self.cb_st5.setChecked(True)
            self.cb_st5.setStyleSheet(Status.bluefont)

