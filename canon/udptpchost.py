import socket
import struct
from random import choices, randint
import threading
import logging
import numpy as np

from .mcode2 import *
from .msg2 import MsgProtocol2

class UDPTPCHost(object):
    def __init__(self, ipAddress, sport):
        self.__udpTarget = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.__serverAddress = ipAddress
        self.__sport = sport
        self.__connected = False
        self.timeStamp = 0
        self.__pbit_status = []
        self.status_info = tuple()
        self.cbit_period = 3000
        self.ibit_period = 3000

        # self.__udpTarget.settimeout(5)
        self.__udpTarget.connect((self.__serverAddress, self.__sport))
        logging.info("*"*40)
        logging.info("***** SERVER BIND :: %s :: %s :: %s :: %s", self.__serverAddress, self.__sport, self.__udpTarget, "*"*10)
        logging.info("*"*40)
        self.__connected = True
        self.__boot_udp = False
        self.make_pbit_status()

        self.__thread = threading.Thread(target=self.__listen, args=())
        self.__thread.start()

    def __listen(self):
        logging.info("############## UDPClient :: Start Request .....")
        self.__stoplistening = False
        self.__receivedata = bytearray()
        try:
            while not self.__stoplistening:
                if len(self.__receivedata) == 0:
                    self.__receivedata = bytearray()
                    self.__timeout = 500
                    if self.__udpTarget is not None:
                        try:
                            self.__receivedata = self.__udpTarget.recv(BUFFERSIZE)
                            self.__athread = threading.Thread(target=self.__action_response, args=())
                            self.__athread.start()
                        except Exception as e:
                            pass
                        
        except socket.timeout:
            logging.info("__listen :: socket.timeout :: event")
            self.__receivedata = None

    def close_server(self):
        self.__udpTarget.close()

    def __action_response(self):
        message = self.__receivedata
        msg = MsgProtocol2(ptype=1)
        msg.make_protocol_header_from_msg(message)
        logging.info(f"__action_response :: {msg} :: {message}\n")
        if msg.get_checksum():
            checksum_pass = True
        else:
            checksum_pass = False

        ## self.__receivedata 를 비워야 새로운 데이터를 받을수 있음
        self.__receivedata = bytearray()
        ## TPC_TCC_BOOT_DONE_UPD
        if msg.code == TCC_TPC_BOOT_DONE_UPD:
            logging.info("Success Connection")
            self.__boot_udp = True
            self.timer.cancel()

        elif msg.code == TCC_TPC_PBIT_RESULT_REQ:
            self.response_pbit_result()

        elif msg.code == TCC_TPC_CBIT_RESULT_REQ:
            self.response_cbit_result()
        
        elif msg.code == TCC_TPC_IBIT_RESULT_REQ:
            self.response_ibit_result()

        elif msg.code == TCC_TPC_RESET_UPD:
            if checksum_pass:
                self.response_reset(msg)

        elif msg.code == TCC_TPC_SOFT_EMERGENCY_STOP_UPD:
            if checksum_pass:
                self.response_emergency_stop(msg)

        elif msg.code == TCC_TPC_CTRL_POWER_CMD:
            if checksum_pass:
                self.response_control_onoff(msg)

        elif msg.code == TCC_TPC_DRIVE_POWER_CMD:
            if checksum_pass:
                self.response_upgrade_onoff(msg)

        elif msg.code == TCC_TPC_STATE_INFO_REQ:
            if checksum_pass:
                # logging.info("TCC_TPC_STATE_INFO_REQ CHECKSUM PASS ######")
                self.status_info = self.make_data_pcs_statusinfo()
        
        elif msg.code == TCC_TPC_CBIT_SET_UPD:
            if checksum_pass:
                self.cbit_period = round(msg.data[0]/1000, 1)
                self.response_cbit_period(msg)
            
        elif msg.code == TCC_TPC_IBIT_RUN_CMD:
            if checksum_pass:
                self.ibit_period = round(msg.data[0]/1000, 1)
                self.response_ibit_period(msg)

        elif msg.code == ACK_CODE:
            logging.info("ACK CODE :: %s", msg)
            if msg.data:
                if msg.data[0] == TPC_TCC_BOOT_DONE_UPD:
                    logging.info("Success Connection")
                    self.__boot_udp = True
                    self.timer.cancel()
                

    def response_cbit_period(self, msg):
        ## ACK Response
        msg = MsgProtocol2(2, msg.timeStamp, TPC_ID, TCC_ID, ACK_CODE, CNACK, 1, 1, 4, [TPC_TCC_CBIT_SET_UPD, 0])
        msg.make_encode()
        self.send(msg.message)

    def response_ibit_period(self, msg):
        msg = MsgProtocol2(2, msg.timeStamp, TPC_ID, TCC_ID, ACK_CODE, CNACK, 1, 1, 4, [TPC_TCC_IBIT_DONE_UPD, 0])
        msg.make_encode()
        self.send(msg.message)

    def response_reset(self, msg):
        ## ACK Response
        msg = MsgProtocol2(2, msg.timeStamp, TPC_ID, TCC_ID, ACK_CODE, CNACK, 1, 1, 4, [TCC_TPC_RESET_UPD, 0])
        msg.make_encode()
        logging.info(f"response_reset :: {msg}")
        self.send(msg.message)

    def response_emergency_stop(self, msg):
        ## ACK Response
        msg = MsgProtocol2(2, msg.timeStamp, TPC_ID, TCC_ID, ACK_CODE, CNACK, 1, 1, 4, [TCC_TPC_SOFT_EMERGENCY_STOP_UPD, 0])
        msg.make_encode()
        logging.info(f"response_emergency_stop :: {msg}")
        self.send(msg.message)

    def response_control_onoff(self, oldmsg):
        ## ACK Response
        msg = MsgProtocol2(2, oldmsg.timeStamp, TPC_ID, TCC_ID, ACK_CODE, CNACK, 1, 1, 4, [TCC_TPC_CTRL_POWER_CMD, 0])
        msg.make_encode()
        logging.info(f"response_control_onoff :: {msg}")
        self.send(msg.message)

        ## Control Command Result
        logging.info(f"response_control_onoff :: {oldmsg.data}")
        msg = MsgProtocol2(2, msg.timeStamp, TPC_ID, TCC_ID, TPC_TCC_CTRL_POWER_UPD, CNACK, 1, 1, 4, oldmsg.data)
        msg.make_encode()
        logging.info(f"Result :: {msg}")
        self.send(msg.message)

    def response_upgrade_onoff(self, oldmsg):
        msg = MsgProtocol2(2, oldmsg.timeStamp, TPC_ID, TCC_ID, ACK_CODE, CNACK, 1, 1, 4, [TCC_TPC_DRIVE_POWER_CMD, 0])
        msg.make_encode()
        logging.info(f"response_upgrade_onoff :: {msg}")
        self.send(msg.message)

        ## Control Command Result
        logging.info(f"response_upgrade_onoff :: {oldmsg.data}")
        msg = MsgProtocol2(2, msg.timeStamp, TPC_ID, TCC_ID, TPC_TCC_DRIVE_POWER_UPD, CNACK, 1, 1, 4, oldmsg.data)
        msg.make_encode()
        logging.info(f"Result :: {msg}")
        self.send(msg.message)

    ## TCC_TPC_STATE_INFO_REQ
    def make_data_pcs_statusinfo(self):
        self.timeStamp += 1
        dataLength = 111
        status = []
        for i in range(111):
            if i < 23:
                status.append(1)
            else:
                n = randint(1, 10000)
                if n > 9500:
                    status.append(4)
                else:
                    status.append(0)

        dataLength = 222
        msg = MsgProtocol2(2, self.timeStamp, TPC_ID, TCC_ID, TPC_TCC_STATE_INFO_REF, PNACK, 1, 1, dataLength, status)
        msg.make_encode()
        self.send(msg.message)

    def send(self, data):
        try:
            self.__udpTarget.send(data)
        except Exception as e:
            pass

    def loop_pcs_boot_signal(self):
        if not self.__boot_udp:
            msg = MsgProtocol2(2, 0, TPC_ID, TCC_ID, TPC_TCC_BOOT_DONE_UPD, PACK, 1, 1, 0)
            logging.info(f"loop_pcs_boot_signal :: {msg}")
            msg.make_encode()
            self.send(msg.message)

            self.timer = threading.Timer(BOOT_SIGNAL_REPEAT, self.loop_pcs_boot_signal)
            self.timer.start()

    def make_pbit_status(self):
        dataLength = 37
        for i in range(dataLength):
            self.__pbit_status.append(0)

    def make_pbit_status_error(self):
        rd = choices(list(range(0, 37)))[0]
        self.__pbit_status[rd] = 2
        logging.info(f"\n\n################# make_pbit_status_error :: {self.__pbit_status[rd]}\n\n")

    def response_pbit_result(self):
        # TCC_TPC_PBIT_RESULT_REQ
        self.timeStamp += 1
        dataLength = 74

        n = randint(1, 10000)
        if n > 9500:
            self.make_pbit_status_error()
        
        msg = MsgProtocol2(2, self.timeStamp, TPC_ID, TCC_ID, TPC_TCC_PBIT_RESULT_REF, PNACK, 1, 1, dataLength, self.__pbit_status)
        msg.make_encode()
        self.send(msg.message)

        logging.info("RESPONSE :: TCC_TPC_PBIT_RESULT_REQ :: %s\n", msg)

        timer = threading.Timer(TPERIOD, self.response_pbit_result)
        timer.start()

    def response_ibit_result(self):
        # TCC_TPC_PBIT_RESULT_REQ
        self.timeStamp += 1
        dataLength = 37

        n = randint(1, 10000)
        if n > 9900:
            self.make_pbit_status_error()

        dataLength = 74
        msg = MsgProtocol2(2, self.timeStamp, TPC_ID, TCC_ID, TPC_TCC_IBIT_RESULT_REF, PNACK, 1, 1, dataLength, self.__pbit_status)
        msg.make_encode()
        self.send(msg.message)

        logging.info("RESPONSE :: TCC_TPC_IBIT_RESULT_REQ :: %s\n", msg)

        timer = threading.Timer(self.ibit_period, self.response_ibit_result)
        timer.start()

    def response_cbit_result(self):
        # TCC_TPC_PBIT_RESULT_REQ
        self.timeStamp += 1
        dataLength = 37

        n = randint(1, 10000)
        if n > 9900:
            self.make_pbit_status_error()

        dataLength = 74
        msg = MsgProtocol2(2, self.timeStamp, TPC_ID, TCC_ID, TPC_TCC_CBIT_RESULT_REF, PNACK, 1, 1, dataLength, self.__pbit_status)
        msg.make_encode()
        self.send(msg.message)

        logging.info("RESPONSE :: TPC_TCC_CBIT_RESULT_REF :: %s\n", msg)

        timer = threading.Timer(self.cbit_period, self.response_cbit_result)
        timer.start()

    def request_pcs_status_info(self):
        # TCC_TPC_PBIT_RESULT_REQ
        msg = MsgProtocol2(2, self.timeStamp, TPC_ID, TCC_ID, TCC_TPC_STATE_INFO_REQ, PNACK, 1, 1, 0)
        msg.make_encode()
        self.send(msg.message)

        logging.info("RESPONSE :: TCC_TPC_STATE_INFO_REQ_NOACK :: %s :: %s\n", msg.message, msg)

        # timer = threading.Timer(BOOT_SIGNAL_REPEAT, self.request_pcs_status_info)
        # timer.start()

    def close(self):
        """
        Closes Serial port, or TCP-Socket connection
        """
        if self.__udpTarget is not None:
            self.__stoplistening = True
            self.__udpTarget.shutdown(socket.SHUT_RDWR)
            self.__udpTarget.close()
        self.__connected = False

    @property
    def ipaddress(self):
        """
        Gets the IP-Address of the Server to be connected
        """
        return self.__serverAddress

    @ipaddress.setter
    def ipaddress(self, ipAddress):
        """
        Sets the IP-Address of the Server to be connected
        """
        self.__serverAddress = ipAddress



