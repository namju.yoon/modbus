#!/usr/bin/env python
# coding: utf-8

# 예제 내용
# * 기본 위젯을 사용하여 기본 창을 생성
# * 다양한 레이아웃 위젯 사용
import os
import sys
import pandas as pd
import time
import json, codecs
import can
# can.interface.detect_available_configs()
from datetime import datetime

from PyQt5.QtWidgets import QWidget, QLabel, QPushButton, QDesktopWidget, QMainWindow
from PyQt5.QtWidgets import QGroupBox, QVBoxLayout, QHBoxLayout, QGridLayout, QLineEdit
from PyQt5.QtWidgets import QApplication, QStatusBar, QFileDialog, QLCDNumber, QMessageBox
from PyQt5 import QtCore
from PyQt5.QtCore import QDate, Qt
from PyQt5.QtGui import QPixmap
import pyqtgraph as pyGraph

GRAPH = 1
DATA = 2
DATA_SAVE = 100
SAVE_NUM = 1140

import logging
FORMAT = ('%(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.INFO)
# log.setLevel(logging.DEBUG)

def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)

def select_device():
    device_list = can.interface.detect_available_configs()
    # logging.info(device_list)
    selected = None
    for device in device_list:
        if device['interface'] == 'ixxat':
            selected = device 
            break
    logging.info(selected)
    return selected


class CLed(QPushButton):
    def __init__(self, name):
        super(CLed, self).__init__()
        self.setText(name)
        self.setFixedSize(120, 120)
        style = f"border: 1px solid lightgray;border-radius: 60px;background-color: #ccc;color: black;font-size: 14px;font-weight: bold;"
        self.setStyleSheet(style)

    def changeColor(self, color):
        style = f"border: 1px solid lightgray;border-radius: 60px;background-color: {color};color: black;font-size: 14px;font-weight: bold;"
        self.setStyleSheet(style)


class CircleButton(QPushButton):
    def __init__(self, name):
        super(CircleButton, self).__init__()
        self.setText(name)
        self.setFixedSize(120, 120)
        style = f"border: 1px solid lightgray;border-radius: 60px;background-color: #ccc;color: black;font-size: 24px;font-weight: bold;"
        self.setStyleSheet(style)

    def changeColor(self, color):
        style = f"border: 1px solid lightgray;border-radius: 60px;background-color: {color};color: black;font-size: 24px;font-weight: bold;"
        self.setStyleSheet(style)


class RecLed(QPushButton):
    def __init__(self, name):
        super(RecLed, self).__init__()
        self.setText(name)
        self.setFixedSize(120, 120)
        style = f"border: 1px solid lightgray;border-radius: 10px;background-color: #ccc;color: black;font-size: 28px;font-weight: bold;"
        self.setStyleSheet(style)

    def changeColor(self, color):
        style = f"border: 1px solid lightgray;border-radius: 10px;background-color: {color};color: black;font-size: 28px;font-weight: bold;"
        self.setStyleSheet(style)


def decon(msg_id):
    msg_prio=msg_id>>26
    msg_type=(msg_id>>24) & 0x03
    msg_dst=(msg_id>>16) & 0xFF
    msg_src=(msg_id>>8) & 0xFF
    msg_cmd=msg_id & 0xFF
    # logging.info(f"prio: {hex(msg_prio)}, type: {hex(msg_type)}, dst: {hex(msg_dst)}, src: {hex(msg_src)}, cmd: {hex(msg_cmd)}")
    return [msg_prio, msg_type, msg_dst, msg_src, msg_cmd]


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.device = select_device()
        self.canbus = None
        if self.device:
            try:
                self.canbus = can.Bus(interface='ixxat', channel=self.device['channel'], bitrate=500000, extended=True)
                logging.info(f"can.Bus :: SUCCESS :: {self.canbus}")
                logging.info('Connected to {}: {}'.format(self.canbus.__class__.__name__, self.canbus.channel_info))
            except Exception as e:
                self.canbus = None
                logging.info(f"can.Bus :: ERROR :: {str(e)}")
                self.error_message.setText(str(e))

        if not os.path.exists("data"):
            print("CTEATE :: data/")
            os.makedirs("data") 

        self.main_label = "QLabel{font-size: 20pt; font-weight: bold}"
        self.error_label = "QLabel{font-size: 14pt; font-weight: bold; color: red}"
        self.stimer = QtCore.QTimer()
        self.dtimer = QtCore.QTimer()

        self.run_flag = False

        self.start_time = None
        self.graph_flag = False
        self.countN = 0
        self.dataN = 0

        self.time = 0

        self.tmarks = []
        self.interlock1s = []
        self.interlock2s = []
        self.interlock3s = []
        self.bcustatuss = []

        self.vol1s = []
        self.vol2s = []
        self.vol3s = []
        self.cur1s = []
        self.cur2s = []
        self.cur2s = []
        self.cur3s = []

        self.pen_vol1 = pyGraph.mkPen(width=2, color=(255, 0, 0))
        self.pen_cur1 = pyGraph.mkPen(width=2, color=(0, 0, 255))
        self.pen_vol2 = pyGraph.mkPen(width=2, color=(255, 191, 0))
        self.pen_cur2 = pyGraph.mkPen(width=2, color=(26, 175, 51))
        
        self.initUI()

    def displayMenuLayout(self, parent_layout, glabel, btn, second=None, third=None):
        grp_sub = QGroupBox(glabel)
        layout_sub = QHBoxLayout()
        grp_sub.setLayout(layout_sub)
        layout_sub.addWidget(btn)
        if second:
            layout_sub.addWidget(second)
        if third:
            layout_sub.addWidget(third)
        parent_layout.addWidget(grp_sub)

    def displayButtonLayout(self, parent_layout, glabel, btn):
        grp_sub = QGroupBox(glabel)
        layout_sub = QVBoxLayout()
        grp_sub.setLayout(layout_sub)
        layout_sub.addWidget(btn)
        parent_layout.addWidget(grp_sub)

    def menu_display(self, layout):
        grp_menu = QGroupBox("")
        layout_menu = QHBoxLayout()
        grp_menu.setLayout(layout_menu)

        ## replace #1st ###############
        # 전원 장치 연결
        grp_channel = QGroupBox("")
        layout_channel = QGridLayout()
        grp_channel.setLayout(layout_channel)

        self.label_can = QLabel("CAN INTERFACE : ")
        self.label_canid = QLabel("ixxat")

        if self.canbus:
            logging.info(f"self.canbus = {self.canbus}")
            self.error_message = QLabel("")
        else:
            logging.info("ixxat interface 를 찾을수 없습니다.")
            self.error_message = QLabel("ixxat interface 를 찾을수 없습니다.")
            self.error_message.setStyleSheet(self.error_label)

        layout_channel.addWidget(self.label_can, 0, 0)
        layout_channel.addWidget(self.label_canid, 0, 1)
        layout_channel.addWidget(self.error_message, 1, 0, 1, 2)
        layout_menu.addWidget(grp_channel)
        
        #### Title
        title = QLabel("AS10 Monitoring")
        title.setStyleSheet(self.main_label)
        self.displayMenuLayout(layout_menu, "title", title)
        
        #### Num 데이터 주기  
        self.btn_num = QLineEdit("20")
        label = QLabel("ms")
        self.displayMenuLayout(layout_menu, "데이터주기", self.btn_num, label)

        #### 단말기 RUN/STOP
        self.btn_run = CLed("RUN")
        self.btn_run.clicked.connect(self.device_run)

        if self.canbus:
            self.btn_run.changeColor('#0f0')
        else:
            self.btn_run.setEnabled(False)

        self.btn_stop = CLed("STOP")
        self.btn_stop.setEnabled(False)
        self.btn_stop.clicked.connect(self.device_stop)

        
        self.displayMenuLayout(layout_menu, "DEVICE RUN/STOP", self.btn_run, self.btn_stop)

        # Logo Image
        labelLogo = QLabel("")
        pixmap = QPixmap(resource_path("logo.png"))
        labelLogo.setAlignment(Qt.AlignRight)
        labelLogo.setPixmap(pixmap)
        layout_menu.addWidget(labelLogo)

        layout.addWidget(grp_menu)

    ## menu_timer
    def menu_timer(self, layout):     
        grp_num = QGroupBox("")
        layout_num = QHBoxLayout()
        grp_num.setLayout(layout_num)

        ### 시작 시간 보여 주기 
        grp_start = QGroupBox("start 일자/시간")
        layout_start = QVBoxLayout()
        grp_start.setLayout(layout_start)
        self.label_start_time = QLabel("%Y-%m-%d %H:%M:%S")
        self.label_start_time.setStyleSheet(self.main_label)
        layout_start.addWidget(self.label_start_time)
        layout_num.addWidget(grp_start)

        #### Timer Display
        grp_time = QGroupBox("Time")
        layout_time = QVBoxLayout()
        grp_time.setLayout(layout_time)

        self.display_time = QLCDNumber()
        self.display_time.setDigitCount(12)
        self.display_time.setStyleSheet('background: white')
        timedisplay = '{:02d}D:{:02d}:{:02d}:{:02d}'.format(
                (self.time) // 86400,
                ((self.time % 86400) // 60) // 60, 
                ((self.time % 86400) // 60) % 60, ((self.time) % 86400) % 60)
        self.display_time.display(timedisplay)

        layout_time.addWidget(self.display_time)
        layout_num.addWidget(grp_time)

        layout.addWidget(grp_num)

    ## status
    def status_display(self, layout):
        grp_status = QGroupBox("GRAPH")
        layout_status = QHBoxLayout()
        grp_status.setLayout(layout_status)
        self.status_list = []

        self.interlock1 = CircleButton("InterLock1")
        self.displayMenuLayout(layout_status, "InterLock1", self.interlock1)
        self.status_list.append(self.interlock1)

        self.interlock2 = CircleButton("InterLock2")
        self.displayMenuLayout(layout_status, "InterLock2", self.interlock2)
        self.status_list.append(self.interlock2)

        self.interlock3 = CircleButton("InterLock3")
        self.displayMenuLayout(layout_status, "InterLock3", self.interlock3)
        self.status_list.append(self.interlock3)

        self.bcustatus = CircleButton("BCU Status")
        self.displayMenuLayout(layout_status, "BCU Status", self.bcustatus)
        self.status_list.append(self.bcustatus)

        self.vol1 = RecLed("0")
        self.displayMenuLayout(layout_status, "Output Voltage 1", self.vol1)
        self.vol2 = RecLed("0")
        self.displayMenuLayout(layout_status, "Output Voltage 2", self.vol2)
        self.vol3 = RecLed("0")
        self.displayMenuLayout(layout_status, "Output Voltage 3", self.vol3)

        self.cur1 = RecLed("0")
        self.displayMenuLayout(layout_status, "Output Current 1", self.cur1)
        self.cur2 = RecLed("0")
        self.displayMenuLayout(layout_status, "Output Current 2", self.cur2)
        self.cur3 = RecLed("0")
        self.displayMenuLayout(layout_status, "Output Current 3", self.cur3)
        layout.addWidget(grp_status)

    ## initUI
    def initUI(self):
        self.setWindowTitle("PSTEK AS10 MONITORING")
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width(), screensize.height()) 

        mainwidget = QWidget()  # 위젯의 인스턴스 생성만으로도 QMainWindow에 붙는다.
        self.setCentralWidget(mainwidget)

        ## Main Layout 설정
        main_layer = QVBoxLayout()
        mainwidget.setLayout(main_layer)
        
        ## 1st ########################
        self.menu_display(main_layer)

        ## 2th ########################
        # num
        self.menu_timer(main_layer)

         ## 3th ########################
        # 그래프 디스플레이
        self.status_display(main_layer)
        
        ## 3th ########################
        # Status
        self.statusbar = QStatusBar()
        self.setStatusBar(self.statusbar)
        self.statusbar.setObjectName("statusbar")
        
        self.statusmessage = 'PSTEK, {},  {}'
        now = QDate.currentDate()
        displaymessage = self.statusmessage.format(now.toString(Qt.ISODate), 'PSTEK is aiming for a global leader. !!')
        self.statusbar.showMessage(displaymessage)

    def keyPressEvent(self, e): #키가 눌러졌을 때 실행됨
        if e.key() == Qt.Key_Escape:
            logging.info("esc pressed")
            self.canbus.shutdown()
            logging.info("SHUT DOWN!!!!")
            self.close()
        elif e.key() == Qt.Key_A:
            logging.info("A is pressed)")
        else:
            logging.info((e.key()))
     
    # 2bit Check
    def bit_check(self, x):
        data = []
        for idx in [3, 2, 1, 0]:
            shift = idx * 2
            val = ((x >> shift ) & 3)
            data.append(val)
        return data
        
    # 데이터 가져 오기
    def get_data(self):
        msg = can.Message(
            arbitration_id=0x020080F0, data=[0, 0, 0, 0, 0, 0, 0, 0], is_extended_id=True
        )
        try:
            self.canbus.send(msg)
            # logging.info(f"GET DATA Message sent on {self.canbus.channel_info}")
        except can.CanError:
            logging.info("GET DATA Message NOT sent")

        revmsg = self.canbus.recv(0.1)
        
        if revmsg is not None:
            # logging.info(f"RECV ORGIN : {revmsg}")
            if revmsg.arbitration_id == 0x1636c0f5:
                self.countN += 1
                data = list(revmsg.data)

                if self.countN % 2 == 0:
                    first = data[0]
                    result = self.bit_check(first)

                    vol1 = int(data[1] * 2)
                    self.vol1.setText(str(vol1))
                    vol2 = int(data[2] * 2)
                    self.vol2.setText(str(vol2))
                    vol3 = int(data[3] * 2)
                    self.vol3.setText(str(vol3))
                    
                    # cur1 = int(data[4] / 5)
                    self.cur1.setText(str(data[4]))
                    # cur2 = int(data[5] / 5)
                    self.cur2.setText(str(data[5]))
                    # cur3 = int(data[6] / 5)
                    self.cur3.setText(str(data[6]))

                    for idx, val in enumerate(result):
                        if val == 0:
                            self.status_list[idx].changeColor('#f30')
                            self.status_list[idx].setText('Disabled')
                        elif val == 2:
                            self.status_list[idx].changeColor('#f00')
                            self.status_list[idx].setText('Error')
                        elif val == 3:
                            self.status_list[idx].changeColor('#0ff')
                            self.status_list[idx].setText("Don't Care")

                    if self.countN % DATA_SAVE == 0:
                        self.dataN += 1
                        tm = time.time()
                        self.tmarks.append(tm)
                        self.vol1s.append(data[1])
                        self.vol2s.append(data[2])
                        self.vol3s.append(data[3])

                        cur1 = int(data[4] / 5)
                        self.cur1s.append(cur1)
                        cur2 = int(data[5] / 5)
                        self.cur2s.append(cur2)
                        cur3 = int(data[6] / 5)
                        self.cur3s.append(cur3)

                        for idx, val in enumerate(result):
                            if idx == 0:
                                self.interlock1s.append(val)
                            elif idx == 1:
                                self.interlock2s.append(val)
                            elif idx == 2:
                                self.interlock3s.append(val)
                            elif idx == 3:
                                self.bcustatuss.append(val)

                    if self.dataN ==  SAVE_NUM:
                        self.regular_save_json()
                        self.countN = 0
                        self.dataN = 0
                        

    # Timer 
    def get_time(self):
        self.time += 1
        timedisplay = '{:02d}D:{:02d}:{:02d}:{:02d}'.format(
                (self.time) // 86400,
                ((self.time % 86400) // 60) // 60, 
                ((self.time % 86400) // 60) % 60, ((self.time) % 86400) % 60)
        self.display_time.display(timedisplay)
        
    # Run Device (중간에 Run 을 눌러서 디바이스 시작시킴)
    def device_run(self):
        self.btn_run.setEnabled(False)
        self.btn_run.changeColor('#ccc')

        self.btn_stop.setEnabled(True)
        self.btn_stop.changeColor('#f00')

        self.start_time = datetime.now()
        label_start_time = datetime.strftime(self.start_time, '%Y-%m-%d %H:%M:%S')
        self.label_start_time.setText(label_start_time)

        now = QDate.currentDate()
        displaymessage = self.statusmessage.format(now.toString(Qt.ISODate), "DEVICE RUN")
        self.statusbar.showMessage(displaymessage)

        self.run_flag = True 

        if not self.dtimer:
            self.dtimer = QtCore.QTimer()
            self.stimer = QtCore.QTimer()

        DATA_PERIOD = int(self.btn_num.text())
        # DATA_PERIOD = int(1000/num)

        self.stimer.setInterval(1000)
        self.stimer.timeout.connect(self.get_time)
        self.stimer.start()

        # setting timer
        self.dtimer.setInterval(DATA_PERIOD)
        self.dtimer.timeout.connect(self.get_data)
        self.dtimer.start()

        msg = can.Message(
            arbitration_id=0x1235C7F0, data=[2, 0, 0, 0, 0, 0, 0, 0], is_extended_id=True
        )
        try:
            response = self.canbus.send(msg)
            logging.info(f"RUN Message sent on {self.canbus.channel_info} :: {response}")
        except can.CanError:
            logging.info("RUN Message NOT sent")

        for idx in range(4):
            self.status_list[idx].changeColor('#0f0')
            self.status_list[idx].setText('GOOD')


    # 단말기 STOP
    def device_stop(self):
        self.btn_stop.setEnabled(False)
        self.btn_stop.changeColor('#ccc')

        self.btn_run.setEnabled(True)
        self.btn_run.changeColor('#0f0')

        msg = can.Message(
            arbitration_id=0x1235C7F0, data=[1, 0, 0, 0, 0, 0, 0, 0], is_extended_id=True
        )
        try:
            response = self.canbus.send(msg)
            logging.info(f"STOP Message sent on {self.canbus.channel_info} :: {response}")
            
        except can.CanError:
            logging.info("STOP Message NOT sent")

        if self.dtimer:
            self.dtimer.stop()
            self.dtimer = None
            self.stimer.stop()
            self.stimer = None

        self.data_save()

        now = QDate.currentDate()
        displaymessage = self.statusmessage.format(now.toString(Qt.ISODate), "DEVICE STOP")
        self.statusbar.showMessage(displaymessage)

    # SAVE DATA as JSON FILE
    def data_save(self):
        start_time = datetime.now()
        file = f"AS10_{start_time.year:04d}_{start_time.month:02d}_{start_time.day:02d}_{start_time.hour:02d}_{start_time.minute:02d}_{start_time.second:02d}"
        
        time_list = []
        for tm in self.tmarks:
            if isinstance(tm, str):
                time_list.append(tm)
            else:
                tm = time.localtime(tm)
                string = time.strftime('%H:%M:%S', tm)
                time_list.append(string)

        dataset = pd.DataFrame({
            "times": time_list,
            "interlock1s" :self.interlock1s,
            "interlock2s" :self.interlock2s,
            "interlock3s" :self.interlock3s,
            "bcustatuss" :self.bcustatuss,
            'vol1s': self.vol1s,
            'vol2s': self.vol2s,
            'vol3s': self.vol3s,
            'cur1s': self.cur1s,
            'cur2s': self.cur2s,
            'cur3s': self.cur3s,
        })
        
        # CSS FILE
        csvfile = f"data/{file}.csv"
        dataset.to_csv(csvfile, sep=',')
        logging.info(f"########## data save : { csvfile } #####")

    def regular_save_json(self):
        self.data_save()
        logging.info("########## regular_save_json #####")

        self.countN = 0
        self.dataN = 0
        self.tmarks = []
        self.interlock1s = []
        self.interlock2s = []
        self.interlock3s = []
        self.bcustatuss = []

        self.vol1s = []
        self.vol2s = []
        self.vol3s = []
        self.cur1s = []
        self.cur2s = []
        self.cur2s = []
        self.cur3s = []

    def closeEvent(self, event):
        close = QMessageBox()
        close.setText("You sure?")
        close.setStandardButtons(QMessageBox.Yes | QMessageBox.Cancel)
        close = close.exec()

        if close == QMessageBox.Yes:
            self.canbus.shutdown()
            logging.info("SHUT DOWN!!!!")
            event.accept()
        else:
            event.ignore()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    form = MainWindow()
    form.show()
    sys.exit(app.exec_())