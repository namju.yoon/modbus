#!/usr/bin/env python
# coding: utf-8

# 예제 내용
# * 기본 위젯을 사용하여 기본 창을 생성
# * 다양한 레이아웃 위젯 사용
import os
import sys
import numpy as np
from datetime import datetime
import threading
from PyQt5.QtWidgets import QWidget, QLabel, QPushButton, QDesktopWidget, QMainWindow, QTextEdit
from PyQt5.QtWidgets import QGroupBox, QVBoxLayout, QHBoxLayout, QGridLayout, QDialog, QCheckBox
from PyQt5.QtWidgets import QApplication, QStatusBar, QTabWidget, QLineEdit, QTableWidgetItem
from PyQt5.QtWidgets import QHeaderView, QTableWidget, QAbstractItemView, QRadioButton, QComboBox, QSpinBox
from PyQt5 import QtCore
from PyQt5.QtCore import QDate, Qt, pyqtSlot
from PyQt5.QtGui import QPixmap

from canon.mcode import *
from common.block import MyLabel, resource_path, WarningDialog, CLed
from canon.msg import SubSystem, Module, Field, Status, PModule, PField
from canon.udpserver import UDPServer

import logging
FORMAT = ('%(module)-10s: %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.DEBUG)


class CanonMainWindow(QMainWindow):
    cyanfont = "border: 1px solid lightgray;border-radius: 25px;background-color: #ccffff;color: black;font-size: 14px;font-weight: bold;"
    skyfont = "border: 1px solid lightgray;border-radius: 25px;background-color: #33acff;color: black;font-size: 14px;font-weight: bold;"
    purplefont = "border: 1px solid lightgray;border-radius: 25px;background-color: #F991FC;color: black;font-size: 14px;font-weight: bold;"

    redfont = "border: 1px solid lightgray;border-radius: 25px;background-color: #FF0000;color: black;font-size: 14px;font-weight: bold;"
    grayfont = "border: 1px solid lightgray;border-radius: 25px;background-color: #ccc;color: black;font-size: 14px;font-weight: bold;"

    def __init__(self, parent=None):
        super(CanonMainWindow, self).__init__(parent)

        self.tccServer = None
        self.index = 0
        self.event = threading.Event()

        self.select_part = None
        self.onoff = None
        self.highonoff = None

        self.cflag = False
        self.pflag = False
        self.iflag = False

        self.emergency_stop_flag = False
        self.pfields = []

        self.checktimer = None
        self.pcstimer = None
        self.ibittime = None
        self.cbit_period = 5
        self.ibit_period = 7

        self.pbitsystems = {}
        self.cbitsystems = {}
        self.ibitsystems = {}

        self.pbit_fields = []
        self.cbit_fields = []
        self.ibit_fields = []

        self.cbit_flag = False
        self.ibit_flag = False 
        self.fbit_flag = False
        self.pcs_flag = False

        self.pbittimer = None
        self.cbittimer = None
        self.ibittimer = None

        self.reqrow = 0
        self.resrow = 0

        self.client = None
        self.gen_port = None

        self.countN = 0
        self.initial = {}
        self.time_delay= 10

        self.btn_boot = None
        self.btn_stop = None

        self.modules = ['-----', '모듈1', '모듈2', '모듈3','모듈4','모듈5']
        self.choices = ['-----', '입력 과전류', '입력 과전압', '입력 저전압', '출력 과전류', '출력 과전압', '출력 저전압', '제품 과온도']

        self.initUI()

    def __str__(self):
        return f"CanonMainWindow :: __str__ "

    def closeEvent(self, event):
        try:
            if self.tccServer:
                self.tccServer.close_server()
        except Exception as e:
            logging.debug("closeEvent :: %s", e)

    ## Process and Logic
    def run_pcs(self):
        # 최초 BOOT 할 때,
        # if not self.tccServer: # 향후 수정 예ㅈ
        logging.debug("CanonMainWindow :: run_pcs :: %s", self.tccServer)
        pcs_ipaddress = self.edit_address.text()
        pcs_port = int(self.edit_port.text())

        while not self.tccServer:
            self.event.wait(2)
            try:
                self.tccServer = UDPServer(self, pcs_ipaddress, pcs_port)
                self.tccServer.send_requestBox.connect(self.updateRequest)
                self.tccServer.send_responseBox.connect(self.updateResponse)
                self.tccServer.send_normalMsgBox.connect(self.updateNormalMessage)
                self.tccServer.send_highMsgBox.connect(self.updateHighMessage)

                logging.debug("Start bootServer \n")
            except Exception as e:
                self.index += 1
                logging.debug("INDEX :: %d :: %s", self.index, e)
        
        index = 1
        while True:
            self.event.wait(2)
            logging.debug("run_pcs :: self.tccServer :: %d", index)
            index += 1
            if self.tccServer.check_bootflag():
                self.btn_boot.setEnabled(False)
                self.btn_boot.setStyleSheet(CanonMainWindow.grayfont)

                self.btn_pbit.setEnabled(True)
                self.btn_pbit.setStyleSheet(CanonMainWindow.cyanfont)
                break
        ## CPU 리셋과 같은 경우 발생 할때
        # else:

    # power_control_cmd
    def power_control_cmd(self):
        checked_list = []

        for i in range(self.layout_parts.count()):
            chBox = self.layout_parts.itemAt(i).widget()
            if chBox.isChecked():
                checked_list.append(chBox.text())

        check_value = []
        for name in checked_list:
            for key, value in PCS_PARTS.items():
                if name == value:
                    check_value.append(key)
                    break

        for val in check_value:
            self.tccServer.power_control_cmd(self.onoff, val)  

    # high_power_control_cmd
    def high_power_control_cmd(self):
        self.tccServer.high_power_control_cmd(self.highonoff)  

    ## START PBIT
    def request_pbit(self):
        self.tccServer.request_Pbit_status()
        self.event.wait(TIME_WAIT)
        self.update_Pbit_result()

    def request_cbit(self):
        if not self.cbit_flag:
            self.cbit_flag = True
            data = [0xff]
            self.tccServer.request_Cbit_status(data)
            self.event.wait(TIME_WAIT)
            self.update_Cbit_result()

        else:
            self.cbit_flag = False 
            data = [0x0]
            self.tccServer.request_Cbit_status(data)
            self.event.wait(TIME_WAIT)

    def request_ibit(self):
        if not self.ibit_flag:
            self.ibit_flag = True
            data = [0xff]
            self.tccServer.request_Ibit_status(data)
            self.event.wait(TIME_WAIT)
            self.update_Cbit_result()

        else:
            self.ibit_flag = False 
            data = [0x0]
            self.tccServer.request_Ibit_status(data)
            self.event.wait(TIME_WAIT)

    def request_shutdown_ready(self):
        self.tccServer.request_shutdown_ready()
        self.event.wait(TIME_WAIT)

    def request_pcs_reset(self, val):
        logging.debug("request_pcs_reset %s \n", val)
        self.tccServer.request_pcs_reset(val)

    ## Bit update
    def update_Pbit_result(self):
        for index, data in enumerate(self.tccServer.pbit_status):
            for idx, dd in enumerate(data):
                try:
                    if dd:
                        field = self.pbit_fields[index][idx]
                        logging.debug(f"update_Pbit_result :: {field}, {dd}\n\n")
                        if dd == 1:
                            field.change_warning()
                        elif dd >= 2:
                            field.change_fail()
                    else:
                        field = self.pbit_fields[index][idx]
                        if field.value in [WARNING, FAIL]:
                            field.change_normal()
                        
                except Exception as e:
                    pass
                    # logging.debug(f"update_Pbit_result :: {index}, {idx} :: %s\n\n", e)

        if not self.pflag:
            self.pflag = True 
            self.btn_cbit.setEnabled(True)
            self.btn_cbit.setStyleSheet(CanonMainWindow.skyfont)

            self.btn_cbitperiod.setEnabled(True)
            self.btn_cbitperiod.setStyleSheet(CanonMainWindow.skyfont)

            ### PCS_STATUS
            self.pcstimer = QtCore.QTimer()
            self.pcstimer.timeout.connect(self.request_status)
            self.pcstimer.start(2000)

            ## 
            self.btn_em_stop.setEnabled(True)
            self.btn_em_stop.setStyleSheet(CanonMainWindow.redfont)
            # self.btn_waiting.setEnabled(True)
            self.btn_fault.setEnabled(True)
            self.btn_cpu.setEnabled(True)

    ## Bit update
    def update_Cbit_result(self):
        for index, data in enumerate(self.tccServer.cbit_status):
            for idx, dd in enumerate(data):
                try:
                    if dd:
                        field = self.cbit_fields[index][idx]
                        logging.debug(f"update_Cbit_result :: {field}, {dd}\n\n")
                        if dd == 1:
                            field.change_warning()
                        elif dd >= 2:
                            field.change_fail()
                    else:
                        field = self.cbit_fields[index][idx]
                        if field.value in [WARNING, FAIL]:
                            field.change_normal()
                except Exception as e:
                    pass
                    # logging.debug(f"update_Cbit_result :: {index}, {idx} :: %s\n\n", e)

        if not self.ibit_flag:
            self.ibit_flag = True 
            self.btn_ibit.setEnabled(True)
            self.btn_ibit.setStyleSheet(CanonMainWindow.purplefont)

            self.btn_ibitperiod.setEnabled(True)
            self.btn_ibitperiod.setStyleSheet(CanonMainWindow.purplefont)

            ## PBit
            self.cbittimer = QtCore.QTimer()
            self.cbittimer.timeout.connect(self.update_Cbit_result)
            self.cbittimer.start(ACK_TIME)

    ## Bit update
    def update_Ibit_result(self):
        for index, data in enumerate(self.tccServer.ibit_status):
            for idx, dd in enumerate(data):
                try:
                    if dd:
                        field = self.ibit_fields[index][idx]
                        logging.debug(f"update_Ibit_result :: {field}, {dd}\n\n")
                        if dd == 1:
                            field.change_warning()
                        elif dd >= 2:
                            field.change_fail()
                    else:
                        field = self.ibit_fields[index][idx]
                        if field.value in [WARNING, FAIL]:
                            field.change_normal()
                except Exception as e:
                    pass
                    # logging.debug(f"update_Ibit_result :: {index}, {idx} :: %s\n\n", e)

        if not self.fbit_flag:
            self.fbit_flag = True
            self.ibittimer = QtCore.QTimer()
            self.ibittimer.timeout.connect(self.update_Ibit_result)
            self.ibittimer.start(ACK_TIME)


    ## CBIT 전송 주기 설정 setup_cbitperiod
    def setup_cbitperiod(self):
        cbitperiod = int(self.edit_cbitperiod.text())
        logging.debug("CanonMainWindow :: setup_cbitperiod :: %d", cbitperiod)
        self.cbit_period = int(cbitperiod/1000)
        self.tccServer.setup_cbitperiod([cbitperiod])

    ## IBIT 전송 주기 설정 setup_ibitime
    def setup_ibitime(self):
        self.iflag = False
        ibitperiod = int(self.edit_ibitperiod.text())
        self.ibit_period = int(ibitperiod/1000)
        self.tccServer.setup_ibitime([ibitperiod])
        # logging.debug("CanonMainWindow :: setup_ibitime :: %d", ibitperiod)

        # TIME CHECK 
        self.ibittime = datetime.now()
        self.checktimer = QtCore.QTimer()
        self.checktimer.timeout.connect(self.ibit_timecheck)
        self.checktimer.start(ibitperiod)

    ## IBIT TIME CHECK
    def ibit_timecheck(self):
        now = datetime.now()
        secondtime = int((now - self.ibittime).total_seconds())
        # logging.debug("ibit_timecheck :: %d", secondtime)
        
        if self.iflag:
            self.checktimer.stop()
            self.checktimer = None 
            return

        if secondtime >= self.ibit_period * 1.5:
            self.checktimer.stop()
            self.checktimer = None 

            dialog = WarningDialog("캘리브레이션을 할 제품의 번호를 입력하세요")
            dialog.show()
            response = dialog.exec_()

            # OK 를 하면 설정 값을 읽어와서 통신을 한다.
            if response == QDialog.Accepted:
                pass

    def update_ibit_flag(self):
        self.iflag = True

    ## STATUS
    def request_status(self):
        self.tccServer.request_pcs_status_info()

    ## PCS State
    def update_pcs_stateinfo(self):
        # data = [0x01, 5000, 0x00, 1, 0, 1, 0, 1, 0, 
        #         0x01, 0x01, 0x01, 0x01, 100, 
        #         0x01, 0x01, 0x01, 0x01, 0x01, 
        #         0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x02]
        # logging.debug("CanonMainWindow :: update_pcs_stateinfo")
        if self.tccServer.status_info:
            data = self.tccServer.status_info
            self.tccServer.status_info = None

            ## CBIT 전송 Flag
            self.cbitflag.change_status(data[0]-1)
            ## CBIT 전송 주기
            self.edit_cbit_period.setText(str(data[1]))
            ## IBIT 수행 상태
            self.ibitflag.change_status(data[2])

            ## 포탑 전원 조절기 버전정보
            self.edit_tcc_ver.setText(f"{data[3]}.{data[4]}")
            ## 포탑 승압기 버전정보
            self.edit_pcs_ver.setText(f"{data[5]}.{data[6]}")
            ## 차체 전원 조절기 버전정보
            self.edit_self_ver.setText(f"{data[7]}.{data[8]}")

            ## 비상정지 정보
            self.emstop.change_status(data[9])
            ## 고전원 출력상태
            self.highout.change_status(data[10])
            
            ## 2022 7 27 update
            ##################################
            ## 저전원 출력상태
            # self.lowout.change_status(data[11])
            self.tcc_lowpo1.change_status(data[11])
            self.tcc_lowpo2.change_status(data[12])
            self.tcc_lowpo3.change_status(data[13])
            self.tcc_lowpo4.change_status(data[14])
            self.tcc_lowpo5.change_status(data[15])
            self.tcc_lowpo6.change_status(data[16])
            self.tcc_lowpo7.change_status(data[17])
            self.tcc_lowpo8.change_status(data[18])

            ## 포신잠금장치 상태
            self.artlock.change_status(data[19])
            ## 해치 상태
            self.hatch.change_status(data[20])
            
            ##### 포탑전원조절기
            self.tcc_mod1.change_status(data[21])
            self.tcc_mod2.change_status(data[22])
            self.tcc_mod3.change_status(data[23])
            self.tcc_mod4.change_status(data[24])
            self.tcc_mod5.change_status(data[25])

            ##### 포탑승압기
            self.pcs_mod1.change_status(data[26])
            self.pcs_mod2.change_status(data[27])
            self.pcs_mod3.change_status(data[28])
            self.pcs_mod4.change_status(data[29])
            self.pcs_mod5.change_status(data[30])
            self.pcs_mod6.change_status(data[31])

            ##### 차체전원조절기
            self.device.change_status(data[32])

            ##### PCS 추가 정보 
            for idx, pf in enumerate(self.pfields):
                pf.set_value(data[idx+33])
                # logging.debug(f"{pf} :: {data[idx+33]}")


    ## EMERGENCY STOP
    def request_emergency_stop(self):
        if self.emergency_stop_flag:
            self.emergency_stop_flag = False
            self.btn_em_stop.setStyleSheet(CanonMainWindow.redfont)
            self.btn_em_stop.setText("비상 정지")
            data = [2]
            self.tccServer.emergency_stop(data)
        else:
            self.emergency_stop_flag = True 
            self.btn_em_stop.setStyleSheet(CanonMainWindow.cyanfont)
            self.btn_em_stop.setText("비상 정지 해제")
            data = [1]
            self.tccServer.emergency_stop(data)


    def displayUnitLayout(self, parent_layout, btn, cbx, label, unit):
        grp_sub = QGroupBox("")
        layout_sub = QGridLayout()
        grp_sub.setLayout(layout_sub)

        label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        unit.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)

        layout_sub.addWidget(btn, 0, 0, 1, 3)
        layout_sub.addWidget(cbx, 0, 4)
        layout_sub.addWidget(label, 1, 0, 1, 3)
        layout_sub.addWidget(unit, 1, 4)
        parent_layout.addWidget(grp_sub)


    def displayMenuLayout(self, parent_layout, glabel, btn, second=None):
        grp_sub = QGroupBox(glabel)
        layout_sub = QHBoxLayout()
        grp_sub.setLayout(layout_sub)
        layout_sub.addWidget(btn)
        if second:
            layout_sub.addWidget(second)
        parent_layout.addWidget(grp_sub)

    ## SCREEN LAYOUT
    def initUI(self):
        self.setWindowTitle("PSTEK CANON SYSTEM MONITORING")
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width()-50, screensize.height()) 

        mainwidget = QWidget()                # 위젯의 인스턴스 생성만으로도 QCanonMainWindow에 붙는다.
        self.setCentralWidget(mainwidget)

        self.main_layout = QGridLayout()
        mainwidget.setLayout(self.main_layout)

        ## Main Layout 설정 ########################
        self.displayConnect()
        self.displayRequest()
        self.displayResponse()
        self.displayPSTEKLogo()

        ## Q Tabs ########################
        tabs = QTabWidget()
        tabs.setStyleSheet(open('canon/style.css').read());

        mainControlTab = QWidget()
        subPcsInfoTab = QWidget()
        tabs.addTab(mainControlTab, 'MAIN CONTROL')
        tabs.addTab(subPcsInfoTab, 'PCS STATUS INFO')
        self.displayMainControl(mainControlTab)
        self.displaySubPCSInfo(subPcsInfoTab)

        tabCbit = QWidget()
        tab_device_cbit1 = QWidget()
        tab_device_cbit2 = QWidget()
        tabs.addTab(tabCbit, 'MAIN CBIT')
        tabs.addTab(tab_device_cbit1, 'CBIT 1')
        tabs.addTab(tab_device_cbit2, 'CBIT 2')
        
        tabPbit = QWidget()
        tab_device_pbit1 = QWidget()
        tab_device_pbit2 = QWidget()
        tabs.addTab(tabPbit, 'MAIN PBIT')
        tabs.addTab(tab_device_pbit1, 'PBIT 1')
        tabs.addTab(tab_device_pbit2, 'PBIT 2')

        tabIbit = QWidget()
        tab_device_ibit1 = QWidget()
        tab_device_ibit2 = QWidget()
        tabs.addTab(tabIbit, 'MAIN IBIT')
        tabs.addTab(tab_device_ibit1, 'IBIT 1')
        tabs.addTab(tab_device_ibit2, 'IBIT 2')
        
        self.displaySubModules1(tab_device_pbit1, self.pbit_fields, self.pbitsystems)
        self.displaySubModules2(tab_device_pbit2, self.pbit_fields, self.pbitsystems)
        self.displayMainSummary(tabPbit, self.pbitsystems)

        self.displaySubModules1(tab_device_cbit1, self.cbit_fields, self.cbitsystems)
        self.displaySubModules2(tab_device_cbit2, self.cbit_fields, self.cbitsystems)
        self.displayMainSummary(tabCbit, self.cbitsystems)

        self.displaySubModules1(tab_device_ibit1, self.ibit_fields, self.ibitsystems)
        self.displaySubModules2(tab_device_ibit2, self.ibit_fields, self.ibitsystems)
        self.displayMainSummary(tabIbit, self.ibitsystems)


        self.main_layout.addWidget(tabs, 2, 0, 1, 5)

        ## Status BAR ########################
        self.statusbar = QStatusBar()
        self.setStatusBar(self.statusbar)
        self.statusbar.setObjectName("statusbar")
        
        self.statusmessage = 'PSTEK, {},  {}'
        now = QDate.currentDate()
        displaymessage = self.statusmessage.format(now.toString(Qt.ISODate), 'Ready !!')
        self.statusbar.showMessage(displaymessage)

        self.show()

        # self.update_pcs_stateinfo()

    ## Main Display
    def displayMainSummary(self, tab, subsystems):
        main_layout = QGridLayout()
        tab.setLayout(main_layout)

        index = 0
        for subname, mods in subsystems.items():
            group = QGroupBox(subname)
            layout = QVBoxLayout()
            group.setLayout(layout)

            for mod in mods:
                layout.addWidget(mod.btn)

            if index == 4:
                main_layout.addWidget(group, 0, index)
            elif index == 5:
                main_layout.addWidget(group, 1, index-1)
            else:
                main_layout.addWidget(group, 0, index, 2, 1)
            index += 1

    def layoutStatus2(self, layout, row, col, edit, name, comment1, comment2=None):
        subgroup = QGroupBox(name)
        sublayout = QVBoxLayout()
        subgroup.setLayout(sublayout)

        # sublayout.addWidget(QLabel(name))
        sublayout.addWidget(edit)
        sublayout.addWidget(QLabel(comment1))
        if comment2:
            sublayout.addWidget(QLabel(comment2))

        layout.addWidget(subgroup, row, col)

    ## MAIN CONTROL
    # displayMainControl
    def displayMainControl(self, tab):
        main_layout = QHBoxLayout()
        tab.setLayout(main_layout)

        # Graph 
        grp_controller = self.displayController()
        main_layout.addWidget(grp_controller)

        grp_reset = self.displayReset()
        main_layout.addWidget(grp_reset)

        group = self.modulePcsStatusInfo()
        main_layout.addWidget(group)

    ## 1-1 포탑 전원 조절기
    def displayController(self):
        grp_controller = QGroupBox("포탑 전원 조절기")
        layout_controller = QGridLayout()
        grp_controller.setLayout(layout_controller)

        ## Sub 전원 통제 
        grp_onoff = QGroupBox("ON / OFF 선택")
        layout_onoff  = QHBoxLayout()
        grp_onoff.setLayout(layout_onoff)

        self.rd_power_on = QRadioButton("ON")
        self.rd_power_on.clicked.connect(lambda:self.radioBtnClicked("NORMAL", self.rd_power_on))
        self.rd_power_off = QRadioButton("OFF")
        self.rd_power_off.clicked.connect(lambda:self.radioBtnClicked("NORMAL", self.rd_power_off))
        layout_onoff.addWidget(self.rd_power_on)
        layout_onoff.addWidget(self.rd_power_off)
        layout_controller.addWidget(grp_onoff)

        ## 
        grp_part = QGroupBox("부분 선택")
        self.layout_parts  = QVBoxLayout()
        grp_part.setLayout(self.layout_parts)

        for part, name in PCS_PARTS.items():
            self.rd_btn = QRadioButton(name)
            self.layout_parts.addWidget(self.rd_btn)

        layout_controller.addWidget(grp_part)
        
        # Button 
        grp_btn = QGroupBox("")
        layout_btn  = QHBoxLayout()
        grp_btn.setLayout(layout_btn)

        self.btn_normalcmd = QPushButton("전송")
        self.btn_normalcmd.setEnabled(False)
        self.btn_normalcmd.clicked.connect(self.power_control_cmd)
        layout_btn.addWidget(self.btn_normalcmd)
        layout_controller.addWidget(grp_btn)

        # Table
        grp_msg = QGroupBox("")
        layout_msg  = QHBoxLayout()
        grp_msg.setLayout(layout_msg)

        
        self.tableMsg = QTableWidget()
        self.tableMsg.setColumnCount(1)
        self.tableMsg.setEditTriggers(QAbstractItemView.NoEditTriggers)

        column_headers = ['MESSAGE']
        self.tableMsg.setHorizontalHeaderLabels(column_headers)
        self.tableMsg.setRowCount(0)

        header = self.tableMsg.horizontalHeader()       
        header.setSectionResizeMode(0, QHeaderView.Stretch)

        layout_msg.addWidget(self.tableMsg)
        layout_controller.addWidget(grp_msg)

        return grp_controller

    ## 1-2 Reset/STop
    def displayReset(self):
        grp_sub = QGroupBox("Stop/Reset")
        layout_sub = QVBoxLayout()
        grp_sub.setLayout(layout_sub)

        ## 고전압 출력
        ## Sub 전원 통제 
        grp_high_onoff = QGroupBox("고전압 출력")
        layout_high_onoff  = QGridLayout()
        grp_high_onoff.setLayout(layout_high_onoff)

        self.rd_highpower_on = QRadioButton("ON")
        self.rd_highpower_on.clicked.connect(lambda:self.radioBtnClicked("HIGH", self.rd_highpower_on))
        self.rd_highpower_off = QRadioButton("OFF")
        self.rd_highpower_off.clicked.connect(lambda:self.radioBtnClicked("HIGH", self.rd_highpower_off))
        layout_high_onoff.addWidget(self.rd_highpower_on, 0, 0)
        layout_high_onoff.addWidget(self.rd_highpower_off, 0, 1)

        self.btn_highcmd = QPushButton("전송")
        self.btn_highcmd.setEnabled(False)
        self.btn_highcmd.clicked.connect(self.high_power_control_cmd)
        layout_high_onoff.addWidget(self.btn_highcmd, 1, 0, 1, 2)


        self.tableHighMsg = QTableWidget()
        self.tableHighMsg.setColumnCount(1)
        self.tableHighMsg.setEditTriggers(QAbstractItemView.NoEditTriggers)

        column_headers = ['MESSAGE']
        self.tableHighMsg.setHorizontalHeaderLabels(column_headers)
        self.tableHighMsg.setRowCount(0)

        header = self.tableHighMsg.horizontalHeader()       
        header.setSectionResizeMode(0, QHeaderView.Stretch)
        layout_high_onoff.addWidget(self.tableHighMsg, 2, 0, 1, 2)

        layout_sub.addWidget(grp_high_onoff)


        # RESET 준비 
        grp_em_stop = QGroupBox("비상 정지")
        layout_em_stop= QVBoxLayout()
        grp_em_stop.setLayout(layout_em_stop)

        self.btn_em_stop = CLed("비상 정지", 80, 80, "#ccc")
        self.btn_em_stop.setEnabled(False)
        self.btn_em_stop.clicked.connect(lambda:self.request_emergency_stop())

        layout_em_stop.addWidget(self.btn_em_stop)
        layout_sub.addWidget(grp_em_stop)


        # RESET 준비 
        grp_reset = QGroupBox("리셋")
        layout_reset= QVBoxLayout()
        grp_reset.setLayout(layout_reset)

        self.btn_fault = QPushButton("Fault 리셋")
        self.btn_fault.setEnabled(False)
        self.btn_fault.clicked.connect(lambda:self.request_pcs_reset([FAULT_RESET]))

        self.btn_cpu = QPushButton("CPU 리셋")
        self.btn_cpu.setEnabled(False)
        self.btn_cpu.clicked.connect(lambda:self.request_pcs_reset([CPU_RESET]))

        layout_reset.addWidget(self.btn_fault)
        layout_reset.addWidget(self.btn_cpu)
        layout_sub.addWidget(grp_reset)

        return grp_sub

    ## 1-3 PCSStatus
    def modulePcsStatusInfo(self):
        group = QGroupBox("PCS 상태 정보")
        layout = QGridLayout()
        group.setLayout(layout)

        ## 0 th
        self.cbitflag = Status("CBIT 전송 Flag", "전송상태", "미전송 상태")
        group_cbitflag = self.cbitflag.display()
        layout.addWidget(group_cbitflag, 0, 0)

        ## 1th
        self.edit_cbit_period = QLineEdit(str(self.cbit_period * 1000))
        period_name = '{0: <15}'.format("CBIT 전송 주기(m sec)")
        period_comment = '{0: <30}'.format("현재 CBIT 결과 전송주기 설정 값")
        self.layoutStatus2(layout, 0, 1, self.edit_cbit_period, period_name, 
                    period_comment)
        ## 2th
        self.ibitflag = Status("IBIT 수행 상태", "미수행(수행완료)", "수행중")
        group_ibitflag = self.ibitflag.display()
        layout.addWidget(group_ibitflag, 1, 0)

        ## 3 ~ 4th
        self.edit_tcc_ver = QLineEdit("0.1")
        self.layoutStatus2(layout, 1, 1, self.edit_tcc_ver, 
                "포탑 전원 조절기 버전정보", "Major 버전, Minor 버전")

        ## 5 ~ 6th
        self.edit_pcs_ver = QLineEdit("0.1")
        pcsver_name = '{0: <15}'.format("포탑 승압기 버전정보")
        pcsver_comment = '{0: <30}'.format("Major 버전, Minor 버전")
        self.layoutStatus2(layout, 2, 0, self.edit_pcs_ver, pcsver_name, 
                     pcsver_comment)

        ## 7 ~ 8th
        self.edit_self_ver = QLineEdit("0.1")
        self.layoutStatus2(layout, 2, 1, self.edit_self_ver, 
                "차체 전원 조절기 버전정보", "Major 버전, Minor 버전")

        ## 9 th
        self.emstop = Status("비상정지 정보", "비상정지 해제상태", 
                        "비상정지 상태(내부)", "비상정지 상태(외부)", "비상정지 상태(모두)")

        group_emstop = self.emstop.display()
        layout.addWidget(group_emstop, 3, 0)

        ## 10th
        self.highout = Status("고전원 출력상태", "미출력(방전완료)", 
                        "승압중", "출력(승압완료)", "방전중")
        group_highout = self.highout.display()
        layout.addWidget(group_highout, 3, 1)

        ## 2022 7 27 update
        ##################################
        ## 11th
        group_temp = QGroupBox("저전원 출력상태")
        layout.addWidget(group_temp, 4, 0)

        ###################################

        ## 12th
        self.artlock = Status("포신잠금장치 상태", "잠김", "풀림")
        group_artlock = self.artlock.display()
        layout.addWidget(group_artlock, 4, 1)

        self.hatch = Status("해치 상태", "닫힘", "열림")
        group_hatch = self.hatch.display()
        layout.addWidget(group_hatch, 5, 0)

        self.device = Status("차체전원조절기", "미사용(스위치 차단 상태)", 
                        "동작중(정상 운용가능 상태)", "정지(고장 등 비정상 상태)")
        group_device = self.device.display()
        layout.addWidget(group_device, 5, 1)

        self.subLowPowerPCSStatus(layout)
        self.subModulePCSStatus(layout)
        
        return group

    def subLowPowerPCSStatus(self, layout):
        subgroup = QGroupBox("저전원 출력상태")
        sublayout = QVBoxLayout()
        subgroup.setLayout(sublayout)

        self.tcc_lowpo1 = Status("포/포탑구동장치", "미출력", "켬 진행중", "출력", "끔 진행중")
        group_tcc_lowpo1 = self.tcc_lowpo1.display2row()
        sublayout.addWidget(group_tcc_lowpo1)

        self.tcc_lowpo2 = Status("포탄적재이송장치", "미출력", "켬 진행중", "출력", "끔 진행중")
        group_tcc_lowpo2 = self.tcc_lowpo2.display2row()
        sublayout.addWidget(group_tcc_lowpo2)

        self.tcc_lowpo3 = Status("장약적재이송장치", "미출력", "켬 진행중", "출력", "끔 진행중")
        group_tcc_lowpo3 = self.tcc_lowpo3.display2row()
        sublayout.addWidget(group_tcc_lowpo3)

        self.tcc_lowpo4 = Status("탄약장전장치", "미출력", "켬 진행중", "출력", "끔 진행중")
        group_tcc_lowpo4 = self.tcc_lowpo4.display2row()
        sublayout.addWidget(group_tcc_lowpo4)

        self.tcc_lowpo5 = Status("포미개폐장치", "미출력", "켬 진행중", "출력", "끔 진행중")
        group_tcc_lowpo5 = self.tcc_lowpo5.display2row()
        sublayout.addWidget(group_tcc_lowpo5)

        self.tcc_lowpo6 = Status("항법장치", "미출력", "켬 진행중", "출력", "끔 진행중")
        group_tcc_lowpo6 = self.tcc_lowpo6.display2row()
        sublayout.addWidget(group_tcc_lowpo6)

        self.tcc_lowpo7 = Status("자동시한장입장치", "미출력", "켬 진행중", "출력", "끔 진행중")
        group_tcc_lowpo7 = self.tcc_lowpo7.display2row()
        sublayout.addWidget(group_tcc_lowpo7)

        self.tcc_lowpo8 = Status("통신장치(무전기)", "미출력", "켬 진행중", "출력", "끔 진행중")
        group_tcc_lowpo8 = self.tcc_lowpo8.display2row()
        sublayout.addWidget(group_tcc_lowpo8)

        layout.addWidget(subgroup, 0, 2, 6, 1)

    def subModulePCSStatus(self, layout):
        subgroup = QGroupBox("포탑전원조절기")
        sublayout = QVBoxLayout()
        subgroup.setLayout(sublayout)

        self.tcc_mod1 = Status("모듈1상태", "미사용(스위치 차단 상태)", "동작중(정상 운용가능 상태)", "정지(고장 등 비정상 상태)")
        group_tcc_mod1 = self.tcc_mod1.display()
        sublayout.addWidget(group_tcc_mod1)

        self.tcc_mod2 = Status("모듈2상태", "미사용(스위치 차단 상태)", "동작중(정상 운용가능 상태)", "정지(고장 등 비정상 상태)")
        group_tcc_mod2 = self.tcc_mod2.display()
        sublayout.addWidget(group_tcc_mod2)

        self.tcc_mod3 = Status("모듈3상태", "미사용(스위치 차단 상태)", "동작중(정상 운용가능 상태)", "정지(고장 등 비정상 상태)")
        group_tcc_mod3 = self.tcc_mod3.display()
        sublayout.addWidget(group_tcc_mod3)

        self.tcc_mod4 = Status("모듈4상태", "미사용(스위치 차단 상태)", "동작중(정상 운용가능 상태)", "정지(고장 등 비정상 상태)")
        group_tcc_mod4 = self.tcc_mod4.display()
        sublayout.addWidget(group_tcc_mod4)

        self.tcc_mod5 = Status("모듈5상태", "미사용(스위치 차단 상태)", "동작중(정상 운용가능 상태)", "정지(고장 등 비정상 상태)")
        group_tcc_mod5 = self.tcc_mod5.display()
        sublayout.addWidget(group_tcc_mod5)

        layout.addWidget(subgroup, 0, 3, 6, 1)

        subgroup = QGroupBox("포탑승압기")
        sublayout = QVBoxLayout()
        subgroup.setLayout(sublayout)

        self.pcs_mod1 = Status("모듈1상태", "미사용(스위치 차단 상태)", "동작중(정상 운용가능 상태)", "정지(고장 등 비정상 상태)")
        group_pcs_mod1 = self.pcs_mod1.display()
        sublayout.addWidget(group_pcs_mod1)

        self.pcs_mod2 = Status("모듈2상태", "미사용(스위치 차단 상태)", "동작중(정상 운용가능 상태)", "정지(고장 등 비정상 상태)")
        group_pcs_mod2 = self.pcs_mod2.display()
        sublayout.addWidget(group_pcs_mod2)

        self.pcs_mod3 = Status("모듈3상태", "미사용(스위치 차단 상태)", "동작중(정상 운용가능 상태)", "정지(고장 등 비정상 상태)")
        group_pcs_mod3 = self.pcs_mod3.display()
        sublayout.addWidget(group_pcs_mod3)

        self.pcs_mod4 = Status("모듈4상태", "미사용(스위치 차단 상태)", "동작중(정상 운용가능 상태)", "정지(고장 등 비정상 상태)")
        group_pcs_mod4 = self.pcs_mod4.display()
        sublayout.addWidget(group_pcs_mod4)

        self.pcs_mod5 = Status("모듈5상태", "미사용(스위치 차단 상태)", "동작중(정상 운용가능 상태)", "정지(고장 등 비정상 상태)")
        group_pcs_mod5 = self.pcs_mod5.display()
        sublayout.addWidget(group_pcs_mod5)

        self.pcs_mod6 = Status("모듈6상태", "미사용(스위치 차단 상태)", "동작중(정상 운용가능 상태)", "정지(고장 등 비정상 상태)")
        group_pcs_mod6 = self.pcs_mod6.display()
        sublayout.addWidget(group_pcs_mod6)

        layout.addWidget(subgroup, 0, 4, 6, 1)


    def displaySubPCSInfo(self, tab):
        main_layout = QGridLayout()
        tab.setLayout(main_layout)

        mods = []

        for submod, pfields in PCSSTATUS.items():
            pmod = PModule(submod)
            mods.append(pmod)

            for pfield in pfields:
                field = PField(pmod, pfield['name'], pfield['divide'], pfield['unit'])
                # print(field)
                pmod.fields.append(field)
                self.pfields.append(field)

        for idx, mod in enumerate(mods):
            group = mod.display()
            if idx == 0:
                main_layout.addWidget(group, 0, 0, 2, 1)
            else:
                quote = (idx + 1) // 2 
                remain = (idx + 1) % 2

                if remain == 0:
                    main_layout.addWidget(group, 0, quote)
                elif remain == 1:
                    main_layout.addWidget(group, 1, quote)
                

    # displaySubModules
    def displaySubModules1(self, tab, field_table, subsystems):
        main_layout = QHBoxLayout()
        tab.setLayout(main_layout)

        subsysnum = 0
        self.bytenum = 0
        self.nth = 0
        self.rowfields = []
        index = 1

        ## Layout 을 위한 모듈 
        for subsys, submods in NAMES.items():
            
            subsystem = SubSystem(subsys)
            mods = []

            ## Sub Module 
            for sub in submods:
                
                for subname, funs in sub.items():

                    mod = Module(subname)
                    mods.append(mod)

                    ## Function 단위 
                    for fun in funs:
                        # logging.debug(f"### :: {self.bytenum} :: {self.nth} :: {subsys} :: {subname} :: {fun} ")
                        field = Field(mod, fun, self.bytenum, self.nth, 0)
                        self.rowfields.append(field)
                        mod.add_field(field)
                        # print("mod 1 :: index = ", index, field)
                        # index += 1

                        if self.nth == 3:
                            field_table.append(self.rowfields)
                            self.rowfields = []
                            self.bytenum += 1
                            self.nth = 0
                        else:
                            self.nth += 1

                    subsystem.add_module(mod)
            subsystems[subsys] = mods

            subsysnum += 1

            group = subsystem.display()
            main_layout.addWidget(group)

            if subsysnum == 5:
                break

    # displaySubModules
    def displaySubModules2(self, tab, field_table, subsystems):
        main_layout = QGridLayout()
        tab.setLayout(main_layout)

        subsysnum = 0
        columns = 0
        index = 1

        ## Layout 을 위한 모듈 
        for subsys, submods in NAMES.items():
            if subsysnum < 5:
                subsysnum += 1
                continue

            subsystem = SubSystem(subsys)
            mods = []

            ## Sub Module 
            for sub in submods:
                for subname, funs in sub.items():

                    mod = Module(subname)
                    mods.append(mod)

                    ## Function 단위 
                    for fun in funs:
                        field = Field(mod, fun, self.bytenum, self.nth, 0)
                        self.rowfields.append(field)
                        mod.add_field(field)
                        # print("mod 2 :: index = ", index, field)
                        # index += 1

                        if self.nth == 3:
                            field_table.append(self.rowfields)
                            self.rowfields = []
                            self.bytenum += 1
                            self.nth = 0
                        else:
                            self.nth += 1
                    
                    subsystem.add_module(mod)
            subsystems[subsys] = mods
            subsysnum += 1
            columns += 1

            group = subsystem.display()
            if columns == 5:
                main_layout.addWidget(group, 0, columns)
                # print(group, 0, columns)
            elif columns == 6:
                main_layout.addWidget(group, 1, columns-1)
                # print(group, 1, columns-1)
            else:
                main_layout.addWidget(group, 0, columns, 2, 1)
                # print(group, 0, columns, 2, 1)

        pfirstmods = []
        psecondmods = []
        pthirdmods = []
        pfirstname = None
        psecondname = None
        pthirdname = None

        sfirstmods = []
        ssecondmods = []
        sthirdmods = []
        sfirstname = None
        ssecondname = None
        sthirdname = None

        lfirstmods = []
        lsecondmods = []
        lfirstname = None
        lsecondname = None

        for name, mods in subsystems.items():
            if name == '포탑전원조절기 1':
                pfirstmods = mods
                pfirstname = name

            if name == '포탑전원조절기 2':
                psecondmods = mods
                psecondname = name

            if name == '포탑전원조절기 3':
                pthirdmods = mods
                pthirdname = name

            if name == '포탑승압기 1':
                sfirstmods = mods
                sfirstname = name

            if name == '포탑승압기 2':
                ssecondmods = mods
                ssecondname = name

            if name == '포탑승압기 3':
                sthirdmods = mods
                sthirdname = name

            if name == '연결/통신 포탑전원조절기 타구성품 연동 1':
                lfirstmods = mods
                lfirstname = name

            if name == '연결/통신 포탑전원조절기 타구성품 연동 2':
                lsecondmods = mods
                lsecondname = name

        
        pfirstmods = pfirstmods + psecondmods + pthirdmods
        subsystems[pfirstname] = pfirstmods
        del subsystems[psecondname]
        del subsystems[pthirdname]

        sfirstmods = sfirstmods + ssecondmods + sthirdmods
        subsystems[sfirstname] = sfirstmods
        del subsystems[ssecondname]
        del subsystems[sthirdname]

        lfirstmods = lfirstmods + lsecondmods
        subsystems[lfirstname] = lfirstmods
        del subsystems[lsecondname]


    ## Menu Connect
    def displayConnect(self):
        grp_conn = QGroupBox("연결 확인")
        layout_conn = QGridLayout()
        grp_conn.setLayout(layout_conn)

        lbl_address = QLabel("주 소(IP)")
        lbl_port = QLabel("포트(Port)")
        lbl_cbitperiod = QLabel("CBIT 전송주기")
        lbl_ibitperiod = QLabel("IBIT 전송주기")

        self.edit_address = QLineEdit(TCC_HOST_IP)
        self.edit_port = QLineEdit(str(TCC_HOST_PORT))
        self.edit_cbitperiod = QLineEdit(str(self.cbit_period * 1000))
        self.edit_ibitperiod = QLineEdit(str(self.ibit_period * 1000))

        self.btn_boot = CLed("BOOT", 50, 50, "#ccc")
        self.btn_boot.setStyleSheet(CanonMainWindow.cyanfont)
        self.btn_boot.clicked.connect(self.run_pcs)

        self.btn_pbit = CLed("PBIT", 50, 50, "#ccc")
        self.btn_pbit.setEnabled(False)
        self.btn_pbit.clicked.connect(self.request_pbit)

        self.btn_cbit = CLed("CBIT", 50, 50, "#ccc")
        self.btn_cbit.setEnabled(False)
        self.btn_cbit.clicked.connect(self.request_cbit)

        self.btn_ibit = CLed("iBIT", 50, 50, "#ccc")
        self.btn_ibit.setEnabled(False)
        self.btn_ibit.clicked.connect(self.request_ibit)

        self.btn_cbitperiod = QPushButton("CBIT 주기 전송")
        self.btn_cbitperiod.setEnabled(False)
        self.btn_cbitperiod.setStyleSheet(CanonMainWindow.grayfont)
        self.btn_cbitperiod.clicked.connect(self.setup_cbitperiod)

        self.btn_ibitperiod = QPushButton("IBIT 주기 전송")
        self.btn_ibitperiod.setEnabled(False)
        self.btn_ibitperiod.setStyleSheet(CanonMainWindow.grayfont)
        self.btn_ibitperiod.clicked.connect(self.setup_ibitime)

        layout_conn.addWidget(lbl_address, 0, 0)
        layout_conn.addWidget(self.edit_address, 0, 1)

        layout_conn.addWidget(self.btn_boot, 0, 2, 2, 1)
        layout_conn.addWidget(self.btn_pbit, 0, 3, 2, 1)
        layout_conn.addWidget(self.btn_cbit, 0, 4, 2, 1)

        layout_conn.addWidget(lbl_port, 1, 0)
        layout_conn.addWidget(self.edit_port, 1, 1)

        layout_conn.addWidget(lbl_cbitperiod, 2, 0)
        layout_conn.addWidget(self.edit_cbitperiod, 2, 1)
        layout_conn.addWidget(self.btn_cbitperiod, 2, 2, 1, 2)

        layout_conn.addWidget(lbl_ibitperiod, 3, 0)
        layout_conn.addWidget(self.edit_ibitperiod, 3, 1)
        layout_conn.addWidget(self.btn_ibitperiod, 3, 2, 1, 2)
        layout_conn.addWidget(self.btn_ibit, 3, 4)

        self.main_layout.addWidget(grp_conn, 0, 0, 2, 1)

    ## Menu Request
    def displayRequest(self):
        grp_command = QGroupBox("명령메시지")
        layout_command = QVBoxLayout()
        grp_command.setLayout(layout_command)

        ## display Table
        self.tableQ = QTableWidget()
        self.tableQ.setColumnCount(6)
        self.tableQ.setEditTriggers(QAbstractItemView.NoEditTriggers)

        column_headers = ['SID', 'DID', 'CODE', 'ACK', 'DATA LEN', 'DATA']
        self.tableQ.setHorizontalHeaderLabels(column_headers)
        self.tableQ.setRowCount(0)

        header = self.tableQ.horizontalHeader()       
        header.setSectionResizeMode(0, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(1, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(2, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(2, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(4, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(5, QHeaderView.Stretch)

        # layout_command.addWidget(self.edit_request)
        layout_command.addWidget(self.tableQ)
        self.main_layout.addWidget(grp_command, 0, 1, 2, 1)

    ## Display Request Info
    @pyqtSlot(str)
    def updateRequest(self, msg):
        msgBox = msg.split(':')
        rowPosition = self.tableQ.rowCount()
        code = TCC_CODE.get(msgBox[2], 'NOT REGISTED OR RESPONSE')
        self.tableQ.insertRow(rowPosition)
        self.tableQ.setItem(rowPosition, 0, QTableWidgetItem(msgBox[0]))
        self.tableQ.setItem(rowPosition, 1, QTableWidgetItem(msgBox[1]))
        self.tableQ.setItem(rowPosition, 2, QTableWidgetItem(code))
        self.tableQ.setItem(rowPosition, 3, QTableWidgetItem(msgBox[3]))
        self.tableQ.setItem(rowPosition, 4, QTableWidgetItem(msgBox[4]))
        self.tableQ.setItem(rowPosition, 5, QTableWidgetItem(msgBox[5]))

        if int(msgBox[4]):
            rowPosition = self.tableQ.rowCount()
            self.tableQ.insertRow(rowPosition)
            self.tableQ.setItem(rowPosition, 1, QTableWidgetItem("PAYLOAD"))
            self.tableQ.setItem(rowPosition, 2, QTableWidgetItem(msgBox[5]))

        self.tableQ.resizeColumnsToContents()
        self.tableQ.resizeRowsToContents()
        self.tableQ.selectRow(rowPosition)

    ## Menu Response
    def displayResponse(self):
        grp_command = QGroupBox("응답메시지")
        layout_command = QVBoxLayout()
        grp_command.setLayout(layout_command)

        ## display Table
        self.tableRes = QTableWidget()
        self.tableRes.setColumnCount(6)
        self.tableRes.setEditTriggers(QAbstractItemView.NoEditTriggers)

        column_headers = ['SID', 'DID', 'CODE', 'ACK', 'DATA LEN', 'DATA']
        self.tableRes.setHorizontalHeaderLabels(column_headers)
        self.tableRes.setRowCount(0)

        header = self.tableRes.horizontalHeader()       
        header.setSectionResizeMode(0, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(1, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(2, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(3, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(4, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(5, QHeaderView.Stretch)

        layout_command.addWidget(self.tableRes)
        self.main_layout.addWidget(grp_command, 0, 2, 2, 1)

    ## Display Response Result
    @pyqtSlot(str)
    def updateResponse(self, msg):
        msgBox = msg.split(':')
        # logging.debug(f"updateResponse :: {msg} : {type(msgBox[2])}")
        rowPosition = self.tableRes.rowCount()
        code = PCS_CODE.get(msgBox[2], 'NOT REGISTED OR RESPONSE')
        self.tableRes.insertRow(rowPosition)
        self.tableRes.setItem(rowPosition, 0, QTableWidgetItem(msgBox[0]))
        self.tableRes.setItem(rowPosition, 1, QTableWidgetItem(msgBox[1]))
        self.tableRes.setItem(rowPosition, 2, QTableWidgetItem(code))
        self.tableRes.setItem(rowPosition, 3, QTableWidgetItem(msgBox[3]))
        self.tableRes.setItem(rowPosition, 4, QTableWidgetItem(msgBox[4]))
        self.tableRes.setItem(rowPosition, 5, QTableWidgetItem(msgBox[5]))

        if int(msgBox[4]):
            rowPosition = self.tableRes.rowCount()
            self.tableRes.insertRow(rowPosition)
            self.tableRes.setItem(rowPosition, 1, QTableWidgetItem("PAYLOAD"))
            self.tableRes.setItem(rowPosition, 2, QTableWidgetItem(msgBox[5]))

        self.tableRes.resizeColumnsToContents()
        self.tableRes.resizeRowsToContents()
        self.tableRes.selectRow(rowPosition)


    ## Display Response Result
    @pyqtSlot(str)
    def updateNormalMessage(self, msg):
        rowPosition = self.tableMsg.rowCount()
        self.tableMsg.insertRow(rowPosition)
        self.tableMsg.setItem(rowPosition, 0, QTableWidgetItem(msg))

        self.tableMsg.resizeColumnsToContents()
        self.tableMsg.resizeRowsToContents()
        self.tableMsg.selectRow(rowPosition)

    ## Display Response Result
    @pyqtSlot(str)
    def updateHighMessage(self, msg):
        rowPosition = self.tableHighMsg.rowCount()
        self.tableHighMsg.insertRow(rowPosition)
        self.tableHighMsg.setItem(rowPosition, 0, QTableWidgetItem(msg))

        self.tableHighMsg.resizeColumnsToContents()
        self.tableHighMsg.resizeRowsToContents()
        self.tableHighMsg.selectRow(rowPosition)

    ## LOGO
    def displayPSTEKLogo(self):
        # Logo Image
        labelLogo = QLabel("")
        pixmap = QPixmap(resource_path("logo.png"))
        labelLogo.setAlignment(Qt.AlignRight)
        labelLogo.setPixmap(pixmap)
        self.main_layout.addWidget(labelLogo, 0, 4)

        
    def radioBtnClicked(self, ptype, btn):
        result = btn.text()
        logging.debug(f"radioBtnClicked :: {ptype} :: {result}")
        if ptype == "HIGH":
            if result == 'ON':
                self.highonoff = ON
            else:
                self.highonoff = OFF

            if self.highonoff:
                self.btn_highcmd.setEnabled(True)

        elif ptype == "NORMAL":
            if result == 'ON':
                self.onoff = ON
            else:
                self.onoff = OFF

            if self.onoff:
                self.btn_normalcmd.setEnabled(True)

    def LayoutRadioBoxGrid(self, layout_grid, row, label, rdb1, rdb2, btn=None):
        layout_grid.addWidget(MyLabel(label), row, 0)
        layout_grid.addWidget(rdb1, row, 1)
        layout_grid.addWidget(rdb2, row, 2)
        if btn:
            layout_grid.addWidget(btn, row, 3)

    def LayoutDropCombBoxGrid(self, layout_grid, row, label, comb, btn=None):
        layout_grid.addWidget(MyLabel(label), row, 0)
        layout_grid.addWidget(comb, row, 1)
        if btn:
            layout_grid.addWidget(btn, row, 2)

        if isinstance(comb, QSpinBox):
            comb.setRange(0, 1000)
            comb.setValue(0)

    # Keyboard Event
    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Escape:
            pass
        elif e.key() == Qt.Key_F:
            self.showFullScreen()
        elif e.key() == Qt.Key_N:
            self.showNormal()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    form = CanonMainWindow()
    sys.exit(app.exec_())