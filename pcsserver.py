import socket 
import struct
import random
from ctypes import c_int16

from canon.mcode2 import *

from canon.udptpchost import UDPTPCHost

import logging
FORMAT = ('%(module)-15s:%(lineno)-8s:%(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.INFO)

if __name__ == '__main__':
    bootClient = UDPTPCHost(TCC_HOST_IP, TCC_HOST_PORT)
    bootClient.loop_pcs_boot_signal()


    # pcsServer = UDPServer(None, TCC_HOST_IP, TCC_HOST_PORT)


# ## Main Loop
# with socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM) as pcsServerSocket:
#     pcsServerSocket.bind((localIP, localPort)) 

#     ## PCS_TCC_BOOT_DONE_UPD 
#     # 데이터그램 소켓을 생성 
#     with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as pcsBootSocket:

#         while True:
#             # TCC 가 켜질때까지 connect 시도 
#             try:
#                 pcsBootSocket.connect((TCC_IP, TCC_PORT))
#                 break
#             except Exception as e:
#                 print("$$$$", "ERROR CONNECTION", TCC_IP, TCC_PORT, e)

#         # SEND :: 0x50, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x53
#         bootData = bytearray([0x50, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x53])
        
#         result = pcsBootSocket.send(bootData)
#         print("^"*5, bootData, result)
        
#         try:
#             bytesAddressPair = pcsBootSocket.recvfrom(BUFFERSIZE)
#             data = bytesAddressPair[0] 
#             address = bytesAddressPair[1]

#             bootReturnData = list(data)
#             print(data, len(data), bootReturnData, address)
#             print("PCS_TCC_BOOT_DONE_UPD_ACK 수신 완료 !!!! \n\n")
#         except Exception as e:
#             pass
#             # print("Time out :: ", e)
                
            

#     # 데이터그램 소켓을 생성 
#     # 주소와 IP로 Bind 
#     print("#"*5, "PCS server up and listening", "#"*5) 

#     # 들어오는 데이터그램 Listen 
#     while True:
#         bytesAddressPair = pcsServerSocket.recvfrom(BUFFERSIZE)
#         print(bytesAddressPair, type(bytesAddressPair))
#         data = bytesAddressPair[0] 
#         address = bytesAddressPair[1]

#         receiveData = list()
#         for i in range(0, 3):
#             receiveData.append(c_int16(data[i * 2] + (data[i * 2 + 1] << 8)).value )

#         print(data, len(data), receiveData, address)


#         my_list = []
#         for i in range(3):
#             my_list.append(random.randint(-2500, 2500))
        
#         bytesToSend = struct.pack("<"+"h"*len(my_list),*my_list)
#         pcsServerSocket.sendto(bytesToSend, address)
#         print(my_list, bytesToSend, " :: Return \n\n")

