import re
import serial
import logging
from binascii import hexlify

FORWARD = 0
REVERSE = 1

def scpi_command_2ch(commtype, val1=0, val2=0):
    # logging.info(f"scpi_command_2ch :: {commtype} :: {val1}")

    ## CONFIGURE READ
    if commtype == 'CONF_VOLTAGE_1':
        command = 'CONF:VOLT? (@1)\r\n'
    elif commtype == 'CONF_VOLTAGE_2':
        command = 'CONF:VOLT? (@2)\r\n'
    elif commtype == 'CONF_VOLTAGE_ALL':
        command = 'CONF:VOLT? (@1,2)\r\n'

    elif commtype == 'CONF_CURRENT_1':
        command = 'CONF:CURR? (@1)\r\n'
    elif commtype == 'CONF_CURRENT_2':
        command = 'CONF:CURR? (@2)\r\n'
    elif commtype == 'CONF_CURRENT_ALL':
        command = 'CONF:CURR? (@1,2)\r\n'

    elif commtype == 'CONF_RAMP_UP':
        command = 'CONF:RAMP? UP\r\n'
    elif commtype == 'CONF_RAMP_DOWN':
        command = 'CONF:RAMP? DOWN\r\n'

    elif commtype == 'CONF_TOGGLE':
        command = 'AT?\r\n'

    elif commtype == 'CONF_TOGGLE_COUNT':
        command = 'AT:COUN?\r\n'

    elif commtype == 'CONF_TOGGLE_VOLTAGE':
        command = 'AT:VOLT?\r\n'

    elif commtype == 'CONF_DEVICE_STATUS':
        command = 'OUTPUT?\r\n'



    ## CONFIGURE WRITE
    elif commtype == 'SET_VOLTAGE_1':
        command = f'CONF:VOLT {val1},{val2}\r\n'
    elif commtype == 'SET_VOLTAGE_2':
        command = f'CONF:VOLT {val1},{val2}\r\n'

    elif commtype == 'SET_CURRENT_1':
        command = f'CONF:CURR {val1},{val2}\r\n'
    elif commtype == 'SET_CURRENT_2':
        command = f'CONF:CURR {val1},{val2}\r\n'

    elif commtype == 'SET_RAMP_UP':
        command = f'CONF:RAMP UP,{val1}\r\n'
    elif commtype == 'SET_RAMP_DOWN':
        command = f'CONF:RAMP DOWN,{val1}\r\n'

    elif commtype == 'SET_TOGGLE_FWD':
        command = f'TOGGLE ON\r\n'
    elif commtype == 'SET_TOGGLE_BACK':
        command = f'TOGGLE OFF\r\n'

    elif commtype == 'SET_TOGGLE_COUNT':
        command = f'AT:COUNT {val1}\r\n'

    elif commtype == 'SET_TOGGLE_VOLTAGE':
        command = f'AT:VOLT {val1}\r\n'

    ## 동장중 변경 
    elif commtype == 'SOURCE_VOLTAGE_1':
        command = f'VOLT {val1},{val2}\r\n'
    elif commtype == 'SOURCE_VOLTAGE_2':
        command = f'VOLT {val1},{val2}\r\n'

    elif commtype == 'SOURCE_CURRENT_1':
        command = f'CURR {val1},{val2}\r\n'
    elif commtype == 'SOURCE_CURRENT_2':
        command = f'CURR {val1},{val2}\r\n'

    elif commtype == 'SOURCE_RAMP_UP':
        command = f'RAMP UP,{val1}\r\n'
    elif commtype == 'SOURCE_RAMP_DOWN':
        command = f'RAMP DOWN,{val1}\r\n'

    elif commtype == 'SOURCE_TOGGLE_FWD':
        command = f'TOGGLE ON\r\n'
    elif commtype == 'SOURCE_TOGGLE_BACK':
        command = f'TOGGLE OFF\r\n'

    elif commtype == 'SOURCE_TOGGLE_COUNT':
        command = f'AT:COUNT {val1}\r\n'

    elif commtype == 'SOURCE_TOGGLE_VOLTAGE':
        command = f'AT:VOLT {val1}\r\n'

    ## READ VALUE
    elif commtype == 'ALL_DATA':
        command = f'STAT?\r\n'
    elif commtype == 'READ_VOLTAGE':
        command = f'Meas:volt? (@1,2)\r\n'
    elif commtype == 'READ_CURRENT':
        command = f'Meas:curr? (@1,2)\r\n'
    elif commtype == 'READ_LEAKAGE':
        command = f'Meas:leak? (@1,2)\r\n'
    elif commtype == 'READ_CAPACITOR':
        command = 'TAR:CAP?\r\n'


    elif commtype == 'TOGGLE_ON':
        command = 'TOGGLE ON\r\n'
    elif commtype == 'TOGGLE_OFF':
        command = 'TOGGLE OFF\r\n'

    elif commtype == 'RUN':
        command = 'OUTPUT ON\r\n'
    elif commtype == 'STOP':
        command = 'OUTPUT OFF\r\n'
    else:
        command = f'Meas:volt? (@1,2)\r\n'
    return command.encode()


def scpi_get_measure_2ch(client, command):
    client.write(command)
    while True:
        try:
            feedback = client.readline()
            if feedback:
                # logging.info(f"scpi_get_measure :: {command} :: {feedback}")
                bitestr = feedback.decode('utf-8')
                try:
                    return re.findall('[-0-9]+', str(bitestr))
                except Exception as e:
                    return 0
                break
        except KeyboardInterrupt:
            feedback = None
            break
    return 0


def read_configure(client):
    data = {}

    command = scpi_command_2ch('CONF_VOLTAGE_ALL')
    result = scpi_get_measure_2ch(client, command)
    try:
        data['VOL_CH1'] = int(result[0])
        data['VOL_CH2'] = int(result[1])
    except Exception as e:
        data['VOL_CH1'] = 0
        data['VOL_CH2'] = 0

    command = scpi_command_2ch('CONF_CURRENT_ALL')
    result = scpi_get_measure_2ch(client, command)
    try:
        data['CUR_CH1'] = int(result[0])
        data['CUR_CH2'] = int(result[1])
    except Exception as e:
        data['CUR_CH1'] = 0
        data['CUR_CH2'] = 0

    command = scpi_command_2ch('CONF_RAMP_UP')
    result = scpi_get_measure_2ch(client, command)
    try:
        data['RAMP_UP'] = int(result[0])
    except Exception as e:
        data['RAMP_UP'] = 0

    command = scpi_command_2ch('CONF_RAMP_DOWN')
    result = scpi_get_measure_2ch(client, command)
    try:
        data['RAMP_DOWN'] = int(result[0])
    except Exception as e:
        data['RAMP_DOWN'] = 0

    # command = scpi_command_2ch('CONF_CAPACITOR')
    # result = scpi_get_measure_2ch(client, command)
    # try:
    #     data['CAPACITOR'] = int(result[0])
    # except Exception as e:
    #     data['CAPACITOR'] = 0

    command = scpi_command_2ch('CONF_TOGGLE')
    result = scpi_get_measure_2ch(client, command)
    try:
        data['TOGGLE'] = int(result[0])
    except Exception as e:
        data['TOGGLE'] = 0

    command = scpi_command_2ch('CONF_TOGGLE_COUNT')
    result = scpi_get_measure_2ch(client, command)
    try:
        data['TOGGLE_COUNT'] = int(result[0])
    except Exception as e:
        data['TOGGLE_COUNT'] = 0

    command = scpi_command_2ch('CONF_TOGGLE_VOLTAGE')
    result = scpi_get_measure_2ch(client, command)
    try:
        data['TOGGLE_VOLTAGE'] = int(result[0])
    except Exception as e:
        data['TOGGLE_VOLTAGE'] = 0

    return data