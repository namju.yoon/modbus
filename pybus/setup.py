import sys
import serial
import logging
from PyQt5.QtWidgets import QSpinBox, QLabel, QLineEdit, QDesktopWidget, QGridLayout
from PyQt5.QtWidgets import QGroupBox, QVBoxLayout, QHBoxLayout, QDialogButtonBox
from PyQt5.QtWidgets import QApplication, QDialog, QRadioButton, QPushButton
from PyQt5 import QtCore
from PyQt5.QtCore import  Qt
from PyQt5.QtGui import QFont


GRAPH = 1
DATA = 2


def check_stopbit(x): 
    return {'7 Bit': 7, '8 Bit': 8, '1 Bit': 1, '2 Bit': 2, '9600': 9600, '19200': 19200, '38400': 38400, '57600':57600, '115200': 115200, 'NONE': serial.PARITY_NONE, 'ODD': serial.PARITY_ODD, 'EVEN': serial.PARITY_EVEN, 'MARK': serial.PARITY_MARK, 'SPACE': serial.PARITY_SPACE,}[x]


def get_comm_port():
    port_list = []
    import serial.tools.list_ports
    ports = serial.tools.list_ports.comports()
    for port, desc, hwid in sorted(ports):
        if desc != 'n/a':
            port_list.append(port)
    return port_list


class SettingWin(QDialog):
    def __init__(self, Dialog):
        super().__init__()
        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        self.setGeometry(100, 100, 400, 500)
        font = QFont()
        font.setPointSize(12)

        self.channelnum = False
        self.ndp_flag = False
        self.selected = None
        self.ipaddress = None 
        self.ipport = None 

        self.gen_port = None
        self.com_speed = 115200
        self.com_data = 8
        self.com_parity = serial.PARITY_NONE
        self.com_stop = 1

        # Main 
        main_layer = QVBoxLayout()
        self.setLayout(main_layer)

        # 선택 TCP or RTU
        self.grp_select = QGroupBox("RTU or SCPI or TCP 선택")
        layout_select = QHBoxLayout()

        self.rd_rtu = QRadioButton("RTU")
        self.rd_rtu.setChecked(True)
        self.selected = 'RTU'
        self.rd_rtu.clicked.connect(lambda:self.radioSelectClicked(self.rd_rtu))
        layout_select.addWidget(self.rd_rtu)

        self.rd_scpi = QRadioButton("SCPI")
        self.rd_scpi.clicked.connect(lambda:self.radioSelectClicked(self.rd_scpi))
        layout_select.addWidget(self.rd_scpi)

        self.rd_semes = QRadioButton("SEMES")
        self.rd_semes.clicked.connect(lambda:self.radioSelectClicked(self.rd_semes))
        layout_select.addWidget(self.rd_semes)

        self.rd_tcp = QRadioButton("TCP")
        self.rd_tcp.clicked.connect(lambda:self.radioSelectClicked(self.rd_tcp))
        layout_select.addWidget(self.rd_tcp)

        self.grp_select.setLayout(layout_select)
        main_layer.addWidget(self.grp_select)


        ## 1Ch, 2Ch 선택 
        self.grp_chtype = QGroupBox("MONO/BIPOLAR 선택")
        layout_chtype = QHBoxLayout()

        self.rd_mono = QRadioButton("MONOPOLAR")
        self.rd_mono.clicked.connect(lambda:self.radioSelectClicked(self.rd_mono))
        self.rd_mono.setChecked(True)
        self.ch_ptype = False

        layout_chtype.addWidget(self.rd_mono)
        self.rd_bipolar = QRadioButton("BIPOLAR")
        self.rd_bipolar.clicked.connect(lambda:self.radioSelectClicked(self.rd_bipolar))
        layout_chtype.addWidget(self.rd_bipolar)
        self.grp_chtype.setLayout(layout_chtype)
        main_layer.addWidget(self.grp_chtype)


        # IP Address 
        self.grp_ipaddress = QGroupBox("IP ADDRESS")
        layout_ipaddress = QHBoxLayout()
        self.edit_ipaddress = QLineEdit("192.168.10.1")
        layout_ipaddress.addWidget(self.edit_ipaddress)
        self.grp_ipaddress.setLayout(layout_ipaddress)
        main_layer.addWidget(self.grp_ipaddress)
        self.grp_ipaddress.hide()

        # IP Port 
        self.grp_ipport = QGroupBox("Port")
        layout_ipport = QHBoxLayout()
        self.edit_ipport = QLineEdit("5000")
        layout_ipport.addWidget(self.edit_ipport)
        self.grp_ipport.setLayout(layout_ipport)
        main_layer.addWidget(self.grp_ipport)
        self.grp_ipport.hide()


        # 통신 포트  설정(Serial Port)"
        self.grp_port = QGroupBox("통신 포트  설정(Serial Port)")
        layout_port = QHBoxLayout()

        ports = get_comm_port()
        if ports:
            if len(ports) == 1:
                port = ports[0]
                self.rd_port_1 = QRadioButton(port)
                self.rd_port_1.setChecked(True)
                self.gen_port = port
                self.rd_port_1.clicked.connect(lambda:self.radioPortClicked(self.rd_port_1))
                layout_port.addWidget(self.rd_port_1)
            elif len(ports) == 2:
                port = ports[0]
                self.rd_port_1 = QRadioButton(port)
                self.rd_port_1.setChecked(True)
                self.gen_port = port
                self.rd_port_1.clicked.connect(lambda:self.radioPortClicked(self.rd_port_1))
                layout_port.addWidget(self.rd_port_1)

                port = ports[1]
                self.rd_port_2 = QRadioButton(port)
                self.rd_port_2.clicked.connect(lambda:self.radioPortClicked(self.rd_port_2))
                layout_port.addWidget(self.rd_port_2)

            elif len(ports) == 3:
                port = ports[0]
                self.rd_port_1 = QRadioButton(port)
                self.rd_port_1.setChecked(True)
                self.gen_port = port
                self.rd_port_1.clicked.connect(lambda:self.radioPortClicked(self.rd_port_1))
                layout_port.addWidget(self.rd_port_1)
                port = ports[1]
                self.rd_port_2 = QRadioButton(port)
                self.rd_port_2.clicked.connect(lambda:self.radioPortClicked(self.rd_port_2))
                layout_port.addWidget(self.rd_port_2)
                port = ports[2]
                self.rd_port_3 = QRadioButton(port)
                self.rd_port_3.clicked.connect(lambda:self.radioPortClicked(self.rd_port_3))
                layout_port.addWidget(self.rd_port_3)
            else:
                port = ports[0]
                self.rd_port_1 = QRadioButton(port)
                self.rd_port_1.setChecked(True)
                self.gen_port = port
                self.rd_port_1.clicked.connect(lambda:self.radioPortClicked(self.rd_port_1))
                layout_port.addWidget(self.rd_port_1)
                port = ports[1]
                self.rd_port_2 = QRadioButton(port)
                self.rd_port_2.clicked.connect(lambda:self.radioPortClicked(self.rd_port_2))
                layout_port.addWidget(self.rd_port_2)
                port = ports[2]
                self.rd_port_3 = QRadioButton(port)
                self.rd_port_3.clicked.connect(lambda:self.radioPortClicked(self.rd_port_3))
                layout_port.addWidget(self.rd_port_3)
                print("Too many")
        else:
            self.label_result = QLabel("현재 사용 가능한 포트가 없습니다. 연결 선을 USB에 연결하세요.")
            layout_port.addWidget(self.label_result)

        self.grp_port.setLayout(layout_port)
        main_layer.addWidget(self.grp_port)
        # self.grp_port.hide()

        # 전송속도 (Baud Rate)"
        self.grp_speed = QGroupBox("전송속도 (Baud Rate)")
        self.grp_speed.setFont(font)
        layout_speed = QHBoxLayout()

        self.rd_speed_96 = QRadioButton("9600")
        self.rd_speed_96.clicked.connect(lambda:self.radioValueClicked(self.rd_speed_96, "SPEED"))
        layout_speed.addWidget(self.rd_speed_96)

        self.rd_speed_384 = QRadioButton("38400")
        self.rd_speed_384.clicked.connect(lambda:self.radioValueClicked(self.rd_speed_384, "SPEED"))
        layout_speed.addWidget(self.rd_speed_384)

        self.rd_speed_576 = QRadioButton("57600")
        self.rd_speed_576.clicked.connect(lambda:self.radioValueClicked(self.rd_speed_576, "SPEED"))
        layout_speed.addWidget(self.rd_speed_576)

        self.rd_speed_1152 = QRadioButton("115200")
        self.com_speed = 115200
        self.rd_speed_1152.clicked.connect(lambda:self.radioValueClicked(self.rd_speed_1152, "SPEED"))
        layout_speed.addWidget(self.rd_speed_1152)
        self.rd_speed_1152.setChecked(True)

        self.grp_speed.setLayout(layout_speed)
        main_layer.addWidget(self.grp_speed)
        # self.grp_speed.hide()

        self.buttonBox = QDialogButtonBox()
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        main_layer.addWidget(self.buttonBox)

        self.buttonBox.accepted.connect(self.on_accepted)
        self.buttonBox.rejected.connect(self.reject)

    def on_accepted(self):
        if self.selected == 'RTU':
            if self.gen_port and self.com_speed and self.com_data and self.com_parity and self.com_stop:
                logging.info("All data meeted")
                self.accept()
            else:
                logging.info("ComPort/ Speed 를 모두 선택하시요.")
        elif self.selected == 'SCPI':
            logging.info("SCPI :: All data meeted")
            self.accept()
        elif self.selected == 'SEMES':
            logging.info("SEMES :: All data meeted")
            self.accept()
        else:
            self.ipaddress = self.edit_ipaddress.text()
            self.ipport = self.edit_ipport.text()
            if self.ipaddress and self.ipport:
                logging.info("All data meeted")
                self.accept()
            else:
                logging.info("IP Address and Port 를 모두 선택하시요.")


    def radioSelectClicked(self, btn):
        result = btn.text()
        logging.info(f"select :: {result}")
        if result == 'RTU':
            self.selected = 'RTU'
            self.grp_ipaddress.hide()
            self.grp_ipport.hide()
            
            self.grp_port.show()
            self.grp_speed.show()

        elif result == 'SEMES':
            self.selected = 'SEMES'
            self.grp_ipaddress.hide()
            self.grp_ipport.hide()
            
            self.grp_port.show()
            self.grp_speed.show()

        elif result == 'SCPI':
            self.selected = 'SCPI'
            self.grp_ipaddress.hide()
            self.grp_ipport.hide()
            
            self.grp_port.show()
            self.grp_speed.show()

        elif result == 'TCP':
            self.selected = 'TCP'
            self.grp_port.hide()
            self.grp_speed.hide()

            self.grp_ipaddress.show()
            self.grp_ipport.show()

        elif result == 'MONOPOLAR':
            self.ch_ptype = False

        elif result == 'BIPOLAR':
            self.ch_ptype = True
            
        
    def radioPortClicked(self, btn):
        result = btn.text()
        self.gen_port = result


    def radioValueClicked(self, btn, ptype):
        result = check_stopbit(btn.text())
        if ptype == "SPEED":
            self.com_speed = int(result)
            print("SPEED :: ", self.com_speed)
        elif ptype == "DATA":
            self.com_data = result
        elif ptype == "PARITY":
            self.com_parity = result
        elif ptype == "STOP":
            self.com_stop = result


class MyLabel(QLabel):
    def __init__(self, *args, **kwargs):
        super(MyLabel, self).__init__(*args, **kwargs)
        self.setFixedHeight(15)
        self.setMaximumHeight(15)
        self.setStyleSheet('margin: -1px; padding: 0px; font-size: 14pt; color: blue;background-color: #ccc;font-weight:bold;')
        self.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)

class Myblack(QLabel):
    def __init__(self, *args, **kwargs):
        super(Myblack, self).__init__(*args, **kwargs)
        self.setFixedHeight(15)
        self.setMaximumHeight(15)
        self.setStyleSheet('margin: -1px; padding: 0px; font-size: 14pt; color: black;background-color: #ccc;')
        self.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)


def add_read_column(layout, nth, address, label):
    edit_address = QSpinBox()
    edit_address.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
    edit_address.setValue(address)
    edit_address.setEnabled(False)
    layout.addWidget(edit_address, nth, 0)

    mylbl = MyLabel(label)
    layout.addWidget(mylbl, nth, 1)


def add_write_column(layout, nth, address, value, label):
    edit_address = QSpinBox()
    edit_address.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
    edit_address.setValue(address)
    edit_address.setEnabled(False)
    layout.addWidget(edit_address, nth, 0)

    edit_val = QSpinBox()
    edit_val.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
    edit_val.setStyleSheet('margin: -1px; padding: 0px; font-size: 14pt; color: blue;background-color: white')
    edit_val.setRange(-5000, 5000)
    edit_val.setValue(value)
    edit_val.setEnabled(False)
    layout.addWidget(edit_val, nth, 1)

    mylbl = MyLabel(label)
    layout.addWidget(mylbl, nth, 2)


class RTUManualWin(QDialog):
    def __init__(self, Dialog):
        super().__init__()
        self.title = "RTU(486) 매뉴얼"
        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(1024, 800) 

        main_layer = QHBoxLayout()
        self.setWindowTitle(self.title)
        self.setLayout(main_layer)

        ## READ
        self.grp_read = QGroupBox("READ (FUNCTION :: 04)")
        layout_read = QGridLayout()
        self.grp_read.setLayout(layout_read)

        myadd = QPushButton("Address")
        myval = QPushButton("Value")
        myexp = QPushButton("Explain")
        layout_read.addWidget(myadd, 0, 0)
        layout_read.addWidget(myexp, 0, 1)

        add_read_column(layout_read, 1, 5, "VOLTAGE 1: V")
        add_read_column(layout_read, 2, 6, "CURRENT 1: mA")
        add_read_column(layout_read, 3, 7, "VOLTAGE 2: V")
        add_read_column(layout_read, 4, 8, "CURRENT 2: mA")

        add_read_column(layout_read, 5, 10, "VCS 전압 레벨")
        add_read_column(layout_read, 6, 11, "ICS 전압 레벨")
        add_read_column(layout_read, 7, 12, "CP ( 정전용량 값 )")
        add_read_column(layout_read, 8, 13, "IO LEAK1(리크 전류1 현재값)")
        add_read_column(layout_read, 9, 15, "IO LEAK2(리크 전류2 현재값)")

        main_layer.addWidget(self.grp_read)


        ## WRITE
        self.grp_write = QGroupBox("WRITE (FUNCTION :: 16)")
        layout_write = QGridLayout()
        self.grp_write.setLayout(layout_write)

        myadd = QPushButton("Address")
        myval = QPushButton("Value")
        myexp = QPushButton("Explain")
        layout_write.addWidget(myadd, 0, 0)
        layout_write.addWidget(myval, 0, 1)
        layout_write.addWidget(myexp, 0, 2)

        add_write_column(layout_write, 1, 0, 0, "STOP")
        add_write_column(layout_write, 2, 0, 2, "RUN")
        add_write_column(layout_write, 3, 1, 2500, "VOLTAGE 1 SETUP : 2500 V")
        add_write_column(layout_write, 4, 2, 9900, "CURRENT 1 SETUP : 9.9 mA")
        add_write_column(layout_write, 5, 3, -2500, "VOLTAGE 2 SETUP : -2500 V")
        add_write_column(layout_write, 6, 4, 8800, "CURRENT 2 SETUP : 8.8 mA")
        add_write_column(layout_write, 7, 5, 300, "SET LEAK FAULT LEVEL: 0.3 mA")
        add_write_column(layout_write, 8, 6, 0, "SET RO MIN FAULT ")
        add_write_column(layout_write, 9, 7, 0, "STOP")
        add_write_column(layout_write, 10, 8, 0, "STOP")
        add_write_column(layout_write, 11, 9, 1, "RAMP UP TIME")
        add_write_column(layout_write, 12, 10, 1, "RAMP DOWN TIME")
        add_write_column(layout_write, 13, 11, 1, "AUTO TOGGLE MODE ( 0 : OFF , 1 : ON )")
        add_write_column(layout_write, 14, 12, 1, "AUTO TOGGLE COUNT ")
        add_write_column(layout_write, 15, 13, 1, "SET SLOPE "    )
        add_write_column(layout_write, 16, 14, 1, "SET COEFF")
        add_write_column(layout_write, 17, 16, 1, "ON/OFF SELECTION (0 : INTERNAL , 1 : REMOTE )")
        add_write_column(layout_write, 18, 18, 1, "ARC DELAY")
        add_write_column(layout_write, 19, 19, 1, "ARC RATE")
        add_write_column(layout_write, 20, 20, 1, "TOGGLE ( 0 : FORWARD , 1 : REVERSE )")
        add_write_column(layout_write, 21, 21, 1, "ARC_CONTROL( 0 : OFF , 1 : ON )")
        add_write_column(layout_write, 22, 22, 1, "OCP_CONTROL( 0 : OFF , 1 : ON )")
        add_write_column(layout_write, 23, 23, 1, "TARGET CAPACITOR")
        add_write_column(layout_write, 24, 24, 1, "CAP DEVIATION")

        main_layer.addWidget(self.grp_write)


class SCPIManualWin(QDialog):
    def __init__(self, Dialog):
        super().__init__()
        self.title = "SCPI 매뉴얼"
        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(1024, 800) 

        main_layer = QHBoxLayout()
        self.setWindowTitle(self.title)
        self.setLayout(main_layer)

        ## READ
        self.grp_read = QGroupBox("READ COMMAND")
        layout_read = QGridLayout()
        self.grp_read.setLayout(layout_read)

        cmd1 = QPushButton("Meaning")
        myval1 = QPushButton("SCPI Command")
        layout_read.addWidget(cmd1, 0, 0)
        layout_read.addWidget(myval1, 0, 1)

        self.add_read_column(layout_read, 1, "READ VOLTAGE 1, 2", "Meas:volt? (@1,2)")
        self.add_read_column(layout_read, 2, "READ CURRENT 1, 2", "Meas:curr? (@1,2)")
        self.add_read_column(layout_read, 3, "READ LEAK CURRENT 1, 2", "Meas:leak? (@1,2)")

        main_layer.addWidget(self.grp_read)


        ## WRITE
        self.grp_write = QGroupBox("WRITE COMMAND")
        layout_write = QGridLayout()
        self.grp_write.setLayout(layout_write)

        cmd2 = QPushButton("Meaning")
        myval2 = QPushButton("SCPI Command")
        layout_write.addWidget(cmd2, 0, 0)
        layout_write.addWidget(myval2, 0, 1)

        self.add_read_column(layout_write, 1, "RUN", "OUTPUT ON")
        self.add_read_column(layout_write, 2, "STOP", "OUTPUT OFF")

        self.add_read_column(layout_write, 3, "SET VOLTAGE 1, 2", "CONF:Volt 2500,-2500")
        self.add_read_column(layout_write, 4, "SET CURRENT 1, 2", "CONF:Curr 900,900")
        self.add_read_column(layout_write, 5, "SET LEAK CURRENT 1, 2", "CONF:Leak 10,10")

        self.add_read_column(layout_write, 6, "CHANGE VOLTAGE 1, 2", "Volt 2500,-2500")
        self.add_read_column(layout_write, 7, "CHANGE CURRENT 1, 2", "Curr 900,900")
        self.add_read_column(layout_write, 8, "CHANGE LEAK CURRENT 1, 2", "Leak 10,10")

        self.add_read_column(layout_write, 9, "TOGGLE FORWARD", "TOGGLE ON")
        self.add_read_column(layout_write, 10, "TOGGLE REVERSE", "TOGGLE OFF")

        self.add_read_column(layout_write, 11, "SET RAMP UP TIME", "CONF:RAMP UP,10")
        self.add_read_column(layout_write, 12, "SET RAMP DOWN TIME", "CONF:RAMP DOWN,10")

        main_layer.addWidget(self.grp_write)

    def add_read_column(self, layout, nth, meaning, command):
        meanlabel = Myblack(meaning)
        layout.addWidget(meanlabel, nth, 0)
        commandlabel = MyLabel(command)
        layout.addWidget(commandlabel, nth, 1)


class SEMESManualWin(QDialog):
    def __init__(self, Dialog):
        super().__init__()
        self.title = "SEMES 매뉴얼"
        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(1024, 800) 

        main_layer = QHBoxLayout()
        self.setWindowTitle(self.title)
        self.setLayout(main_layer)

        ## READ
        self.grp_read = QGroupBox("READ COMMAND")
        layout_read = QGridLayout()
        self.grp_read.setLayout(layout_read)

        cmd1 = QPushButton("Meaning")
        myval1 = QPushButton("SEMES Command")
        layout_read.addWidget(cmd1, 0, 0)
        layout_read.addWidget(myval1, 0, 1)

        self.add_read_column(layout_read, 1, "READ VOLTAGE", "RV")
        self.add_read_column(layout_read, 2, "READ CURRENT", "R+")

        main_layer.addWidget(self.grp_read)


        ## WRITE
        self.grp_write = QGroupBox("WRITE COMMAND")
        layout_write = QGridLayout()
        self.grp_write.setLayout(layout_write)

        cmd2 = QPushButton("Meaning")
        myval2 = QPushButton("SEMES Command")
        layout_write.addWidget(cmd2, 0, 0)
        layout_write.addWidget(myval2, 0, 1)

        self.add_read_column(layout_write, 1, "RUN", "EV")
        self.add_read_column(layout_write, 2, "STOP", "DV")

        self.add_read_column(layout_write, 3, "SET VOLTAGE 2500", "SV2500.000000")
        self.add_read_column(layout_write, 4, "SET CURRENT 10", "SI10.000000")

        self.add_read_column(layout_write, 5, "TOGGLE FORWARD", "V+")
        self.add_read_column(layout_write, 6, "TOGGLE REVERSE", "V-")

        self.add_read_column(layout_write, 7, "SET RAMP UP TIME 3", "P+3")
        self.add_read_column(layout_write, 8, "SET RAMP DOWN TIME 3", "P-3")

        main_layer.addWidget(self.grp_write)

    def add_read_column(self, layout, nth, meaning, command):
        meanlabel = Myblack(meaning)
        layout.addWidget(meanlabel, nth, 0)
        commandlabel = MyLabel(command)
        layout.addWidget(commandlabel, nth, 1)


class SettingPort(QDialog):
    def __init__(self, Dialog):
        super().__init__()
        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        self.setGeometry(100, 100, 400, 200)
        font = QFont()
        font.setPointSize(12)

        self.gen_port = None
        self.com_speed = 115200
        self.com_data = 8
        self.com_parity = serial.PARITY_NONE
        self.com_stop = 1

    # Main 
        main_layer = QVBoxLayout()
        self.setLayout(main_layer)

        # 통신 포트  설정(Serial Port)"
        grp_port = QGroupBox("통신 포트  설정(Serial Port)")
        grp_port.setFont(font)

        layout_port = QHBoxLayout()
        poss_ports = get_comm_port()

        if poss_ports:
            if len(poss_ports) == 1:
                port = poss_ports[0]
                self.rd_port_1 = QRadioButton(port)
                self.rd_port_1.setChecked(True)
                self.gen_port = port
                self.rd_port_1.clicked.connect(lambda:self.radioPortClicked(self.rd_port_1))
                layout_port.addWidget(self.rd_port_1)
            elif len(poss_ports) == 2:
                port = poss_ports[0]
                self.rd_port_1 = QRadioButton(port)
                self.rd_port_1.setChecked(True)
                self.gen_port = port
                self.rd_port_1.clicked.connect(lambda:self.radioPortClicked(self.rd_port_1))
                layout_port.addWidget(self.rd_port_1)
                port = poss_ports[1]
                self.rd_port_2 = QRadioButton(port)
                self.rd_port_2.clicked.connect(lambda:self.radioPortClicked(self.rd_port_2))
                layout_port.addWidget(self.rd_port_2)
            elif len(poss_ports) == 3:
                port = poss_ports[0]
                self.rd_port_1 = QRadioButton(port)
                self.rd_port_1.setChecked(True)
                self.gen_port = port
                self.rd_port_1.clicked.connect(lambda:self.radioPortClicked(self.rd_port_1))
                layout_port.addWidget(self.rd_port_1)
                port = poss_ports[1]
                self.rd_port_2 = QRadioButton(port)
                self.rd_port_2.clicked.connect(lambda:self.radioPortClicked(self.rd_port_2))
                layout_port.addWidget(self.rd_port_2)
                port = poss_ports[2]
                self.rd_port_3 = QRadioButton(port)
                self.rd_port_3.clicked.connect(lambda:self.radioPortClicked(self.rd_port_3))
                layout_port.addWidget(self.rd_port_3)
        else:
            self.label_result = QLabel("현재 사용 가능한 포트가 없습니다. 연결 선을 USB에 연결하세요.")
            layout_port.addWidget(self.label_result)

        grp_port.setLayout(layout_port)
        main_layer.addWidget(grp_port)

        # 전송속도 (Baud Rate)"
        grp_speed = QGroupBox("전송속도 (Baud Rate)")
        grp_speed.setFont(font)

        layout_speed = QHBoxLayout()
        speeds = ['38400', '115200']

        speed = speeds[0]
        self.rd_speed_1 = QRadioButton(speed)
        self.rd_speed_1.clicked.connect(lambda:self.radioValueClicked(self.rd_speed_1, 'SPEED'))
        layout_speed.addWidget(self.rd_speed_1)
        speed = speeds[1]
        self.rd_speed_2 = QRadioButton(speed)
        self.rd_speed_2.clicked.connect(lambda:self.radioValueClicked(self.rd_speed_2, 'SPEED'))
        self.rd_speed_2.setChecked(True)
        
        layout_speed.addWidget(self.rd_speed_2)


        grp_speed.setLayout(layout_speed)
        main_layer.addWidget(grp_speed)


        self.buttonBox = QDialogButtonBox()
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        main_layer.addWidget(self.buttonBox)

        self.buttonBox.accepted.connect(self.on_accepted)
        self.buttonBox.rejected.connect(self.reject)

    def on_accepted(self):
        if self.gen_port and self.com_speed:
            # print("All data meeted")
            self.accept()
        else:
            print("COM PORT를 선택하시요.")
        
    def radioPortClicked(self, btn):
        result = btn.text()
        self.gen_port = result

    def radioValueClicked(self, btn, ptype):
        result = btn.text()
        if ptype == "SPEED":
            self.com_speed = int(result)
        elif ptype == "PORT":
            self.gen_port = result



if __name__ == "__main__":
    app = QApplication(sys.argv)
    Dialog = QDialog()
    mywindow = SettingWin(Dialog)
    mywindow.show()
    app.exec_()
