import os
import sys
import logging
from ctypes import c_int16

import logging
from PyQt5.QtCore import Qt, QThread, pyqtSignal
from PyQt5.QtWidgets import QWidget, QLabel, QPushButton, QGroupBox, QDialog, QCheckBox, QRadioButton
from PyQt5.QtWidgets import QSpinBox, QApplication, QGridLayout, QLineEdit, QHBoxLayout, QVBoxLayout
from PyQt5.QtWidgets import QDialogButtonBox
from .semes import semes_get_measure, semes_write

from . import rtu486 as device

OFF = 0
ON = 1

class Field:
    grayfont = "color: #222;"
    bluefont = "color: #0000ff;"
    redfont = "color: #ff0000;font-weight: bold;"

    def __init__(self, labelname, status1, status2, value, comment=None):
        self.value = value
        self.labelname = labelname
        self.lbl_name = QLabel(labelname)
        self.rd_status1 = QRadioButton(status1)
        self.rd_status1.setChecked(True)
        self.rd_status1.clicked.connect(lambda:self.radioPortClicked(self.rd_status1))

        self.rd_status2 = QRadioButton(status2)
        self.rd_status2.clicked.connect(lambda:self.radioPortClicked(self.rd_status2))

        self.comment = comment

    def __str__(self):
        return f"{self.lbl_name}"

    def set_value(self, value):
        self.value = value

    def display(self):
        group = QGroupBox()
        layout = QHBoxLayout()
        group.setLayout(layout)

        layout.addWidget(self.lbl_name)
        layout.addWidget(self.rd_status1)
        layout.addWidget(self.rd_status2)

        if self.comment:
            layout.addWidget(QLabel(self.comment))

        return group

    def displayV(self):
        group = QGroupBox(self.labelname)
        layout = QVBoxLayout()
        group.setLayout(layout)

        # layout.addWidget(self.lbl_name)
        layout.addWidget(self.rd_status1)
        layout.addWidget(self.rd_status2)

        return group
    
    def getValue(self):
        return self.value

    def change_normal(self):
        self.value = OFF
        self.rd_status1.setChecked(True)
        self.rd_status1.setStyleSheet(Field.bluefont)

        self.rd_status2.setChecked(False)
        self.rd_status2.setStyleSheet(Field.grayfont)

    def change_warning(self):
        self.value = ON
        self.rd_status1.setChecked(False)
        self.rd_status1.setStyleSheet(Field.grayfont)

        self.rd_status2.setChecked(True)
        self.rd_status2.setStyleSheet(Field.redfont)

    def reset(self):
        self.rd_status1.setChecked(False)
        self.rd_status1.setStyleSheet(Field.grayfont)

        self.rd_status2.setChecked(False)
        self.rd_status2.setStyleSheet(Field.grayfont)

    def radioPortClicked(self, btn):
        result = btn.text()
        logging.info(f"SELECT PRODUCT :: {result}")
        self.value = result

class MyLineEdit(QLineEdit):
    def __init__(self, *args, **kwargs):
        super(MyLineEdit, self).__init__(*args, **kwargs)
        self.setAlignment(Qt.AlignRight)
        self.setFixedHeight(25)
        self.setMaximumHeight(25)
        self.setStyleSheet("background-color: #ccc;font-size: 16px; font-weight:bold")

    def change_warning(self):
        self.setStyleSheet("color: #f00;background-color: #eee;font-size: 16px; font-weight:bold")

    def change_normal(self):
        self.setStyleSheet("color: #00f;background-color: #eee;font-size: 16px; font-weight:bold")

    def reset(self):
        self.setText("")
        self.setStyleSheet("background-color: #ccc;font-size: 16px; font-weight:bold")
        
class BlueLineEdit(QLineEdit):
    def __init__(self, *args, **kwargs):
        super(BlueLineEdit, self).__init__(*args, **kwargs)
        self.setAlignment(Qt.AlignRight)
        self.setFixedHeight(25)
        self.setMaximumHeight(25)
        self.setStyleSheet("color:#401552;background-color: #eee;font-size: 16px; font-weight:bold")
        self.setEnabled(False)

class MyLabel(QLabel):
    def __init__(self, *args, **kwargs):
        super(MyLabel, self).__init__(*args, **kwargs)
        self.setFixedHeight(20)
        self.setMaximumHeight(20)

class GLabel(MyLabel):
    def __init__(self, *args, **kwargs):
        super(GLabel, self).__init__(*args, **kwargs)
        self.setAlignment(Qt.AlignRight)

class WarningDialog(QDialog):
    def __init__(self, message):
        super().__init__()
        self.message = message
        self.setupUI()

    def setupUI(self):
        self.setGeometry(100, 100, 400, 200)

        self.setWindowTitle("Warning !!")
        layout = QVBoxLayout()
        self.setLayout(layout)
        self.label = QLabel(self.message)
        layout.addWidget(self.label)

        self.buttonBox = QDialogButtonBox()
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        layout.addWidget(self.buttonBox)

        self.buttonBox.accepted.connect(self.on_accepted)
        self.buttonBox.rejected.connect(self.reject)

    def on_accepted(self):
        print("WarningDialog :: OK")
        self.accept()

class OffsetGain:
    def __init__(self, parent, layout, name, offset_address, gain_address, offsetname, gainname, row, nth, unit=""):       
        self.parent = parent
        self.layout_grid = layout
        self.name = name
        self.unit = unit
        self.offset_address = offset_address
        self.gain_address = gain_address

        self.row = row
        self.nth = nth

        self.offset = 0
        self.offset_avg = 0

        self.edit_offset = MyLineEdit('0')
        self.edit_offset.returnPressed.connect(self.modify_offset)
        self.edit_offset_avg = BlueLineEdit("0")

        self.greenback = "font-weight: bold; background-color: #00ff00; border-radius: 5px; padding: 3px"
        self.grayback = "background-color: #fcfcfc; border: 1px solid lightgray;border-radius: 5px; padding: 3px"
        self.bluefont = "background-color: #ccc;color: #0000ff;font-weight: bold;"
        self.redfont = "background-color: #fff;color: #0000ff;font-weight: bold;"

        self.btn_offset = QPushButton("CONTROL")
        self.btn_offset.clicked.connect(self.modify_offset)
        self.btn_offset.setStyleSheet(self.grayback)
        self.btn_offset.setEnabled(False)

        if gainname:
            self.gain = 0
            self.gain_avg = 0
            self.edit_gain = MyLineEdit('0')
            self.edit_gain.returnPressed.connect(self.modify_gain)
            self.edit_gain_avg = BlueLineEdit("0")

            self.btn_gain = QPushButton("CONTROL")
            self.btn_gain.clicked.connect(self.modify_gain)
            self.btn_gain.setStyleSheet(self.grayback)
            self.btn_gain.setEnabled(False)
        else:
            self.btn_gain = None


    def display(self):
        if self.btn_gain:
            self.layout_grid.addWidget(GLabel(self.name), self.row, self.nth)
            self.layout_grid.addWidget(self.edit_offset, self.row, self.nth+1)
            self.layout_grid.addWidget(self.btn_offset, self.row, self.nth+2)
            
            self.layout_grid.addWidget(self.edit_gain, self.row, self.nth+3)
            self.layout_grid.addWidget(self.btn_gain, self.row, self.nth+4)
        else:
            self.layout_grid.addWidget(GLabel(self.name), self.row, self.nth)
            self.layout_grid.addWidget(self.edit_offset, self.row, self.nth+1, 1, 2)
            self.layout_grid.addWidget(self.btn_offset, self.row, self.nth+3, 1, 2)


    def update_avg(self, offset_avg, gain_avg=0):
        if offset_avg:
            self.offset_avg = offset_avg
        else:
            self.offset_avg = 0
        self.edit_offset_avg.setText(str(round(self.offset_avg)))
        
        if self.btn_gain:
            if gain_avg:
                self.gain_avg = gain_avg
            else:
                self.gain_avg = 0
            self.edit_gain_avg.setText(str(round(self.gain_avg)))


    def update_val(self, offset, gain=0):
        self.offset = offset
        self.edit_offset.setText(str(round(offset)))
        
        if self.btn_gain:
            self.gain = gain
            self.edit_gain.setText(str(round(gain)))


    def update_offset(self, val):
        # logging.info(f"update_offset :: {val}")
        val = c_int16(val).value
        self.offset = val
        self.edit_offset.setText(str(round(val)))
        self.edit_offset.setStyleSheet(self.bluefont)

    def update_gain(self, val):
        # logging.info(f"update_gain :: {val}")
        val = c_int16(val).value
        self.gain = val
        self.edit_gain.setText(str(round(val)))
        self.edit_gain.setStyleSheet(self.bluefont)


    def unlock(self):
        self.edit_offset.setStyleSheet(self.redfont)
        self.btn_offset.setEnabled(True)
        self.btn_offset.setStyleSheet(self.bluefont)

        if self.btn_gain:
            self.edit_gain.setStyleSheet(self.redfont)
            self.btn_gain.setEnabled(True)
            self.btn_gain.setStyleSheet(self.bluefont)


    def modify_offset(self):
        val = int(self.edit_offset.text())
        self.parent.modify_device(self.offset_address, val)


    def modify_gain(self):
        val = int(self.edit_gain.text())
        self.parent.modify_device(self.gain_address, val)
        
    def reset(self):
        self.edit_offset.setText(str(0))
        self.btn_offset.setStyleSheet(self.grayback)

        if self.btn_gain:
            self.edit_gain.setText(str(0))
            self.btn_gain.setStyleSheet(self.grayback)

class VolReadWrite:
    def __init__(self, parent, layout, name, read_address, write_addresss=0, col=0):       
        self.parent = parent
        self.layout_grid = layout
        self.name = name
        self.read_address = read_address
        self.write_addresss = write_addresss
        self.col = col

        self.edit_read = BlueLineEdit("0")
        self.edit_write = MyLineEdit("0")
        self.edit_write.returnPressed.connect(self.modify_offset)

        self.greenback = "font-weight: bold; background-color: #00ff00; border-radius: 5px; padding: 3px"
        self.grayback = "background-color: #fcfcfc; border: 1px solid lightgray;border-radius: 5px; padding: 3px"
        self.bluefont = "background-color: #ccc;color: #0000ff;font-weight: bold;"
        self.redfont = "background-color: #fff;color: #0000ff;font-weight: bold;"

        self.btn_write = QPushButton("조 정")
        self.btn_write.clicked.connect(self.modify_offset)
        self.btn_write.setStyleSheet(self.grayback)
        self.btn_write.setEnabled(False)

    def display(self):
        btn_lbl = QPushButton(self.name)
        btn_lbl.setStyleSheet(self.grayback)
        self.layout_grid.addWidget(btn_lbl, 0, self.col)
        self.layout_grid.addWidget(self.edit_read, 1, self.col)

        if self.write_addresss:
            self.layout_grid.addWidget(self.edit_write, 2, self.col)
            self.layout_grid.addWidget(self.btn_write, 3, self.col)
    
    def update(self, val):
        val = c_int16(val).value
        if self.name.startswith("IO LEAK"):
            val = val / 10
        self.edit_read.setText(str(val))
        
    def unlock(self):
        self.edit_write.setStyleSheet(self.redfont)
        if self.write_addresss:
            self.btn_write.setEnabled(True)
            self.btn_write.setStyleSheet(self.bluefont)

    def modify_offset(self):
        val = int(self.edit_write.text())
        self.parent.modify_device(self.write_addresss, val)

    def reset(self):
        self.edit_read.setText("")

class RadioField:
    def __init__(self, parent, address, labelname, status1, status2, decision, comment=None):
        self.grayfont = "background-color: #fcfcfc; border: 1px solid lightgray;border-radius: 5px; padding: 3px"
        self.bluefont = "background-color: #ccc;color: #0000ff;font-weight: bold;"

        self.parent = parent
        self.decision = decision
        self.value = OFF
        self.address = address
        self.labelname = labelname
        self.lbl_name = QLabel(labelname)
        self.rd_status1 = QRadioButton(status1)
        self.rd_status1.setChecked(True)
        self.rd_status1.clicked.connect(lambda:self.radioPortClicked(self.rd_status1))

        self.rd_status2 = QRadioButton(status2)
        self.rd_status2.clicked.connect(lambda:self.radioPortClicked(self.rd_status2))
        self.comment = comment
        self.btn_control = QPushButton("CONTROL")
        self.btn_control.clicked.connect(self.modify_offset)
        self.btn_control.setStyleSheet(self.grayfont)
        self.btn_control.setEnabled(False)

    def __str__(self):
        return f"{self.lbl_name}"

    def set_value(self, value):
        self.value = value

    def display(self):
        group = QGroupBox()
        layout = QHBoxLayout()
        group.setLayout(layout)

        layout.addWidget(self.lbl_name)
        layout.addWidget(self.rd_status1)
        layout.addWidget(self.rd_status2)
        layout.addWidget(QLabel(self.comment))
        layout.addWidget(self.btn_control)

        return group

    def displayV(self):
        group = QGroupBox(self.labelname)
        layout = QVBoxLayout()
        group.setLayout(layout)

        # layout.addWidget(self.lbl_name)
        layout.addWidget(self.rd_status1)
        layout.addWidget(self.rd_status2)

        return group
    
    def getValue(self):
        return self.value

    def change_off(self):
        self.value = OFF
        self.rd_status1.setChecked(True)
        self.rd_status1.setStyleSheet(self.bluefont)

        self.rd_status2.setChecked(False)
        self.rd_status2.setStyleSheet(self.grayfont)

    def change_on(self):
        self.value = ON
        self.rd_status1.setChecked(False)
        self.rd_status1.setStyleSheet(self.grayfont)

        self.rd_status2.setChecked(True)
        self.rd_status2.setStyleSheet(self.bluefont)

    def reset(self):
        self.rd_status1.setChecked(False)
        self.rd_status1.setStyleSheet(self.grayfont)

        self.rd_status2.setChecked(False)
        self.rd_status2.setStyleSheet(self.grayfont)

    def radioPortClicked(self, btn):
        result = btn.text()
        if result == self.decision:
            self.value = ON
        else:
            self.value = OFF
        logging.info(f"SELECT ON/OFF :: {result} :: {self.value}")

    def modify_offset(self):
        self.parent.modify_device(self.address, self.value)

    def unlock(self):
        self.btn_control.setEnabled(True)
        self.btn_control.setStyleSheet(self.bluefont)

class RadioThree:
    def __init__(self, parent, address, labelname, status1, status2, status3, comment, status4=None):
        self.grayback = "background-color: #fcfcfc; border: 1px solid lightgray;border-radius: 5px; padding: 3px"
        self.bluefont = "background-color: #ccc;color: #0000ff;font-weight: bold;"

        self.parent = parent
        self.value = 0
        self.address = address
        self.labelname = labelname
        self.lbl_name = QLabel(labelname)
        self.rd_status1 = QRadioButton(status1)
        self.rd_status1.setChecked(True)
        self.rd_status1.clicked.connect(lambda:self.radioPortClicked(self.rd_status1))

        self.rd_status2 = QRadioButton(status2)
        self.rd_status2.clicked.connect(lambda:self.radioPortClicked(self.rd_status2))
        
        self.rd_status3 = QRadioButton(status3)
        self.rd_status3.clicked.connect(lambda:self.radioPortClicked(self.rd_status3))

        if status4:
            self.rd_status4 = QRadioButton(status4)
            self.rd_status4.clicked.connect(lambda:self.radioPortClicked(self.rd_status4))

        self.comment = comment
        self.btn_control = QPushButton("CONTROL")
        self.btn_control.clicked.connect(self.modify_offset)
        self.btn_control.setStyleSheet(self.grayback)
        self.btn_control.setEnabled(False)

    def __str__(self):
        return f"{self.lbl_name}"

    def set_value(self, value):
        self.value = value

    def display(self):
        group = QGroupBox()
        layout = QHBoxLayout()
        group.setLayout(layout)

        layout.addWidget(self.lbl_name)
        layout.addWidget(self.rd_status1)
        layout.addWidget(self.rd_status2)
        layout.addWidget(self.rd_status3)
        if self.rd_status4:
            layout.addWidget(self.rd_status4)
        layout.addWidget(QLabel(self.comment))
        layout.addWidget(self.btn_control)

        return group

    def getValue(self):
        return self.value

    def change_0(self):
        self.value = OFF
        self.rd_status1.setChecked(True)
        self.rd_status1.setStyleSheet(self.bluefont)

        self.rd_status2.setChecked(False)
        self.rd_status2.setStyleSheet(self.grayfont)

        self.rd_status3.setChecked(False)
        self.rd_status3.setStyleSheet(self.grayfont)

    def change_1(self):
        self.value = ON
        self.rd_status1.setChecked(False)
        self.rd_status1.setStyleSheet(self.grayfont)

        self.rd_status2.setChecked(True)
        self.rd_status2.setStyleSheet(self.bluefont)

        self.rd_status3.setChecked(False)
        self.rd_status3.setStyleSheet(self.grayfont)

    def change_2(self):
        self.value = ON
        self.rd_status1.setChecked(False)
        self.rd_status1.setStyleSheet(self.grayfont)

        self.rd_status2.setChecked(False)
        self.rd_status2.setStyleSheet(self.grayfont)

        self.rd_status3.setChecked(True)
        self.rd_status3.setStyleSheet(self.bluefont)

    def reset(self):
        self.rd_status1.setChecked(False)
        self.rd_status1.setStyleSheet(self.grayfont)

        self.rd_status2.setChecked(False)
        self.rd_status2.setStyleSheet(self.grayfont)

        self.rd_status3.setChecked(False)
        self.rd_status3.setStyleSheet(self.grayfont)

    def radioPortClicked(self, btn):
        result = btn.text()
        logging.info(f"SELECT ON/OFF :: {result}")
        if result == "ON":
            self.value = 1
        else:
            self.value = 0

    def modify_offset(self):
        self.parent.modify_device(self.address, self.value)

    def unlock(self):
        self.btn_control.setEnabled(True)
        self.btn_control.setStyleSheet(self.bluefont)


