import os
import time
import serial
import pandas as pd
from datetime import datetime

# MODBUS RTU Slave 장치의 포트와 주소
SERIAL_PORT = '/dev/cu.usbserial-AC024JHG'  # 포트명은 실제 사용하는 시스템에 맞게 수정해야 합니다.
SERIAL_BAUDRATE = 115200
SERIAL_BYTESIZE = serial.EIGHTBITS
SERIAL_PARITY = serial.PARITY_NONE
SERIAL_STOPBITS = serial.STOPBITS_ONE
SLAVE_ADDRESS = 1

# 데이터를 저장할 리스트
data_list = []
time_list = []

def get_comm_port():
    port_list = []
    import serial.tools.list_ports
    ports = serial.tools.list_ports.comports()
    for port, desc, hwid in sorted(ports):
        if desc != 'n/a':
            port_list.append(port)
    return port_list


def read_modbus_data():
    # MODBUS RTU 장치 생성
    port_list = get_comm_port()
    print(port_list)
    serialport = input('Input Com Port : (ex: COM3):')
    serialport.upper()

    try:
        client = serial.Serial(
            port=serialport,
            baudrate=SERIAL_BAUDRATE,
            bytesize=SERIAL_BYTESIZE,
            parity=SERIAL_PARITY,
            stopbits=SERIAL_STOPBITS
        )
        print(client)
        while True:
            try:
                # 레지스터 값 읽기
                byte_string = client.readline()
                # 읽어온 데이터를 리스트에 추가
                if byte_string:
                    result = [byte for byte in byte_string]
                    time_list.append(time.time())
                    data_list.append(result)
                    print(datetime.now(), result)

                    # 데이터 개수가 3000개 이상이면 파일에 저장
                    if len(data_list) >= 3000:
                        save_data_to_file()

                # 1초 대기
            except Exception as e:
                print("Error :: ", e)
    except Exception as e:
        print("Error Happen !! : ", e)

    except KeyboardInterrupt:
        # Ctrl+C 입력 시 종료
        print("Exiting...")


def save_data_to_file():
    # 파일에 데이터 저장
    stime = datetime.now()
    file = f"asmk_{stime.year:04d}_{stime.month:02d}_{stime.day:02d}_{stime.hour:02d}_{stime.minute:02d}_{stime.second:02d}"

    tmarks = []
    for tm in time_list:
        tm = time.localtime(tm)
        string = time.strftime('%H:%M:%S', tm)
        tmarks.append(string)

    dataset = pd.DataFrame({
        "TIMES": tmarks,
        'DATAS': data_list,
    })

    csvfile = f"data/{file}.csv"
    dataset.to_csv(csvfile, sep=',')
    print("Data saved to file:", csvfile)

    # 저장된 데이터 초기화
    data_list.clear()
    time_list.clear()


if __name__ == "__main__":
    if not os.path.exists("data"):
        print("CTEATE :: data/")
        os.makedirs("data") 

    read_modbus_data()
