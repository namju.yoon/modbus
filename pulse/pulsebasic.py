import numpy as np
import pandas as pd
from random import randint, choices

from PyQt5.QtWidgets import QGroupBox, QHBoxLayout, QGridLayout, QLabel, QDialog
from PyQt5.QtWidgets import QMessageBox

from .block import SimpleDisplay, DataView, MyLabel, ChLabel, Field, QPushButton, WarningDialog
from .zoom import GraphWindow

NAMES = [
    "VDC OVP HW",
    "IDC OVP HW",
    "VDC OVP1 SW",
    "IDC OCP1 SW",
    "VDC OVP2 SW",
    "IDC OCP2 SW",
    "RO MIN 1",
    "RO MIN 2",
    "VDC UVP1 SW",
    "VDC UVP2 SW",
]

class Channel:
    def __init__(self, parent, name):
        self.parent = parent 
        self.name = name 

        self.red = "QPushButton{font-size: 12pt; font-weight: bold; color: white; background-color: red}"

        self.label_status = Field('Status', '정상', '경고', 0)
        self.label_fault = ChLabel("")

        self.po_list = []
        self.po_btn = QPushButton("PO")
        self.po_btn.clicked.connect(lambda:self.displayGraph("PO"))
        self.po_label = MyLabel()

        self.vo_list = []
        self.vo_btn = QPushButton("VO")
        self.vo_btn.clicked.connect(lambda:self.displayGraph("VO"))
        self.vo_label = MyLabel()

        self.io_list = []
        self.io_btn = QPushButton("IO")
        self.io_btn.clicked.connect(lambda:self.displayGraph("IO"))
        self.io_label = MyLabel()

        self.v24_list = []
        self.v24_btn = QPushButton("V24")
        self.v24_btn.clicked.connect(lambda:self.displayGraph("V24"))
        self.v24_label = MyLabel()

        self.i24_list = []
        self.i24_btn = QPushButton("I24")
        self.i24_btn.clicked.connect(lambda:self.displayGraph("I24"))
        self.i24_label = MyLabel()

        self.duty_list = [] # us
        self.duty_btn = QPushButton("DUTY")
        self.duty_btn.clicked.connect(lambda:self.displayGraph("DUTY"))
        self.duty_label = MyLabel()

        self.ro_list = [] # Ohm
        self.ro_btn = QPushButton("RO")
        self.ro_btn.clicked.connect(lambda:self.displayGraph("RO"))
        self.ro_label = MyLabel()

        self.temp1_list = [] # Ohm
        self.temp1_btn = QPushButton("FET#1 온도")
        self.temp1_btn.clicked.connect(lambda:self.displayGraph("FTP1"))
        self.temp1_label = MyLabel()
        self.temp1_origin = 0

        self.temp2_list = [] # Ohm
        self.temp2_btn = QPushButton("FET#2 온도")
        self.temp2_btn.clicked.connect(lambda:self.displayGraph("FTP2"))
        self.temp2_label = MyLabel()
        self.temp2_origin = 0

        self.temp3_list = [] # Ohm
        self.temp3_btn = QPushButton("FET#3 온도")
        self.temp3_btn.clicked.connect(lambda:self.displayGraph("FTP3"))
        self.temp3_label = MyLabel()
        self.temp3_origin = 0

        self.temp4_list = [] # Ohm
        self.temp4_btn = QPushButton("FET#4 온도")
        self.temp4_btn.clicked.connect(lambda:self.displayGraph("FTP4"))
        self.temp4_label = MyLabel()
        self.temp4_origin = 0

        
    def status_normal(self):
        self.label_status.change_normal()


    def reset(self):
        self.po_list = []
        self.vo_list = []
        self.io_list = []
        self.v24_list = []
        self.i24_list = []
        self.duty_list = []
        self.ro_list = []

        self.temp1_list = []
        self.temp2_list = []
        self.temp3_list = []
        self.temp4_list = []

        self.temp1_origin = 0
        self.temp2_origin = 0
        self.temp3_origin = 0
        self.temp4_origin = 0

    def get_ratio(self, current, old):
        ratio = int(((current - old) / old) * 100)
        return ratio

    def change_value(self, dchannel):
        try:
            # self.label_status.change_normal()
            self.fault = dchannel[1]
            if dchannel[1]:
                self.label_status.change_warning()

            self.po_list.append(dchannel[2])
            self.vo_list.append(dchannel[3])
            self.io_list.append(dchannel[4])
            self.v24_list.append(dchannel[5])
            self.i24_list.append(dchannel[6])
            self.duty_list.append(dchannel[7])
            self.ro_list.append(dchannel[8])

            if not self.temp1_origin:
                self.temp1_list.append(dchannel[9])
                self.temp2_list.append(dchannel[10])
                self.temp3_list.append(dchannel[11])
                self.temp4_list.append(dchannel[12])

                self.temp1_origin = dchannel[9]
                self.temp2_origin = dchannel[10]
                self.temp3_origin = dchannel[11]
                self.temp4_origin = dchannel[12]
            else:

                ## TEMP 1
                current = dchannel[9]
                previous = self.temp1_list[-1]
                if current > previous:
                    self.temp1_list.append(dchannel[9])
                    if self.get_ratio(current, self.temp1_origin) >= self.parent.parent.alarm_ratio:
                        print(current, self.temp1_origin, self.get_ratio(current, self.temp1_origin))
                        self.temp1_btn.setStyleSheet(self.red)
                        warning_message = f"{self.parent.name}::{self.name}::FET #1 의 온도가 초기 온도보다 {self.parent.parent.alarm_ratio}% 이상 높습니다."
                        print(warning_message)
                        dialog = WarningDialog(warning_message)
                        dialog.show()
                        response = dialog.exec_()
                else:
                    self.temp1_list.append(previous)

                ## TEMP 2
                current = dchannel[10]
                previous = self.temp2_list[-1]
                if current > previous:
                    self.temp2_list.append(dchannel[10])
                    if self.get_ratio(current, self.temp2_origin) >= self.parent.parent.alarm_ratio:
                        print(current, self.temp2_origin, self.get_ratio(current, self.temp2_origin))
                        self.temp2_btn.setStyleSheet(self.red)
                        warning_message = f"{self.parent.name}::{self.name}::FET #2 의 온도가 초기 온도보다 {self.parent.parent.alarm_ratio}% 이상 높습니다."
                        print(warning_message)
                        dialog = WarningDialog(warning_message)
                        dialog.show()
                        response = dialog.exec_()
                else:
                    self.temp2_list.append(previous)

                ## TEMP 3
                current = dchannel[11]
                previous = self.temp3_list[-1]
                if current > previous:
                    self.temp3_list.append(dchannel[11])
                    if self.get_ratio(current, self.temp3_origin) >= self.parent.parent.alarm_ratio:
                        print(current, self.temp3_origin, self.get_ratio(current, self.temp3_origin))
                        self.temp3_btn.setStyleSheet(self.red)
                        warning_message = f"{self.parent.name}::{self.name}::FET #3 의 온도가 초기 온도보다 {self.parent.parent.alarm_ratio}% 이상 높습니다."
                        print(warning_message)
                        dialog = WarningDialog(warning_message)
                        dialog.show()
                        response = dialog.exec_()
                else:
                    self.temp3_list.append(previous)

                ## TEMP 4
                current = dchannel[12]
                previous = self.temp4_list[-1]
                if current > previous:
                    self.temp4_list.append(dchannel[12])
                    if self.get_ratio(current, self.temp4_origin) >= self.parent.parent.alarm_ratio:
                        print(current, self.temp4_origin, self.get_ratio(current, self.temp4_origin))
                        self.temp4_btn.setStyleSheet(self.red)
                        warning_message = f"{self.parent.name}::{self.name}::FET #4 의 온도가 초기 온도보다 {self.parent.parent.alarm_ratio}% 이상 높습니다."
                        print(warning_message)
                        dialog = WarningDialog(warning_message)
                        dialog.show()
                        response = dialog.exec_()
                else:
                    self.temp4_list.append(previous)

        except Exception as e:
            print(e)


    def change_channel(self, dchannel):
        try:
            # self.label_status.change_normal()
            self.fault = dchannel[1]
            if dchannel[1]:
                self.label_status.change_warning()
                self.label_fault.setText(str(dchannel[1]))

            self.po_label.setText(str(dchannel[2]))
            self.vo_label.setText(str(dchannel[3]))
            self.io_label.setText(str(dchannel[4]))
            self.v24_label.setText(str(dchannel[5]))
            self.i24_label.setText(str(dchannel[6]))
            self.duty_label.setText(str(dchannel[7]))
            self.ro_label.setText(str(dchannel[8]))

            self.temp1_label.setText(str(dchannel[9]))
            self.temp2_label.setText(str(dchannel[10]))
            self.temp3_label.setText(str(dchannel[11]))
            self.temp4_label.setText(str(dchannel[12]))
            
            self.change_value(dchannel)

        except Exception as e:
            print(e)


    def display(self):
        self.grp_channel = QGroupBox(self.name)
        self.layout_channel = QGridLayout()
        self.grp_channel.setLayout(self.layout_channel)

        self.displayLoutField(0, self.label_status)
        self.displayLout(1, QLabel('Fault'), self.label_fault)
        self.displayLout(2, self.po_btn, self.po_label, QLabel('kW'))
        self.displayLout(3, self.vo_btn, self.vo_label, QLabel('V'))
        self.displayLout(4, self.io_btn, self.io_label, QLabel('mA'))
        self.displayLout(5, self.v24_btn, self.v24_label, QLabel('mV'))
        self.displayLout(6, self.i24_btn, self.i24_label, QLabel('mA'))
        self.displayLout(7, self.duty_btn, self.duty_label, QLabel('us'))
        self.displayLout(8, self.ro_btn, self.ro_label, QLabel('Ohm'))

        self.displayLout(9, self.temp1_btn, self.temp1_label, QLabel('°C'))
        self.displayLout(10, self.temp2_btn, self.temp2_label, QLabel('°C'))
        self.displayLout(11, self.temp3_btn, self.temp3_label, QLabel('°C'))
        self.displayLout(12, self.temp4_btn, self.temp4_label, QLabel('°C'))
        return self.grp_channel


    def displayLout(self, nth, label, value, unit=''):
        if unit:
            self.layout_channel.addWidget(label, nth, 0)
            self.layout_channel.addWidget(value, nth, 1, 1, 2)
            self.layout_channel.addWidget(unit, nth, 3)
        else:
            self.layout_channel.addWidget(label, nth, 0)
            self.layout_channel.addWidget(value, nth, 1, 1, 3)


    def displayLoutField(self, nth, field):
        self.layout_channel.addWidget(field.lbl_name, nth, 0, 1, 2)
        self.layout_channel.addWidget(field.rd_status1, nth, 2)
        self.layout_channel.addWidget(field.rd_status2, nth, 3)


    def displayGraph(self, ptype):
        if ptype == 'PO':
            vals = self.po_list
            title = "Power"
        elif ptype == 'VO':
            vals = self.vo_list
            title = "Voltage"
        elif ptype == 'IO':
            vals = self.io_list
            title = "Current"
        elif ptype == 'V24':
            vals = self.v24_list
            title = "V24 Voltage"
        elif ptype == 'I24':
            vals = self.i24_list
            title = "I24 Current"
        elif ptype == 'DUTY':
            vals = self.duty_list
            title = "Primary DUTY"
        elif ptype == 'RO':
            vals = self.ro_list
            title = "RO"
        elif ptype == 'FTP1':
            vals = self.temp1_list
            title = "FET #1 온도"
        elif ptype == 'FTP2':
            vals = self.temp2_list
            title = "FET #2 온도"
        elif ptype == 'FTP3':
            vals = self.temp3_list
            title = "FET #3 온도"
        elif ptype == 'FTP4':
            vals = self.temp4_list
            title = "FET #4 온도"

        indexs = list(range(len(vals)))
        Dialog = QDialog()
        dialog = GraphWindow(Dialog, title, (255, 0, 0), indexs, vals)
        dialog.show()
        response = dialog.exec_()
        if response == QDialog.Accepted or response == QDialog.Rejected:
            self.graph_flag = False


    def update_last(self):
        self.po_label.setText(str(self.po_list[-1]))
        self.vo_label.setText(str(self.vo_list[-1]))
        self.io_label.setText(str(self.io_list[-1]))
        self.v24_label.setText(str(self.v24_list[-1]))
        self.i24_label.setText(str(self.i24_list[-1]))
        self.duty_label.setText(str(self.duty_list[-1]))
        self.ro_label.setText(str(self.ro_list[-1]))

        self.temp1_label.setText(str(self.temp1_list[-1]))
        self.temp2_label.setText(str(self.temp2_list[-1]))
        self.temp3_label.setText(str(self.temp3_list[-1]))
        self.temp4_label.setText(str(self.temp4_list[-1]))


class PulseModule:
    def __init__(self, parent, name, bcolor="#fff"):
        self.parent = parent

        self.name = name
        self.channels = []
        self.ch01 = Channel(self, 'Channel #1')
        self.channels.append(self.ch01)
        self.ch02 = Channel(self, 'Channel #2')
        self.channels.append(self.ch02)
        self.ch03 = Channel(self, 'Channel #3')
        self.channels.append(self.ch03)
        self.ch04 = Channel(self, 'Channel #4')
        self.channels.append(self.ch04)

        self.bcolor = bcolor
        self.main_label = "QLabel{font-size: 14pt; font-weight: bold}"

    def display(self):
        grp_module = QGroupBox(self.name)
        layout_module = QGridLayout()
        grp_module.setStyleSheet(f"background-color: {self.bcolor}")
        grp_module.setLayout(layout_module)

        ch_group1 = self.ch01.display()
        layout_module.addWidget(ch_group1, 0, 0)
        ch_group2 = self.ch02.display()
        layout_module.addWidget(ch_group2, 0, 1)
        ch_group3 = self.ch03.display()
        layout_module.addWidget(ch_group3, 1, 0)
        ch_group4 = self.ch04.display()
        layout_module.addWidget(ch_group4, 1, 1)
        
        return grp_module

    def active_normal(self):
        self.ch01.status_normal()
        self.ch02.status_normal()
        self.ch03.status_normal()
        self.ch04.status_normal()

    def update_module(self, mod):
        try:
            self.ch01.change_channel(mod[:13])
            self.ch02.change_channel(mod[13:26])
            self.ch03.change_channel(mod[26:39])
            self.ch04.change_channel(mod[39:])
        except Exception as e:
            print("PulseModule :: update_module :: ", e)

    def change_value(self, mod):
        try:
            self.ch01.change_value(mod[:13])
            self.ch02.change_value(mod[13:26])
            self.ch03.change_value(mod[26:39])
            self.ch04.change_value(mod[39:])
        except Exception as e:
            print("PulseModule :: change_value :: ", e)

    def update_last(self):
        self.ch01.update_last()
        self.ch02.update_last()
        self.ch03.update_last()
        self.ch04.update_last()

    def reset(self):
        self.ch01.reset()
        self.ch02.reset()
        self.ch03.reset()
        self.ch04.reset()


class SlowChannel:
    def __init__(self, name):
        self.name = name 

        self.vo_list = []
        self.vo_btn = QPushButton("S VO")
        self.vo_btn.clicked.connect(lambda:self.displayGraph("VO"))
        self.vo_label = MyLabel()

        self.io_list = []
        self.io_btn = QPushButton("S IO")
        self.io_btn.clicked.connect(lambda:self.displayGraph("IO"))
        self.io_label = MyLabel()

        self.po_list = []
        self.po_btn = QPushButton("S PO")
        self.po_btn.clicked.connect(lambda:self.displayGraph("PO"))
        self.po_label = MyLabel()

    def change_channel(self, dchannel):
        try:
            self.vo_list.append(dchannel[0])
            self.vo_label.setText(str(dchannel[0]))

            self.io_list.append(dchannel[1])
            self.io_label.setText(str(dchannel[1]))

            self.po_list.append(dchannel[2])
            self.po_label.setText(str(dchannel[2]))

        except Exception as e:
            print(e)


    def change_value(self, dchannel):
        try:
            # self.label_status.change_normal()
            self.vo_list.append(dchannel[0])
            self.io_list.append(dchannel[1])
            self.po_list.append(dchannel[2])

        except Exception as e:
            print(e)


    def display(self):
        self.grp_channel = QGroupBox(self.name)
        self.layout_channel = QGridLayout()
        self.grp_channel.setLayout(self.layout_channel)

        self.displayLout(0, self.vo_btn, self.vo_label, QLabel('V'))
        self.displayLout(1, self.io_btn, self.io_label, QLabel('mA'))
        self.displayLout(2, self.po_btn, self.po_label, QLabel('kW'))

        return self.grp_channel

    def reset(self):
        self.vo_list = []
        self.io_list = []
        self.po_list = []


    def displayLout(self, nth, label, value, unit=''):
        if unit:
            self.layout_channel.addWidget(label, nth, 0)
            self.layout_channel.addWidget(value, nth, 1, 1, 2)
            self.layout_channel.addWidget(unit, nth, 3)
        else:
            self.layout_channel.addWidget(label, nth, 0)
            self.layout_channel.addWidget(value, nth, 1, 1, 3)


    def displayGraph(self, ptype):
        if ptype == 'PO':
            vals = self.po_list
            title = "Power"
        elif ptype == 'VO':
            vals = self.vo_list
            title = "Voltage"
        elif ptype == 'IO':
            vals = self.io_list
            title = "Current"
            
        indexs = list(range(len(vals)))
        Dialog = QDialog()
        dialog = GraphWindow(Dialog, title, (255, 0, 0), indexs, vals)
        dialog.show()
        response = dialog.exec_()
        if response == QDialog.Accepted or response == QDialog.Rejected:
            self.graph_flag = False

    def update_last(self):
        self.po_label.setText(str(self.po_list[-1]))
        self.vo_label.setText(str(self.vo_list[-1]))
        self.io_label.setText(str(self.io_list[-1]))


class PulseModuleSlow:
    def __init__(self, name, bcolor="#fff"):
        self.name = name
        self.channels = []
        self.ch01 = SlowChannel('Channel #1')
        self.channels.append(self.ch01)
        self.ch02 = SlowChannel('Channel #2')
        self.channels.append(self.ch02)
        self.ch03 = SlowChannel('Channel #3')
        self.channels.append(self.ch03)
        self.ch04 = SlowChannel('Channel #4')
        self.channels.append(self.ch04)

        self.bcolor = bcolor
        self.main_label = "QLabel{font-size: 14pt; font-weight: bold}"

        self.v24_list = []
        self.v24_btn = QPushButton("S V24")
        self.v24_btn.clicked.connect(lambda:self.displayGraph("V24"))
        self.v24_label = MyLabel()

        self.i24_list = []
        self.i24_btn = QPushButton("S I24")
        self.i24_btn.clicked.connect(lambda:self.displayGraph("I24"))
        self.i24_label = MyLabel()


    def display(self):
        grp_module = QGroupBox(self.name)
        layout_module = QGridLayout()
        grp_module.setStyleSheet(f"background-color: {self.bcolor}")
        grp_module.setLayout(layout_module)

        ch_group1 = self.ch01.display()
        layout_module.addWidget(ch_group1, 0, 0)

        ch_group2 = self.ch02.display()
        layout_module.addWidget(ch_group2, 0, 1)

        ch_group3 = self.ch03.display()
        layout_module.addWidget(ch_group3, 1, 0)

        ch_group4 = self.ch04.display()
        layout_module.addWidget(ch_group4, 1, 1)

        self.grp_common = QGroupBox("Common")
        self.layout_common = QGridLayout()
        self.grp_common.setLayout(self.layout_common)

        self.displayLout(0, self.v24_btn, self.v24_label, QLabel('V'))
        self.displayLout(1, self.i24_btn, self.i24_label, QLabel('mA'))

        layout_module.addWidget(self.grp_common, 2, 0, 1, 2)

        return grp_module

    def displayLout(self, nth, label, value, unit=''):
        if unit:
            self.layout_common.addWidget(label, nth, 0)
            self.layout_common.addWidget(value, nth, 1, 1, 2)
            self.layout_common.addWidget(unit, nth, 3)
        else:
            self.layout_common.addWidget(label, nth, 0)
            self.layout_common.addWidget(value, nth, 1, 1, 3)


    def active_normal(self):
        self.ch01.status_normal()
        self.ch02.status_normal()
        self.ch03.status_normal()
        self.ch04.status_normal()

    def update_module(self, mod):
        try:
            self.ch01.change_channel(mod[:3])
            self.ch02.change_channel(mod[3:6])
            self.ch03.change_channel(mod[6:9])
            self.ch04.change_channel(mod[9:12])

            self.v24_list.append(mod[12])
            self.v24_label.setText(str(mod[12]))

            self.i24_list.append(mod[13])
            self.i24_label.setText(str(mod[13]))
        except Exception as e:
            print("PulseModuleSlow :: update_module :: ", e)


    def change_value(self, mod):
        try:
            self.ch01.change_value(mod[:3])
            self.ch02.change_value(mod[3:6])
            self.ch03.change_value(mod[6:9])
            self.ch04.change_value(mod[9:12])

            self.v24_list.append(mod[12])
            self.i24_list.append(mod[13])
        except Exception as e:
            print("PulseModuleSlow :: change_value :: ", e)


    def displayGraph(self, ptype):
        if ptype == 'V24':
            title = "Slow V24 Voltage"
            vals = self.v24_list
        elif ptype == 'I24':
            title = "Slow I24 Current"
            vals = self.i24_list

        indexs = list(range(len(vals)))
        Dialog = QDialog()
        dialog = GraphWindow(Dialog, title, (255, 0, 0), indexs, vals)
        dialog.show()
        response = dialog.exec_()
        if response == QDialog.Accepted or response == QDialog.Rejected:
            self.graph_flag = False

    def update_last(self):
        self.ch01.update_last()
        self.ch02.update_last()
        self.ch03.update_last()
        self.ch04.update_last()
        self.v24_label.setText(str(self.v24_list[-1]))
        self.i24_label.setText(str(self.i24_list[-1]))


    def reset(self):
        self.ch01.reset()
        self.ch02.reset()
        self.ch03.reset()
        self.ch04.reset()
        self.v24_list = []
        self.i24_list = []


