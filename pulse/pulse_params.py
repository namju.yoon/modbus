import sys
import serial
import time 

from PyQt5.QtWidgets import QWidget, QLabel, QLineEdit, QPushButton, QDialogButtonBox
from PyQt5.QtWidgets import QGroupBox, QBoxLayout, QVBoxLayout, QHBoxLayout, QGridLayout
from PyQt5.QtWidgets import QApplication, QDialog, QRadioButton, QSpinBox, QDoubleSpinBox
from PyQt5 import QtCore
from PyQt5.QtCore import QDate, Qt
from PyQt5.QtGui import QPixmap, QFont

from . import pulse_serial as dvc
from ctypes import c_uint16

class PulseParams(QDialog):
    def __init__(self, Dialog, client,  ptype, initial):
        super().__init__()
        self.client = client
        self.ptype = ptype
        self.data = initial
        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        self.setGeometry(100, 100, 400, 410)
        font = QFont()
        font.setPointSize(12)
    
        # Main 
        main_layer = QVBoxLayout()
        self.setLayout(main_layer)

        # 통신 포트  설정(Serial Port)"
        group_setting = QGroupBox("Setting Parameters")
        grid_box = QGridLayout()

        self.voltage1 = QSpinBox()
        self.voltage1.valueChanged.connect(lambda:self.spinValueChanged(self.voltage1, 'VOLTAGE1'))
        self.displayComponent(grid_box, self.voltage1, -5000, 5000, 10, "SET OUT VOLTAGE1:", 0, "V")

        self.current1 = QDoubleSpinBox()
        self.current1.valueChanged.connect(
                    lambda:self.spinValueChanged(self.current1, 'CURRENT1'))
        self.displayComponent(grid_box, 
                    self.current1, 0, 10.00, 0.1, "SET OUT CURRENT1:", 1, "mA")

        self.voltage2 = QSpinBox()
        self.voltage2.valueChanged.connect(
                    lambda:self.spinValueChanged(self.voltage2, 'VOLTAGE2'))
        self.displayComponent(grid_box, 
                    self.voltage2, -5000, 5000, 10, "SET OUT VOLTAGE2:", 2, "V")

        self.current2 = QDoubleSpinBox()
        self.current2.valueChanged.connect(
                    lambda:self.spinValueChanged(self.current2, 'CURRENT2'))
        self.displayComponent(grid_box, 
                    self.current2, 0, 10.00, 0.1, "SET OUT CURRENT2:", 3, "mA")

        self.leak_fault_level = QDoubleSpinBox()
        self.leak_fault_level.valueChanged.connect(
                    lambda:self.spinValueChanged(self.leak_fault_level, 'LEAKFAULT'))
        self.displayComponent(grid_box, 
                    self.leak_fault_level, 0, 1.00, 0.1, "SET LEAK FAULT LEVEL:", 4, "mA")
        
        self.ro_min_fault = QDoubleSpinBox()
        self.ro_min_fault.valueChanged.connect(
                    lambda:self.spinValueChanged(self.ro_min_fault, 'ROMIN'))
        self.displayComponent(grid_box, 
                    self.ro_min_fault, 1.0, 999.9, 0.3, "SET RO MIN FALUT:", 5, "Kohm")
        
        self.up_time = QDoubleSpinBox()
        self.up_time.valueChanged.connect(
                    lambda:self.spinValueChanged(self.up_time, 'UPTIME'))
        self.displayComponent(grid_box, 
                    self.up_time, 0.30, 9.90, 0.3, "RAMP UP TIME:", 6, "sec")
        
        self.down_time = QDoubleSpinBox()
        self.down_time.valueChanged.connect(
                    lambda:self.spinValueChanged(self.down_time, 'DOWNTIME'))
        self.displayComponent(grid_box, 
                    self.down_time, 0.30, 9.90, 0.1, "RAMP DOWN TIME:", 7, "sec")
    
        ### Toggle 
        self.rd_mode_off = QRadioButton("OFF")
        self.rd_mode_off.setChecked(True)
        self.rd_mode_off.clicked.connect(
                    lambda:self.radioValueClicked(self.rd_mode_off, "MODE"))
        self.rd_mode_on = QRadioButton("ON")
        self.rd_mode_on.clicked.connect(
                    lambda:self.radioValueClicked(self.rd_mode_on, "MODE"))
        self.mode_value = 0
        self.displayRadioBox(grid_box, "AUTO TOGGLE MODE:", self.rd_mode_off, 
                    self.rd_mode_on, "(O:OFF, 1:ON)", 8)


        self.toggle_count = QDoubleSpinBox()
        self.toggle_count.valueChanged.connect(lambda:self.spinValueChanged(self.toggle_count, 'TOGGLECOUNT'))
        self.displayComponent(grid_box, self.toggle_count, 1, 99, 1, "AUTO TOGGLE COUNT:", 9, "")

        self.slope = QDoubleSpinBox()
        self.slope.valueChanged.connect(lambda:self.spinValueChanged(self.slope, 'SLOPE'))
        self.displayComponent(grid_box, self.slope, 1.0, 10.0, 0.1, "SET SLOPE:", 10, "")

        self.coeff = QDoubleSpinBox()
        self.coeff.valueChanged.connect(lambda:self.spinValueChanged(self.coeff, 'COEFF'))
        self.displayComponent(grid_box, self.coeff, 0.1, 1.0, 0.1, "SET COEFF:", 11, "")

        ### INTERNAL/REMOTE
        self.rd_select_internal = QRadioButton("INTERNAL")
        self.rd_select_internal.setChecked(True)
        self.rd_select_internal.clicked.connect(lambda:self.radioValueClicked(self.rd_select_internal, "SELECT"))
        self.rd_select_remote = QRadioButton("REMOTE")
        self.rd_select_remote.clicked.connect(lambda:self.radioValueClicked(self.rd_select_remote, "SELECT"))
        self.select_value = 0
        self.displayRadioBox(grid_box, "ON/OFF SELECTION", self.rd_select_internal, 
                    self.rd_select_remote, "(O:INTERNAL, 1:REMOTE)", 12)


        self.local_address = QDoubleSpinBox()
        self.local_address.valueChanged.connect(lambda:self.spinValueChanged(self.local_address, 'LOCALADD'))
        self.displayComponent(grid_box, self.local_address, 1, 31, 1, "LOCAL ADDRESS:", 13, "")

        self.arc_delay = QDoubleSpinBox()
        self.arc_delay.valueChanged.connect(lambda:self.spinValueChanged(self.arc_delay, 'ARCDELAY'))
        self.displayComponent(grid_box, self.arc_delay, 10.00, 50.00, 1, "ARC DELAY:", 14, "m sec")
        
        self.arc_rate = QDoubleSpinBox()
        self.arc_rate.valueChanged.connect(lambda:self.spinValueChanged(self.arc_rate, 'ARCRATE'))
        self.displayComponent(grid_box, self.arc_rate, 1, 1000, 5, "ARC RATE:", 15, "a/s")

        # Third Region
        self.rd_toggle_off = QRadioButton("FORWARD")
        self.rd_toggle_off.setChecked(True)
        self.rd_toggle_off.clicked.connect(lambda:self.radioValueClicked(self.rd_toggle_off, "TOGGLE"))
        self.rd_toggle_on = QRadioButton("REVERSE")
        self.rd_toggle_on.clicked.connect(lambda:self.radioValueClicked(self.rd_toggle_on, "TOGGLE"))
        self.toggle_value = 0
        self.displayRadioBox(grid_box, "TOGGLE :", self.rd_toggle_off, 
                    self.rd_toggle_on, "(0:FORWARD, 1:REVERSE)", 16)


        self.rd_arc_off = QRadioButton("OFF")
        self.rd_arc_off.setChecked(True)
        self.rd_arc_off.clicked.connect(lambda:self.radioValueClicked(self.rd_arc_off, "ARC"))
        self.rd_arc_on = QRadioButton("ON")
        self.rd_arc_on.clicked.connect(lambda:self.radioValueClicked(self.rd_arc_on, "ARC"))
        self.arc_value = 0
        self.displayRadioBox(grid_box, "ARC CONTROL :(O:OFF, 1:ON)", self.rd_arc_off, 
                    self.rd_arc_on, "(O:OFF, 1:ON)", 17)

        self.rd_ocp_off = QRadioButton("OFF")
        self.rd_ocp_off.setChecked(True)
        self.rd_ocp_off.clicked.connect(lambda:self.radioValueClicked(self.rd_ocp_off, "OCP"))
        self.rd_ocp_on = QRadioButton("ON")
        self.rd_ocp_on.clicked.connect(lambda:self.radioValueClicked(self.rd_ocp_on, "OCP"))
        self.ocp_value = 0
        self.displayRadioBox(grid_box, "OCP CONTROL:", self.rd_ocp_off, 
                    self.rd_ocp_on, "(O:OFF, 1:ON)", 18)


        self.target_cap = QDoubleSpinBox()
        self.target_cap.valueChanged.connect(lambda:self.spinValueChanged(self.target_cap, 'TAGETCAP'))
        self.displayComponent(grid_box, self.target_cap, 1, 15000, 100, "TARGET CAPACITOR:", 19, "pF")

        self.cap_deviation = QDoubleSpinBox()
        self.cap_deviation.valueChanged.connect(lambda:self.spinValueChanged(self.target_cap, 'CAPDEVI'))
        self.displayComponent(grid_box, self.cap_deviation, 0, 100, 1, "CAP DEVIATION:", 20, "%")

        group_setting.setLayout(grid_box)
        main_layer.addWidget(group_setting)

        self.display_value()
        self.voltage1_changed = False
        self.current1_changed = False
        self.voltage2_changed = False
        self.current2_changed = False
        self.leak_fault_level_changed = False
        self.ro_min_fault_changed = False
        self.up_time_changed = False
        self.down_time_changed = False
        self.mode_changed = False
        self.toggle_count_changed = False
        self.slope_changed = False
        self.coeff_changed = False
        self.select_changed = False
        self.local_address_changed = False
        self.arc_delay_changed = False
        self.arc_rate_changed = False
        self.toggle_changed = False
        self.arc_changed = False
        self.ocp_changed = False
        self.target_cap_changed = False
        self.cap_deviation_changed = False

        self.buttonBox = QDialogButtonBox()
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.addButton('MODIFY PARAMS', QDialogButtonBox.AcceptRole)
        self.buttonBox.addButton('CANCEL', QDialogButtonBox.RejectRole)
        self.buttonBox.addButton('DATA CLEAR', QDialogButtonBox.ResetRole)
        main_layer.addWidget(self.buttonBox)

        self.buttonBox.accepted.connect(self.on_accepted)
        self.buttonBox.rejected.connect(self.reject)

    def displayComponent(self, grid, com, vmin, vmax, step, label, nth, unit):
        com.setRange(vmin, vmax)
        com.setSingleStep(step)
        grid.addWidget(QLabel(label), nth, 0)
        grid.addWidget(com, nth, 1, 1, 2)
        grid.addWidget(QLabel(f"{vmin} ~ {vmax}"), nth, 3)
        grid.addWidget(QLabel(unit), nth, 4)

    def displayRadioBox(self, grid_box, title, rd1, rd2, reference, nth):
        group_mode = QGroupBox("")
        mode_layout = QHBoxLayout()
        group_mode.setLayout(mode_layout)
        mode_layout.addWidget(rd1)
        mode_layout.addWidget(rd2)
        grid_box.addWidget(QLabel(title), nth, 0)
        grid_box.addWidget(group_mode, nth, 1, 1, 2)
        grid_box.addWidget(QLabel(reference), nth, 3)


    def display_value(self):
        if self.data:
            self.voltage1.setValue(self.data["voltage1"])
            self.voltage2.setValue(self.data["voltage2"])
            self.current1.setValue(self.data["current1"])            
            self.current2.setValue(self.data["current2"])

            self.leak_fault_level.setValue(self.data["leak_fault_level"])
            self.ro_min_fault.setValue(self.data["ro_min_fault"])
            self.up_time.setValue(self.data["up_time"])
            self.down_time.setValue(self.data["down_time"])

            self.toggle_count.setValue(self.data["toggle_count"])
            self.slope.setValue(self.data["slope"])
            self.coeff.setValue(self.data["coeff"])

            self.local_address.setValue(self.data["local_address"])
            self.arc_delay.setValue(self.data["arc_delay"])
            self.arc_rate.setValue(self.data["arc_rate"])
            self.target_cap.setValue(self.data["target_cap"])
            self.cap_deviation.setValue(self.data["cap_deviation"])

            if self.data['rd_mode']:
                self.rd_mode_on.setChecked(True)
                self.rd_mode_off.setChecked(False)
            else:
                self.rd_mode_off.setChecked(True)
                self.rd_mode_on.setChecked(False)

            if self.data['rd_select']:
                self.rd_select_internal.setChecked(True)
                self.rd_select_remote.setChecked(False)
            else:
                self.rd_select_remote.setChecked(True)
                self.rd_select_internal.setChecked(False)

            if self.data['rd_toggle']:
                self.rd_toggle_on.setChecked(True)
                self.rd_toggle_off.setChecked(False)
            else:
                self.rd_toggle_off.setChecked(True)
                self.rd_toggle_on.setChecked(False)

            if self.data['rd_arc']:
                self.rd_arc_on.setChecked(True)
                self.rd_arc_off.setChecked(False)
            else:
                self.rd_arc_off.setChecked(True)
                self.rd_arc_on.setChecked(False)

            if self.data['rd_ocp']:
                self.rd_ocp_on.setChecked(True)
                self.rd_ocp_off.setChecked(False)
            else:
                self.rd_ocp_off.setChecked(True)
                self.rd_ocp_on.setChecked(False)

        else:
            print("No data !!")

    def on_accepted(self):
        if self.voltage1_changed:
            value = int(self.voltage1.value())
            # val = c_uint16(value).value
            dvc.write_registers(self.client, dvc.WRITE_VOLTAGE1, value, self.ptype)
            self.voltage1_changed = False

        if self.current1_changed:
            value = int(self.current1.value()) * 1000
            res = dvc.write_registers(self.client, dvc.WRITE_CURRENT1, value, self.ptype)
            self.current1_changed = False

        if self.voltage2_changed:
            value = int(self.voltage2.value())
            # val = c_uint16(value).value
            res = dvc.write_registers(self.client, dvc.WRITE_VOLTAGE2, value, self.ptype)
            self.voltage2_changed = False

        if self.current2_changed:
            value = int(self.current2.value()) * 1000
            res = dvc.write_registers(self.client, dvc.WRITE_CURRENT2, value, self.ptype)
            self.current2_changed  = False

        if self.leak_fault_level_changed:
            value = int(self.leak_fault_level.value()) * 1000
            res = dvc.write_registers(self.client, dvc.WRITE_LEAK_LEVEL, value, self.ptype)
            self.leak_fault_level_changed = False

        if self.ro_min_fault_changed:
            value = int(self.ro_min_fault.value()) * 1000
            res = dvc.write_registers(self.client, dvc.WRITE_RO_MIN_FAULT, value, self.ptype)
            self.ro_min_fault_changed = False

        if self.up_time_changed:
            value = int(self.up_time.value()) * 10
            # value = c_uint16(value).value
            res = dvc.write_registers(self.client, dvc.WRITE_RAMP_UP_TIME, value, self.ptype)
            self.up_time_changed = False

        if self.down_time_changed:
            value = int(self.down_time.value()) * 10
            res = dvc.write_registers(self.client, dvc.WRITE_RAMP_DOWN_TIME, value, self.ptype)
            self.down_time_changed = False

        if self.mode_changed:
            value = self.mode_value
            res = dvc.write_registers(self.client, dvc.WRITE_TOGGLE_MODE, value, self.ptype)
            self.mode_changed = False

        if self.toggle_count_changed:
            value = int(self.toggle_count.value())
            res = dvc.write_registers(self.client, dvc.WRITE_TOGGLE_COUNT, value, self.ptype)
            self.toggle_count_changed = False

        if self.slope_changed:
            value = int(self.slope.value()) * 10
            res = dvc.write_registers(self.client, dvc.WRITE_SET_SLOPE, value, self.ptype)
            self.slope_changed = False
        
        if self.coeff_changed:
            value = int(self.coeff.value() * 10)
            res = dvc.write_registers(self.client, dvc.WRITE_SET_COEFF, value, self.ptype)
            self.coeff_changed = False

        if self.select_changed:
            value = self.select_value
            res = dvc.write_registers(self.client, dvc.WRITE_TOGGLE, value, self.ptype)
            self.select_changed = False

        if self.local_address_changed:
            value = int(self.local_address.value())
            res = dvc.write_registers(self.client, dvc.WRITE_LOCAL_ADDRESS, value, self.ptype)
            self.local_address_changed = False

        if self.arc_delay_changed:
            value = int(self.arc_delay.value()) * 100
            res = dvc.write_registers(self.client, dvc.WRITE_ARC_DELAY, value, self.ptype)
            self.arc_delay_changed = False

        if self.arc_rate_changed:
            value = int(self.arc_rate.value())
            res = dvc.write_registers(self.client, dvc.WRITE_ARC_RATE, value, self.ptype)
            self.arc_rate_changed = False

        if self.toggle_changed:
            value = self.toggle_value
            res = dvc.write_registers(self.client, dvc.WRITE_ONOFF_SELECT, value, self.ptype)
            self.toggle_changed = False

        if self.arc_changed:
            value = self.arc_value
            res = dvc.write_registers(self.client, dvc.WRITE_ARC_CONTROL, value, self.ptype)
            self.arc_changed = False

        if self.ocp_changed:
            value = self.ocp_value
            res = dvc.write_registers(self.client, dvc.WRITE_OCP_CONTROL, value, self.ptype)
            self.ocp_changed = False

        if self.target_cap_changed:
            value = int(self.target_cap.value())
            res = dvc.write_registers(self.client, dvc.WRITE_TARGET_CAP, value, self.ptype)
            self.target_cap_changed = False

        if self.cap_deviation_changed:
            value = int(self.cap_deviation.value())
            res = dvc.write_registers(self.client, dvc.WRITE_CAP_DEVIATION, value, self.ptype)
            self.cap_deviation_changed = False

        self.voltage1_changed = False
        self.current1_changed = False
        self.voltage2_changed = False
        self.current2_changed = False
        self.leak_fault_level_changed = False
        self.ro_min_fault_changed = False
        self.up_time_changed = False
        self.down_time_changed = False
        self.mode_changed = False
        self.toggle_count_changed = False
        self.slope_changed = False
        self.coeff_changed = False
        self.select_changed = False
        self.local_address_changed = False
        self.arc_delay_changed = False
        self.arc_rate_changed = False
        self.toggle_changed = False
        self.arc_changed = False
        self.ocp_changed = False
        self.target_cap_changed = False
        self.cap_deviation_changed = False

        self.accept()

    def radioValueClicked(self, btn, mode):
        if mode == 'MODE':
            self.mode_changed = True
            if btn.text() == 'OFF':
                self.mode_value = 0
            else:
                self.mode_value = 1
        elif mode == 'SELECT':
            self.select_changed = True
            if btn.text() == 'OFF':
                self.select_value = 0
            else:
                self.select_value = 1
        elif mode == 'TOGGLE':
            self.toggle_changed = True
            if btn.text() == 'FORWARD':
                self.toggle_value = 0
            else:
                self.toggle_value = 1
        elif mode == 'ARC':
            self.arc_changed = True
            if btn.text() == 'OFF':
                self.arc_value = 0
            else:
                self.arc_value = 1
        elif mode == 'OCP':
            self.ocp_changed = True
            if btn.text() == 'OFF':
                self.ocp_value = 0
            else:
                self.ocp_value = 1

    def spinValueChanged(self, edit, mode):
        if mode == 'VOLTAGE1':
            self.voltage1_changed = True
        elif mode == 'CURRENT1':
            self.current1_changed = True
        elif mode == 'VOLTAGE2':
            self.voltage2_changed = True
        elif mode == 'CURRENT2':
            self.current2_changed = True
        elif mode == 'LEAKFAULT':
            self.leak_fault_level_changed = True
        elif mode == 'ROMIN':
            self.ro_min_fault_changed = True
        elif mode == 'UPTIME':
            self.up_time_changed = True
        elif mode == 'DOWNTIME':
            self.down_time_changed = True
        elif mode == 'TOGGLECOUNT':
            self.toggle_count_changed = True
        elif mode == 'SLOPE':
            self.slope_changed = True
        elif mode == 'COEFF':
            self.coeff_changed = True
        elif mode == 'LOCALADD':
            self.local_address_changed = True
        elif mode == 'ARCDELAY':
            self.arc_delay_changed = True
        elif mode == 'ARCRATE':
            self.arc_rate_changed = True
        elif mode == 'TAGETCAP':
            self.target_cap_changed = True
        elif mode == 'CAPDEVI':
            self.cap_deviation_changed = True


if __name__ == "__main__":
    app = QApplication(sys.argv)
    Dialog = QDialog()
    client = None
    mywindow = ESCComm(Dialog, client)
    mywindow.show()
    app.exec_()