import time
from datetime import datetime
from ctypes import c_int16

from PyQt5.QtWidgets import QWidget, QLabel, QPushButton, QDesktopWidget
from PyQt5.QtWidgets import QGroupBox, QVBoxLayout, QHBoxLayout, QGridLayout
from PyQt5.QtWidgets import QApplication, QDialog, QTabWidget
from PyQt5.QtWidgets import QLCDNumber, QStatusBar, QMainWindow
from PyQt5.QtCore import QDate, Qt, QTimer
from PyQt5.QtGui import QPixmap
import pyqtgraph as pyGraph

# import magnetic.serial as device
import magnetic.serial_v2 as device
from magnetic.setup_device import SettingWin
from magnetic.common import SetVal, resource_path, LineView, CheckField
from magnetic.basic_v2 import ModuleStatus2, ModuleFault, DeviceAlarm, HeadAlarm
from magnetic.basic_v2 import ValueSetting, VUModule, CLed, WarningDialog
from magnetic.address_v2 import FAULT_NAMES

TPERIOD = 1000
DATA_TIME = 2000
FAULT_TIME = 5000
GRAPH_PERIOD = 5000

RTU = 'RTU'
TCP = 'TCP'

import logging
FORMAT = ('%(threadName)-15s '
          '%(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.INFO)

STOP = 0
RUN = 1

class MagneticMonitoring(QMainWindow):
    def __init__(self):
        super().__init__()

        self.gen_port = None
        self.countN = 0
        self.client = None
        # self.client = True
        self.used_ports = []

        self.ptype = TCP
        self.com_open_flag = False
        pyGraph.setConfigOptions(background='w')  # 흰색 배경 

        self.btn_default = "QPushButton{font-size: 12pt; color: gray; background-color: #ddd;}"
        self.btn_alarm = "QPushButton{font-size: 12pt; font-weight: bold; color: blue; background-color: white;}"
        self.run_style = "border: 3px solid lightgray;border-radius: 40px;background-color: blue;color: white;font-size: 16px;font-weight: bold;"
        self.stop_style = "border: 3px solid lightgray;border-radius: 40px;background-color: gray;color: white;font-size: 16px;font-weight: bold;"
        self.main_label = "QLabel{font-size: 20pt; font-weight: bold}"
        self.font_green = 'QLabel{margin: -1px; font-size: 16pt; padding: 0px; color: green;font-weight: bold;}'
        self.font_red = 'QLabel{margin: -1px; font-size: 16pt; padding: 0px; color: red;font-weight: bold;}'

        self.indexs = []

        self.time = 0
        self.timer = QTimer()
        self.dtimer = QTimer()
        self.faulttimer = QTimer()
        self.graphtimer = QTimer()
        self.start_time = None

        self.run_flag = False 

        self.initUI()


    def initUI(self):
        self.setWindowTitle("PSTEK MAGNETIC MONITORING")
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width(), screensize.height()) 

        mainwidget = QWidget()              # 위젯의 인스턴스 생성만으로도 QMainWindow에 붙는다.
        self.setCentralWidget(mainwidget)
        main_layout = QVBoxLayout()
        mainwidget.setLayout(main_layout)

        ################### MENU
        grp_menu = QGroupBox("")
        grp_menu.setStyleSheet("QGroupBox{margin:2px;padding:2px;}")
        layout_menu = QGridLayout()
        grp_menu.setLayout(layout_menu)
        
        #### 통신 체크 
        grp_comm = QGroupBox("")
        grp_comm.setStyleSheet("QGroupBox{margin:2px;padding:2px;}")
        layout_comm = QVBoxLayout()
        grp_comm.setLayout(layout_comm)

        self.btn_com_gen = QPushButton("전원 장치 연결")
        self.btn_com_gen.clicked.connect(self.setting_device)
        self.lbl_com_gen = QLabel("")
        self.lbl_com_gen.setStyleSheet(self.font_green)

        layout_comm.addWidget(self.btn_com_gen)
        layout_comm.addWidget(self.lbl_com_gen)
        layout_menu.addWidget(grp_comm, 0, 0)

        #### Title
        grp_title = QGroupBox("")
        layout_title = QVBoxLayout()
        grp_title.setLayout(layout_title)

        title1 = QLabel("MAGNETIC")
        title2 = QLabel("MONITORING")
        title1.setStyleSheet(self.main_label)
        title2.setStyleSheet(self.main_label)
        layout_title.addWidget(title1)
        layout_title.addWidget(title2)

        layout_menu.addWidget(grp_title, 0, 1)

        ####  RUN/STOP 
        grp_run = QGroupBox("")
        layout_run = QHBoxLayout()
        grp_run.setLayout(layout_run)

        self.btn_run1 = CLed(1)
        self.run_flag1 = False
        self.btn_run1.clicked.connect(lambda:self.start_module(1, self.btn_run1))
        layout_run.addWidget(self.btn_run1)
        self.btn_run2 = CLed(2)
        self.run_flag2 = False
        self.btn_run2.clicked.connect(lambda:self.start_module(2, self.btn_run2))
        layout_run.addWidget(self.btn_run2)
        self.btn_run3 = CLed(3)
        self.run_flag3 = False
        self.btn_run3.clicked.connect(lambda:self.start_module(3, self.btn_run3))
        layout_run.addWidget(self.btn_run3)
        self.btn_run4 = CLed(4)
        self.run_flag4 = False
        self.btn_run4.clicked.connect(lambda:self.start_module(4, self.btn_run4))
        layout_run.addWidget(self.btn_run4)
        self.btn_run5 = CLed(5)
        self.run_flag5 = False
        self.btn_run5.clicked.connect(lambda:self.start_module(5, self.btn_run5))
        layout_run.addWidget(self.btn_run5)
        layout_menu.addWidget(grp_run, 0, 2, 1, 3)

        ####  RUN/STOP 
        grp_runstop = QGroupBox("")
        layout_runstop = QVBoxLayout()
        grp_runstop.setLayout(layout_runstop)

        self.btn_device_run = QPushButton("RUN")
        self.btn_device_run.setStyleSheet(self.stop_style)
        self.btn_device_run.clicked.connect(self.magnetic_device_run)

        self.btn_device_stop = QPushButton("STOP")
        self.btn_device_stop.setStyleSheet(self.stop_style)
        self.btn_device_stop.clicked.connect(self.magnetic_device_stop)

        layout_runstop.addWidget(self.btn_device_run)
        layout_runstop.addWidget(self.btn_device_stop)

        layout_menu.addWidget(grp_runstop, 0, 7)

        #### ON/OFF 
        grp_btn = QGroupBox("")
        layout_btn = QGridLayout()
        grp_btn.setLayout(layout_btn)

        self.data_time = SetVal("데이터 간격", DATA_TIME, "m sec", layout_btn, 0)
        self.fault_time = SetVal("fault 간격", FAULT_TIME, "m sec", layout_btn, 1)

        layout_menu.addWidget(grp_btn, 0, 8)

        #### TIME 
        grp_time = QGroupBox("")
        layout_time = QVBoxLayout()
        grp_time.setLayout(layout_time)

        # Timer Display
        self.display_time = QLCDNumber()
        self.display_time.setDigitCount(8)
        self.display_time.setStyleSheet('background: white')
        timedisplay = '{:02d}:{:02d}:{:02d}'.format(
                (self.time // 60) // 60, (self.time // 60) % 60, self.time % 60)
        self.display_time.display(timedisplay)

        layout_time.addWidget(self.display_time)
        layout_menu.addWidget(grp_time, 0, 9)


        grp_setup = QGroupBox("")
        layout_setup = QVBoxLayout()
        grp_setup.setLayout(layout_setup)

        self.btn_faultclear = QPushButton("FAULT CLEAR")
        self.btn_faultclear.clicked.connect(self.fault_clear)
        layout_setup.addWidget(self.btn_faultclear)

        # self.btn_graphdata = QPushButton("RESET")
        # self.btn_graphdata.clicked.connect(self.reset_device)
        # layout_setup.addWidget(self.btn_graphdata)

        self.label_start_time = QLabel("")
        layout_setup.addWidget(self.label_start_time)
        layout_menu.addWidget(grp_setup, 0, 10)

        # Logo
        grp_logo = QGroupBox("")
        layout_logo = QVBoxLayout()
        grp_logo.setLayout(layout_logo)


        labelLogo = QLabel("")
        pixmap = QPixmap(resource_path("logo.png"))
        labelLogo.setAlignment(Qt.AlignCenter)
        labelLogo.setPixmap(pixmap)
        layout_logo.addWidget(labelLogo)

        
        self.message = QLabel("")
        self.message.setStyleSheet(self.font_green)
        layout_logo.addWidget(self.message)
        layout_menu.addWidget(grp_logo, 0, 11)

        main_layout.addWidget(grp_menu)

    ##### MODULE FAULT
        grp_fault = QGroupBox("")
        layout_fault = QHBoxLayout()
        grp_fault.setLayout(layout_fault)

        self.md1_fault = CheckField("MD #1", "NORMAL", "FAULT")
        layout_fault.addWidget(self.md1_fault.groupdisplay())
        self.md2_fault = CheckField("MD #2", "NORMAL", "FAULT")
        layout_fault.addWidget(self.md2_fault.groupdisplay())
        self.md3_fault = CheckField("MD #3", "NORMAL", "FAULT")
        layout_fault.addWidget(self.md3_fault.groupdisplay())
        self.md4_fault = CheckField("MD #4", "NORMAL", "FAULT")
        layout_fault.addWidget(self.md4_fault.groupdisplay())
        self.md5_fault = CheckField("MD #5", "NORMAL", "FAULT")
        layout_fault.addWidget(self.md5_fault.groupdisplay())
        
        main_layout.addWidget(grp_fault)

    ##### TAB Layout
        # Tab 생성
        tab_module = QWidget()
        tab_alarm = QWidget()
        tab_change = QWidget()
        tab_graph_1 = QWidget()
        tab_graph_2 = QWidget()
        tab_graph_3 = QWidget()
        tab_graph_4 = QWidget()
        tab_graph_5 = QWidget()

        tabs = QTabWidget()
        tabs.addTab(tab_module, 'MODULES')
        tabs.addTab(tab_alarm, 'ALARM')
        tabs.addTab(tab_change, 'VALUE SETTING')
        tabs.addTab(tab_graph_1, 'GRAPH MOD #1')
        tabs.addTab(tab_graph_2, 'GRAPH MOD #2')
        tabs.addTab(tab_graph_3, 'GRAPH MOD #3')
        tabs.addTab(tab_graph_4, 'GRAPH MOD #4')
        tabs.addTab(tab_graph_5, 'GRAPH MOD #5')
        main_layout.addWidget(tabs)

        ## 4th ########################
        # Status
        self.statusbar = QStatusBar()
        self.setStatusBar(self.statusbar)
        self.statusbar.setObjectName("statusbar")
        
        self.statusmessage = 'PSTEK, {},  {}'
        now = QDate.currentDate()
        displaymessage = self.statusmessage.format(now.toString(Qt.ISODate), 'PSTEK is aiming for a global leader. !!')
        self.statusbar.showMessage(displaymessage)

        self.displayModules(tab_module)
        self.displayAlarm(tab_alarm)
        self.displayValueChange1(tab_change)
        self.displayGraph_1(tab_graph_1)
        self.displayGraph_2(tab_graph_2)
        self.displayGraph_3(tab_graph_3)
        self.displayGraph_4(tab_graph_4)
        self.displayGraph_5(tab_graph_5)

        self.show()


    def displayModules(self, tab):
        main_layer = QHBoxLayout()
        tab.setLayout(main_layer)

        self.mod1 = ModuleStatus2('#1 MODULE', bcolor="#F2D7D5")
        group1 = self.mod1.display()
        main_layer.addWidget(group1)

        self.mod2 = ModuleStatus2('#2 MODULE', bcolor="#D4E6F1")
        group2 = self.mod2.display()
        main_layer.addWidget(group2)

        self.mod3 = ModuleStatus2('#3 MODULE', bcolor="#FCF3CF")
        group3 = self.mod3.display()
        main_layer.addWidget(group3)

        self.mod4 = ModuleStatus2('#4 MODULE', bcolor="#E8F8F5")
        group4 = self.mod4.display()
        main_layer.addWidget(group4)

        self.mod5 = ModuleStatus2('#5 MODULE', bcolor="#f9e79f")
        group5 = self.mod5.display()
        main_layer.addWidget(group5)


    def displayGraph_1(self, tab):
        main_layer = QGridLayout()
        tab.setLayout(main_layer)

        self.view_vdc_1 = LineView('VDC', self.indexs, [], '#DE3163')
        self.view_idc_1 = LineView('IDC', self.indexs, [], '#FF7F50')
        self.view_ir_1 = LineView('IR', self.indexs, [], '#008080')
        self.view_io_1 = LineView('IO', self.indexs, [], '#6495ED')
        self.view_io_avg_1 = LineView('IO_AVG', self.indexs, [], '#f9e79f')

        main_layer.addWidget(self.view_vdc_1.btn, 0, 0)
        main_layer.addWidget(self.view_vdc_1.plotWidget, 0, 1)

        main_layer.addWidget(self.view_idc_1.btn, 1, 0)
        main_layer.addWidget(self.view_idc_1.plotWidget, 1, 1)
        
        main_layer.addWidget(self.view_ir_1.btn, 2, 0)
        main_layer.addWidget(self.view_ir_1.plotWidget, 2, 1)
        
        main_layer.addWidget(self.view_io_1.btn, 3, 0)
        main_layer.addWidget(self.view_io_1.plotWidget, 3, 1)
        
        main_layer.addWidget(self.view_io_avg_1.btn, 4, 0)
        main_layer.addWidget(self.view_io_avg_1.plotWidget, 4, 1)


    def displayGraph_2(self, tab):
        main_layer = QGridLayout()
        tab.setLayout(main_layer)

        self.view_vdc_2 = LineView('VDC', self.indexs, [], '#DE3163')
        self.view_idc_2 = LineView('IDC', self.indexs, [], '#FF7F50')
        self.view_ir_2 = LineView('IR', self.indexs, [], '#008080')
        self.view_io_2 = LineView('IO', self.indexs, [], '#6495ED')
        self.view_io_avg_2 = LineView('IO_AVG', self.indexs, [], '#f9e79f')

        main_layer.addWidget(self.view_vdc_2.btn, 0, 0)
        main_layer.addWidget(self.view_vdc_2.plotWidget, 0, 1)

        main_layer.addWidget(self.view_idc_2.btn, 1, 0)
        main_layer.addWidget(self.view_idc_2.plotWidget, 1, 1)
        
        main_layer.addWidget(self.view_ir_2.btn, 2, 0)
        main_layer.addWidget(self.view_ir_2.plotWidget, 2, 1)
        
        main_layer.addWidget(self.view_io_2.btn, 3, 0)
        main_layer.addWidget(self.view_io_2.plotWidget, 3, 1)
        
        main_layer.addWidget(self.view_io_avg_2.btn, 4, 0)
        main_layer.addWidget(self.view_io_avg_2.plotWidget, 4, 1)


    def displayGraph_3(self, tab):
        main_layer = QGridLayout()
        tab.setLayout(main_layer)

        self.view_vdc_3 = LineView('VDC', self.indexs, [], '#DE3163')
        self.view_idc_3 = LineView('IDC', self.indexs, [], '#FF7F50')
        self.view_ir_3 = LineView('IR', self.indexs, [], '#008080')
        self.view_io_3 = LineView('IO', self.indexs, [], '#6495ED')
        self.view_io_avg_3 = LineView('IO_AVG', self.indexs, [], '#f9e79f')

        main_layer.addWidget(self.view_vdc_3.btn, 0, 0)
        main_layer.addWidget(self.view_vdc_3.plotWidget, 0, 1)

        main_layer.addWidget(self.view_idc_3.btn, 1, 0)
        main_layer.addWidget(self.view_idc_3.plotWidget, 1, 1)
        
        main_layer.addWidget(self.view_ir_3.btn, 2, 0)
        main_layer.addWidget(self.view_ir_3.plotWidget, 2, 1)
        
        main_layer.addWidget(self.view_io_3.btn, 3, 0)
        main_layer.addWidget(self.view_io_3.plotWidget, 3, 1)
        
        main_layer.addWidget(self.view_io_avg_3.btn, 4, 0)
        main_layer.addWidget(self.view_io_avg_3.plotWidget, 4, 1)


    def displayGraph_4(self, tab):
        main_layer = QGridLayout()
        tab.setLayout(main_layer)

        self.view_vdc_4 = LineView('VDC', self.indexs, [], '#DE3163')
        self.view_idc_4 = LineView('IDC', self.indexs, [], '#FF7F50')
        self.view_ir_4 = LineView('IR', self.indexs, [], '#008080')
        self.view_io_4 = LineView('IO', self.indexs, [], '#6495ED')
        self.view_io_avg_4 = LineView('IO_AVG', self.indexs, [], '#f9e79f')

        main_layer.addWidget(self.view_vdc_4.btn, 0, 0)
        main_layer.addWidget(self.view_vdc_4.plotWidget, 0, 1)

        main_layer.addWidget(self.view_idc_4.btn, 1, 0)
        main_layer.addWidget(self.view_idc_4.plotWidget, 1, 1)
        
        main_layer.addWidget(self.view_ir_4.btn, 2, 0)
        main_layer.addWidget(self.view_ir_4.plotWidget, 2, 1)
        
        main_layer.addWidget(self.view_io_4.btn, 3, 0)
        main_layer.addWidget(self.view_io_4.plotWidget, 3, 1)
        
        main_layer.addWidget(self.view_io_avg_4.btn, 4, 0)
        main_layer.addWidget(self.view_io_avg_4.plotWidget, 4, 1)


    def displayGraph_5(self, tab):
        main_layer = QGridLayout()
        tab.setLayout(main_layer)

        self.view_vdc_5 = LineView('VDC', self.indexs, [], '#DE3163')
        self.view_idc_5 = LineView('IDC', self.indexs, [], '#FF7F50')
        self.view_ir_5 = LineView('IR', self.indexs, [], '#008080')
        self.view_io_5 = LineView('IO', self.indexs, [], '#6495ED')
        self.view_io_avg_5 = LineView('IO_AVG', self.indexs, [], '#f9e79f')

        main_layer.addWidget(self.view_vdc_5.btn, 0, 0)
        main_layer.addWidget(self.view_vdc_5.plotWidget, 0, 1)

        main_layer.addWidget(self.view_idc_5.btn, 1, 0)
        main_layer.addWidget(self.view_idc_5.plotWidget, 1, 1)
        
        main_layer.addWidget(self.view_ir_5.btn, 2, 0)
        main_layer.addWidget(self.view_ir_5.plotWidget, 2, 1)
        
        main_layer.addWidget(self.view_io_5.btn, 3, 0)
        main_layer.addWidget(self.view_io_5.plotWidget, 3, 1)
        
        main_layer.addWidget(self.view_io_avg_5.btn, 4, 0)
        main_layer.addWidget(self.view_io_avg_5.plotWidget, 4, 1)


    def displayAlarm(self, tab):
        main_layer = QGridLayout()
        tab.setLayout(main_layer)

        grp_alarm = QGroupBox("Alarm")
        layout_alarm = QGridLayout()
        grp_alarm.setLayout(layout_alarm)

        # self.sfields
        self.headAlarm = HeadAlarm("Device Setting")
        group_head = self.headAlarm.display()
        main_layer.addWidget(group_head, 0, 0, 1, 6)

        self.deviceAlarm = DeviceAlarm("Device Setting")
        group_setup = self.deviceAlarm.display()
        main_layer.addWidget(group_setup, 1, 0)

        ## COLUMN 2
        self.fault_modules = []
        bcolors = ['#F2D7D5', '#D4E6F1', '#FCF3CF', '#E8F8F5', '#f9e79f']
        for idx in range(1, 6):
            mname = f"#{idx} MODULE"
            mod = ModuleFault(mname, bcolors[idx-1])

            for name in FAULT_NAMES:
                field = CheckField(name, '정상', '경고', 0)
                mod.add_field(field)

            group = mod.display()
            main_layer.addWidget(group, 1, idx)

            self.fault_modules.append(mod)


    def displayValueChange1(self, tab):
        main_layer = QGridLayout()
        tab.setLayout(main_layer)

        self.setting = ValueSetting(self)
        group = self.setting.display()
        main_layer.addWidget(group, 0, 0, 1, 5)

        self.vumod1 = VUModule(self, '#1 MODULE', address=5, loadadd=64, md_add=69, bcolor="#F2D7D5")
        group1 = self.vumod1.display()
        main_layer.addWidget(group1, 1, 0)

        self.vumod2 = VUModule(self, '#2 MODULE', address=15, loadadd=65, md_add=70, bcolor="#D4E6F1")
        group2 = self.vumod2.display()
        main_layer.addWidget(group2, 1, 1)

        self.vumod3 = VUModule(self, '#3 MODULE', address=25, loadadd=66, md_add=71, bcolor="#FCF3CF")
        group3 = self.vumod3.display()
        main_layer.addWidget(group3, 1, 2)

        self.vumod4 = VUModule(self, '#4 MODULE', address=35, loadadd=67, md_add=72, bcolor="#E8F8F5")
        group4 = self.vumod4.display()
        main_layer.addWidget(group4, 1, 3)

        self.vumod5 = VUModule(self, '#5 MODULE', address=45, loadadd=68, md_add=73, bcolor="#E8F8F5")
        group5 = self.vumod5.display()
        main_layer.addWidget(group5, 1, 4)


    def updateAllChangedValue(self):
        self.setting.updateChangedValue()
        self.vumod1.updateChangedValue()
        self.vumod2.updateChangedValue()
        self.vumod3.updateChangedValue()
        self.vumod4.updateChangedValue()
        self.vumod5.updateChangedValue()

    ## Graph Module
    def drawGraph_1(self):
        ch1_vdcs = self.mod1.get_ch1_vdcs()
        ch2_vdcs = self.mod1.get_ch2_vdcs()
        ch3_vdcs = self.mod1.get_ch3_vdcs()

        ch1_idcs = self.mod1.get_ch1_idcs()
        ch2_idcs = self.mod1.get_ch2_idcs()
        ch3_idcs = self.mod1.get_ch3_idcs()

        ch1_irs = self.mod1.get_ch1_irs()
        ch2_irs = self.mod1.get_ch2_irs()
        ch3_irs = self.mod1.get_ch3_irs()

        io1_avg = self.mod1.get_io1_avgs()
        io2_avg = self.mod1.get_io2_avgs()
        io3_avg = self.mod1.get_io3_avgs()
        io4_avg = self.mod1.get_io4_avgs()
        io5_avg = self.mod1.get_io5_avgs()

        try:
            self.view_vdc_1.reDrawMulti("CH #1", ch1_vdcs, "CH #2", ch2_vdcs, "CH #3", ch3_vdcs)
            self.view_idc_1.reDrawMulti("CH #1", ch1_idcs, "CH #2", ch2_idcs, "CH #3", ch3_idcs)
            self.view_ir_1.reDrawMulti("CH #1", ch1_irs, "CH #2", ch2_irs, "CH #3", ch3_irs)

            self.view_io_1.reDraw("CH #1", self.mod1.get_ios())
            self.view_io_avg_1.reDrawMulti("CH #1", io1_avg, "CH #2", io2_avg, "CH #3", io3_avg, "CH #4", io4_avg, "CH #5", io5_avg)
        except Exception as e:
            print(" !!!!! drawGraph ", e)


    def drawGraph_2(self):
        ch1_vdcs = self.mod2.get_ch1_vdcs()
        ch2_vdcs = self.mod2.get_ch2_vdcs()
        ch3_vdcs = self.mod2.get_ch3_vdcs()

        ch1_idcs = self.mod2.get_ch1_idcs()
        ch2_idcs = self.mod2.get_ch2_idcs()
        ch3_idcs = self.mod2.get_ch3_idcs()

        ch1_irs = self.mod2.get_ch1_irs()
        ch2_irs = self.mod2.get_ch2_irs()
        ch3_irs = self.mod2.get_ch3_irs()

        io1_avg = self.mod2.get_io1_avgs()
        io2_avg = self.mod2.get_io2_avgs()
        io3_avg = self.mod2.get_io3_avgs()
        io4_avg = self.mod2.get_io4_avgs()
        io5_avg = self.mod2.get_io5_avgs()

        try:
            self.view_vdc_2.reDrawMulti("CH #1", ch1_vdcs, "CH #2", ch2_vdcs, "CH #3", ch3_vdcs)
            self.view_idc_2.reDrawMulti("CH #1", ch1_idcs, "CH #2", ch2_idcs, "CH #3", ch3_idcs)
            self.view_ir_2.reDrawMulti("CH #1", ch1_irs, "CH #2", ch2_irs, "CH #3", ch3_irs)

            self.view_io_2.reDraw("CH #1", self.mod2.get_ios())
            self.view_io_avg_2.reDrawMulti("CH #1", io1_avg, "CH #2", io2_avg, "CH #3", io3_avg, "CH #4", io4_avg, "CH #5", io5_avg)
        except Exception as e:
            print(" !!!!! drawGraph ", e)


    def drawGraph_3(self):
        ch1_vdcs = self.mod3.get_ch1_vdcs()
        ch2_vdcs = self.mod3.get_ch2_vdcs()
        ch3_vdcs = self.mod3.get_ch3_vdcs()

        ch1_idcs = self.mod3.get_ch1_idcs()
        ch2_idcs = self.mod3.get_ch2_idcs()
        ch3_idcs = self.mod3.get_ch3_idcs()

        ch1_irs = self.mod3.get_ch1_irs()
        ch2_irs = self.mod3.get_ch2_irs()
        ch3_irs = self.mod3.get_ch3_irs()

        io1_avg = self.mod3.get_io1_avgs()
        io2_avg = self.mod3.get_io2_avgs()
        io3_avg = self.mod3.get_io3_avgs()
        io4_avg = self.mod3.get_io4_avgs()
        io5_avg = self.mod3.get_io5_avgs()

        try:
            self.view_vdc_3.reDrawMulti("CH #1", ch1_vdcs, "CH #2", ch2_vdcs, "CH #3", ch3_vdcs)
            self.view_idc_3.reDrawMulti("CH #1", ch1_idcs, "CH #2", ch2_idcs, "CH #3", ch3_idcs)
            self.view_ir_3.reDrawMulti("CH #1", ch1_irs, "CH #2", ch2_irs, "CH #3", ch3_irs)

            self.view_io_3.reDraw("CH #1", self.mod3.get_ios())
            self.view_io_avg_3.reDrawMulti("CH #1", io1_avg, "CH #2", io2_avg, "CH #3", io3_avg, "CH #4", io4_avg, "CH #5", io5_avg)
        except Exception as e:
            print(" !!!!! drawGraph 3", e)

    
    def drawGraph_4(self):
        ch1_vdcs = self.mod4.get_ch1_vdcs()
        ch2_vdcs = self.mod4.get_ch2_vdcs()
        ch3_vdcs = self.mod4.get_ch3_vdcs()

        ch1_idcs = self.mod4.get_ch1_idcs()
        ch2_idcs = self.mod4.get_ch2_idcs()
        ch3_idcs = self.mod4.get_ch3_idcs()

        ch1_irs = self.mod4.get_ch1_irs()
        ch2_irs = self.mod4.get_ch2_irs()
        ch3_irs = self.mod4.get_ch3_irs()

        io1_avg = self.mod4.get_io1_avgs()
        io2_avg = self.mod4.get_io2_avgs()
        io3_avg = self.mod4.get_io3_avgs()
        io4_avg = self.mod4.get_io4_avgs()
        io5_avg = self.mod4.get_io5_avgs()

        try:
            self.view_vdc_4.reDrawMulti("CH #1", ch1_vdcs, "CH #2", ch2_vdcs, "CH #3", ch3_vdcs)
            self.view_idc_4.reDrawMulti("CH #1", ch1_idcs, "CH #2", ch2_idcs, "CH #3", ch3_idcs)
            self.view_ir_4.reDrawMulti("CH #1", ch1_irs, "CH #2", ch2_irs, "CH #3", ch3_irs)

            self.view_io_4.reDraw("CH #1", self.mod4.get_ios())
            self.view_io_avg_4.reDrawMulti("CH #1", io1_avg, "CH #2", io2_avg, "CH #3", io3_avg, "CH #4", io4_avg, "CH #5", io5_avg)
        except Exception as e:
            print(" !!!!! drawGraph ", e)

    
    def drawGraph_5(self):
        ch1_vdcs = self.mod5.get_ch1_vdcs()
        ch2_vdcs = self.mod5.get_ch2_vdcs()
        ch3_vdcs = self.mod5.get_ch3_vdcs()

        ch1_idcs = self.mod5.get_ch1_idcs()
        ch2_idcs = self.mod5.get_ch2_idcs()
        ch3_idcs = self.mod5.get_ch3_idcs()

        ch1_irs = self.mod5.get_ch1_irs()
        ch2_irs = self.mod5.get_ch2_irs()
        ch3_irs = self.mod5.get_ch3_irs()

        io1_avg = self.mod5.get_io1_avgs()
        io2_avg = self.mod5.get_io2_avgs()
        io3_avg = self.mod5.get_io3_avgs()
        io4_avg = self.mod5.get_io4_avgs()
        io5_avg = self.mod5.get_io5_avgs()

        try:
            self.view_vdc_5.reDrawMulti("CH #1", ch1_vdcs, "CH #2", ch2_vdcs, "CH #3", ch3_vdcs)
            self.view_idc_5.reDrawMulti("CH #1", ch1_idcs, "CH #2", ch2_idcs, "CH #3", ch3_idcs)
            self.view_ir_5.reDrawMulti("CH #1", ch1_irs, "CH #2", ch2_irs, "CH #3", ch3_irs)

            self.view_io_5.reDraw("CH #1", self.mod5.get_ios())
            self.view_io_avg_5.reDrawMulti("CH #1", io1_avg, "CH #2", io2_avg, "CH #3", io3_avg, "CH #4", io4_avg, "CH #5", io5_avg)
        except Exception as e:
            print(" !!!!! drawGraph ", e)

    # BIT 연산 
    def bit_check(self, x):
        data = []
        for idx in range(16):
            if (x & (1<<idx)):
                data.append(idx)
        return data


    def modifyDeviceValue(self, address, value):
        logging.info(f"main :: modifyDeviceValue :: {address}, {value}")
        if self.client:
            result = device.write_registers(self.client, address, value, self.ptype)
            if not result:
                self.client = device.connect_tcp(self.gen_port, self.ipport)
                result = device.write_registers(self.client, address, value, self.ptype)
        else:
            self.lbl_com_gen.setText("전원 장치와 연결이 필요.")


    # 전원 장치와 송신을 위한 설정
    def setting_device(self):
        if self.com_open_flag == False:
            Dialog = QDialog()
            self.com_open_flag == True
            dialog = SettingWin(Dialog, self.used_ports)
            dialog.show()
            response = dialog.exec_()

            # OK 를 하면 설정 값을 읽어와서 통신을 한다.
            if response == QDialog.Accepted:
                if dialog.selected == 'RTU':
                    self.gen_port = dialog.gen_port
                    try:
                        _, port = dialog.gen_port.split('__')
                    except Exception as e:
                        port = dialog.gen_port
                        print("setting_device ::", e)
                    com_speed = dialog.com_speed
                    com_data = dialog.com_data
                    com_parity = dialog.com_parity
                    com_stop = dialog.com_stop
                    self.com_open_flag = False

                    self.client = device.connect_rtu(
                        port=self.gen_port, ptype='rtu',
                        speed=com_speed, bytesize=com_data, 
                        parity=com_parity, stopbits=com_stop
                    )
                    self.ptype = RTU
                    print("setting_device ::", self.client)
                else:
                    self.gen_port = dialog.ipaddress
                    self.ipport = int(dialog.ipport)
                    print(self.gen_port, self.ipport)
                    port = self.gen_port
                    self.client = device.connect_tcp(self.gen_port, self.ipport)
                    self.ptype = TCP

                if self.client:
                    self.lbl_com_gen.setText(port)
                    self.btn_com_gen.hide()
                    self.btn_run1.setEnabled(True)
                    self.btn_run2.setEnabled(True)
                    self.btn_run3.setEnabled(True)
                    self.btn_run4.setEnabled(True)
                    self.btn_run5.setEnabled(True)

                    self.start_pulldata()
                    
        else:
            print("Open Dialog")

    ## START MODULE
    def start_module(self, idx, btn):
        if not self.client:
            dialog = WarningDialog("전자석 전원 장치와 먼저 연결하세요.")
            dialog.show()
            response = dialog.exec_()

        if btn.getStatus():
            btn.changeStop()
            flag = STOP
        else:
            btn.changeRun()
            flag = RUN

        if flag == RUN:
            if idx == 1:
                self.run_flag1 = True
            elif idx == 2:
                self.run_flag2 = True
            elif idx == 3:
                self.run_flag3 = True
            elif idx == 4:
                self.run_flag4 = True
            elif idx == 5:
                self.run_flag5 = True
        else:
            if idx == 1:
                self.run_flag1 = False
            elif idx == 2:
                self.run_flag2 = False
            elif idx == 3:
                self.run_flag3 = False
            elif idx == 4:
                self.run_flag4 = False
            elif idx == 5:
                self.run_flag5 = False

        total_val = 0
        if self.run_flag1:
            val = pow(2, 1)
            total_val += val
        
        if self.run_flag2:
            val = pow(2, 2)
            total_val += val
        
        if self.run_flag3:
            val = pow(2, 3)
            total_val += val
        
        if self.run_flag4:
            val = pow(2, 4)
            total_val += val

        if self.run_flag5:
            val = pow(2, 5)
            total_val += val

        result = device.write_registers(self.client, device.SETTING, total_val, self.ptype)
        if not result:
            self.client = device.connect_tcp(self.gen_port, self.ipport)
            result = device.write_registers(self.client, device.SETTING, total_val, self.ptype)
        logging.info(f"{idx} :: {total_val}")


    def start_pulldata(self):
        if self.client:
            self.start_time = datetime.now()
            start_time = datetime.strftime(self.start_time, '%Y-%m-%d %H:%M:%S')
            self.label_start_time.setText(start_time)

            if not self.timer:
                self.timer = QTimer()
                self.dtimer = QTimer()
                self.faulttimer = QTimer()

            else:
                self.timer.stop()
                self.dtimer.stop()
                self.faulttimer.stop()

                self.timer = None
                self.dtimer = None
                self.faulttimer = None

                self.timer = QTimer()
                self.dtimer = QTimer()
                self.faulttimer = QTimer()

            self.countN = 0
            self.time = 0
            self.indexs = []
            self.run_flag = True
            
            # setting timer
            self.timer.setInterval(1000)
            self.timer.timeout.connect(self.get_time)
            self.timer.start()

            DATA_PERIOD = int(self.data_time.get_val()) ## * 1000
            FAULT_PERIOD = int(self.fault_time.get_val()) ## * 1000

            self.dtimer.setInterval(DATA_PERIOD)
            self.dtimer.timeout.connect(self.get_data)
            self.dtimer.start()

            self.faulttimer.setInterval(FAULT_PERIOD)
            self.faulttimer.timeout.connect(self.get_fault)
            self.faulttimer.start()

        else:
            self.lbl_com_gen.setText("전원 장치를 먼저 연결하세요.")
            self.lbl_com_gen.setStyleSheet(self.font_green)


    def updateSetting(self, setting):
        # print("updateSetting :: ", setting)
        self.hlink.update(setting)
        self.mdstatus1.update_module(setting[3:13])
        self.mdstatus2.update_module(setting[13:23])
        self.mdstatus3.update_module(setting[23:33])
        self.mdstatus4.update_module(setting[33:43])


    def initialSetting(self, setting):
        self.vumod1.update_module(setting[3:13])
        self.vumod2.update_module(setting[13:23])
        self.vumod3.update_module(setting[23:33])
        self.vumod4.update_module(setting[33:43])


    def get_setting_value(self):
        if self.client:
            data = device.read_setting_from_device(self.client, self.ptype)
            if data['working']:
                setting = data['setting']
                # print(setting)
                self.updateSetting(setting)
            else:
                self.lbl_com_gen.setText("통신 에레 발생")
                self.lbl_com_gen.setStyleSheet(self.font_red)
        else:
            self.lbl_com_gen.setText("전원 장치를 먼저 연결하세요.")
            self.lbl_com_gen.setStyleSheet(self.font_green)


    def get_data(self):
        data = device.read_data_v2(self.client, self.ptype)
        logging.info(f"get_data :: {self.countN} :: {data} \n")
        try:
            if data['mod12345']:
                self.countN = self.countN  + 1
                self.indexs.append(self.countN)

                try:
                    md = data['mod12345']
                    self.mod1.change_value(md[:10], md[50:55])
                    self.mod2.change_value(md[10:20], md[55:60])
                    self.mod3.change_value(md[20:30], md[60:65])
                    self.mod4.change_value(md[30:40], md[65:70])
                    self.mod5.change_value(md[40:], md[70:])

                except Exception as e:
                    print(e)

                if self.countN % 5 == 1:
                    self.drawGraph_1()
                elif self.countN % 5 == 2:
                    self.drawGraph_2()
                elif self.countN % 5 == 3:
                    self.drawGraph_3()
                elif self.countN % 5 == 4:
                    self.drawGraph_4()
                elif self.countN % 5 == 0:
                    self.drawGraph_5()
                else:
                    self.drawGraph_1()
        except Exception as e:
            print("get_data :: ", e)
        # else:
        #     self.client = device.connect_tcp(self.gen_port, self.ipport)
            # self.lbl_com_gen.setText("통신 에레 발생")
            # self.lbl_com_gen.setStyleSheet(self.font_red)


    def get_fault(self):
        messages = []
        data = device.read_fault_v2(self.client, self.ptype)
        # print("get_fault :: ", data)
        if data:
            for idx, val in enumerate(data):
                if val:
                    faults = self.bit_check(val) ## BIT 연산
                    for fault in faults:
                        try:
                            logging.info(f"MOD # :: {idx + 1} :: fault # :: {fault}, {FAULT_NAMES[fault]}")
                            message = f"{FAULT_NAMES[fault]}"
                            messages.append(message)
                        except Exception as e:
                            pass

                    if idx == 0:
                        self.headAlarm.status_update(faults)

                    elif idx == 3:
                        self.deviceAlarm.status_update(faults)
                        if 1 in faults:
                            self.md1_fault.change_warning()
                        if 2 in faults:
                            self.md2_fault.change_warning()
                        if 3 in faults:
                            self.md3_fault.change_warning()
                        if 4 in faults:
                            self.md4_fault.change_warning()
                        if 5 in faults:
                            self.md5_fault.change_warning()
                    
                    elif idx == 5:
                        self.fault_modules[0].update_warning(faults)
                        self.md1_fault.change_warning()
                        self.mod1.status_warning()
                        self.mod1.change_warning_labels(messages)
                    elif idx == 6:
                        self.fault_modules[1].update_warning(faults)
                        self.md2_fault.change_warning()
                        self.mod2.status_warning()
                        self.mod2.change_warning_labels(messages)
                    elif idx == 7:
                        self.fault_modules[2].update_warning(faults)
                        self.md3_fault.change_warning()
                        self.mod3.status_warning()
                        self.mod3.change_warning_labels(messages)
                    elif idx == 8:
                        self.fault_modules[3].update_warning(faults)
                        self.md4_fault.change_warning()
                        self.mod4.status_warning()
                        self.mod4.change_warning_labels(messages)
                    elif idx == 9:
                        self.fault_modules[4].update_warning(faults)
                        self.md4_fault.change_warning()
                        self.mod5.status_warning()
                        self.mod5.change_warning_labels(messages)


    def get_graphdata(self):
        self.graphtimer.stop()
        self.graphtimer = None

        self.mod1_list = []
        self.mod2_list = []
        self.mod3_list = []
        self.mod4_list = []

        data = device.read_graphdata(self.client, self.ptype)
        if data['working']:
            for val in data['mod1']:
                value = c_int16(val).value
                self.mod1_list.append(value)

            for val in data['mod2']:
                value = c_int16(val).value
                self.mod2_list.append(value)

            for val in data['mod3']:
                value = c_int16(val).value
                self.mod3_list.append(value)

            for val in data['mod4']:
                value = c_int16(val).value
                self.mod4_list.append(value)

            self.drawGraph()


    def module_update(self, condition):
        if 1 in condition:
            self.md1_fault.change_warning()
        if 2 in condition:
            self.md2_fault.change_warning()
        if 3 in condition:
            self.md3_fault.change_warning()
        if 4 in condition:
            self.md4_fault.change_warning()
        if 5 in condition:
            self.md4_fault.change_warning()


    def save_data(self):
        pass


    def save_data_as_json(self):
        pass

    # READ DATA 
    def read_data(self):
       pass

 
    def fault_clear(self):
        self.fault_reset()
        self.magnetic_device_stop()
        if self.client:
            device.write_registers(self.client, 0, 64, self.ptype)
            time.sleep(2)
            device.write_registers(self.client, 0, 0, self.ptype)

        else:
            self.lbl_com_gen.setText("전원 장치와 연결이 필요.")

        self.magnetic_device_stop()


    def reset_device(self):
        if self.client:
            device.write_registers(self.client, 0, 64, self.ptype)
        else:
            self.lbl_com_gen.setText("전원 장치와 연결이 필요.")


    def get_time(self):
        self.time += 1
        timedisplay = '{:02d}:{:02d}:{:02d}'.format((self.time // 60) // 60, 
                        (self.time // 60) % 60, self.time % 60)
        self.display_time.display(timedisplay)


    def fault_reset(self):
        self.md1_fault.change_normal()
        self.md2_fault.change_normal()
        self.md3_fault.change_normal()
        self.md4_fault.change_normal()
        self.md5_fault.change_normal()

        self.mod1.reset()
        self.mod2.reset()
        self.mod3.reset()
        self.mod4.reset()
        self.mod5.reset()

        self.headAlarm.reset()
        self.deviceAlarm.reset()

        for mod in self.fault_modules:
            mod.reset()


    # Run Device (중간에 Run 을 눌러서 디바이스 시작시킴)
    def magnetic_device_run(self):
        logging.info(" ####################### RUN DEVICE ####################### ")
        # 전원 장치를 시작하고, 데이터를 읽어옴
        
        if self.client:
            self.btn_device_stop.setEnabled(True)
            result = device.write_registers(self.client, device.SETTING, 62, self.ptype)
            if not result:
                self.client = device.connect_tcp(self.gen_port, self.ipport)
                result = device.write_registers(self.client, device.SETTING, 62, self.ptype)

            self.btn_device_run.setStyleSheet(self.run_style)
            self.btn_device_run.setEnabled(False)

            self.run_flag1 = True
            self.run_flag2 = True
            self.run_flag3 = True
            self.run_flag4 = True
            self.run_flag5 = True

            self.btn_run1.changeRun()
            self.btn_run2.changeRun()
            self.btn_run3.changeRun()
            self.btn_run4.changeRun()
            self.btn_run5.changeRun()

        else:
            self.lbl_com_gen.setText("전원 장치와 연결이 필요.")
            self.lbl_com_gen.setStyleSheet(self.font_red)

    # 단말기 STOP
    def magnetic_device_stop(self):
        logging.info(" ####################### STOP DEVICE ####################### ")
        device.write_registers(self.client, device.SETTING, 0, self.ptype)
        
        self.btn_device_run.setStyleSheet(self.stop_style)
        self.btn_device_run.setEnabled(True)

        self.drawGraph_1()
        self.drawGraph_2()
        self.drawGraph_3()
        self.drawGraph_4()
        self.drawGraph_5()

        self.run_flag1 = False
        self.run_flag2 = False
        self.run_flag3 = False
        self.run_flag4 = False
        self.run_flag5 = False

        self.btn_run1.changeStop()
        self.btn_run2.changeStop()
        self.btn_run3.changeStop()
        self.btn_run4.changeStop()
        self.btn_run5.changeStop()

        self.btn_device_run.setStyleSheet("border: 3px solid lightgray;border-radius: 40px;background-color: gray;color: white;font-size: 16px;font-weight: bold;")
        self.btn_device_run.setText('RUN')


if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    ex = MagneticMonitoring()
    sys.exit(app.exec_())

