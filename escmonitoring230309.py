#!/usr/bin/env python
# coding: utf-8

# 예제 내용
# * 기본 위젯을 사용하여 기본 창을 생성
# * 다양한 레이아웃 위젯 사용
import os
import sys
import numpy as np
import pandas as pd
import time
import json, codecs

from random import randint
from PyQt5.QtWidgets import QWidget, QLabel, QPushButton, QDesktopWidget, QMainWindow
from PyQt5.QtWidgets import QGroupBox, QVBoxLayout, QHBoxLayout, QGridLayout, QLineEdit
from PyQt5.QtWidgets import QApplication, QDialog, QStatusBar, QFileDialog, QCheckBox
from PyQt5 import QtCore
from PyQt5.QtCore import QDate, Qt
from PyQt5.QtGui import QPixmap
import pyqtgraph as pyGraph

from esc.esc_setup import SettingWin
from esc.esc_params import ESCParams
import esc.esc_serial as device
from common.zoom import GraphWindow, ZoomWindow, ZoomWindow6

from ctypes import c_int16
from threading import Timer

GRAPH = 1
DATA = 2

RTU = 'RTU'
TCP = 'TCP'

# import logging
# FORMAT = ('%(asctime)-15s %(threadName)-15s '
#           '%(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
# logging.basicConfig(format=FORMAT)
# log = logging.getLogger()
# log.setLevel(logging.DEBUG)


def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)

class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()

        self.toggle = device.FORWARD

        self.faultmessage = {
            0 : "Fault01-0 : FAULT LEAK CURRENT OVER  ",
            1 : "Fault01-1 : FAULT HW OUT CURRENT OVER1  ",
            2 : "Fault01-2 : FAULT HW OUT CURRENT OVER2  ",
            3 : "Fault01-3 : FAULT HW OUT VOLTAGE OVER1  ",
            4 : "Fault01-4 : FAULT HW OUT VOLTAGE OVER2  ",
            5 : "Fault01-5 : FAULT EXT INTERLOCK  ",
            6 : "Fault01-6 : FAULT OUT CURRENT OVER1  ",
            7 : "Fault01-7 : FAULT OUT CURRENT OVER2  ",
            8 : "Fault01-8 : FAULT OUT VOLTAGE OVER1  ",
            9 : "Fault01-9 : FAULT OUT VOLTAGE OVER2  ",
            10: "Fault01-10 : FAULT COMMUNICATION ERROR  ",
            11: "Fault01-11 : FAULT TOO MANY ARC  ",
            12: "Fault01-12 : FAULT RO MIN   ",
            13: "Fault01-13 : RESONANT CURRENT OVER1  ",
            14: "Fault01-14 : RESONANT CURRENT OVER2  ",
        }

        self.timer = QtCore.QTimer()
        # self.stoptimer = QtCore.QTimer()

        self.faulttimer = QtCore.QTimer()

        self.com_setting_flag = False
        self.com_open_flag = False
        self.run_flag = False

        ## 2022-02-14
        ## data only, or graph 
        self.graph_type = GRAPH

        self.client = None
        self.ptype = RTU
        self.qr_client = None
        self.gen_port = None
        self.qr_port = None
        self.com_speed = None
        self.com_data = None
        self.com_parity = None
        self.com_stop = None

        self.graph_flag = False
        self.countN = 0

        self.initial = {}
        self.time_delay= 10

        self.view_vol1 = True
        self.view_cur1 = True
        self.view_vol2 = True
        self.view_cur2 = True
        self.view_vbia = True
        self.view_cps = True
        self.view_leak_cur1 = False
        self.view_leak_cur2 = False

        self.indexs = np.array([])
        self.times = np.array([])
        self.vol1s = np.array([])
        self.curt1s = np.array([])
        self.vol2s = np.array([])
        self.curt2s = np.array([])
        self.vbias = np.array([])
        self.vcss = np.array([])
        self.icss = np.array([])
        self.cpss = np.array([])
        self.leak_cur1s = np.array([])
        self.leak_cur2s = np.array([])

        self.temp_indexs = np.array([])
        self.temp_times = np.array([])
        self.temp_vol1s = np.array([])
        self.temp_cur1s = np.array([])
        self.temp_vol2s = np.array([])
        self.temp_cur2s = np.array([])
        self.temp_vbias = np.array([])
        self.temp_vcss = np.array([])
        self.temp_icss = np.array([])
        self.temp_cpss = np.array([])
        self.temp_leak_cur1s = np.array([])
        self.temp_leak_cur2s = np.array([])

        self.pen_vol1 = pyGraph.mkPen(width=2, color=(255, 0, 0))
        self.pen_cur1 = pyGraph.mkPen(width=2, color=(0, 0, 255))
        self.pen_vol2 = pyGraph.mkPen(width=2, color=(255, 191, 0))
        self.pen_cur2 = pyGraph.mkPen(width=2, color=(26, 175, 51))
        self.pen_vbia = pyGraph.mkPen(width=2, color=(0,0,128))
        self.pen_cps = pyGraph.mkPen(width=2, color=(108, 52, 131))
        self.pen_leak_cur1 = pyGraph.mkPen(width=2, color=(204, 0, 153))
        self.pen_leak_cur2 = pyGraph.mkPen(width=2, color=(102, 0, 204))

        self.initUI()

    def eventFilter(self, watched, event):
        pass

    def displayUnitLayout(self, parent_layout, btn, cbx, label, unit):
        grp_sub = QGroupBox("")
        layout_sub = QGridLayout()
        grp_sub.setLayout(layout_sub)

        label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        unit.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)

        layout_sub.addWidget(btn, 0, 0, 1, 3)
        layout_sub.addWidget(cbx, 0, 4)
        layout_sub.addWidget(label, 1, 0, 1, 3)
        layout_sub.addWidget(unit, 1, 4)
        parent_layout.addWidget(grp_sub)


    def displayMenuLayout(self, parent_layout, glabel, btn, second=None):
        grp_sub = QGroupBox(glabel)
        layout_sub = QHBoxLayout()
        grp_sub.setLayout(layout_sub)
        layout_sub.addWidget(btn)
        if second:
            layout_sub.addWidget(second)
        parent_layout.addWidget(grp_sub)


    def initUI(self):
        self.setWindowTitle("PSTEK ELECTROSTATIC CHUCK MONITORING")
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width(), screensize.height()) 

        mainwidget = QWidget()                # 위젯의 인스턴스 생성만으로도 QMainWindow에 붙는다.
        self.setCentralWidget(mainwidget)

        ## Main Layout 설정
        main_layer = QVBoxLayout()
        mainwidget.setLayout(main_layer)

        bluefont = "color: #0000ff;"
        redfont = "color: #ff0000;"
        layout_setup = QHBoxLayout()
        
        ## replace #1st ###############
        # 전원 장치 연결
        self.btn_com_gen = QPushButton("SELECT PORT")
        self.btn_com_gen.clicked.connect(self.setting_generator)
        self.label_gen_port = QLabel("N/A")
        self.displayMenuLayout(layout_setup, "Connect Device(485/ModBus)", 
                self.btn_com_gen, self.label_gen_port)

        # GET DATA
        self.btn_pulldata = QPushButton("PULL DATA")
        self.btn_pulldata.setEnabled(False)
        self.btn_pulldata.setStyleSheet(bluefont)
        self.btn_pulldata.clicked.connect(self.start_pulldata)
        self.displayMenuLayout(layout_setup, "PULL DATA", self.btn_pulldata)

        # 단말기 RUN/STOP
        grp_runstop = QGroupBox("")
        layout_runstop = QHBoxLayout()
        self.btn_esc_run = QPushButton("RUN")
        self.btn_esc_run.setEnabled(False)
        self.btn_esc_run.setStyleSheet(bluefont)
        self.btn_esc_run.clicked.connect(self.esc_device_run)
        self.btn_esc_stop = QPushButton("STOP")
        self.btn_esc_stop.setStyleSheet(redfont)
        self.btn_esc_stop.clicked.connect(self.esc_device_stop)
        self.btn_esc_stop.setEnabled(False)
        self.displayMenuLayout(layout_setup, "DEVICE RUN/STOP", self.btn_esc_run, self.btn_esc_stop)

        # 단말기 WRITE
        self.btn_params = QPushButton("MODIFY PARAMS")
        self.btn_params.setEnabled(False)
        self.btn_params.clicked.connect(self.device_params_modify)
        self.displayMenuLayout(layout_setup, "Modify Parameters", self.btn_params)

        # Toggle 기능 
        self.btn_toggle = QPushButton("TOGGLE")
        self.btn_toggle.setEnabled(False)
        self.btn_toggle.clicked.connect(self.device_toggle)
        self.displayMenuLayout(layout_setup, "TOGGLE", self.btn_toggle)

        # Toggle 기능 
        self.btn_num = QLineEdit("10")
        label = QLabel("횟수/초")
        self.displayMenuLayout(layout_setup, "데이터횟수", self.btn_num, label)

        # STOP DATA
        self.btn_stopdata = QPushButton("STOP DATA")
        self.btn_stopdata.setEnabled(False)
        self.btn_stopdata.setStyleSheet(bluefont)
        self.btn_stopdata.clicked.connect(self.stop_pulldata)
        self.displayMenuLayout(layout_setup, "STOP DATA", self.btn_stopdata)

        # 데이터 저장 / 데이터 불어오기
        self.btn_savedata = QPushButton("SAVE")
        self.btn_savedata.setEnabled(False)
        self.btn_savedata.clicked.connect(self.save_data)
        self.btn_readdata = QPushButton("READ")
        self.btn_readdata.clicked.connect(self.read_data)
        self.displayMenuLayout(layout_setup, "SAVE/READ DATA", self.btn_savedata, self.btn_readdata)

        # ZOOM Function
        self.btn_zoomA = QPushButton("ZOOM IN")
        self.btn_zoomA.setEnabled(False)
        self.btn_zoomA.clicked.connect(self.zoom_graph)
        self.btn_zoom6 = QPushButton("6 WINDOWS")
        self.btn_zoom6.setEnabled(False)
        self.btn_zoom6.clicked.connect(self.zoom_graph6)
        self.displayMenuLayout(layout_setup, "ZOOM", self.btn_zoomA)

        # Logo Image
        labelLogo = QLabel("")
        pixmap = QPixmap(resource_path("logo.png"))
        labelLogo.setAlignment(Qt.AlignRight)
        labelLogo.setPixmap(pixmap)
        layout_setup.addWidget(labelLogo)        
        main_layer.addLayout(layout_setup)

        ## 2rd ########################
        # 그래프 범례
        grp_legend = QGroupBox("LEGEND")
        layout_legend = QHBoxLayout()
        grp_legend.setLayout(layout_legend)
        main_layer.addWidget(grp_legend)

        # 1 vol1 
        btn_vol1 = QPushButton("OUT VOLTAGE1")
        btn_vol1.setStyleSheet("color: #ff0000;")
        btn_vol1.clicked.connect(lambda:self.draw_graph_each('OUT VOLTAGE1', color=(255, 0, 0)))
        self.cbx_vol1 = QCheckBox("")
        self.cbx_vol1.setChecked(True)
        self.cbx_vol1.stateChanged.connect(lambda:self.checkBoxChanged("OUT VOLTAGE1"))
        self.label_vol1 = QLabel("0")
        label_vol1_unit = QLabel("V")
        self.displayUnitLayout(layout_legend, btn_vol1, self.cbx_vol1, self.label_vol1, label_vol1_unit)

        # 2 curt1 
        btn_cur1 = QPushButton("OUT CURRENT1")
        btn_cur1.setStyleSheet("color: #0000ff;")
        btn_cur1.clicked.connect(lambda:self.draw_graph_each('OUT CURRENT1', color=(0, 0, 255)))
        self.cbx_cur1 = QCheckBox("")
        self.cbx_cur1.setChecked(True)
        self.cbx_cur1.stateChanged.connect(lambda:self.checkBoxChanged("OUT CURRENT1"))
        self.label_curt1 = QLabel("0")
        label_curt1_unit = QLabel("mA")
        self.displayUnitLayout(layout_legend, btn_cur1, self.cbx_cur1, self.label_curt1, label_curt1_unit)

        # 3 vol2 
        btn_vol2 = QPushButton("OUT VOLTAGE2")
        btn_vol2.setStyleSheet("color: #ffbf00;")
        btn_vol2.clicked.connect(lambda:self.draw_graph_each('OUT VOLTAGE2', color=(255, 191, 0)))
        self.cbx_vol2 = QCheckBox("")
        self.cbx_vol2.setChecked(True)
        self.cbx_vol2.stateChanged.connect(lambda:self.checkBoxChanged("OUT VOLTAGE2"))
        self.label_vol2 = QLabel("0")
        label_vol2_unit = QLabel("V")
        self.displayUnitLayout(layout_legend, btn_vol2, self.cbx_vol2, self.label_vol2, label_vol2_unit)

        # 4 curt2 
        btn_cur2 = QPushButton("OUT CURRENT2")
        btn_cur2.setStyleSheet("color: #1AAF33;")
        btn_cur2.clicked.connect(lambda:self.draw_graph_each('OUT CURRENT2', color=(26, 175, 51)))
        self.cbx_cur2 = QCheckBox("")
        self.cbx_cur2.setChecked(True)
        self.cbx_cur2.stateChanged.connect(lambda:self.checkBoxChanged("OUT CURRENT2"))
        self.label_curt2 = QLabel("0")
        label_curt2_unit = QLabel("mA")
        self.displayUnitLayout(layout_legend, btn_cur2, self.cbx_cur2, self.label_curt2, label_curt2_unit)

        # 5 vbia 
        btn_vbia = QPushButton("VBIAS")
        btn_vbia.setStyleSheet("color: #000080;")
        btn_vbia.clicked.connect(lambda:self.draw_graph_each('VBIAS', color=(0,0,128)))
        self.cbx_vbia = QCheckBox("")
        self.cbx_vbia.setChecked(True)
        self.cbx_vbia.stateChanged.connect(lambda:self.checkBoxChanged("VBIAS"))
        self.label_vbia = QLabel("0")
        label_vbia_unit = QLabel("V")
        self.displayUnitLayout(layout_legend, btn_vbia, self.cbx_vbia, self.label_vbia, label_vbia_unit)
        
        # 6 cps 
        btn_cps = QPushButton("CPS")
        btn_cps.setStyleSheet("color: #6C3483;")
        btn_cps.clicked.connect(lambda:self.draw_graph_each('CPS', color=(108, 52, 131)))
        self.cbx_cps = QCheckBox("")
        self.cbx_cps.setChecked(True)
        self.cbx_cps.stateChanged.connect(lambda:self.checkBoxChanged("CPS"))
        self.label_cps = QLabel("0")
        label_cps_unit = QLabel("pF")
        self.displayUnitLayout(layout_legend, btn_cps, self.cbx_cps, self.label_cps, label_cps_unit)

        # 7_leak_cur1 
        btn_leakcurrent1 = QPushButton("LEAK CURRENT1")
        btn_leakcurrent1.setStyleSheet("color: #cc0099;")
        btn_leakcurrent1.clicked.connect(lambda:self.draw_graph_each('LEAK CURRENT1', color=(204, 0, 153)))
        self.cbx_leakcurrent1 = QCheckBox("")
        self.cbx_leakcurrent1.setChecked(True)
        self.cbx_leakcurrent1.stateChanged.connect(lambda:self.checkBoxChanged("LEAK CURRENT1"))
        self.label_leakcurrent1 = QLabel("0")
        label_leakcurrent1_unit = QLabel("uA")
        self.displayUnitLayout(layout_legend, btn_leakcurrent1, self.cbx_leakcurrent1, self.label_leakcurrent1, label_leakcurrent1_unit)

        # 8_leak_cur2 
        btn_leakcurrent2 = QPushButton("LEAK CURRENT2")
        btn_leakcurrent2.setStyleSheet("color: #6600cc;")
        btn_leakcurrent2.clicked.connect(lambda:self.draw_graph_each('LEAK CURRENT2', color=(102, 0, 204)))
        self.cbx_leakcurrent2 = QCheckBox("")
        self.cbx_leakcurrent2.setChecked(True)
        self.cbx_leakcurrent2.stateChanged.connect(lambda:self.checkBoxChanged("LEAK CURRENT2"))
        self.label_leakcurrent2 = QLabel("0")
        label_leakcurrent2_unit = QLabel("uA")
        self.displayUnitLayout(layout_legend, btn_leakcurrent2, self.cbx_leakcurrent2, self.label_leakcurrent2, label_leakcurrent2_unit)
    
        ## 2-1rd Fault Message ########
        grp_fault = QGroupBox("에러 메시지")
        layout_fault = QHBoxLayout()
        self.label_fault = QLabel("")
        layout_fault.addWidget(self.label_fault)
        grp_fault.setLayout(layout_fault)
        main_layer.addWidget(grp_fault)

        ## 3th ########################
        # 그래프 디스플레이
        grp_display = QGroupBox("GRAPH")
        layout_display = QHBoxLayout()

        self.graphWidget = pyGraph.PlotWidget()
        self.graphWidget.setBackground('w')
        self.graphWidget.setMouseEnabled(x=False, y=False)
        
        ## create mainAxis
        self.axisA = self.graphWidget.plotItem
        self.axisA.setLabels(left='Voltage')
        self.axisA.showAxis('right')

        ## Second ViewBox
        self.axisB = pyGraph.ViewBox()
        self.axisA.scene().addItem(self.axisB)
        self.axisA.getAxis('right').linkToView(self.axisB)
        self.axisB.setXLink(self.axisA)
        self.axisA.getAxis('right').setLabel('Current', color='#0000ff')

        ## create Third ViewBox. 
        self.axisC = pyGraph.ViewBox()
        ax3 = pyGraph.AxisItem('right')
        self.axisA.layout.addItem(ax3, 2, 3)
        self.axisA.scene().addItem(self.axisC)
        ax3.linkToView(self.axisC)
        self.axisC.setXLink(self.axisA)
        ax3.setLabel('Vais/CPS', color='#000080')

        ## create Forth ViewBox. 
        self.axisD = pyGraph.ViewBox()
        ax4 = pyGraph.AxisItem('right')
        self.axisA.layout.addItem(ax4, 2, 4)
        self.axisA.scene().addItem(self.axisD)
        ax4.linkToView(self.axisD)
        self.axisD.setXLink(self.axisA)
        ax4.setLabel('Dechuck', color='#6C3483')

        self.updateViews()
        self.axisA.vb.sigResized.connect(self.updateViews)

        # mainAxis
        self.graph_vol1 = self.axisA.plot(self.temp_indexs, 
                    self.temp_vol1s, pen=self.pen_vol1, name="Voltage1s")
        self.graph_vol2 = self.axisA.plot(self.temp_indexs, 
                    self.temp_vol2s, pen=self.pen_vol2, name="Voltage2s")
        
        # axisB
        self.itemCurrent1 = pyGraph.PlotCurveItem(self.temp_indexs, 
                    self.temp_cur1s, pen=self.pen_cur1, name="Current1s")
        self.graph_cur1s = self.axisB.addItem(self.itemCurrent1)
        
        self.itemCurrent2 = pyGraph.PlotCurveItem(self.temp_indexs, 
                    self.temp_cur2s, pen=self.pen_cur2, name="Current2s")
        self.graph_cur2s = self.axisB.addItem(self.itemCurrent2)

    # axisC
        self.itemVbia = pyGraph.PlotCurveItem(self.temp_indexs, 
                    self.temp_vbias, pen=self.pen_vbia, name="Vbias")
        self.graph_vbias = self.axisC.addItem(self.itemVbia)

        self.itemCps = pyGraph.PlotCurveItem(self.temp_indexs, 
                    self.temp_cpss, pen=self.pen_cps, name="CPS")
        self.graph_cpss = self.axisC.addItem(self.itemCps)
        
        # axisD
        self.itemLeakCur1 = pyGraph.PlotCurveItem(self.temp_indexs, 
                    self.temp_leak_cur1s, pen=self.pen_leak_cur1, name="Leak_Cur1")
        self.graph_leak_cur1s = self.axisD.addItem(self.itemLeakCur1)

        self.itemLeakCur2 = pyGraph.PlotCurveItem(self.temp_indexs, 
                    self.temp_leak_cur2s, pen=self.pen_leak_cur2, name="Dechuck_end")
        self.graph_leak_cur2s = self.axisD.addItem(self.itemLeakCur2)

        layout_display.addWidget(self.graphWidget)
        grp_display.setLayout(layout_display)
        main_layer.addWidget(grp_display)
    
    ## 4th ########################
        # Status
        self.statusbar = QStatusBar()
        self.setStatusBar(self.statusbar)
        self.statusbar.setObjectName("statusbar")
        
        self.statusmessage = 'PSTEK, {},  {}'
        now = QDate.currentDate()
        displaymessage = self.statusmessage.format(now.toString(Qt.ISODate), 'Ready !!')
        self.statusbar.showMessage(displaymessage)

        # self.showFullScreen()

    def updateViews(self):
        ## view has resized; update auxiliary views to match
        # global self.axisA, self.axisB, self.axisC
        self.axisB.setGeometry(self.axisA.vb.sceneBoundingRect())
        self.axisC.setGeometry(self.axisA.vb.sceneBoundingRect())
        self.axisD.setGeometry(self.axisA.vb.sceneBoundingRect())
        self.axisB.linkedViewChanged(self.axisA.vb, self.axisB.XAxis)
        self.axisC.linkedViewChanged(self.axisA.vb, self.axisC.XAxis)
        self.axisD.linkedViewChanged(self.axisA.vb, self.axisD.XAxis)

    
    # 전원 장치와 송신을 위한 설정
    def device_params_modify(self):
        Dialog = QDialog()
        self.initial = device.read_setting_value(self.client, self.ptype)
        print(self.initial)

        dialog = ESCParams(Dialog, self.client, self.ptype, self.initial)
        dialog.show()
        response = dialog.exec_()


    # 전원 장치 Toggle
    def device_toggle(self):
        if self.toggle == device.FORWARD:
            self.toggle = device.REVERSE
            device.write_registers(self.client, device.WRITE_TOGGLE, device.REVERSE, self.ptype)

        else:
            self.toggle = device.FORWARD
            device.write_registers(self.client, device.WRITE_TOGGLE, device.FORWARD, self.ptype)

    # Bit return 함수 
    def bit_check(self, x):
        data = []
        for idx in range(15):
            if (x & (1<<idx)):
                data.append(idx)
        return data


    # Falult 오류 확인 
    def fault_check(self):
        x = device.read_fault(self.client, self.ptype)
        faults = self.bit_check(x)

        if faults:
            self.faulttimer.stop()
            self.timer.stop()
            # self.stoptimer.stop()

            message = ""

            for fault in faults:
                message += self.faultmessage[fault]

            self.label_fault.setText(message)


    # 데이터를 가져오는 오는 시작점
    def start_pulldata(self):
        if not self.client:
            self.setting_generator()

        # 데이터를 가져오기 시작하면 STOP 버턴 활성화
        
        # self.btn_savedata.setEnabled(True)
        self.run_flag = True 

        if not self.timer:
            self.timer = QtCore.QTimer()

        num = int(self.btn_num.text())
        DATA_PERIOD = int(1000/num)

        # setting timer
        self.timer.setInterval(DATA_PERIOD)
        self.timer.timeout.connect(self.get_data)
        self.timer.start()

        self.faulttimer.setInterval(10000)
        # self.faulttimer.timeout.connect(self.fault_check)
        self.faulttimer.start()


    # 데이터 가져 오기
    def get_data(self):

        data = device.read_registers(self.client, self.ptype)
        if data:
            self.countN = self.countN  + 1
            tm = time.time()

            # Label 에 데이터 보여주기
            self.label_vol1.setText(str(data['voltage1']))
            self.label_curt1.setText(str(data['current1']))
            self.label_vol2.setText(str(data['voltage2']))
            self.label_curt2.setText(str(data['current2']))
            
            self.label_vbia.setText(str(data['vbia']))
            self.label_cps.setText(str(data['cpss']))
            self.label_leak_cur1.setText(str(data['leak_cur1s']))
            self.label_leak_cur2.setText(str(data['leak_cur2s']))

            # 데이터 값을 Array 에 저장하기 
            self.indexs = np.append(self.indexs, self.countN)
            self.times = np.append(self.times, tm)
            self.vol1s = np.append(self.vol1s, data['voltage1'])
            self.curt1s = np.append(self.curt1s, data['current1'])
            self.vol2s = np.append(self.vol2s, data['voltage2'])
            self.curt2s = np.append(self.curt2s, data['current2'])
            self.vbias = np.append(self.vbias, data['vbia'])

            self.vcss = np.append(self.vcss, data['vcss'])
            self.icss = np.append(self.icss, data['icss'])
            self.cpss = np.append(self.cpss, data['cpss'])
            self.leak_cur1s = np.append(self.leak_cur1s, data['leak_cur1s'])
            self.leak_cur2s = np.append(self.leak_cur2s, data['leak_cur2s'])

            if self.graph_type == GRAPH:
                # 데이터 값 임시 값에 저장하기 Display 용
                self.temp_indexs = np.append(self.temp_indexs, self.countN)
                self.temp_times = np.append(self.temp_times, tm)
                self.temp_vol1s = np.append(self.temp_vol1s, data['voltage1'])
                self.temp_cur1s = np.append(self.temp_cur1s, data['current1'])
                self.temp_vol2s = np.append(self.temp_vol2s, data['voltage2'])
                self.temp_cur2s = np.append(self.temp_cur2s, data['current2'])
                self.temp_vbias = np.append(self.temp_vbias, data['vbia'])

                self.temp_vcss = np.append(self.temp_vcss, data['vcss'])
                self.temp_icss = np.append(self.temp_icss, data['icss'])
                self.temp_cpss = np.append(self.temp_cpss, data['cpss'])
                self.temp_leak_cur1s = np.append(self.temp_leak_cur1s, data['leak_cur1s'])
                self.temp_leak_cur2s = np.append(self.temp_leak_cur2s, data['leak_cur2s'])

                if self.countN >= 100 and self.countN % 10 == 0:
                    self.temp_indexs = self.temp_indexs[10:]
                    self.temp_times = self.temp_times[10:]
                    self.temp_vol1s = self.temp_vol1s[10:]
                    self.temp_cur1s = self.temp_cur1s[10:]
                    self.temp_vol2s = self.temp_vol2s[10:]
                    self.temp_cur2s = self.temp_cur2s[10:]
                    self.temp_vbias = self.temp_vbias[10:]

                    self.temp_vcss = self.temp_vcss[10:]
                    self.temp_icss = self.temp_icss[10:]
                    self.temp_cpss = self.temp_cpss[10:]
                    self.temp_leak_cur1s = self.temp_leak_cur1s[10:]
                    self.temp_leak_cur2s = self.temp_leak_cur2s[10:]

                if self.countN % 10 == 0:
                    self.draw_graph()

    # draw_graph using all graph
    def draw_graph(self):
        self.updateViews()
        self.axisA.vb.sigResized.connect(self.updateViews)

        if self.view_vol1:
            self.graph_vol1.hide()
            self.graph_vol1 = self.axisA.plot(self.temp_indexs, 
                    self.temp_vol1s, pen=self.pen_vol1, name="Voltage1s")
            self.graph_vol1.show()
        else:
            self.graph_vol1.hide()

        if self.view_cur1:
            self.itemCurrent1 = pyGraph.PlotCurveItem(self.temp_indexs, 
                    self.temp_cur1s, pen=self.pen_cur1, name="Current1s")
            self.graph_cur1s = self.axisB.addItem(self.itemCurrent1)
        else:
            self.axisB.hide()


        if self.view_vol2:
            self.graph_vol2.hide()
            self.graph_vol2 = self.axisA.plot(self.temp_indexs, 
                    self.temp_vol2s, pen=self.pen_vol2, name="Voltage2s")
            self.graph_vol2.show()
        else:
            self.graph_vol2.hide()

        if self.view_cur2:
            self.itemCurrent2 = pyGraph.PlotCurveItem(self.temp_indexs, 
                    self.temp_cur2s, pen=self.pen_cur2, name="Current2s")
            self.graph_cur2s = self.axisB.addItem(self.itemCurrent2)
        else:
            self.axisB.hide()

    # VBIAS
        if self.view_vbia:
            self.itemVbia = pyGraph.PlotCurveItem(self.temp_indexs, 
                    self.temp_vbias, pen=self.pen_vbia, name="Vbias")
            self.graph_vbias = self.axisC.addItem(self.itemVbia)
        else:
            self.view_vbia = False 
            self.axisC.removeItem(self.itemVbia)

        # CPS
        if self.view_cps:
            self.itemCps = pyGraph.PlotCurveItem(self.temp_indexs, 
                    self.temp_cpss, pen=self.pen_cps, name="CPS")
            self.graph_cpss = self.axisC.addItem(self.itemCps)
        else:
            self.view_cps = False  
            self.axisD.removeItem(self.itemCps)

        # Leak_Cur1
        if self.view_leak_cur1:
            self.itemLeakCur1 = pyGraph.PlotCurveItem(self.temp_indexs, 
                    self.temp_leak_cur1s, pen=self.pen_leak_cur1, name="Leak_Cur1")
            self.graph_leak_cur1s = self.axisD.addItem(self.itemLeakCur1)
            # self.axisD.hide(self.itemLeakCur1)
        else:
            self.view_leak_cur1 = False 
            self.axisD.removeItem(self.itemLeakCur1)


        if self.view_leak_cur2:
            self.itemLeakCur2 = pyGraph.PlotCurveItem(self.temp_indexs, 
                    self.temp_leak_cur2s, pen=self.pen_leak_cur2, name="Dechuck_end")
            self.graph_leak_cur2s = self.axisD.addItem(self.itemLeakCur2)
        else:
            self.view_leak_cur2 = False 
            self.axisD.removeItem(self.itemLeakCur2)


    # Run Device (중간에 Run 을 눌러서 디바이스 시작시킴)
    def esc_device_run(self):
        # 전원 장치를 시작하고, 데이터를 읽어옴
        device.write_registers(self.client, 
                device.WRITE_BIT_POWER_ON, device.RUN_VALUE, self.ptype)

        self.btn_toggle.setEnabled(True)
        self.btn_esc_stop.setEnabled(True)
        self.btn_stopdata.setEnabled(True)

        now = QDate.currentDate()
        displaymessage = self.statusmessage.format(now.toString(Qt.ISODate), "DEVICE RUN")
        self.statusbar.showMessage(displaymessage)

        if not self.run_flag:
            # setting timer
            self.run_flag = True
            self.start_pulldata()


    # 단말기 STOP
    def esc_device_stop(self):
        self.btn_toggle.setEnabled(False)
        device.write_registers(self.client, 
                device.WRITE_BIT_POWER_ON, device.STOP_VALUE, self.ptype)



    # 데이터 전송 중단
    def stop_pulldata(self):

        self.run_flag = False

        self.btn_savedata.setEnabled(True)
        self.btn_pulldata.setEnabled(True)
        self.btn_esc_run.setEnabled(True)

        self.btn_zoomA.setEnabled(True)
        self.btn_zoom6.setEnabled(True)
        # self.btn_esc_stop.setEnabled(False)
        
        if self.timer:
            self.timer.stop()
            self.timer = None
            # self.stoptimer.stop()
        
        now = QDate.currentDate()
        displaymessage = self.statusmessage.format(now.toString(Qt.ISODate), "DEVICE STOP")
        self.statusbar.showMessage(displaymessage)

        if self.graph_type == DATA:
            # 데이터 값 임시 값에 저장하기 Display 용
            self.temp_indexs = self.indexs
            self.temp_times = self.times
            self.temp_vol1s = self.vol1s
            self.temp_cur1s = self.curt1s
            self.temp_vol2s = self.vol2s
            self.temp_cur2s = self.curt2s
            self.temp_vbias = self.vbias

            self.temp_vcss = self.vcss
            self.temp_icss = self.icss
            self.temp_cpss = self.cpss
            self.temp_leak_cur1s = self.leak_cur1s
            self.temp_leak_cur2s = self.leak_cur2s

            self.draw_graph()


    # Keyboard Event
    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Escape:
            pass
        elif e.key() == Qt.Key_F:
            self.showFullScreen()
        elif e.key() == Qt.Key_N:
            self.showNormal()

  # RESET 
    def reset_data(self):
        self.countN = 0
        
        # Reser Data
        self.indexs = np.array([])
        self.times = np.array([])
        self.vol1s = np.array([])
        self.curt1s = np.array([])
        self.vol2s = np.array([])
        self.curt2s = np.array([])
        self.vbias = np.array([])
        self.vcss = np.array([])
        self.icss = np.array([])
        self.cpss = np.array([])
        self.leak_cur1s = np.array([])
        self.leak_cur2s = np.array([])

        # Reser Temp Data
        self.temp_indexs = np.array([])
        self.temp_times = np.array([])
        self.temp_vol1s = np.array([])
        self.temp_cur1s = np.array([])
        self.temp_vol2s = np.array([])
        self.temp_cur2s = np.array([])
        self.temp_vbias = np.array([])
        self.temp_vcss = np.array([])
        self.temp_icss = np.array([])
        self.temp_cpss = np.array([])
        self.temp_leak_cur1s = np.array([])
        self.temp_leak_cur2s = np.array([])

        # Flag Value Reset
        self.view_vol1 = True
        self.view_cur1 = True
        self.view_vol2 = True
        self.view_cur2 = True
        self.view_vbia = True
        self.view_cps = True
        self.view_leak_cur1 = False
        self.view_leak_cur2 = False

        # Reset 그림 그리기
        self.draw_graph()

    # Graph 개별 그래프 
    def draw_graph_each(self, category, color):
        if self.graph_flag == False:
            self.graph_flag = True
            Dialog = QDialog()
            if category == 'OUT VOLTAGE1':
                values = self.vol1s
            elif category == 'OUT CURRENT1':
                values = self.curt1s
            elif category == 'OUT VOLTAGE2':
                values = self.vol2s
            elif category == 'OUT CURRENT2':
                values = self.curt2s
            elif category == 'VBIAS':
                values = self.vbias
            elif category == 'CPS':
                values = self.cpss
            elif category == 'DECHUCK START':
                values = self.leak_cur1s
            elif category == 'DECHUCK END':
                values = self.leak_cur2s

            dialog = GraphWindow(Dialog, category, color, self.indexs, values)
            dialog.show()
            response = dialog.exec_()

            if response == QDialog.Accepted or response == QDialog.Rejected:
                self.graph_flag = False

    # zoom_graph
    def zoom_graph(self):
        Dialog = QDialog()
        dialog = ZoomWindow(Dialog, self.indexs, self.vol1s, self.curt1s, self.vol2s, self.curt2s, self.vbias, self.cpss, self.leak_cur1s, self.leak_cur2s)
        dialog.show()
        response = dialog.exec_()

        if response == QDialog.Accepted or response == QDialog.Rejected:
            self.graph_flag = False

    # zoom_graph8
    def zoom_graph6(self):
        Dialog = QDialog()
        dialog = ZoomWindow6(Dialog, self.indexs, self.vol1s, self.curt1s, self.vol2s, self.curt2s, self.vbias, self.cpss, self.leak_cur1s, self.leak_cur2s)
        dialog.show()
        response = dialog.exec_()

        if response == QDialog.Accepted or response == QDialog.Rejected:
            self.graph_flag = False

    # 전원 장치와 송신을 위한 설정
    def setting_generator(self):
        if self.com_open_flag == False:
            Dialog = QDialog()
            self.com_open_flag == True
            dialog = SettingWin(Dialog)
            dialog.show()
            response = dialog.exec_()

            # OK 를 하면 설정 값을 읽어와서 통신을 한다.
            if response == QDialog.Accepted:
                if dialog.selected == 'RTU':
                    self.gen_port = dialog.gen_port
                    self.com_speed = dialog.com_speed
                    self.com_data = dialog.com_data
                    self.com_parity = dialog.com_parity
                    self.com_stop = dialog.com_stop
                    self.com_open_flag = False

                    self.client = device.connect_rtu(
                        port=self.gen_port, ptype='rtu',
                        speed=self.com_speed, bytesize=self.com_data, 
                        parity=self.com_parity, stopbits=self.com_stop
                    )

                    self.ptype = RTU
                else:
                    self.gen_port = dialog.ipaddress
                    self.ipport = int(dialog.ipport)
                    print(self.gen_port, self.ipport)

                    self.client = device.connect_tcp(self.gen_port, self.ipport)
                    self.ptype = TCP

                # Graph
                self.graph_type = dialog.graph_type

                if self.client:
                    self.label_gen_port.setText(self.gen_port)
                    self.btn_com_gen.hide()
                    self.btn_pulldata.setEnabled(True)
                    self.btn_esc_run.setEnabled(True)
                    self.btn_params.setEnabled(True)
                    
                self.initial = device.read_setting_value(self.client, self.ptype)
                if self.initial:
                    self.toggle = self.initial['rd_toggle']

        else:
            print("Open Dialog")

    # CheckBox clicked
    def checkBoxChanged(self, category):
        if category == 'OUT VOLTAGE1':
            if self.cbx_vol1.isChecked() == True:
                self.view_vol1 = True
                self.graph_vol1.show()
            else:
                self.view_vol1 = False
                self.graph_vol1.hide()

        elif category == 'OUT CURRENT1':
            if self.cbx_cur1.isChecked() == True:
                self.view_cur1 = True
                self.axisB.addItem(self.itemCurrent1)
                self.axisB.show()
            else:
                self.view_cur1 = False 
                self.axisB.removeItem(self.itemCurrent1)
                # self.axisB.hide()

        elif category == 'OUT VOLTAGE2':
            if self.cbx_vol2.isChecked() == True:
                self.view_vol2 = True
                self.graph_vol2.show()
            else:
                self.view_vol2 = False 
                self.graph_vol2.hide()

        elif category == 'OUT CURRENT2':
            if self.cbx_cur2.isChecked() == True:
                self.view_cur2 = True
                self.axisB.addItem(self.itemCurrent2)
                self.axisB.show()
            else:
                self.view_cur2 = False
                self.axisB.removeItem(self.itemCurrent2)
                # self.axisB.hide()

        elif category == 'VBIAS':
            if self.cbx_vbia.isChecked() == True:
                self.view_vbia = True
                self.axisC.addItem(self.itemVbia)
                self.axisC.show()
            else:
                self.view_vbia = False 
                self.axisC.removeItem(self.itemVbia)
                # self.axisC.hide()

        elif category == 'CPS':
            if self.cbx_cps.isChecked() == True:
                self.view_cps = True
                self.axisD.addItem(self.itemCps)
                self.axisD.show()
            else:
                self.view_cps = False  
                self.axisD.removeItem(self.itemCps)
                # self.axisD.hide()

        elif category == 'LEAK CURRENT1':
            if self.cbx_leak_cur1.isChecked() == True:
                self.view_leak_cur1 = True
                self.axisD.addItem(self.itemLeakCur1)
                self.axisD.show()
            else:
                self.view_leak_cur1 = False 
                self.axisD.removeItem(self.itemLeakCur1)
                # self.axisD.hide()

        elif category == 'LEAK CURRENT2':
            if self.cbx_leak_cur2.isChecked() == True:
                self.view_leak_cur2 = True
                self.axisD.addItem(self.itemLeakCur2)
                self.axisD.show()
            else:
                self.view_leak_cur2 = False 
                self.axisD.removeItem(self.itemLeakCur2)
                # self.axisD.hide()
        else:
            print(category, "No Category")

    # SAVE DATA as JSON FILE
    def save_data(self):
        data = {}
        # Dialog = QDialog()
        # self.com_open_flag == True
        # dialog = DataWin(Dialog)
        # dialog.show()
        # response = dialog.exec_()
        FileSave = QFileDialog.getSaveFileName(self, '파일 저장', './')
        file = FileSave[0]

        time_list = []

        for tm in self.times:
            if isinstance(tm, str):
                time_list.append(tm)
            else:
                tm = time.localtime(tm)
                string = time.strftime('%I:%M:%S', tm)
                time_list.append(string)

        
        dataset = pd.DataFrame({'indexs': self.indexs.tolist(),
                    "times": time_list,
                    'vol1s': self.vol1s.tolist(),
                    'curt1s': self.curt1s.tolist(),
                    'vol2s': self.vol2s.tolist(),
                    'curt2s': self.curt2s.tolist(),
                    'vbias': self.vbias.tolist(),
                    'vcss': self.vcss.tolist(),
                    'icss': self.icss.tolist(),
                    'cpss': self.cpss.tolist(),
                    'leak_cur1s': self.leak_cur1s.tolist(),
                    'leak_cur2s': self.leak_cur2s.tolist(),
        })

        product = 'PSES-50250PBN'
        user = 'USER'

        # CSS FILE
        csvfile = f"{file}.csv"
        dataset.to_csv(csvfile, sep=',')

        Nindexs = self.indexs.tolist()
        Times = time_list
        NVoltage1s = self.vol1s.tolist()
        NCurrent1s = self.curt1s.tolist()
        NVoltage2s = self.vol2s.tolist()
        NCurrent2s = self.curt2s.tolist()
        NVbias = self.vbias.tolist()

        NVcss = self.vcss.tolist()
        NIcss = self.icss.tolist()
        NCpss = self.cpss.tolist()
        NLeak_Cur1s = self.leak_cur1s.tolist()
        NLeak_Cur2s = self.leak_cur2s.tolist()

        data['product'] = 'PSES-50250PBN'
        data['user'] = 'USER'
        
        data['indexs'] = Nindexs
        data['times'] = Times
        data['vol1s'] = NVoltage1s
        data['curt1s'] = NCurrent1s
        data['vol2s'] = NVoltage2s
        data['curt2s'] = NCurrent2s
        data['vbias'] = NVbias
        data['vcss'] = NVcss
        data['icss'] = NIcss
        data['cpss'] = NCpss
        data['leak_cur1s'] = NLeak_Cur1s
        data['leak_cur2s'] = NLeak_Cur2s
        
        # JSON FILE
        jsonfile = f"{file}.json"
        with open(jsonfile, 'wb') as afile:
            json.dump(data, codecs.getwriter('utf-8')(afile))


    # Read File DATA (JSON)
    def read_data(self):
        fname, _ = QFileDialog.getOpenFileName(self, 'Open file', 
         '.',"Json files (*.json)")
        with open(fname, 'r', encoding='utf-8') as json_file:
            data = json.load(json_file)
            self.indexs = data['indexs']
            self.times = data['times']
            # print(self.times)
            self.vol1s = data['vol1s']
            self.curt1s = data['curt1s']
            self.vol2s = data['vol2s']
            self.curt2s = data['curt2s']
            self.vbias = data['vbias']
            self.vcss = data['vcss']
            self.icss = data['icss']
            self.cpss = data['cpss']

            try:
                self.leak_cur1s = data['leak_cur1s']
            except:
                self.leak_cur1s = data['dechuck_starts']
            
            try:
                self.leak_cur2s = data['leak_cur2s']
            except:
                self.leak_cur2s = data['dechuck_ends']

            self.temp_indexs = data['indexs']
            self.temp_times = data['times']
            self.temp_vol1s = data['vol1s']
            self.temp_cur1s = data['curt1s']
            self.temp_vol2s = data['vol2s']
            self.temp_cur2s = data['curt2s']
            self.temp_vbias = data['vbias']
            self.temp_vcss = data['vcss']
            self.temp_icss = data['icss']
            self.temp_cpss = data['cpss']
            
            try:
                self.temp_leak_cur1s = data['leak_cur1s']
            except:
                self.temp_leak_cur1s = data['dechuck_starts']
            
            try:
                self.temp_leak_cur2s = data['leak_cur2s']
            except:
                self.temp_leak_cur2s = data['dechuck_ends']

            product =  data.get('product')
            user =  data.get('user', '이름없음')

            title = "PRODUCT : {} / WRITER : {}".format(product, user)

        self.graphWidget.setTitle(title, color="b", size="20pt")

        self.btn_savedata.setEnabled(False)
        self.btn_zoomA.setEnabled(True)
        self.btn_zoom6.setEnabled(True)

        self.draw_graph()

  

if __name__ == "__main__":
    app = QApplication(sys.argv)
    form = MainWindow()
    form.show()
    sys.exit(app.exec_())