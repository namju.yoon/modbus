#!/usr/bin/env python
# coding: utf-8

# 예제 내용
# * 기본 위젯을 사용하여 기본 창을 생성
# * 다양한 레이아웃 위젯 사용
import os
import sys
import time
import random
import math
from datetime import datetime
from random import randint
import pyqtgraph as pyGraph
import pyqtgraph.exporters

import numpy as np
import pandas as pd
import serial
import json, codecs
from ctypes import c_uint16
from threading import Timer

from PyQt5.QtWidgets import QWidget, QLabel, QLineEdit, QPushButton, QDesktopWidget, QMainWindow
from PyQt5.QtWidgets import QGroupBox, QBoxLayout, QVBoxLayout, QHBoxLayout, QGridLayout, QSpinBox
from PyQt5.QtWidgets import QApplication, QDialog, QStatusBar, QLineEdit, QCheckBox, QSizePolicy
from PyQt5.QtWidgets import QRadioButton
from PyQt5 import QtCore
from PyQt5.QtCore import QDate, Qt
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QLCDNumber
from PyQt5.QtCore import QDate, Qt, QThread, pyqtSlot

from common.setup_deviceaging import SettingWin
from common.esc_params import ESCParams
from common.block import resource_path, SimpleDisplay
from common import escjig_communication as device
from common.code import *

# import logging
# FORMAT = ('%(asctime)-15s %(threadName)-15s '
#           '%(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
# logging.basicConfig(format=FORMAT)
# log = logging.getLogger()
# log.setLevel(logging.DEBUG)

class MyLineEdit(QLineEdit):
    def __init__(self, *args, **kwargs):
        super(MyLineEdit, self).__init__(*args, **kwargs)
        self.setAlignment(Qt.AlignRight)
        self.setFixedHeight(20)
        self.setMaximumHeight(20)
        self.setStyleSheet("background-color: #eee;")

    def reset_edit(self):
        self.setText("0")
        self.setStyleSheet("background-color: #eee;")

class MyLabel(QLabel):
    def __init__(self, *args, **kwargs):
        super(MyLabel, self).__init__(*args, **kwargs)
        self.setFixedHeight(20)
        self.setMaximumHeight(20)

class GLabel(MyLabel):
    def __init__(self, *args, **kwargs):
        super(GLabel, self).__init__(*args, **kwargs)
        self.setAlignment(Qt.AlignRight)

class ESCCalibration(QMainWindow):
    def __init__(self):
        super(ESCCalibration, self).__init__()

        ## port 중복 제
        self.used_port = []

        self.com_open_flag = False
        self.esc_client = None
        self.jig_client = None
        self.stopFlag = True

        self.timer = QtCore.QTimer()
        self.displaytimer = QtCore.QTimer()
        self.fault_loop = 0

        self.esc_val = 0
        self.jig_val = 0

        self.set_offset = 0
        self.curr_gain = 0

        self.cal_index = 0
        self.gain_dir = None

        self.cal_params = BPOW
        self.old_acoeff = 0
        self.old_bpow = 0
        self.starttime = None

        ## ESC value
        self.serialNo = None
        self.fw_version = None

        self.vo1_offset = 0
        self.vo1_gain = 0
        self.vo2_offset = 0
        self.vo2_gain = 0

        self.io1_offset = 0
        self.io1_gain = 0
        self.io2_offset = 0
        self.io2_gain = 0

        self.ir1_offset = 0
        self.ir1_gain = 0
        self.ir2_offset = 0
        self.ir2_gain = 0

        self.vbia_offset = 0
        self.vbia_gain = 0
        
        self.cap_offset = 0
        self.zcs1_gain = 0
        self.zcs2_gain = 0

        self.vcs_gain = 0
        self.ics_gain = 0

        self.initUI()

    def initUI(self):
        ## Main Layout 설정
        self.setWindowTitle("PSTEK ELECTROSTATIC CHUCK CALIBRATION AUTOMATION")
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width(), screensize.height()) 
 
        mainwidget = QWidget()                # 위젯의 인스턴스 생성만으로도 QMainWindow에 붙는다.
        self.setCentralWidget(mainwidget)

        main_layer = QVBoxLayout()
        mainwidget.setLayout(main_layer)

        self.bluefont = "font-weight: bold; color: #0000ff;"
        self.redfont = "font-weight: bold; color: #ff0000;"
        self.greenfont = "font-weight: bold; color: #00ff00;"
        self.btnback = "font-weight: bold; background-color: #cdcdcd;"
        self.blackfont = "font-weight: bold; background-color: #000;"
        self.jigback = "font-weight: bold; background-color: #ccffeb"
        self.unitback = "font-weight: bold; background-color: #e0ebeb"
        self.greenback = "font-weight: bold; background-color: #00ff00"
        self.redback = "font-weight: bold; background-color: #ff0000"

        layout_setup = QHBoxLayout()    
        main_layer.addLayout(layout_setup)
        self.displayMainMenu(layout_setup)

        ## 2nd green button ##################
        # 그래프 범례
        grp_legend = self.displayCalibationButton()
        main_layer.addWidget(grp_legend)
        # main_layer.addLayout(layout_legend)

        ## 3rd gain offset value ############
        ## Calibration Data Display
        grp_datadisplay = self.displayCalibationResult()
        main_layer.addWidget(grp_datadisplay)

        ## 4th CAP value ###############
        ## ZCS Calibration
        grp_zcscalib = self.displayZCSCalibration()
        main_layer.addWidget(grp_zcscalib)

        ## 5 th ########################
        grp_escvalue = self.displayESCValue()
        main_layer.addWidget(grp_escvalue)
        ## 6th Status Bar ########################
        # Status
        self.statusbar = QStatusBar()
        self.setStatusBar(self.statusbar)
        self.statusbar.setObjectName("statusbar")
        
        self.statusmessage = 'PSTEK :: 현재 등록자:() :: 등록 시간 : {}'
        now = QDate.currentDate()
        displaymessage = self.statusmessage.format(now.toString(Qt.ISODate))
        self.statusbar.showMessage(displaymessage)

        ## TEMP
        self.temp_setup_connection()

    ## Main Menu
    def displayMainMenu(self, layout_setup):
        ## replace #1st ###############
        # JIG 연결
        # self.btn_esc_gen.setStyleSheet("color: #ff0000;")
        self.btn_jig = QPushButton("JIG 연결")
        self.btn_jig.clicked.connect(self.connect_jig_gen)
        self.label_jig = QLabel("No")
        self.displayMenuLayout(layout_setup, "JIG 연결)", 
                self.btn_jig, self.label_jig)

        # 전원 장치 JIG 연결
        self.btn_esc_gen = QPushButton("ESC 연결")
        self.btn_esc_gen.clicked.connect(self.connect_esc_gen)
        self.label_esc_gen = QLabel("No")
        self.displayMenuLayout(layout_setup, "ESC 연결)", 
                self.btn_esc_gen, self.label_esc_gen)

        # DEVICE UNIT MODIFY VOLTAGE, TOGGLE
        # self.btn_esc_gen.setStyleSheet("color: #ff0000;")
        self.btn_params = QPushButton("전원 장치 값 변경")
        self.btn_params.clicked.connect(self.device_params_modify)
        self.displayMenuLayout(layout_setup, "전원 장치  변경", 
                self.btn_params)

        # 단말기 Calibration
        self.btn_calib = QPushButton("캘리 시작")
        self.btn_calib.setStyleSheet(self.bluefont)
        # self.btn_calib.setEnabled(False)
        self.btn_calib.clicked.connect(self.esc_calibration)
        
        # ZCS ACOF BPOW START
        self.btn_cal_irleak = QPushButton("IRLEAK")
        # self.btn_cal_irleak.setEnabled(False)
        self.btn_cal_irleak.clicked.connect(self.start_irleak)

        self.btn_cal_vbias = QPushButton("VBIAS")
        # self.btn_cal_vbias.setEnabled(False)
        self.btn_cal_vbias.clicked.connect(self.start_vbias_offset_new)

        self.btn_cal_cap = QPushButton("ZCS")
        # self.btn_cal_cap.setEnabled(False)
        self.btn_cal_cap.clicked.connect(self.start_cap_value)

        self.btn_aout = QPushButton("AOUT")
        # self.btn_aout.setEnabled(False)
        self.btn_aout.clicked.connect(self.analogOut_setting)

        self.btn_din = QPushButton("D IN")
        # self.btn_din.setEnabled(False)
        self.btn_din.clicked.connect(self.checkout_digital_start)

        self.displayVerLayout(layout_setup, "Calibration", 
                self.btn_calib, self.btn_cal_irleak, 
                self.btn_cal_vbias, self.btn_cal_cap, 
                self.btn_aout, self.btn_din)

        # 단말기 STOP
        self.btn_stop = QPushButton("ESC Stop")
        self.btn_stop.setStyleSheet(self.redfont)
        self.btn_stop.clicked.connect(self.all_device_stop)
        self.displayMenuLayout(layout_setup, "Stop", 
                self.btn_stop)

        self.btn_reset = QPushButton("RESET")
        self.btn_reset.setStyleSheet(self.redfont)
        self.btn_reset.clicked.connect(self.reset_data)
        self.displayMenuLayout(layout_setup, "Reset", 
                self.btn_reset)

        # Logo Image
        labelLogo = QLabel("")
        pixmap = QPixmap(resource_path("logo.png"))
        labelLogo.setPixmap(pixmap)
        labelLogo.setAlignment(Qt.AlignRight)
        layout_setup.addWidget(labelLogo)

    ## Calibration 완료 여부 
    def displayCalibationButton(self):
        grp_legend = QGroupBox("Calibration 완료 (녹색이면 완료)")
        layout_legend = QGridLayout()

        # button
        btn_calib_finish = QPushButton("Cali 완료")
        btn_calib_finish.setStyleSheet(self.bluefont)
        layout_legend.addWidget(btn_calib_finish, 0, 0, 2, 1)

        # voltage 
        self.btn_vo1_offset = QPushButton("VO 1 옵셋")
        self.btn_vo1_gain = QPushButton("VO 1 GAIN")
        self.btn_vo2_offset = QPushButton("VO 2 옵셋")
        self.btn_vo2_gain = QPushButton("VO 2 GAIN")
        self.layoutButtonGid(layout_legend, 1, self.btn_vo1_offset, 
                self.btn_vo1_gain, self.btn_vo2_offset, self.btn_vo2_gain)
        
        # current 
        self.btn_io1_offset = QPushButton("IO 1 옵셋")
        self.btn_io1_gain = QPushButton("IO 1 GAIN")
        self.btn_io2_offset = QPushButton("IO 2 옵셋")
        self.btn_io2_gain = QPushButton("IO 2 GAIN")
        self.layoutButtonGid(layout_legend, 3, self.btn_io1_offset, 
                self.btn_io1_gain, self.btn_io2_offset, self.btn_io2_gain)

        # IR 
        self.btn_ir1_offset = QPushButton("IR 1 옵셋")
        self.btn_ir1_gain = QPushButton("IR 1 GAIN")
        self.btn_ir2_offset = QPushButton("IR 2 옵셋")
        self.btn_ir2_gain = QPushButton("IR 2 GAIN")
        self.layoutButtonGid(layout_legend, 5, self.btn_ir1_offset, 
                self.btn_ir1_gain, self.btn_ir2_offset, self.btn_ir2_gain)

        # CAP
        self.btn_vbia_offset = QPushButton("VBIAS 옵셋")
        self.btn_vbia_gain = QPushButton("VBIAS GAIN")
        self.btn_cap_offset = QPushButton("CAP 옵셋")
        self.btn_zcs1_gain = QPushButton("ZCS1 GAIN")
        self.layoutButtonGid(layout_legend, 7, self.btn_vbia_offset, 
                self.btn_vbia_gain, self.btn_cap_offset, self.btn_zcs1_gain)

        self.btn_zcs2_gain = QPushButton("ZCS2 GAIN")
        self.btn_aout1_gain = QPushButton("A OUT1 GAIN")
        self.btn_aout2_gain = QPushButton("A OUT2 GAIN")
        self.btn_din1_check = QPushButton("D IN CHECK")
        self.layoutButtonGid(layout_legend, 9, self.btn_zcs2_gain, 
                self.btn_aout1_gain, self.btn_aout2_gain, self.btn_din1_check)

        self.btn_dout1_check = QPushButton("DOUT1 CHECK")
        self.btn_dout2_check = QPushButton("DOUT2 CHECK")
        self.btn_dout3_check = QPushButton("DOUT3 CHECK")
        
        self.layoutButtonGid(layout_legend, 11, self.btn_dout1_check, 
                self.btn_dout2_check, self.btn_dout3_check)
        

        self.display_time = QLCDNumber()
        self.display_time.setDigitCount(8)
        self.display_time.setStyleSheet(self.jigback)
        secondtime = 0
        timedisplay = '{:02d}:{:02d}:{:02d}'.format(
                (secondtime // 60) // 60, (secondtime // 60) % 60, secondtime % 60)
        self.display_time.display(timedisplay)

        layout_legend.addWidget(self.display_time, 0, 13, 2, 2)
        grp_legend.setLayout(layout_legend)

        return grp_legend

    ## Offset Gain 값
    def displayCalibationResult(self):
        grp_datadisplay = QGroupBox("Calibration Value Display")
        layout_datadisplay = QGridLayout()

        btn_item1 = QPushButton("구분")
        btn_item1.setStyleSheet(self.jigback)
        btn_offset1 = QPushButton("OFFSET")
        btn_offset1.setStyleSheet(self.jigback)
        btn_gain1 = QPushButton("GAIN")
        btn_gain1.setStyleSheet(self.jigback)

        btn_item2 = QPushButton("구분")
        btn_item2.setStyleSheet(self.jigback)
        btn_offset2 = QPushButton("OFFSET")
        btn_offset2.setStyleSheet(self.jigback)
        btn_gain2 = QPushButton("GAIN")
        btn_gain2.setStyleSheet(self.jigback)

        self.LayoutCalGrid(layout_datadisplay, 0,
            btn_item1, btn_offset1, btn_gain1, "단위",
            btn_item2, btn_offset2, btn_gain2, "단위")

        # Voltage 
        btn_vol1 = GLabel("OUT VOLTAGE 1 : ")
        self.edit_vo1_offset = MyLineEdit("0")
        self.edit_vo1_gain = MyLineEdit("0")
        btn_vol2 = GLabel("OUT VOLTAGE 2 : ")
        self.edit_vo2_offset = MyLineEdit("0")
        self.edit_vo2_gain = MyLineEdit("0")
        self.LayoutCalGrid(layout_datadisplay, 1,
            btn_vol1, self.edit_vo1_offset, self.edit_vo1_gain, "V",
            btn_vol2, self.edit_vo2_offset, self.edit_vo2_gain, "V")

        # Current 
        btn_cur1 = GLabel("OUT CURRENT 1 : ")
        self.edit_io1_offset = MyLineEdit("0")
        self.edit_io1_gain = MyLineEdit("0")
        btn_cur2 = GLabel("OUT CURRENT 2 : ")
        self.edit_io2_offset = MyLineEdit("0")
        self.edit_io2_gain = MyLineEdit("0")
        self.LayoutCalGrid(layout_datadisplay, 2,
            btn_cur1, self.edit_io1_offset, self.edit_io1_gain, "mA",
            btn_cur2, self.edit_io2_offset, self.edit_io2_gain, "mA")

        # IR 
        btn_irleak1 = GLabel("IO LEAK 1 : ")
        self.edit_ir1_offset = MyLineEdit("0")
        self.edit_ir1_gain = MyLineEdit("0")
        btn_irleak2 = GLabel("IO LEAK 2 : ")
        self.edit_ir2_offset = MyLineEdit("0")
        self.edit_ir2_gain = MyLineEdit("0")
        self.LayoutCalGrid(layout_datadisplay, 3,
            btn_irleak1, self.edit_ir1_offset, self.edit_ir1_gain, "uA",
            btn_irleak2, self.edit_ir2_offset, self.edit_ir2_gain, "uA")

        # VBIAS 
        btn_vbia = GLabel("VBIAS : ")
        self.edit_vbia_offset = MyLineEdit("0")
        self.edit_vbia_gain = MyLineEdit("0")

        # CAP 
        btn_cap = GLabel("CAP OFFSET : ")
        self.edit_cap_offset = MyLineEdit("0")
        self.edit_cap_gain = MyLineEdit("0")

        self.LayoutCalGrid(layout_datadisplay, 4,
            btn_vbia, self.edit_vbia_offset, self.edit_vbia_gain, "pF",
            btn_cap, self.edit_cap_offset, self.edit_cap_gain, "pF")

        # ZCS CAP 2
        btn_zcsacoeff = GLabel("ZCS A COE : ")
        self.edit_zcsacoeff = MyLineEdit("0")
        btn_zcsbpow = GLabel("ZCS B POW : ")
        self.edit_zcsbpow = MyLineEdit("0")
        self.LayoutCalGrid2(layout_datadisplay, 5, 
                btn_zcsacoeff, self.edit_zcsacoeff,
                btn_zcsbpow, self.edit_zcsbpow)

        ## A OUT GAIN
        btn_aout1_gain = GLabel("A OUT 1 GAIN:")
        self.edit_aout1_gain = MyLineEdit("0")
        btn_aout2_gain = GLabel("A OUT 2 GAIN:")
        self.edit_aout2_gain = MyLineEdit("0")

        self.LayoutCalGrid2(layout_datadisplay, 6, 
                btn_aout1_gain, self.edit_aout1_gain,
                btn_aout2_gain, self.edit_aout2_gain)


        grp_datadisplay.setLayout(layout_datadisplay)
        return grp_datadisplay 

    ## CAP 값 
    def displayZCSCalibration(self):
        grp_zcscalib = QGroupBox("ZCS Calibration")
        layout_zcscalib = QGridLayout()

        label_cap0 = GLabel("CAP0")
        self.edit_base_cap0 = QLineEdit("160")
        self.edit_val_cap0 = MyLineEdit("")
        self.edit_rate0 = MyLineEdit("")
        self.LayoutTwoRwoGrid(layout_zcscalib, 0, 0, label_cap0, 
                self.edit_base_cap0, self.edit_val_cap0, self.edit_rate0)

        label_cap1 = GLabel("CAP1")
        self.edit_base_cap1 = QLineEdit("244")
        self.edit_val_cap1 = MyLineEdit("")
        self.edit_rate1 = MyLineEdit("")
        self.LayoutTwoRwoGrid(layout_zcscalib, 0, 4, label_cap1, 
                self.edit_base_cap1, self.edit_val_cap1, self.edit_rate1)

        label_cap2 = GLabel("CAP2")
        self.edit_base_cap2 = QLineEdit("485")
        self.edit_val_cap2 = MyLineEdit("")
        self.edit_rate2 = MyLineEdit("")
        self.LayoutTwoRwoGrid(layout_zcscalib, 0, 8, label_cap2, 
                self.edit_base_cap2, self.edit_val_cap2, self.edit_rate2)

        label_cap3 = GLabel("CAP3")
        self.edit_base_cap3 = QLineEdit("748")
        self.edit_val_cap3 = MyLineEdit("")
        self.edit_rate3 = MyLineEdit("")
        self.LayoutTwoRwoGrid(layout_zcscalib, 0, 12, label_cap3, 
                self.edit_base_cap3, self.edit_val_cap3, self.edit_rate3)

        label_cap4 = GLabel("CAP4")
        self.edit_base_cap4 = QLineEdit("1079")
        self.edit_val_cap4 = MyLineEdit("")
        self.edit_rate4 = MyLineEdit("")
        self.LayoutTwoRwoGrid(layout_zcscalib, 0, 16, label_cap4, 
                self.edit_base_cap4, self.edit_val_cap4, self.edit_rate4)

        label_cap5 = GLabel("CAP5")
        self.edit_base_cap5 = QLineEdit("1463")
        self.edit_val_cap5 = MyLineEdit("")
        self.edit_rate5 = MyLineEdit("")
        self.LayoutTwoRwoGrid(layout_zcscalib, 1, 0, label_cap5, 
                self.edit_base_cap5, self.edit_val_cap5, self.edit_rate5)

        label_cap6 = GLabel("CAP6")
        self.edit_base_cap6 = QLineEdit("2766")
        self.edit_val_cap6 = MyLineEdit("")
        self.edit_rate6 = MyLineEdit("")
        self.LayoutTwoRwoGrid(layout_zcscalib, 1, 4, label_cap6, 
                self.edit_base_cap6, self.edit_val_cap6, self.edit_rate6)

        label_cap7 = GLabel("CAP7")
        self.edit_base_cap7 = QLineEdit("4899")
        self.edit_val_cap7 = MyLineEdit("")
        self.edit_rate7 = MyLineEdit("")
        self.LayoutTwoRwoGrid(layout_zcscalib, 1, 8, label_cap7, 
                self.edit_base_cap7, self.edit_val_cap7, self.edit_rate7)

        label_cap8 = GLabel("CAP8")
        self.edit_base_cap8 = QLineEdit("7394")
        self.edit_val_cap8 = MyLineEdit("")
        self.edit_rate8 = MyLineEdit("")
        self.LayoutTwoRwoGrid(layout_zcscalib, 1, 12, label_cap8, 
                self.edit_base_cap8, self.edit_val_cap8, self.edit_rate8)

        label_cap9 = GLabel("CAP9")
        self.edit_base_cap9 = QLineEdit("10016")
        self.edit_val_cap9 = MyLineEdit("")
        self.edit_rate9 = MyLineEdit("")
        self.LayoutTwoRwoGrid(layout_zcscalib, 1, 16, label_cap9, 
                self.edit_base_cap9, self.edit_val_cap9, self.edit_rate9)
        
        grp_zcscalib.setLayout(layout_zcscalib)

        return grp_zcscalib

    ## ESC value
    def displayESCValue(self):
        ## 5-1 ########################
        ## ESC 전원장치 설정값
        grp_escvalue = QGroupBox("ESC 전원장치 설정값")
        
        # 전체 Group 내 배치 
        layout_group = QGridLayout()
        grp_escvalue.setLayout(layout_group)

        group_setting = QGroupBox("")
        grid_box = QGridLayout()
        group_setting.setLayout(grid_box)
        
        # Control Commands 
        # First Region
        self.voltage1 = MyLineEdit("0")
        self.current1 = MyLineEdit("0")
        self.LayoutValueGrid(grid_box, 0,
            "OUT VOLTAGE1: ", self.voltage1, "-2500 ~ 2500", "V",
            "OUT CURRENT1: ", self.current1, "0 ~ 10.00", "mA")
        
        self.voltage2 = MyLineEdit("0")
        self.current2 = MyLineEdit("0")
        self.LayoutValueGrid(grid_box, 1,
            "OUT VOLTAGE2: ", self.voltage2, "-2500 ~ 2500", "V",
            "OUT CURRENT2: ", self.current2, "0 ~ 10.00", "mA")

        self.leak_fault_level = MyLineEdit("0")
        self.ro_min_fault = MyLineEdit("0")
        self.LayoutValueGrid(grid_box, 2,
            "LEAK FAULT LEVEL: ", self.leak_fault_level, "0 ~ 1.00", "mA",
            "RO MIN FALUT: ", self.ro_min_fault, "0 ~ 999", "Kohm")

        self.up_time = MyLineEdit("0")
        self.down_time = MyLineEdit("0")
        self.LayoutValueGrid(grid_box, 3,
            "RAMP UP TIME: ", self.up_time, "0.3 ~ 9.9 ", "sec",
            "RAMP DOWN TIME: ", self.down_time, "0.3 ~ 9.9 ", "sec")


        self.slope = MyLineEdit("0")
        self.toggle_count = MyLineEdit("0")
        self.LayoutValueGrid(grid_box, 4,
            "SLOPE: ", self.slope, "10 ~ 100", "",
            "AUTO TOGGLE COUNT: ", self.toggle_count, "1 ~ 99", "")


        self.arc_delay = MyLineEdit("0")
        self.coeff = MyLineEdit("0")
        self.LayoutValueGrid(grid_box, 5,
            "ARC DELAY: ", self.arc_delay, "10.00 ~ 50.00 ", "m sec",
            "COEFF: ", self.coeff, "1 ~ 10", "")


        self.arc_rate = MyLineEdit("0")
        self.local_address = MyLineEdit("0")
        self.LayoutValueGrid(grid_box, 6,
            "ARC RATE: ", self.arc_rate, "1 ~ 1000 ", "a/s",
            "LOCAL ADDRESS: ", self.local_address, "1 ~ 31", "")

        self.target_cap = MyLineEdit("0")
        self.cap_deviation = MyLineEdit("0")
        self.LayoutValueGrid(grid_box, 7,
            "TARGET CAPACITOR: ", self.target_cap, "1 ~ 15000 ", "pF",
            "CAP DEVIATION: ", self.cap_deviation, "0 ~ 100", "%")

        self.time_delay = MyLineEdit("0")
        grid_box.addWidget(GLabel("TIME DELAY"), 8, 0)
        grid_box.addWidget(self.time_delay, 8, 1)
        grid_box.addWidget(MyLabel("0 ~ 100"), 8, 2)
        grid_box.addWidget(MyLabel("Sec"), 8, 3)

        layout_group.addWidget(group_setting, 0, 0, 1, 3)

        ## 5-2 ########################
        ## ESC 전원장치 설정값
        grp_esc = QGroupBox("")
        layout_esc = QGridLayout()

        # voltage 1
        self.lbl_voltage1 = MyLineEdit("0")
        unit_voltage1 = MyLabel("V")
        self.LayoutThreeGrid(layout_esc, 1, "OUT VOLTAGE 1 : ", 
                self.lbl_voltage1, unit_voltage1)

        # current 1
        self.lbl_current1 = MyLineEdit("0")
        unit_current1 = MyLabel("mA")
        self.LayoutThreeGrid(layout_esc, 2, "OUT CURRENT 1 : ", 
                self.lbl_current1, unit_current1)

        # voltage 2
        self.lbl_voltage2 = MyLineEdit("0")
        unit_voltage2 = MyLabel("V")
        self.LayoutThreeGrid(layout_esc, 3, "OUT VOLTAGE 2 : ", 
                self.lbl_voltage2, unit_voltage2)

        # current 2
        self.lbl_current2 = MyLineEdit("0")
        unit_current2 = MyLabel("mA")
        self.LayoutThreeGrid(layout_esc, 4, "OUT CURRENT 2 : ", 
                self.lbl_current2, unit_current2)

        # VBIAS
        self.lbl_vbias = MyLineEdit("0")
        unit_vbias = MyLabel("V")
        self.LayoutThreeGrid(layout_esc, 5, "VBIAS : ", 
                self.lbl_vbias, unit_vbias)

        # VCS
        self.lbl_vcs = MyLineEdit("0")
        unit_vcs = MyLabel("V")
        self.LayoutThreeGrid(layout_esc, 6, "VCS : ", 
                self.lbl_vcs, unit_vcs)

        # ICS
        self.lbl_ics = MyLineEdit("0")
        unit_ics = MyLabel("V")
        self.LayoutThreeGrid(layout_esc, 7, "ICS : ", 
                self.lbl_ics, unit_ics)

        # CP
        self.lbl_cp = MyLineEdit("0")
        unit_cp = MyLabel("pF")
        self.LayoutThreeGrid(layout_esc, 8, "CAP : ", 
                self.lbl_cp, unit_cp)

        # IO LEAK 1
        self.lbl_leak1 = MyLineEdit("0")
        unit_leak1 = MyLabel("uA")
        self.LayoutThreeGrid(layout_esc, 9, "IO LEAK 1 : ", 
                self.lbl_leak1, unit_leak1)

        # IO LEAK 2
        self.lbl_leak2 = MyLineEdit("0")
        unit_leak2 = MyLabel("uA")
        self.LayoutThreeGrid(layout_esc, 10, "IO LEAK 2 : ", 
                self.lbl_leak2, unit_leak2)
    
        # grp_esc
        grp_esc.setLayout(layout_esc)
        layout_group.addWidget(grp_esc, 0, 3, 1, 2)

        ## 5-3
        # # Radio Button 
        self.rd_mode_off = QRadioButton("OFF")
        self.rd_mode_off.setChecked(True)
        self.rd_mode_on = QRadioButton("ON")
        self.LayoutRadioBoxGrid(layout_group, 1, 0, "AUTO TOGGLE :",
                self.rd_mode_off, self.rd_mode_on, "(O:OFF, 1:ON)")


        self.rd_select_internal = QRadioButton("INTERNAL")
        self.rd_select_internal.setChecked(True)
        self.rd_select_remote = QRadioButton("REMOTE")
        self.LayoutRadioBoxGrid(layout_group, 1, 1, "SELECTION :",
                self.rd_select_internal, self.rd_select_remote, "(O:INTERNAL, 1:REMOTE)")

        
        # Third Region
        self.rd_toggle_off = QRadioButton("FORWARD")
        self.rd_toggle_off.setChecked(True)
        self.rd_toggle_on = QRadioButton("REVERSE")
        self.LayoutRadioBoxGrid(layout_group, 1, 2, "TOGGLE:",
                self.rd_toggle_off, self.rd_toggle_on, "(0:FORWARD, 1:REVERSE)")


        self.rd_arc_off = QRadioButton("OFF")
        self.rd_arc_off.setChecked(True)
        self.rd_arc_on = QRadioButton("ON")
        self.LayoutRadioBoxGrid(layout_group, 1, 3, "ARC CONTROL:",
                self.rd_arc_off, self.rd_arc_on, "(O:OFF, 1:ON)")

        
        self.rd_ocp_off = QRadioButton("OFF")
        self.rd_ocp_off.setChecked(True)
        self.rd_ocp_on = QRadioButton("ON")
        self.LayoutRadioBoxGrid(layout_group, 1, 4, "OCP CONTROL:",
                self.rd_ocp_off, self.rd_ocp_on, "(O:OFF, 1:ON)")

        grp_escvalue.setLayout(layout_group)

        return grp_escvalue


    def start_timer(self):
        self.starttime = datetime.now()
        self.displaytimer = QtCore.QTimer()
        self.displaytimer.timeout.connect(self.timedisplay)
        self.displaytimer.start(DISPLAY_TIME)

    # esc_calibration
    def esc_calibration(self):
        
        device.check_password(self.esc_client)

        self.start_timer()
        
        ## ONONON
        # PC에서 JIG에 CAP_ON10, CAP_ON11, CAP_ON14을 ON 설정하고, RUN 지령을 한다.
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON10, device.ON)
        time.sleep(TIME_INTERVAL)

        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON11, device.ON)
        time.sleep(TIME_INTERVAL)

        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON14, device.ON)
        time.sleep(TIME_INTERVAL)

        # Voltage 
        # PC에서 ESC에 VO1 = 2500V, VO2 = -2500V로 설정하고, RUN 지령
        self.setting_voltage(2500, -2500)
        
        # DEVICE RUN
        self.esc_device_run()
        
        # PC에서 ESC에 FORWARD와 REVERSE 지령을 반복하면서 JIG의 VO1 값을 확인한다.
        # JIG의 VO1 FORWARD와 REVERSE 값이 같아지도록 ESC에 VO1 OFFSET을 조정한다.

        # CAll Function for Voltage 1 
        # client            : self.esc_client
        # value_read_address   : device.JIG_READ_VOLTAGE1
        # offset_address    : device.ESC_DEV_VO1_OFFSET
        # dis_value          : self.edit_vo1_gain
        # edit_rev          : self.edit_vol_tar1
        # btn               : self.btn_vo1_offset
        # ptype             : VOLTAGE1OFFSET
        
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(lambda:self.start_offset_calibration(self.jig_client, 
                    device.JIG_READ_VOLTAGE1, device.ESC_DEV_VO1_OFFSET, 
                    self.edit_vo1_offset, self.btn_vo1_offset, "VOLTAGE1OFFSET"))
        self.timer.start(CAPON_TIME)
    

    #############################################################
    #########  2. OFFSET START ##################################
    #############################################################
    # PC에서 ESC :: FORWARD와 REVERSE 지령을 반복하면서 JIG의 VO1 값을 확인한다.
    # JIG의 VO1 FORWARD와 REVERSE 값이 같아지도록 ESC에 VO1 OFFSET을 조정한다.

    # CAll Function for Voltage 1 
    # client            : JIG
    # value_read_address   : 읽어드릴 값의 주소
    # offset_address    : ESC의 값 조정 주소 
    # dis_value          : 해당 변화 값을 보여주는 Edit
    # btn               : 완료 후 색 변환
    # ptype             : 해당 type
    def start_offset_calibration(self, client, value_read_address, 
            offset_address, dis_value, btn, ptype):
    #
        # Timer 정지
        self.timer.stop()
        self.timer = None

        print("\n#### ", ptype, " START ####\n")
        # 기존 OFFSET Value 를 읽어 온다.
        self.curr_gain = device.read_value(self.esc_client, offset_address)
        dis_value.setText(str(self.curr_gain))

        self.gain_dir = None
        self.cal_index = 0
        self.old_diff = 0
        self.before_dir = PLUS
        self.repeatP = 0
        self.repeatN = 0

        # FORWARD
        device.write_registers(self.esc_client, device.ESC_WRITE_TOGGLE, device.FORWARD)
    
        # setting timer
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(lambda:self.step1_read_forward(client, value_read_address, 
                    offset_address, dis_value, btn, ptype))
        self.timer.start(READ_TIME)


    ## OFFSET :: FORWARD 값을 읽고, REVERSE 로 TOGGLE함
    def step1_read_forward(self, client, value_read_address, 
                offset_address, dis_value, btn, ptype):
    #
        # Timer 정지
        self.timer.stop()
        self.timer = None

        # READ_TIME 2초 후에 ESC 똔 JIG의 VO1 값을 읽어와 해당 값을 저장함 
        self.fwd_val = device.read_value(client, value_read_address)

        # ESC에 REVERSE 로 출력 명령을 내림
        device.write_registers(self.esc_client, device.ESC_WRITE_TOGGLE, device.REVERSE)

        # setting timer
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(lambda:self.step2_read_reverse(client, value_read_address, 
                    offset_address, dis_value, btn, ptype))
        self.timer.start(READ_TIME)


    ## OFFSET :: REVERSE 값을 읽고, 값을 비교하면서 값을 조정함
    ## @@@@ calculate_adjust_offsetgain
    def step2_read_reverse(self, client, value_read_address, 
                offset_address, dis_value, btn, ptype):
    #
        # Timer 정지
        self.timer.stop()
        self.timer = None 

        # 1초 후에 JIG의 VO1 값을 읽어와 해당 값을 저장함 : JIG_REV_VO1 = 2450
        self.rev_val = device.read_value(client, value_read_address)

        # JIG_FWD_VO1 과 JIG_REV_VO1 값의 차이를 구함 : DIFF_VO1 = JIG_REV_VO1 - JIG_FWD_VO1
        diff_val = abs(self.fwd_val) - abs(self.rev_val)
        print(ptype, " ::: FWD VAL :     ", self.fwd_val, "REV VAL :      ",  
                    self.rev_val, " ****************> ", diff_val)

        # OFFSET 조정 완료 
        if MINUS_OFFSET <= diff_val and diff_val <= PLUS_OFFSET:
            print("*"*50)
            print("***** :: OFFSET finish value : ", self.curr_gain)
            print("*"*50)
            btn.setStyleSheet(self.greenback)
            dis_value.setStyleSheet(self.greenback)
            self.get_esc_settings_value()
            self.get_esc_params()

            if ptype == 'VOLTAGE1OFFSET':
                # SAVE OFFSET VALUE 
                self.vo1_offset = self.curr_gain
                # ESC의 VO1 FORWARD값이 JIG의 VO1 값과 같아지도록 ESC에 VO1 GAIN을 조정한다.
                # ESC의 VO1 FORWARD값이 JIG의 VO1 값과 편차 1V 이내가 되면 VO1 GAIN은 완료.
                # CAll Function for Voltage 1 GAIN
                # client            : self.esc_client
                # esc_vol_address    : device.ESC_READ_VOLTAGE1
                # jig_vol_address    : device.JIG_READ_VOLTAGE1
                # gain_address      : device.ESC_DEV_VO1_GAIN
                # dis_value          : 해당 변화 값을 보여주는 Edit
                # edit_rev          : 해당 변화 값을 보여주는 Edit
                # btn               : 완료 후 색 변환
                # ptype             : 해당 type
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(lambda:self.start_gain_calibration(self.esc_client, 
                        device.ESC_READ_VOLTAGE1, device.JIG_READ_VOLTAGE1, device.ESC_DEV_VO1_GAIN,
                        self.edit_vo1_gain, self.btn_vo1_gain, "VOLTAGE1GAIN"))
                self.timer.start(READ_TIME)

            elif ptype == 'VOLTAGE2OFFSET':
                # SAVE OFFSET VALUE 
                self.vo2_offset = self.curr_gain

                # setting timer
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(lambda:self.start_gain_calibration(self.esc_client, 
                        device.ESC_READ_VOLTAGE2, device.JIG_READ_VOLTAGE2, device.ESC_DEV_VO2_GAIN,
                        self.edit_vo2_gain, self.btn_vo2_gain, "VOLTAGE2GAIN"))
                self.timer.start(READ_TIME)

            elif ptype == 'CURRENT1OFFSET':
                # SAVE OFFSET VALUE 
                self.io1_offset = self.curr_gain

                # ESC의 IO1 FORWARD값이 JIG의 IO1 값과 같아지도록 ESC에 IO1 GAIN을 조정한다.
                # ESC의 IO1 FORWARD값이 JIG의 IO1 값과 편차 0.001mA 이내가 되면 IO1 GAIN은 완료.
                # CURRENT1GAIN
                # client            : self.esc_client
                # esc_vol_address    : device.ESC_READ_VOLTAGE2
                # jig_vol_address    : device.JIG_READ_VOLTAGE2
                # gain_address      : device.ESC_DEV_VO2_GAIN
                # dis_value         : 해당 변화 값을 보여주는 Edit
                # edit_rev          : 해당 변화 값을 보여주는 Edit
                # btn               : 완료 후 색 변환
                # ptype             : 해당 type
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(lambda:self.start_gain_calibration(self.esc_client, 
                        device.ESC_READ_CURRENT1, device.JIG_READ_CURRENT1, device.ESC_DEV_IO1_GAIN,
                        self.edit_io1_gain, self.btn_io1_gain, "CURRENT1GAIN"))
                self.timer.start(READ_TIME)

            elif ptype == 'CURRENT2OFFSET':
                # SAVE OFFSET VALUE 
                self.io2_offset = self.curr_gain

                # CURRENT2GAIN
                # setting timer
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(lambda:self.start_gain_calibration(self.esc_client, 
                        device.ESC_READ_CURRENT2, device.JIG_READ_CURRENT2, device.ESC_DEV_IO2_GAIN,
                        self.edit_io2_gain, self.btn_io2_gain, "CURRENT2GAIN"))
                self.timer.start(READ_TIME)

            elif ptype == 'IRLEAK1OFFSET':
                # SAVE OFFSET VALUE 
                self.ir1_offset = self.curr_gain

                # ESC의 IR1 FORWARD값이 JIG의 IR1 값과 같아지도록 ESC에 IR1 GAIN을 조정한다.
                # ESC의 IR1 FORWARD값이 JIG의 IR1 값과 편차 0.01uA 이내가 되면 IR1 GAIN은 완료.
                # CAll Function for Voltage 2 GAIN
                # client            : self.esc_client
                # esc_vol_address    : device.ESC_READ_VOLTAGE2
                # jig_vol_address    : device.JIG_READ_VOLTAGE2
                # gain_address      : device.ESC_DEV_VO2_GAIN
                # dis_value         : 해당 변화 값을 보여주는 Edit
                # btn               : 완료 후 색 변환
                # ptype             : 해당 type
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(lambda:self.start_gain_calibration(self.esc_client, 
                        device.ESC_READ_IO_LEAK1, device.JIG_READ_IO_LEAK1, device.ESC_DEV_LEAK1_GAIN,
                        self.edit_ir1_gain, self.btn_ir1_gain, "IRLEAK1GAIN"))
                self.timer.start(READ_TIME)

            elif ptype == 'IRLEAK2OFFSET':
                # SAVE OFFSET VALUE 
                self.ir2_offset = self.curr_gain

                # setting timer
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(lambda:self.start_gain_calibration(self.esc_client, 
                        device.ESC_READ_IO_LEAK2, device.JIG_READ_IO_LEAK2, device.ESC_DEV_LEAK2_GAIN,
                        self.edit_ir2_gain, self.btn_ir2_gain, "IRLEAK2GAIN"))
                self.timer.start(READ_TIME)

        # OFFSET 조정 필요
        else:
            self.esc_val = self.fwd_val
            self.jig_val = self.rev_val            
            self.calculate_adjust_offsetgain(ptype, dis_value)

            # Offset 값 정리 
            self.cal_index += 1
            
            device.write_registers(self.esc_client, device.ESC_WRITE_TOGGLE, device.FORWARD)
            time.sleep(TIME_INTERVAL)
            # Offset 값 정리 

            div_value = c_uint16(self.curr_gain).value
            device.write_registers(self.esc_client, offset_address, div_value)
            # setting timer
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(lambda:self.step1_read_forward(client, value_read_address, 
                            offset_address, dis_value, btn, ptype))
            self.timer.start(READ_TIME)
    

    #############################################################
    #########  2. GAIN START ####################################
    #############################################################
    # ESC 값
    # JIG 값 
    # client            : ESC or JIG
    # esc_vol_address    : ESC 읽어드릴 값의 주소
    # jig_vol_address    : JIG 읽어드릴 값의 주소
    # gain_address      : ESC의 값 조정 주소 
    # dis_value          : 해당 변화 값을 보여주는 Edit
    # edit_rev          : 해당 변화 값을 보여주는 Edit
    # btn               : 완료 후 색 변환
    # ptype             : 해당 type
    def start_gain_calibration(self, client, esc_vol_address, jig_vol_address, gain_address, dis_value, btn, ptype):
        # Timer 정지
        self.timer.stop()
        self.timer = None 

        print("\n#### ", ptype, " START ####\n")
        # 기존 GAIN Value 를 읽어 온다.
        self.curr_gain = device.read_value(self.esc_client, gain_address)
        dis_value.setText(str(self.curr_gain))

        self.cal_index = 0
        self.old_diff = 0
        self.before_dir = PLUS
        self.repeatN = 0

        device.write_registers(self.esc_client, device.ESC_WRITE_TOGGLE, device.FORWARD)
    
        # setting timer
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(lambda:self.gain_read_esc_value(client, esc_vol_address, 
                                    jig_vol_address, gain_address, dis_value, btn, ptype))
        self.timer.start(READ_TIME)

    ## GAIN CALIBRATION :: ESC VALUE
    def gain_read_esc_value(self, client, esc_vol_address, jig_vol_address, gain_address, dis_value, btn, ptype):
        # Timer 정지
        self.timer.stop()
        self.timer = None 

        # 1초 후에 JIG의 VO1 값을 읽어와 해당 값을 저장함 : JIG_REV_VO1 = 2450
        self.esc_val = device.read_value(client, esc_vol_address)

        # setting timer
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(lambda:self.gain_read_jig_value(self.jig_client, esc_vol_address, 
                                    jig_vol_address, gain_address, dis_value, btn, ptype))
        self.timer.start(READ_TIME)

    ## GAIN CALIBRATION :: JIG VALUE
    ## @@@@ calculate_adjust_offsetgain
    def gain_read_jig_value(self, client, esc_vol_address, jig_vol_address, gain_address, dis_value, btn, ptype):
        # Timer 정지
        self.timer.stop()
        self.timer = None 

        # 1초 후에 JIG의 VO1 값을 읽어와 해당 값을 저장함 : JIG_REV_VO1 = 2450
        self.jig_val = device.read_value(client, jig_vol_address)
        
        # JIG_FWD_VO1 과 JIG_REV_VO1 값의 차이를 구함 : DIFF_VO1 = JIG_REV_VO1 - JIG_FWD_VO1
        diff_val = abs(self.esc_val) - abs(self.jig_val)
        print(ptype, " ::: ESC VAL :     ", self.esc_val, "JIG VAL :      ",  
                self.jig_val, " ******************      ", diff_val)

        # GAIN 조정 완료 
        if MINUS_OFFSET <= diff_val and diff_val <= PLUS_OFFSET:
            print("*"*50)
            print("***** :: :: GAIN finish value : ", self.curr_gain)
            print("*"*50)
            btn.setStyleSheet(self.greenback)  
            dis_value.setStyleSheet(self.greenback)
            self.get_esc_settings_value()
            self.get_esc_params()

            if ptype == 'VOLTAGE1GAIN':
                # SAVE OFFSET VALUE 
                self.vo1_gain = self.curr_gain

                # VOLTAGE2OFFSET
                # client            : self.jig_client
                # value_read_address   : device.JIG_READ_VOLTAGE2
                # offset_address    : device.ESC_DEV_VO2_OFFSET
                # dis_value          : self.edit_vo2_gain
                # btn               : self.btn_vo2_offset
                # ptype             : VOLTAGE2OFFSET
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(lambda:self.start_offset_calibration(self.jig_client, 
                        device.JIG_READ_VOLTAGE2, device.ESC_DEV_VO2_OFFSET,
                        self.edit_vo2_offset, self.btn_vo2_offset, "VOLTAGE2OFFSET"))
                self.timer.start(READ_TIME)

            elif ptype == 'VOLTAGE2GAIN':
                # SAVE OFFSET VALUE 
                self.vo2_gain = self.curr_gain

                self.esc_device_stop()
                time.sleep(TIME_INTERVAL)
                ## OFFOFF
                device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON10, device.OFF)
                time.sleep(TIME_INTERVAL)
                device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON11, device.OFF)
                time.sleep(TIME_INTERVAL)
                device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON14, device.OFF)
                time.sleep(TIME_INTERVAL)
                

                #############################################################
                ######### CURRNET   #########################################
                #############################################################
                self.setting_voltage(CURRENT_CALIBRATION_VOLTAGE, -CURRENT_CALIBRATION_VOLTAGE)

                ## ONON
                # JIG에 CAP_ON12를 ON 설정하고, RUN 지령
                device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON12, device.ON)
                time.sleep(TIME_INTERVAL)

                # CAll Function for Current 1
                self.esc_device_run()

                # PC에서 ESC에 FORWARD와 REVERSE 지령을 반복하면서 ESC의 IO1 값을 확인한다.
                # ESC의 IO1 FORWARD와 REVERSE 값이 같아지도록 ESC의 IO1 OFFSET을 조정한다.

                # CURRENT 1 OFFSET
                # client            : self.esc_client
                # value_read_address   : device.JIG_READ_CURRENT1
                # offset_address    : device.ESC_DEV_IO1_OFFSET
                # dis_value          : self.edit_vo2_gain
                # btn               : self.btn_vo2_offset
                # ptype             : VOLTAGE2OFFSET
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(lambda:self.start_offset_calibration(self.esc_client, 
                        device.ESC_READ_CURRENT1, device.ESC_DEV_IO1_OFFSET,
                        self.edit_io1_offset, self.btn_io1_offset, "CURRENT1OFFSET"))
                self.timer.start(CAPON_TIME)

            elif ptype == 'CURRENT1GAIN':
                # SAVE OFFSET VALUE 
                self.io1_gain = self.curr_gain

                # CURRENT 2 OFFSET
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(lambda:self.start_offset_calibration(self.esc_client, 
                        device.ESC_READ_CURRENT2, device.ESC_DEV_IO2_OFFSET,
                        self.edit_io2_offset, self.btn_io2_offset, "CURRENT2OFFSET"))
                self.timer.start(READ_TIME)


            elif ptype == 'CURRENT2GAIN':
                # SAVE OFFSET VALUE 
                self.io2_gain = self.curr_gain

                self.esc_device_stop()
                time.sleep(TIME_INTERVAL)
                device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON12, device.OFF)
                time.sleep(TIME_INTERVAL)

                #############################################################
                ######### IO LEAK   #########################################
                #############################################################

                # PC에서 ESC에 VO1 = 600V, VO2 = -600V로 설정하고, RUN 지령을 한다.
                self.setting_voltage(IRLEAK_CALIBRATION_VOLTAGE, -IRLEAK_CALIBRATION_VOLTAGE)
                

                ## ONON
                # PC에서 JIG에 CAP_ON13를 ON 설정하고, RUN 지령을 한다.
                device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON13, device.ON)
                time.sleep(TIME_INTERVAL)
                # CAll Function for Current 1
                self.esc_device_run()

                # PC에서 ESC에 FORWARD와 REVERSE 지령을 반복하면서 ESC의 IR1 값을 확인한다.
                # ESC의 IR1 FORWARD와 REVERSE 값이 같아지도록 ESC에 IR1 OFFSET을 조정한다.
                # IRLEAK 1 OFFSET
                # client            : self.esc_client
                # value_read_address   : device.ESC_READ_IO_LEAK1
                # offset_address    : device.ESC_DEV_IO1_OFFSET
                # dis_value          : self.edit_ir1_gain
                # edit_rev          : self.edit_irleak_rev_jig1
                # btn               : self.btn_ir1_offset
                # ptype             : IRLEAK1OFFSET
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(lambda:self.start_offset_calibration(self.esc_client, 
                        device.ESC_READ_IO_LEAK1, device.ESC_DEV_LEAK1_OFFSET,
                        self.edit_ir1_offset, self.btn_ir1_offset, "IRLEAK1OFFSET"))
                self.timer.start(CAPON_TIME)

            elif ptype == 'IRLEAK1GAIN':
                # SAVE OFFSET VALUE 
                self.ir1_gain = self.curr_gain

                ## 
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(lambda:self.start_offset_calibration(self.esc_client, 
                        device.ESC_READ_IO_LEAK2, device.ESC_DEV_LEAK2_OFFSET,
                        self.edit_ir2_offset, self.btn_ir2_offset, "IRLEAK2OFFSET"))
                self.timer.start(READ_TIME)

            elif ptype == 'IRLEAK2GAIN':
                # SAVE OFFSET VALUE 
                self.ir2_gain = self.curr_gain


                self.esc_device_stop()
                time.sleep(TIME_INTERVAL)

                device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON13, device.OFF)

                ##############################
                ##############################
                ######### ORIGIN #############
                # setting timer
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(self.start_vbias_offset_new)
                self.timer.start(READ_TIME)


        # GAIN 조정 필요
        else:
            self.calculate_adjust_offsetgain(ptype, dis_value)

            # Gain Calibration
            self.cal_index += 1

            div_value = c_uint16(self.curr_gain).value
            device.write_registers(self.esc_client, gain_address, div_value)
            # setting timer
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(lambda:self.gain_read_esc_value(self.esc_client, 
                            esc_vol_address, jig_vol_address, gain_address, dis_value, btn, ptype))
            self.timer.start(READ_TIME)


    #############################################################
    #########  3. IRLEAK START ##################################
    #############################################################
    def start_irleak(self):
        device.check_password(self.esc_client)

        if not self.displaytimer:
            self.start_timer()
        
        # PC에서 ESC에 VO1 = 600V, VO2 = -600V로 설정하고, RUN 지령을 한다.
        self.setting_voltage(420, -420)

        ## ONON
        # PC에서 JIG에 CAP_ON13를 ON 설정하고, RUN 지령을 한다.
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON13, device.ON)
        time.sleep(TIME_INTERVAL)
        # CAll Function for Current 1
        self.esc_device_run()

        # PC에서 ESC에 FORWARD와 REVERSE 지령을 반복하면서 ESC의 IR1 값을 확인한다.
        # ESC의 IR1 FORWARD와 REVERSE 값이 같아지도록 ESC에 IR1 OFFSET을 조정한다.
        # IRLEAK 1 OFFSET
        # client            : self.esc_client
        # value_read_address   : device.ESC_READ_IO_LEAK1
        # offset_address    : device.ESC_DEV_IO1_OFFSET
        # dis_value          : self.edit_ir1_gain
        # edit_rev          : self.edit_irleak_rev_jig1
        # btn               : self.btn_ir1_offset
        # ptype             : IRLEAK1OFFSET
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(lambda:self.start_offset_calibration(self.esc_client, 
                device.ESC_READ_IO_LEAK1, device.ESC_DEV_LEAK1_OFFSET,
                self.edit_ir1_offset, self.btn_ir1_offset, "IRLEAK1OFFSET"))
        self.timer.start(CAPON_TIME)

    
    #############################################################
    #########  4. BIAS OFFSET NEW **  CAP14, 15 캘리브레이션 #######
    #############################################################
    def start_vbias_offset_new(self):
        device.check_password(self.esc_client)

        if not self.displaytimer:
            self.start_timer()
        
        # Timer 정지
        self.timer.stop()
        self.timer = None

        self.cal_index = 0
        self.old_diff = 0
        self.fwd_val = None
        self.rev_val = None
        print("\n@@@@ ", "VBIA OFFSET", " START @@@@\n")

        ## ONON
        # PC에서 JIG에 JIG_WRITE_CAP_ON15 ON 설정하고, RUN 지령을 한다.
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON15, device.ON)
        time.sleep(TIME_INTERVAL)
        
        # PC에서 JIG에 JIG_WRITE_CAP_ON14 ON 설정하고, RUN 지령을 한다.
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON14, device.ON)
        time.sleep(TIME_INTERVAL)

        # - PC에서 ESC에 VO1 = 1250V, VO2 = -1250V로 설정하고, RUN 지령을 한다.
        self.setting_voltage(1250, -1250)

        # JIG에 VBIA = 125V롤 설정하고 Run
        self.jig_device_run()
        # CAll Function for Current 1
        self.esc_device_run()

        self.curr_gain = device.read_value(self.esc_client, device.ESC_DEV_VBIAS_OFFSET)
        self.edit_vbia_offset.setText(str(self.curr_gain))

        # setting timer
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.get_forward_vbia_offset)
        self.timer.start(CAPON_TIME*2)


    def get_forward_vbia_offset(self):

        # Timer 정지
        self.timer.stop()
        self.timer = None 

        # ESC VBIA 값을 읽는다.
        self.fwd_val = device.read_value(self.esc_client, device.ESC_READ_VBIA)

        # ESC JIG STOP
        self.esc_device_stop()
        self.jig_device_stop()

        ## SET REVERSE DIRECTION
        # PC에서 JIG에 JIG_WRITE_CAP_ON15 OFF 설정하고, RUN 지령을 한다.
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON15, device.OFF)
        time.sleep(TIME_INTERVAL)

        # PC에서 JIG에 JIG_WRITE_CAP_ON16 ON 설정하고, RUN 지령을 한다.
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON16, device.ON)
        time.sleep(TIME_INTERVAL)

        # JIG에 VBIA = 125V롤 설정하고 Run
        self.jig_device_run()
        # CAll Function for Current 1
        self.esc_device_run()

        # setting timer
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.get_reverse_vbia_offset)
        self.timer.start(CAPON_TIME*2)


    def get_reverse_vbia_offset(self):

        # Timer 정지
        self.timer.stop()
        self.timer = None 

        # ESC VBIA 값을 읽는다.
        self.rev_val = device.read_value(self.esc_client, device.ESC_READ_VBIA)

        # ESC JIG STOP
        self.esc_device_stop()
        self.jig_device_stop()

        vbia_val = abs(self.fwd_val) - abs(self.rev_val)
        
        ############################################################
        ########  4. VBIA OFFSET 조정 완료  ##########################
        ############################################################
        if vbia_val == 0:
            # SAVE OFFSET VALUE 
            self.vbia_offset = self.curr_gain

            print("***** :: VBIAS OFFSET ***** :: finish value : ", self.fwd_val, self.rev_val)
            self.btn_vbia_offset.setStyleSheet(self.greenback)
            self.edit_vbia_offset.setStyleSheet(self.greenback)

            #########  BIAS GAIN. 캘리브레이션 #############################
            print("\n@@@@ ", "VBIA GAIN", " START @@@@\n")

            # PC에서 JIG에 JIG_WRITE_CAP_ON16 OFF 설정하고
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON16, device.OFF)
            time.sleep(TIME_INTERVAL)

            # PC에서 JIG에 JIG_WRITE_CAP_ON15 ON 설정하고
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON15, device.ON)
            time.sleep(TIME_INTERVAL)

            # JIG에 VBIA = 125V롤 설정하고 Run
            self.jig_device_run()
            # CAll Function for Current 1
            self.esc_device_run()

            # setting timer
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(self.start_vbias_gain_new)
            self.timer.start(CAPON_TIME)

        # VBIA OFFSET 조정 
        else:
            self.esc_val = self.fwd_val
            self.jig_val = self.rev_val  

            self.calculate_vbias("VBIAOFFSET", vbia_val, self.edit_vbia_offset)
            # Offset 값 정리 
            self.cal_index += 1

            div_value = c_uint16(self.curr_gain).value
            device.write_registers(self.esc_client, device.ESC_DEV_VBIAS_OFFSET, div_value)

            # PC에서 JIG에 JIG_WRITE_CAP_ON16 OFF 설정하고
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON16, device.OFF)
            time.sleep(TIME_INTERVAL)

            # PC에서 JIG에 JIG_WRITE_CAP_ON15 ON 설정하고, RUN 지령을 한다.
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON15, device.ON)
            time.sleep(TIME_INTERVAL)

            # - PC에서 ESC에 VO1 = 1250V, VO2 = -1250V로 설정하고, RUN 지령을 한다.
            self.setting_voltage(1250, -1250)
            
            # JIG에 VBIA = 125V롤 설정하고 Run
            self.jig_device_run()
            # CAll Function for Current 1
            self.esc_device_run()


            # setting timer
            self.cal_index = 0
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(self.get_forward_vbia_offset)
            self.timer.start(CAPON_TIME*2)


    #############################################################
    #########  4. BIAS GAIN  NEW ***  CAP14, 15 캘리브레이션 #######
    #############################################################
    def start_vbias_gain_new(self):
        # Timer 정지
        self.timer.stop()
        self.timer = None

        # 1초 후에 ESC 똔 JIG의 VO1 값을 읽어와 해당 값을 저장함 
        self.esc_val = device.read_value(self.esc_client, device.ESC_READ_VBIA)
        self.jig_val = device.read_value(self.jig_client, device.JIG_READ_VBIA)

        diff_val = abs(self.esc_val) - abs(self.jig_val)
        print("VBIA GAIN", " ::: ESC V :     ", self.esc_val, "JIG V :      ",  
                self.jig_val, " ***********************>      ", diff_val)

        ############################################################
        ########  4. VBIA GAIN 조정 완료  ############################
        ############################################################
        if abs(diff_val) <= 1:
            # SAVE OFFSET VALUE 
            self.vbia_gain = self.curr_gain

            ## SETTING
            self.btn_vbia_gain.setStyleSheet(self.greenback)
            self.edit_vbia_gain.setStyleSheet(self.greenback)

            self.esc_device_stop()
            time.sleep(TIME_INTERVAL)

            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON14, device.OFF)
            time.sleep(TIME_INTERVAL)
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON15, device.OFF)
            time.sleep(TIME_INTERVAL)
            
            ########  CAP OFFSET 캘리브레이션  #############################
            print("\n@@@@ ", "CAP CALIBARATION", " START @@@@\n")

            self.jig_device_stop()
            
            # setting timer
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(lambda:self.start_cap_value())
            self.timer.start(CAPON_TIME)

        # VBIA GAIN 조정 필요 
        else:
            # PC에서 ESC에 VBIAS 값이 0V 사이가 되도록 VBIAS OFFSET을 조정한다
            self.calculate_adjust_offsetgain("VBIAGAIN", self.edit_vbia_gain)

            # Offset 값 정리 
            self.cal_index += 1

            div_value = c_uint16(self.curr_gain).value
            device.write_registers(self.esc_client, device.ESC_DEV_VBIAS_GAIN, div_value)

            # setting timer
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(self.start_vbias_gain_new)
            self.timer.start(READ_TIME)

    #############################################################
    #########  4. BIAS OFFSET  OLD 캘리브레이션 ####################
    #############################################################
    def start_vbias_old(self):
        device.check_password(self.esc_client)
        
        # Timer 정지
        self.timer.stop()
        self.timer = None 
        # PC에서 JIG에 CAP_ON13을 OFF 설정하고, RUN 지령을 한다.
        
        ## ONON
        # PC에서 JIG에 CAP_ON13를 ON 설정하고, RUN 지령을 한다.
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON14, device.ON)
        time.sleep(TIME_INTERVAL)

        # - PC에서 ESC에 VO1 = 1250V, VO2 = -1250V로 설정하고, RUN 지령을 한다.
        self.setting_voltage(1250, -1250)

        # CAll Function for Current 1
        self.esc_device_run()
        time.sleep(TIME_INTERVAL)

        self.curr_gain = device.read_value(self.esc_client, device.ESC_DEV_VBIAS_OFFSET)

        self.cal_index = 0
        self.old_diff = 0
        print("\n@@@@ ", "VBIA OFFSET", " START @@@@\n")

        # setting timer
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.start_vbias_offset)
        self.timer.start(CAPON_TIME)


    ## USING calculate_gain
    ## ESC_READ_VBIA 값이 0 이 되도록 ESC_DEV_VBIAS_OFFSET 값을 조정한다.
    def start_vbias_offset(self):
        # Timer 정지
        self.timer.stop()
        self.timer = None 

        # 1초 후에 ESC 똔 JIG의 VO1 값을 읽어와 해당 값을 저장함 
        vbia_val = device.read_value(self.esc_client, device.ESC_READ_VBIA)
        # print(" >> Start VBIAS OFFSET ***** :: finish value : ", vbia_val)

        self.edit_vbia_offset.setText(str(self.curr_gain))
        
        ############################################################
        ########  4. VBIA OFFSET 조정 완료  ##########################
        ############################################################
        if vbia_val == 0:
            # SAVE OFFSET VALUE 
            self.vbia_offset = self.curr_gain

            print("***** :: VBIAS OFFSET ***** :: finish value : ", vbia_val)
            self.btn_vbia_offset.setStyleSheet(self.greenback)
            self.edit_vbia_offset.setStyleSheet(self.greenback)

            #########  BIAS GAIN. 캘리브레이션 #############################
            print("\n@@@@ ", "VBIA GAIN", " START @@@@\n")

            self.jig_device_run()
            vbia_val = device.read_value(self.esc_client, device.ESC_DEV_VBIAS_GAIN)
            self.curr_gain = vbia_val
            self.edit_vbia_gain.setText(str(vbia_val))
            self.cal_index = 0
            
            # setting timer
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(self.start_vbias_gain)
            self.timer.start(CAPON_TIME)

        # VBIA OFFSET 조정 
        else:
            self.calculate_gain("VBIAOFFSET", vbia_val, self.edit_vbia_offset)
            # Offset 값 정리 
            self.cal_index += 1

            div_value = c_uint16(self.curr_gain).value
            device.write_registers(self.esc_client, device.ESC_DEV_VBIAS_OFFSET, div_value)
            # print(f"device.write_registers(self.esc_client, device.ESC_DEV_VBIAS_OFFSET, {div_value})")

            # setting timer
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(self.start_vbias_offset)
            self.timer.start(READ_TIME)


    #########  BIAS GAIN 캘리브레이션 #############################
    ## @@@@ calculate_adjust_offsetgain
    ## ESC_READ_VBIA, JIG_READ_VBIA 같아지도록 
    def start_vbias_gain(self):
        # Timer 정지
        self.timer.stop()
        self.timer = None 

        # 1초 후에 ESC 똔 JIG의 VO1 값을 읽어와 해당 값을 저장함 
        self.esc_val = device.read_value(self.esc_client, device.ESC_READ_VBIA)
        self.jig_val = device.read_value(self.jig_client, device.JIG_READ_VBIA)

        diff_val = abs(self.esc_val) - abs(self.jig_val)
        print("VBIA GAIN", " ::: ESC V :     ", self.esc_val, "JIG V :      ",  
                self.jig_val, " ***********************>      ", diff_val)

        ############################################################
        ########  4. VBIA GAIN 조정 완료  ############################
        ############################################################
        if diff_val == 0:
            # SAVE OFFSET VALUE 
            self.vbia_gain = self.curr_gain

            ##
            self.btn_vbia_gain.setStyleSheet(self.greenback)
            self.edit_vbia_gain.setStyleSheet(self.greenback)

            self.esc_device_stop()
            time.sleep(TIME_INTERVAL)

            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON14, device.OFF)
            time.sleep(TIME_INTERVAL)
            
            ########  CAP OFFSET 캘리브레이션  #############################
            print("\n@@@@ ", "CAP OFFSET CALIBARATION", " START @@@@\n")

            self.jig_device_stop()
            
            # setting timer
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(lambda:self.start_cap_value())
            self.timer.start(CAPON_TIME)

            # print("#"*50)
            # print("################ TEMP END ###############")
            # print("#"*50)

        # VBIA GAIN 조정 필요 
        else:
            # PC에서 ESC에 VBIAS 값이 0V 사이가 되도록 VBIAS OFFSET을 조정한다
            self.calculate_adjust_offsetgain("VBIAGAIN", self.edit_vbia_gain)

            # Offset 값 정리 
            self.cal_index += 1

            div_value = c_uint16(self.curr_gain).value
            device.write_registers(self.esc_client, device.ESC_DEV_VBIAS_GAIN, div_value)

            # setting timer
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(self.start_vbias_gain)
            self.timer.start(READ_TIME)


    #############################################################
    #########  5. CAP 캘리브레이션 #################################
    #############################################################
    def start_cap_value(self):
        device.check_password(self.esc_client)

        if not self.displaytimer:
            self.start_timer()
        
        ## ONONON
        # PC에서 JIG에 CAP_ON0  ON 설정하고, RUN 지령을 한다.
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON0, device.ON)
        time.sleep(TIME_INTERVAL)

        ## COE, POW 값을 1로 설정한다.
        device.write_registers(self.esc_client, device.ESC_DEV_ZCS1_GAIN, 1000)
        device.write_registers(self.esc_client, device.ESC_DEV_ZCS2_GAIN, 1000)
        time.sleep(TIME_INTERVAL)

        # - PC에서 ESC에 VO1 = 1250V, VO2 = -1250V로 설정하고, RUN 지령을 한다.
        self.setting_voltage(1250, -1250)

        self.esc_device_run()
        time.sleep(TIME_INTERVAL)
        
        self.curr_gain = device.read_value(self.esc_client, device.ESC_DEV_CAP_OFFSET)
        self.edit_cap_offset.setText(str(self.curr_gain))
        self.old_diff = 0
        self.cal_index = 0
        print(f"CAP OFFSET INITIAL VALUE :: {self.curr_gain}")
        
        # setting timer
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.start_cap_offset)
        self.timer.start(CAPON_TIME)


    ######### OLD VERSION CAP OFFSET 캘리브레이션 ##################
    ## ** calculate_gain
    ## START END START SETTING, READ VALUE 
    def start_cap_offset(self):
        # Timer 정지
        self.timer.stop()
        self.timer = None 

        # 1초 후에 ESC 똔 JIG의 VO1 값을 읽어와 해당 값을 저장함 
        vbia_val = device.read_value(self.esc_client, device.ESC_READ_CAP)
        # print(f"CAP OFFSET :: Cap Value = {vbia_val}")
        
        # CAP OFFSET 조정 완료 
        MIN_CAP_VAL = int(self.edit_base_cap0.text()) - 1
        MAX_CAP_VAL = int(self.edit_base_cap0.text()) + 1
        
        if vbia_val >= MIN_CAP_VAL and vbia_val <= MAX_CAP_VAL:
            # SAVE OFFSET VALUE 
            self.cap_offset = self.curr_gain

            ##
            self.btn_cap_offset.setStyleSheet(self.greenback)
            self.edit_cap_offset.setStyleSheet(self.greenback)
            print("***** :: CAP OFFSET finish value : ", vbia_val)
            #############################################################
            #########  ZCS1 GAIN. 캘리브레이션 #############################
            #############################################################
            self.esc_device_stop()
            # JIG에 CAP_ON0를 ON 설정하고, RUN 지령
            
            # setting timer
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(self.start_new_pow_coeff)
            self.timer.start(CAPON_TIME)

        # CAP OFFSET 조정 
        else:
            # CAP OFFSET 조정 완료 160 ~ 162 사이로 조정
            diff_val = 160 - vbia_val
            self.calculate_gain("CAPOFFSET", diff_val, self.edit_cap_offset)
                
            # Offset 값 정리 
            self.cal_index += 1

            div_value = c_uint16(self.curr_gain).value
            device.write_registers(self.esc_client, device.ESC_DEV_CAP_OFFSET, div_value)

            # setting timer
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(lambda:self.start_cap_offset())
            self.timer.start(READ_TIME)


    #############################################################
    #########  calculate_adjust_offsetgain  #####################
    def calculate_adjust_offsetgain(self, ptype, display_edit):
        esc_val = abs(self.esc_val)
        jig_val = abs(self.jig_val)

        if esc_val > jig_val:
            direction = UP
        else:
            direction = DOWN

        abs_diff_val = abs(esc_val - jig_val)
        div_value = abs_diff_val
        self.old_gain = self.curr_gain

        if self.cal_index == 0:
            self.repeatP = 0 
            self.repeatN = 0
            self.old_direction = direction

            if ptype.startswith("VOLTAGE") and ptype.endswith("OFFSET"):
                if direction == UP:
                    self.gain_dir = PLUS
                    self.curr_gain += div_value
                else:
                    self.gain_dir = MINUS
                    self.curr_gain -= div_value

            elif ptype.startswith("VOLTAGE") and ptype.endswith("GAIN"):
                if direction == UP:
                    self.gain_dir = MINUS
                    self.curr_gain -= div_value
                else:
                    self.gain_dir = PLUS
                    self.curr_gain += div_value

            elif ptype.startswith("CURRENT") and ptype.endswith("OFFSET"):
                div_value = int(div_value/2)
                if direction == UP:
                    self.gain_dir = MINUS
                    self.curr_gain -= div_value
                else:
                    self.gain_dir = PLUS
                    self.curr_gain += div_value

            elif ptype.startswith("CURRENT") and ptype.endswith("GAIN"):
                div_value = int(div_value/2)
                if direction == UP:
                    self.gain_dir = MINUS
                    self.curr_gain -= div_value
                else:
                    self.gain_dir = PLUS
                    self.curr_gain += div_value

            elif ptype.startswith("IRLEAK") and ptype.endswith("OFFSET"):
                div_value = int(div_value/2)
                if direction == UP:
                    self.gain_dir = MINUS
                    self.curr_gain -= div_value
                else:
                    self.gain_dir = PLUS
                    self.curr_gain += div_value

            elif ptype.startswith("IRLEAK") and ptype.endswith("GAIN"):
                div_value = int(div_value/2)
                if direction == UP:
                    self.gain_dir = PLUS
                    self.curr_gain += div_value
                else:
                    self.gain_dir = MINUS
                    self.curr_gain -= div_value

            elif ptype.startswith("VBIA") and ptype.endswith("GAIN"):
                if direction == UP:
                    self.gain_dir = MINUS
                    self.curr_gain -= div_value
                else:
                    self.gain_dir = PLUS
                    self.curr_gain += div_value

            else:
                self.curr_gain += div_value

        else:
            # UP DOWN 방향이 바뀌지 않았다면
            if self.old_direction == direction:
                # PLUS 방향으로 작아 지고 있음. GOOD
                if abs_diff_val < self.old_diff and self.gain_dir == PLUS:
                    if ptype.startswith("IRLEAK") and ptype.endswith("OFFSET") and abs_diff_val > 10:
                        self.curr_gain += div_value
                    elif ptype.startswith("IRLEAK") and ptype.endswith("OFFSET") and abs_diff_val > 5:
                        self.curr_gain += int(div_value/2)
                    elif ptype.startswith("CURRENT") and abs_diff_val <= 3:
                        self.curr_gain += 1
                    else:
                        self.curr_gain += div_value
                    
                    if self.repeatP:
                        self.repeatP = 0

                # MINUS 방향으로 작아 지고 있음. GOOD
                elif abs_diff_val < self.old_diff and self.gain_dir == MINUS:
                    if ptype.startswith("IRLEAK") and ptype.endswith("OFFSET") and abs_diff_val > 10:
                        self.curr_gain -= div_value 
                    elif ptype.startswith("IRLEAK") and ptype.endswith("OFFSET") and abs_diff_val > 5:
                        self.curr_gain -= int(div_value/2)
                    elif ptype.startswith("CURRENT") and abs_diff_val <= 3:
                        self.curr_gain -= 1
                    else:
                        self.curr_gain -= div_value
                    
                    if self.repeatN:
                        self.repeatN = 0

                # PLUS 로 가는데, 값이 커지고 있음. 
                elif abs_diff_val > self.old_diff and self.gain_dir == PLUS:
                    # 이미 2회 이상이고, 반복 회수가 REPEATN (2) 회 이하이면
                    if self.cal_index > REPEATN and self.repeatP < REPEATN:
                        self.curr_gain += int(div_value/2) or 1
                        self.repeatP += 1
                    # 방향을 바꿔어야 함.
                    else:
                        self.curr_gain -= int(div_value/2) or 1
                        self.gain_dir = MINUS
                        self.repeatP = 0
                        print("DIR CHANGE MINUS ", abs_diff_val, self.old_diff)
                
                # MINUS 로 가는데, 값이 커지고 있음.
                elif abs_diff_val > self.old_diff and self.gain_dir == MINUS:
                    if self.cal_index > REPEATN and self.repeatN < REPEATN:
                        self.curr_gain -= int(div_value/2) or 1
                        self.repeatN += 1
                    else:
                        self.curr_gain += int(div_value/2) or 1
                        self.gain_dir = PLUS
                        self.repeatN = 0
                        print("DIR CHANGE PLUS ", abs_diff_val, self.old_diff)

                # PLUS 방향으로 가다가 같은 값이 나오면.
                elif abs_diff_val == self.old_diff and self.gain_dir == PLUS:
                    # 10회 이상 반복 했다면 방향을 바꿔 줌
                    if self.repeatP >= REPEATP:
                        self.gain_dir = MINUS 
                        self.curr_gain = -20
                        self.repeatP = 0
                    # 지속적으로 작은 값을 제공함
                    else:
                        if abs_diff_val > 3 and ptype.startswith("VOLTAGE") or ptype.startswith("IRLEAK") \
                                or ptype.startswith("VBIA") or ptype.startswith("CAP") or ptype.startswith("ZCS"):
                            self.curr_gain += div_value
                        else:
                            self.curr_gain += 1
                        self.repeatP += 1

                # MINUS 방향으로 가다가 같은 값이 나오면.
                elif abs_diff_val == self.old_diff and self.gain_dir == MINUS:
                    if self.repeatN >= REPEATP:
                        self.gain_dir = PLUS 
                        self.curr_gain += 20
                        self.repeatN = 0
                    else:
                        if abs_diff_val > 3 and ptype.startswith("VOLTAGE") or ptype.startswith("IRLEAK") \
                                or ptype.startswith("VBIA") or ptype.startswith("CAP") or ptype.startswith("ZCS"):
                            self.curr_gain -= div_value
                        else:
                            self.curr_gain -= 1
                        self.repeatN += 1
                        
                else:
                    self.curr_gain += 5
                    print("             &&&& I don't know", abs_diff_val, self.old_diff)

            # UP DOWN 발생
            else:
                self.old_direction = direction
                if self.gain_dir == PLUS:
                    self.gain_dir = MINUS 
                    self.curr_gain -= int(div_value / 3)
                else:
                    self.gain_dir = PLUS 
                    self.curr_gain += int(div_value / 3)

        print(f" @@ {ptype} :: {self.cal_index}th, OLD DIFF = {self.old_diff}, CUR DIFF = {abs_diff_val},  NEW VALUE : {self.curr_gain} :: {div_value} \n")
        self.old_diff = abs_diff_val

        #### Display
        display_edit.setText(str(self.curr_gain))


    #########  calculate_gain ###################################
    def calculate_gain(self, ptype, diff_val, display_edit):
        ##
        origin_diff = diff_val
        abs_diff_val = abs(diff_val)
        if ptype.startswith("VBIA"): # and ptype.endswith("GAIN"):
            if abs_diff_val > PLUS_OFFSET * 50:
                div_value = 30
            elif abs_diff_val > PLUS_OFFSET * 30:
                div_value = 20
            elif abs_diff_val > PLUS_OFFSET * 20:
                div_value = 10
            elif abs_diff_val > PLUS_OFFSET * 10:
                div_value = 5
            elif abs_diff_val > PLUS_OFFSET * 8:
                div_value = 4
            elif abs_diff_val > PLUS_OFFSET * 6:
                div_value = 3
            elif abs_diff_val > PLUS_OFFSET * 3:
                div_value = 2
            else:
                div_value = 1 

        elif ptype.startswith("AOUT"): # and ptype.endswith("GAIN"):
            
            if abs_diff_val > PLUS_OFFSET * 30:
                div_value = 50
            elif abs_diff_val > PLUS_OFFSET * 20:
                div_value = 30
            elif abs_diff_val > PLUS_OFFSET * 10:
                div_value = 20
            elif abs_diff_val > PLUS_OFFSET * 8:
                div_value = 5
            elif abs_diff_val > PLUS_OFFSET * 6:
                div_value = 3
            elif abs_diff_val > PLUS_OFFSET * 3:
                div_value = 2
            else:
                div_value = 1 

        elif ptype.startswith("CAP"):
            if abs_diff_val >= PLUS_OFFSET * 100:
                div_value = 10
            elif abs_diff_val >= PLUS_OFFSET * 50:
                div_value = 7
            elif abs_diff_val >= PLUS_OFFSET * 20:
                div_value = 4
            elif abs_diff_val >= PLUS_OFFSET * 10:
                div_value = 3
            elif abs_diff_val >= PLUS_OFFSET * 5:
                div_value = 2
            else:
                div_value = 1 

        else:
            if abs_diff_val > PLUS_OFFSET * 30:
                div_value = 30
            elif abs_diff_val > PLUS_OFFSET * 20:
                div_value = 10
            elif abs_diff_val > PLUS_OFFSET * 10:
                div_value = 5
            elif abs_diff_val > PLUS_OFFSET * 3:
                div_value = 2
            else:
                div_value = 1 


        new_direction = self.offset_updown()

        if self.cal_index == 0:
            print("Start Value : ", self.curr_gain)
            self.gain_updown = new_direction
            self.repeatP = 0
            self.repeatN = 0

            if ptype.startswith("CAP"):
                if origin_diff > 0:
                    self.curr_gain += div_value
                    self.gain_dir = PLUS 
                else:
                    self.curr_gain -= div_value
                    self.gain_dir = MINUS

            elif ptype.startswith("VBIA"):
                if origin_diff < 0:
                    self.curr_gain += div_value
                    self.gain_dir = PLUS 
                else:
                    self.curr_gain -= div_value
                    self.gain_dir = MINUS 

            else:
                if self.gain_updown == UP:
                    self.curr_gain += div_value
                    self.gain_dir = PLUS 
                else:
                    self.curr_gain -= div_value
                    self.gain_dir = MINUS

        # PLUS 방향으로 작아 지고 있음. GOOD
        elif abs_diff_val < self.old_diff and self.gain_dir == PLUS:
            self.curr_gain += div_value
            if self.repeatP:
                self.repeatP = 0

        # MINUS 방향으로 작아 지고 있음. GOOD
        elif abs_diff_val < self.old_diff and self.gain_dir == MINUS:
            self.curr_gain -= div_value
            if self.repeatN:
                self.repeatN = 0

        # PLUS 로 가는데, 값이 커지고 있음. 
        elif abs_diff_val > self.old_diff and self.gain_dir == PLUS:
            # 이미 2회 이상이고, 반복 회수가 REPEATN (2) 회 이하이면
            if self.cal_index > REPEATN and self.repeatP < REPEATN:
                self.curr_gain += int(div_value/2) or 1
                self.repeatP += 1
            # 방향을 바꿔어야 함.
            else:
                self.curr_gain -= int(div_value/2) or 1
                self.gain_dir = MINUS
                self.repeatP = 0
        
        # MINUS 로 가는데, 값이 커지고 있음.
        elif abs_diff_val > self.old_diff and self.gain_dir == MINUS:
            if self.cal_index > REPEATN and self.repeatN < REPEATN:
                self.curr_gain -= int(div_value/2) or 1
                self.repeatN += 1
            else:
                self.curr_gain += int(div_value/2) or 1
                self.gain_dir = PLUS
                self.repeatN = 0

        # PLUS 방향으로 가다가 같은 값이 나오면.
        elif abs_diff_val == self.old_diff and self.gain_dir == PLUS:
            # 10회 이상 반복 했다면 방향을 바꿔 줌
            if self.repeatP >= REPEATP:
                self.gain_dir = MINUS 
                self.curr_gain = -20
                print(f"     !!! Same PLUS :: Change Direction {self.curr_gain} {self.repeatP}th")
                self.repeatP = 0
            # 지속적으로 작은 값을 제공함
            else:
                if abs_diff_val > 3 and ptype.startswith("VOLTAGE") or ptype.startswith("IRLEAK") \
                        or ptype.startswith("VBIA") or ptype.startswith("CAP") or ptype.startswith("ZCS"):
                    self.curr_gain += div_value
                else:
                    self.curr_gain += 1
                self.repeatP += 1
                print(f"     !!! Same PLUS :: {self.curr_gain} {self.repeatP}th")

        # MINUS 방향으로 가다가 같은 값이 나오면.
        elif abs_diff_val == self.old_diff and self.gain_dir == MINUS:
            if self.repeatN >= REPEATP:
                self.gain_dir = PLUS 
                self.curr_gain += 20
                print(f"     !!! Same MINUS :: Change Direction {self.curr_gain} {self.repeatN}th")
                self.repeatN = 0
            else:
                if abs_diff_val > 3 and ptype.startswith("VOLTAGE") or ptype.startswith("IRLEAK") \
                        or ptype.startswith("VBIA") or ptype.startswith("CAP") or ptype.startswith("ZCS"):
                    self.curr_gain -= div_value
                else:
                    self.curr_gain -= 1
                self.repeatN += 1
                print(f"     !!! Same MINUS :: {self.curr_gain} {self.repeatN}th")
                
        else:
            self.curr_gain += 5
            print("    &&&& I don't know", abs_diff_val, self.old_diff, self.gain_updown)

        print(f"  ** :: {ptype} :: {self.cal_index}th :: FWD :: {self.esc_val} :: REV :: {self.jig_val} :: NEW GAIN :: {self.curr_gain} :: {div_value} :: {origin_diff}\n")
        self.old_diff = abs_diff_val

        #### Display
        display_edit.setText(str(self.curr_gain))


    #########  calculate_vbias ###################################
    def calculate_vbias(self, ptype, diff_val, display_edit):
        ##
        origin_diff = abs(self.fwd_val) - abs(self.rev_val)
        abs_diff_val = abs(origin_diff)

        if abs_diff_val > PLUS_OFFSET * 50:
            div_value = 20
        elif abs_diff_val > PLUS_OFFSET * 30:
            div_value = 15
        elif abs_diff_val > PLUS_OFFSET * 20:
            div_value = 10
        elif abs_diff_val > PLUS_OFFSET * 10:
            div_value = 5
        elif abs_diff_val > PLUS_OFFSET * 8:
            div_value = 4
        elif abs_diff_val > PLUS_OFFSET * 6:
            div_value = 3
        elif abs_diff_val > PLUS_OFFSET * 3:
            div_value = 2
        else:
            div_value = 1 

        new_direction = self.get_direction_of_vbias()

        if self.cal_index == 0:
            print("Start Value : ", self.curr_gain)
            self.gain_updown = new_direction
            self.repeatP = 0
            self.repeatN = 0

            if origin_diff < 0:
                self.curr_gain += div_value
                self.gain_dir = PLUS 
            else:
                self.curr_gain -= div_value
                self.gain_dir = MINUS 

        # PLUS 방향으로 작아 지고 있음. GOOD
        elif abs_diff_val < self.old_diff and self.gain_dir == PLUS:
            self.curr_gain += div_value
            if self.repeatP:
                self.repeatP = 0

        # MINUS 방향으로 작아 지고 있음. GOOD
        elif abs_diff_val < self.old_diff and self.gain_dir == MINUS:
            self.curr_gain -= div_value
            if self.repeatN:
                self.repeatN = 0

        # PLUS 로 가는데, 값이 커지고 있음. 
        elif abs_diff_val > self.old_diff and self.gain_dir == PLUS:
            # 이미 2회 이상이고, 반복 회수가 REPEATN (2) 회 이하이면
            if self.cal_index > REPEATN and self.repeatP < REPEATN:
                self.curr_gain += int(div_value/2) or 1
                self.repeatP += 1
            # 방향을 바꿔어야 함.
            else:
                self.curr_gain -= int(div_value/2) or 1
                self.gain_dir = MINUS
                self.repeatP = 0
        
        # MINUS 로 가는데, 값이 커지고 있음.
        elif abs_diff_val > self.old_diff and self.gain_dir == MINUS:
            if self.cal_index > REPEATN and self.repeatN < REPEATN:
                self.curr_gain -= int(div_value/2) or 1
                self.repeatN += 1
            else:
                self.curr_gain += int(div_value/2) or 1
                self.gain_dir = PLUS
                self.repeatN = 0

        # PLUS 방향으로 가다가 같은 값이 나오면.
        elif abs_diff_val == self.old_diff and self.gain_dir == PLUS:
            # 10회 이상 반복 했다면 방향을 바꿔 줌
            if self.repeatP >= REPEATP:
                self.gain_dir = MINUS 
                self.curr_gain = -20
                print(f"     !!! Same PLUS :: Change Direction {self.curr_gain} {self.repeatP}th")
                self.repeatP = 0
            # 지속적으로 작은 값을 제공함
            else:
                if abs_diff_val > 3 and ptype.startswith("VOLTAGE") or ptype.startswith("IRLEAK") \
                        or ptype.startswith("VBIA") or ptype.startswith("CAP") or ptype.startswith("ZCS"):
                    self.curr_gain += div_value
                else:
                    self.curr_gain += 1
                self.repeatP += 1
                print(f"     !!! Same PLUS :: {self.curr_gain} {self.repeatP}th")

        # MINUS 방향으로 가다가 같은 값이 나오면.
        elif abs_diff_val == self.old_diff and self.gain_dir == MINUS:
            if self.repeatN >= REPEATP:
                self.gain_dir = PLUS 
                self.curr_gain += 20
                print(f"     !!! Same MINUS :: Change Direction {self.curr_gain} {self.repeatN}th")
                self.repeatN = 0
            else:
                if abs_diff_val > 3 and ptype.startswith("VOLTAGE") or ptype.startswith("IRLEAK") \
                        or ptype.startswith("VBIA") or ptype.startswith("CAP") or ptype.startswith("ZCS"):
                    self.curr_gain -= div_value
                else:
                    self.curr_gain -= 1
                self.repeatN += 1
                print(f"     !!! Same MINUS :: {self.curr_gain} {self.repeatN}th")
                
        else:
            self.curr_gain += 5
            print("    &&&& I don't know", abs_diff_val, self.old_diff, self.gain_updown)

        print(f"  ** :: {ptype} :: {self.cal_index}th :: FWD :: {self.fwd_val} :: REV :: {self.rev_val} :: NEW GAIN :: {self.curr_gain} :: {div_value} :: abs(DIFF) :: {origin_diff}\n")
        self.old_diff = abs_diff_val

        #### Display
        display_edit.setText(str(self.curr_gain))


    #############################################################
    #########  COE, POW GAIN 조정 ################################
    #############################################################
    # 0. START : SETTING 
    def start_new_pow_coeff(self):
        device.check_password(self.esc_client)
        # Timer 정지
        self.timer.stop()
        self.timer = None 

        ## POW 값 초기
        device.write_registers(self.esc_client, device.ESC_DEV_ZCS1_GAIN, 1000)
        device.write_registers(self.esc_client, device.ESC_DEV_ZCS2_GAIN, 1000)

        print("@"*40)
        print("@@@@ ", "NEW POW COEFF CALIBARATION", " START @@@@")
        print("@"*40)

        ## CAL 
        # self.cap_list = np.array([160, 244, 485, 748, 1079, 1463, 2766, 4899, 7354, 10016])
        self.cap_list = []
        self.cap_list.append(int(self.edit_base_cap0.text()))
        self.cap_list.append(int(self.edit_base_cap1.text()))
        self.cap_list.append(int(self.edit_base_cap2.text()))
        self.cap_list.append(int(self.edit_base_cap3.text()))
        self.cap_list.append(int(self.edit_base_cap4.text()))
        self.cap_list.append(int(self.edit_base_cap5.text()))
        self.cap_list.append(int(self.edit_base_cap6.text()))
        self.cap_list.append(int(self.edit_base_cap7.text()))
        self.cap_list.append(int(self.edit_base_cap8.text()))
        self.cap_list.append(int(self.edit_base_cap9.text()))
        print(self.cap_list)

        ## GET
        self.device_cap = np.array([])
        self.rate_list = np.array([])

        self.zcs_index = 0
        self.zcs_loop_index = 0


        # setting timer
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.set_capon_on)
        self.timer.start(CAPON_TIME)


    # 1. JIG_WRITE_CAP_ON ON
    def set_capon_on(self):
        # Timer 정지
        self.timer.stop()
        self.timer = None 

        if self.zcs_index == 0:
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON0, device.ON)
        elif self.zcs_index == 1:
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON1, device.ON)
        elif self.zcs_index == 2:
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON2, device.ON)
        elif self.zcs_index == 3:
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON3, device.ON)
        elif self.zcs_index == 4:
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON4, device.ON)
        elif self.zcs_index == 5:
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON5, device.ON)
        elif self.zcs_index == 6:
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON6, device.ON)
        elif self.zcs_index == 7:
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON7, device.ON)
        elif self.zcs_index == 8:
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON8, device.ON)
        elif self.zcs_index == 9:
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON9, device.ON)

        time.sleep(TIME_INTERVAL)
        ##### esc_device_run
        self.esc_device_run()

        # setting timer
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.read_cap_value)
        self.timer.start(CAPON_TIME2)


    # 2. READ VALUE
    def read_cap_value(self):
        # Timer 정지
        self.timer.stop()
        self.timer = None 

        capval = device.read_value(self.esc_client, device.ESC_READ_CAP)
        self.device_cap = np.append(self.device_cap, capval)
        
        baseval = self.cap_list[self.zcs_index]
        caprate = round((capval - baseval)/baseval * 100, 2)
        self.rate_list = np.append(self.rate_list, caprate)

        ##### esc_device_stop
        self.esc_device_stop()
        time.sleep(TIME_INTERVAL)

        if self.zcs_index == 0:
            self.edit_val_cap0.setText(str(capval))
            self.edit_rate0.setText(str(caprate))
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON0, device.OFF)
            
        elif self.zcs_index == 1:
            self.edit_val_cap1.setText(str(capval))
            self.edit_rate1.setText(str(caprate))
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON1, device.OFF)

        elif self.zcs_index == 2:
            self.edit_val_cap2.setText(str(capval))
            self.edit_rate2.setText(str(caprate))
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON2, device.OFF)

        elif self.zcs_index == 3:
            self.edit_val_cap3.setText(str(capval))
            self.edit_rate3.setText(str(caprate))
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON3, device.OFF)

        elif self.zcs_index == 4:
            self.edit_val_cap4.setText(str(capval))
            self.edit_rate4.setText(str(caprate))
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON4, device.OFF)

        elif self.zcs_index == 5:
            self.edit_val_cap5.setText(str(capval))
            self.edit_rate5.setText(str(caprate))
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON5, device.OFF)

        elif self.zcs_index == 6:
            self.edit_val_cap6.setText(str(capval))
            self.edit_rate6.setText(str(caprate))
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON6, device.OFF)

        elif self.zcs_index == 7:
            self.edit_val_cap7.setText(str(capval))
            self.edit_rate7.setText(str(caprate))
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON7, device.OFF)

        elif self.zcs_index == 8:
            self.edit_val_cap8.setText(str(capval))
            self.edit_rate8.setText(str(caprate))
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON8, device.OFF)

        elif self.zcs_index == 9:
            self.edit_val_cap9.setText(str(capval))
            self.edit_rate9.setText(str(caprate))
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON9, device.OFF)

        ## CAP 9 이면
        if self.zcs_index == 9:
            
            self.get_esc_params()
            self.get_esc_settings_value()

            if self.zcs_loop_index == 1:
                print(self.cap_list)
                print(self.device_cap)
                print(self.rate_list)

                self.edit_rate0.setStyleSheet(self.greenback)
                self.edit_rate1.setStyleSheet(self.greenback)
                self.edit_rate2.setStyleSheet(self.greenback)
                self.edit_rate3.setStyleSheet(self.greenback)
                self.edit_rate4.setStyleSheet(self.greenback)
                self.edit_rate5.setStyleSheet(self.greenback)
                self.edit_rate6.setStyleSheet(self.greenback)
                self.edit_rate7.setStyleSheet(self.greenback)
                self.edit_rate8.setStyleSheet(self.greenback)
                self.edit_rate9.setStyleSheet(self.greenback)

                self.edit_zcsacoeff.setStyleSheet(self.greenback)
                self.edit_zcsbpow.setStyleSheet(self.greenback)
                
                self.btn_zcs1_gain.setStyleSheet(self.greenback)
                self.btn_zcs2_gain.setStyleSheet(self.greenback)

                print("#############################################################")
                print("#########  FINISH CALIBARATION  #############################")
                print("#############################################################")

                self.analogOut_setting()
            
            else:
                print("#########  ZCS1 GAIN. 캘리브레이션 #############################")
                self.zcs_loop_index += 1

                print(self.cap_list)
                print(self.device_cap)
                print(self.rate_list)

                cap_log = device.cal_log(self.cap_list)
                cap_avg = np.average(cap_log)
                cap_delta =  cap_log - cap_avg

                get_log = device.cal_log(self.device_cap)
                get_avg = np.average(get_log)
                get_delta =  get_log - get_avg

                get_cal_mul = get_delta * cap_delta
                sum1_get_cal_mul = np.sum(get_cal_mul)

                get_squre = get_delta * get_delta
                sum2_get_square = np.sum(get_squre)

                bpow = sum1_get_cal_mul / sum2_get_square
                acoeff = math.pow(math.e, cap_avg - (bpow*get_avg))

                acoeff = round(acoeff, 3)
                bpow = round(bpow, 3)

                ## 계산된 값을 DEVICE 에 반영
                value = int(acoeff * 1000)
                self.zcs1_gain = value
                self.edit_zcsacoeff.setText(str(value))
                device.write_registers(self.esc_client, device.ESC_DEV_ZCS1_GAIN, value)

                value = int(bpow * 1000)
                self.zcs2_gain = value
                self.edit_zcsbpow.setText(str(value))
                device.write_registers(self.esc_client, device.ESC_DEV_ZCS2_GAIN, value)

                ## 반영 후에 한번 돌려본다.
                self.zcs_index = 0
                self.device_cap = np.array([])
                self.rate_list = np.array([])

                self.edit_val_cap0.setText("")
                self.edit_rate0.setText("")
                self.edit_val_cap1.setText("")
                self.edit_rate1.setText("")
                self.edit_val_cap2.setText("")
                self.edit_rate2.setText("")
                self.edit_val_cap3.setText("")
                self.edit_rate3.setText("")
                self.edit_val_cap4.setText("")
                self.edit_rate4.setText("")
                self.edit_val_cap5.setText("")
                self.edit_rate5.setText("")
                self.edit_val_cap6.setText("")
                self.edit_rate6.setText("")
                self.edit_val_cap7.setText("")
                self.edit_rate7.setText("")
                self.edit_val_cap8.setText("")
                self.edit_rate8.setText("")
                self.edit_val_cap9.setText("")
                self.edit_rate9.setText("")

                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(self.set_capon_on)
                self.timer.start(CAPON_TIME2)

        else:
            self.zcs_index += 1
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(self.set_capon_on)
            self.timer.start(CAPON_TIME2)


    def analogOut_setting(self):
        device.check_password(self.esc_client)

        if not self.displaytimer:
            self.start_timer()

        # PC에서 JIG에 JIG_WRITE_CAP_ON12 ON 설정하고, RUN 지령을 한다.
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON12, device.ON)
        time.sleep(TIME_INTERVAL)

        self.setting_voltage(2500, -2500)

        # JIG에 VBIA = 2500 설정하고 Run
        # CAll Function for Current 1
        self.esc_device_run()

        # setting timer
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(lambda:self.analogOut_gain("AOUT1",
                    device.ESC_DEV_AOIO1_GAIN, device.ESC_READ_CURRENT1, 
                    device.JIG_READ_SET_AOUT1, self.edit_aout1_gain))
        self.timer.start(READ_TIME)


    def analogOut_gain(self, ptype, gain_add, esc_add, jig_add, display_edit):
        # Timer 정지
        self.timer.stop()
        self.timer = None 

        self.esc_val = int(device.read_value(self.esc_client, esc_add)/2)
        self.jig_val = device.read_value(self.jig_client, jig_add)

        diff_val = abs(self.esc_val - self.jig_val)
        print(ptype, " :: GAIN", " ::: ESC V :     ", self.esc_val, "JIG V :      ",  
                self.jig_val, " ***********************>      ", diff_val)

        ############################################################
        ########  4. VBIA GAIN 조정 완료  ############################
        ############################################################
        if diff_val <= 10:
            # SAVE OFFSET VALUE 
            print("#"*50)
            print("##"*10, ptype, " COMPLETE", "#"*10)
            print("#"*50)
            self.vbia_gain = self.curr_gain

            if ptype == 'AOUT1':
                self.btn_aout1_gain.setStyleSheet(self.greenback)
                self.edit_aout1_gain.setStyleSheet(self.greenback)
                gain_val = device.read_value(self.esc_client, gain_add)
                self.edit_aout1_gain.setText(str(gain_val))

                # setting timer
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(lambda:self.analogOut_gain("AOUT2",
                    device.ESC_DEV_AOIO2_GAIN, device.ESC_READ_CURRENT2, 
                    device.JIG_READ_SET_AOUT2, self.edit_aout2_gain))
                self.timer.start(CAPON_TIME)

            elif ptype == 'AOUT2':
                self.btn_aout2_gain.setStyleSheet(self.greenback)
                self.edit_aout2_gain.setStyleSheet(self.greenback)
                gain_val = device.read_value(self.esc_client, gain_add)
                self.edit_aout2_gain.setText(str(gain_val))

                # setting timer
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(lambda:self.checkout_digital_start())
                self.timer.start(CAPON_TIME)

        # VBIA GAIN 조정 필요 
        else:
            # PC에서 ESC에 VBIAS 값이 0V 사이가 되도록 VBIAS OFFSET을 조정한다
            self.calculate_gain(ptype, diff_val, display_edit)

            # Offset 값 정리 
            self.cal_index += 1

            div_value = c_uint16(self.curr_gain).value
            device.write_registers(self.esc_client, gain_add, div_value)

            # setting timer
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(lambda:self.analogOut_gain(ptype, 
                        gain_add, esc_add, jig_add, display_edit))
            self.timer.start(READ_TIME)


    def checkout_digital_start(self):
        device.check_password(self.esc_client)

        if not self.displaytimer:
            self.start_timer()

        # Timer 정지
        self.timer.stop()
        self.timer = None 

        # JIG DIN1 ON
        device.write_registers(self.jig_client, device.JIG_WRITE_DIN1, device.FAULT)
        time.sleep(TIME_INTERVAL)
        # ESC RUN
        self.esc_device_run()

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.checkout_digital_input)
        self.timer.start(READ_TIME)


    def checkout_digital_input(self):
        # CHECK INTERLOCK FAULT 
        ### APPEND
        fault = device.read_fault(self.esc_client, device.ESC_READ_DEVICE_STATUS)
        if 7 in fault:
            self.btn_din1_check.setStyleSheet(self.greenback)

            # ESC STOP
            self.esc_device_stop()
            device.write_registers(self.jig_client, device.JIG_WRITE_DIN1, device.OFF)

            print("send BIT_FAULT_CLEAR")
            device.write_registers(self.esc_client, device.ESC_WRITE_BIT_POWER_ON, device.BIT_FAULT_CLEAR)

            #### NEXT SUCCESS  
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(self.checkout_digital_dout2)
            self.timer.start(READ_TIME*5)
        
        else:
            self.btn_din1_check.setStyleSheet(self.redback)
            self.fault_loop += 1

            if self.fault_loop <= 3:
                # LOOP
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(self.checkout_digital_input)
                self.timer.start(READ_TIME)

            else:

                #### NEXT FAIL  
                self.esc_device_stop()
                device.write_registers(self.jig_client, device.JIG_WRITE_DIN1, device.OFF)

                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(self.checkout_digital_dout2)
                self.timer.start(READ_TIME)

    def checkout_digital_dout2(self):
        self.timer.stop()
        self.timer = None 

        # ESC RUN
        self.esc_device_run()
        time.sleep(TIME_INTERVAL)

        # CHECK DOUT2 
        print("step 2 : device.read_value(self.jig_client, device.JIG_READ_SET_DOUT2)")
        result = device.read_value(self.jig_client, device.JIG_READ_SET_DOUT2)
        print("step 2", result)
        if result:

            self.btn_dout2_check.setStyleSheet(self.greenback)

        self.esc_device_stop()
        time.sleep(TIME_INTERVAL)
        ## CAP2 ON
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON2, device.ON)
        time.sleep(TIME_INTERVAL)

        self.esc_device_run()
        
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.checkout_digital_dout3)
        self.timer.start(READ_TIME)

    def checkout_digital_dout3(self):
        self.timer.stop()
        self.timer = None 

        # CHECK DOUT3
        result = device.read_value(self.jig_client, device.JIG_READ_SET_DOUT3)
        print("step 3", result)
        if result:
            self.btn_dout3_check.setStyleSheet(self.greenback)

        # ESC STOP
        self.esc_device_stop()
        time.sleep(TIME_INTERVAL)

        ## CAP2 OFF
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON2, device.OFF)

        print("#"*50)
        print("#"*20, "FINISH DOUT", "#"*20)
        print("#"*50)

    def setting_voltage(self, vol1, vol2):
        # - PC에서 ESC에 VO1 = 1250V, VO2 = -1250V로 설정하고, RUN 지령을 한다.
        device.write_registers(self.esc_client, device.ESC_WRITE_VOLTAGE1, vol1)
        value = c_uint16(vol2).value
        device.write_registers(self.esc_client, device.ESC_WRITE_VOLTAGE2, value)
        self.lbl_voltage1.setText("2500")
        self.lbl_voltage2.setText("-2500")


    #############################################################
    # OFFSET GAIN 완료 후 ESC 정보 읽어와 보여주기
    def get_esc_settings_value(self):
        # self.esc_device_run()
        # time.sleep(TIME_INTERVAL)
        result = device.read_esc_values(self.esc_client)
        # print("Settings : ", result)
        if result:
            self.lbl_voltage1.setText(str(result['voltage1']))
            self.lbl_voltage2.setText(str(result['voltage2']))
            self.lbl_current1.setText(str(result['current1']))
            self.lbl_current2.setText(str(result['current2']))
            self.lbl_vbias.setText(str(result['vbias']))
            self.lbl_vcs.setText(str(result['vcs']))
            self.lbl_ics.setText(str(result['ics']))
            self.lbl_cp.setText(str(result['cp']))
            self.lbl_leak1.setText(str(result['ioleak1']))
            self.lbl_leak2.setText(str(result['ioleak2']))

    def get_esc_offsetgain_value(self):
        data = device.read_esc_offsetgain_values(self.esc_client)
        if data:
            self.vo1_offset = data["vo1_offset"]  
            self.vo1_gain = data["vo1_gain"]  
            self.vo2_offset = data["vo2_offset"]  
            self.vo2_gain = data["vo2_gain"]  

            self.io1_offset = data["io1_offset"]  
            self.io1_gain = data["io1_gain"]  
            self.io2_offset = data["io2_offset"]  
            self.io2_gain = data["io2_gain"]  

            self.ir1_offset = data["ir1_offset"]  
            self.ir1_gain = data["ir1_gain"]  
            self.ir2_offset = data["ir2_offset"]  
            self.ir2_gain = data["ir2_gain"]  

            self.vbia_offset = data["vbia_offset"]  
            self.vbia_gain = data["vbia_gain"]  
            self.cap_offset = data["cap_offset"]  

            self.zcs1_gain = data["zcs1_gain"]  
            self.zcs2_gain = data["zcs2_gain"]

            ## SET
            self.edit_vo1_offset.setText(str(data["vo1_offset"]))
            self.edit_vo1_gain.setText(str(data["vo1_gain"]))
            self.edit_vo2_offset.setText(str(data["vo2_offset"]))
            self.edit_vo2_gain.setText(str(data["vo2_gain"]))

            self.edit_io1_offset.setText(str(data["io1_offset"]))
            self.edit_io1_gain.setText(str(data["io1_gain"]))
            self.edit_io2_offset.setText(str(data["io2_offset"]))
            self.edit_io2_gain.setText(str(data["io2_gain"]))

            self.edit_ir1_offset.setText(str(data["ir1_offset"]))
            self.edit_ir1_gain.setText(str(data["ir1_gain"]))
            self.edit_ir2_offset.setText(str(data["ir2_offset"]))
            self.edit_ir2_gain.setText(str(data["ir2_gain"]))

            self.edit_vbia_offset.setText(str(data["vbia_offset"]))
            self.edit_vbia_gain.setText(str(data["vbia_gain"]))
            self.edit_cap_offset.setText(str(data["cap_offset"]))

            self.edit_zcsacoeff.setText(str(data["zcs1_gain"]))
            self.edit_zcsbpow.setText(str(data["zcs1_gain"]))

    #  OFFSET G data[#  OFFSET"]AIN 완료 후 ESC 정보 읽어와 보여주기
    def get_esc_params(self):
        result = device.read_esc_setting_value(self.esc_client)
        # print("Params : ", result)

        if result:
            self.voltage1.setText(str(result['voltage1']))
            self.current1.setText(str(result['current1']))
            self.voltage2.setText(str(result['voltage2']))
            self.current2.setText(str(result['current2']))
            self.leak_fault_level.setText(str(result['leak_fault_level']))
            self.ro_min_fault.setText(str(result['ro_min_fault']))
            self.up_time.setText(str(result['up_time']))
            self.down_time.setText(str(result['down_time']))
            
            if result['rd_mode']:
                self.rd_mode_on.setChecked(True)
            else:
                self.rd_mode_off.setChecked(True)

            self.toggle_count.setText(str(result['toggle_count']))
            self.slope.setText(str(result['slope']))
            self.coeff.setText(str(result['coeff']))

            if result['rd_select']:
                self.rd_select_remote.setChecked(True)
            else:
                self.rd_select_internal.setChecked(True)

            self.local_address.setText(str(result['local_address']))
            self.arc_delay.setText(str(result['arc_delay']))
            self.arc_rate.setText(str(result['arc_rate']))

            if result['rd_toggle']:
                self.rd_toggle_on.setChecked(True)
            else:
                self.rd_toggle_off.setChecked(True)

            if result['rd_arc']:
                self.rd_arc_on.setChecked(True)
            else:
                self.rd_arc_off.setChecked(True)

            if result['rd_ocp']:
                self.rd_ocp_on.setChecked(True)
            else:
                self.rd_ocp_off.setChecked(True)
            
            self.target_cap.setText(str(result['target_cap']))
            self.cap_deviation.setText(str(result['cap_deviation']))
            # self.time_delay.setText(str(result['time_delay']))

    # 전원 장치와 송신을 위한 설정
    def device_params_modify(self):
        Dialog = QDialog()
        self.initial = device.read_esc_setting_value(self.esc_client)
        print(self.initial)

        dialog = ESCParams(Dialog, self.esc_client, self.initial)
        dialog.show()
        response = dialog.exec_()

    @pyqtSlot(str)
    def read_product_serial(self, code):
        if code:
            self.btn_qrcode.hide()
            self.label_qrcode.setText(code)
            self.btn_jig.setEnabled(True)
            self.serialNo = code

            self.btn_params.setEnabled(True)
            self.btn_calib.setEnabled(True)
            self.btn_cal_irleak.setEnabled(True)
            self.btn_cal_vbias.setEnabled(True)
            self.btn_cal_cap.setEnabled(True)
            self.btn_aout.setEnabled(True)
            
        else:
            print("No code :: read_product_serial ")

    def readQRCode(self, nth):
        self.btn_qrcode.setEnabled(False)
        self.label_qrcode.setText("????")
        
    def timedisplay(self):
        now = datetime.now()
        secondtime = int((now - self.starttime).total_seconds())
        timedisplay = '{:02d}:{:02d}:{:02d}'.format(
                (secondtime // 60) // 60, (secondtime // 60) % 60, secondtime % 60)
        self.display_time.display(timedisplay)

    # NORMAL
    def offset_updown(self):
        if abs(self.esc_val) > abs(self.jig_val):
            return UP
        else:
            return DOWN

    def get_direction_of_vbias(self):
        if self.esc_val > self.jig_val:
            return UP
        else:
            return DOWN

    # 임시용 ESC JIG Connection
    def temp_setup_connection(self):
        # if self.qrreader:
        #     self.btn_qrcode.setEnabled(True)
        #     self.btn_esc_gen.setEnabled(False)

        #     self.btn_params.setEnabled(False)
        #     self.btn_calib.setEnabled(False)
        #     self.btn_cal_irleak.setEnabled(False)
        #     self.btn_cal_vbias.setEnabled(False)
        #     self.btn_cal_cap.setEnabled(False)


        # else:
        #     print("QR Reader를 연결하고 프로그램을 시작하시오.")
        #     ###CHANGE
        #     # sys.exit()
        pass

    # ESC 전원 장치와 송신을 위한 설정
    def connect_esc_gen(self):
        if self.com_open_flag == False:
            Dialog = QDialog()
            self.com_open_flag == True
            dialog = SettingWin(Dialog, self.used_port)
            dialog.show()
            response = dialog.exec_()

            # OK 를 하면 설정 값을 읽어와서 통신을 한다.
            if response == QDialog.Accepted:
                self.gen_port = dialog.gen_port
                self.com_speed = dialog.com_speed
                self.com_data = dialog.com_data
                self.com_parity = dialog.com_parity
                self.com_stop = dialog.com_stop
                self.com_open_flag = False
                self.esc_client = device.make_connect(
                    port=self.gen_port, ptype='rtu',
                    speed=self.com_speed, bytesize=self.com_data, 
                    parity=self.com_parity, stopbits=self.com_stop
                )

                if self.esc_client:
                    self.used_port.append(self.gen_port)
                    self.label_esc_gen.setText(self.gen_port)
                    self.label_esc_gen.setStyleSheet(self.greenback)
                    self.btn_esc_gen.hide()
                
                self.com_open_flag = False

                self.get_esc_offsetgain_value()

        else:
            print("Already Open Dialog")

    # JIG 전원 장치와 송신을 위한 설정
    def connect_jig_gen(self):
        if self.com_open_flag == False:
            Dialog = QDialog()
            self.com_open_flag == True
            dialog = SettingWin(Dialog, self.used_port)
            dialog.show()
            response = dialog.exec_()

            # OK 를 하면 설정 값을 읽어와서 통신을 한다.
            if response == QDialog.Accepted:
                self.gen_port = dialog.gen_port
                self.com_speed = dialog.com_speed
                self.com_data = dialog.com_data
                self.com_parity = dialog.com_parity
                self.com_stop = dialog.com_stop
                self.com_open_flag = False
                self.jig_client = device.make_connect(
                    port=self.gen_port, ptype='rtu',
                    speed=self.com_speed, bytesize=self.com_data, 
                    parity=self.com_parity, stopbits=self.com_stop
                )

                if self.jig_client:
                    self.used_port.append(self.gen_port)
                    self.btn_jig.hide()
                    self.label_jig.setText(self.gen_port)
                    self.label_jig.setStyleSheet(self.greenback)
                    self.btn_esc_gen.setEnabled(True)
                
                self.com_open_flag = False

        else:
            print("Already Open Dialog")

    # ESC RESET
    def reset_data(self):
        print("#"*40)
        print("######## ESC RESET ##########################")
        print("#"*40)

        # self.btn_qrcode.show()
        # self.btn_qrcode.setEnabled(True)
        # self.label_qrcode.setText('#')

        self.used_port.pop()

        self.btn_esc_gen.show()
        self.btn_esc_gen.setEnabled(True)
        self.label_esc_gen.setText("No")
        self.label_esc_gen.setStyleSheet(self.btnback)

        self.com_open_flag = False
        self.esc_client = None
        self.stopFlag = True

        self.timer = QtCore.QTimer()
        self.displaytimer = QtCore.QTimer()

        self.esc_val = 0
        self.jig_val = 0

        self.set_offset = 0
        self.curr_gain = 0

        self.cal_index = 0
        self.gain_dir = None

        self.cal_params = BPOW
        self.old_acoeff = 0
        self.old_bpow = 0
        self.starttime = 0

        ## ESC value
        self.serialNo = None
        self.fw_version = None

        self.vo1_offset = 0
        self.vo1_gain = 0
        self.vo2_offset = 0
        self.vo2_gain = 0

        self.io1_offset = 0
        self.io1_gain = 0
        self.io2_offset = 0
        self.io2_gain = 0

        self.ir1_offset = 0
        self.ir1_gain = 0
        self.ir2_offset = 0
        self.ir2_gain = 0

        self.vbia_offset = 0
        self.vbia_gain = 0
        
        self.cap_offset = 0
        self.zcs1_gain = 0
        self.zcs2_gain = 0

        ## Btn Reset
        self.btn_vo1_offset.setStyleSheet(self.btnback)
        self.btn_vo1_gain.setStyleSheet(self.btnback)
        self.btn_vo2_offset.setStyleSheet(self.btnback)
        self.btn_vo2_gain.setStyleSheet(self.btnback)

        self.btn_io1_offset.setStyleSheet(self.btnback)
        self.btn_io1_gain.setStyleSheet(self.btnback)
        self.btn_io2_offset.setStyleSheet(self.btnback)
        self.btn_io2_gain.setStyleSheet(self.btnback)

        self.btn_ir1_offset.setStyleSheet(self.btnback)
        self.btn_ir1_gain.setStyleSheet(self.btnback)
        self.btn_ir2_offset.setStyleSheet(self.btnback)
        self.btn_ir2_gain.setStyleSheet(self.btnback)

        self.btn_vbia_offset.setStyleSheet(self.btnback)
        self.btn_vbia_gain.setStyleSheet(self.btnback)

        self.btn_cap_offset.setStyleSheet(self.btnback)
        self.btn_zcs1_gain.setStyleSheet(self.btnback)
        self.btn_zcs2_gain.setStyleSheet(self.btnback)

        self.btn_aout1_gain.setStyleSheet(self.btnback)
        self.btn_aout2_gain.setStyleSheet(self.btnback)
        self.btn_din1_check.setStyleSheet(self.btnback)

        self.btn_dout1_check.setStyleSheet(self.btnback)
        self.btn_dout2_check.setStyleSheet(self.btnback)
        self.btn_dout3_check.setStyleSheet(self.btnback)

        secondtime = 0
        timedisplay = '{:02d}:{:02d}:{:02d}'.format(
                (secondtime // 60) // 60, (secondtime // 60) % 60, secondtime % 60)
        self.display_time.display(timedisplay)

        ## Value Reset
        self.edit_vo1_offset.reset_edit()
        self.edit_vo1_gain.reset_edit()
        self.edit_vo2_offset.reset_edit()
        self.edit_vo2_gain.reset_edit()

        self.edit_io1_offset.reset_edit()
        self.edit_io1_gain.reset_edit()
        self.edit_io2_offset.reset_edit()
        self.edit_io2_gain.reset_edit()

        self.edit_ir1_offset.reset_edit()
        self.edit_ir1_gain.reset_edit()
        self.edit_ir2_offset.reset_edit()
        self.edit_ir2_gain.reset_edit()

        self.edit_vbia_offset.reset_edit()
        self.edit_vbia_gain.reset_edit()
        self.edit_cap_offset.reset_edit()

        self.edit_zcsacoeff.reset_edit()
        self.edit_zcsbpow.reset_edit()

        self.edit_aout1_gain.reset_edit()
        self.edit_aout2_gain.reset_edit()

        ## ZCS
        self.edit_rate0.reset_edit()
        self.edit_rate1.reset_edit()
        self.edit_rate2.reset_edit()
        self.edit_rate3.reset_edit()
        self.edit_rate4.reset_edit()
        self.edit_rate5.reset_edit()
        self.edit_rate6.reset_edit()
        self.edit_rate7.reset_edit()
        self.edit_rate8.reset_edit()
        self.edit_rate9.reset_edit()

        ## ESC Value
        self.voltage1.reset_edit()
        self.current1.reset_edit()
        
        self.voltage2.reset_edit()
        self.current2.reset_edit()

        self.leak_fault_level.reset_edit()
        self.ro_min_fault.reset_edit()

        self.up_time.reset_edit()
        self.down_time.reset_edit()

        self.slope.reset_edit()
        self.toggle_count.reset_edit()

        self.arc_delay.reset_edit()
        self.coeff.reset_edit()

        self.arc_rate.reset_edit()
        self.local_address.reset_edit()

        self.target_cap.reset_edit()
        self.cap_deviation.reset_edit()

        self.time_delay.reset_edit()

        # voltage 1
        self.lbl_voltage1.reset_edit()
        self.lbl_current1.reset_edit()

        self.lbl_voltage2.reset_edit()
        self.lbl_current2.reset_edit()

        self.lbl_vbias.reset_edit()
        self.lbl_vcs.reset_edit()

        self.lbl_ics.reset_edit()
        self.lbl_cp.reset_edit()

        self.lbl_leak1.reset_edit()
        self.lbl_leak2.reset_edit()

        ## Button
        self.btn_params.setEnabled(False)
        self.btn_calib.setEnabled(False)
        self.btn_cal_irleak.setEnabled(False)
        self.btn_cal_vbias.setEnabled(False)
        self.btn_cal_cap.setEnabled(False)
        
    # 단말기 STOP
    def all_device_stop(self):
        print(" STOP ALL DEVICE ")
        self.esc_device_stop()
        time.sleep(TIME_INTERVAL)
        device.jig_device_stop(self.jig_client)
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON0, device.OFF)
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON1, device.OFF)
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON2, device.OFF)
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON3, device.OFF)
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON4, device.OFF)
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON5, device.OFF)
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON6, device.OFF)
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON7, device.OFF)
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON8, device.OFF)
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON9, device.OFF)
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON10, device.OFF)
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON11, device.OFF)
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON12, device.OFF)
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON13, device.OFF)
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON14, device.OFF)

        # self.btn_stop.setEnabled(False)
        self.stopFlag = True
        if self.timer:
            self.timer.stop()
            self.timer = None
            self.timer = QtCore.QTimer()

            self.displaytimer.stop()
            self.displaytimer = None
            self.displaytimer = QtCore.QTimer()

    # Run Device (중간에 Run 을 눌러서 디바이스 시작시킴)
    def jig_device_run(self):
        device.jig_device_run(self.jig_client)

    def jig_device_stop(self):
        device.jig_device_stop(self.jig_client)

    def esc_device_run(self):
        device.esc_device_run(self.esc_client)

    def esc_device_stop(self):
        device.esc_device_stop(self.esc_client)

    ######### DISPLAY #########################
    def displayMenuLayout(self, parent_layout, glabel, btn, second=None):
        grp_sub = QGroupBox(glabel)
        layout_sub = QHBoxLayout()
        grp_sub.setLayout(layout_sub)
        layout_sub.addWidget(btn)
        if second:
            layout_sub.addWidget(second)
        parent_layout.addWidget(grp_sub)

    def displayVerLayout(self, parent_layout, glabel, first, second=None, third=None, forth=None, fifth=None, sixth=None):
        grp_sub = QGroupBox(glabel)
        layout_sub = QHBoxLayout()
        grp_sub.setLayout(layout_sub)
        layout_sub.addWidget(first)
        if second:
            layout_sub.addWidget(second)
        if third:
            layout_sub.addWidget(third)
        if forth:
            layout_sub.addWidget(forth)
        if fifth:
            layout_sub.addWidget(fifth)
        if sixth:
            layout_sub.addWidget(sixth)

        parent_layout.addWidget(grp_sub)

    def layoutButtonGid(self, layout_grid, nth, btn1, btn2, btn3, btn4=None):
        layout_grid.addWidget(btn1, 0, nth)
        layout_grid.addWidget(btn2, 1, nth)
        layout_grid.addWidget(btn3, 0, nth+1)
        if btn4:
            layout_grid.addWidget(btn4, 1, nth+1)
        

    def LayoutCalGrid(self, layout_grid, nth, 
                btn_item1, edit_offset1, edit_gain1, unit1,
                btn_item2, edit_offset2, edit_gain2, unit2):
    ## 
        layout_grid.addWidget(btn_item1, nth, 0)
        layout_grid.addWidget(edit_offset1, nth, 1)
        layout_grid.addWidget(edit_gain1, nth, 2)
        layout_grid.addWidget(QLabel(unit1), nth, 3)

        layout_grid.addWidget(btn_item2, nth, 4)
        layout_grid.addWidget(edit_offset2, nth, 5)
        layout_grid.addWidget(edit_gain2, nth, 6)
        layout_grid.addWidget(QLabel(unit2), nth, 7)

    def LayoutCalGrid2(self, layout_grid, nth, 
                btn_item1, edit_gain1,
                btn_item2, edit_gain2):
    ##
        layout_grid.addWidget(btn_item1, nth, 0)
        layout_grid.addWidget(edit_gain1, nth, 1, 1, 2)
        layout_grid.addWidget(QLabel(""), nth, 3)

        layout_grid.addWidget(btn_item2, nth, 4)
        layout_grid.addWidget(edit_gain2, nth, 5, 1, 2)
        layout_grid.addWidget(QLabel(""), nth, 7)


    def LayoutValueGrid(self, grid_box, nth, label1, edit1, range1, unit1, 
                                        label2, edit2, range2, unit2):
    ##
        grid_box.addWidget(GLabel(label1), nth, 0)
        grid_box.addWidget(edit1, nth, 1)
        grid_box.addWidget(MyLabel(range1), nth, 2)
        grid_box.addWidget(MyLabel(unit1), nth, 3)

        grid_box.addWidget(GLabel(label2), nth, 4)
        grid_box.addWidget(edit2, nth, 5)
        grid_box.addWidget(MyLabel(range2), nth, 6)
        grid_box.addWidget(MyLabel(unit2), nth, 7)

    def LayoutThreeGrid(self, layout_esc, nth, label, value, unit):
        layout_esc.addWidget(GLabel(label), nth, 0)
        layout_esc.addWidget(value, nth, 1)
        layout_esc.addWidget(unit, nth, 2)

    def LayoutTwoRwoGrid(self, layout_esc, row, nth, label, base, value, rate):
        layout_esc.addWidget(label, row, nth)
        layout_esc.addWidget(base, row, nth+1)
        layout_esc.addWidget(value, row, nth+2)
        layout_esc.addWidget(rate, row, nth+3)

    def LayoutRadioBoxGrid(self, layout_grid, row, col, label, 
                                btn1, btn2, comment):
    ##
        grou_sub = QGroupBox(label)
        layout_sub = QHBoxLayout()
        grou_sub.setLayout(layout_sub)
        layout_sub.addWidget(MyLabel(label))
        layout_sub.addWidget(btn1)
        layout_sub.addWidget(btn2)
        # layout_sub.addWidget(MyLabel(comment))
        layout_grid.addWidget(grou_sub, row, col)


    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Escape:
            pass
        elif e.key() == Qt.Key_F:
            self.showFullScreen()
        elif e.key() == Qt.Key_N:
            self.showNormal()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    form = ESCCalibration()
    form.show()
    sys.exit(app.exec_())