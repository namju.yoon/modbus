import numpy as np

from PyQt5.QtWidgets import QWidget, QLabel, QPushButton, QDesktopWidget, QMainWindow
from PyQt5.QtWidgets import QGroupBox,QVBoxLayout, QHBoxLayout, QGridLayout
from PyQt5.QtWidgets import QApplication, QDialog, QTabWidget
from PyQt5.QtWidgets import QLCDNumber, QStatusBar
from PyQt5.QtCore import pyqtSlot, Qt
from PyQt5 import QtCore
from PyQt5.QtCore import QDate, Qt
from PyQt5.QtGui import QPixmap
from itertools import count
import pyqtgraph as pg

import epc_serial as device
from common.setup_device import SettingWin
from common.epcbasic import DataView, EpcModule
from common.block import SimpleBlock, resource_path, SimpleDisplay, DataGroupBlock, AlarmBlock

TPERIOD = 3000

import logging
FORMAT = ('%(asctime)-15s %(threadName)-15s '
          '%(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.DEBUG)


def CLed():
    btn = QPushButton('RUN')
    btn.setFixedSize(80, 80)
    style = "border: 3px solid lightgray;border-radius: 40px;background-color: green;color: white;font-size: 16px;font-weight: bold;"
    btn.setStyleSheet(style)
    return btn


class EpcMonitoring(QMainWindow):
    def __init__(self):
        super().__init__()

        self.timer = QtCore.QTimer()
        self.port = "COM4"
        self.countN = 0
        self.client = None
        self.com_open_flag = False
        pg.setConfigOptions(background='w')  # 흰색 배경 

        self.btn_default = "QPushButton{font-size: 16pt; color: gray; background-color: #ddd;}"
        self.btn_alarm = "QPushButton{font-size: 16pt; font-weight: bold; color: blue; background-color: white;}"

        self.initUI()

    def initUI(self):
        self.setWindowTitle("PSTEK EPC MONITORING")
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width(), screensize.height()) 

        mainwidget = QWidget()                # 위젯의 인스턴스 생성만으로도 QMainWindow에 붙는다.
        self.setCentralWidget(mainwidget)
        main_layout = QVBoxLayout()
        mainwidget.setLayout(main_layout)

        #### MENU
        grp_menu = QGroupBox("")
        layout_menu = QGridLayout()
        grp_menu.setLayout(layout_menu)
        
        # 통신 체크 
        grp_comm = QGroupBox("")
        layout_comm = QVBoxLayout()
        grp_comm.setLayout(layout_comm)

        self.btn_com_gen = QPushButton("전원 장치 연결")
        self.btn_com_gen.clicked.connect(self.setting_generator)
        layout_comm.addWidget(self.btn_com_gen)

        self.lbl_com_gen = QLabel("")
        layout_comm.addWidget(self.lbl_com_gen)

        comm = QLabel("통신체크")
        comm.setStyleSheet("QLabel{font-size: 20pt; font-weight: bold}")
        layout_comm.addWidget(comm)

        self.lbl_count = QLabel(str(self.countN))
        self.lbl_count.setStyleSheet('margin: -1px; font-size: 16pt; padding: 0px; color: green;font-weight: bold')
        self.lbl_count.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        layout_comm.addWidget(self.lbl_count)
        layout_menu.addWidget(grp_comm, 0, 0)

        # Title
        title = QLabel("85kV 1000mA")
        title.setStyleSheet("QLabel{font-size: 30pt; font-weight: bold}")
        # self.displayMenuLayout(layout_menu, '', title)
        layout_menu.addWidget(title, 0, 1, 1, 2)

        # LIMIT 설정 
        grp_limit = QGroupBox("LIMIT 설정")
        layout_limit = QGridLayout()
        grp_limit.setLayout(layout_limit)

        self.vol_limit = DataGroupBlock(self, "VOL LIMIT", 1, 67.00, 'kV', layout_limit, 0, 1, 80)
        self.cur_limit = DataGroupBlock(self, "CUR LIMIT", 2, 575.00, 'mA', layout_limit, 1, 1, 1000)
        layout_menu.addWidget(grp_limit, 0, 4, 1, 2)

        # RUN/STOP 
        grp_run = QGroupBox("RUN/Stop")
        layout_run = QGridLayout()
        grp_run.setLayout(layout_run)

        self.btn_run = CLed()
        layout_run.addWidget(self.btn_run, 0, 0, 2, 1)
        
        self.btn_vlim = QPushButton("V LIMIT")
        self.btn_vlim.setStyleSheet(self.btn_default)
        layout_run.addWidget(self.btn_vlim, 0, 1)

        self.btn_ilim = QPushButton("I LIMIT")
        self.btn_ilim.setStyleSheet(self.btn_default)
        layout_run.addWidget(self.btn_ilim, 1, 1)

        layout_menu.addWidget(grp_run, 0, 6)

        # ON/OFF 
        grp_btn = QGroupBox("ON/OFF")
        layout_btn = QVBoxLayout()
        grp_btn.setLayout(layout_btn)

        self.btn_on = QPushButton("ON")
        self.btn_on.setStyleSheet("QPushButton{font-size: 16pt; font-weight: bold; color: red}")
        self.btn_off = QPushButton("OFF")
        self.btn_off.setStyleSheet("QPushButton{font-size: 16pt; font-weight: bold; color: blue}")
        layout_btn.addWidget(self.btn_on)
        layout_btn.addWidget(self.btn_off)
        layout_menu.addWidget(grp_btn, 0, 7)

        # Logo
        labelLogo = QLabel("")
        pixmap = QPixmap(resource_path("logo.png"))
        labelLogo.setAlignment(Qt.AlignRight)
        labelLogo.setPixmap(pixmap)
        layout_menu.addWidget(labelLogo, 0, 8)        

        main_layout.addWidget(grp_menu)

        ##### TAB Layout
        # Tab 생성
        mainTab = QWidget()
        tab_modules = QWidget()
        tab_graph = QWidget()
        tab_alarm = QWidget()
        tab_setting = QWidget()

        tabs = QTabWidget()
        tabs.setStyleSheet("QTabBar{font-size: 16pt; font-weight: bold}");
        tabs.addTab(mainTab, 'MAIN')
        tabs.addTab(tab_modules, 'MODULES')
        tabs.addTab(tab_graph, 'GRAPH')
        tabs.addTab(tab_alarm, 'ALARM')
        tabs.addTab(tab_setting, 'SETTING')
        main_layout.addWidget(tabs)

        ## 4th ########################
        # Status
        self.statusbar = QStatusBar()
        self.setStatusBar(self.statusbar)
        self.statusbar.setObjectName("statusbar")
        
        self.statusmessage = 'PSTEK, {},  {}'
        now = QDate.currentDate()
        displaymessage = self.statusmessage.format(now.toString(Qt.ISODate), 'Ready !!')
        self.statusbar.showMessage(displaymessage)

        self.displayMain(mainTab)
        self.displayModules(tab_modules)
        self.displayGraph(tab_graph)
        self.displayAlarm(tab_alarm)
        self.displaySetting(tab_setting)

        self.show()

    def displayMenuLayout(self, parent_layout, glabel, btn, second=None):
        grp_sub = QGroupBox(glabel)
        layout_sub = QHBoxLayout()
        grp_sub.setLayout(layout_sub)
        layout_sub.addWidget(btn)
        if second:
            layout_sub.addWidget(second)
        parent_layout.addWidget(grp_sub)

    def mainGroupDisplay(self, text, board, bcolor):
        grp_board = QGroupBox(text)
        grp_board.setStyleSheet(f"background-color: {bcolor}")
        layout_board = QVBoxLayout()
        grp_board.setLayout(layout_board)

        # SmileKey 01
        group01 = QGroupBox("SmileKey 01")
        layout_sm01 = QGridLayout()
        group01.setLayout(layout_sm01)
        sm1 = board.smilekey01
        layout_sm01.addWidget(sm1.main_btn, 0, 0, 1, 2)
        layout_sm01.addWidget(sm1.main_conn, 1, 0)
        layout_sm01.addWidget(sm1.main_status, 1, 1)
        layout_board.addWidget(group01)

        # SmileKey 02
        group02 = QGroupBox("SmileKey 02")
        layout_sm02 = QGridLayout()
        group02.setLayout(layout_sm02)
        sm2 = board.smilekey02
        layout_sm02.addWidget(sm2.main_btn, 0, 0, 1, 2)
        layout_sm02.addWidget(sm2.main_conn, 1, 0)
        layout_sm02.addWidget(sm2.main_status, 1, 1)
        layout_board.addWidget(group02)

        # SmileKey 03
        group03 = QGroupBox("SmileKey 03")
        layout_sm03 = QGridLayout()
        group03.setLayout(layout_sm03)
        sm3 = board.smilekey03
        layout_sm03.addWidget(sm3.main_btn, 0, 0, 1, 2)
        layout_sm03.addWidget(sm3.main_conn, 1, 0)
        layout_sm03.addWidget(sm3.main_status, 1, 1)
        layout_board.addWidget(group03)

        return grp_board

    def displayMain(self, tab):
        main_layer = QHBoxLayout()
        tab.setLayout(main_layer)

        # Graph 
        grp_graph = QGroupBox("전원 장치 출력 그래프")
        layout_graph = QHBoxLayout()
        grp_graph.setLayout(layout_graph)

        x = np.array([])
        y1 = np.array([])
        y2 = np.array([])
        y3 = np.array([])
        y4 = np.array([])
        y5 = np.array([])

        self.v_ac = DataView('V AC', x, y1, 1000, '#3AE044')
        self.a_ac = DataView('A AC', x, y2, 2000, '#F7D634')
        self.po = DataView('PO(kW)', x, y3, 1000, '#ED6032')
        self.kv_dc = DataView('kV DC', x, y4, 2000, '#B84EED')
        self.ma_dc = DataView('mA DC', x, y5, 1000, '#34ABF7')

        self.gp_vac = self.drawBarChart(self.v_ac)
        layout_graph.addWidget(self.gp_vac)

        self.gp_aac = self.drawBarChart(self.a_ac)
        layout_graph.addWidget(self.gp_aac)

        self.gp_po = self.drawBarChart(self.po)
        layout_graph.addWidget(self.gp_po)

        self.gp_kvdc = self.drawBarChart(self.kv_dc)
        layout_graph.addWidget(self.gp_kvdc)

        self.gp_ = self.drawBarChart(self.ma_dc)
        layout_graph.addWidget(self.gp_)

        main_layer.addWidget(grp_graph)

        # Data Value
        grp_data = QGroupBox("전원 장치 출력 데이터")
        layout_data = QGridLayout()
        grp_data.setLayout(layout_data)
        
        self.d_vac = SimpleDisplay("V AC", "kV", 0, layout_data, 0)
        self.d_aac = SimpleDisplay("A AC", "A", 0, layout_data, 1)
        self.d_po = SimpleDisplay("PO(kW)", "kW", 0, layout_data, 2)
        self.d_kvdc = SimpleDisplay("kV DC", "kV", 0, layout_data, 3)
        self.d_madc = SimpleDisplay("mA DC", "mA", 0, layout_data, 4)
        self.d_temp = SimpleDisplay("Temperature", "°C", 0, layout_data, 5)
        self.d_spark = SimpleDisplay("SPARK", "회", 0, layout_data, 6)
        self.d_kvp = SimpleDisplay("kVP", "", 0, layout_data, 7)

        main_layer.addWidget(grp_data)

    def displayModules(self, tab):
        main_layer = QHBoxLayout()
        tab.setLayout(main_layer)

        self.mod1 = EpcModule('#1 Module Info')
        self.mod2 = EpcModule('#2 Module Info')
        self.mod3 = EpcModule('#3 Module Info')
        self.mod4 = EpcModule('#4 Module Info')
        self.mod5 = EpcModule('#5 Module Info')

        self.showModuleData(self.mod1, main_layer)
        self.showModuleData(self.mod2, main_layer)
        self.showModuleData(self.mod3, main_layer)
        self.showModuleData(self.mod4, main_layer)
        self.showModuleData(self.mod5, main_layer)

    def showModuleData(self, mod, player):
        grp_mod = mod.initUI()
        player.addWidget(grp_mod)

    def displayGraph(self, tab):
        main_layer = QHBoxLayout()
        tab.setLayout(main_layer)

        grp_graph = QGroupBox("Graph")
        layout_graph = QGridLayout()
        grp_graph.setLayout(layout_graph)
        main_layer.addWidget(grp_graph)

        layout_graph.addWidget(self.v_ac.btn1, 0, 0)
        layout_graph.addWidget(self.v_ac.graph, 0, 1)
        
        layout_graph.addWidget(self.a_ac.btn1, 1, 0)
        layout_graph.addWidget(self.a_ac.graph, 1, 1)

        layout_graph.addWidget(self.po.btn1, 2, 0)
        layout_graph.addWidget(self.po.graph, 2, 1)

        layout_graph.addWidget(self.kv_dc.btn1, 3, 0)
        layout_graph.addWidget(self.kv_dc.graph, 3, 1)

        layout_graph.addWidget(self.ma_dc.btn1, 4, 0)
        layout_graph.addWidget(self.ma_dc.graph, 4, 1)

    def displayAlarm(self, tab):
        main_layer = QHBoxLayout()
        tab.setLayout(main_layer)

        grp_alarm = QGroupBox("Graph")
        layout_alarm = QGridLayout()
        grp_alarm.setLayout(layout_alarm)
        main_layer.addWidget(grp_alarm)

        self.e_ovsw = AlarmBlock('OVER VOLTAGE S/W', 0, 0, layout_alarm) 
        self.e_ocsw = AlarmBlock('OVER CURRENT S/W', 0, 4, layout_alarm) 
        
        self.e_ovhw = AlarmBlock('OVER VOLTAGE H/W', 1, 0, layout_alarm) 
        self.e_ochw = AlarmBlock('OVER CURRENT H/W', 1, 4, layout_alarm) 
        
        self.e_rescur = AlarmBlock('OVER RESONANT CURRENT', 2, 0, layout_alarm) 
        self.e_emergency = AlarmBlock('EMERGENCY STOP', 2, 4, layout_alarm) 
        
        self.e_sprate = AlarmBlock('OVER SPARK RATE', 3, 0, layout_alarm) 
        self.e_comerr = AlarmBlock('COMM ERROR', 3, 4, layout_alarm) 

        self.e_heat = AlarmBlock('HEATSINK TEMP OVER', 4, 0, layout_alarm) 
        self.e_curunbal = AlarmBlock('RESONANT CURRENT UNBALANCE', 4, 4, layout_alarm) 

        self.e_cpu = AlarmBlock('CPU DOWN ERROR', 5, 0, layout_alarm) 
        self.e_under = AlarmBlock('UNDER VOLTAGE', 5, 4, layout_alarm) 

        self.e_falut = AlarmBlock('FAULT SCR', 6, 0, layout_alarm)
        self.e_oil = AlarmBlock('OIL LEVEL LOW', 6, 4, layout_alarm) 

    def displaySetting(self, tab):
        main_layer = QGridLayout()
        tab.setLayout(main_layer)

        grp_first = QGroupBox()
        first_layout = QGridLayout()
        grp_first.setLayout(first_layout)

        self.set_po = DataGroupBlock(self, "SET POWER", 1, 0, "KW", first_layout, 0)
        self.set_pv = DataGroupBlock(self, "SET V BASE", 2, 0, "kV", first_layout, 1)
        self.set_pi = DataGroupBlock(self, "SET I BASE", 3, 0, "mA", first_layout, 2)
        self.set_spark = DataGroupBlock(self, "SET SPARK RATE", 4, 0, "S/min", first_layout, 3)
        self.set_quench = DataGroupBlock(self, "SET QUENCH", 5, 0, "ms", first_layout, 4)
        self.set_voltimedelay = DataGroupBlock(self, "UNDER VOLT TIME DELAY", 9, 0, "sec", first_layout, 5)
        self.set_vollvl = DataGroupBlock(self, "REDUCE VOLT LEVEL", 27, 9.00, "ms", first_layout, 6)

        grp_second = QGroupBox()
        second_layout = QGridLayout()
        grp_second.setLayout(second_layout)

        self.set_unvol = DataGroupBlock(self, "SET UNDER VOLT LV", 8, 0, "kV", second_layout, 0)
        self.set_setback = DataGroupBlock(self, "SET SETBACK", 6, 0, "%", second_layout, 1)
        self.set_imode = DataGroupBlock(self, "SET IE ON/OFF", 10, 0, "KW", second_layout, 2)
        self.set_itime = DataGroupBlock(self, "SET ON TIME", 11, 0, "ms", second_layout, 3)
        self.set_offtime = DataGroupBlock(self, "SET OFF TIME", 12, 0, "ms", second_layout, 4)
        self.set_minlvl = DataGroupBlock(self, "REGISTOR MIN LEVEL", 29, 0, "kΩ", second_layout, 5)
        self.set_restarttime = DataGroupBlock(self, "RESISTOR RESTART TIME", 1, 1, "sec", second_layout, 6)

        grp_third = QGroupBox()
        third_layout = QGridLayout()
        grp_third.setLayout(third_layout)

        self.set_po = DataGroupBlock(self, "SET UNDER VOLT LV", 1, 0, "kV", third_layout, 0)
        self.set_po = DataGroupBlock(self, "SET UNDER VOLT LV", 1, 0, "kV", third_layout, 1)

        main_layer.addWidget(grp_first, 0, 0)
        main_layer.addWidget(grp_second, 0, 1)
        main_layer.addWidget(grp_third, 1, 0, 1, 2)

    def drawBarChart(self, dt):
        grp = QGroupBox("")
        layout = QVBoxLayout()
        grp.setLayout(layout)

        layout.addWidget(dt.btn)
        layout.addWidget(dt.pw)
        layout.addWidget(dt.label)
        return grp

    # BIT 연산 
    def bit_check(self, x):
        data = []
        for idx in range(16):
            if (x & (1<<idx)):
                data.append(idx)
        return data


    def changeAlarm(self, data):
        self.e_ovsw.resetAlarm()
        self.e_ocsw.resetAlarm()
        self.e_ovhw.resetAlarm()
        self.e_ochw.resetAlarm()
        self.e_rescur.resetAlarm()
        self.e_emergency.resetAlarm()
        self.e_sprate.resetAlarm()
        self.e_comerr.resetAlarm()
        self.e_heat.resetAlarm()
        self.e_curunbal.resetAlarm()
        self.e_cpu.resetAlarm()
        self.e_under.resetAlarm()
        self.e_falut.resetAlarm()
        self.e_oil.resetAlarm()

        if 0 in data:
            self.e_ovsw.turnRed()
        if 1 in data:
            self.e_ocsw.turnRed()
        if 2 in data:
            self.e_ovhw.turnRed()
        if 3 in data:
            self.e_ochw.turnRed()
        if 4 in data:
            self.e_rescur.turnRed()
        if 5 in data:
            self.e_emergency.turnRed()
        if 6 in data:
            self.e_sprate.turnRed()
        if 7 in data:
            self.e_comerr.turnRed()
        if 8 in data:
            self.e_heat.turnRed()
        if 9 in data:
            self.e_curunbal.turnRed()
        if 10 in data:
            self.e_cpu.turnRed()
        if 11 in data:
            self.e_under.turnRed()
        if 12 in data:
            self.e_falut.turnRed()
        if 13 in data:
            self.e_oil.turnRed()


    def changeGraph(self):
        from random import randint
        self.countN += 1
        self.lbl_count.setText(str(self.countN))

        val = randint(1, 10) * 100
        self.v_ac.setY(self.countN, val)
        self.v_ac.reDraw(val)
        self.d_vac.setValue(val)
        self.d_vac.update()

        val = randint(1, 20) * 100
        self.a_ac.setY(self.countN, val)
        self.a_ac.reDraw(val)
        self.d_aac.setValue(val)
        self.d_aac.update()

        val = randint(1, 10) * 100
        self.po.setY(self.countN, val)
        self.po.reDraw(val)
        self.d_po.setValue(val)
        self.d_po.update()

        val = randint(1, 20) * 100
        self.kv_dc.setY(self.countN, val)
        self.kv_dc.reDraw(val)
        self.d_kvdc.setValue(val)
        self.d_kvdc.update()

        val = randint(1, 10) * 100
        self.ma_dc.setY(self.countN, val)
     
        self.ma_dc.reDraw(val)
        self.d_madc.setValue(val)
        self.d_madc.update()

        from random import choice
        vals = [0, 1, 2, 4, 10, 17, 32, 64, 128, 256, 258, 512, 1024, 2048, 4096]
        val = choice(vals)
        data = self.bit_check(val)

        self.changeAlarm(data)

        if self.countN % 5 == 2:
            self.v_ac.drawPlot()
            self.a_ac.drawPlot()
            self.po.drawPlot()
            self.kv_dc.drawPlot()
            self.ma_dc.drawPlot()

            self.mod1.randomData()
            self.mod2.randomData()
            self.mod3.randomData()
            # self.mod4.randomData()
            # self.mod5.randomData()

            num = randint(1, 100)
            if num >= 50:
                print("btn_vlim alarm")
                self.btn_vlim.setStyleSheet(self.btn_alarm)
                self.btn_ilim.setStyleSheet(self.btn_alarm)
            else:
                self.btn_vlim.setStyleSheet(self.btn_default)
                self.btn_ilim.setStyleSheet(self.btn_default)


    def settingValue(self, address, value):
        if not self.client:
            self.setting_generator()
        else:
            print(self.client, address, value)

    # 전원 장치와 송신을 위한 설정
    def setting_generator(self):
        if self.com_open_flag == False:
            Dialog = QDialog()
            self.com_open_flag == True
            dialog = SettingWin(Dialog)
            dialog.show()
            response = dialog.exec_()

            # OK 를 하면 설정 값을 읽어와서 통신을 한다.
            if response == QDialog.Accepted:
                self.gen_port = dialog.gen_port
                self.com_speed = dialog.com_speed
                self.com_data = dialog.com_data
                self.com_parity = dialog.com_parity
                self.com_stop = dialog.com_stop
                self.com_open_flag = False
                self.client = device.make_connect(
                    port=self.gen_port, ptype='rtu',
                    speed=self.com_speed, bytesize=self.com_data, 
                    parity=self.com_parity, stopbits=self.com_stop
                )

                if self.client:
                    self.lbl_com_gen.setText(self.gen_port)
                    self.btn_com_gen.hide()

                    self.timer.setInterval(TPERIOD)
                    self.timer.timeout.connect(self.changeGraph)
                    self.timer.start()
                    
        else:
            print("Open Dialog")


if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    ex = EpcMonitoring()
    sys.exit(app.exec_())

