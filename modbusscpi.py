#!/usr/bin/env python
# coding: utf-8

# 예제 내용
# * 기본 위젯을 사용하여 기본 창을 생성
# * 다양한 레이아웃 위젯 사용
import os
import re
import sys
import serial

from PyQt5.QtWidgets import QWidget, QLabel, QPushButton, QDesktopWidget, QMainWindow
from PyQt5.QtWidgets import QGroupBox, QVBoxLayout, QHBoxLayout, QGridLayout, QSpinBox, QCheckBox
from PyQt5.QtWidgets import QApplication, QDialog,  QDialogButtonBox, QLineEdit, QLCDNumber
from PyQt5.QtWidgets import  QTableWidget, QHeaderView, QAbstractItemView, QTableWidgetItem
from PyQt5 import QtCore
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap
import pyqtgraph as pyGraph

# from modbus import semes as semes
from pybus import scpi as scpi
from pybus import rtu486 as modbus
from pybus.setup import SettingWin, RTUManualWin, SCPIManualWin, SEMESManualWin

GRAPH = 1
DATA = 2
DATA_PERIOD = 1000

RTU = 'RTU'
SCPI = 'SCPI'
SEMES = 'SEMES'
TCP = 'TCP'

FORWARD = 0 
REVERSE = 1 

import logging
FORMAT = ('%(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.INFO)

def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)


class GBlueLabel(QLineEdit):
    def __init__(self, val, *args, **kwargs):
        super(GBlueLabel, self).__init__(*args, **kwargs)
        self.setAlignment(Qt.AlignLeft)
        self.setStyleSheet("background-color: #82E0AA; font-size: 14px")
        self.setText(str(val))


class QTableWidgetSort(QTableWidgetItem):
    def __init__(self, text, sortKey):
        #call custom constructor with UserType item type
        QTableWidgetItem.__init__(self, text, QTableWidgetItem.UserType)
        self.sortKey = sortKey

    def __lt__(self, other):
        return self.sortKey < other.sortKey


class WarningDialog(QDialog):
    def __init__(self, message):
        super().__init__()
        self.message = message
        self.setupUI()

    def setupUI(self):
        self.setGeometry(100, 100, 400, 200)

        self.setWindowTitle("Warning !!")
        layout = QVBoxLayout()
        self.setLayout(layout)
        self.label = QLabel(self.message)
        layout.addWidget(self.label)

        self.buttonBox = QDialogButtonBox()
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        layout.addWidget(self.buttonBox)

        self.buttonBox.accepted.connect(self.on_accepted)
        self.buttonBox.rejected.connect(self.reject)

    def on_accepted(self):
        print("WarningDialog :: OK")
        self.accept()


class WriteFunctionRTU:
    grayfont = "color: #222;"
    bluefont = 'margin: -1px; padding: 0px; color: blue'
    yellowfont = "color: #FFC300;font-weight: bold;"
    btncolor = "QPushButton{font-size: 12pt; font-weight: bold; color: blue}"

    def __init__(self, parent, name, add, val):
        self.name = name
        self.parent = parent

        self.edit_address = QSpinBox()
        self.edit_address.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self.edit_address.setStyleSheet(WriteFunctionRTU.bluefont)
        self.edit_address.setRange(0, 9000)
        self.edit_address.setValue(add)

        self.edit_val = QSpinBox()
        self.edit_val.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self.edit_val.setStyleSheet(WriteFunctionRTU.bluefont)
        self.edit_val.setRange(-5000, 50000)
        self.edit_val.setValue(val)

        self.btn_send = QPushButton("Send")
        self.btn_send.setEnabled(False)
        self.btn_send.setStyleSheet(WriteFunctionRTU.btncolor)
        self.btn_send.clicked.connect(self.send_value)

    def display(self):
        group = QGroupBox(self.name)
        layout = QHBoxLayout()
        group.setLayout(layout)

        lbl_address = QLabel("Address :")
        layout.addWidget(lbl_address)
        layout.addWidget(self.edit_address)

        lbl_val = QLabel("Value :")
        layout.addWidget(lbl_val)
        layout.addWidget(self.edit_val)
        layout.addWidget(self.btn_send)
        
        return group

    def send_value(self):
        if self.parent.client:
            address = self.edit_address.value()
            value = self.edit_val.value()
            modbus.write_registers(self.parent.client, address, value, self.parent.ptype)
        else:
            dialog = WarningDialog("NO PORT CONNECTION")
            dialog.show()
            response = dialog.exec_()

    def send_enable(self):
        self.btn_send.setEnabled(True)


class ReadFunctionRTU:
    grayfont = "color: #222;"
    bluefont = 'margin: -1px; padding: 0px; color: blue'
    btnblue = "QPushButton{font-size: 12pt; font-weight: bold; color: blue}"
    btnred = "QPushButton{font-size: 12pt; font-weight: bold; color: red}"

    def __init__(self, parent, name):
        self.name = name
        self.parent = parent
        self.indexs = []
        self.data1 = []
        self.data2 = []
        self.data3 = []
        self.data4 = []
        self.channelnum = self.parent.ch_ptype

        self.dtimer = QtCore.QTimer()
        self.toogletimer = QtCore.QTimer()
        self.time = 0
        self.toggle = FORWARD


        self.edit_address = QSpinBox()
        self.edit_address.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self.edit_address.setStyleSheet(ReadFunctionRTU.bluefont)
        self.edit_address.setRange(0, 9999)
        self.edit_address.setValue(5)

        self.edit_count = QSpinBox()
        self.edit_count.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self.edit_count.setStyleSheet(ReadFunctionRTU.bluefont)
        self.edit_count.setRange(0, 20)
        self.edit_count.setValue(2)

        self.btn_send = QPushButton("Read")
        self.btn_send.setEnabled(False)
        self.btn_send.setStyleSheet(ReadFunctionRTU.btnblue)
        self.btn_send.clicked.connect(self.start_data)

        self.btn_stop = QPushButton("Stop")
        self.btn_stop.setStyleSheet(ReadFunctionRTU.btnred)
        self.btn_stop.clicked.connect(self.stop_data)

        # Toggle 기능 
        self.auto_toggle = QCheckBox("Auto Toggle")
        self.toogle_time = QLineEdit("10")
        self.label2 = QLabel("초 (토글 시간)")


        self.tableMsg = QTableWidget()
        self.tableMsg.setColumnCount(3)
        self.tableMsg.setEditTriggers(QAbstractItemView.NoEditTriggers)
        column_headers = ['data#1', 'data#2', 'date#3']
        self.tableMsg.setHorizontalHeaderLabels(column_headers)
        header = self.tableMsg.horizontalHeader()       
        header.setSectionResizeMode(0, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(1, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(2, QHeaderView.Stretch)
        self.tableMsg.resizeColumnsToContents()
        
        # Graph #1
        self.graphWidget1 = pyGraph.PlotWidget()
        self.graphWidget1.setBackground('w')
        self.graphWidget1.setMouseEnabled(x=False, y=False)

        self.pen1 = pyGraph.mkPen(width=2, color=(255, 0, 0))
        self.graphWidget1.plot(self.indexs, self.data1, pen=self.pen1, name="data1")

        # Graph #2
        self.graphWidget2 = pyGraph.PlotWidget()
        self.graphWidget2.setBackground('w')
        self.graphWidget2.setMouseEnabled(x=False, y=False)
        self.pen2 = pyGraph.mkPen(width=2, color=(0, 255, 0))
        self.graphWidget2.plot(self.indexs, self.data2, pen=self.pen2, name="data2")

        # Graph #3
        self.graphWidget3 = pyGraph.PlotWidget()
        self.graphWidget3.setBackground('w')
        self.graphWidget3.setMouseEnabled(x=False, y=False)
        self.pen3 = pyGraph.mkPen(width=2, color=(0, 0, 255))
        self.graphWidget3.plot(self.indexs, self.data3, pen=self.pen3, name="data3")

        # Graph #4
        self.graphWidget4 = pyGraph.PlotWidget()
        self.graphWidget4.setBackground('w')
        self.graphWidget4.setMouseEnabled(x=False, y=False)
        self.pen4 = pyGraph.mkPen(width=2, color=(204, 0, 153))
        self.graphWidget4.plot(self.indexs, self.data4, pen=self.pen4, name="data4")


    def display(self):
        group = QGroupBox(self.name)
        layout = QGridLayout()
        group.setLayout(layout)

        lbl_address = QLabel("Start Address :")
        lbl_val = QLabel("Count(데이터 갯수) :")

        layout.addWidget(lbl_address, 0, 0)
        layout.addWidget(self.edit_address, 0, 1)

        layout.addWidget(lbl_val, 0, 2)
        layout.addWidget(self.edit_count, 0, 3)
        layout.addWidget(self.btn_send, 0, 4)
        layout.addWidget(self.btn_stop, 0, 5)

        layout.addWidget(self.auto_toggle, 1, 0, 1, 2)
        layout.addWidget(self.toogle_time, 1, 2, 1, 2)
        layout.addWidget(self.label2, 1, 4, 1, 2)


        layout.addWidget(self.tableMsg, 2, 0, 1, 6)

        layout.addWidget(self.graphWidget1, 3, 0, 5, 6)
        layout.addWidget(self.graphWidget2, 8, 0, 5, 6)
        layout.addWidget(self.graphWidget3, 13, 0, 5, 6)
        layout.addWidget(self.graphWidget4, 18, 0, 5, 6)
        
        return group

    def start_data(self):
        self.channelnum = self.parent.ch_ptype
        logging.info(f"start_data :: self.channelnum :: {self.channelnum}")

        self.data1 = []
        self.data2 = []
        self.data3 = []
        self.data4 = []

        self.graphWidget1.clear()
        self.graphWidget2.clear()
        self.graphWidget3.clear()
        self.graphWidget4.clear()

        count = self.edit_count.value()
        self.tableMsg.setColumnCount(count)

        column_headers = []
        for idx in range(count):
            column_headers.append(f"data#{idx+1}")

        logging.info(f"start_data :: {column_headers}")
        self.tableMsg.setHorizontalHeaderLabels(column_headers)
        self.tableMsg.resizeColumnsToContents()

        self.dtimer.setInterval(DATA_PERIOD)
        self.dtimer.timeout.connect(self.get_data)
        self.dtimer.start()

        if self.channelnum:
            modbus.write_registers(self.parent.client, modbus.WRITE_TOGGLE, FORWARD, RTU)
        else:
            modbus.write_registers(self.parent.client, modbus.WRITE_TOGGLE_1U, FORWARD, RTU)

        if self.auto_toggle.isChecked():
            toogle_time = int(self.toogle_time.text())
            logging.info(f"toogle_time :: {toogle_time}")
            TOOGLE_TIME = toogle_time * 1000
            self.toogletimer.setInterval(TOOGLE_TIME)
            self.toogletimer.timeout.connect(self.device_toggle)
            self.toogletimer.start()
        else:
            print("self.auto_toggle.isChecked():")

    def device_toggle(self):
        if self.toggle == FORWARD:
            self.toggle = REVERSE
            if self.channelnum:
                modbus.write_registers(self.parent.client, modbus.WRITE_TOGGLE, REVERSE, RTU)
            else:
                modbus.write_registers(self.parent.client, modbus.WRITE_TOGGLE_1U, REVERSE, RTU)

        else:
            self.toggle = FORWARD
            if self.channelnum:
                modbus.write_registers(self.parent.client, modbus.WRITE_TOGGLE, FORWARD, RTU)
            else:
                modbus.write_registers(self.parent.client, modbus.WRITE_TOGGLE_1U, FORWARD, RTU)
        logging.info(f"modbus_toggle :: {self.toggle}\n\n")


    def get_time(self):
        self.time += 1
        timedisplay = '{:02d}:{:02d}:{:02d}'.format((self.time // 60) // 60, 
                        (self.time // 60) % 60, self.time % 60)
        self.parent.display_time.display(timedisplay)


    def stop_data(self):
        self.dtimer.stop()
        self.dtimer = None 
        self.dtimer = QtCore.QTimer()

        self.toogletimer.stop()
        self.toogletimer = None 
        self.toogletimer = QtCore.QTimer()


    def updateMessage(self, data):
        rowPosition = self.tableMsg.rowCount()
        self.tableMsg.insertRow(rowPosition)

        for idx, dt in enumerate(data):
            self.tableMsg.setItem(rowPosition, idx, QTableWidgetItem(str(dt)))
        
        self.tableMsg.resizeColumnsToContents()
        self.tableMsg.selectRow(rowPosition)


    def get_data(self):
        self.get_time()
        if self.parent.client:
            address = self.edit_address.value()
            count = self.edit_count.value()
            result = modbus.read_data(self.parent.client, address, count, self.parent.ptype)

            if result:
                logging.info(f"get_data :: {result}")
                self.data1.append(result[0])

                if count >= 4:
                    self.data2.append(result[1])
                    self.data3.append(result[2])
                    self.data4.append(result[3])
                elif count == 3:
                    self.data2.append(result[1])
                    self.data3.append(result[2])
                elif count == 2:
                    self.data2.append(result[1])

                self.updateMessage(result)
                self.draw_graph()

        else:
            dialog = WarningDialog("NO PORT CONNECTION")
            dialog.show()
            response = dialog.exec_()


    def draw_graph(self):
        count = self.edit_count.value()
        index1 = list(range(len(self.data1)))
        self.graphWidget1.plot(index1, self.data1, pen=self.pen1, name="data#1")

        if count >= 4:
            index2 = list(range(len(self.data2)))
            self.graphWidget2.plot(index2, self.data2, pen=self.pen2, name="data#2")
            index3 = list(range(len(self.data3)))
            self.graphWidget3.plot(index3, self.data3, pen=self.pen3, name="data#3")
            index4 = list(range(len(self.data4)))
            self.graphWidget4.plot(index4, self.data4, pen=self.pen4, name="data#4")
        elif count == 3:
            index2 = list(range(len(self.data2)))
            self.graphWidget2.plot(index2, self.data2, pen=self.pen2, name="data#2")
            index3 = list(range(len(self.data3)))
            self.graphWidget3.plot(index3, self.data3, pen=self.pen3, name="data#3")
        elif count == 2:
            index2 = list(range(len(self.data2)))
            self.graphWidget2.plot(index2, self.data2, pen=self.pen2, name="data#2")


    def send_enable(self):
        self.btn_send.setEnabled(True)


class WriteFunctionSCPI:
    grayfont = "color: #222;"
    bluefont = 'margin: -1px; padding: 0px; color: blue'
    yellowfont = "color: #FFC300;font-weight: bold;"
    btncolor = "QPushButton{font-size: 12pt; font-weight: bold; color: blue}"

    def __init__(self, parent, ptype, name, example):
        self.name = name
        self.parent = parent
        self.command = QLineEdit(example)
        self.btn_send = QPushButton("Send")
        self.btn_send.setEnabled(False)
        self.btn_send.setStyleSheet(WriteFunctionSCPI.btncolor)

        if ptype == 'SCPI':
            self.btn_send.clicked.connect(self.send_scpi_command)
        else:
            self.btn_send.clicked.connect(self.send_semes_command)

        self.example = GBlueLabel(example)

    def display(self):
        group = QGroupBox(self.name)
        layout = QHBoxLayout()
        group.setLayout(layout)
        lbl_address = QLabel("COMMAND :")
        layout.addWidget(lbl_address)
        layout.addWidget(self.example)
        layout.addWidget(self.command)
        layout.addWidget(self.btn_send)
        
        return group

    def send_semes_command(self):
        logging.info(type(self.parent.client))
        if self.parent.client:
            command = self.command.text()
            mcommand = f"{command}\r".encode()
            logging.info(f"COMMAND :: {mcommand}")
            self.parent.client.write(mcommand)
        else:
            dialog = WarningDialog("NO PORT CONNECTION")
            dialog.show()
            response = dialog.exec_()

    def send_scpi_command(self):
        if self.parent.client:
            command = self.command.text()
            mcommand = f"{command}\r\n".encode()
            logging.info(f"COMMAND :: {mcommand}")
            self.parent.client.write(mcommand)
        else:
            dialog = WarningDialog("NO PORT CONNECTION")
            dialog.show()
            response = dialog.exec_()


    def send_enable(self):
        self.btn_send.setEnabled(True)


class ReadFunctionSCPI:
    grayfont = "color: #222;"
    bluefont = 'margin: -1px; padding: 0px; color: blue'
    btnblue = "QPushButton{font-size: 12pt; font-weight: bold; color: blue}"
    btnred = "QPushButton{font-size: 12pt; font-weight: bold; color: red}"

    def __init__(self, parent, name):
        self.name = name
        self.parent = parent
        self.indexs = []
        self.data1 = []
        self.data2 = []

        self.dtimer = QtCore.QTimer()
        self.toogletimer = QtCore.QTimer()
        self.time = 0
        self.toggle = FORWARD

        self.command = QLineEdit("Meas:volt? (@1,2)")
        self.command.setStyleSheet(ReadFunctionSCPI.bluefont)

        self.btn_send = QPushButton("Read")
        self.btn_send.setEnabled(False)
        self.btn_send.setStyleSheet(ReadFunctionRTU.btnblue)
        self.btn_send.clicked.connect(self.start_data)

        self.btn_stop = QPushButton("Stop")
        self.btn_stop.setStyleSheet(ReadFunctionRTU.btnred)
        self.btn_stop.clicked.connect(self.stop_data)

        # Toggle 기능 
        self.auto_toggle = QCheckBox("Auto Toggle")
        self.toogle_time = QLineEdit("10")
        self.label2 = QLabel("초 (토글 시간)")

        # TABLE
        self.tableMsg = QTableWidget()
        self.tableMsg.setColumnCount(3)
        self.tableMsg.setEditTriggers(QAbstractItemView.NoEditTriggers)
        column_headers = ['data#1', 'data#2', 'date#3']
        self.tableMsg.setHorizontalHeaderLabels(column_headers)
        header = self.tableMsg.horizontalHeader()       
        header.setSectionResizeMode(0, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(1, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(2, QHeaderView.Stretch)
        self.tableMsg.resizeColumnsToContents()
        
        # Graph #1
        self.graphWidget1 = pyGraph.PlotWidget()
        self.graphWidget1.setBackground('w')
        self.graphWidget1.setMouseEnabled(x=False, y=False)

        self.pen1 = pyGraph.mkPen(width=2, color=(255, 0, 0))
        self.graphWidget1.plot(self.indexs, self.data1, pen=self.pen1, name="data1")

        # Graph #2
        self.graphWidget2 = pyGraph.PlotWidget()
        self.graphWidget2.setBackground('w')
        self.graphWidget2.setMouseEnabled(x=False, y=False)
        self.pen2 = pyGraph.mkPen(width=2, color=(0, 255, 0))
        self.graphWidget2.plot(self.indexs, self.data2, pen=self.pen2, name="data2")


    def display(self):
        group = QGroupBox(self.name)
        layout = QGridLayout()
        group.setLayout(layout)

        lbl_command = QLabel("READ COMMAND :")
        layout.addWidget(lbl_command, 0, 0)
        layout.addWidget(self.command, 0, 1)

        ex1 = GBlueLabel("Meas:volt? (@1,2)")
        ex2 = GBlueLabel("Meas:curr? (@1,2)")
        ex3 = GBlueLabel("Meas:leak? (@1,2)")

        layout.addWidget(ex1, 0, 2)
        layout.addWidget(ex2, 0, 3)
        layout.addWidget(ex3, 0, 4)
        layout.addWidget(self.btn_send, 0, 5)
        layout.addWidget(self.btn_stop, 0, 6)

        layout.addWidget(self.auto_toggle, 1, 0, 1, 2)
        layout.addWidget(self.toogle_time, 1, 2, 1, 2)
        layout.addWidget(self.label2, 1, 4, 1, 2)

        layout.addWidget(self.tableMsg, 2, 0, 1, 7)
        layout.addWidget(self.graphWidget1, 3, 0, 8, 7)
        layout.addWidget(self.graphWidget2, 11, 0, 8, 7)
        
        return group


    def get_time(self):
        self.time += 1
        timedisplay = '{:02d}:{:02d}:{:02d}'.format((self.time // 60) // 60, 
                        (self.time // 60) % 60, self.time % 60)
        self.parent.display_time.display(timedisplay)


    def start_data(self):
        self.data1 = []
        self.data2 = []

        self.graphWidget1.clear()
        self.graphWidget2.clear()

        count = 2
        self.tableMsg.setColumnCount(count)

        column_headers = []
        for idx in range(count):
            column_headers.append(f"data#{idx+1}")

        self.tableMsg.setHorizontalHeaderLabels(column_headers)
        self.tableMsg.resizeColumnsToContents()

        self.dtimer.setInterval(DATA_PERIOD)
        self.dtimer.timeout.connect(self.get_data_scpi)
        self.dtimer.start()

        if self.auto_toggle.isChecked():
            toogle_time = int(self.toogle_time.text())
            logging.info(f"toogle_time :: {toogle_time}")
            TOOGLE_TIME = toogle_time * 1000
            self.toogletimer.setInterval(TOOGLE_TIME)
            self.toogletimer.timeout.connect(self.device_toggle)
            self.toogletimer.start()
        else:
            print("self.auto_toggle.isChecked():")


    def device_toggle(self):
        if self.toggle == FORWARD:
            self.toggle = REVERSE
            command = "TOGGLE OFF"
            mcommand = f"{command}\r\n".encode()

        else:
            self.toggle = FORWARD
            command = "TOGGLE ON"
            mcommand = f"{command}\r\n".encode()
        self.parent.client.write(mcommand)
        logging.info(f"device_toggle :: {self.toggle} :: {mcommand}\n\n")


    def stop_data(self):
        self.dtimer.stop()
        self.dtimer = None 
        self.dtimer = QtCore.QTimer()

        self.toogletimer.stop()
        self.toogletimer = None 
        self.toogletimer = QtCore.QTimer()


    def updateMessage(self, data):
        rowPosition = self.tableMsg.rowCount()
        self.tableMsg.insertRow(rowPosition)

        for idx, dt in enumerate(data):
            self.tableMsg.setItem(rowPosition, idx, QTableWidgetItem(str(dt)))
        
        self.tableMsg.resizeColumnsToContents()
        self.tableMsg.selectRow(rowPosition)


    def scpi_get_measure_2ch(self, client, command):
        client.write(command)
        while True:
            try:
                feedback = client.readline()
                if feedback:
                    bitestr = feedback.decode('utf-8')
                    try:
                        return re.findall('[-0-9]+', str(bitestr))
                    except Exception as e:
                        return None
                    break
            except KeyboardInterrupt:
                feedback = None
                break
        return None


    def get_data_scpi(self):
        self.get_time()
        if self.parent.client:
            command = self.command.text()
            mcommand = f"{command}\r\n".encode()
            result = self.scpi_get_measure_2ch(self.parent.client, mcommand)

            if result:
                logging.info(f"get_data_scpi :: {mcommand} :: {result}")
                self.data1.append(int(result[0]))

                if len(result) >= 2:
                    self.data2.append(int(result[1]))

                self.updateMessage(result)
                self.draw_graph()


    def draw_graph(self):
        index1 = list(range(len(self.data1)))
        self.graphWidget1.plot(index1, self.data1, pen=self.pen1, name="data#1")

        if self.data2:
            index2 = list(range(len(self.data2)))
            self.graphWidget2.plot(index2, self.data2, pen=self.pen2, name="data#2")


    def send_enable(self):
        self.btn_send.setEnabled(True)


class ReadFunctionSEMES:
    grayfont = "color: #222;"
    bluefont = 'margin: -1px; padding: 0px; color: blue'
    btnblue = "QPushButton{font-size: 12pt; font-weight: bold; color: blue}"
    btnred = "QPushButton{font-size: 12pt; font-weight: bold; color: red}"

    def __init__(self, parent, name):
        self.name = name
        self.parent = parent
        self.indexs = []
        self.data1 = []
        self.data2 = []

        self.dtimer = QtCore.QTimer()
        self.toogletimer = QtCore.QTimer()
        self.time = 0
        self.toggle = FORWARD

        self.command = QLineEdit("RV")
        self.command.setStyleSheet(ReadFunctionSCPI.bluefont)

        self.btn_send = QPushButton("Read")
        self.btn_send.setEnabled(False)
        self.btn_send.setStyleSheet(ReadFunctionRTU.btnblue)
        self.btn_send.clicked.connect(self.start_data)

        self.btn_stop = QPushButton("Stop")
        self.btn_stop.setStyleSheet(ReadFunctionRTU.btnred)
        self.btn_stop.clicked.connect(self.stop_data)

        # Toggle 기능 
        self.auto_toggle = QCheckBox("Auto Toggle")
        self.toogle_time = QLineEdit("10")
        self.label2 = QLabel("초 (토글 시간)")

        self.tableMsg = QTableWidget()
        self.tableMsg.setColumnCount(3)
        self.tableMsg.setEditTriggers(QAbstractItemView.NoEditTriggers)
        column_headers = ['data#1', 'data#2', 'date#3']
        self.tableMsg.setHorizontalHeaderLabels(column_headers)
        header = self.tableMsg.horizontalHeader()       
        header.setSectionResizeMode(0, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(1, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(2, QHeaderView.Stretch)
        self.tableMsg.resizeColumnsToContents()
        
        # Graph #1
        self.graphWidget1 = pyGraph.PlotWidget()
        self.graphWidget1.setBackground('w')
        self.graphWidget1.setMouseEnabled(x=False, y=False)

        self.pen1 = pyGraph.mkPen(width=2, color=(255, 0, 0))
        self.graphWidget1.plot(self.indexs, self.data1, pen=self.pen1, name="data1")

        # Graph #2
        self.graphWidget2 = pyGraph.PlotWidget()
        self.graphWidget2.setBackground('w')
        self.graphWidget2.setMouseEnabled(x=False, y=False)
        self.pen2 = pyGraph.mkPen(width=2, color=(0, 255, 0))
        self.graphWidget2.plot(self.indexs, self.data2, pen=self.pen2, name="data2")


    def display(self):
        group = QGroupBox(self.name)
        layout = QGridLayout()
        group.setLayout(layout)

        lbl_command = QLabel("READ COMMAND :")
        layout.addWidget(lbl_command, 0, 0)
        layout.addWidget(self.command, 0, 1)

        ex1 = GBlueLabel("RV")
        ex2 = GBlueLabel("R+")
        ex3 = GBlueLabel("R-")

        layout.addWidget(ex1, 0, 2)
        layout.addWidget(ex2, 0, 3)
        layout.addWidget(ex3, 0, 4)
        layout.addWidget(self.btn_send, 0, 5)
        layout.addWidget(self.btn_stop, 0, 6)

        layout.addWidget(self.auto_toggle, 1, 0, 1, 2)
        layout.addWidget(self.toogle_time, 1, 2, 1, 2)
        layout.addWidget(self.label2, 1, 4, 1, 2)

        layout.addWidget(self.tableMsg, 2, 0, 1, 7)
        layout.addWidget(self.graphWidget1, 3, 0, 8, 7)
        layout.addWidget(self.graphWidget2, 11, 0, 8, 7)
        return group


    def get_time(self):
        self.time += 1
        timedisplay = '{:02d}:{:02d}:{:02d}'.format((self.time // 60) // 60, 
                        (self.time // 60) % 60, self.time % 60)
        self.parent.display_time.display(timedisplay)


    def start_data(self):
        self.data1 = []
        self.data2 = []

        self.graphWidget1.clear()
        self.graphWidget2.clear()

        count = 2
        self.tableMsg.setColumnCount(count)

        column_headers = []
        for idx in range(count):
            column_headers.append(f"data#{idx+1}")

        self.tableMsg.setHorizontalHeaderLabels(column_headers)
        self.tableMsg.resizeColumnsToContents()

        self.dtimer.setInterval(DATA_PERIOD)
        self.dtimer.timeout.connect(self.get_data_semes)
        self.dtimer.start()

        if self.auto_toggle.isChecked():
            toogle_time = int(self.toogle_time.text())
            logging.info(f"toogle_time :: {toogle_time}")
            TOOGLE_TIME = toogle_time * 1000
            self.toogletimer.setInterval(TOOGLE_TIME)
            self.toogletimer.timeout.connect(self.device_toggle)
            self.toogletimer.start()
        else:
            print("self.auto_toggle.isChecked():")


    def device_toggle(self):
        if self.toggle == FORWARD:
            self.toggle = REVERSE
            command = "V-"
            mcommand = f"{command}\r".encode()

        else:
            self.toggle = FORWARD
            command = "V+"
            mcommand = f"{command}\r".encode()
        self.parent.client.write(mcommand)
        logging.info(f"device_toggle :: {self.toggle} :: {mcommand}\n\n")


    def stop_data(self):
        self.dtimer.stop()
        self.dtimer = None 
        self.dtimer = QtCore.QTimer()

        self.toogletimer.stop()
        self.toogletimer = None 
        self.toogletimer = QtCore.QTimer()


    def updateMessage(self, data):
        rowPosition = self.tableMsg.rowCount()
        self.tableMsg.insertRow(rowPosition)

        for idx, dt in enumerate(data):
            self.tableMsg.setItem(rowPosition, idx, QTableWidgetItem(str(dt)))
        
        self.tableMsg.resizeColumnsToContents()
        self.tableMsg.selectRow(rowPosition)


    def get_data_semes(self):
        self.get_time()
        if self.parent.client:
            command = self.command.text()
            mcommand = f"{command}\r".encode()
            result = self.semes_get_measure(self.parent.client, mcommand)

            if result:
                logging.info(f"get_data_semes :: {mcommand} :: {result}")
                self.data1.append(int(result[0]))
                
                if len(result) >= 2:
                    self.data2.append(int(result[1]))

                self.updateMessage(result)
                self.draw_graph()


    def semes_get_measure(self, client, command):
        client.write(command)
        while True:
            try:
                feedback = client.readline()
                if feedback:
                    bitestr = feedback.decode('utf-8')
                    try:
                        return re.findall('[-0-9]+', str(bitestr))
                    except Exception as e:
                        return None
                    break
            except KeyboardInterrupt:
                feedback = None
                break
        return None
    

    def draw_graph(self):
        index1 = list(range(len(self.data1)))
        self.graphWidget1.plot(index1, self.data1, pen=self.pen1, name="data#1")

        if self.data2:
            index2 = list(range(len(self.data2)))
            self.graphWidget2.plot(index2, self.data2, pen=self.pen2, name="data#2")


    def send_enable(self):
        self.btn_send.setEnabled(True)


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()

        self.stimer = QtCore.QTimer()
        self.dtimer = QtCore.QTimer()

        self.com_setting_flag = False
        self.com_open_flag = False
        self.run_flag = False
        self.ch_ptype = False

        self.client = None
        # self.client = 1

        self.ptype = RTU
        self.time = 0

        self.initUI()

    def eventFilter(self, watched, event):
        pass

    
    def displayVerticalLayout(self, parent_layout, glabel, btn, second=None, third=None):
        grp_sub = QGroupBox(glabel)
        layout_sub = QVBoxLayout()
        grp_sub.setLayout(layout_sub)
        layout_sub.addWidget(btn)
        if second:
            layout_sub.addWidget(second)
        if third:
            layout_sub.addWidget(third)
        parent_layout.addWidget(grp_sub)


    def initUI(self):
        self.setWindowTitle("PSTEK MODBUS MONITORING PROGRAM")
        # self.setMinimumSize(1024, 800) 

        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width(), screensize.height()) 

        mainwidget = QWidget()                # 위젯의 인스턴스 생성만으로도 QMainWindow에 붙는다.
        self.setCentralWidget(mainwidget)

        ## Main Layout 설정
        self.main_layer = QVBoxLayout()
        mainwidget.setLayout(self.main_layer)

        bluefont = "color: #0000ff;"
        redfont = "color: #ff0000;"
        
        layout_setup = QHBoxLayout()
        self.main_layer.addLayout(layout_setup)
        
        ## replace #1st ###############
        # 전원 장치 연결
        self.btn_com_gen = QPushButton("CONNECT")
        self.btn_com_gen.clicked.connect(self.setting_device)
        self.label_gen_port = QLabel("N/A")
        self.label_ptype = QLabel("N/A")
        self.displayVerticalLayout(layout_setup, "Connect(485/ModBus)", 
                self.btn_com_gen, self.label_gen_port, self.label_ptype)

        ## RTU
        self.grp_rtu = QGroupBox("Function 16 : WRITE WORD")
        layout_rtu = QGridLayout()
        self.grp_rtu.setLayout(layout_rtu)
        self.write1 = WriteFunctionRTU(self, 'Write 1', 0, 0)
        self.write2 = WriteFunctionRTU(self, 'Write 2', 0, 2)
        self.write3 = WriteFunctionRTU(self, 'Write 3', 1, 2500)
        self.write4 = WriteFunctionRTU(self, 'Write 4', 20, 0)

        layout_rtu.addWidget(self.write1.display(), 0, 0)
        layout_rtu.addWidget(self.write2.display(), 0, 1)
        layout_rtu.addWidget(self.write3.display(), 1, 0)
        layout_rtu.addWidget(self.write4.display(), 1, 1)
        layout_setup.addWidget(self.grp_rtu)

        ## SCPI 
        self.grp_scpi = QGroupBox("SCPI : WRITE COMMAND")
        layout_scpi = QGridLayout()
        self.grp_scpi.setLayout(layout_scpi)
        self.comm_scpi_1 = WriteFunctionSCPI(self, self.ptype, 'Comm 1', 'OUTPUT ON')
        self.comm_scpi_2 = WriteFunctionSCPI(self, self.ptype, 'Comm 2', 'OUTPUT OFF')
        self.comm_scpi_3 = WriteFunctionSCPI(self, self.ptype, 'Comm 3', 'VOLT 2000,-2000')
        self.comm_scpi_4 = WriteFunctionSCPI(self, self.ptype, 'Comm 4', 'CONF:VOLT 2000,-2000')

        layout_scpi.addWidget(self.comm_scpi_1.display(), 0, 0)
        layout_scpi.addWidget(self.comm_scpi_2.display(), 0, 1)
        layout_scpi.addWidget(self.comm_scpi_3.display(), 1, 0)
        layout_scpi.addWidget(self.comm_scpi_4.display(), 1, 1)

        layout_setup.addWidget(self.grp_scpi)
        self.grp_scpi.hide()

        ## SEMES 
        self.grp_semes = QGroupBox("SEMES : WRITE COMMAND")
        layout_semes = QGridLayout()
        self.grp_semes.setLayout(layout_semes)
        self.comm_semes_1 = WriteFunctionSCPI(self, self.ptype, 'Comm 1', 'EV')
        self.comm_semes_2 = WriteFunctionSCPI(self, self.ptype, 'Comm 2', 'DV')
        self.comm_semes_3 = WriteFunctionSCPI(self, self.ptype, 'Comm 3', 'SV2000.000000')
        self.comm_semes_4 = WriteFunctionSCPI(self, self.ptype, 'Comm 4', 'SI900.000000')

        layout_semes.addWidget(self.comm_semes_1.display(), 0, 0)
        layout_semes.addWidget(self.comm_semes_2.display(), 0, 1)
        layout_semes.addWidget(self.comm_semes_3.display(), 1, 0)
        layout_semes.addWidget(self.comm_semes_4.display(), 1, 1)

        layout_setup.addWidget(self.grp_semes)
        self.grp_semes.hide()

        ## TIMER
        self.display_time = QLCDNumber()
        self.display_time.setDigitCount(8)
        self.display_time.setStyleSheet('background: white')
        timedisplay = '{:02d}:{:02d}:{:02d}'.format(
                (self.time // 60) // 60, (self.time // 60) % 60, self.time % 60)
        self.display_time.display(timedisplay)
        layout_setup.addWidget(self.display_time)

        ## MANUAL
        self.btn_man_rtu = QPushButton("RTU ADDRESS")
        self.btn_man_rtu.clicked.connect(self.manual_rtu)

        self.btn_man_scpi = QPushButton("SCPI COMMAND")
        self.btn_man_scpi.clicked.connect(self.manual_scpi)

        self.btn_man_semes = QPushButton("SEMES COMMAND")
        self.btn_man_semes.clicked.connect(self.manual_semes)
        self.displayVerticalLayout(layout_setup, "Manual", 
                self.btn_man_rtu, self.btn_man_scpi, self.btn_man_semes)

        # Logo Image
        labelLogo = QLabel("")
        pixmap = QPixmap(resource_path("logo.png"))
        labelLogo.setAlignment(Qt.AlignRight)
        labelLogo.setPixmap(pixmap)
        layout_setup.addWidget(labelLogo)
        self.setup_read_menu()


    def manual_rtu(self):
        Dialog = QDialog()
        self.com_open_flag == True
        dialog = RTUManualWin(Dialog)
        dialog.show()
        response = dialog.exec_() 


    def manual_scpi(self):
        Dialog = QDialog()
        self.com_open_flag == True
        dialog = SCPIManualWin(Dialog)
        dialog.show()
        response = dialog.exec_()  


    def manual_semes(self):
        Dialog = QDialog()
        self.com_open_flag == True
        dialog = SEMESManualWin(Dialog)
        dialog.show()
        response = dialog.exec_()  


    def setup_read_menu(self):
        self.grp_read_rtu = QGroupBox("Function 04 : READ WORD")
        layout_rtu = QVBoxLayout()
        self.grp_read_rtu.setLayout(layout_rtu)
        self.main_layer.addWidget(self.grp_read_rtu)
        self.read_rtu = ReadFunctionRTU(self, 'Read RTU')
        layout_rtu.addWidget(self.read_rtu.display())
        self.main_layer.addWidget(self.grp_read_rtu)

        self.grp_read_scpi = QGroupBox("SCPI : READ COMMAND")
        layout_scpi = QVBoxLayout()
        self.grp_read_scpi.setLayout(layout_scpi)
        self.main_layer.addWidget(self.grp_read_scpi)
        self.read_scpi = ReadFunctionSCPI(self, 'Read SCPI')
        layout_scpi.addWidget(self.read_scpi.display())
        self.main_layer.addWidget(self.grp_read_scpi)
        self.grp_read_scpi.hide()

        self.grp_read_semes = QGroupBox("SEMES : READ COMMAND")
        layout_semes = QVBoxLayout()
        self.grp_read_semes.setLayout(layout_semes)
        self.main_layer.addWidget(self.grp_read_semes)
        self.read_semes = ReadFunctionSEMES(self, 'Read SEMES')
        layout_semes.addWidget(self.read_semes.display())
        self.main_layer.addWidget(self.grp_read_semes)
        self.grp_read_semes.hide()


    # 전원 장치와 송신을 위한 설정
    def setting_device(self):
        if self.com_open_flag == False:
            Dialog = QDialog()
            self.com_open_flag == True
            dialog = SettingWin(Dialog)
            dialog.show()
            response = dialog.exec_()

            # OK 를 하면 설정 값을 읽어와서 통신을 한다.
            if response == QDialog.Accepted:
                if dialog.selected == RTU:
                    gen_port = dialog.gen_port
                    com_speed = dialog.com_speed
                    com_data = dialog.com_data
                    com_parity = dialog.com_parity
                    com_stop = dialog.com_stop
                    self.ch_ptype = dialog.ch_ptype

                    self.com_open_flag = False

                    self.client = modbus.connect_rtu(
                        port=gen_port, ptype='rtu',
                        speed=com_speed, bytesize=com_data, 
                        parity=com_parity, stopbits=com_stop
                    )
                    self.ptype = RTU
                    self.grp_scpi.hide()
                    self.grp_semes.hide()
                    self.grp_rtu.show()
                    self.grp_read_scpi.hide()
                    self.grp_read_semes.hide()
                    self.grp_read_rtu.show()

                elif dialog.selected == SCPI:
                    gen_port = dialog.gen_port
                    com_speed = dialog.com_speed
                    com_data = dialog.com_data
                    com_parity = dialog.com_parity
                    com_stop = dialog.com_stop
                    self.ch_ptype = dialog.ch_ptype
                    self.com_open_flag = False

                    self.client = serial.Serial(
                        port=gen_port,
                        baudrate=com_speed,
                        parity=serial.PARITY_NONE,
                        stopbits=serial.STOPBITS_ONE,
                        bytesize=serial.EIGHTBITS,
                        timeout=2
                    )
                    self.ptype = SCPI
                    self.grp_rtu.hide()
                    self.grp_semes.hide()
                    self.grp_scpi.show()
                    self.grp_read_rtu.hide()
                    self.grp_read_semes.hide()
                    self.grp_read_scpi.show()
                    
                elif dialog.selected == SEMES:
                    gen_port = dialog.gen_port
                    com_speed = dialog.com_speed
                    com_data = dialog.com_data
                    com_parity = dialog.com_parity
                    com_stop = dialog.com_stop
                    self.ch_ptype = dialog.ch_ptype
                    self.com_open_flag = False

                    self.client = serial.Serial(
                        port=gen_port,
                        baudrate=com_speed,
                        parity=serial.PARITY_NONE,
                        stopbits=serial.STOPBITS_ONE,
                        bytesize=serial.EIGHTBITS,
                        timeout=2
                    )
                    self.ptype = SEMES
                    self.grp_rtu.hide()
                    self.grp_scpi.hide()
                    self.grp_semes.show()
                    self.grp_read_rtu.hide()
                    self.grp_read_scpi.hide()
                    self.grp_read_semes.show()
                    
                elif dialog.selected == TCP:
                    gen_port = dialog.ipaddress
                    self.ch_ptype = dialog.ch_ptype
                    self.ipport = int(dialog.ipport)
                    self.client = modbus.connect_tcp(gen_port, self.ipport)

                    self.ptype = TCP
                    self.grp_scpi.hide()
                    self.grp_semes.hide()
                    self.grp_rtu.show()
                    self.grp_read_scpi.hide()
                    self.grp_read_semes.hide()
                    self.grp_read_rtu.show()

                else:
                    gen_port = dialog.gen_port
                    com_speed = dialog.com_speed
                    com_data = dialog.com_data
                    com_parity = dialog.com_parity
                    com_stop = dialog.com_stop
                    self.ch_ptype = dialog.ch_ptype
                    self.com_open_flag = False

                    self.client = modbus.connect_rtu(
                        port=gen_port, ptype='rtu',
                        speed=com_speed, bytesize=com_data, 
                        parity=com_parity, stopbits=com_stop
                    )
                    self.ptype = RTU
                    self.grp_scpi.hide()
                    self.grp_rtu.show()
                    self.grp_read_scpi.hide()
                    self.grp_read_semes.hide()
                    self.grp_read_rtu.show()

                logging.info(f"setting_device :: {self.ptype} :: {self.client}")
                logging.info(f"setting_device :: self.ch_ptype :: {self.ch_ptype}")

                # Graph
                if self.client and self.ptype == RTU:
                    self.label_gen_port.setText(gen_port)
                    self.label_ptype.setText(self.ptype)
                    self.btn_com_gen.hide()

                    self.write1.send_enable()
                    self.write2.send_enable()
                    self.write3.send_enable()
                    self.write4.send_enable()
                    self.read_rtu.send_enable()

                elif self.client and self.ptype == SEMES:
                    self.label_gen_port.setText(gen_port)
                    self.label_ptype.setText(self.ptype)
                    self.btn_com_gen.hide()

                    self.comm_semes_1.send_enable()
                    self.comm_semes_2.send_enable()
                    self.comm_semes_3.send_enable()
                    self.comm_semes_4.send_enable()
                    self.read_semes.send_enable()

                elif self.client and self.ptype == SCPI:
                    self.label_gen_port.setText(gen_port)
                    self.label_ptype.setText(self.ptype)
                    self.btn_com_gen.hide()

                    self.comm_scpi_1.send_enable()
                    self.comm_scpi_2.send_enable()
                    self.comm_scpi_3.send_enable()
                    self.comm_scpi_4.send_enable()
                    self.read_scpi.send_enable()

        else:
            logging.info("Open Dialog")


if __name__ == "__main__":
    app = QApplication(sys.argv)
    form = MainWindow()
    form.show()
    sys.exit(app.exec_())