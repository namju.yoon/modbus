import serial
from binascii import hexlify

def get_comm_port():
    port_list = []
    import serial.tools.list_ports
    ports = serial.tools.list_ports.comports()
    for port, desc, hwid in sorted(ports):
        if desc != 'n/a':
            port_list.append(port)
    return port_list

ports = get_comm_port()
port = ports[0]

serialPort = serial.Serial(
    port=port,
    baudrate=9600,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS,
    timeout=2
)

serialPort.is_open


def feedBack():
    while True:
        try:
            feedback = serialPort.readline()
            if feedback:
                print(feedback)
                break
        except KeyboardInterrupt:
            break

def semes_write(ptype, val=0):
    if ptype == 'VOLTAGE':
        command = f'SV{val}.000000\r'
    elif ptype == 'CURRENT':
        pval = int(val*1000)
        command = f'SI{pval}.000000\r'
    elif ptype == 'RAMPUP':
        command = f'P+{val}\r'
    elif ptype == 'RAMPDOWN':
        command = f'P-{val}\r'
    elif ptype == 'TOGGLEON':
        command = 'D+\r'
    elif ptype == 'TOGGLEOFF':
        command = 'D-\r'
    elif ptype == 'REVERSEON':
        command = 'V+\r'
    elif ptype == 'REVERSEOFF':
        command = 'V-\r'
    elif ptype == 'RUN':
        command = 'EV\r'
    elif ptype == 'STOP':
        command = 'DV\r'
    return command.encode()



def semes_measure(ptype):
    if ptype == 'VOLTAGE':
        command = 'RV\r'
    elif ptype == 'CURRENT+':
        command = 'R+\r'
    elif ptype == 'CURRENT-':
        command = 'R-\r'
    return command.encode()

def semes_get_measure(ptype):
    if ptype == 'VOLTAGE':
        command = 'RV\r'
    elif ptype == 'CURRENT+':
        command = 'R+\r'
    elif ptype == 'CURRENT-':
        command = 'R-\r'
    serialPort.write(command.encode())
    while True:
        try:
            feedback = serialPort.readline()
            if feedback:
                print(feedback)
                break
        except KeyboardInterrupt:
            feedback = None
            break
    if feedback:
        print(bitestr)
        bitestr = feedback.decode('utf-8')
        return int(re.findall('[0-9]+', str(bitestr))[0])
    return 0

    
semes_get_measure('VOLTAGE')



command = semes_write('VOLTAGE', 500)
command
serialPort.write(command)
feedBack()

command = semes_write('VOLTAGE', 2000)
command
serialPort.write(command)
feedBack()



command = semes_write('RUN')
command
serialPort.write(command)
feedBack()

## RUN
command = b'EV\r'
serialPort.write(command)
feedBack()


## STOP
command = b'DV\r'
serialPort.write(command)
feedBack()


command = semes_write('STOP')
command
serialPort.write(command)
feedBack()




command = semes_measure('VOLTAGE')
command
serialPort.write(command)
feedBack()


command = semes_measure('CURRENT+')
command
serialPort.write(command)
feedBack()
