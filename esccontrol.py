#!/usr/bin/env python
# coding: utf-8

# 예제 내용
# * 기본 위젯을 사용하여 기본 창을 생성
# * 다양한 레이아웃 위젯 사용
import os
import sys

from PyQt5.QtWidgets import QWidget, QLabel, QPushButton, QDesktopWidget, QMainWindow, QRadioButton
from PyQt5.QtWidgets import QGroupBox, QVBoxLayout, QHBoxLayout, QGridLayout
from PyQt5.QtWidgets import QApplication, QDialog, QStatusBar
from PyQt5.QtCore import QDate, Qt, pyqtSlot
from PyQt5.QtGui import QPixmap
from PyQt5 import QtCore

import pyqtgraph as pyGraph

from pybus import rtu486 as modbus
from pybus.setup import SettingPort
from pybus.block import OffsetGain, VolReadWrite, RadioField, RadioThree

GRAPH = 1
DATA = 2
ZERO = 0
DATA_PERIOD = 1000

RTU = 'RTU'

FORWARD = 0 
REVERSE = 1 

import logging
FORMAT = ('%(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.INFO)

def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()

        self.dtimer = None
        self.com_open_flag = False
        self.run_flag = False
        self.ch_ptype = False

        self.client = None

        self.ptype = RTU
        self.time = 0
        self.toggle = modbus.FORWARD

        self.initUI()


    def eventFilter(self, watched, event):
        pass

    
    def displayMenuLayout(self, parent_layout, glabel, btn, second=None, third=None, forth=None):
        grp_sub = QGroupBox(glabel)
        layout_sub = QVBoxLayout()
        grp_sub.setLayout(layout_sub)
        layout_sub.addWidget(btn)
        if second:
            layout_sub.addWidget(second)
        if third:
            layout_sub.addWidget(third)
        if forth:
            layout_sub.addWidget(forth)
        parent_layout.addWidget(grp_sub)


    def LayoutCalGrid(self, layout_grid, nth, 
                label1, edit_off1, btn_offset1, edit_gain1, btn_gainmod1,
                label2, edit_off2, btn_offset2, edit_gain2, btn_gainmod2):
        layout_grid.addWidget(label1, nth, 0)
        layout_grid.addWidget(edit_off1, nth, 1)
        # layout_grid.addWidget(edit_offavg1, nth, 2)
        layout_grid.addWidget(btn_offset1, nth, 2)

        layout_grid.addWidget(edit_gain1, nth, 3)
        # layout_grid.addWidget(edit_gainavg1, nth, 4)
        layout_grid.addWidget(btn_gainmod1, nth, 4)

        layout_grid.addWidget(label2, nth, 5)
        layout_grid.addWidget(edit_off2, nth, 6)
        # layout_grid.addWidget(edit_offavg2, nth, 9)
        layout_grid.addWidget(btn_offset2, nth, 7)

        layout_grid.addWidget(edit_gain2, nth, 8)
        # layout_grid.addWidget(edit_gainavg2, nth, 9)
        layout_grid.addWidget(btn_gainmod2, nth, 9)


    def displayVerticalLayout(self, parent_layout, glabel, btn, second=None, third=None):
        grp_sub = QGroupBox(glabel)
        layout_sub = QVBoxLayout()
        grp_sub.setLayout(layout_sub)
        layout_sub.addWidget(btn)
        if second:
            layout_sub.addWidget(second)
        if third:
            layout_sub.addWidget(third)
        parent_layout.addWidget(grp_sub)


    def displayMenuGridLayout(self, parent_layout, glabel, btn1, rd1, rd2, pserial):
        grp_sub = QGroupBox(glabel)
        layout_sub = QGridLayout()
        grp_sub.setLayout(layout_sub)
        layout_sub.addWidget(btn1, 0, 0)
        layout_sub.addWidget(rd1, 0, 1)
        layout_sub.addWidget(rd2, 0, 2)
        layout_sub.addWidget(pserial, 1, 0, 1, 3)
        parent_layout.addWidget(grp_sub)


    def initUI(self):
        self.setWindowTitle("PSTEK ELECTRONIC STATIC CHUCK CONTROL PROGRAM")
        # self.setMinimumSize(1024, 800) 

        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width(), screensize.height()) 

        mainwidget = QWidget()                # 위젯의 인스턴스 생성만으로도 QMainWindow에 붙는다.
        self.setCentralWidget(mainwidget)

        ## Main Layout 설정
        self.main_layer = QVBoxLayout()
        mainwidget.setLayout(self.main_layer)

        self.bluefont = "font-weight: bold; color: #0000ff;border-radius: 5px; border: 1px solid blue; padding: 5px;"
        self.redfont = "font-weight: bold; color: #ff0000; border-radius: 5px; border: 1px solid red; padding: 5px;"
        self.unitback = "font-weight: bold; background-color: #e0ebeb; border-radius: 5px; padding: 3px"
        self.redback = "font-weight: bold; background-color: #ff0000; border-radius: 5px; padding: 3px"
        self.blueback = "font-weight: bold; background-color: #0000ff;color: white; border-radius: 5px; padding: 3px"
        self.greenback = "font-weight: bold; background-color: #00ff00; border-radius: 5px; padding: 3px"
        self.grayback = "background-color: #fcfcfc; border: 1px solid lightgray;border-radius: 5px; padding: 3px"

        layout_setup = QHBoxLayout()    
        self.main_layer.addLayout(layout_setup)

        ## 1st Main Menu ##################
        self.displayMainMenu(layout_setup)

        ## 2nd green button ##################
        # 그래프 범례
        grp_voltage = self.displayMainValue()
        self.main_layer.addWidget(grp_voltage)

        ## 2nd green button ##################
        # 그래프 범례
        grp_datadisplay = self.displayEngineeringMode()
        self.main_layer.addWidget(grp_datadisplay)

        ## 3th Status Bar ########################
        # Status
        self.statusbar = QStatusBar()
        self.setStatusBar(self.statusbar)
        self.statusbar.setObjectName("statusbar")

        self.statusmessage = 'PSTEK is aming for global leader!! :: 현재 등록자:() :: 등록 시간 : {}'
        now = QDate.currentDate()
        displaymessage = self.statusmessage.format(now.toString(Qt.ISODate))
        self.statusbar.showMessage(displaymessage)


    def displayMainMenu(self, layout_setup):
        # 전원 장치 JIG 연결
        self.btn_esc_gen = QPushButton("ESC 연결")
        self.btn_esc_gen.clicked.connect(self.setting_modbus)
        self.label_esc_gen = QLabel("No")
        self.displayMenuLayout(layout_setup, "ESC 연결)", 
                self.btn_esc_gen, self.label_esc_gen)

        ############### ############### ###############
        # QR Serial
        self.btn_qrcode = QPushButton("QR READ")
        self.label_serial = QLabel("")

        self.rd_left = QRadioButton("LEFT")
        self.rd_left.setChecked(True)
        self.rd_left.clicked.connect(
                    lambda:self.radioValueClicked(self.rd_left, "SELECTION"))
        # self.rd_select = ESCCaliValue.LEFT
        self.rd_right = QRadioButton("RIGHT")
        self.rd_right.clicked.connect(
                    lambda:self.radioValueClicked(self.rd_right, "SELECTION"))

        self.btn_qrcode.setEnabled(True)
        self.btn_qrcode.clicked.connect(self.readQRCode)
        self.displayMenuGridLayout(layout_setup, "QR READER", 
                self.btn_qrcode, self.rd_left, self.rd_right, self.label_serial)
        
        # 단말기 RUN/STOP
        self.btn_esc_run = QPushButton("RUN")
        self.btn_esc_run.setEnabled(False)
        self.btn_esc_run.setStyleSheet(self.bluefont)
        self.btn_esc_run.clicked.connect(self.esc_device_run)

        self.btn_esc_stop = QPushButton("STOP")
        self.btn_esc_stop.setStyleSheet(self.redfont)
        self.btn_esc_stop.clicked.connect(self.esc_device_stop)
        self.btn_esc_stop.setEnabled(False)
        self.displayVerticalLayout(layout_setup, "RUN/STOP", self.btn_esc_run, self.btn_esc_stop)

        # Toggle 기능 
        self.btn_toggle = QPushButton("TOGGLE")
        self.btn_toggle.setEnabled(False)
        self.btn_toggle.clicked.connect(self.device_toggle)
        self.displayVerticalLayout(layout_setup, "TOGGLE", self.btn_toggle)

        # Logo Image
        labelLogo = QLabel("")
        pixmap = QPixmap(resource_path("logo.png"))
        labelLogo.setPixmap(pixmap)
        labelLogo.setAlignment(Qt.AlignRight)
        layout_setup.addWidget(labelLogo)


    def displayMainValue(self):
        grp_cali = QGroupBox("Voltage & Current")
        layout_cali = QGridLayout()
        grp_cali.setLayout(layout_cali)

        self.vol1 = VolReadWrite(self, layout_cali, "VOLTAGE 1", modbus.READ_VOLTAGE1, write_addresss=modbus.WRITE_VOLTAGE1, col=0)
        self.cur1 = VolReadWrite(self, layout_cali, "CURRENT 1", modbus.READ_CURRENT1, write_addresss=modbus.WRITE_CURRENT1, col=1)
        self.vol2 = VolReadWrite(self, layout_cali, "VOLTAGE 2", modbus.READ_VOLTAGE2, write_addresss=modbus.WRITE_VOLTAGE2, col=2)
        self.cur2 = VolReadWrite(self, layout_cali, "CURRENT 2", modbus.READ_CURRENT2, write_addresss=modbus.WRITE_CURRENT2, col=3)
        self.leaklvl = VolReadWrite(self, layout_cali, "LEAK LEVEL", modbus.READ_WRITE_LEAK_FAULT_LEVEL, write_addresss=modbus.WRITE_LEAK_LEVEL, col=4)

        self.vbias = VolReadWrite(self, layout_cali, "VBIAS", modbus.READ_VBIA, write_addresss=0, col=5)
        self.vcs = VolReadWrite(self, layout_cali, "VCS", modbus.READ_VCS, write_addresss=0, col=6)
        self.ics = VolReadWrite(self, layout_cali, "ICS", modbus.READ_ICS, write_addresss=0, col=7)
        self.cap = VolReadWrite(self, layout_cali, "CAP", modbus.READ_ADDRESS_CP, write_addresss=0, col=8)
        self.leak1 = VolReadWrite(self, layout_cali, "IO LEAK 1", modbus.ESC_READ_IO_LEAK1, write_addresss=0, col=9)
        self.leak2 = VolReadWrite(self, layout_cali, "IO LEAK 2", modbus.ESC_READ_IO_LEAK2, write_addresss=0, col=10)

        self.vol1.display()
        self.cur1.display()
        self.vol2.display()
        self.cur2.display()
        self.leaklvl.display()
        self.vbias.display()
        self.vcs.display()
        self.ics.display()
        self.cap.display()
        self.leak1.display()
        self.leak2.display()

        return grp_cali 
        

    def displayEngineeringMode(self):
        grp_cali = QGroupBox("Engineering Mode")
        layout_cali = QGridLayout()

        # Unlock Button
        btn_unlock = QPushButton("UNLOCK")
        btn_unlock.setStyleSheet(self.bluefont)
        btn_unlock.clicked.connect(self.unlock_password)
        layout_cali.addWidget(btn_unlock, 0, 8, 1, 2)

        # Calibration Button
        btn_lbl1 = QPushButton("구분")
        btn_lbl1.setStyleSheet(self.grayback)
        btn_off1 = QPushButton("OFFSET")
        btn_off1.setStyleSheet(self.grayback)
        # btn_offavg1 = QPushButton("OFFSET 평균")
        # btn_offavg1.setStyleSheet(self.grayback)
        btn_offset1 = QPushButton("OFFSET 조정")
        btn_gain1 = QPushButton("GAIN")
        btn_gain1.setStyleSheet(self.grayback)
        btn_gainavg1 = QPushButton("GAIN평균")
        btn_gainmod1 = QPushButton("GAIN 조정")

        btn_lbl2 = QPushButton("구분")
        btn_lbl2.setStyleSheet(self.grayback)
        btn_off2 = QPushButton("OFFSET")
        btn_off2.setStyleSheet(self.grayback)
        # btn_offavg2 = QPushButton("OFFSET 평균")
        # btn_offavg2.setStyleSheet(self.grayback)
        btn_offset2 = QPushButton("OFFSET 조정")
        btn_gain2 = QPushButton("GAIN")
        btn_gain2.setStyleSheet(self.grayback)
        # btn_gainavg2 = QPushButton("GAIN평균")
        btn_gainmod2 = QPushButton("GAIN 조정")

        self.LayoutCalGrid(layout_cali, 1,
            btn_lbl1, btn_off1, btn_offset1, btn_gain1, btn_gainmod1,
            btn_lbl2, btn_off2, btn_offset2, btn_gain2, btn_gainmod2)

        # VO 01
        self.vo1_offset_gain = OffsetGain(self, layout_cali, "VOLTAGE 1 : ", modbus.ESC_DEV_VO1_OFFSET,
                modbus.ESC_DEV_VO1_GAIN, "VO 1 옵셋", "VO 1 GAIN", 2, 0)
        self.vo1_offset_gain.display()

        # VO 02
        self.vo2_offset_gain = OffsetGain(self, layout_cali, "VOLTAGE 2 : ", modbus.ESC_DEV_VO2_OFFSET,
                modbus.ESC_DEV_VO2_GAIN, "VO 2 옵셋", "VO 2 GAIN", 2, 5)
        self.vo2_offset_gain.display()

        # IO 01
        self.io1_offset_gain = OffsetGain(self, layout_cali, "CURRENT 1 : ", modbus.ESC_DEV_IO1_OFFSET,
                modbus.ESC_DEV_IO1_GAIN, "IO 1 옵셋", "IO 1 GAIN", 3, 0)
        self.io1_offset_gain.display()

        # VO 02
        self.io2_offset_gain = OffsetGain(self, layout_cali, "CURRENT 2 : ", modbus.ESC_DEV_IO2_OFFSET,
                modbus.ESC_DEV_IO2_GAIN, "IO 2 옵셋", "IO 2 GAIN", 3, 5)
        self.io2_offset_gain.display()

        # IR 01
        self.ir1_offset_gain = OffsetGain(self, layout_cali, "IR LEAK 1 : ", modbus.ESC_DEV_LEAK1_OFFSET,
                modbus.ESC_DEV_LEAK1_GAIN, "IO LEAK 1 옵셋", "IO LEAK 1 GAIN", 4, 0)
        self.ir1_offset_gain.display()

        # IR 02
        self.ir2_offset_gain = OffsetGain(self, layout_cali, "IR LEAK 2 : ", modbus.ESC_DEV_LEAK2_OFFSET,
                modbus.ESC_DEV_LEAK2_GAIN, "IO LEAK 2 옵셋", "IO LEAK 2 GAIN", 4, 5)
        self.ir2_offset_gain.display()

        # VBIAS
        self.vbia_offset_gain = OffsetGain(self, layout_cali, "VBIAS : ", modbus.ESC_DEV_VBIAS_OFFSET,
                modbus.ESC_DEV_VBIAS_GAIN, "VBIAS 옵셋", "VBIAS GAIN", 5, 0)
        self.vbia_offset_gain.display()

        # CAP
        self.cap_offset = OffsetGain(self, layout_cali, "CAP : ", modbus.ESC_DEV_CAP_OFFSET,
                ZERO, "CAP 옵셋", None, 5, 5)
        self.cap_offset.display()

        # # VCS
        self.vcs_gain = OffsetGain(self, layout_cali, "VCS : ", modbus.ESC_DEV_VCS_GAIN,
                ZERO, "VCS", None, 6, 0)
        self.vcs_gain.display()

        # ICS
        self.ics_gain = OffsetGain(self, layout_cali, "ICS  : ", modbus.ESC_DEV_ICS_GAIN,
                ZERO, "ICS", None, 6, 5)
        self.ics_gain.display()

        # # CAP COE
        self.cap_coe = OffsetGain(self, layout_cali, "CAP CoE : ", modbus.ESC_DEV_CAP_COE,
                ZERO, "CAP CoE", None, 7, 0)
        self.cap_coe.display()

        # CAP POW
        self.cap_pow = OffsetGain(self, layout_cali, "CAP PoW  : ", modbus.ESC_DEV_CAP_POW,
                ZERO, "ICS", None, 7, 5)
        self.cap_pow.display()

        # ZCS  A COE
        self.aout1_gain = OffsetGain(self, layout_cali, "AO IO 1 GAIN : ", modbus.ESC_DEV_AOIO1_GAIN,
                ZERO, "AO IO 1 GAIN", None, 8, 0)
        self.aout1_gain.display()

        # ZCS  B POW 
        self.aout2_gain = OffsetGain(self, layout_cali, "AO IO 2 GAIN : ", modbus.ESC_DEV_AOIO2_GAIN,
                ZERO, "AO IO 2 GAIN", None, 8, 5)
        self.aout2_gain.display()

        # MODEL
        self.model = OffsetGain(self, layout_cali, "MODEL : ", modbus.ESC_DEV_AOIO1_GAIN,
                ZERO, "MODEL", None, 9, 0)
        self.model.display()

        # Version 
        self.version = OffsetGain(self, layout_cali, "VERSION : ", modbus.ESC_DEV_AOIO2_GAIN,
                ZERO, "VERSION", None, 9, 5)
        self.version.display()


        self.ovp = RadioField(self, modbus.ESC_DEV_OVP_CONTROL, "OVP CONTROL:", "OVP", "NO OVP", "NO OVP", "(O:OVP, 1:NO OVP)")
        ovp_group = self.ovp.display()
        layout_cali.addWidget(ovp_group, 10, 0, 1, 5)

        self.irocp = RadioField(self, modbus.ESC_DEV_IROVP_CONTROL, "IROCP CONTROL:", "OFF", "ON", "ON", "(O:OFF, 1:ON)")
        irocp_group = self.irocp.display()
        layout_cali.addWidget(irocp_group, 10, 5, 1, 5)

        ## APPEND
        self.rs = OffsetGain(self, layout_cali, "RS : ", modbus.ESC_DEV_RS,
                ZERO, "RS", None, 11, 0)
        self.rs.display()

        # VCS COEF 
        self.vcs_coef = OffsetGain(self, layout_cali, "VCS_COEF : ", modbus.ESC_DEV_VCS_COEF,
                ZERO, "VCS_COEF", None, 11, 5)
        self.vcs_coef.display()

        self.ics_coef = OffsetGain(self, layout_cali, "ICS_COEF : ", modbus.ESC_DEV_ICS_COEF,
                ZERO, "ICS_COEF", None, 12, 0)
        self.ics_coef.display()

        self.cap_mode = RadioField(self, modbus.ESC_DEV_CAP_MODE, "CAP MODE:", "CAP", "PHASE", "PHASE", "(O:CAP, 1:PHASE)")
        cap_mode_group = self.cap_mode.display()
        layout_cali.addWidget(cap_mode_group, 12, 5, 1, 5)

        # PHASE OFFSEET
        self.phase_offset = OffsetGain(self, layout_cali, "PHASE OFFSEET : ", modbus.ESC_DEV_PHASE_OFFSEET,
                ZERO, "PHASE OFFSEET", None, 13, 0)
        self.phase_offset.display()

        self.cs1 = OffsetGain(self, layout_cali, "CS1 : ", modbus.ESC_DEV_CS1,
                ZERO, "CS1", None, 13, 5)
        self.cs1.display()

        # CS2
        self.cs2 = OffsetGain(self, layout_cali, "CS2 : ", modbus.ESC_DEV_CS2,
                ZERO, "CS2", None, 14, 0)
        self.cs2.display()

        self.ls0 = OffsetGain(self, layout_cali, "LS0 : ", modbus.ESC_DEV_LS0,
                ZERO, "LS0", None, 14, 5)
        self.ls0.display()

        ## LS50
        self.ls50 = OffsetGain(self, layout_cali, "LS50 : ", modbus.ESC_DEV_LS50,
                ZERO, "LS50", None, 15, 0)
        self.ls50.display()
        self.ics0 = OffsetGain(self, layout_cali, "ICS0 : ", modbus.ESC_DEV_ICS0,
                ZERO, "ICS0", None, 15, 5)
        self.ics0.display()

        ## ICS50
        self.ics50 = OffsetGain(self, layout_cali, "ICS50 : ", modbus.ESC_DEV_ICS50,
                ZERO, "ICS50", None, 16, 0)
        self.ics50.display()

        self.aout_mode = RadioThree(self, modbus.ESC_DEV_AOUTMODE, "AOUT MODE:", "VO", "IO", "CAP", "(O:VO, 1:IO, 2:CAP, 3:LEAK)", "LEAK")
        aout_mode_group = self.aout_mode.display()
        layout_cali.addWidget(aout_mode_group, 16, 5, 1, 5)


        grp_cali.setLayout(layout_cali)
        return grp_cali 

    # 전원 장치와 송신을 위한 설정
    def setting_modbus(self):
        if self.com_open_flag == False:
            Dialog = QDialog()
            self.com_open_flag == True
            dialog = SettingPort(Dialog)
            dialog.show()
            response = dialog.exec_()

            # OK 를 하면 설정 값을 읽어와서 통신을 한다.
            if response == QDialog.Accepted:
                gen_port = dialog.gen_port
                com_speed = dialog.com_speed
                com_data = dialog.com_data
                com_parity = dialog.com_parity
                com_stop = dialog.com_stop
                self.com_open_flag = False
                self.ptype = 'RTU'

                self.client = modbus.connect_rtu(
                    port=gen_port, ptype='rtu',
                    speed=com_speed, bytesize=com_data, 
                    parity=com_parity, stopbits=com_stop
                )

                logging.info(f"setting_modbus :: {self.ptype} :: {self.client}")

                # Graph
                if self.client:
                    self.label_esc_gen.setText(gen_port)
                    self.btn_esc_gen.hide()
                    self.btn_esc_run.setEnabled(True)

                    self.vol1.unlock()
                    self.cur1.unlock()
                    self.vol2.unlock()
                    self.cur2.unlock()
                    self.leaklvl.unlock()

        else:
            logging.info("Open Dialog")


    def unlock_password(self):
        logging.info("unlock_password")

        modbus.check_password(self.client)
        self.read_engineering_mode()
        self.btn_toggle.setEnabled(True)
        
        self.vo1_offset_gain.unlock()
        self.vo2_offset_gain.unlock()
        self.io1_offset_gain.unlock()
        self.io2_offset_gain.unlock()
        self.ir1_offset_gain.unlock()
        self.ir2_offset_gain.unlock()
        self.vbia_offset_gain.unlock()
        self.cap_offset.unlock()
        self.vcs_gain.unlock()
        self.ics_gain.unlock()
        self.cap_coe.unlock()
        self.cap_pow.unlock()
        self.aout1_gain.unlock()
        self.aout2_gain.unlock()
        self.ovp.unlock()
        self.irocp.unlock()
        

        if self.fullflag:
            self.rs.unlock()
            self.vcs_coef.unlock()
            self.ics_coef.unlock()
            self.cap_mode.unlock()

            self.phase_offset.unlock()
            self.cs1.unlock()
            self.cs2.unlock()
            self.ls0.unlock()
            self.ls50.unlock()
            self.ics0.unlock()
            self.ics50.unlock()

        if not self.dtimer:
            self.dtimer = QtCore.QTimer()
            self.dtimer.timeout.connect(self.read_data)
            self.dtimer.start(DATA_PERIOD)


    def modify_device(self, address, val):
        logging.info(f"modify_offset :: {address} :: {val}")
        modbus.write_registers(self.client, address, val)

        
    def read_data(self):
        results = modbus.read_data(self.client, modbus.READ_VOLTAGE1, 16)
        # logging.info(f"#### :: read_data :: {results}")
        if results:
            self.vol1.update(results[0])
            self.cur1.update(results[1])
            self.vol2.update(results[2])
            self.cur2.update(results[3])
            self.vbias.update(results[4])
            self.vcs.update(results[5])
            self.ics.update(results[6])
            self.cap.update(results[7])
            self.leak1.update(results[8])
            self.leak2.update(results[9])
            self.leaklvl.update(results[15])
    
    def read_engineering_mode(self):
        results = modbus.read_data(self.client, modbus.ESC_DEV_CAP_OFFSET, 39)
        self.fullflag = True
        if not results:
            results = modbus.read_data(self.client, modbus.ESC_DEV_CAP_OFFSET, 27)
            self.fullflag = False
        
        logging.info(f"read_data :: {results}")
        if results:
            self.cap_offset.update_offset(results[0])

            self.vo1_offset_gain.update_gain(results[1])
            self.io1_offset_gain.update_gain(results[2])

            self.vo1_offset_gain.update_offset(results[3])
            self.io1_offset_gain.update_offset(results[4])

            self.vo2_offset_gain.update_gain(results[5])
            self.io2_offset_gain.update_gain(results[6])

            self.vo2_offset_gain.update_offset(results[7])
            self.io2_offset_gain.update_offset(results[8])
            
            # VBIAS
            self.vbia_offset_gain.update_gain(results[16])
            self.vbia_offset_gain.update_offset(results[17])

            # Leak
            self.ir1_offset_gain.update_offset(results[23])
            self.ir2_offset_gain.update_offset(results[24])

            self.ir1_offset_gain.update_gain(results[25])
            self.ir2_offset_gain.update_gain(results[26])

            # AOUT
            self.aout1_gain.update_offset(results[14])
            self.aout2_gain.update_offset(results[15])

            self.vcs_gain.update_offset(results[9])
            self.ics_gain.update_offset(results[10])

            self.cap_coe.update_offset(results[11])
            self.cap_pow.update_offset(results[12])

            # Model
            self.model.update_offset(results[21])
            self.version.update_offset(results[22])

            if results[18]:
                self.ovp.change_on()
            else:
                self.ovp.change_off()

            if results[19]:
                self.irocp.change_on()
            else:
                self.irocp.change_off()

            if self.fullflag:
                self.rs.update_offset(results[27])
                self.vcs_coef.update_offset(results[28])
                self.ics_coef.update_offset(results[29])

                if results[30]:
                    self.cap_mode.change_on()
                else:
                    self.cap_mode.change_off()

                self.phase_offset.update_offset(results[31])
                self.cs1.update_offset(results[32])
                self.cs2.update_offset(results[33])
                self.ls0.update_offset(results[34])
                self.ls50.update_offset(results[35])
                self.ics0.update_offset(results[36])
                self.ics50.update_offset(results[37])


    def esc_device_run(self):
        logging.info("esc_device_run")
        modbus.write_registers(self.client, modbus.WRITE_BIT_POWER_ON, modbus.RUN_VALUE)
        # self.btn_esc_run.setEnabled(False)
        self.btn_esc_stop.setEnabled(True)
        self.btn_toggle.setEnabled(True)
        
        if not self.dtimer:
            self.dtimer = QtCore.QTimer()
            self.dtimer.timeout.connect(self.read_data)
            self.dtimer.start(DATA_PERIOD)


    def esc_device_stop(self):
        logging.info("esc_device_stop")
        modbus.write_registers(self.client, modbus.WRITE_BIT_POWER_ON, modbus.STOP_VALUE)   
        self.btn_esc_run.setEnabled(True)
        self.btn_esc_stop.setEnabled(False)

        self.dtimer.stop()
        self.dtimer = None


    def readQRCode(self, nth):
        self.btn_qrcode.hide()
        self.label_serial.setText("????")

    @pyqtSlot(str)
    def read_product_serial(self, code):
        try:
            logging.info(f"read_product_serial :: {code}")
            if code:
                self.btn_qrcode.hide()
                self.label_serial.setText(code)
                self.btn_jig.setEnabled(True)
                self.serialNo = code
                self.get_esc_offsetgain_value()

            else:
                logging.info("No code :: read_product_serial ")
        except Exception as e:
            logging.info(e)


    # 전원 장치 Toggle
    def device_toggle(self):
        logging.info("device_toggle")
        if self.toggle == modbus.FORWARD:
            self.toggle = modbus.REVERSE
            modbus.write_registers(self.client, modbus.WRITE_TOGGLE, modbus.REVERSE)

        else:
            self.toggle = modbus.FORWARD
            modbus.write_registers(self.client, modbus.WRITE_TOGGLE, modbus.FORWARD)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    form = MainWindow()
    form.show()
    sys.exit(app.exec_())