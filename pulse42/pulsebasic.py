import logging
import numpy as np
import pandas as pd
from random import randint, choices

from PyQt5.QtWidgets import QGroupBox, QHBoxLayout, QGridLayout, QLabel, QDialog
from PyQt5.QtWidgets import QMessageBox, QVBoxLayout

from .block import SimpleDisplay, DataView, MyLabel, ChLabel, Field, QPushButton, WarningDialog
from .zoom import GraphWindow

NAMES = [
    "VO OVP HW",
    "IO OCP HW",
    "VO OVP1 SW",
    "IO OCP1 SW",
    "VO OVP2 SW",
    "IO OCP2 SW",
    "RO1 MIN SW",
    "RO2 MIN SW",
    "IR OCP HW",
    "OVERTEMP2",
    "ARC",
    "",
    "MAIN FAULT",
    "MODBUS COMM ERROR",
]

def bit_check(x):
    data = []
    for idx in range(16):
        if (x & (1<<idx)):
            data.append(idx)
    return data

class Comm:
    def __init__(self, parent, name):
        self.parent = parent 
        self.name = name 
        self.flag = True 

        self.red = "QPushButton{font-size: 12pt; font-weight: bold; color: white; background-color: red}"

        self.label_status = Field('Status', '정상', '경고', 0)
        self.fault = 0
        self.label_fault = ChLabel("")
        self.fault_list = []
        self.version = ChLabel("")

        self.v16_list = []
        self.v16_btn = QPushButton("V16")
        self.v16_btn.clicked.connect(lambda:self.displayGraph("V16"))
        self.v16_label = MyLabel()

        self.i16_list = []
        self.i16_btn = QPushButton("I16")
        self.i16_btn.clicked.connect(lambda:self.displayGraph("I16"))
        self.i16_label = MyLabel()

        self.v16slow_list = []
        self.v16slow_btn = QPushButton("V16(s)")
        self.v16slow_btn.clicked.connect(lambda:self.displayGraph("V16(s)"))
        self.v16slow_label = MyLabel()

        self.i16slow_list = []
        self.i16slow_btn = QPushButton("I16(s)")
        self.i16slow_btn.clicked.connect(lambda:self.displayGraph("I16(s)"))
        self.i16slow_label = MyLabel()

    def status_normal(self):
        self.label_status.change_normal()

    def reset(self):
        self.v16_list = []
        self.i16_list = []
        self.v16slow_list = []
        self.i16slow_list = []

        self.status_normal()
        self.label_fault = ChLabel("")
        self.v16_label.setText("")
        self.i16_label.setText("")
        self.v16slow_label.setText("")
        self.i16slow_label.setText("")


    def update_data(self, dch):
        # logging.info(f"Comm :: update_data :: {dch}")
        try:

            if self.flag:
                self.flag = False
                self.version.setText(str(dch[8]))

            self.fault = dch[1]
            if dch[1]:
                # 먼저 경고등 변경
                self.label_status.change_warning()
                
                faults = bit_check(dch[1])
                messages = ""
                if faults:
                    for fault in faults:
                        messages += self.NAMES[fault]
                    self.label_fault.setText(messages)
            else:
                self.status_normal()

            self.fault_list.append(dch[1])
            self.v16_list.append(dch[9])
            self.i16_list.append(dch[10])
            self.v16slow_list.append(dch[23])
            self.i16slow_list.append(dch[24])

            self.upate_label(dch)

        except Exception as e:
            logging.warning(f"class COMM :: update_data :: {dch} :: {e}")

    def upate_label(self, dch):
        try:
            self.v16_label.setText(str(dch[9]))
            self.i16_label.setText(str(dch[10]))
            self.v16slow_label.setText(str(dch[23]))
            self.i16slow_label.setText(str(dch[24]))

        except Exception as e:
            logging.warning(f"class COMM :: upate_label :: {dch} :: {e}")

    def display(self):
        grp_comm = QGroupBox(self.name)
        layout_comm = QGridLayout()
        grp_comm.setLayout(layout_comm)

        self.displayLoutField(layout_comm, 0, self.label_status)
        self.displayLout(layout_comm, 1, QLabel('Fault'), self.label_fault)
        self.displayLout(layout_comm, 2, QLabel('Version'), self.version)
        
        self.displayLout(layout_comm, 3, self.v16_btn, self.v16_label, QLabel('mV'))
        self.displayLout(layout_comm, 4, self.i16_btn, self.i16_label, QLabel('mA'))
        self.displayLout(layout_comm, 5, self.v16slow_btn, self.v16slow_label, QLabel('mV'))
        self.displayLout(layout_comm, 6, self.i16slow_btn, self.i16slow_label, QLabel('mA'))
        
        return grp_comm

    def displayLout(self, layout, nth, label, value, unit=''):
        if unit:
            layout.addWidget(label, nth, 0)
            layout.addWidget(value, nth, 1, 1, 2)
            layout.addWidget(unit, nth, 3)
        else:
            layout.addWidget(label, nth, 0)
            layout.addWidget(value, nth, 1, 1, 3)

    def displayLoutField(self, layout, nth, field):
        layout.addWidget(field.lbl_name, nth, 0, 1, 2)
        layout.addWidget(field.rd_status1, nth, 2)
        layout.addWidget(field.rd_status2, nth, 3)

    def displayGraph(self, ptype):
        if ptype == 'V16':
            vals = self.v16_list
            title = "V16 Voltage"
        elif ptype == 'I16':
            vals = self.i16_list
            title = "I16 Current"
        elif ptype == 'V16(s)':
            vals = self.v16slow_list
            title = "V16 SLOW"
        elif ptype == 'I16(s)':
            vals = self.i16slow_list
            title = "I16 SLOW"

        indexs = list(range(len(vals)))
        Dialog = QDialog()
        dialog = GraphWindow(Dialog, title, (255, 0, 0), indexs, vals)
        dialog.show()
        response = dialog.exec_()
        if response == QDialog.Accepted or response == QDialog.Rejected:
            self.graph_flag = False

        
class Channel:
    def __init__(self, parent, name):
        self.parent = parent 
        self.name = name 

        self.vo_list = []
        self.vo_btn = QPushButton("VO")
        self.vo_btn.clicked.connect(lambda:self.displayGraph("VO"))
        self.vo_label = MyLabel()

        self.io_list = []
        self.io_btn = QPushButton("IO")
        self.io_btn.clicked.connect(lambda:self.displayGraph("IO"))
        self.io_label = MyLabel()

        self.ir_list = []
        self.ir_btn = QPushButton("IR")
        self.ir_btn.clicked.connect(lambda:self.displayGraph("IR"))
        self.ir_label = MyLabel()

        self.po_list = []
        self.po_btn = QPushButton("PO")
        self.po_btn.clicked.connect(lambda:self.displayGraph("PO"))
        self.po_label = MyLabel()

        self.ro_list = [] # Ohm
        self.ro_btn = QPushButton("RO")
        self.ro_btn.clicked.connect(lambda:self.displayGraph("RO"))
        self.ro_label = MyLabel()

        self.voslow_list = []
        self.voslow_btn = QPushButton("VO(s)")
        self.voslow_btn.clicked.connect(lambda:self.displayGraph("VO(s)"))
        self.voslow_label = MyLabel()

        self.ioslow_list = []
        self.ioslow_btn = QPushButton("IO(s)")
        self.ioslow_btn.clicked.connect(lambda:self.displayGraph("IO(s)"))
        self.ioslow_label = MyLabel()

        self.poslow_list = []
        self.poslow_btn = QPushButton("PO(s)")
        self.poslow_btn.clicked.connect(lambda:self.displayGraph("PO(s)"))
        self.poslow_label = MyLabel()

        self.irslow_list = []
        self.irslow_btn = QPushButton("IR(s)")
        self.irslow_btn.clicked.connect(lambda:self.displayGraph("IR(s)"))
        self.irslow_label = MyLabel()
        
    def reset(self):
        self.po_list = []
        self.vo_list = []
        self.io_list = []
        self.ir_list = []
        self.ro_list = []
        self.voslow_list = []
        self.ioslow_list = []
        self.poslow_list = []
        self.irslow_list = []

        self.vo_label.setText("")
        self.io_label.setText("")
        self.ir_label.setText("")
        self.po_label.setText("")
        self.ro_label.setText("")
        self.voslow_label.setText("")
        self.ioslow_label.setText("")
        self.poslow_label.setText("")
        self.irslow_label.setText("")

    def get_ratio(self, current, old):
        ratio = int(((current - old) / old) * 100)
        return ratio

    def update_data(self, index, ch, dch):
        # logging.info(f"Channel :: update_data :: {ch} {dch}")
        try:
            if ch == 1:
                self.vo_list.append(dch[2])
                self.io_list.append(dch[4])
                self.ir_list.append(dch[6])
                self.po_list.append(dch[11])
                self.ro_list.append(dch[13])
                self.voslow_list.append(dch[15])
                self.ioslow_list.append(dch[17])
                self.poslow_list.append(dch[19])
                self.irslow_list.append(dch[21])
            else:
                self.vo_list.append(dch[3])
                self.io_list.append(dch[5])
                self.ir_list.append(dch[7])
                self.po_list.append(dch[12])
                self.ro_list.append(dch[14])
                self.voslow_list.append(dch[16])
                self.ioslow_list.append(dch[18])
                self.poslow_list.append(dch[20])
                self.irslow_list.append(dch[22])

            if index % 10 == 1:
                self.upate_label(ch, dch)

        except Exception as e:
            logging.warning(f"class Channel :: update_data :: {dch} :: {e}")

    def upate_label(self, ch, dch):
        try:
            if ch == 1:
                self.vo_label.setText(str(dch[2]))
                self.io_label.setText(str(dch[4]))
                self.ir_label.setText(str(dch[6]))
                self.po_label.setText(str(dch[11]))
                self.ro_label.setText(str(dch[13]))
                self.voslow_label.setText(str(dch[15]))
                self.ioslow_label.setText(str(dch[17]))
                self.poslow_label.setText(str(dch[19]))
                self.irslow_label.setText(str(dch[21]))
            else:
                self.vo_label.setText(str(dch[3]))
                self.io_label.setText(str(dch[5]))
                self.ir_label.setText(str(dch[7]))
                self.po_label.setText(str(dch[12]))
                self.ro_label.setText(str(dch[14]))
                self.voslow_label.setText(str(dch[16]))
                self.ioslow_label.setText(str(dch[18]))
                self.poslow_label.setText(str(dch[20]))
                self.irslow_label.setText(str(dch[22]))
            
        except Exception as e:
            logging.warning(f"class Channel :: update_data :: {dch} :: {e}")

    def display(self):
        grp_channel = QGroupBox(self.name)
        layout_channel = QGridLayout()
        grp_channel.setLayout(layout_channel)

        self.displayLout(layout_channel, 0, self.vo_btn, self.vo_label, QLabel('V'))
        self.displayLout(layout_channel, 1, self.io_btn, self.io_label, QLabel('mA'))
        self.displayLout(layout_channel, 2, self.ir_btn, self.ir_label, QLabel('mA'))
        self.displayLout(layout_channel, 3, self.po_btn, self.po_label, QLabel('kW'))
        self.displayLout(layout_channel, 4, self.ro_btn, self.ro_label, QLabel('Ohm'))

        self.displayLout(layout_channel, 5, self.voslow_btn, self.voslow_label, QLabel('mV'))
        self.displayLout(layout_channel, 6, self.ioslow_btn, self.ioslow_label, QLabel('mA'))
        self.displayLout(layout_channel, 7, self.poslow_btn, self.poslow_label, QLabel('kW'))
        self.displayLout(layout_channel, 8, self.irslow_btn, self.irslow_label, QLabel('mA'))
        
        return grp_channel

    def displayLout(self, layout, nth, label, value, unit=''):
        if unit:
            layout.addWidget(label, nth, 0)
            layout.addWidget(value, nth, 1, 1, 2)
            layout.addWidget(unit, nth, 3)
        else:
            layout.addWidget(label, nth, 0)
            layout.addWidget(value, nth, 1, 1, 3)

    def displayGraph(self, ptype):
        if ptype == 'VO':
            vals = self.vo_list
            title = "Voltage"
        elif ptype == 'IO':
            vals = self.io_list
            title = "Current"
        elif ptype == 'IR':
            vals = self.ir_list
            title = "IR"
        elif ptype == 'PO':
            vals = self.po_list
            title = "Power"
        elif ptype == 'RO':
            vals = self.ro_list
            title = "RO"

        elif ptype == 'VO(s)':
            vals = self.voslow_list
            title = "VO SLOW"
        elif ptype == 'IO(s)':
            vals = self.ioslow_list
            title = "IO SLOW"
        elif ptype == 'PO(s)':
            vals = self.poslow_list
            title = "PO SLOW"
        elif ptype == 'IR(s)':
            vals = self.irslow_list
            title = "IR SLOW"
        
        indexs = list(range(len(vals)))
        Dialog = QDialog()
        dialog = GraphWindow(Dialog, title, (255, 0, 0), indexs, vals)
        dialog.show()
        response = dialog.exec_()
        if response == QDialog.Accepted or response == QDialog.Rejected:
            self.graph_flag = False


class PulseModule:
    def __init__(self, parent, name, bcolor="#fff"):
        self.parent = parent

        self.name = name
        self.comm = Comm(self, 'Common')

        self.channels = []
        self.ch01 = Channel(self, 'Channel #1')
        self.channels.append(self.ch01)
        self.ch02 = Channel(self, 'Channel #2')
        self.channels.append(self.ch02)

        self.bcolor = bcolor
        self.main_label = "QLabel{font-size: 14pt; font-weight: bold}"

    def display(self):
        grp_module = QGroupBox(self.name)
        layout_module = QVBoxLayout()
        grp_module.setStyleSheet(f"background-color: {self.bcolor}")
        grp_module.setLayout(layout_module)

        comm_group = self.comm.display()
        layout_module.addWidget(comm_group)
        ch_group1 = self.ch01.display()
        layout_module.addWidget(ch_group1)
        ch_group2 = self.ch02.display()
        layout_module.addWidget(ch_group2)
        
        return grp_module

    def active_normal(self):
        self.comm.status_normal()

    def update_data(self, idx, dt):
        # logging.info(f"PulseModule :: update_data :: {dt}")
        try:
            self.comm.update_data(dt)
            self.ch01.update_data(idx, 1, dt)
            self.ch02.update_data(idx, 2, dt)
            
        except Exception as e:
            logging.warning(f"PulseModule :: update_data :: {e}")

    def upate_label(self, dt):
        try:
            self.comm.update_label(dt)
            self.ch01.update_label(1, dt)
            self.ch02.update_label(2, dt)

        except Exception as e:
            logging.warning("PulseModule :: upate_label :: {e}")


    def reset(self):
        self.comm.reset()
        self.ch01.reset()
        self.ch02.reset()



