import sys  # We need sys so that we can pass argv to QApplication
import os
import time
import random
import logging
import pyqtgraph as pyGraph
import logging
from random import randint

from PyQt5.QtWidgets import QDialog, QHBoxLayout, QDialogButtonBox, QDialogButtonBox, QGroupBox, QHBoxLayout
from PyQt5.QtWidgets import QDialogButtonBox, QApplication, QVBoxLayout, QDesktopWidget, QPushButton
from PyQt5 import QtCore
from pyqtgraph import PlotWidget, plot

def rgb_to_hex(r, g, b):
    return '#{:02x}{:02x}{:02x}'.format(r, g, b)

class GraphWindow(QDialog):
    def __init__(self, Dialog, title, color, x, y):
        super().__init__()
        self.title = title
        self.x = x 
        self.y = y
        self.color = color
        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        # self.setGeometry(100, 100, 1200, 800)
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width(), screensize.height()) 

        main_layer = QVBoxLayout()
        self.setWindowTitle("GRAPH FOR EACH")
        self.setLayout(main_layer)

        self.graphWidget = pyGraph.PlotWidget()
        main_layer.addWidget(self.graphWidget)

        self.graphWidget.setBackground('w')
        self.graphWidget.setTitle(self.title, color=self.color, size="30pt")
    
        self.plot(self.x, self.y, self.color)

        self.buttonBox = QDialogButtonBox()
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        main_layer.addWidget(self.buttonBox)

        self.buttonBox.accepted.connect(self.on_accepted)
        self.buttonBox.rejected.connect(self.reject)


    def plot(self, x, y, color):
        pen = pyGraph.mkPen(color=color, width=2)
        self.graphWidget.plot(x, y, pen=pen, symbolSize=3, symbolBrush=('b'))

    def on_accepted(self):
        self.accept()


class ZoomGraph(QDialog):
    def __init__(self, Dialog, title, indexs, data):
        super().__init__()

        ## Align Layout
        pyGraph.setConfigOptions(antialias=True)

        self.title = title
        self.indexes = indexs

        self.data = data
        
        self.color_range = list(range(255))
        self.colors = []
        for idx in range(20): # 80 -> 16
            color = random.choices(self.color_range)[0], random.choices(self.color_range)[0], random.choices(self.color_range)[0]
            self.colors.append(color)

        self.pens = []
        for color in self.colors:
            self.pens.append(pyGraph.mkPen(width=2, color=color))

        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width(), screensize.height()) 

        main_layer = QVBoxLayout()
        self.setWindowTitle(self.title)
        self.setLayout(main_layer)

        ## MAIN
        self.main = pyGraph.PlotWidget()
        self.main.setBackground('w')
        self.axisA = self.main.plotItem
        self.axisA.setLabels(left=title)
        self.axisA.showAxis('right')
        
        ## SUB
        self.zoom = pyGraph.PlotWidget()
        self.zoom.setBackground('w')

        self.zoomWin = self.zoom.plotItem
        self.zoomWin.setLabels(left=title)
        self.zoomWin.showAxis('right')

        self.btn_group = QGroupBox()
        layout = QHBoxLayout()
        self.btn_group.setLayout(layout)

        count = 0
        try:
            for midx in range(5):
                for subidx in range(4):
                    name = f"#M{midx+1}#C{subidx+1}"
                    btn = QPushButton(name)
                    btn.setFixedSize(100, 30)
                    color = rgb_to_hex(*self.colors[count])
                    btn.setStyleSheet(f"background-color: {color}; font-size: 10px;font-weight: bold;")
                    layout.addWidget(btn)
                    count += 1

        except Exception as e:
            logging.warning(f"Button :: {e}")

        main_layer.addWidget(self.btn_group)
        main_layer.addWidget(self.main)
        main_layer.addWidget(self.zoom)

        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        
        try:
            count = 0
            for midx in range(5):
                for subidx in range(4):
                    coloridx = count % 16
                    count += 1
                    name = f"mod{midx+1}_ch{subidx +1}"
                    temp1_data = self.data[name]
                    # logging.info(f"{name} :: {temp1_data}")
                    self.temp1_graph = self.axisA.plot(self.indexes, temp1_data, pen=self.pens[coloridx], name=name)

            linZone = pyGraph.LinearRegionItem([20,40])
            linZone.setZValue(-10)
            self.axisA.addItem(linZone)

            count = 0
            for midx in range(5):
                for subidx in range(4):
                    coloridx = count % 16
                    count += 1
                    name = f"mod{midx+1}_ch{subidx +1}"
                    temp1_data = self.data[name]
                    # logging.info(f"{name} :: {temp1_data}")
                    self.temp1_graph = self.zoomWin.plot(self.indexes, temp1_data, pen=self.pens[coloridx], name=name)

            # Region 이 변경되면 오른쪽 그래프를 변경해 줌
            def updateChangePlot():
                self.zoomWin.setXRange(*linZone.getRegion(), padding=0)

            def updateRegion():
                linZone.setRegion(self.zoomWin.getViewBox().viewRange()[0])

            linZone.sigRegionChanged.connect(updateChangePlot)
            self.zoomWin.sigXRangeChanged.connect(updateRegion)

            updateChangePlot()

        except Exception as e:
            logging.warning(f"ZoomGraph :: setupUI :: {e}")


class ZoomWindowFull(QDialog):

    def __init__(self, Dialog, indexs, voltage1s, current1s, voltage2s, current2s, vbias, cpss, dechuck_starts, dechuck_ends):
        super().__init__()
        self.title = "ZOOM IN DATA"
        self.indexs = indexs 
        self.voltage1s = voltage1s 
        self.current1s = current1s 
        self.voltage2s = voltage2s 
        self.current2s = current2s 
        self.vbias = vbias 
        self.cpss = cpss 
        self.dechuck_starts = dechuck_starts 
        self.dechuck_ends = dechuck_ends 

        self.pen_voltage1 = pyGraph.mkPen(width=2, color=(255, 0, 0))
        self.pen_current1 = pyGraph.mkPen(width=2, color=(0, 0, 255))
        self.pen_voltage2 = pyGraph.mkPen(width=2, color=(255, 191, 0))
        self.pen_current2 = pyGraph.mkPen(width=2, color=(26, 175, 51))
        self.pen_vbia = pyGraph.mkPen(width=2, color=(0,0,128))
        self.pen_cps = pyGraph.mkPen(width=2, color=(204, 153, 0))
        self.pen_dechuckstart = pyGraph.mkPen(width=2, color=(204, 0, 153))
        self.pen_dechuckend = pyGraph.mkPen(width=2, color=(102, 0, 204))

        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        self.setGeometry(0, 0, 1200, 800)

        main_layer = QVBoxLayout()
        self.setLayout(main_layer)

        self.window = pyGraph.PlotWidget()
        pyGraph.setConfigOptions(antialias=True)
        main_layer.addWidget(self.window)

        ## create mainAxis
        self.mainAxis = self.window.plotItem
        self.mainAxis.setLabels(left='Voltage')
        self.mainAxis.showAxis('right')

        ## Second ViewBox
        self.axisB = pyGraph.ViewBox()
        self.mainAxis.scene().addItem(self.axisB)
        self.mainAxis.getAxis('right').linkToView(self.axisB)
        self.axisB.setXLink(self.mainAxis)
        self.mainAxis.getAxis('right').setLabel('Current', color='#0000ff')

        ## create Third ViewBox. 
        self.axisC = pyGraph.ViewBox()
        ax3 = pyGraph.AxisItem('right')
        self.mainAxis.layout.addItem(ax3, 2, 3)
        self.mainAxis.scene().addItem(self.axisC)
        ax3.linkToView(self.axisC)
        self.axisC.setXLink(self.mainAxis)
        ax3.setLabel('Vais', color='#000080')

        ## Handle view resizing 
        def updateViews():
            ## view has resized; update auxiliary views to match
            # global self.mainAxis, self.axisB, self.axisC
            self.axisB.setGeometry(self.mainAxis.vb.sceneBoundingRect())
            self.axisC.setGeometry(self.mainAxis.vb.sceneBoundingRect())
            
            self.axisB.linkedViewChanged(self.mainAxis.vb, self.axisB.XAxis)
            self.axisC.linkedViewChanged(self.mainAxis.vb, self.axisC.XAxis)

        updateViews()
        self.mainAxis.vb.sigResized.connect(updateViews)

        self.graph_voltage1s =  self.mainAxis.plot(self.indexs, self.voltage1s, pen=self.pen_voltage1, name="Voltage1s")
        self.graph_current1s =  self.axisB.addItem(pyGraph.PlotCurveItem(self.indexs, self.current1s, pen=self.pen_current1, name="Current1s"))
        self.graph_voltage2s =  self.mainAxis.plot(self.indexs, self.voltage2s, pen=self.pen_voltage2, name="voltage2s")
        self.graph_current2s =  self.axisB.addItem(pyGraph.PlotCurveItem(self.indexs, self.current2s, pen=self.pen_current2, name="Current2s"))

        self.graph_vbias =  self.axisC.addItem(pyGraph.PlotCurveItem(self.indexs, self.vbias, pen=self.pen_vbia, name="Vbias"))
        self.graph_cpss =  self.mainAxis.plot(self.indexs, self.cpss, pen=self.pen_cps, name="Cps")
        self.graph_dechuck_starts =  self.mainAxis.plot(self.indexs, self.dechuck_starts, pen=self.pen_dechuckstart, name="Leak_Current1")
        self.graph_dechuck_ends =  self.axisC.addItem(pyGraph.PlotCurveItem(self.indexs, self.dechuck_ends, pen=self.pen_dechuckend, name="Leak_Current2"))

        
    def on_accepted(self):
        self.accept()


## Pulse DC Two Channel Compare
class GraphWindowTwo(QDialog):
    def __init__(self, Dialog, title, data):
        super().__init__()
        self.title = title
        self.data = data
        logging.info(data)

        self.buttons = []
        color_range = list(range(255))
        self.pens = []
        for idx in range(20): # 80 -> 16
            color = random.choices(color_range)[0], random.choices(color_range)[0], random.choices(color_range)[0]
            hex_color = "#{:02X}{:02X}{:02X}".format(color[0], color[1], color[2])
            self.pens.append(hex_color)

        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width(), screensize.height()) 
        # self.setGeometry(0, 0, 1200, 800)

        main_layer = QVBoxLayout()
        self.setLayout(main_layer)

        btn_title = QPushButton(self.title)
        btn_title.setStyleSheet(f"background-color: white;color: #ED6032; font-size: 14px;font-weight: bold;")
        main_layer.addWidget(btn_title)

        grp_graph = QGroupBox("")
        layout_graph = QHBoxLayout()
        grp_graph.setLayout(layout_graph)

        for idx, dt in enumerate(self.data):
            name = dt['name']
            qbtn = QPushButton(name)
            style = f"color:{self.pens[idx]};"
            qbtn.setStyleSheet(style)
            self.buttons.append(qbtn)
            layout_graph.addWidget(qbtn)

        main_layer.addWidget(grp_graph)

        try:
            ## MOD #1
            self.graph = pyGraph.PlotWidget()
            pyGraph.setConfigOptions(antialias=True)
            main_layer.addWidget(self.graph)

            for idx, dt in enumerate(self.data):
                name = dt['name']
                data = dt['data']
                ## create mainAxis
                self.mainAxis = self.graph.plotItem
                pen_mod = pyGraph.mkPen(width=2, color=self.pens[idx])
                self.mainAxis.plot(data, pen=pen_mod, name=name)
            
        except Exception as e:
            print(e)

    def on_accepted(self):
        self.accept()


def main():
    app = QApplication(sys.argv)
    main = ZoomWindow()
    main.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
