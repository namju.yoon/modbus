import sys
import os
import time

import numpy as np
import pandas as pd

from PyQt5.QtWidgets import QWidget, QLabel, QTextEdit, QPushButton, QDesktopWidget, QMainWindow
from PyQt5.QtWidgets import QGroupBox, QBoxLayout, QVBoxLayout, QHBoxLayout, QGridLayout
from PyQt5.QtWidgets import QApplication, QDialog, QStatusBar, QFileDialog, QTabWidget
from PyQt5.QtWidgets import QSpinBox
from PyQt5 import QtCore
from PyQt5.QtCore import QDate, Qt
from PyQt5.QtGui import QPixmap
from itertools import count
import pyqtgraph as pg

from setup_esc import SettingWin
import epc_serial as device
from save_esc import DataWin

from common.block import DataBlock


def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)


class MainWindow(QWidget):
    def __init__(self):
        super().__init__()

        self.stopFlag = True
        self.period = 3
        self.timer = QtCore.QTimer()
        self.client = None

        self.index = 0
        self.idx_list = np.array([])
        self.po_list = np.array([])
        self.vo_list = np.array([])
        self.io_list = np.array([])
        self.temp_list = np.array([])

    # MODULE 1
        self.vo_index1 = 0
        self.vo_indexs1 = np.array([])
        self.com_index1 = 0
        self.com_indexs1 = np.array([])

        self.VO1_1 = np.array([])
        self.VO1_2 = np.array([])
        self.VO1_3 = np.array([])
        self.VO1_4 = np.array([])
        self.VO1_5 = np.array([])
        self.VO1_6 = np.array([])
        self.VO1_7 = np.array([])
        self.VO1_8 = np.array([])

        # self.VD1_1 = np.array([])
        # self.VD1_2 = np.array([])
        # self.VD1_3 = np.array([])

        self.IR1_1 = np.array([])
        self.IR1_2 = np.array([])
        self.IR1_3 = np.array([])
        self.IR1_4 = np.array([])
        self.IR1_5 = np.array([])
        self.IR1_6 = np.array([])
        self.IR1_7 = np.array([])
        self.IR1_8 = np.array([])

        self.IO1_1 = np.array([])
        self.IO1_2 = np.array([])
        self.IO1_3 = np.array([])

        self.VOSUM1_1 = np.array([])
        self.VOSUM1_2 = np.array([])
        self.VOSUM1_3 = np.array([])

        self.ISPMS1 = np.array([])
        self.PO1 = np.array([])
        self.IINR1 = np.array([])
        self.TEMP1 = np.array([])
        self.VRS1 = np.array([])
        self.VST1 = np.array([])
        self.STATUS1 = np.array([])
        self.VPEAK1 = np.array([])

    # MODULE 2
        self.vo_index2 = 0
        self.vo_indexs2 = np.array([])
        self.com_index2 = 0
        self.com_indexs2 = np.array([])

        self.VO2_1 = np.array([])
        self.VO2_2 = np.array([])
        self.VO2_3 = np.array([])

        self.VD2_1 = np.array([])
        self.VD2_2 = np.array([])
        self.VD2_3 = np.array([])

        self.IR2 = np.array([])

        self.IO2_1 = np.array([])
        self.IO2_2 = np.array([])
        self.IO2_3 = np.array([])

        self.VOSUM2_1 = np.array([])
        self.VOSUM2_2 = np.array([])
        self.VOSUM2_3 = np.array([])

        self.ISPMS2 = np.array([])
        self.PO2 = np.array([])
        self.IINR2 = np.array([])
        self.TEMP2 = np.array([])
        self.VRS2 = np.array([])
        self.VST2 = np.array([])
        self.STATUS2 = np.array([])
        self.VPEAK2 = np.array([])

    # MODULE 3
        self.vo_index3 = 0
        self.vo_indexs3 = np.array([])
        self.com_index3 = 0
        self.com_indexs3 = np.array([])

        self.VO3_1 = np.array([])
        self.VO3_2 = np.array([])
        self.VO3_3 = np.array([])

        self.VD3_1 = np.array([])
        self.VD3_2 = np.array([])
        self.VD3_3 = np.array([])

        self.IR3 = np.array([])

        self.IO3_1 = np.array([])
        self.IO3_2 = np.array([])
        self.IO3_3 = np.array([])

        self.VOSUM3_1 = np.array([])
        self.VOSUM3_2 = np.array([])
        self.VOSUM3_3 = np.array([])

        self.ISPMS3 = np.array([])
        self.PO3 = np.array([])
        self.IINR3 = np.array([])
        self.TEMP3 = np.array([])
        self.VRS3 = np.array([])
        self.VST3 = np.array([])
        self.STATUS3 = np.array([])
        self.VPEAK3 = np.array([])

    # MODULE 4
        self.vo_index4 = 0
        self.vo_indexs4 = np.array([])
        self.com_index4 = 0
        self.com_indexs4 = np.array([])

        self.VO4_1 = np.array([])
        self.VO4_2 = np.array([])
        self.VO4_3 = np.array([])

        self.VD4_1 = np.array([])
        self.VD4_2 = np.array([])
        self.VD4_3 = np.array([])

        self.IR4 = np.array([])

        self.IO4_1 = np.array([])
        self.IO4_2 = np.array([])
        self.IO4_3 = np.array([])

        self.VOSUM4_1 = np.array([])
        self.VOSUM4_2 = np.array([])
        self.VOSUM4_3 = np.array([])

        self.ISPMS4 = np.array([])
        self.PO4 = np.array([])
        self.IINR4 = np.array([])
        self.TEMP4 = np.array([])
        self.VRS4 = np.array([])
        self.VST4 = np.array([])
        self.STATUS4 = np.array([])
        self.VPEAK4 = np.array([])

        self.com_open_flag = False

    # PEN Color
        self.pen_vo = pg.mkPen(width=2, color=(0, 255,0))
        self.pen_diff = pg.mkPen(width=2, color=(204, 0, 0))
        self.pen_ir = pg.mkPen(width=2, color=(0, 0, 255))
        self.pen_sumvo1 = pg.mkPen(width=2, color=(255, 0, 0))
        self.pen_io1 = pg.mkPen(width=2, color=(0, 255, 0))
        self.pen_sumvo2 = pg.mkPen(width=2, color=(255, 128, 0))
        self.pen_io2 = pg.mkPen(width=2, color=(0, 255, 128))
        self.pen_sumvo3 = pg.mkPen(width=2, color=(255, 255, 0))
        self.pen_io3 = pg.mkPen(width=2, color=(0, 255, 255))
        
        
        self.pen_ismps = pg.mkPen(width=2, color=(0, 255, 64))
        self.pen_po = pg.mkPen(width=2, color=(0, 191, 255))
        self.pen_iinr = pg.mkPen(width=2, color=(128, 0, 255))
        self.pen_temp = pg.mkPen(width=2, color=(255, 0, 191))
        self.pen_vrs = pg.mkPen(width=2, color=(255, 153, 153))
        self.pen_vst = pg.mkPen(width=2, color=(102, 0, 204))
        self.pen_status = pg.mkPen(width=2, color=(255, 0, 128))
        self.pen_vpeak = pg.mkPen(width=2, color=(255, 128, 0))

        self.initUI()

    # GUI initUI
    def initUI(self):
        main_layout = QVBoxLayout()

        grp_menu = QGroupBox("")
        menu_layout = QHBoxLayout()
        grp_menu.setLayout(menu_layout)

    ## replace #1st ##############
        # 전원 장치 연결
        grp_generator = QGroupBox("전원 장치 연결(485/ModBus)")
        layout_generator = QHBoxLayout()
        self.btn_com_gen = QPushButton("전원 장치 연결 포트 선택")
        self.label_gen_port = QLabel("N/A")
        self.btn_com_gen.clicked.connect(self.setting_generator)
        layout_generator.addWidget(self.btn_com_gen)
        layout_generator.addWidget(self.label_gen_port)
        grp_generator.setLayout(layout_generator)
        menu_layout.addWidget(grp_generator)

        # 통신 시작
        grp_startstop = QGroupBox("START/STOP")
        layout_startstop = QHBoxLayout()
        self.btn_start_data = QPushButton("단말기 정보 요청")
        self.btn_start_data.setStyleSheet("font-size: 14px; font-weight: bold;color: #0000ff;")
        
        self.btn_start_data.clicked.connect(self.run_device)
        self.btn_data_stop = QPushButton("정보 요청 Stop")
        self.btn_data_stop.setStyleSheet("font-size: 14px; font-weight: bold;color: #ff0000;")
        self.btn_data_stop.clicked.connect(self.stop_device)
        layout_startstop.addWidget(self.btn_start_data)
        layout_startstop.addWidget(self.btn_data_stop)
        grp_startstop.setLayout(layout_startstop)
        menu_layout.addWidget(grp_startstop)

        # 데이터 읽어 오기 주기 변경
        grp_readterm = QGroupBox("읽어 오는 주기 설정")
        layout_readterm = QHBoxLayout()
        self.edit_readterm = QSpinBox()
        self.edit_readterm.setRange(1, 30)
        self.edit_readterm.setValue(3)
        self.btn_readterm = QPushButton("주기 설정")
        self.btn_readterm.setStyleSheet("font-size: 14px; font-weight: bold;color: #0000ff;")
        
        self.btn_readterm.clicked.connect(self.change_readterm)
        layout_readterm.addWidget(self.edit_readterm)
        layout_readterm.addWidget(self.btn_readterm)
        grp_readterm.setLayout(layout_readterm)
        menu_layout.addWidget(grp_readterm)

        # 데이터 저장
        grp_savedata = QGroupBox("데이터 저장")
        layout_savedata = QHBoxLayout()
        self.btn_savedata = QPushButton("데이터 저장")
        self.btn_savedata.setStyleSheet("font-size: 14px; font-weight: bold;")
        self.btn_savedata.clicked.connect(self.save_data)
        layout_savedata.addWidget(self.btn_savedata)
        grp_savedata.setLayout(layout_savedata)
        menu_layout.addWidget(grp_savedata)

        # 데이터 불어오기
        grp_readdata = QGroupBox("파일 읽어오기")
        layout_readdata = QHBoxLayout()
        self.btn_readdata = QPushButton("파일 읽어오기")
        self.btn_readdata.setStyleSheet("font-size: 14px; font-weight: bold;")
        # self.btn_readdata.clicked.connect(self.read_data)
        layout_readdata.addWidget(self.btn_readdata)
        grp_readdata.setLayout(layout_readdata)
        menu_layout.addWidget(grp_readdata)

        # Logo Image
        labelLogo = QLabel("")
        pixmap = QPixmap(resource_path("logo.png"))
        labelLogo.setAlignment(Qt.AlignRight)
        labelLogo.setPixmap(pixmap)
        menu_layout.addWidget(labelLogo)        

        main_layout.addWidget(grp_menu)

        mainTab = QWidget()
        tab1 = QWidget()
        tab2 = QWidget()
        tab3 = QWidget()
        tab4 = QWidget()
        modidataTab = QWidget()

        tabs = QTabWidget()
        tabs.addTab(mainTab, 'MAIN')
        tabs.addTab(tab1, 'Module 1')
        tabs.addTab(tab2, 'Module 2')
        tabs.addTab(tab3, 'Module 3')
        tabs.addTab(tab4, 'Module 4')
        tabs.addTab(modidataTab, 'Data')

        # vbox = QVBoxLayout()
        main_layout.addWidget(tabs)

        self.setLayout(main_layout)
        self.setWindowTitle("PSTEK 집진기 전원 장치 모니터링용")
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width() - 100, screensize.height() - 50) 
        # self.setMinimumSize(screensize.width() - 100, 200) 

        self.displayMain(mainTab)
        self.displayTab1(tab1)
        self.displayTab2(tab2)
        self.displayTab3(tab3)
        self.displayTab4(tab4)
        self.displayModiData(modidataTab)

        self.show()


    # Main Tab
    def displayMain(self, tab):

        # Main Display
        main_layer = QVBoxLayout()
        tab.setLayout(main_layer)

    ## 2nd Layer Data Value
        grp_legend = QGroupBox("데이터 범례")
        layout_legend = QHBoxLayout()

        # 출력 out power
        grp_power = QGroupBox("")
        layout_power = QGridLayout()

        btn_power = QPushButton("POWER")
        btn_power.setStyleSheet("font-weight: bold; color: #cc0000")
        # btn_power.clicked.connect(lambda:self.draw_graph_each('Power', color=(204, 0, 0)))
        self.label_mainpower = QLabel("0")
        unit_power = QLabel("kW")
        self.label_mainpower.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        layout_power.addWidget(btn_power, 0, 0, 1, 4)
        layout_power.addWidget(self.label_mainpower, 1, 0, 1, 3)
        layout_power.addWidget(unit_power, 1, 3)

        grp_power.setLayout(layout_power)
        layout_legend.addWidget(grp_power)

        # 전압 out voltage
        grp_voltage = QGroupBox("")
        layout_voltage = QGridLayout()

        btn_voltage = QPushButton("VO")
        btn_voltage.setStyleSheet("font-weight: bold; color: #00ff00")
        # btn_voltage.clicked.connect(lambda:self.draw_graph_each('Voltage', color=(0, 255,0)))
        self.label_mainvoltage = QLabel("0")
        unit_voltage = QLabel("kV")
        self.label_mainvoltage.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        layout_voltage.addWidget(btn_voltage, 0, 0, 1, 4)
        layout_voltage.addWidget(self.label_mainvoltage, 1, 0, 1, 3)
        layout_voltage.addWidget(unit_voltage, 1, 3)

        grp_voltage.setLayout(layout_voltage)
        layout_legend.addWidget(grp_voltage)

        # 전류 out current 
        grp_current = QGroupBox("")
        layout_current = QGridLayout()

        btn_current = QPushButton("IO")
        btn_current.setStyleSheet("font-weight: bold; color: #0000ff;")
        # btn_current.clicked.connect(lambda:self.draw_graph_each('Current', color=(0, 0, 255)))
        self.label_maincurrent = QLabel("0")
        unit_current = QLabel("mA")
        self.label_maincurrent.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        layout_current.addWidget(btn_current, 0, 0, 1, 4)
        layout_current.addWidget(self.label_maincurrent, 1, 0, 1, 3)
        layout_current.addWidget(unit_current, 1, 3)

        grp_current.setLayout(layout_current)
        layout_legend.addWidget(grp_current)

        # TEMPERATURE
        grp_temperature = QGroupBox("")
        layout_temperature = QGridLayout()

        btn_temperature = QPushButton("온도")
        btn_temperature.setStyleSheet("font-weight: bold; color: #cc9900")
        # btn_temperature.clicked.connect(lambda:self.draw_graph_each('Impedence', color=(204, 153, 0)))
        self.label_maintemp = QLabel("0")
        unit_temperature = QLabel("℃")
        self.label_maintemp.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        layout_temperature.addWidget(btn_temperature, 0, 0, 1, 4)
        layout_temperature.addWidget(self.label_maintemp, 1, 0, 1, 3)
        layout_temperature.addWidget(unit_temperature, 1, 3)

        grp_temperature.setLayout(layout_temperature)
        layout_legend.addWidget(grp_temperature)

        grp_legend.setLayout(layout_legend)
        main_layer.addWidget(grp_legend)

    ## 3rd Fault Message ########
        grp_fault = QGroupBox("에러 메시지")
        layout_fault = QHBoxLayout()
        self.label_fault = QLabel("")
        layout_fault.addWidget(self.label_fault)
        grp_fault.setLayout(layout_fault)
        main_layer.addWidget(grp_fault)

    ## 4th Graph ################
        # Graph Window
        # 그래프 디스플레이
        grp_display = QGroupBox("GRAPH")
        layout_display = QHBoxLayout()
        self.graphWidget = pg.PlotWidget()
        self.graphWidget.setBackground('w')
        
        ## create mainAxis
        self.mainAxis = self.graphWidget.plotItem
        self.mainAxis.setLabels(left='PO')
        self.mainAxis.showAxis('right')

        ## Second ViewBox
        self.axisB = pg.ViewBox()
        self.mainAxis.scene().addItem(self.axisB)
        self.mainAxis.getAxis('right').linkToView(self.axisB)
        self.axisB.setXLink(self.mainAxis)
        self.mainAxis.getAxis('right').setLabel('VO', color='#0000ff')

        ## create Third ViewBox. 
        self.axisC = pg.ViewBox()
        ax3 = pg.AxisItem('right')
        self.mainAxis.layout.addItem(ax3, 2, 3)
        self.mainAxis.scene().addItem(self.axisC)
        ax3.linkToView(self.axisC)
        self.axisC.setXLink(self.mainAxis)
        ax3.setLabel('IO', color='#00ff00')

        # self.updateViews()
        # self.mainAxis.vb.sigResized.connect(self.updateViews)

        self.graph_po = self.mainAxis.plot(self.idx_list, self.po_list, pen=self.pen_po, name="PO")
        
        self.itemVO= pg.PlotCurveItem(self.idx_list, self.vo_list, pen=self.pen_vo, name="VO")
        self.graph_vo = self.axisB.addItem(self.itemVO)

        self.itemIO = pg.PlotCurveItem(self.idx_list, self.io_list, pen=self.pen_io1, name="IO")
        self.graph_vbias = self.axisC.addItem(self.itemIO)

        layout_display.addWidget(self.graphWidget)
        grp_display.setLayout(layout_display)
        main_layer.addWidget(grp_display)


    def displayTab1(self, tab):
        # TAB Display
        main_layer = QVBoxLayout()
        tab.setLayout(main_layer)

        # Sub VO 1
        # grp_sub1 = QGroupBox("Moduel1 VO")
        # layout_sub1 = QHBoxLayout()

        grp_vosub1 = QGroupBox("1 VO")
        layout_vosub1 = QVBoxLayout()
        # self.label_vo11 = QLabel("")
        # self.label_diff11 = QLabel("")
        self.graphvo11 = pg.PlotWidget()
        self.graphvo11.setBackground('w')

        # layout_vosub1.addWidget(self.label_vo11)
        # layout_vosub1.addWidget(self.label_diff11)
        layout_vosub1.addWidget(self.graphvo11)
        grp_vosub1.setLayout(layout_vosub1)
        main_layer.addWidget(grp_vosub1)

        grp_ir1 = QGroupBox("IR")
        layout_ir1 = QVBoxLayout()
        # self.label_ir1 = QLabel("")
        self.graphir1 = pg.PlotWidget()
        self.graphir1.setBackground('w')

        # layout_ir1.addWidget(self.label_ir1)
        layout_ir1.addWidget(self.graphir1)
        grp_ir1.setLayout(layout_ir1)
        main_layer.addWidget(grp_ir1)

        # Sub ETC : ISMPS, PO, IINR, TEMP, VRS, VST, STATUS, VPEAK
        grp_etcsub1 = QGroupBox("ISMPS, PO, IINR, TEMP, VRS, VST, STATUS, VPEAK")
        layout_etcsub1 = QHBoxLayout()
        
        self.graphio1 = pg.PlotWidget()
        self.graphetc1 = pg.PlotWidget()

        layout_etcsub1.addWidget(self.graphio1)
        layout_etcsub1.addWidget(self.graphetc1)
        grp_etcsub1.setLayout(layout_etcsub1)
        main_layer.addWidget(grp_etcsub1)


    def displayTab2(self, tab):
        # TAB Display
        main_layer = QVBoxLayout()
        tab.setLayout(main_layer)

        # Sub VO 1
        grp_sub1 = QGroupBox("Moduel2 VO")
        layout_sub1 = QHBoxLayout()

        grp_vosub1 = QGroupBox("1 VO")
        layout_vosub1 = QVBoxLayout()
        self.label_vo21 = QLabel("")
        self.label_diff21 = QLabel("")
        self.graphvo21 = pg.PlotWidget()
        self.graphvo21.setBackground('w')

        layout_vosub1.addWidget(self.label_vo21)
        layout_vosub1.addWidget(self.label_diff21)
        layout_vosub1.addWidget(self.graphvo21)
        grp_vosub1.setLayout(layout_vosub1)

        grp_vosub2 = QGroupBox("2 VO")
        layout_vosub2 = QVBoxLayout()
        self.label_vo22 = QLabel("")
        self.label_diff22 = QLabel("")
        self.graphvo22 = pg.PlotWidget()
        self.graphvo22.setBackground('w')

        layout_vosub2.addWidget(self.label_vo22)
        layout_vosub2.addWidget(self.label_diff22)
        layout_vosub2.addWidget(self.graphvo22)
        grp_vosub2.setLayout(layout_vosub2)


        grp_vosub3 = QGroupBox("3 VO")
        layout_vosub3 = QVBoxLayout()
        self.label_vo23 = QLabel("")
        self.label_diff23 = QLabel("")
        self.graphvo23 = pg.PlotWidget()
        self.graphvo23.setBackground('w')

        layout_vosub3.addWidget(self.label_vo23)
        layout_vosub3.addWidget(self.label_diff23)
        layout_vosub3.addWidget(self.graphvo23)
        grp_vosub3.setLayout(layout_vosub3)

        grp_ir1 = QGroupBox("IR")
        layout_ir1 = QVBoxLayout()
        self.label_ir2 = QLabel("")
        self.graphir2 = pg.PlotWidget()
        self.graphir2.setBackground('w')

        layout_ir1.addWidget(self.label_ir2)
        layout_ir1.addWidget(self.graphir2)
        grp_ir1.setLayout(layout_ir1)


        layout_sub1.addWidget(grp_vosub1)
        layout_sub1.addWidget(grp_vosub2)
        layout_sub1.addWidget(grp_vosub3)
        layout_sub1.addWidget(grp_ir1)
        
        grp_sub1.setLayout(layout_sub1)
        main_layer.addWidget(grp_sub1)


        # Sub ETC : ISMPS, PO, IINR, TEMP, VRS, VST, STATUS, VPEAK
        grp_etcsub1 = QGroupBox("ISMPS, PO, IINR, TEMP, VRS, VST, STATUS, VPEAK")
        layout_etcsub1 = QHBoxLayout()
        
        self.graphio2 = pg.PlotWidget()
        self.graphetc2 = pg.PlotWidget()

        layout_etcsub1.addWidget(self.graphio2)
        layout_etcsub1.addWidget(self.graphetc2)
        grp_etcsub1.setLayout(layout_etcsub1)
        main_layer.addWidget(grp_etcsub1)


    def displayTab3(self, tab):
        # TAB 3 Display
        main_layer = QVBoxLayout()
        tab.setLayout(main_layer)

        # Sub VO 1
        grp_sub1 = QGroupBox("Moduel3 VO")
        layout_sub1 = QHBoxLayout()

        grp_vosub1 = QGroupBox("1 VO")
        layout_vosub1 = QVBoxLayout()
        self.label_vo31 = QLabel("")
        self.label_diff31 = QLabel("")
        self.graphvo31 = pg.PlotWidget()
        self.graphvo31.setBackground('w')

        layout_vosub1.addWidget(self.label_vo31)
        layout_vosub1.addWidget(self.label_diff31)
        layout_vosub1.addWidget(self.graphvo31)
        grp_vosub1.setLayout(layout_vosub1)

        grp_vosub2 = QGroupBox("2 VO")
        layout_vosub2 = QVBoxLayout()
        self.label_vo32 = QLabel("")
        self.label_diff32 = QLabel("")
        self.graphvo32 = pg.PlotWidget()
        self.graphvo32.setBackground('w')

        layout_vosub2.addWidget(self.label_vo32)
        layout_vosub2.addWidget(self.label_diff32)
        layout_vosub2.addWidget(self.graphvo32)
        grp_vosub2.setLayout(layout_vosub2)

        grp_vosub3 = QGroupBox("3 VO")
        layout_vosub3 = QVBoxLayout()
        self.label_vo33 = QLabel("")
        self.label_diff33 = QLabel("")
        self.graphvo33 = pg.PlotWidget()
        self.graphvo33.setBackground('w')

        layout_vosub3.addWidget(self.label_vo33)
        layout_vosub3.addWidget(self.label_diff33)
        layout_vosub3.addWidget(self.graphvo33)
        grp_vosub3.setLayout(layout_vosub3)

        grp_ir1 = QGroupBox("IR")
        layout_ir1 = QVBoxLayout()
        self.label_ir3 = QLabel("")
        self.graphir3 = pg.PlotWidget()
        self.graphir3.setBackground('w')

        layout_ir1.addWidget(self.label_ir3)
        layout_ir1.addWidget(self.graphir3)
        grp_ir1.setLayout(layout_ir1)

        layout_sub1.addWidget(grp_vosub1)
        layout_sub1.addWidget(grp_vosub2)
        layout_sub1.addWidget(grp_vosub3)
        layout_sub1.addWidget(grp_ir1)
        
        grp_sub1.setLayout(layout_sub1)
        main_layer.addWidget(grp_sub1)

        # Sub ETC : ISMPS, PO, IINR, TEMP, VRS, VST, STATUS, VPEAK
        grp_etcsub1 = QGroupBox("ISMPS, PO, IINR, TEMP, VRS, VST, STATUS, VPEAK")
        layout_etcsub1 = QHBoxLayout()
        
        self.graphio3 = pg.PlotWidget()
        self.graphetc3 = pg.PlotWidget()

        layout_etcsub1.addWidget(self.graphio3)
        layout_etcsub1.addWidget(self.graphetc3)
        grp_etcsub1.setLayout(layout_etcsub1)
        main_layer.addWidget(grp_etcsub1)


    def displayTab4(self, tab):
        # TAB 4 Display
        main_layer = QVBoxLayout()
        tab.setLayout(main_layer)

        # Sub VO 1
        grp_sub1 = QGroupBox("Moduel4 VO")
        layout_sub1 = QHBoxLayout()

        grp_vosub1 = QGroupBox("1 VO")
        layout_vosub1 = QVBoxLayout()
        self.label_vo41 = QLabel("")
        self.label_diff41 = QLabel("")
        self.graphvo41 = pg.PlotWidget()
        self.graphvo41.setBackground('w')

        layout_vosub1.addWidget(self.label_vo41)
        layout_vosub1.addWidget(self.label_diff41)
        layout_vosub1.addWidget(self.graphvo41)
        grp_vosub1.setLayout(layout_vosub1)

        grp_vosub2 = QGroupBox("2 VO")
        layout_vosub2 = QVBoxLayout()
        self.label_vo42 = QLabel("")
        self.label_diff42 = QLabel("")
        self.graphvo42 = pg.PlotWidget()
        self.graphvo42.setBackground('w')

        layout_vosub2.addWidget(self.label_vo42)
        layout_vosub2.addWidget(self.label_diff42)
        layout_vosub2.addWidget(self.graphvo42)
        grp_vosub2.setLayout(layout_vosub2)


        grp_vosub3 = QGroupBox("3 VO")
        layout_vosub3 = QVBoxLayout()
        self.label_vo43 = QLabel("")
        self.label_diff43 = QLabel("")
        self.graphvo43 = pg.PlotWidget()
        self.graphvo43.setBackground('w')

        layout_vosub3.addWidget(self.label_vo43)
        layout_vosub3.addWidget(self.label_diff43)
        layout_vosub3.addWidget(self.graphvo43)
        grp_vosub3.setLayout(layout_vosub3)

        grp_ir1 = QGroupBox("IR")
        layout_ir1 = QVBoxLayout()
        self.label_ir4 = QLabel("")
        self.graphir4 = pg.PlotWidget()
        self.graphir4.setBackground('w')

        layout_ir1.addWidget(self.label_ir4)
        layout_ir1.addWidget(self.graphir4)
        grp_ir1.setLayout(layout_ir1)


        layout_sub1.addWidget(grp_vosub1)
        layout_sub1.addWidget(grp_vosub2)
        layout_sub1.addWidget(grp_vosub3)
        layout_sub1.addWidget(grp_ir1)
        
        grp_sub1.setLayout(layout_sub1)
        main_layer.addWidget(grp_sub1)


        # Sub ETC : ISMPS, PO, IINR, TEMP, VRS, VST, STATUS, VPEAK
        grp_etcsub1 = QGroupBox("ISMPS, PO, IINR, TEMP, VRS, VST, STATUS, VPEAK")
        layout_etcsub1 = QHBoxLayout()
        
        self.graphio4 = pg.PlotWidget()
        self.graphetc4 = pg.PlotWidget()

        layout_etcsub1.addWidget(self.graphio4)
        layout_etcsub1.addWidget(self.graphetc4)
        grp_etcsub1.setLayout(layout_etcsub1)
        main_layer.addWidget(grp_etcsub1)

    def updateViews(self):
        ## view has resized; update auxiliary views to match
        # global self.mainAxis, self.axisB, self.axisC
        self.axisB.setGeometry(self.mainAxis.vb.sceneBoundingRect())
        self.axisC.setGeometry(self.mainAxis.vb.sceneBoundingRect())
        self.axisB.linkedViewChanged(self.mainAxis.vb, self.axisB.XAxis)
        self.axisC.linkedViewChanged(self.mainAxis.vb, self.axisC.XAxis)


    # DATA TAB
    def displayModiData(self, tab):
        # Tab Data Display
        main_layer = QHBoxLayout()
        tab.setLayout(main_layer)

        grp_first = QGroupBox()
        first_layout = QVBoxLayout()
        grp_first.setLayout(first_layout)

        self.data_vol0 = DataBlock("OUTPUT POWER", 1, 42, 0, 42.00, "KW", first_layout)
        self.data_vol1 = DataBlock("SET BASE VOLTAGE", 2, 70, 0, 70.00, "KV", first_layout)
        self.data_vol2 = DataBlock("SET BASE CURRENT", 3, 600, 10, 600, "mA", first_layout)
        self.data_vol3 = DataBlock("SPARK RATE", 4, 1, 1, 120, "A/m", first_layout)
        self.data_vol4 = DataBlock("QUENCH", 5, 1000, 4, 1000, "ms", first_layout)
        self.data_vol5 = DataBlock("SETBACK", 6, 6, 0.1, 30.0, "%", first_layout)
        self.data_vol6 = DataBlock("SETBACK TIME",7, 1000, 0, 1000, "ms", first_layout)
        self.data_vol7 = DataBlock("UV LEVEL",8, 10, 0, 20.00, "kV", first_layout)
        self.data_vol8 = DataBlock("UV TIME DELAY", 9, 30, 0, 45, "S", first_layout)
        self.data_vol9 = DataBlock("IE MODE ON TIME", 11, 1, 1, 999,"mS", first_layout)

        self.data_vol10 = DataBlock("IE MODE OFF TIME", 12, 0, 0, 999, "mS", first_layout)
        self.data_vol11 = DataBlock("0:LOCAL , 1:Ext.Net, 2:Ext.Di", 10, 0, 0, 1, "", first_layout)
        self.data_vol12 = DataBlock("OVER TEMPERATURE LEVEL", 19, 700, 0, 1000, "oC", first_layout)
        self.data_vol13 = DataBlock("VOLTAGE OVER PROTECT LEVEL", 20, 90, 0, 95.00, "kV", first_layout)
        
        # first_layout.addWidget(self.data_po)

        grp_second = QGroupBox()
        second_layout = QVBoxLayout()
        grp_second.setLayout(second_layout)

        self.data_10 = DataBlock("CURRENT OVER PROTECT LEVEL", 21, 1100, 0, 1500, "mA", second_layout)
        self.data_11 = DataBlock("START REFERENCE TIME", 22, 5, 1, 9000, "ms", second_layout)
        self.data_12 = DataBlock("SPARK PERCENT SETTING", 23, 50, 10, 90, "%", second_layout)
        self.data_13 = DataBlock("ANALOG OUTPUT VOLTAGE GAIN", 24, 0, -999, 999,"", second_layout)
        self.data_14 = DataBlock("ANALOG OUTPUT CURRENT GAIN", 25, 0, -999, 999,"", second_layout)
        self.data_15 = DataBlock("MD1 BASE VOLTAGE OFFSET", 26, 0, -999, 999, "", second_layout)
        self.data_16 = DataBlock("MD1 BASE CURRENT OFFSET", 27, 0, -999, 999, "", second_layout)
        self.data_17 = DataBlock("MD1 BASE VOLTAGE GAIN", 28, 0, -999, 999, "", second_layout)
        self.data_18 = DataBlock("MD1 BASE CURRENT GAIN", 29, 0, -999, 999, "%", second_layout)
        self.data_19 = DataBlock("IR UNBALANCE CHECK LEVEL", 30, 100, 10, 100, "%", second_layout)

        self.data_20 = DataBlock("SPARK PERCENT SETTING", 23, 50, 10, 90, "%", second_layout)
        self.data_21 = DataBlock("SPM ( spark per min )", 32, 1, 1, 120, "A/m", second_layout)
        self.data_22 = DataBlock("SPARK AVG COUNT", 33, 5, 1, 9000, "", second_layout)
        self.data_23 = DataBlock("SPARK LATCH TIME", 34, 5, 1, 9000, "", second_layout)

        grp_third = QGroupBox()
        third_layout = QVBoxLayout()
        grp_third.setLayout(third_layout)

        self.data_i10 = DataBlock("REDUCE LEVEL", 35, 70, 0, 70.00, "", third_layout)
        self.data_i11 = DataBlock("REDUCE TIME", 36, 60, 10, 6000, "sec", third_layout)
        self.data_i12 = DataBlock("MD2 BASE VOLTAGE OFFSET", 37, 0, -999, 999, "", third_layout)
        self.data_i13 = DataBlock("MD2 BASE CURRENT OFFSET", 38, 0, -999, 999, "", third_layout)
        self.data_i14 = DataBlock("MD2 BASE VOLTAGE GAIN", 39, 0, -999, 999, "", third_layout)
        self.data_i15 = DataBlock("MD2 BASE CURRENT GAIN", 40, 0, -999, 999, "", third_layout)
        self.data_i16 = DataBlock("MD3 BASE VOLTAGE OFFSET", 41, 0, -999, 999, "", third_layout)
        self.data_i17 = DataBlock("MD3 BASE CURRENT OFFSET", 42, 0, -999, 999, "", third_layout)
        self.data_i18 = DataBlock("MD3 BASE VOLTAGE GAIN", 43, 0, -999, 999, "", third_layout)
        self.data_i19 = DataBlock("MD3 BASE CURRENT GAIN", 44, 0, -999, 999, "", third_layout)
        self.data_i20 = DataBlock("MD4 BASE VOLTAGE OFFSET", 45, 0, -999, 999, "", third_layout)
        self.data_i21 = DataBlock("MD4 BASE CURRENT OFFSET", 46, 0, -999, 999, "", third_layout)
        self.data_i22 = DataBlock("MD4 BASE VOLTAGE GAIN", 47, 0, -999, 999, "", third_layout)
        self.data_i23 = DataBlock("MD4 BASE CURRENT GAIN", 48, 0, -999, 999, "", third_layout)

        grp_forth = QGroupBox()

        forth_layout = QVBoxLayout()
        grp_forth.setLayout(forth_layout)

        self.data_j10 = DataBlock("MD1 OTP GAIN", 49, 0, -999, 999, "", forth_layout)
        self.data_j11 = DataBlock("MD2 OTP GAIN", 50, 0, -999, 999, "", forth_layout)
        self.data_j12 = DataBlock("MD3 OTP GAIN", 51, 0, -999, 999, "", forth_layout)
        self.data_j13 = DataBlock("MD4 OTP GAIN", 52, 0, -999, 999, "", forth_layout)
        self.data_j14 = DataBlock("MD1 IIN GAIN", 53, 0, -999, 999, "", forth_layout)
        self.data_j15 = DataBlock("MD2 IIN GAIN", 54, 0, -999, 999, "", forth_layout)
        self.data_j16 = DataBlock("MD3 IIN GAIN", 55, 0, -999, 999, "", forth_layout)
        self.data_j17 = DataBlock("MD4 IIN GAIN", 56, 0, -999, 999, "", forth_layout)
        
        self.data_j18 = DataBlock("MD1 VRS GAIN", 57, 0, -999, 999, "", forth_layout)
        self.data_j19 = DataBlock("MD2 VRS GAIN", 58, 0, -999, 999, "", forth_layout)
        self.data_j20 = DataBlock("MD3 VRS GAIN", 59, 0, -999, 999, "", forth_layout)

        main_layer.addWidget(grp_first)
        main_layer.addWidget(grp_second)
        main_layer.addWidget(grp_third)
        main_layer.addWidget(grp_forth)

    # 전원 장치와 송신을 위한 설정
    def setting_generator(self):
        if self.com_open_flag == False:
            Dialog = QDialog()
            self.com_open_flag == True
            dialog = SettingWin(Dialog)
            dialog.show()
            response = dialog.exec_()

            # OK 를 하면 설정 값을 읽어와서 통신을 한다.
            if response == QDialog.Accepted:
                self.gen_port = dialog.gen_port
                self.com_speed = dialog.com_speed
                self.com_data = dialog.com_data
                self.com_parity = dialog.com_parity
                self.com_stop = dialog.com_stop
                self.com_open_flag = False
                self.client = device.make_connect(
                    port=self.gen_port, ptype='rtu',
                    speed=self.com_speed, bytesize=self.com_data, 
                    parity=self.com_parity, stopbits=self.com_stop
                )

                if self.client:
                    print(self.client)
                    self.label_gen_port.setText(self.gen_port)
                    self.btn_com_gen.hide()
                    self.btn_start_data.setEnabled(True)
                    
        else:
            print("Open Dialog")

    # RUN DEVICE & START DATA 
    def run_device(self):
        # 전원 장치를 시작하고, 데이터를 읽어옴
        # device.device_run(self.client)
        print("device.device_run(self.client)")
        time.sleep(1)

        # 데이터를 가져오기 시작하면 STOP 버턴 활성화
        self.stopFlag = False

        # setting timer
        self.timer.setInterval(self.period*1000)
        self.timer.timeout.connect(self.get_data)
        self.timer.start()

    # 종료
    def stop_device(self):
        print("Stop DATA")
        self.timer.stop()

    # 읽어 오는 주기 변경 
    def change_readterm(self):
        self.timer.stop()
        self.period = self.edit_readterm.value()
        self.timer.setInterval(self.period*1000)
        # self.timer.start()

    # GET DATA & PROCESS
    def get_data(self):
        main = device.read_registers(self.client, device.READ_PO, 4)
        # main = device.read_registers_main(self.client, device.READ_PO)

        if main:
            self.main_processing(main)

        result = device.read_registers(self.client, device.AD1_1_VO1, 46)
        # result = device.read_registers_simu(self.client, device.AD1_1_VO1)
        if result:
            self.data_processing(result, 1)

        # result = device.read_registers(self.client, device.AD2_1_VO1, 46)
        # # result = device.read_registers_simu(self.client, device.AD2_1_VO1)
        # if result:
        #     self.data_processing(result, 2)

        # result = device.read_registers(self.client, device.AD3_1_VO1, 46)
        # # result = device.read_registers_simu(self.client, device.AD3_1_VO1)
        # if result:
        #     self.data_processing(result, 3)

        # result = device.read_registers(self.client, device.AD4_1_VO1, 46)
        # # result = device.read_registers_simu(self.client, device.AD4_1_VO1)
        # if result:
        #     self.data_processing(result, 4)


    # DATA PROCESSing
    def main_processing(self, main):
        self.index += 1
        self.idx_list = np.append(self.idx_list, self.index)
        
        power = round(main[0]/100, 2)
        self.po_list = np.append(self.po_list, power)
        voltage = round(main[1]/100, 2)
        self.vo_list = np.append(self.vo_list, voltage)
        current = main[2]
        self.io_list = np.append(self.io_list, current)
        tempeature = round(main[3]/10, 2)
        self.temp_list = np.append(self.temp_list, tempeature)

        self.label_mainpower.setText(str(power))
        self.label_mainvoltage.setText(str(voltage))
        self.label_maincurrent.setText(str(current))
        self.label_maintemp.setText(str(tempeature))

        # Main Window
        self.updateViews()
        self.mainAxis.vb.sigResized.connect(self.updateViews)

        self.graph_po = self.mainAxis.plot(self.idx_list, self.po_list, pen=self.pen_sumvo1, name="PO")
        
        self.itemVO= pg.PlotCurveItem(self.idx_list, self.vo_list, pen=self.pen_io1, name="VO")
        self.graph_vo = self.axisB.addItem(self.itemVO)

        self.itemIO = pg.PlotCurveItem(self.idx_list, self.io_list, pen=self.pen_ir, name="IO")
        self.graph_vbias = self.axisC.addItem(self.itemIO)


    # DATA PROCESSing
    def data_processing(self, result, modul):
        # 반복해서 데이터를 저장하는 경우
        temp_vo1 = []
        temp_vo2 = []
        temp_vo3 = []

        temp_diff1 = []
        temp_diff2 = []
        temp_diff3 = []

        irlist = []

        if modul == 1:
            # self.vo_index1 += 1
            # self.vo_indexs1 = np.append(self.vo_indexs1, self.vo_index1)
            
            
            # temp_vo1.append(result[idx])
            # self.VO1_2 = np.append(self.VO1_2, result[10+idx])
            # temp_vo2.append(result[10+idx])
            # self.VO1_3 = np.append(self.VO1_3, result[20+idx])
            # temp_vo3.append(result[20+idx])

            # if idx == 0:
            #     diff = result[idx] - 0
            #     temp_diff1.append(diff)
            #     self.VD1_1 = np.append(self.VD1_1, diff)
            #     diff = result[idx+10] - 0
            #     temp_diff2.append(diff)
            #     self.VD1_2 = np.append(self.VD1_2, diff)
            #     diff = result[20] - 0
            #     temp_diff3.append(diff)
            #     self.VD1_3 = np.append(self.VD1_3, diff)
            # else:
            #     diff = result[idx] - result[idx-1]
            #     temp_diff1.append(diff)
            #     self.VD1_1 = np.append(self.VD1_1, diff)
            #     diff = result[idx+10] - result[idx+9]
            #     temp_diff2.append(diff)
            #     self.VD1_2 = np.append(self.VD1_2, diff)
            #     diff = result[idx+20] - result[idx+19]
            #     temp_diff3.append(diff)
            #     self.VD1_3 = np.append(self.VD1_3, diff)

            # self.IR1 = np.append(self.IR1, result[31+idx])
            # irlist.append(result[31+idx])

            # 1회성 데이터 저장
            self.com_index1 += 1

            self.com_indexs1 = np.append(self.com_indexs1, self.com_index1)

            self.VO1_1 = np.append(self.VO1_1, result[0])
            self.VO1_2 = np.append(self.VO1_2, result[1])
            self.VO1_3 = np.append(self.VO1_3, result[2])
            self.VO1_4 = np.append(self.VO1_4, result[3])
            self.VO1_5 = np.append(self.VO1_5, result[4])
            self.VO1_6 = np.append(self.VO1_6, result[5])
            self.VO1_7 = np.append(self.VO1_7, result[6])
            self.VO1_8 = np.append(self.VO1_8, result[7])

            self.VOSUM1_1 = np.append(self.VOSUM1_1, result[8])
            self.IO1_1 = np.append(self.IO1_1, result[9])

            self.VOSUM1_2 = np.append(self.VOSUM1_2, result[18])
            self.IO1_2 = np.append(self.IO1_2, result[19])

            self.VOSUM1_3 = np.append(self.VOSUM1_3, result[28])
            self.IO1_3 = np.append(self.IO1_3, result[29])

            self.IR1_1 = np.append(self.IR1_1, result[30])
            self.IR1_2 = np.append(self.IR1_2, result[31])
            self.IR1_3 = np.append(self.IR1_3, result[32])
            self.IR1_4 = np.append(self.IR1_4, result[33])
            self.IR1_5 = np.append(self.IR1_5, result[34])
            self.IR1_6 = np.append(self.IR1_6, result[35])
            self.IR1_7 = np.append(self.IR1_7, result[36])
            self.IR1_8 = np.append(self.IR1_8, result[37])

            self.ISPMS1 = np.append(self.ISPMS1, result[39])
            self.PO1 = np.append(self.PO1, result[40])
            self.IINR1 = np.append(self.IINR1, result[41])
            self.TEMP1 = np.append(self.TEMP1, result[42])
            self.VRS1 = np.append(self.VRS1, result[43])
            self.VST1 = np.append(self.VST1, result[44])
            self.VPEAK1 = np.append(self.VPEAK1, result[45])

            # self.label_vo11.setText(", ".join(str(e) for e in temp_vo1))
            # self.label_diff11.setText(", ".join(str(e) for e in temp_diff1))

            # self.label_vo12.setText(", ".join(str(e) for e in temp_vo2))
            # self.label_diff12.setText(", ".join(str(e) for e in temp_diff2))

            # self.label_vo13.setText(", ".join(str(e) for e in temp_vo3))
            # self.label_diff13.setText(", ".join(str(e) for e in temp_diff3))

            # self.label_ir1.setText(", ".join(str(e) for e in irlist))
            self.draw_graph(1)

            # TEMP
            if self.com_index1 == 40:
                self.timer.stop()

        elif modul == 2:
            for idx in range(8):
                self.vo_index2 += 1
                self.vo_indexs2 = np.append(self.vo_indexs2, self.vo_index2)
                
                self.VO2_1 = np.append(self.VO2_1, result[idx])
                temp_vo1.append(result[idx])
                self.VO2_2 = np.append(self.VO2_2, result[10+idx])
                temp_vo2.append(result[10+idx])
                self.VO2_3 = np.append(self.VO2_3, result[20+idx])
                temp_vo3.append(result[20+idx])

                if idx == 0:
                    diff = result[idx] - 0
                    temp_diff1.append(diff)
                    self.VD2_1 = np.append(self.VD2_1, diff)
                    diff = result[idx+10] - 0
                    temp_diff2.append(diff)
                    self.VD2_2 = np.append(self.VD2_2, diff)
                    diff = result[20] - 0
                    temp_diff3.append(diff)
                    self.VD2_3 = np.append(self.VD2_3, diff)
                else:
                    diff = result[idx] - result[idx-1]
                    temp_diff1.append(diff)
                    self.VD2_1 = np.append(self.VD2_1, diff)
                    diff = result[idx+10] - result[idx+9]
                    temp_diff2.append(diff)
                    self.VD2_2 = np.append(self.VD2_2, diff)
                    diff = result[idx+20] - result[idx+19]
                    temp_diff3.append(diff)
                    self.VD2_3 = np.append(self.VD2_3, diff)

                self.IR2 = np.append(self.IR2, result[31+idx])
                irlist.append(result[31+idx])

            # 1회성 데이터 저장
            self.com_index2 += 1

            self.com_indexs2 = np.append(self.com_indexs2, self.com_index2)

            self.VOSUM2_1 = np.append(self.VOSUM2_1, result[8])
            self.IO2_1 = np.append(self.IO2_1, result[9])

            self.VOSUM2_2 = np.append(self.VOSUM2_2, result[18])
            self.IO2_2 = np.append(self.IO2_2, result[19])

            self.VOSUM2_3 = np.append(self.VOSUM2_3, result[28])
            self.IO2_3 = np.append(self.IO2_3, result[29])

            self.ISPMS2 = np.append(self.ISPMS2, result[39])
            self.PO2 = np.append(self.PO2, result[40])
            self.IINR2 = np.append(self.IINR2, result[41])
            self.TEMP2 = np.append(self.TEMP2, result[42])
            self.VRS2 = np.append(self.VRS2, result[43])
            self.VST2 = np.append(self.VST2, result[44])
            self.VPEAK2 = np.append(self.VPEAK2, result[45])

            self.label_vo21.setText(", ".join(str(e) for e in temp_vo1))
            self.label_diff21.setText(", ".join(str(e) for e in temp_diff1))

            self.label_vo22.setText(", ".join(str(e) for e in temp_vo2))
            self.label_diff22.setText(", ".join(str(e) for e in temp_diff2))

            self.label_vo23.setText(", ".join(str(e) for e in temp_vo3))
            self.label_diff23.setText(", ".join(str(e) for e in temp_diff3))

            self.label_ir2.setText(", ".join(str(e) for e in irlist))

            self.draw_graph(2)

        elif modul == 3:
            for idx in range(8):
                self.vo_index3 += 1
                self.vo_indexs3 = np.append(self.vo_indexs3, self.vo_index3)
                
                self.VO3_1 = np.append(self.VO3_1, result[idx])
                temp_vo1.append(result[idx])
                self.VO3_2 = np.append(self.VO3_2, result[10+idx])
                temp_vo2.append(result[10+idx])
                self.VO3_3 = np.append(self.VO3_3, result[20+idx])
                temp_vo3.append(result[20+idx])

                if idx == 0:
                    diff = result[idx] - 0
                    temp_diff1.append(diff)
                    self.VD3_1 = np.append(self.VD3_1, diff)
                    diff = result[idx+10] - 0
                    temp_diff2.append(diff)
                    self.VD3_2 = np.append(self.VD3_2, diff)
                    diff = result[20] - 0
                    temp_diff3.append(diff)
                    self.VD3_3 = np.append(self.VD3_3, diff)
                else:
                    diff = result[idx] - result[idx-1]
                    temp_diff1.append(diff)
                    self.VD3_1 = np.append(self.VD3_1, diff)
                    diff = result[idx+10] - result[idx+9]
                    temp_diff2.append(diff)
                    self.VD3_2 = np.append(self.VD3_2, diff)
                    diff = result[idx+20] - result[idx+19]
                    temp_diff3.append(diff)
                    self.VD3_3 = np.append(self.VD3_3, diff)

                self.IR3 = np.append(self.IR3, result[31+idx])
                irlist.append(result[31+idx])

            # 1회성 데이터 저장
            self.com_index3 += 1

            self.com_indexs3 = np.append(self.com_indexs3, self.com_index3)

            self.VOSUM3_1 = np.append(self.VOSUM3_1, result[8])
            self.IO3_1 = np.append(self.IO3_1, result[9])

            self.VOSUM3_2 = np.append(self.VOSUM3_2, result[18])
            self.IO3_2 = np.append(self.IO3_2, result[19])

            self.VOSUM3_3 = np.append(self.VOSUM3_3, result[28])
            self.IO3_3 = np.append(self.IO3_3, result[29])

            self.ISPMS3 = np.append(self.ISPMS3, result[39])
            self.PO3 = np.append(self.PO3, result[40])
            self.IINR3 = np.append(self.IINR3, result[41])
            self.TEMP3 = np.append(self.TEMP3, result[42])
            self.VRS3 = np.append(self.VRS3, result[43])
            self.VST3 = np.append(self.VST3, result[44])
            self.VPEAK3 = np.append(self.VPEAK3, result[45])

            self.label_vo31.setText(", ".join(str(e) for e in temp_vo1))
            self.label_diff31.setText(", ".join(str(e) for e in temp_diff1))

            self.label_vo32.setText(", ".join(str(e) for e in temp_vo2))
            self.label_diff32.setText(", ".join(str(e) for e in temp_diff2))

            self.label_vo33.setText(", ".join(str(e) for e in temp_vo3))
            self.label_diff33.setText(", ".join(str(e) for e in temp_diff3))

            self.label_ir3.setText(", ".join(str(e) for e in irlist))

            self.draw_graph(3)

        elif modul == 4:
            for idx in range(8):
                self.vo_index4 += 1
                self.vo_indexs4 = np.append(self.vo_indexs4, self.vo_index4)
                
                self.VO4_1 = np.append(self.VO4_1, result[idx])
                temp_vo1.append(result[idx])
                self.VO4_2 = np.append(self.VO4_2, result[10+idx])
                temp_vo2.append(result[10+idx])
                self.VO4_3 = np.append(self.VO4_3, result[20+idx])
                temp_vo3.append(result[20+idx])

                if idx == 0:
                    diff = result[idx] - 0
                    temp_diff1.append(diff)
                    self.VD4_1 = np.append(self.VD4_1, diff)
                    diff = result[idx+10] - 0
                    temp_diff2.append(diff)
                    self.VD4_2 = np.append(self.VD4_2, diff)
                    diff = result[20] - 0
                    temp_diff3.append(diff)
                    self.VD4_3 = np.append(self.VD4_3, diff)
                else:
                    diff = result[idx] - result[idx-1]
                    temp_diff1.append(diff)
                    self.VD4_1 = np.append(self.VD4_1, diff)
                    diff = result[idx+10] - result[idx+9]
                    temp_diff2.append(diff)
                    self.VD4_2 = np.append(self.VD4_2, diff)
                    diff = result[idx+20] - result[idx+19]
                    temp_diff3.append(diff)
                    self.VD4_3 = np.append(self.VD4_3, diff)

                self.IR4 = np.append(self.IR4, result[31+idx])
                irlist.append(result[31+idx])

            # 1회성 데이터 저장
            self.com_index4 += 1

            self.com_indexs4 = np.append(self.com_indexs4, self.com_index4)

            self.VOSUM4_1 = np.append(self.VOSUM4_1, result[8])
            self.IO4_1 = np.append(self.IO4_1, result[9])

            self.VOSUM4_2 = np.append(self.VOSUM4_2, result[18])
            self.IO4_2 = np.append(self.IO4_2, result[19])

            self.VOSUM4_3 = np.append(self.VOSUM4_3, result[28])
            self.IO4_3 = np.append(self.IO4_3, result[29])

            self.ISPMS4 = np.append(self.ISPMS4, result[39])
            self.PO4 = np.append(self.PO4, result[40])
            self.IINR4 = np.append(self.IINR4, result[41])
            self.TEMP4 = np.append(self.TEMP4, result[42])
            self.VRS4 = np.append(self.VRS4, result[43])
            self.VST4 = np.append(self.VST4, result[44])
            self.VPEAK4 = np.append(self.VPEAK4, result[45])

            self.label_vo41.setText(", ".join(str(e) for e in temp_vo1))
            self.label_diff41.setText(", ".join(str(e) for e in temp_diff1))

            self.label_vo42.setText(", ".join(str(e) for e in temp_vo2))
            self.label_diff42.setText(", ".join(str(e) for e in temp_diff2))

            self.label_vo43.setText(", ".join(str(e) for e in temp_vo3))
            self.label_diff43.setText(", ".join(str(e) for e in temp_diff3))

            self.label_ir4.setText(", ".join(str(e) for e in irlist))

            self.draw_graph(4)

    # DRAW GRAPH
    def draw_graph(self, module):

        self.pen_vo = pg.mkPen(width=2, color=(0, 255,0))
        self.pen_diff = pg.mkPen(width=2, color=(204, 0, 0))
        self.pen_ir = pg.mkPen(width=2, color=(0, 0, 255))
        self.pen_sumvo1 = pg.mkPen(width=2, color=(255, 0, 0))
        self.pen_io1 = pg.mkPen(width=2, color=(0, 255, 0))
        self.pen_sumvo2 = pg.mkPen(width=2, color=(255, 128, 0))
        self.pen_io2 = pg.mkPen(width=2, color=(0, 255, 128))
        self.pen_sumvo3 = pg.mkPen(width=2, color=(255, 255, 0))
        self.pen_io3 = pg.mkPen(width=2, color=(0, 255, 255))

        if module == 1:
            # 8개 증가 하는 것
            self.mod1_vo1 =  self.graphvo11.plot(self.com_indexs1, self.VO1_1, pen=self.pen_vo, name="VO1")
            self.mod1_vo2 =  self.graphvo11.plot(self.com_indexs1, self.VO1_2, pen=self.pen_diff, name="VO2")
            self.mod1_vo3 =  self.graphvo11.plot(self.com_indexs1, self.VO1_3, pen=self.pen_ir, name="VO3")
            self.mod1_vo4 =  self.graphvo11.plot(self.com_indexs1, self.VO1_4, pen=self.pen_sumvo1, name="VO3")
            self.mod1_vo5 =  self.graphvo11.plot(self.com_indexs1, self.VO1_5, pen=self.pen_io1, name="VO3")
            self.mod1_vo6 =  self.graphvo11.plot(self.com_indexs1, self.VO1_6, pen=self.pen_sumvo2, name="VO3")
            self.mod1_vo7 =  self.graphvo11.plot(self.com_indexs1, self.VO1_7, pen=self.pen_io2, name="VO3")
            self.mod1_vo8 =  self.graphvo11.plot(self.com_indexs1, self.VO1_8, pen=self.pen_sumvo3, name="VO3")

            self.mod1_ir1 =  self.graphir1.plot(self.com_indexs1, self.IR1_1, pen=self.pen_vo, name="IR1")
            self.mod1_ir2 =  self.graphir1.plot(self.com_indexs1, self.IR1_2, pen=self.pen_diff, name="IR2")
            self.mod1_ir3 =  self.graphir1.plot(self.com_indexs1, self.IR1_3, pen=self.pen_ir, name="IR3")
            self.mod1_ir4 =  self.graphir1.plot(self.com_indexs1, self.IR1_4, pen=self.pen_sumvo1, name="IR4")
            self.mod1_ir5 =  self.graphir1.plot(self.com_indexs1, self.IR1_5, pen=self.pen_io1, name="IR5")
            self.mod1_ir6 =  self.graphir1.plot(self.com_indexs1, self.IR1_6, pen=self.pen_sumvo2, name="IR6")
            self.mod1_ir7 =  self.graphir1.plot(self.com_indexs1, self.IR1_7, pen=self.pen_io2, name="IR7")
            self.mod1_ir8 =  self.graphir1.plot(self.com_indexs1, self.IR1_8, pen=self.pen_sumvo3, name="IR8")

            # 모듈별 1개씩 증가        
            self.mod1_io1 =  self.graphio1.plot(self.com_indexs1, self.VOSUM1_1, pen=self.pen_sumvo1, name="VO1 SUM")
            self.mod1_io1 =  self.graphio1.plot(self.com_indexs1, self.IO1_1, pen=self.pen_io1, name="IO1")

            self.mod1_io1 =  self.graphio1.plot(self.com_indexs1, self.VOSUM1_2, pen=self.pen_sumvo2, name="VO2 SUM")
            self.mod1_io1 =  self.graphio1.plot(self.com_indexs1, self.IO1_2, pen=self.pen_io2, name="IO2")
        
            self.mod1_io1 =  self.graphio1.plot(self.com_indexs1, self.VOSUM1_3, pen=self.pen_sumvo3, name="VO3 SUM")
            self.mod1_io1 =  self.graphio1.plot(self.com_indexs1, self.IO1_3, pen=self.pen_io3, name="IO3")

            self.mod1_etc1 =  self.graphetc1.plot(self.com_indexs1, self.ISPMS1, pen=self.pen_ismps, name="ISPMS1")
            self.mod1_etc1 =  self.graphetc1.plot(self.com_indexs1, self.PO1, pen=self.pen_po, name="PO1")
            self.mod1_etc1 =  self.graphetc1.plot(self.com_indexs1, self.IINR1, pen=self.pen_iinr, name="IINR1")
            self.mod1_etc1 =  self.graphetc1.plot(self.com_indexs1, self.TEMP1, pen=self.pen_temp, name="TEMP1")
            self.mod1_etc1 =  self.graphetc1.plot(self.com_indexs1, self.VRS1, pen=self.pen_vrs, name="VRS1")
            self.mod1_etc1 =  self.graphetc1.plot(self.com_indexs1, self.VST1, pen=self.pen_vst, name="VST1")
            self.mod1_etc1 =  self.graphetc1.plot(self.com_indexs1, self.VPEAK1, pen=self.pen_vpeak, name="VPEAK1")

        elif module == 2:
            # 8개 증가 하는 것
            self.mod2_vo1 =  self.graphvo21.plot(self.vo_indexs2, self.VO2_1, pen=self.pen_vo, name="VO1")
            self.mod2_vd1 =  self.graphvo21.plot(self.vo_indexs2, self.VD2_1, pen=self.pen_diff, name="DIFF1")
            self.mod2_vo2 =  self.graphvo22.plot(self.vo_indexs2, self.VO2_2, pen=self.pen_vo, name="VO2")
            self.mod2_vd2 =  self.graphvo22.plot(self.vo_indexs2, self.VD2_2, pen=self.pen_diff, name="DIFF2")
            self.mod2_vo3 =  self.graphvo23.plot(self.vo_indexs2, self.VO2_3, pen=self.pen_vo, name="VO3")
            self.mod2_vd3 =  self.graphvo23.plot(self.vo_indexs2, self.VD2_3, pen=self.pen_diff, name="DIFF3")

            self.mod2_ir1 =  self.graphir2.plot(self.vo_indexs2, self.IR2, pen=self.pen_ir, name="VO3")

            # 모듈별 1개씩 증가        
            self.mod2_io1 =  self.graphio2.plot(self.com_indexs2, self.VOSUM2_1, pen=self.pen_sumvo1, name="VO1 SUM")
            self.mod2_io1 =  self.graphio2.plot(self.com_indexs2, self.IO2_1, pen=self.pen_io1, name="IO1")
            self.mod2_io1 =  self.graphio2.plot(self.com_indexs2, self.VOSUM2_2, pen=self.pen_sumvo2, name="VO2 SUM")
            self.mod2_io1 =  self.graphio2.plot(self.com_indexs2, self.IO2_2, pen=self.pen_io2, name="IO2")
            self.mod2_io1 =  self.graphio2.plot(self.com_indexs2, self.VOSUM2_3, pen=self.pen_sumvo3, name="VO3 SUM")
            self.mod2_io1 =  self.graphio2.plot(self.com_indexs2, self.IO2_3, pen=self.pen_io3, name="IO3")

            self.mod2_etc1 =  self.graphetc2.plot(self.com_indexs2, self.ISPMS2, pen=self.pen_ismps, name="ISPMS1")
            self.mod2_etc1 =  self.graphetc2.plot(self.com_indexs2, self.PO2, pen=self.pen_po, name="PO1")
            self.mod2_etc1 =  self.graphetc2.plot(self.com_indexs2, self.IINR2, pen=self.pen_iinr, name="IINR1")
            self.mod2_etc1 =  self.graphetc2.plot(self.com_indexs2, self.TEMP2, pen=self.pen_temp, name="TEMP1")
            self.mod2_etc1 =  self.graphetc2.plot(self.com_indexs2, self.VRS2, pen=self.pen_vrs, name="VRS1")
            self.mod2_etc1 =  self.graphetc2.plot(self.com_indexs2, self.VST2, pen=self.pen_vst, name="VST1")
            self.mod2_etc1 =  self.graphetc2.plot(self.com_indexs2, self.VPEAK2, pen=self.pen_vpeak, name="VPEAK1")

        elif module == 3:
            # 8개 증가 하는 것
            self.mod3_vo1 =  self.graphvo31.plot(self.vo_indexs3, self.VO3_1, pen=self.pen_vo, name="VO1")
            self.mod3_vd1 =  self.graphvo31.plot(self.vo_indexs3, self.VD3_1, pen=self.pen_diff, name="DIFF1")
            self.mod3_vo2 =  self.graphvo32.plot(self.vo_indexs3, self.VO3_2, pen=self.pen_vo, name="VO2")
            self.mod3_vd2 =  self.graphvo32.plot(self.vo_indexs3, self.VD3_2, pen=self.pen_diff, name="DIFF2")
            self.mod3_vo3 =  self.graphvo33.plot(self.vo_indexs3, self.VO3_3, pen=self.pen_vo, name="VO3")
            self.mod3_vd3 =  self.graphvo33.plot(self.vo_indexs3, self.VD3_3, pen=self.pen_diff, name="DIFF3")

            self.mod3_ir1 =  self.graphir3.plot(self.vo_indexs3, self.IR3, pen=self.pen_ir, name="VO3")

            # 모듈별 1개씩 증가        
            self.mod3_io1 =  self.graphio3.plot(self.com_indexs3, self.VOSUM3_1, pen=self.pen_sumvo1, name="VO1 SUM")
            self.mod3_io1 =  self.graphio3.plot(self.com_indexs3, self.IO3_1, pen=self.pen_io1, name="IO1")

            self.mod3_io1 =  self.graphio3.plot(self.com_indexs3, self.VOSUM3_2, pen=self.pen_sumvo2, name="VO2 SUM")
            self.mod3_io1 =  self.graphio3.plot(self.com_indexs3, self.IO3_2, pen=self.pen_io2, name="IO2")
        
            self.mod3_io1 =  self.graphio3.plot(self.com_indexs3, self.VOSUM3_3, pen=self.pen_sumvo3, name="VO3 SUM")
            self.mod3_io1 =  self.graphio3.plot(self.com_indexs3, self.IO3_3, pen=self.pen_io3, name="IO3")

            self.mod3_etc1 =  self.graphetc3.plot(self.com_indexs3, self.ISPMS3, pen=self.pen_ismps, name="ISPMS1")
            self.mod3_etc1 =  self.graphetc3.plot(self.com_indexs3, self.PO3, pen=self.pen_po, name="PO1")
            self.mod3_etc1 =  self.graphetc3.plot(self.com_indexs3, self.IINR3, pen=self.pen_iinr, name="IINR1")
            self.mod3_etc1 =  self.graphetc3.plot(self.com_indexs3, self.TEMP3, pen=self.pen_temp, name="TEMP1")
            self.mod3_etc1 =  self.graphetc3.plot(self.com_indexs3, self.VRS3, pen=self.pen_vrs, name="VRS1")
            self.mod3_etc1 =  self.graphetc3.plot(self.com_indexs3, self.VST3, pen=self.pen_vst, name="VST1")
            self.mod3_etc1 =  self.graphetc3.plot(self.com_indexs3, self.VPEAK3, pen=self.pen_vpeak, name="VPEAK1")

        elif module == 4:
            # 8개 증가 하는 것
            self.mod4_vo1 =  self.graphvo41.plot(self.vo_indexs4, self.VO4_1, pen=self.pen_vo, name="VO1")
            self.mod4_vd1 =  self.graphvo41.plot(self.vo_indexs4, self.VD4_1, pen=self.pen_diff, name="DIFF1")

            self.mod4_vo2 =  self.graphvo42.plot(self.vo_indexs4, self.VO4_2, pen=self.pen_vo, name="VO2")
            self.mod4_vd2 =  self.graphvo42.plot(self.vo_indexs4, self.VD4_2, pen=self.pen_diff, name="DIFF2")

            self.mod4_vo3 =  self.graphvo43.plot(self.vo_indexs4, self.VO4_3, pen=self.pen_vo, name="VO3")
            self.mod4_vd3 =  self.graphvo43.plot(self.vo_indexs4, self.VD4_3, pen=self.pen_diff, name="DIFF3")

            self.mod4_ir1 =  self.graphir4.plot(self.vo_indexs4, self.IR4, pen=self.pen_ir, name="VO3")

            # 모듈별 1개씩 증가        
            self.mod4_io1 =  self.graphio4.plot(self.com_indexs4, self.VOSUM4_1, pen=self.pen_sumvo1, name="VO1 SUM")
            self.mod4_io1 =  self.graphio4.plot(self.com_indexs4, self.IO4_1, pen=self.pen_io1, name="IO1")

            self.mod4_io1 =  self.graphio4.plot(self.com_indexs4, self.VOSUM4_2, pen=self.pen_sumvo2, name="VO2 SUM")
            self.mod4_io1 =  self.graphio4.plot(self.com_indexs4, self.IO4_2, pen=self.pen_io2, name="IO2")
        
            self.mod4_io1 =  self.graphio4.plot(self.com_indexs4, self.VOSUM4_3, pen=self.pen_sumvo3, name="VO3 SUM")
            self.mod4_io1 =  self.graphio4.plot(self.com_indexs4, self.IO4_3, pen=self.pen_io3, name="IO3")

            self.mod4_etc1 =  self.graphetc4.plot(self.com_indexs4, self.ISPMS4, pen=self.pen_ismps, name="ISPMS1")
            self.mod4_etc1 =  self.graphetc4.plot(self.com_indexs4, self.PO4, pen=self.pen_po, name="PO1")
            self.mod4_etc1 =  self.graphetc4.plot(self.com_indexs4, self.IINR4, pen=self.pen_iinr, name="IINR1")
            self.mod4_etc1 =  self.graphetc4.plot(self.com_indexs4, self.TEMP4, pen=self.pen_temp, name="TEMP1")
            self.mod4_etc1 =  self.graphetc4.plot(self.com_indexs4, self.VRS4, pen=self.pen_vrs, name="VRS1")
            self.mod4_etc1 =  self.graphetc4.plot(self.com_indexs4, self.VST4, pen=self.pen_vst, name="VST1")
            self.mod4_etc1 =  self.graphetc4.plot(self.com_indexs4, self.VPEAK4, pen=self.pen_vpeak, name="VPEAK1")

    # SAVE DATA
    def save_data(self):
        data = {}
        Dialog = QDialog()
        self.com_open_flag == True
        dialog = DataWin(Dialog)
        dialog.show()
        response = dialog.exec_()

        if response == QDialog.Accepted:
            main_df = pd.DataFrame({
                        'indexs': self.idx_list.tolist(),
                        "PO": self.po_list.tolist(),
                        'VO': self.vo_list.tolist(),
                        'IO': self.io_list.tolist(),
                        'TEMPERATURE': self.temp_list.tolist(),
            })

            tab1_vo_df = pd.DataFrame({
                        'indexs1': self.vo_indexs1.tolist(),
                        "VO1": self.VO1_1.tolist(),
                        'VO1 DIFF': self.VD1_1.tolist(),
                        "VO2": self.VO1_2.tolist(),
                        'VO2 DIFF': self.VD1_2.tolist(),
                        "VO3": self.VO1_3.tolist(),
                        'VO3 DIFF': self.VD1_3.tolist(),
                        "IR": self.IR1.tolist()
            })

            tab1_com_df = pd.DataFrame({
                        'indexs1': self.com_indexs1.tolist(),
                        "VO_SUM1": self.VOSUM1_1.tolist(),
                        'IO1': self.IO1_1.tolist(),
                        "VO_SUM2": self.VOSUM1_2.tolist(),
                        'IO2': self.IO1_2.tolist(),
                        "VO_SUM3": self.VOSUM1_3.tolist(),
                        'IO3': self.IO1_3.tolist(),

                        "ISPMS1": self.ISPMS1.tolist(),
                        "PO1": self.PO1.tolist(),
                        "IINR1": self.IINR1.tolist(),
                        "TEMP1": self.TEMP1.tolist(),
                        "VRS1": self.VRS1.tolist(),
                        "VST1": self.VST1.tolist(),
                        "VPEAK1": self.VPEAK1.tolist(),
            })


            tab2_vo_df = pd.DataFrame({
                        'indexs2': self.vo_indexs2.tolist(),
                        "VO1": self.VO2_1.tolist(),
                        'VO1 DIFF': self.VD2_1.tolist(),
                        "VO2": self.VO2_2.tolist(),
                        'VO2 DIFF': self.VD2_2.tolist(),
                        "VO3": self.VO2_3.tolist(),
                        'VO3 DIFF': self.VD2_3.tolist(),
                        "IR": self.IR2.tolist()
            })

            tab2_com_df = pd.DataFrame({
                        'indexs2': self.com_indexs2.tolist(),
                        "VO_SUM1": self.VOSUM2_1.tolist(),
                        'IO1': self.IO2_1.tolist(),
                        "VO_SUM2": self.VOSUM2_2.tolist(),
                        'IO2': self.IO2_2.tolist(),
                        "VO_SUM3": self.VOSUM2_3.tolist(),
                        'IO3': self.IO2_3.tolist(),

                        "ISPMS1": self.ISPMS2.tolist(),
                        "PO1": self.PO2.tolist(),
                        "IINR1": self.IINR2.tolist(),
                        "TEMP1": self.TEMP2.tolist(),
                        "VRS1": self.VRS2.tolist(),
                        "VST1": self.VST2.tolist(),
                        "VPEAK1": self.VPEAK2.tolist(),
            })

            tab3_vo_df = pd.DataFrame({
                        'indexs3': self.vo_indexs3.tolist(),
                        "VO1": self.VO3_1.tolist(),
                        'VO1 DIFF': self.VD3_1.tolist(),
                        "VO2": self.VO3_2.tolist(),
                        'VO2 DIFF': self.VD3_2.tolist(),
                        "VO3": self.VO3_3.tolist(),
                        'VO3 DIFF': self.VD3_3.tolist(),
                        "IR": self.IR3.tolist()
            })

            tab3_com_df = pd.DataFrame({
                        'index3': self.com_indexs3.tolist(),
                        "VO_SUM1": self.VOSUM3_1.tolist(),
                        'IO1': self.IO3_1.tolist(),
                        "VO_SUM2": self.VOSUM3_2.tolist(),
                        'IO2': self.IO3_2.tolist(),
                        "VO_SUM3": self.VOSUM3_3.tolist(),
                        'IO3': self.IO3_3.tolist(),

                        "ISPMS1": self.ISPMS3.tolist(),
                        "PO1": self.PO3.tolist(),
                        "IINR1": self.IINR3.tolist(),
                        "TEMP1": self.TEMP3.tolist(),
                        "VRS1": self.VRS3.tolist(),
                        "VST1": self.VST3.tolist(),
                        "VPEAK1": self.VPEAK3.tolist(),
            })

            tab4_vo_df = pd.DataFrame({
                        'indexs4': self.vo_indexs4.tolist(),
                        "VO1": self.VO4_1.tolist(),
                        'VO1 DIFF': self.VD4_1.tolist(),
                        "VO2": self.VO4_2.tolist(),
                        'VO2 DIFF': self.VD4_2.tolist(),
                        "VO3": self.VO4_3.tolist(),
                        'VO3 DIFF': self.VD4_3.tolist(),
                        "IR": self.IR3.tolist()
            })

            tab4_com_df = pd.DataFrame({
                        'indexs4': self.com_indexs3.tolist(),
                        "VO_SUM1": self.VOSUM4_1.tolist(),
                        'IO1': self.IO4_1.tolist(),
                        "VO_SUM2": self.VOSUM4_2.tolist(),
                        'IO2': self.IO4_2.tolist(),
                        "VO_SUM3": self.VOSUM4_3.tolist(),
                        'IO3': self.IO4_3.tolist(),

                        "ISPMS1": self.ISPMS4.tolist(),
                        "PO1": self.PO4.tolist(),
                        "IINR1": self.IINR4.tolist(),
                        "TEMP1": self.TEMP4.tolist(),
                        "VRS1": self.VRS4.tolist(),
                        "VST1": self.VST4.tolist(),
                        "VPEAK1": self.VPEAK4.tolist(),
            })


            product = dialog.text_product.text()
            user = dialog.text_user.text()

            now = str(time.time()).split('.')[0]
            subdir = os.path.join('.', 'output')
            if not os.path.exists(subdir):
                os.makedirs(subdir)

            # CSS FILE
            excelfile = f"epc_{product}_{user}_{now}.xlsx"
            print(excelfile)
            excelfile = os.path.join(subdir, excelfile)
            
            writer = pd.ExcelWriter(excelfile, engine='xlsxwriter')

            main_df.to_excel(writer, sheet_name='MAIN')
            tab1_vo_df.to_excel(writer, sheet_name='Module1 Voltage')
            tab1_com_df.to_excel(writer, sheet_name='Module1 Common')

            tab2_vo_df.to_excel(writer, sheet_name='Module2 Voltage')
            tab2_com_df.to_excel(writer, sheet_name='Module2 Common')

            tab3_vo_df.to_excel(writer, sheet_name='Module3 Voltage')
            tab3_com_df.to_excel(writer, sheet_name='Module3 Common')

            tab4_vo_df.to_excel(writer, sheet_name='Module4 Voltage')
            tab4_com_df.to_excel(writer, sheet_name='Module4 Common')

            writer.save()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MainWindow()
    sys.exit(app.exec_())