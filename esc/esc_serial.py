import serial
import logging

from ctypes import c_int16

import socket
from umodbus import conf
from umodbus.client import tcp
from ctypes import c_uint16


## RTU
PTYPE = 'rtu'
# PTYPE = 'tcp'

if PTYPE == 'tcp':
    PORT = 1502
    PTYPE = 'tcp'

# READ_ADDRESS_DEVICE_STATUS = 0x00 
# READ_ADDRESS_DEVICE_WARNING = 0x01
# READ_ADDRESS_DEVICE_FAIL = 0x02
READ_ADDRESS_FAULT = 0x03

READ_VOLTAGE1 = 0x05 # range -2500 ~ +2500 V
READ_CURRENT1 = 0x06  # range 0 ~ 10000 mA
READ_VOLTAGE2 = 0x07 # range -2500 ~ +2500 V
READ_CURRENT2 = 0x08 # range 0 ~ 10000 mA

READ_VBIA = 0x09 # range 0 ~ 1250
READ_VCS = 0x0A # range 0.000 ~ 3.300 V

READ_ICS = 0x0B # range 0.000 ~ 3.300 V
READ_ADDRESS_CP = 0x0C # range 0.000 ~ 3.300 V
READ_ADDRESS_DECHUCK_START = 0x0D # range 0.000 ~ 3.300 V
READ_ADDRESS_DECHUCK_END = 0x0E # range 0.000 ~ 3.300 V

# WRITE 후 READ DATA
READ_WRITE_VOLTAGE1 = 0x10 # range -2500 ~ +2500 V
READ_WRITE_CURRENT1 = 0x11  # range 0 ~ 10000 mA
READ_WRITE_VOLTAGE2 = 0x12 # range -2500 ~ +2500 V
READ_WRITE_CURRENT2 = 0x13 # range 0 ~ 10000 mA

READ_WRITE_LEAK_FAULT_LEVEL = 0x14 
READ_WRITE_ROMIN = 0x15 
READ_WRITE_UPTIME = 0x16 
READ_WRITE_DOWNTIME = 0x17 

# READ_WRITE = 0x18 


# WRITE_RUN_ADDRESS = 0x00
RUN_VALUE = 2
STOP_VALUE = 0

WRITE_BIT_POWER_ON = 0x00
WRITE_BIT_FAULT_CLEAR = 0x02

WRITE_VOLTAGE1 = 0x01 # range -2500 ~ +2500 V
WRITE_CURRENT1 = 0x02  # range 0 ~ 10000 mA

WRITE_VOLTAGE2 = 0x03 # range -2500 ~ +2500 V
WRITE_CURRENT2 = 0x04 # range 0 ~ 10000 mA

WRITE_LEAK_LEVEL = 0x05 
WRITE_RO_MIN_FAULT = 0x06 
WRITE_RAMP_UP_TIME = 0x07 
WRITE_RAMP_DOWN_TIME = 0x08 
WRITE_TOGGLE_MODE = 0x09 
WRITE_TOGGLE_COUNT = 0x0A 

WRITE_SET_SLOPE = 0x0B 
WRITE_SET_COEFF = 0x0C 
WRITE_ONOFF_SELECT = 0x0E 
WRITE_LOCAL_ADDRESS = 0x0F 
WRITE_ARC_DELAY = 0x10 
WRITE_ARC_RATE = 0x11 
WRITE_TOGGLE = 0x12 
WRITE_ARC_CONTROL = 0x13 
WRITE_OCP_CONTROL = 0x14 
WRITE_TARGET_CAP = 0x15 
WRITE_CAP_DEVIATION = 0x16 

## 2023. 6. 20
WRITE_NDP_ONOFF = 0x17
WRITE_PDP_ONOFF = 0x18


## 2023. 6. 22
READ_WRITE_VOLTAGE1_1U = 0x08

WRITE_VOLTAGE1_1U = 0x01 # range -2500 ~ +2500 V
WRITE_CURRENT1_1U = 0x02  # range 0 ~ 10000 mA

WRITE_LEAK_LEVEL_1U = 0x03 
WRITE_RO_MIN_FAULT_1U = 0x04 
WRITE_RAMP_UP_TIME_1U = 0x05 
WRITE_RAMP_DOWN_TIME_1U = 0x06 
WRITE_TOGGLE_MODE_1U = 0x07 
WRITE_TOGGLE_COUNT_1U = 0x08 

WRITE_SET_SLOPE_1U = 0x09 
WRITE_SET_COEFF_1U = 0x0a 
WRITE_ONOFF_SELECT_1U = 0x0c 
WRITE_LOCAL_ADDRESS_1U = 0x0d 
WRITE_ARC_DELAY_1U = 0x0e 
WRITE_ARC_RATE_1U = 0x0f 
WRITE_TOGGLE_1U = 0x10 
WRITE_ARC_CONTROL_1U = 0x11 
WRITE_OCP_CONTROL_1U = 0x12 

UNIT = 0x1
END_VALUE = 0x00ed

FORWARD = 0 
REVERSE = 1 

TOGGLE_ON = 1
TOGGLE_OFF = 0

PLUS_VOL = 4
MINUS_VOL = -4

# from pymodbus.constants import Defaults
# Defaults.RetryOnEmpty = True
# Defaults.Timeout = 5
# Defaults.Retries = 3

# from pyModbusTCP.client import ModbusClient
# client = ModbusClient(host="192.168.10.10", port=5000, unit_id=1, auto_open=True)
# regs = client.read_input_registers(5, 10)

def connect_rtu(port, ptype='rtu', speed=38400, bytesize=8, parity='N', stopbits=1):
    from pymodbus.client.sync import ModbusSerialClient as ModbusClient
    client = ModbusClient(method=ptype, port=port, timeout=1,
                      baudrate=speed, bytesize=bytesize, parity=parity, stopbits=stopbits)
    client.connect()

    if client:
        print('*'*50)
        print("******************* RTU DUN SUCCESS *********************", client)
        print('*'*50)

    return client

def connect_tcp(ipaddress='192.168.10.100', ipport=5000):
    
    conf.SIGNED_VALUES = True
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect((ipaddress, ipport))

    if client:
        print('#'*50)
        print(f"******************* TCP {client} SUCCESS *********************")
        print('#'*50)

    return client

def make_qrport(port):
    client = serial.Serial(port)
    return client

# Device Main Data 읽어오기 
def read_data(client, ptype):
    data = {}
    if ptype == 'RTU':
        try:
            # logging.info(f"read_data :: {client} :: {ptype}")
            response = client.read_input_registers(READ_VOLTAGE1, 10, unit=UNIT)
            if response.function_code >= 0x80:
                print("Error Happened : ", )
            result = response.registers
        except Exception as e:
            result = []
            logging.info(f"read_data ERROR :: {client} :: {ptype} :: {READ_VOLTAGE1} :: {e}")
    else:
        try:
            message = tcp.read_input_registers(slave_id=1, 
                        starting_address=READ_VOLTAGE1, quantity=10)
            result = tcp.send_message(message, client)
        except Exception as e:
            result = []
            logging.info(f"read_data ERROR :: {client} :: {ptype} :: {READ_VOLTAGE1} :: {e}")
    return result

# Device Main Data 읽어오기 
def read_one_data(client, address, ptype):
    data = {}
    if ptype == 'RTU':
        try:
            response = client.read_input_registers(address, 1, unit=UNIT)
            if response.function_code >= 0x80:
                print("Error Happened : ", )
            result = response.registers
        except Exception as e:
            result = []
            print("** RTU :: Error read_input_registers : ", e)
    else:
        try:
            message = tcp.read_input_registers(slave_id=1, 
                        starting_address=READ_VOLTAGE1, quantity=10)
            result = tcp.send_message(message, client)
        except Exception as e:
            result = []
            print("## TCP :: Error read_input_registers : ", e)
    return result

def read_data_vol(client, address, ptype):
    data = {}
    if ptype == 'RTU':
        try:
            response = client.read_input_registers(address, 10, unit=UNIT)
            if response.function_code >= 0x80:
                print("Error Happened : ", )
            result = response.registers

        except Exception as e:
            result = []
            print("** RTU :: Error read_input_registers : ", e)
    else:
        try:
            message = tcp.read_input_registers(slave_id=1, 
                        starting_address=READ_VOLTAGE1, quantity=1)
            result = tcp.send_message(message, client)
            
        except Exception as e:
            result = []
            print("## TCP :: Error read_input_registers : ", e)

    return result

def read_data_virtual(client, alist):
    return alist

# Device Main Data 읽어오기 
def read_data_1u(client, ptype):
    data = {}
    if ptype == 'RTU':
        try:
            response = client.read_input_registers(READ_VOLTAGE1, 3, unit=UNIT)
            if response.function_code >= 0x80:
                print("Error Happened : ", )
            result = response.registers
        except Exception as e:
            result = []
            print("** RTU :: Error read_input_registers : ", e)
    else:
        try:
            message = tcp.read_input_registers(slave_id=1, 
                        starting_address=READ_VOLTAGE1, quantity=3)
            result = tcp.send_message(message, client)
        except Exception as e:
            result = []
            print("## TCP :: Error read_input_registers : ", e)
    return result

def read_fault(client, ptype):
    if ptype == 'RTU':
        try:
            response = client.read_input_registers(READ_ADDRESS_FAULT, 1, unit=UNIT)
            if response.function_code >= 0x80:
                print("Error Happened : ", )
            result = response.registers
            return result[0]
        except Exception as e:
            print("read_fault ", e)
            result = 0
    else:
        try:
            message = tcp.read_input_registers(slave_id=1, 
                        starting_address=READ_ADDRESS_FAULT, quantity=1)
            result = tcp.send_message(message, client)[0]
        except Exception as e:
            print("read_fault ", e)
            result = 0
    return result

def read_setting_value(client, ptype):
    data = {}

    if ptype == 'RTU':
        try:
            response = client.read_input_registers(READ_WRITE_VOLTAGE1, 22, unit=UNIT)
            if response.function_code >= 0x80:
                print("Error Happened : ", )
                return data

            result = response.registers
            voltage1 = c_int16(result[0]).value
            voltage2 = c_int16(result[2]).value

            data['voltage1'] = voltage1
            data['current1'] = result[1] / 1000
            data['voltage2'] = voltage2
            data['current2'] = result[3] / 1000
            data['leak_fault_level'] = result[4] / 1000
            data['ro_min_fault'] = result[5] / 1000
            data['up_time'] = result[6] / 10
            data['down_time'] = result[7] / 10
            data['rd_mode'] = result[8]
            data['toggle_count'] = result[9]
            data['slope'] = result[10] / 10
            data['coeff'] = result[11] / 10
            data['rd_select'] = result[13]
            data['local_address'] = result[14]
            data['arc_delay'] = result[15] / 100
            data['arc_rate'] = result[16]
            data['rd_toggle'] = result[17]
            data['rd_arc'] = result[18]
            data['rd_ocp'] = result[19]
            data['target_cap'] = result[20]
            data['cap_deviation'] = result[21]

        except Exception as e:
            response = []
            logging.info(f"read_setting_value ERROR :: {client} :: {ptype} :: {READ_WRITE_VOLTAGE1} :: {e}")
    else:
        message = tcp.read_input_registers(slave_id=1, 
                    starting_address=READ_WRITE_VOLTAGE1, quantity=22)
        result = tcp.send_message(message, client)

        data['voltage1'] = result[0]
        data['current1'] = result[1] / 1000
        data['voltage2'] = result[2]
        data['current2'] = result[3] / 1000
        data['leak_fault_level'] = result[4] / 1000
        data['ro_min_fault'] = result[5] / 1000
        data['up_time'] = result[6] / 10
        data['down_time'] = result[7] / 10
        data['rd_mode'] = result[8]
        data['toggle_count'] = result[9]
        data['slope'] = result[10] / 10
        data['coeff'] = result[11] / 10
        data['rd_select'] = result[13]
        data['local_address'] = result[14]
        data['arc_delay'] = result[15] / 100
        data['arc_rate'] = result[16]
        data['rd_toggle'] = result[17]
        data['rd_arc'] = result[18]
        data['rd_ocp'] = result[19]
        data['target_cap'] = result[20]
        data['cap_deviation'] = result[21]

    logging.info(f"read_setting_value :: {client} :: {ptype} :: {READ_WRITE_VOLTAGE1} :: {data}")
    return data

def read_setting_value_1u(client, ptype):
    data = {}

    try:
        response = client.read_input_registers(READ_WRITE_VOLTAGE1_1U, 18, unit=UNIT)
        if response.function_code >= 0x80:
            print("Error Happened : ", )
            return data

        result = response.registers
        voltage1 = c_int16(result[0]).value
        data['voltage1'] = voltage1
        data['current1'] = result[1] / 1000

        data['leak_fault_level'] = result[2] / 1000
        data['ro_min_fault'] = result[3] / 1000
        data['up_time'] = result[4] / 10
        data['down_time'] = result[5] / 10
        
        data['rd_mode'] = result[6]
        data['toggle_count'] = result[7]
        data['slope'] = result[8] / 10
        data['coeff'] = result[9] / 10
        data['rd_select'] = result[11]

        data['baud_rate'] = result[10]
        data['local_address'] = result[12]

        data['arc_delay'] = result[13] / 100
        data['arc_rate'] = result[14]
        data['rd_toggle'] = result[15]
        data['rd_arc'] = result[16]
        data['rd_ocp'] = result[17]

    except Exception as e:
        response = []
        logging.info(f"read_setting_value ERROR :: {client} :: {ptype} :: {READ_WRITE_VOLTAGE1} :: {e}")

    logging.info(f"read_setting_value :: {client} :: {ptype} :: {READ_WRITE_VOLTAGE1_1U} :: {data}")
    return data

# Write Register
def write_registers(client, address, val, ptype):
    if ptype == 'RTU':
        try:
            value = c_uint16(val).value
            result = client.write_registers(address, value, unit=UNIT)
            logging.info(f"write_registers(address, value, unit=UNIT) : {client} :: {address} :: {val}")
        except Exception as e:
            result = []
    else:
        message = tcp.write_single_register(slave_id=1, 
                    address=address, value=val)
        result = tcp.send_message(message, client)

    # return result
    # return 10

# Read FeedBack Data
def read_feedback(client, address):
    try:
        response = client.read_input_registers(address, 1, unit=UNIT)
        if result.function_code >= 0x80:
            print("Error Happened : ", )
        result = response.registers
    except Exception as e:
        result = []
        print("Error read_input_registers : ", e)
    return result

