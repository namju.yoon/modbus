import sys  # We need sys so that we can pass argv to QApplication
import os
import time
import logging
import pandas as pd
import pyqtgraph as pyGraph
import pyqtgraph.exporters
from ctypes import c_int16, c_uint16

from PyQt5.QtWidgets import QDialog, QHBoxLayout, QDialogButtonBox, QDialogButtonBox, QFileDialog
from PyQt5.QtWidgets import QGroupBox, QSpinBox, QWidget, QGridLayout, QPushButton, QLabel, QRadioButton
from PyQt5.QtWidgets import QDialogButtonBox, QApplication, QVBoxLayout, QDesktopWidget, QCheckBox, QLCDNumber
from PyQt5.QtCore import Qt
from PyQt5 import QtCore

from . import esc_serial as device

ON = True 
OFF = False

UP = True 
DOWN = False

class MySpinEditOLD:
    def __init__(self, title, val=0, vmin=1, vmax=300, vstep=1, *args, **kwargs):
        self.red = 'margin: -1px; padding: 0px; font-size: 14pt; color: red;background-color: white'
        self.blue = 'margin: -1px; padding: 0px; font-size: 14pt; color: blue;background-color: white'

        self.title = title 
        self.vmin = vmin
        self.vmax = vmax
        
        self.edit = QSpinBox()
        self.edit.setFixedHeight(15)
        self.edit.setMaximumHeight(15)
        self.edit.setStyleSheet(self.blue)
        self.edit.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self.edit.setRange(vmin, vmax)
        self.edit.setSingleStep(vstep)
        self.edit.setValue(val)

    def display(self):
        grp_edit = QGroupBox()
        layout_edit = QHBoxLayout()
        grp_edit.setLayout(layout_edit)

        edit_label = QLabel(self.title)
        vrange = f"{self.vmin} ~ {self.vmax}"
        edit_range = QLabel(vrange)
        layout_edit.addWidget(edit_label)
        layout_edit.addWidget(self.edit)
        layout_edit.addWidget(edit_range)

        return grp_edit

    def value(self):
        return self.edit.value()

    def warning(self):
        self.edit.setStyleSheet(self.red)

    def normal(self):
        self.edit.setStyleSheet(self.blue)


class InspParamOLD(QWidget):
    def __init__(self):
        super(InspParam, self).__init__()
        self.edit_stepvol = MySpinEdit("STEP VOLTAGE(V)", 100, -5000, 5000, 100)
        self.edit_maxvol = MySpinEdit("MAX VOLTAGE(V)", 2500, -5000, 5000, 100)
        self.edit_stopvol = MySpinEdit("STOP VOLTAGE(V)", 0, -5000, 5000, 100)
        self.edit_duty = MySpinEdit("DUTY TIME(초,sec)", 3, 1, 60, 1)
        self.edit_offtime = MySpinEdit("OFF TIME(초,sec)", 3, 1, 60, 1)

    def display(self):
        grp_params = QGroupBox("LEGEND")
        layout_params = QHBoxLayout()
        grp_params.setLayout(layout_params)

        ## STEP VOLTAGE
        grp_stepvol = self.edit_stepvol.display()
        layout_params.addWidget(grp_stepvol)

        ## MAX VOLTAGE
        grp_maxvol = self.edit_maxvol.display()
        layout_params.addWidget(grp_maxvol)

        ## STOP VOLTAGE
        grp_stopvol = self.edit_stopvol.display()
        layout_params.addWidget(grp_stopvol)

        ## DUTY TIME
        grp_duty = self.edit_duty.display()
        layout_params.addWidget(grp_duty)

        ## OFF TIME
        grp_offtime = self.edit_offtime.display()
        layout_params.addWidget(grp_offtime)

        return grp_params

    def get_stepvol(self):
        return self.edit_stepvol.value()

    def get_maxvol(self):
        return self.edit_maxvol.value()

    def get_stopvol(self):
        return self.edit_stopvol.value()

    def get_duty(self):
        return self.edit_duty.value()

    def get_offtime(self):
        return self.edit_offtime.value()

    def warning(self):
        self.edit_stepvol.warning()
        self.edit_maxvol.warning()

    def normal(self):
        self.edit_stepvol.normal()
        self.edit_maxvol.normal()

        
# 초기 버전
class InspectWindowOLD(QDialog):
    def __init__(self, Dialog, client, channel):
        super().__init__()
        self.title = '출하 검사 Window'
        self.client = client
        self.channel = channel
        self.params = InspParamOLD()
        self.ptype = 'RTU'

        self.pen_vol1 = pyGraph.mkPen(width=1, color=(255, 0, 0))
        self.pen_vol2 = pyGraph.mkPen(width=1, color=(88, 4, 124))
        self.pen_vol3 = pyGraph.mkPen(width=1, color=(0, 0, 255))
        self.pen_vol4 = pyGraph.mkPen(width=1, color=(204, 0, 153))

        self.edit_stepvol = 0
        self.edit_maxvol = 0
        self.edit_duty = 0
        self.edit_offtime = 0

        self.dutytimer = None
        self.dtimer = None
        self.status = OFF

        self.DATA_PERIOD = 500
        self.DUTY_PERIOD = 3000
        self.OFFTIME_PERIOD = 3000

        self.cur_vol = 0
        self.countN = 0
        self.indexs = []
        self.indexs = []
        self.indexs3 = []
        self.indexs4 = []
        self.vol_ch1s = []
        self.vol_ch2s = []
        self.vol3s = []
        self.vol4s = []

        self.direction = UP
        self.start_dir = None

        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width(), screensize.height()) 

        main_layer = QGridLayout()
        self.setWindowTitle(self.title)
        self.setLayout(main_layer)

        ## Param Layer 
        group_param = self.params.display()
        main_layer.addWidget(group_param, 0, 0, 1, 2)

        ## START Buttong 
        grp_button = QGroupBox("")
        layer_button = QHBoxLayout()
        grp_button.setLayout(layer_button)

        self.btn_start = QPushButton("검사 시작")
        self.btn_start.clicked.connect(self.step1_func)
        self.btn_save = QPushButton("데이터/이미지 저장")
        self.btn_save.clicked.connect(self.save1_func)
        self.btn_stop = QPushButton("STOP")
        self.btn_stop.clicked.connect(self.stop_func)

        layer_button.addWidget(self.btn_start)
        layer_button.addWidget(self.btn_save)
        layer_button.addWidget(self.btn_stop)

        main_layer.addWidget(grp_button, 1, 0, 1, 2)

        ## Graph 1
        grp_step1 = QGroupBox("STEP 1")
        layer_step1 = QVBoxLayout()
        grp_step1.setLayout(layer_step1)
        
        self.gpwd1 = pyGraph.PlotWidget()
        self.gpwd1.setBackground('w')
        self.gpwd1.setTitle("VOL 1", color=(255, 0, 0), size="20pt")
        self.gpwd1.plot([0], [0], pen=self.pen_vol1)
        layer_step1.addWidget(self.gpwd1)

        if self.channel:
            self.gpwd2 = pyGraph.PlotWidget()
            self.gpwd2.setBackground('w')
            self.gpwd2.setTitle("VOL 2", color=(88, 4, 124), size="20pt")
            self.gpwd2.plot([0], [0], pen=self.pen_vol2)
            layer_step1.addWidget(self.gpwd2)

        main_layer.addWidget(grp_step1, 2, 0)

        ## Graph 2
        grp_step2 = QGroupBox("STEP 2")
        layer_step2 = QVBoxLayout()
        grp_step2.setLayout(layer_step2)
        self.btn_start2 = QPushButton("검사 시작")
        self.btn_start2.clicked.connect(self.onoff_step2_func)
        self.btn_save2 = QPushButton("이미지 저장")
        self.btn_save2.clicked.connect(self.save2_func)

        self.gpwd3 = pyGraph.PlotWidget()
        self.gpwd3.setBackground('w')
        self.gpwd3.setTitle("VOL 1", color=(0, 0, 255), size="20pt")
        self.gpwd3.plot([0], [0], pen=self.pen_vol3)
        layer_step2.addWidget(self.gpwd3)

        if self.channel:
            self.gpwd4 = pyGraph.PlotWidget()
            self.gpwd4.setBackground('w')
            self.gpwd4.setTitle("VOL 2", color=(204, 0, 153), size="20pt")
            self.gpwd4.plot([0], [0], pen=self.pen_vol4)
            layer_step2.addWidget(self.gpwd4)

        main_layer.addWidget(grp_step2, 2, 1)

        ## Button
        self.buttonBox = QDialogButtonBox()
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        main_layer.addWidget(self.buttonBox, 3, 0, 1, 2)

        self.buttonBox.accepted.connect(self.on_accepted)
        self.buttonBox.rejected.connect(self.reject)

    def on_accepted(self):
        self.accept()

    def step1_func(self):
        self.edit_stepvol = self.params.get_stepvol()
        self.edit_maxvol = self.params.get_maxvol()
        self.edit_stopvol = self.params.get_stopvol()
        self.edit_duty = self.params.get_duty()
        self.edit_offtime = self.params.get_offtime()

        self.cur_vol = 10
        self.rev_vol = -self.cur_vol
        self.countN = 0
        self.indexs = []
        self.vol_ch1s = []
        self.toggle = True

        if self.channel:
            device.write_registers(self.client, device.WRITE_TOGGLE, device.FORWARD, self.ptype)
        else:
            device.write_registers(self.client, device.WRITE_TOGGLE_1U, device.FORWARD, self.ptype)
        time.sleep(2)
        
        if self.edit_stepvol > 0 and self.edit_maxvol > 0:
            self.start_dir = UP
            self.direction = UP
            self.params.normal()

            if self.channel:
                value = -10
                device.write_registers(self.client, device.WRITE_VOLTAGE2, value, self.ptype)

            value = self.cur_vol
            device.write_registers(self.client, device.WRITE_VOLTAGE1, value, self.ptype)

        elif self.edit_stepvol < 0 and self.edit_maxvol < 0:
            self.start_dir = DOWN
            self.direction = DOWN
            self.params.normal()

            if self.channel:
                value = 10
                device.write_registers(self.client, device.WRITE_VOLTAGE2, value, self.ptype)

            value = self.rev_vol
            device.write_registers(self.client, device.WRITE_VOLTAGE1, value, self.ptype)

        else:
            logging.info("self.params.warning() !!")
            self.params.warning()
            return

        self.gpwd1.clear()
        if self.channel:
            self.gpwd2.clear()

        ## DUTY_PERIOD
        self.DUTY_PERIOD = self.edit_duty * 1000
        # setting timer
        
        ## RUN 
        device.write_registers(self.client, 
                device.WRITE_BIT_POWER_ON, device.RUN_VALUE, self.ptype)
        logging.info("DEVICE START !!! \n\n")

        self.dtimer = QtCore.QTimer()
        self.dtimer.setInterval(self.DATA_PERIOD)
        self.dtimer.timeout.connect(self.get_step1_vol1)
        self.dtimer.start()

        self.dutytimer = QtCore.QTimer()
        self.dutytimer.setInterval(self.DUTY_PERIOD)
        self.dutytimer.timeout.connect(self.change_step1_vol1)
        self.dutytimer.start()

    def get_step1_vol1(self):
        result = device.read_data_vol(self.client, device.READ_VOLTAGE1, self.ptype)
        if result:
            self.countN = self.countN  + 1

            # 데이터 값을 Array 에 저장하기 
            self.indexs.append(self.countN)
            voltage1 = c_int16(result[0]).value
            self.vol_ch1s.append(voltage1)

            if self.countN % 10 == 3:
                # logging.info(f"STEP 1 :: VOL 1 :: {result} :: {self.indexs[-30:]}")
                # logging.info(f"STEP 1 :: VOL 1 :: {self.vol_ch1s[-30:]}\n\n")
                self.draw_graph1()

    def change_step1_vol1(self):
        ## MAX
        # UP 도달 
        if self.start_dir == UP and self.direction == UP and self.cur_vol > self.edit_maxvol:
            self.direction = DOWN
            value = self.edit_maxvol
            self.cur_vol = self.edit_maxvol
            device.write_registers(self.client, device.WRITE_VOLTAGE1, value, self.ptype)

        # DOWN 도달 
        elif self.start_dir == DOWN and self.direction == DOWN and self.cur_vol < self.edit_maxvol:
            self.direction = UP
            value = self.edit_maxvol
            self.cur_vol = self.edit_maxvol
            device.write_registers(self.client, device.WRITE_VOLTAGE1, value, self.ptype)

        ## PEAK 진행 
        ## START UP and UP
        elif self.start_dir == UP and self.direction == UP and self.cur_vol <= self.edit_maxvol:
            value = self.cur_vol
            self.cur_vol += self.edit_stepvol
            device.write_registers(self.client, device.WRITE_VOLTAGE1, value, self.ptype)

        ## START DOWN and UP
        elif self.start_dir == DOWN and self.direction == DOWN and self.cur_vol >= self.edit_maxvol:
            value = self.cur_vol
            self.cur_vol += self.edit_stepvol
            device.write_registers(self.client, device.WRITE_VOLTAGE1, value, self.ptype)

        ## STOP 방향 진행
        ## DOWN
        elif self.start_dir == UP and self.direction == DOWN and self.cur_vol >= self.edit_stopvol:
            value = self.cur_vol
            self.cur_vol -= self.edit_stepvol
            if self.toggle and value <= 0:
                self.toggle = False
                if self.channel:
                    device.write_registers(self.client, device.WRITE_TOGGLE, device.REVERSE, self.ptype)
                else:
                    device.write_registers(self.client, device.WRITE_TOGGLE_1U, device.REVERSE, self.ptype)
                time.sleep(3)
            device.write_registers(self.client, device.WRITE_VOLTAGE1, value, self.ptype)

        ## UP
        elif self.start_dir == DOWN and self.direction == UP and self.cur_vol <= self.edit_stopvol:
            value = self.cur_vol
            self.cur_vol -= self.edit_stepvol
            if self.toggle and value > 0:
                self.toggle = False
                if self.channel:
                    device.write_registers(self.client, device.WRITE_TOGGLE, device.REVERSE, self.ptype)
                else:
                    device.write_registers(self.client, device.WRITE_TOGGLE_1U, device.REVERSE, self.ptype)
                time.sleep(3)
            device.write_registers(self.client, device.WRITE_VOLTAGE1, value, self.ptype)


        ## END 
        elif (self.start_dir == UP and self.direction == DOWN and self.cur_vol < self.edit_stopvol) or \
            (self.start_dir == DOWN and self.direction == UP and self.cur_vol > self.edit_stopvol):
            logging.info(f"STEP 1 :: VOL 1 :: End :: {self.cur_vol} :: {self.edit_stopvol} \n\n")
            self.dutytimer.stop()
            self.dutytimer = None 

            self.dtimer.stop()
            self.dtimer = None 

            self.draw_graph1()

            ## STEP 1 :: VOL 2
            self.cur_vol = 10
            self.countN = 0
            self.indexs = []
            self.vol_ch2s = []
            self.toggle = True
            
            device.write_registers(self.client, 
                    device.WRITE_BIT_POWER_ON, device.STOP_VALUE, self.ptype)
            time.sleep(1)

            if self.channel:
                device.write_registers(self.client, device.WRITE_TOGGLE, device.FORWARD, self.ptype)
            else:
                device.write_registers(self.client, device.WRITE_TOGGLE_1U, device.FORWARD, self.ptype)

            if self.channel:
                ## 0 초기화 
                if self.start_dir == UP:
                    self.direction = UP
                    value = -10
                    device.write_registers(self.client, device.WRITE_VOLTAGE1, value, self.ptype)
                    value = 10
                    device.write_registers(self.client, device.WRITE_VOLTAGE2, value, self.ptype)
                else:
                    self.direction = DOWN
                    value = 10
                    device.write_registers(self.client, device.WRITE_VOLTAGE1, value, self.ptype)
                    value = -10
                    device.write_registers(self.client, device.WRITE_VOLTAGE2, value, self.ptype)

                time.sleep(3)

                ## RUN 
                device.write_registers(self.client, 
                        device.WRITE_BIT_POWER_ON, device.RUN_VALUE, self.ptype)
                logging.info("DEVICE START !!! \n\n")

                self.dtimer = QtCore.QTimer()
                self.dtimer.setInterval(self.DATA_PERIOD)
                self.dtimer.timeout.connect(self.get_step1_vol2)
                self.dtimer.start()

                self.dutytimer = QtCore.QTimer()
                self.dutytimer.setInterval(self.DUTY_PERIOD)
                self.dutytimer.timeout.connect(self.change_step1_vol2)
                self.dutytimer.start()
            
            else:
                self.onoff_step2_func()

        else:
            print("change_step1_vol1 :: ", self.direction, self.cur_vol)

    def get_step1_vol2(self):
        result = device.read_data_vol(self.client, device.READ_VOLTAGE2, self.ptype)
        if result:
            self.countN = self.countN  + 1

            # 데이터 값을 Array 에 저장하기 
            self.indexs.append(self.countN)
            voltage2 = c_int16(result[0]).value
            self.vol_ch2s.append(voltage2)

            if self.countN % 10 == 3:
                # logging.info(f"STEP 1 :: VOL 2 :: {result} :: {self.indexs[-30:]}")
                # logging.info(f"STEP 1 :: VOL 2 :: {self.vol_ch2s[-30:]}\n\n")
                self.draw_graph2()

    def change_step1_vol2(self):
        ## MAX
        # UP 도달 
        if self.start_dir == UP and self.direction == UP and self.cur_vol > self.edit_maxvol:
            self.direction = DOWN
            value = self.edit_maxvol
            self.cur_vol = self.edit_maxvol
            device.write_registers(self.client, device.WRITE_VOLTAGE2, value, self.ptype)

        # DOWN 도달 
        elif self.start_dir == DOWN and self.direction == DOWN and self.cur_vol < self.edit_maxvol:
            self.direction = UP
            value = self.edit_maxvol
            self.cur_vol = self.edit_maxvol
            device.write_registers(self.client, device.WRITE_VOLTAGE2, value, self.ptype)

        ## PEAK 진행 
        ## START UP and UP
        elif self.start_dir == UP and self.direction == UP and self.cur_vol <= self.edit_maxvol:
            value = self.cur_vol
            self.cur_vol += self.edit_stepvol
            device.write_registers(self.client, device.WRITE_VOLTAGE2, value, self.ptype)

        ## START DOWN and UP
        elif self.start_dir == DOWN and self.direction == DOWN and self.cur_vol >= self.edit_maxvol:
            value = self.cur_vol
            self.cur_vol += self.edit_stepvol
            device.write_registers(self.client, device.WRITE_VOLTAGE2, value, self.ptype)

        ## STOP 진행
        ## DOWN
        elif self.start_dir == UP and self.direction == DOWN and self.cur_vol >= self.edit_stopvol:
            value = self.cur_vol
            self.cur_vol -= self.edit_stepvol
            if self.toggle and value <= 0:
                self.toggle = False
                if self.channel:
                    device.write_registers(self.client, device.WRITE_TOGGLE, device.REVERSE, self.ptype)
                else:
                    device.write_registers(self.client, device.WRITE_TOGGLE_1U, device.REVERSE, self.ptype)
                time.sleep(3)
            device.write_registers(self.client, device.WRITE_VOLTAGE2, value, self.ptype)

        ## UP
        elif self.start_dir == DOWN and self.direction == UP and self.cur_vol <= self.edit_stopvol:
            value = self.cur_vol
            self.cur_vol -= self.edit_stepvol
            if self.toggle and value > 0:
                self.toggle = False
                if self.channel:
                    device.write_registers(self.client, device.WRITE_TOGGLE, device.REVERSE, self.ptype)
                else:
                    device.write_registers(self.client, device.WRITE_TOGGLE_1U, device.REVERSE, self.ptype)
                time.sleep(3)
            device.write_registers(self.client, device.WRITE_VOLTAGE2, value, self.ptype)

        ## END 
        elif (self.start_dir == UP and self.direction == DOWN and self.cur_vol < self.edit_stopvol) or \
            (self.start_dir == DOWN and self.direction == UP and self.cur_vol > self.edit_stopvol):
            logging.info(f"STEP 1 :: VOL 2 :: End :: {self.cur_vol} :: {self.edit_stopvol} \n\n")

            self.dutytimer.stop()
            self.dutytimer = None 

            self.dtimer.stop()
            self.dtimer = None 

            self.draw_graph2()
            value = 10
            ## STOP 
            device.write_registers(self.client, 
                device.WRITE_BIT_POWER_ON, device.STOP_VALUE, self.ptype)

            logging.info("Time Delay !!! \n\n")
            time.sleep(3)

            self.onoff_step2_func()
            
        else:
            print("change_step1_vol1 :: ", self.direction, self.cur_vol)

    def onoff_step2_func(self):
        self.countN = 0
        self.indexs3 = []
        self.vol3s = []
        self.toggle = True
        self.base = 0
        
        self.gpwd3.clear()
        if self.channel:
            self.gpwd4.clear()

        ## DUTY_PERIOD
        self.DUTY_PERIOD = self.edit_duty * 1000
        self.OFFTIME_PERIOD = self.edit_offtime * 1000

        if self.channel:
            device.write_registers(self.client, device.WRITE_TOGGLE, device.FORWARD, self.ptype)
        else:
            device.write_registers(self.client, device.WRITE_TOGGLE_1U, device.FORWARD, self.ptype)
        time.sleep(2)

        if self.edit_stepvol > 0 and self.edit_maxvol > 0:
            self.start_dir = UP
            self.direction = UP
            self.cur_vol = 10

            if self.channel:
                value = -10
                device.write_registers(self.client, device.WRITE_VOLTAGE2, value, self.ptype)

            value = self.cur_vol
            device.write_registers(self.client, device.WRITE_VOLTAGE1, value, self.ptype)

        elif self.edit_stepvol < 0 and self.edit_maxvol < 0:
            self.start_dir = DOWN
            self.direction = DOWN
            self.cur_vol = -10

            if self.channel:
                value = 10
                device.write_registers(self.client, device.WRITE_VOLTAGE2, value, self.ptype)

            value = self.rev_vol
            device.write_registers(self.client, device.WRITE_VOLTAGE1, value, self.ptype)

        time.sleep(3)
        
        ## RUN 
        device.write_registers(self.client, 
                device.WRITE_BIT_POWER_ON, device.RUN_VALUE, self.ptype)
        logging.info("DEVICE START !!! \n\n")

        self.dtimer = QtCore.QTimer()
        self.dtimer.timeout.connect(self.get_onoff_step2_vol1)
        self.dtimer.start(self.DATA_PERIOD)

        self.status = OFF
        self.dutytimer = QtCore.QTimer()
        self.dutytimer.timeout.connect(self.change_onoff_voltage1)
        self.dutytimer.start(3000)

    def change_onoff_voltage1(self):
        self.dutytimer.stop()
        self.dutytimer = None

        ## END 
        # UP
        if (self.start_dir == UP and self.direction == DOWN and self.cur_vol < self.edit_stopvol) or \
           (self.start_dir == DOWN and self.direction == UP and self.cur_vol > self.edit_stopvol) :
            logging.info(f"STEP 2 :: VOL 1 :: End :: {self.start_dir} :: {self.direction} :: {self.cur_vol} :: {self.edit_stopvol} \n\n")
            self.dtimer.stop()
            self.dtimer = None 

            self.draw_graph3()

            ## STOP
            device.write_registers(self.client, 
                    device.WRITE_BIT_POWER_ON, device.STOP_VALUE, self.ptype)
            logging.info("Time Delay !!! \n\n")
            time.sleep(3)

            if self.channel:

                ## STEP 1 :: VOL 2
                self.countN = 0
                self.indexs4 = []
                self.vol4s = []
                self.status = ON
                self.toggle = True

                # Toggle FORWARD
                if self.channel:
                    device.write_registers(self.client, device.WRITE_TOGGLE, device.FORWARD, self.ptype)
                else:
                    device.write_registers(self.client, device.WRITE_TOGGLE_1U, device.FORWARD, self.ptype)
                time.sleep(2)

                if self.channel:
                    ## 0 초기화 
                    if self.start_dir == UP:
                        self.cur_vol = 10
                        self.direction = UP
                        value = -10
                        device.write_registers(self.client, device.WRITE_VOLTAGE1, value, self.ptype)
                        value = 10
                        device.write_registers(self.client, device.WRITE_VOLTAGE2, value, self.ptype)
                    else:
                        self.direction = DOWN
                        self.cur_vol = -10
                        value = 10
                        device.write_registers(self.client, device.WRITE_VOLTAGE1, value, self.ptype)
                        value = -10
                        device.write_registers(self.client, device.WRITE_VOLTAGE2, value, self.ptype)

                time.sleep(3)

                ## RUN 
                device.write_registers(self.client, 
                        device.WRITE_BIT_POWER_ON, device.RUN_VALUE, self.ptype)
                logging.info("DEVICE START !!! \n\n")

                self.dtimer = QtCore.QTimer()
                self.dtimer.timeout.connect(self.get_onoff_step2_vol2)
                self.dtimer.start(self.DATA_PERIOD)

                self.dutytimer = QtCore.QTimer()
                self.dutytimer.timeout.connect(self.change_onoff_voltage2)
                self.dutytimer.start(self.DUTY_PERIOD)

        elif self.start_dir == UP and self.direction == UP and self.cur_vol > self.edit_maxvol:
            self.direction = DOWN
            self.dutytimer = QtCore.QTimer()
            self.dutytimer.timeout.connect(self.change_onoff_voltage1)
            self.dutytimer.start(self.DUTY_PERIOD)

        elif self.start_dir == DOWN and self.direction == DOWN and self.cur_vol < self.edit_maxvol:
            self.direction = UP
            self.dutytimer = QtCore.QTimer()
            self.dutytimer.timeout.connect(self.change_onoff_voltage1)
            self.dutytimer.start(self.DUTY_PERIOD)

        else:
            self.dutytimer = QtCore.QTimer()
            if self.status == OFF:
                self.status = ON

                if self.start_dir == UP and self.direction == UP:
                    self.cur_vol += self.edit_stepvol
                    self.base = 10
                elif self.start_dir == UP and self.direction == DOWN:
                    self.cur_vol -= self.edit_stepvol
                    self.base = 10
                elif self.start_dir == DOWN and self.direction == DOWN:
                    self.cur_vol += self.edit_stepvol
                    self.base = -10
                elif self.start_dir == DOWN and self.direction == UP:
                    self.cur_vol -= self.edit_stepvol
                    self.base = -10
                else:
                    logging.info(f"No Codition :: {self.start_dir} :: {self.direction} :: { self.cur_vol }")
                
                value = self.cur_vol

                if (self.toggle and self.start_dir == UP and value <= 0) or \
                   (self.toggle and self.start_dir == DOWN and value > 0):
                    self.toggle = False
                    if self.channel:
                        device.write_registers(self.client, device.WRITE_TOGGLE, device.REVERSE, self.ptype)
                    else:
                        device.write_registers(self.client, device.WRITE_TOGGLE_1U, device.REVERSE, self.ptype)
                    time.sleep(3)

                device.write_registers(self.client, device.WRITE_VOLTAGE1, value, self.ptype)
                
                self.dutytimer.timeout.connect(self.change_onoff_voltage1)
                self.dutytimer.start(self.DUTY_PERIOD)

            else:
                self.status = OFF
                if self.toggle:
                    value = self.base
                else:
                    value = -self.base
                device.write_registers(self.client, device.WRITE_VOLTAGE1, value, self.ptype)
                self.dutytimer.timeout.connect(self.change_onoff_voltage1)
                self.dutytimer.start(self.OFFTIME_PERIOD)

    def get_onoff_step2_vol1(self):
        result = device.read_data_vol(self.client, device.READ_VOLTAGE1, self.ptype)
        if result:
            self.countN = self.countN  + 1

            # 데이터 값을 Array 에 저장하기 
            self.indexs3.append(self.countN)
            voltage1 = c_int16(result[0]).value
            self.vol3s.append(voltage1)

            if self.countN % 10 == 3:
                # logging.info(f"STEP 2 :: VOL 1 :: {result} :: {self.indexs3[-30:]}")
                # logging.info(f"STEP 2 :: VOL 1 :: {self.vol3s[-30:]}\n\n")
                self.draw_graph3()

    def change_onoff_voltage2(self):
        self.dutytimer.stop()
        self.dutytimer = None

        if (self.start_dir == UP and self.direction == DOWN and self.cur_vol < self.edit_stopvol) or \
           (self.start_dir == DOWN and self.direction == UP and self.cur_vol > self.edit_stopvol) :
            logging.info(f"STEP 2 :: VOL 2 :: End :: {self.start_dir} :: {self.direction} :: {self.cur_vol} :: {self.edit_stopvol} \n\n")
            self.dtimer.stop()
            self.dtimer = None 

            self.draw_graph4()

            ## STOP
            device.write_registers(self.client, 
                    device.WRITE_BIT_POWER_ON, device.STOP_VALUE, self.ptype)
            

        elif self.start_dir == UP and self.direction == UP and self.cur_vol > self.edit_maxvol:
            self.direction = DOWN
            self.dutytimer = QtCore.QTimer()
            self.dutytimer.timeout.connect(self.change_onoff_voltage2)
            self.dutytimer.start(self.DUTY_PERIOD)

        elif self.start_dir == DOWN and self.direction == DOWN and self.cur_vol < self.edit_maxvol:
            self.direction = UP
            self.dutytimer = QtCore.QTimer()
            self.dutytimer.timeout.connect(self.change_onoff_voltage2)
            self.dutytimer.start(self.DUTY_PERIOD)

        else:
            self.dutytimer = QtCore.QTimer()
            if self.status == OFF:
                self.status = ON

                if self.start_dir == UP and self.direction == UP:
                    self.cur_vol += self.edit_stepvol
                    self.base = 10
                elif self.start_dir == UP and self.direction == DOWN:
                    self.cur_vol -= self.edit_stepvol
                    self.base = 10
                elif self.start_dir == DOWN and self.direction == DOWN:
                    self.cur_vol += self.edit_stepvol
                    self.base = -10
                elif self.start_dir == DOWN and self.direction == UP:
                    self.cur_vol -= self.edit_stepvol
                    self.base = -10
                else:
                    logging.info(f"No Codition :: {self.start_dir} :: {self.direction} :: { self.cur_vol }")
            
                
                value = self.cur_vol

                if (self.toggle and self.start_dir == UP and value <= 0) or \
                    (self.toggle and self.start_dir == DOWN and value > 0):
                    self.toggle = False
                    if self.channel:
                        device.write_registers(self.client, device.WRITE_TOGGLE, device.REVERSE, self.ptype)
                    else:
                        device.write_registers(self.client, device.WRITE_TOGGLE_1U, device.REVERSE, self.ptype)
                    time.sleep(3)

                device.write_registers(self.client, device.WRITE_VOLTAGE2, value, self.ptype)
                
                self.dutytimer.timeout.connect(self.change_onoff_voltage2)
                self.dutytimer.start(self.DUTY_PERIOD)

            else:
                self.status = OFF
                if self.toggle:
                    value = self.base
                else:
                    value = -self.base
                device.write_registers(self.client, device.WRITE_VOLTAGE2, value, self.ptype)
                self.dutytimer.timeout.connect(self.change_onoff_voltage2)
                self.dutytimer.start(self.OFFTIME_PERIOD)


    def get_onoff_step2_vol2(self):
        result = device.read_data_vol(self.client, device.READ_VOLTAGE2, self.ptype)
        if result:
            self.countN = self.countN  + 1

            # 데이터 값을 Array 에 저장하기 
            self.indexs4.append(self.countN)
            voltage2 = c_int16(result[0]).value
            self.vol4s.append(voltage2)

            if self.countN % 10 == 3:
                # logging.info(f"STEP 2 :: VOL 2 :: {result} :: {self.indexs4[-30:]}")
                # logging.info(f"STEP 2 :: VOL 2 :: {self.vol4s[-30:]}\n\n")
                self.draw_graph4()
                

    def draw_graph1(self):
        self.gpwd1.plot(self.indexs, self.vol_ch1s, pen=self.pen_vol1)
        
    def draw_graph2(self):
        self.gpwd2.plot(self.indexs, self.vol_ch2s, pen=self.pen_vol2)
     
     
    def save1_func(self):
        FileSave = QFileDialog.getSaveFileName(self, '이미지 파일 저장', './data')
        pngfile = FileSave[0]

        ## first 
        name = f"{pngfile}_step1_v1.png"
        exporter = pyGraph.exporters.ImageExporter(self.gpwd1.plotItem)
        exporter.parameters()['width'] = 800
        exporter.export(name)

        dataset = pd.DataFrame({
                    'indexs': self.indexs,
                    'vol1s': self.vol_ch1s,
        })
        csvfile = f"{pngfile}_step1_v1.csv"
        dataset.to_csv(csvfile, sep=',', index=False)

        if self.channel:
            name = f"{pngfile}_step1_v2.png"
            exporter = pyGraph.exporters.ImageExporter(self.gpwd2.plotItem)
            exporter.parameters()['width'] = 800
            exporter.export(name)

            dataset = pd.DataFrame({
                        "indexs": self.indexs,
                        'vol2s': self.vol_ch2s,
            })
            csvfile = f"{pngfile}_step1_v2.csv"
            dataset.to_csv(csvfile, sep=',', index=False)


        name = f"{pngfile}_step2_v1.png"
        exporter = pyGraph.exporters.ImageExporter(self.gpwd3.plotItem)
        exporter.parameters()['width'] = 800
        exporter.export(name)

        dataset = pd.DataFrame({
                    'indexs': self.indexs3,
                    'vol3s': self.vol3s,
        })
        csvfile = f"{pngfile}_step2_v1.csv"
        dataset.to_csv(csvfile, sep=',', index=False)


        if self.channel:
            name = f"{pngfile}_step2_v2.png"
            exporter = pyGraph.exporters.ImageExporter(self.gpwd4.plotItem)
            exporter.parameters()['width'] = 800
            exporter.export(name)

            dataset = pd.DataFrame({
                        'indexs': self.indexs4,
                        'vol4s': self.vol4s,
            })
            csvfile = f"{pngfile}_step2_v2.csv"
            dataset.to_csv(csvfile, sep=',', index=False)


    def stop_func(self):
        self.dutytimer.stop()
        self.dutytimer = None 

        self.dtimer.stop()
        self.dtimer = None 

        device.write_registers(self.client, 
                device.WRITE_BIT_POWER_ON, device.STOP_VALUE, self.ptype)



    def save2_func(self):
        FileSave = QFileDialog.getSaveFileName(self, '이미지 파일 저장', './data')
        pngfile = FileSave[0]

        exporter = pyGraph.exporters.ImageExporter(self.gpwd3.plotItem)
        exporter.parameters()['width'] = 800   # (note this also affects height parameter)
        logging.info(pngfile)
        exporter.export(pngfile)


# 2024. 2. 29일 변경
class MySpinEdit:
    def __init__(self, title, val=0, vmin=1, vmax=300, vstep=1, *args, **kwargs):
        self.red = 'margin: -1px; padding: 0px; font-size: 14pt; color: red;background-color: white'
        self.blue = 'margin: -1px; padding: 0px; font-size: 14pt; color: blue;background-color: white'

        self.title = title 
        self.vmin = vmin
        self.vmax = vmax
        
        self.edit = QSpinBox()
        self.edit.setFixedHeight(15)
        self.edit.setMaximumHeight(15)
        self.edit.setStyleSheet(self.blue)
        self.edit.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self.edit.setRange(vmin, vmax)
        self.edit.setSingleStep(vstep)
        self.edit.setValue(val)

    def display(self):
        grp_edit = QGroupBox()
        layout_edit = QGridLayout()
        grp_edit.setLayout(layout_edit)

        edit_label = QLabel(self.title)
        vrange = f"{self.vmin} ~ {self.vmax}"
        edit_range = QLabel(vrange)
        layout_edit.addWidget(edit_label, 0, 0)
        layout_edit.addWidget(self.edit, 0, 1)
        layout_edit.addWidget(edit_range, 1, 0, 2, 1)

        return grp_edit

    def value(self):
        return self.edit.value()

    def warning(self):
        self.edit.setStyleSheet(self.red)

    def normal(self):
        self.edit.setStyleSheet(self.blue)


# 2024. 2. 29일 변경
class InspParam(QWidget):
    def __init__(self):
        super(InspParam, self).__init__()
        self.edit_stepvol_ch1 = MySpinEdit("STEP VOL(V)", 500, 0, 5000, 1)
        self.edit_maxvol_ch1 = MySpinEdit("MAX VOL(V)", 5000, -5000, 5000, 100)

        self.edit_stepvol_ch2 = MySpinEdit("STEP VOL(V)", 500, 0, 5000, 1)
        self.edit_maxvol_ch2= MySpinEdit("MAX VOL(V)", 5000, -5000, 5000, 100)

        self.edit_duty = MySpinEdit("DUTY(초,sec)", 60, 1, 300, 5)
        self.edit_offtime = MySpinEdit("OFF(초,sec)", 30, 1, 300, 5)
        self.edit_lasttime = MySpinEdit("LAST(초,sec)", 30, 1, 300, 5)

    def display(self):
        grp_params = QGroupBox("")
        layout_params = QHBoxLayout()
        grp_params.setLayout(layout_params)

        ## Channel #1 
        grp_ch1 = QGroupBox("Channel #1")
        layout_ch1 = QHBoxLayout()
        grp_ch1.setLayout(layout_ch1)

        ## STEP VOLTAGE
        grp_stepvol_ch1 = self.edit_stepvol_ch1.display()
        layout_ch1.addWidget(grp_stepvol_ch1)
        ## MAX VOLTAGE
        grp_maxvol_ch1 = self.edit_maxvol_ch1.display()
        layout_ch1.addWidget(grp_maxvol_ch1)
        layout_params.addWidget(grp_ch1)

        ## Channel #2 
        grp_ch2 = QGroupBox("Channel #2")
        layout_ch2 = QHBoxLayout()
        grp_ch2.setLayout(layout_ch2)
        ## STEP VOLTAGE
        grp_stepvol_ch2 = self.edit_stepvol_ch2.display()
        layout_ch2.addWidget(grp_stepvol_ch2)
        ## MAX VOLTAGE
        grp_maxvol_ch2 = self.edit_maxvol_ch2.display()
        layout_ch2.addWidget(grp_maxvol_ch2)
        layout_params.addWidget(grp_ch2)

        ## TIME
        grp_time = QGroupBox("Time")
        layout_time = QHBoxLayout()
        grp_time.setLayout(layout_time)
        ## DUTY TIME
        grp_duty = self.edit_duty.display()
        layout_time.addWidget(grp_duty)
        ## OFF TIME
        grp_offtime = self.edit_offtime.display()
        layout_time.addWidget(grp_offtime)
        ## LAST TIME
        grp_lasttime = self.edit_lasttime.display()
        layout_time.addWidget(grp_lasttime)

        layout_params.addWidget(grp_time)

        return grp_params

    def get_stepvol_ch1(self):
        return self.edit_stepvol_ch1.value()

    def get_maxvol_ch1(self):
        return self.edit_maxvol_ch1.value()

    def get_stepvol_ch2(self):
        return self.edit_stepvol_ch2.value()

    def get_maxvol_ch2(self):
        return self.edit_maxvol_ch2.value()

    def get_duty(self):
        return self.edit_duty.value()

    def get_offtime(self):
        return self.edit_offtime.value()

    def get_lasttime(self):
        return self.edit_lasttime.value()


# 2024. 2. 29일 변경
PLUS_VOL = 4
MINUS_VOL = -4
DUTY = "DUTY"
OFF = "OFF"
class InspectWindow(QDialog):
    def __init__(self, Dialog, client, channel):
        super().__init__()
        self.title = '출하 검사 Window'
        self.client = client
        # self.channel = 1
        self.channel = channel
        self.params = InspParam()
        self.ptype = 'RTU'
        self.mode = 3
        self.time = 0

        self.pen_vol1 = pyGraph.mkPen(width=1, color=(255, 0, 0))
        self.pen_vol2 = pyGraph.mkPen(width=1, color=(0, 0, 255))
        self.pen_vol3 = pyGraph.mkPen(width=1, color=(0, 0, 255))
        self.pen_vol4 = pyGraph.mkPen(width=1, color=(204, 0, 153))

        self.edit_stepvol = 0
        self.edit_maxvol = 0
        self.val_duty = 0
        self.val_offtime = 0

        self.dtimer = None
        self.stimer = None
        self.dutytimer = None
        self.status = OFF

        self.ch1_updown = UP
        self.ch2_updown = UP

        self.DATA_PERIOD = 1000
        self.DUTY_PERIOD = 3000
        self.OFFTIME_PERIOD = 3000

        self.cur_vol_ch1 = PLUS_VOL
        self.cur_vol_ch2 = PLUS_VOL
        self.countN = 0
        self.indexs = []
        self.vol_ch1s = []
        self.vol_ch2s = []
        self.cur_ch1s = []
        self.cur_ch2s = []
        self.leak_ch1s = []
        self.leak_ch2s = []
        self.cps = []

        self.direction = UP
        self.start_dir = None

        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width(), screensize.height()) 

        main_layer = QVBoxLayout()
        self.setWindowTitle(self.title)
        self.setLayout(main_layer)

        ## 1st
        ## Param Layer 
        group_param = self.params.display()
        main_layer.addWidget(group_param)

        ## 2nd
        grp_button = QGroupBox("")
        layout_button = QHBoxLayout()
        grp_button.setLayout(layout_button)

        ####################################
        ## CH BOTH Buttong 
        grp_both = QGroupBox("Select")
        layout_both = QHBoxLayout()
        grp_both.setLayout(layout_both)

        self.rd_both = QRadioButton("Ch1 & Ch2 동시")
        self.rd_both.clicked.connect(lambda:self.radioBtnClicked("BOTH"))
        self.rd_ch1 = QRadioButton("Ch1 만")
        self.rd_ch1.clicked.connect(lambda:self.radioBtnClicked("CH1"))
        self.rd_ch2 = QRadioButton("Ch2 만")
        self.rd_ch2.clicked.connect(lambda:self.radioBtnClicked("CH2"))
        if self.channel:
            self.rd_both.setChecked(True)
        else:
            self.mode = 1
            self.rd_ch1.setChecked(True)
            self.rd_both.setEnabled(False)
            self.rd_ch2.setEnabled(False)

        layout_both.addWidget(self.rd_both)
        layout_both.addWidget(self.rd_ch1)
        layout_both.addWidget(self.rd_ch2)
        layout_button.addWidget(grp_both)


        ####################################
        ## CH1
        grp_ch1 = QGroupBox("CH #1")
        layout_ch1 = QHBoxLayout()
        grp_ch1.setLayout(layout_ch1)
        
        self.rd_ch1_up = QRadioButton("UP 방향")
        self.rd_ch1_up.setChecked(True)
        self.rd_ch1_up.clicked.connect(lambda:self.radioSelectClicked(1,  "UP"))
        self.rd_ch1_down = QRadioButton("DOWN 방향")
        self.rd_ch1_down.clicked.connect(lambda:self.radioSelectClicked(1, "DOWN"))

        layout_ch1.addWidget(self.rd_ch1_up)
        layout_ch1.addWidget(self.rd_ch1_down)
        layout_button.addWidget(grp_ch1)


        ####################################
        ## CH2
        grp_ch2 = QGroupBox("CH #2")
        layout_ch2 = QHBoxLayout()
        grp_ch2.setLayout(layout_ch2)
        
        self.rd_ch2_up = QRadioButton("UP 방향")
        self.rd_ch2_up.setChecked(True)
        self.rd_ch2_up.clicked.connect(lambda:self.radioSelectClicked(2, "UP"))
        self.rd_ch2_down = QRadioButton("DOWN 방향")
        self.rd_ch2_down.clicked.connect(lambda:self.radioSelectClicked(2, "DOWN"))

        layout_ch2.addWidget(self.rd_ch2_up)
        layout_ch2.addWidget(self.rd_ch2_down)
        if self.channel:
            layout_button.addWidget(grp_ch2)

        ####################################
        ## Button
        self.btn_start = QPushButton("START")
        self.btn_start.clicked.connect(self.setting_func)
        
        self.btn_stop = QPushButton("STOP")
        self.btn_stop.clicked.connect(self.final_off)
        self.btn_stop.setEnabled(False)

        self.btn_save = QPushButton("데이터/이미지 저장")
        self.btn_save.clicked.connect(self.save1_func)
        self.btn_save.setEnabled(False)

        layout_button.addWidget(self.btn_start)
        layout_button.addWidget(self.btn_stop)
        layout_button.addWidget(self.btn_save)

        self.display_time = QLCDNumber()
        self.display_time.setDigitCount(5)
        self.display_time.setStyleSheet('background: white')
        timedisplay = '00:00'
        self.display_time.display(timedisplay)
        layout_button.addWidget(self.display_time)

        main_layer.addWidget(grp_button)

        ####################################
        ## Graph 1
        grp_graph1 = QGroupBox("Channel #1")
        layer_graph1 = QVBoxLayout()
        grp_graph1.setLayout(layer_graph1)
        
        self.gpwd1 = pyGraph.PlotWidget()
        self.gpwd1.setBackground('w')
        self.gpwd1.setMouseEnabled(x=False, y=False)
        self.gpwd1.setTitle("Channel #1", color=(255, 0, 0), size="20pt")
        self.gpwd1.plot([0], [0], pen=self.pen_vol1)
        layer_graph1.addWidget(self.gpwd1)

        main_layer.addWidget(grp_graph1)

        ####################################
        ## Graph 2
        grp_graph2 = QGroupBox("Channel #2")
        layer_graph2 = QVBoxLayout()
        grp_graph2.setLayout(layer_graph2)

        self.gpwd2 = pyGraph.PlotWidget()
        self.gpwd2.setBackground('w')
        self.gpwd2.setMouseEnabled(x=False, y=False)
        self.gpwd2.setTitle("Channel #2", color=(0, 0, 255), size="20pt")
        self.gpwd2.plot([0], [0], pen=self.pen_vol2)
        layer_graph2.addWidget(self.gpwd2)

        if self.channel:
            main_layer.addWidget(grp_graph2)

        ## Button
        self.buttonBox = QDialogButtonBox()
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        main_layer.addWidget(self.buttonBox)

        self.buttonBox.accepted.connect(self.on_accepted)
        self.buttonBox.rejected.connect(self.reject)

    def radioBtnClicked(self, ptype):
        if ptype == "BOTH":
            self.mode = 3
        elif ptype == "CH1":
            self.mode = 1
        elif ptype == "CH2":
            self.mode = 2
        else:
            self.mode = 3

    def radioSelectClicked(self, ch, ptype):
        if ch == 1:
            if ptype == 'DOWN':
                self.ch1_updown = DOWN
            else:
                self.ch1_updown = UP
        elif ch == 2:
            if ptype == 'DOWN':
                self.ch2_updown = DOWN
            else:
                self.ch2_updown = UP
        else:
            self.ch1_updown = UP
            self.ch2_updown = UP

    def on_accepted(self):
        self.accept()


    def get_time(self):
        self.time += 1
        timedisplay = '{:02d}:{:02d}'.format(
                (self.time // 60) % 60, self.time % 60)
        self.display_time.display(timedisplay)


    def setting_func(self):
        self.btn_start.setEnabled(False)
        self.btn_stop.setEnabled(True)
        self.btn_save.setEnabled(False)

        if self.mode == 1:
            self.val_stepvol_ch1 = self.params.get_stepvol_ch1()
            self.val_maxvol_ch1 = self.params.get_maxvol_ch1()
        elif self.mode == 2:
            self.val_stepvol_ch2 = self.params.get_stepvol_ch2()
            self.val_maxvol_ch2 = self.params.get_maxvol_ch2()
        else:
            self.val_stepvol_ch1 = self.params.get_stepvol_ch1()
            self.val_maxvol_ch1 = self.params.get_maxvol_ch1()
            self.val_stepvol_ch2 = self.params.get_stepvol_ch2()
            self.val_maxvol_ch2 = self.params.get_maxvol_ch2()

        self.val_duty = self.params.get_duty()
        self.val_offtime = self.params.get_offtime()
        self.val_lasttime = self.params.get_lasttime()

        self.cur_vol_ch1 = PLUS_VOL
        self.cur_vol_ch2 = PLUS_VOL

        self.countN = 0
        self.indexs = []
        self.vol_ch1s = []
        self.vol_ch2s = []
        self.cur_ch1s = []
        self.cur_ch2s = []
        self.leak_ch1s = []
        self.leak_ch2s = []
        self.cps = []

        self.toggle = True
        self.time = 0

        if self.ch1_updown == DOWN:
            self.val_maxvol_ch1 = -self.val_maxvol_ch1
            self.cur_vol_ch1 = MINUS_VOL

        if self.ch2_updown == DOWN:
            self.val_maxvol_ch2 = -self.val_maxvol_ch2
            self.cur_vol_ch2 = MINUS_VOL

        timedisplay = '00:00'
        self.display_time.display(timedisplay)

        # FORWARD 설정
        if self.channel:
            device.write_registers(self.client, device.WRITE_TOGGLE, device.FORWARD, self.ptype)
            device.write_registers(self.client, device.WRITE_VOLTAGE1, self.cur_vol_ch1, self.ptype)
            device.write_registers(self.client, device.WRITE_VOLTAGE2, self.cur_vol_ch2, self.ptype)

        else:
            device.write_registers(self.client, device.WRITE_TOGGLE_1U, device.FORWARD, self.ptype)
            device.write_registers(self.client, device.WRITE_VOLTAGE1, self.cur_vol_ch1, self.ptype)

        # START VOL 설정
        
        # GRAPH
        self.gpwd1.clear()
        if self.channel:
            self.gpwd2.clear()

        ## DUTY_PERIOD
        self.DUTY_PERIOD = self.val_duty * 1000
        self.OFFTIME_PERIOD = self.val_offtime * 1000
        self.LASTIME_PERIOD = abs(self.val_lasttime - self.val_duty) * 1000
        
        ## RUN 
        device.write_registers(self.client, device.WRITE_BIT_POWER_ON, device.RUN_VALUE, self.ptype)
        logging.info("DEVICE START !!! \n\n")

        # 시계 display
        self.stimer = QtCore.QTimer()
        self.stimer.setInterval(1000)
        self.stimer.timeout.connect(self.get_time)
        self.stimer.start()

        self.dtimer = QtCore.QTimer()
        self.dtimer.setInterval(1000)
        self.dtimer.timeout.connect(self.get_data)
        self.dtimer.start()

        self.status = DUTY
        self.dutytimer = QtCore.QTimer()
        self.dutytimer.setInterval(self.DUTY_PERIOD)
        self.dutytimer.timeout.connect(self.duty_stop)
        self.dutytimer.start()

    def get_data(self):
        if self.channel:
            result = device.read_data(self.client, self.ptype)
            # result = device.read_data_virtual(self.client, [self.cur_vol_ch1+1, 0, self.cur_vol_ch2+2])
            print(result)
            if result:
                self.countN = self.countN  + 1

                # 데이터 값을 Array 에 저장하기 
                self.indexs.append(self.countN)

                voltage1 = c_int16(result[0]).value
                self.vol_ch1s.append(voltage1)

                voltage2 = c_int16(result[2]).value
                self.vol_ch2s.append(voltage2)

                self.cur_ch1s.append(result[1])
                self.cur_ch2s.append(result[3])

                self.cps.append(result[7])
                self.leak_ch1s.append(result[8])
                self.leak_ch2s.append(result[9])

        else:
            result = device.read_data_1u(self.client, self.ptype)
            print(result)
            if result:
                self.countN = self.countN  + 1

                # 데이터 값을 Array 에 저장하기 
                self.indexs.append(self.countN)

                voltage1 = c_int16(result[0]).value
                self.vol_ch1s.append(voltage1)
                self.cur_ch1s.append(result[1])
                self.vol_ch2s.append(0)
                self.cur_ch2s.append(0)
                self.cps.append(0)
                self.leak_ch1s.append(result[2])
                self.leak_ch2s.append(0)

        if self.mode == 1:
            if self.countN % 5 == 2:
                self.draw_graph1()
        elif self.mode == 2:
            if self.countN % 5 == 2:
                self.draw_graph2()
        else:
            if self.countN % 5 == 2:
                self.draw_graph1()
                self.draw_graph2()


    def duty_stop(self):
        self.dutytimer.stop()
        self.dutytimer = None
        ## 모두 MAX 보다 높을 때,
        if self.ch1_updown == UP and self.ch2_updown == UP:
            if self.mode == 3 and (self.cur_vol_ch1 >= self.val_maxvol_ch1) and (self.cur_vol_ch2 >= self.val_maxvol_ch2):
                print("if self.ch1_updown == UP and self.ch2_updown == UP:")
                self.stop_func()
                return
            ## Ch1 > Max and Ch2 < max
            elif self.mode == 1 and (self.cur_vol_ch1 >= self.val_maxvol_ch1):
                self.stop_func()
                return
            elif self.mode == 2 and (self.cur_vol_ch2 >= self.val_maxvol_ch2):
                self.stop_func()
                return
            else:
                if self.mode == 1:
                    device.write_registers(self.client, device.WRITE_VOLTAGE1, device.PLUS_VOL, self.ptype)
                elif self.mode == 2:
                    device.write_registers(self.client, device.WRITE_VOLTAGE2, device.PLUS_VOL, self.ptype)
                else:
                    device.write_registers(self.client, device.WRITE_VOLTAGE1, device.PLUS_VOL, self.ptype)
                    device.write_registers(self.client, device.WRITE_VOLTAGE2, device.PLUS_VOL, self.ptype)

        elif self.ch1_updown == UP and self.ch2_updown == DOWN:
            if self.mode == 3 and (self.cur_vol_ch1 >= self.val_maxvol_ch1) and (self.cur_vol_ch2 <= self.val_maxvol_ch2):
                self.stop_func()
                return
            ## Ch1 > Max and Ch2 < max
            elif self.mode == 1 and (self.cur_vol_ch1 >= self.val_maxvol_ch1):
                self.stop_func()
                return
            elif self.mode == 2 and (self.cur_vol_ch2 <= self.val_maxvol_ch2):
                self.stop_func()
                return
            else:
                if self.mode == 1:
                    device.write_registers(self.client, device.WRITE_VOLTAGE1, device.PLUS_VOL, self.ptype)
                elif self.mode == 2:
                    device.write_registers(self.client, device.WRITE_VOLTAGE2, device.MINUS_VOL, self.ptype)
                else:
                    device.write_registers(self.client, device.WRITE_VOLTAGE1, device.PLUS_VOL, self.ptype)
                    device.write_registers(self.client, device.WRITE_VOLTAGE2, device.MINUS_VOL, self.ptype)

        elif self.ch1_updown == DOWN and self.ch2_updown == UP:
            if self.mode == 3 and (self.cur_vol_ch1 <= self.val_maxvol_ch1) and (self.cur_vol_ch2 >= self.val_maxvol_ch2):
                self.stop_func()
                return
            ## Ch1 > Max and Ch2 < max
            elif self.mode == 1 and (self.cur_vol_ch1 <= self.val_maxvol_ch1):
                self.stop_func()
                return
            elif self.mode == 2 and (self.cur_vol_ch2 >= self.val_maxvol_ch2):
                self.stop_func()
                return
            else:
                if self.mode == 1:
                    device.write_registers(self.client, device.WRITE_VOLTAGE1, device.MINUS_VOL, self.ptype)
                elif self.mode == 2:
                    device.write_registers(self.client, device.WRITE_VOLTAGE2, device.PLUS_VOL, self.ptype)
                else:
                    device.write_registers(self.client, device.WRITE_VOLTAGE1, device.MINUS_VOL, self.ptype)
                    device.write_registers(self.client, device.WRITE_VOLTAGE2, device.PLUS_VOL, self.ptype)

        elif self.ch1_updown == DOWN and self.ch2_updown == DOWN:
            if self.mode == 3 and (self.cur_vol_ch1 <= self.val_maxvol_ch1) and (self.cur_vol_ch2 <= self.val_maxvol_ch2):
                self.stop_func()
                return
            ## Ch1 > Max and Ch2 < max
            elif self.mode == 1 and (self.cur_vol_ch1 <= self.val_maxvol_ch1):
                self.stop_func()
                return
            elif self.mode == 2 and (self.cur_vol_ch2 <= self.val_maxvol_ch2):
                self.stop_func()
                return
            else:
                if self.mode == 1:
                    device.write_registers(self.client, device.WRITE_VOLTAGE1, device.MINUS_VOL, self.ptype)
                elif self.mode == 2:
                    device.write_registers(self.client, device.WRITE_VOLTAGE2, device.MINUS_VOL, self.ptype)
                else:
                    device.write_registers(self.client, device.WRITE_VOLTAGE1, device.MINUS_VOL, self.ptype)
                    device.write_registers(self.client, device.WRITE_VOLTAGE2, device.MINUS_VOL, self.ptype)
            
        self.status == OFF
        self.dutytimer = QtCore.QTimer()
        self.dutytimer.setInterval(self.OFFTIME_PERIOD)
        self.dutytimer.timeout.connect(self.offtime_stop)
        self.dutytimer.start()
        

    def offtime_stop(self):
        self.dutytimer.stop()
        self.dutytimer = None

        if self.ch1_updown == UP and self.ch2_updown == UP:
            if self.mode == 3 and (self.cur_vol_ch1 < self.val_maxvol_ch1) and (self.cur_vol_ch2 < self.val_maxvol_ch2):
                self.cur_vol_ch1 += self.val_stepvol_ch1
                self.cur_vol_ch2 += self.val_stepvol_ch2
                device.write_registers(self.client, device.WRITE_VOLTAGE1, self.cur_vol_ch1, self.ptype)
                device.write_registers(self.client, device.WRITE_VOLTAGE2, self.cur_vol_ch2, self.ptype)

            elif self.mode == 3 and (self.cur_vol_ch1 < self.val_maxvol_ch1):
                self.cur_vol_ch1 += self.val_stepvol_ch1
                device.write_registers(self.client, device.WRITE_VOLTAGE1, self.cur_vol_ch1, self.ptype)

            elif self.mode == 3 and (self.cur_vol_ch2 < self.val_maxvol_ch2):
                self.cur_vol_ch2 += self.val_stepvol_ch2
                device.write_registers(self.client, device.WRITE_VOLTAGE2, self.cur_vol_ch2, self.ptype)

            elif self.mode == 1 and (self.cur_vol_ch1 < self.val_maxvol_ch1):
                self.cur_vol_ch1 += self.val_stepvol_ch1
                device.write_registers(self.client, device.WRITE_VOLTAGE1, self.cur_vol_ch1, self.ptype)

            elif self.mode == 2 and (self.cur_vol_ch2 < self.val_maxvol_ch2):
                self.cur_vol_ch2 += self.val_stepvol_ch2
                device.write_registers(self.client, device.WRITE_VOLTAGE2, self.cur_vol_ch2, self.ptype)

        elif self.ch1_updown == UP and self.ch2_updown == DOWN:
            if self.mode == 3 and (self.cur_vol_ch1 < self.val_maxvol_ch1) and (self.cur_vol_ch2 > self.val_maxvol_ch2):
                self.cur_vol_ch1 += self.val_stepvol_ch1
                self.cur_vol_ch2 -= self.val_stepvol_ch2
                device.write_registers(self.client, device.WRITE_VOLTAGE1, self.cur_vol_ch1, self.ptype)
                device.write_registers(self.client, device.WRITE_VOLTAGE2, self.cur_vol_ch2, self.ptype)

            elif self.mode == 3 and (self.cur_vol_ch1 < self.val_maxvol_ch1):
                self.cur_vol_ch1 += self.val_stepvol_ch1
                device.write_registers(self.client, device.WRITE_VOLTAGE1, self.cur_vol_ch1, self.ptype)

            elif self.mode == 3 and (self.cur_vol_ch2 < self.val_maxvol_ch2):
                self.cur_vol_ch2 -= self.val_stepvol_ch2
                device.write_registers(self.client, device.WRITE_VOLTAGE2, self.cur_vol_ch2, self.ptype)

            elif self.mode == 1 and (self.cur_vol_ch1 < self.val_maxvol_ch1):
                self.cur_vol_ch1 += self.val_stepvol_ch1
                device.write_registers(self.client, device.WRITE_VOLTAGE1, self.cur_vol_ch1, self.ptype)

            elif self.mode == 2 and (self.cur_vol_ch2 > self.val_maxvol_ch2):
                self.cur_vol_ch2 -= self.val_stepvol_ch2
                device.write_registers(self.client, device.WRITE_VOLTAGE2, self.cur_vol_ch2, self.ptype)

        elif self.ch1_updown == DOWN and self.ch2_updown == UP:
            if self.mode == 3 and (self.cur_vol_ch1 > self.val_maxvol_ch1) and (self.cur_vol_ch2 < self.val_maxvol_ch2):
                self.cur_vol_ch1 -= self.val_stepvol_ch1
                self.cur_vol_ch2 += self.val_stepvol_ch2
                device.write_registers(self.client, device.WRITE_VOLTAGE1, self.cur_vol_ch1, self.ptype)
                device.write_registers(self.client, device.WRITE_VOLTAGE2, self.cur_vol_ch2, self.ptype)

            elif self.mode == 3 and (self.cur_vol_ch1 < self.val_maxvol_ch1):
                self.cur_vol_ch1 -= self.val_stepvol_ch1
                device.write_registers(self.client, device.WRITE_VOLTAGE1, self.cur_vol_ch1, self.ptype)

            elif self.mode == 3 and (self.cur_vol_ch2 < self.val_maxvol_ch2):
                self.cur_vol_ch2 += self.val_stepvol_ch2
                device.write_registers(self.client, device.WRITE_VOLTAGE2, self.cur_vol_ch2, self.ptype)

            elif self.mode == 1 and (self.cur_vol_ch1 > self.val_maxvol_ch1):
                self.cur_vol_ch1 -= self.val_stepvol_ch1
                device.write_registers(self.client, device.WRITE_VOLTAGE1, self.cur_vol_ch1, self.ptype)

            elif self.mode == 2 and (self.cur_vol_ch2 < self.val_maxvol_ch2):
                self.cur_vol_ch2 += self.val_stepvol_ch2
                device.write_registers(self.client, device.WRITE_VOLTAGE2, self.cur_vol_ch2, self.ptype)

        elif self.ch1_updown == DOWN and self.ch2_updown == DOWN:
            if self.mode == 3 and (self.cur_vol_ch1 > self.val_maxvol_ch1) and (self.cur_vol_ch2 > self.val_maxvol_ch2):
                self.cur_vol_ch1 -= self.val_stepvol_ch1
                self.cur_vol_ch2 -= self.val_stepvol_ch2
                device.write_registers(self.client, device.WRITE_VOLTAGE1, self.cur_vol_ch1, self.ptype)
                device.write_registers(self.client, device.WRITE_VOLTAGE2, self.cur_vol_ch2, self.ptype)

            elif self.mode == 3 and (self.cur_vol_ch1 > self.val_maxvol_ch1):
                self.cur_vol_ch1 -= self.val_stepvol_ch1
                device.write_registers(self.client, device.WRITE_VOLTAGE1, self.cur_vol_ch1, self.ptype)

            elif self.mode == 3 and (self.cur_vol_ch2 > self.val_maxvol_ch2):
                self.cur_vol_ch2 -= self.val_stepvol_ch2
                device.write_registers(self.client, device.WRITE_VOLTAGE2, self.cur_vol_ch2, self.ptype)

            elif self.mode == 1 and (self.cur_vol_ch1 > self.val_maxvol_ch1):
                self.cur_vol_ch1 -= self.val_stepvol_ch1
                device.write_registers(self.client, device.WRITE_VOLTAGE1, self.cur_vol_ch1, self.ptype)

            elif self.mode == 2 and (self.cur_vol_ch2 > self.val_maxvol_ch2):
                self.cur_vol_ch2 -= self.val_stepvol_ch2
                device.write_registers(self.client, device.WRITE_VOLTAGE2, self.cur_vol_ch2, self.ptype)

        self.status == DUTY 
        self.dutytimer = QtCore.QTimer()
        self.dutytimer.setInterval(self.DUTY_PERIOD)
        self.dutytimer.timeout.connect(self.duty_stop)
        self.dutytimer.start()


    def draw_graph1(self):
        self.gpwd1.plot(self.indexs, self.vol_ch1s, pen=self.pen_vol1)
        
    def draw_graph2(self):
        self.gpwd2.plot(self.indexs, self.vol_ch2s, pen=self.pen_vol2)
     
    def save1_func(self):
        FileSave = QFileDialog.getSaveFileName(self, '이미지 파일 저장', './imgs')
        pngfile = FileSave[0]

        ## first 
        name = f"{pngfile}_channel1_voltage.png"
        exporter = pyGraph.exporters.ImageExporter(self.gpwd1.plotItem)
        exporter.parameters()['width'] = 800
        exporter.export(name)

        if self.channel:
            name = f"{pngfile}_channel2_voltage.png"
            exporter = pyGraph.exporters.ImageExporter(self.gpwd2.plotItem)
            exporter.parameters()['width'] = 800
            exporter.export(name)

        dataset = pd.DataFrame({
            'indexs': self.indexs,
            'channel#1 voltage': self.vol_ch1s,
            'channel#1 current': self.cur_ch1s,
            'channel#2 voltage': self.vol_ch2s,
            'channel#2 current': self.cur_ch2s,
            'capacitance': self.cps,
            'channel#1 leak current': self.leak_ch1s,
            'channel#2 leak current': self.leak_ch2s,
        })
        csvfile = f"{pngfile}_channel_data.csv"
        dataset.to_csv(csvfile, sep=',', index=False)


    def stop_func(self):
        self.dutytimer = QtCore.QTimer()
        self.dutytimer.setInterval(self.LASTIME_PERIOD)
        self.dutytimer.timeout.connect(self.final_stop)
        self.dutytimer.start()

    def final_stop(self):
        self.dutytimer.stop()
        self.dutytimer = None

        if self.ch1_updown == UP and self.ch2_updown == UP:
            if self.mode == 1:
                device.write_registers(self.client, device.WRITE_VOLTAGE1, device.PLUS_VOL, self.ptype)
            elif self.mode == 2:
                device.write_registers(self.client, device.WRITE_VOLTAGE2, device.PLUS_VOL, self.ptype)
            else:
                device.write_registers(self.client, device.WRITE_VOLTAGE1, device.PLUS_VOL, self.ptype)
                device.write_registers(self.client, device.WRITE_VOLTAGE2, device.PLUS_VOL, self.ptype)

        elif self.ch1_updown == UP and self.ch2_updown == DOWN:
            if self.mode == 1:
                device.write_registers(self.client, device.WRITE_VOLTAGE1, device.PLUS_VOL, self.ptype)
            elif self.mode == 2:
                device.write_registers(self.client, device.WRITE_VOLTAGE2, device.MINUS_VOL, self.ptype)
            else:
                device.write_registers(self.client, device.WRITE_VOLTAGE1, device.PLUS_VOL, self.ptype)
                device.write_registers(self.client, device.WRITE_VOLTAGE2, device.MINUS_VOL, self.ptype)

        elif self.ch1_updown == DOWN and self.ch2_updown == UP:
            if self.mode == 1:
                device.write_registers(self.client, device.WRITE_VOLTAGE1, device.MINUS_VOL, self.ptype)
            elif self.mode == 2:
                device.write_registers(self.client, device.WRITE_VOLTAGE2, device.PLUS_VOL, self.ptype)
            else:
                device.write_registers(self.client, device.WRITE_VOLTAGE1, device.MINUS_VOL, self.ptype)
                device.write_registers(self.client, device.WRITE_VOLTAGE2, device.PLUS_VOL, self.ptype)

        elif self.ch1_updown == DOWN and self.ch2_updown == DOWN:
            if self.mode == 1:
                device.write_registers(self.client, device.WRITE_VOLTAGE1, device.MINUS_VOL, self.ptype)
            elif self.mode == 2:
                device.write_registers(self.client, device.WRITE_VOLTAGE2, device.MINUS_VOL, self.ptype)
            else:
                device.write_registers(self.client, device.WRITE_VOLTAGE1, device.MINUS_VOL, self.ptype)
                device.write_registers(self.client, device.WRITE_VOLTAGE2, device.MINUS_VOL, self.ptype)
            
        self.dutytimer = QtCore.QTimer()
        self.dutytimer.setInterval(3000)
        self.dutytimer.timeout.connect(self.final_off)
        self.dutytimer.start()


    def final_off(self):
        try:
            self.stimer.stop()
            self.stimer = None 
        except Exception as e:
            pass

        try:
            self.dtimer.stop()
            self.dtimer = None 
        except Exception as e:
            pass

        try:
            self.dutytimer.stop()
            self.dutytimer = None
        except Exception as e:
            pass

        device.write_registers(self.client, 
                    device.WRITE_BIT_POWER_ON, device.STOP_VALUE, self.ptype)

        self.btn_start.setEnabled(True)
        self.btn_save.setEnabled(True)

        self.draw_graph1()
        if self.channel:
            self.draw_graph2()




class GraphWindow(QDialog):
    def __init__(self, Dialog, title, color, x, y):
        super().__init__()
        self.title = title
        self.x = x 
        self.y = y
        self.color = color
        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width(), screensize.height()) 

        main_layer = QVBoxLayout()
        self.setWindowTitle(self.title)
        self.setLayout(main_layer)

        self.graphWidget = pyGraph.PlotWidget()
        main_layer.addWidget(self.graphWidget)

        self.graphWidget.setBackground('w')
        self.graphWidget.setTitle(self.title, color=self.color, size="30pt")
    
        self.plot(self.x, self.y, self.color)

        self.buttonBox = QDialogButtonBox()
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        main_layer.addWidget(self.buttonBox)

        self.buttonBox.accepted.connect(self.on_accepted)
        self.buttonBox.rejected.connect(self.reject)


    def plot(self, x, y, color):
        pen = pyGraph.mkPen(color=color, width=2)
        self.graphWidget.plot(x, y, pen=pen, symbolSize=3, symbolBrush=('b'))

    def on_accepted(self):
        self.accept()


class ZoomWindow(QDialog):
    def __init__(self, Dialog, indexs, voltage1s, current1s, voltage2s, current2s, vbias, cpss, dechuck_starts, dechuck_ends):
        super().__init__()
        self.title = "ZOOM IN DATA"
        self.indexs = indexs
        self.voltage1s = voltage1s 
        self.current1s = current1s 
        self.voltage2s = voltage2s 
        self.current2s = current2s 
        self.vbias = vbias 
        self.cpss = cpss 
        self.dechuck_starts = dechuck_starts 
        self.dechuck_ends = dechuck_ends 
        # logging.info("ZoomWindow :: __init__ :: len(voltage1s) = ", len(voltage1s))

        self.pen_voltage1 = pyGraph.mkPen(width=2, color=(255, 0, 0))
        self.pen_current1 = pyGraph.mkPen(width=2, color=(0, 0, 255))
        self.pen_voltage2 = pyGraph.mkPen(width=2, color=(255, 191, 0))
        self.pen_current2 = pyGraph.mkPen(width=2, color=(26, 175, 51))
        self.pen_vbia = pyGraph.mkPen(width=2, color=(0,0,128))
        self.pen_cps = pyGraph.mkPen(width=2, color=(204, 153, 0))
        self.pen_dechuckstart = pyGraph.mkPen(width=2, color=(204, 0, 153))
        self.pen_dechuckend = pyGraph.mkPen(width=2, color=(102, 0, 204))

        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width(), screensize.height()) 

        main_layer = QVBoxLayout()
        self.setWindowTitle("ZOOM")
        self.setLayout(main_layer)

        self.main = pyGraph.PlotWidget()
        self.main.setBackground('w')
        self.sub = pyGraph.PlotWidget()
        self.sub.setBackground('w')

        pyGraph.setConfigOptions(antialias=True)
        main_layer.addWidget(self.main)
        main_layer.addWidget(self.sub)

        ## create mainAxis
        # First ViewBox
        self.axisA = self.main.plotItem
        self.axisA.setLabels(left='Voltage')
        self.axisA.showAxis('right')

        ## Second ViewBox
        self.axisB = pyGraph.ViewBox()
        self.axisB.setXLink(self.axisA)

        self.axisA.scene().addItem(self.axisB)
        self.axisA.getAxis('right').linkToView(self.axisB)
        self.axisA.getAxis('right').setLabel('Current', color='#0000ff')

        ## create Third ViewBox. 
        self.axisC = pyGraph.ViewBox()
        ax3 = pyGraph.AxisItem('right')
        self.axisA.layout.addItem(ax3, 2, 3)
        self.axisA.scene().addItem(self.axisC)
        ax3.linkToView(self.axisC)
        self.axisC.setXLink(self.axisA)
        ax3.setLabel('Vais/CPS', color='#00ff00')

        ## create Forth ViewBox. 
        self.axisD = pyGraph.ViewBox()
        ax4 = pyGraph.AxisItem('right')
        self.axisA.layout.addItem(ax4, 2, 4)
        self.axisA.scene().addItem(self.axisD)
        ax4.linkToView(self.axisD)
        self.axisD.setXLink(self.axisA)
        ax4.setLabel('Leak Current', color='#cc9900')

        ## Handle view resizing 
        def updateViews():
            ## view has resized; update auxiliary views to match
            # global self.axisA, self.axisB, self.axisC
            self.axisB.setGeometry(self.axisA.vb.sceneBoundingRect())
            self.axisC.setGeometry(self.axisA.vb.sceneBoundingRect())
            self.axisD.setGeometry(self.axisA.vb.sceneBoundingRect())
            self.axisB.linkedViewChanged(self.axisA.vb, self.axisB.XAxis)
            self.axisC.linkedViewChanged(self.axisA.vb, self.axisC.XAxis)
            self.axisD.linkedViewChanged(self.axisA.vb, self.axisD.XAxis)

        updateViews()
        self.axisA.vb.sigResized.connect(updateViews)

        self.axisA.plot(self.indexs, self.voltage1s, pen=self.pen_voltage1, name="Voltage1s")
        self.axisA.plot(self.indexs, self.voltage2s, pen=self.pen_voltage2, name="voltage2s")
        
        self.axisB.addItem(pyGraph.PlotCurveItem(self.indexs, self.current1s, pen=self.pen_current1, name="Current1s"))
        self.axisB.addItem(pyGraph.PlotCurveItem(self.indexs, self.current2s, pen=self.pen_current2, name="Current2s"))

        self.axisC.addItem(pyGraph.PlotCurveItem(self.indexs, self.cpss, pen=self.pen_cps, name="CPS"))
        self.axisC.addItem(pyGraph.PlotCurveItem(self.indexs, self.vbias, pen=self.pen_vbia, name="Vbias"))
        
        self.axisD.addItem(pyGraph.PlotCurveItem(self.indexs, self.dechuck_starts, pen=self.pen_dechuckstart, name="Leak_Current1"))
        self.axisD.addItem(pyGraph.PlotCurveItem(self.indexs, self.dechuck_ends, pen=self.pen_dechuckend, name="Leak_Current2"))

        linZone = pyGraph.LinearRegionItem([100,150])
        linZone.setZValue(-10)
        self.axisA.addItem(linZone)

        ## create subWin
        # First ViewBox
        self.subWin = self.sub.plotItem
        self.subWin.setLabels(left='Voltage')
        self.subWin.showAxis('right')

        ## Second ViewBox
        self.subxisB = pyGraph.ViewBox()
        self.subxisB.setXLink(self.subWin)

        self.subWin.scene().addItem(self.subxisB)
        self.subWin.getAxis('right').linkToView(self.subxisB)
        self.subWin.getAxis('right').setLabel('Current', color='#0000ff')

        ## create Third ViewBox. 
        self.subxisC = pyGraph.ViewBox()
        subax3 = pyGraph.AxisItem('right')
        self.subWin.layout.addItem(subax3, 2, 3)
        self.subWin.scene().addItem(self.subxisC)
        subax3.linkToView(self.subxisC)
        self.subxisC.setXLink(self.subWin)
        subax3.setLabel('Vais/CPS', color='#000080')

        ## create Third ViewBox. 
        self.subxisD = pyGraph.ViewBox()
        subax4 = pyGraph.AxisItem('right')
        self.subWin.layout.addItem(subax4, 2, 4)
        self.subWin.scene().addItem(self.subxisD)
        subax4.linkToView(self.subxisD)
        self.subxisD.setXLink(self.subWin)
        subax4.setLabel('Dechuck', color='#cc9900')


        def updateSubViews():
            # global self.axisA, self.subxisB, self.subxisC
            self.subxisB.setGeometry(self.subWin.vb.sceneBoundingRect())
            self.subxisC.setGeometry(self.subWin.vb.sceneBoundingRect())
            self.subxisD.setGeometry(self.subWin.vb.sceneBoundingRect())
            
            self.subxisB.linkedViewChanged(self.subWin.vb, self.subxisB.XAxis)
            self.subxisC.linkedViewChanged(self.subWin.vb, self.subxisC.XAxis)
            self.subxisD.linkedViewChanged(self.subWin.vb, self.subxisD.XAxis)

        updateSubViews()
        self.axisA.vb.sigResized.connect(updateSubViews)

        self.subWin.plot(self.indexs, self.voltage1s, pen=self.pen_voltage1, name="Voltage1s")
        self.subWin.plot(self.indexs, self.voltage2s, pen=self.pen_voltage2, name="voltage2s")
        
        self.subxisB.addItem(pyGraph.PlotCurveItem(self.indexs, self.current1s, pen=self.pen_current1, name="Current1s"))
        self.subxisB.addItem(pyGraph.PlotCurveItem(self.indexs, self.current2s, pen=self.pen_current2, name="Current2s"))

        self.subxisC.addItem(pyGraph.PlotCurveItem(self.indexs, self.vbias, pen=self.pen_vbia, name="Vbias"))
        self.subxisC.addItem(pyGraph.PlotCurveItem(self.indexs, self.cpss, pen=self.pen_cps, name="CPS"))

        self.subxisD.addItem(pyGraph.PlotCurveItem(self.indexs, self.dechuck_starts, pen=self.pen_dechuckstart, name="Leak_Current1"))
        self.subxisD.addItem(pyGraph.PlotCurveItem(self.indexs, self.dechuck_ends, pen=self.pen_dechuckend, name="Leak_Current2"))
        

        # Region 이 변경되면 오른쪽 그래프를 변경해 줌
        def updateChangePlot():
            self.subWin.setXRange(*linZone.getRegion(), padding=0)

        def updateRegion():
            linZone.setRegion(self.subWin.getViewBox().viewRange()[0])

        linZone.sigRegionChanged.connect(updateChangePlot)
        self.subWin.sigXRangeChanged.connect(updateRegion)

        updateChangePlot()

    def on_accepted(self):
        self.accept()


class ZoomWindow6(QDialog):

    def __init__(self, Dialog, indexs, voltage1s, current1s, voltage2s, current2s, vbias, cpss, dechuck_starts, dechuck_ends):
        super().__init__()
        self.title = "ZOOM IN DATA"
        self.indexs = indexs 
        self.voltage1s = voltage1s 
        self.current1s = current1s 
        self.voltage2s = voltage2s 
        self.current2s = current2s 
        self.vbias = vbias 
        self.cpss = cpss 
        self.dechuck_starts = dechuck_starts 
        self.dechuck_ends = dechuck_ends 

        self.pen_voltage1 = pyGraph.mkPen(width=2, color=(255, 0, 0))
        self.pen_current1 = pyGraph.mkPen(width=2, color=(0, 0, 255))
        self.pen_voltage2 = pyGraph.mkPen(width=2, color=(255, 191, 0))
        self.pen_current2 = pyGraph.mkPen(width=2, color=(0,255, 0))
        self.pen_vbia = pyGraph.mkPen(width=2, color=(26, 175, 51))
        self.pen_cps = pyGraph.mkPen(width=2, color=(204, 153, 0))
        self.pen_dechuckstart = pyGraph.mkPen(width=2, color=(204, 0, 153))
        self.pen_dechuckend = pyGraph.mkPen(width=2, color=(102, 0, 204))

        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width(), screensize.height()) 

        main_layer = QVBoxLayout()
        self.setWindowTitle("6 Windows")
        self.setLayout(main_layer)

        self.window = pyGraph.GraphicsLayoutWidget(show=True, title='8 Window Plot')
        pyGraph.setConfigOptions(antialias=True)
        main_layer.addWidget(self.window)

        voltage_plot = self.window.addPlot(title='OUT VOLTAGE 1, 2')
        voltage_plot.plot(x=self.indexs, y=self.voltage1s, pen=self.pen_voltage1, name="OUT VOLTAGE1")
        voltage_plot.plot(x=self.indexs, y=self.voltage2s, pen=self.pen_voltage2, name="OUT VOLTAGE2")

        current_plot = self.window.addPlot(title='OUT CURRENT 1, 2')
        current_plot.plot(x=self.indexs, y=self.current1s, pen=self.pen_current1, name="OUT CURRENT1")
        current_plot.plot(x=self.indexs, y=self.current2s, pen=self.pen_current2, name="OUT CURRENT2")

        self.window.nextRow()

        vias = self.window.addPlot(title='VBIAS')
        vias.plot(x=self.indexs, y=self.vbias, pen=self.pen_vbia, name="VBIAS")

        cps = self.window.addPlot(title='CPS')
        cps.plot(x=self.indexs, y=self.cpss, pen=self.pen_vbia, name="CPS")

        self.window.nextRow()

        d_start = self.window.addPlot(title='LEAK CURRENT1')
        d_start.plot(x=self.indexs, y=self.dechuck_starts, pen=self.pen_dechuckstart, name="LEAK CURRENT1")

        cps = self.window.addPlot(title='LEAK CURRENT2')
        cps.plot(x=self.indexs, y=self.dechuck_ends, pen=self.pen_dechuckend, name="LEAK CURRENT2")
        
    def on_accepted(self):
        self.accept()


class ZoomWindowFull(QDialog):

    def __init__(self, Dialog, indexs, voltage1s, current1s, voltage2s, current2s, vbias, cpss, dechuck_starts, dechuck_ends):
        super().__init__()
        self.title = "ZOOM IN DATA"
        self.indexs = indexs 
        self.voltage1s = voltage1s 
        self.current1s = current1s 
        self.voltage2s = voltage2s 
        self.current2s = current2s 
        self.vbias = vbias 
        self.cpss = cpss 
        self.dechuck_starts = dechuck_starts 
        self.dechuck_ends = dechuck_ends 

        self.pen_voltage1 = pyGraph.mkPen(width=2, color=(255, 0, 0))
        self.pen_current1 = pyGraph.mkPen(width=2, color=(0, 0, 255))
        self.pen_voltage2 = pyGraph.mkPen(width=2, color=(255, 191, 0))
        self.pen_current2 = pyGraph.mkPen(width=2, color=(26, 175, 51))
        self.pen_vbia = pyGraph.mkPen(width=2, color=(0,0,128))
        self.pen_cps = pyGraph.mkPen(width=2, color=(204, 153, 0))
        self.pen_dechuckstart = pyGraph.mkPen(width=2, color=(204, 0, 153))
        self.pen_dechuckend = pyGraph.mkPen(width=2, color=(102, 0, 204))

        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        self.setGeometry(0, 0, 1200, 800)

        main_layer = QVBoxLayout()
        self.setLayout(main_layer)

        self.window = pyGraph.PlotWidget()
        pyGraph.setConfigOptions(antialias=True)
        main_layer.addWidget(self.window)

        ## create mainAxis
        self.mainAxis = self.window.plotItem
        self.mainAxis.setLabels(left='Voltage')
        self.mainAxis.showAxis('right')

        ## Second ViewBox
        self.axisB = pyGraph.ViewBox()
        self.mainAxis.scene().addItem(self.axisB)
        self.mainAxis.getAxis('right').linkToView(self.axisB)
        self.axisB.setXLink(self.mainAxis)
        self.mainAxis.getAxis('right').setLabel('Current', color='#0000ff')

        ## create Third ViewBox. 
        self.axisC = pyGraph.ViewBox()
        ax3 = pyGraph.AxisItem('right')
        self.mainAxis.layout.addItem(ax3, 2, 3)
        self.mainAxis.scene().addItem(self.axisC)
        ax3.linkToView(self.axisC)
        self.axisC.setXLink(self.mainAxis)
        ax3.setLabel('Vais', color='#000080')

        ## Handle view resizing 
        def updateViews():
            ## view has resized; update auxiliary views to match
            # global self.mainAxis, self.axisB, self.axisC
            self.axisB.setGeometry(self.mainAxis.vb.sceneBoundingRect())
            self.axisC.setGeometry(self.mainAxis.vb.sceneBoundingRect())
            
            self.axisB.linkedViewChanged(self.mainAxis.vb, self.axisB.XAxis)
            self.axisC.linkedViewChanged(self.mainAxis.vb, self.axisC.XAxis)

        updateViews()
        self.mainAxis.vb.sigResized.connect(updateViews)

        self.graph_voltage1s =  self.mainAxis.plot(self.indexs, self.voltage1s, pen=self.pen_voltage1, name="Voltage1s")
        self.graph_current1s =  self.axisB.addItem(pyGraph.PlotCurveItem(self.indexs, self.current1s, pen=self.pen_current1, name="Current1s"))
        self.graph_voltage2s =  self.mainAxis.plot(self.indexs, self.voltage2s, pen=self.pen_voltage2, name="voltage2s")
        self.graph_current2s =  self.axisB.addItem(pyGraph.PlotCurveItem(self.indexs, self.current2s, pen=self.pen_current2, name="Current2s"))

        self.graph_vbias =  self.axisC.addItem(pyGraph.PlotCurveItem(self.indexs, self.vbias, pen=self.pen_vbia, name="Vbias"))
        self.graph_cpss =  self.mainAxis.plot(self.indexs, self.cpss, pen=self.pen_cps, name="Cps")
        self.graph_dechuck_starts =  self.mainAxis.plot(self.indexs, self.dechuck_starts, pen=self.pen_dechuckstart, name="Leak_Current1")
        self.graph_dechuck_ends =  self.axisC.addItem(pyGraph.PlotCurveItem(self.indexs, self.dechuck_ends, pen=self.pen_dechuckend, name="Leak_Current2"))

        
    def on_accepted(self):
        self.accept()


## Pulse DC Two Channel Compare
class GraphWindowTwo(QDialog):

    def __init__(self, Dialog, title, indexs, data):
        super().__init__()
        self.title = title
        self.indexs = indexs 
        self.data = data

        self.pen_mod1 = pyGraph.mkPen(width=2, color="#800000")
        self.pen_mod2 = pyGraph.mkPen(width=2, color="#00FF00")
        self.pen_mod3 = pyGraph.mkPen(width=2, color="#0000FF")
        self.pen_mod4 = pyGraph.mkPen(width=2, color="#808000")

        self.pen_mod5 = pyGraph.mkPen(width=2, color=(255, 127, 80))
        self.pen_mod6 = pyGraph.mkPen(width=2, color=(100, 149, 237))
        self.pen_mod7 = pyGraph.mkPen(width=2, color=(222, 49, 99))
        self.pen_mod8 = pyGraph.mkPen(width=2, color=(204, 204, 255))

        self.view_mod1 = True 
        self.view_mod2 = True 
        self.view_mod3 = True 
        self.view_mod4 = True 
        self.view_mod5 = True 
        self.view_mod6 = True 
        self.view_mod7 = True 
        self.view_mod8 = True 

        self.setupUI()

    def setupUI(self):
        self.setGeometry(0, 0, 1200, 800)

        main_layer = QVBoxLayout()
        self.setLayout(main_layer)

        btn_title = QPushButton(self.title)
        btn_title.setStyleSheet(f"background-color: white;color: #ED6032; font-size: 14px;font-weight: bold;")
        main_layer.addWidget(btn_title)

        grp_graph = QGroupBox("")
        layout_graph = QHBoxLayout()
        grp_graph.setLayout(layout_graph)

        self.chbox_mod1 = QCheckBox("#1 Mod")
        self.chbox_mod1.setStyleSheet('color:#800000;')
        self.chbox_mod1.setChecked(True)
        layout_graph.addWidget(self.chbox_mod1)

        self.chbox_mod2 = QCheckBox("#2 Mod")
        self.chbox_mod2.setStyleSheet('color:#00FF00;')
        self.chbox_mod2.setChecked(True)
        layout_graph.addWidget(self.chbox_mod2)

        self.chbox_mod3 = QCheckBox("#3 Mod")
        self.chbox_mod3.setStyleSheet('color:#0000FF;')
        self.chbox_mod3.setChecked(True)
        layout_graph.addWidget(self.chbox_mod3)

        self.chbox_mod4 = QCheckBox("#4 Mod")
        self.chbox_mod4.setStyleSheet('color:#808000;')
        self.chbox_mod4.setChecked(True)
        layout_graph.addWidget(self.chbox_mod4)

        self.chbox_mod5 = QCheckBox("#5 Mod")
        self.chbox_mod5.setStyleSheet('color:#FF7F50;')
        self.chbox_mod5.setChecked(True)
        layout_graph.addWidget(self.chbox_mod5)

        self.chbox_mod6 = QCheckBox("#6 Mod")
        self.chbox_mod6.setStyleSheet('color:#6495ED;')
        self.chbox_mod6.setChecked(True)
        layout_graph.addWidget(self.chbox_mod6)

        self.chbox_mod7 = QCheckBox("#7 Mod")
        self.chbox_mod7.setStyleSheet('color:#DE3163;')
        self.chbox_mod7.setChecked(True)
        layout_graph.addWidget(self.chbox_mod7)

        self.chbox_mod8 = QCheckBox("#8 Mod")
        self.chbox_mod8.setStyleSheet('color:#CCCCFF;')
        self.chbox_mod8.setChecked(True)
        layout_graph.addWidget(self.chbox_mod8)
        
        self.btn_update = QPushButton("UPDATE GRAPH")
        self.btn_update.clicked.connect(self.UpdateGraph)
        layout_graph.addWidget(self.btn_update)

        main_layer.addWidget(grp_graph)


        self.window_1 = pyGraph.PlotWidget()
        pyGraph.setConfigOptions(antialias=True)
        main_layer.addWidget(self.window_1)

        ## create mainAxis
        self.mainAxis = self.window_1.plotItem
        self.mainAxis.setLabels(left=self.title)
        self.mainAxis.showAxis('right')

        # logging.info("GRAPHT :: ", self.data['mod5ch1'])
        min_val = min(len(self.data['mod1ch1']), len(self.data['mod2ch1']), 
                      len(self.data['mod3ch1']), len(self.data['mod4ch1']),
                      len(self.data['mod5ch1']), len(self.data['mod6ch1']),
                      len(self.data['mod7ch1']), len(self.data['mod8ch1']))

        indexs = list(range(min_val))
        mod1ch1 = self.data['mod1ch1'][:min_val]
        mod2ch1 = self.data['mod2ch1'][:min_val]
        mod3ch1 = self.data['mod3ch1'][:min_val]
        mod4ch1 = self.data['mod4ch1'][:min_val]
        mod5ch1 = self.data['mod5ch1'][:min_val]
        mod6ch1 = self.data['mod6ch1'][:min_val]
        mod7ch1 = self.data['mod7ch1'][:min_val]
        mod8ch1 = self.data['mod8ch1'][:min_val]

        self.graph_mod1ch1 =  self.mainAxis.plot(indexs, mod1ch1, pen=self.pen_mod1, name="mod1ch1")
        self.graph_mod2ch1 =  self.mainAxis.plot(indexs, mod2ch1, pen=self.pen_mod2, name="mod2ch1")
        self.graph_mod3ch1 =  self.mainAxis.plot(indexs, mod3ch1, pen=self.pen_mod3, name="mod3ch1")
        self.graph_mod4ch1 =  self.mainAxis.plot(indexs, mod4ch1, pen=self.pen_mod4, name="mod4ch1")
        self.graph_mod5ch1 =  self.mainAxis.plot(indexs, mod5ch1, pen=self.pen_mod5, name="mod5ch1")
        self.graph_mod6ch1 =  self.mainAxis.plot(indexs, mod6ch1, pen=self.pen_mod6, name="mod6ch1")
        self.graph_mod7ch1 =  self.mainAxis.plot(indexs, mod7ch1, pen=self.pen_mod7, name="mod7ch1")
        self.graph_mod8ch1 =  self.mainAxis.plot(indexs, mod8ch1, pen=self.pen_mod8, name="mod8ch1")

        try:
            if self.data['mod1ch2']:
                self.window_2 = pyGraph.PlotWidget()
                pyGraph.setConfigOptions(antialias=True)
                main_layer.addWidget(self.window_2)

                ## create mainAxis
                self.mainAxis = self.window_2.plotItem
                self.mainAxis.setLabels(left=self.title)
                self.mainAxis.showAxis('right')

                min_val = min(len(self.data['mod1ch2']), len(self.data['mod2ch2']), 
                              len(self.data['mod3ch2']), len(self.data['mod4ch2']),
                              len(self.data['mod5ch2']), len(self.data['mod6ch2']),
                              len(self.data['mod7ch2']), len(self.data['mod8ch2']))

                indexs = list(range(min_val))
                mod1ch2 = self.data['mod1ch2'][:min_val]
                mod2ch2 = self.data['mod2ch2'][:min_val]
                mod3ch2 = self.data['mod3ch2'][:min_val]
                mod4ch2 = self.data['mod4ch2'][:min_val]
                mod5ch2 = self.data['mod5ch2'][:min_val]
                mod6ch2 = self.data['mod6ch2'][:min_val]
                mod7ch2 = self.data['mod7ch2'][:min_val]
                mod8ch2 = self.data['mod8ch2'][:min_val]

                self.graph_mod1ch2 =  self.mainAxis.plot(indexs, mod1ch2, pen=self.pen_mod1, name="mod1ch2")
                self.graph_mod2ch2 =  self.mainAxis.plot(indexs, mod2ch2, pen=self.pen_mod2, name="mod2ch2")
                self.graph_mod3ch2 =  self.mainAxis.plot(indexs, mod3ch2, pen=self.pen_mod3, name="mod3ch2")
                self.graph_mod4ch2 =  self.mainAxis.plot(indexs, mod4ch2, pen=self.pen_mod4, name="mod4ch2")
                self.graph_mod5ch2 =  self.mainAxis.plot(indexs, mod5ch2, pen=self.pen_mod5, name="mod5ch2")
                self.graph_mod6ch2 =  self.mainAxis.plot(indexs, mod6ch2, pen=self.pen_mod6, name="mod6ch2")
                self.graph_mod7ch2 =  self.mainAxis.plot(indexs, mod7ch2, pen=self.pen_mod7, name="mod7ch2")
                self.graph_mod8ch2 =  self.mainAxis.plot(indexs, mod8ch2, pen=self.pen_mod8, name="mod8ch2")

        except Exception as e:
            logging.warning(e)

    def UpdateGraph(self):
        # Mod #1
        if self.chbox_mod1.isChecked() == True:
            self.view_mod1 = True
            self.graph_mod1ch1.show()
            self.graph_mod1ch2.show()
        else:
            self.view_mod1 = False
            self.graph_mod1ch1.hide()
            self.graph_mod1ch2.hide()
        ## Mod #2
        if self.chbox_mod2.isChecked() == True:
            self.view_mod2 = True
            self.graph_mod2ch1.show()
            self.graph_mod2ch2.show()
        else:
            self.view_mod2 = False
            self.graph_mod2ch1.hide()
            self.graph_mod2ch2.hide()
        ## Mod #3
        if self.chbox_mod3.isChecked() == True:
            self.view_mod3 = True
            self.graph_mod3ch1.show()
            self.graph_mod3ch2.show()
        else:
            self.view_mod3 = False
            self.graph_mod3ch1.hide()
            self.graph_mod3ch2.hide()
        ## Mod #4
        if self.chbox_mod4.isChecked() == True:
            self.view_mod4 = True
            self.graph_mod4ch1.show()
            self.graph_mod4ch2.show()
        else:
            self.view_mod4 = False
            self.graph_mod4ch1.hide()
            self.graph_mod4ch2.hide()
        ## Mod #5
        if self.chbox_mod5.isChecked() == True:
            self.view_mod5 = True
            self.graph_mod5ch1.show()
            self.graph_mod5ch2.show()
        else:
            self.view_mod5 = False
            self.graph_mod5ch1.hide()
            self.graph_mod5ch2.hide()
        ## Mod #6
        if self.chbox_mod6.isChecked() == True:
            self.view_mod6 = True
            self.graph_mod6ch1.show()
            self.graph_mod6ch2.show()
        else:
            self.view_mod6 = False
            self.graph_mod6ch1.hide()
            self.graph_mod6ch2.hide()
        ## Mod #7
        if self.chbox_mod7.isChecked() == True:
            self.view_mod7 = True
            self.graph_mod7ch1.show()
            self.graph_mod7ch2.show()
        else:
            self.view_mod7 = False
            self.graph_mod7ch1.hide()
            self.graph_mod7ch2.hide()
        ## Mod #8
        if self.chbox_mod8.isChecked() == True:
            self.view_mod8 = True
            self.graph_mod8ch1.show()
            self.graph_mod8ch2.show()
        else:
            self.view_mod8 = False
            self.graph_mod8ch1.hide()
            self.graph_mod8ch2.hide()
        
        


    def on_accepted(self):
        self.accept()


def main():
    app = QApplication(sys.argv)
    Dialog = QDialog()
    main = InspectWindow(Dialog, 1, 1)
    main.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
