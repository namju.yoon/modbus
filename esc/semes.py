import re
import serial
import logging
from binascii import hexlify

def scpi_write(ptype, val=0):
    logging.info(f"scpi_write :: {ptype} :: {val}")
    if ptype == 'VOLTAGE':
        command = f'SV{val}.000000\r'
    elif ptype == 'CURRENT':
        pval = int(val*1000)
        command = f'SI{pval}.000000\r'
    elif ptype == 'RAMPUP':
        command = f'P+{val}\r'
    elif ptype == 'RAMPDOWN':
        command = f'P-{val}\r'
    elif ptype == 'TOGGLEON':
        command = 'D+\r'
    elif ptype == 'TOGGLEOFF':
        command = 'D-\r'
    elif ptype == 'REVERSEON':
        command = 'V+\r'
    elif ptype == 'REVERSEOFF':
        command = 'V-\r'
    elif ptype == 'RUN':
        command = 'EV\r'
    elif ptype == 'STOP':
        command = 'DV\r'
    return command.encode()


def scpi_measure(ptype):
    if ptype == 'VOLTAGE':
        command = 'RV\r'
    elif ptype == 'CURRENT+':
        command = 'R+\r'
    elif ptype == 'CURRENT-':
        command = 'R-\r'
    logging.info(f"scpi_measure :: {ptype} :: {command.encode()}")
    return command.encode()


def scpi_get_measure(client, ptype):
    if ptype == 'VOLTAGE':
        command = 'RV\r'
    elif ptype == 'CURRENT+':
        command = 'R+\r'
    elif ptype == 'CURRENT-':
        command = 'R-\r'
    else:
        command = 'RV\r'
    client.write(command.encode())
    while True:
        try:
            feedback = client.readline()
            if feedback:
                logging.info(f"scpi_get_measure :: {ptype} :: {feedback}")
                bitestr = feedback.decode('utf-8')
                try:
                    return int(re.findall('[-0-9]+', str(bitestr))[0])
                except Exception as e:
                    return 0
                break
        except KeyboardInterrupt:
            feedback = None
            break
    return 0


