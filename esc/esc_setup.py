import sys
import serial
import logging
from PyQt5.QtWidgets import QWidget, QLabel, QLineEdit, QPushButton, QDesktopWidget
from PyQt5.QtWidgets import QGroupBox, QBoxLayout, QVBoxLayout, QHBoxLayout, QDialogButtonBox
from PyQt5.QtWidgets import QApplication, QDialog, QStatusBar, QFileDialog, QRadioButton
from PyQt5 import QtCore
from PyQt5.QtCore import QDate, Qt
from PyQt5.QtGui import QPixmap, QFont


GRAPH = 1
DATA = 2

def check_stopbit(x): 
    return {'7 Bit': 7, '8 Bit': 8, '1 Bit': 1, '2 Bit': 2, '9600': 9600, '19200': 19200, '38400': 38400, '57600':57600, '115200': 115200, 'NONE': serial.PARITY_NONE, 'ODD': serial.PARITY_ODD, 'EVEN': serial.PARITY_EVEN, 'MARK': serial.PARITY_MARK, 'SPACE': serial.PARITY_SPACE,}[x]


def get_comm_port():
    port_list = []
    import serial.tools.list_ports
    ports = serial.tools.list_ports.comports()
    for port, desc, hwid in sorted(ports):
        if desc != 'n/a':
            port_list.append(port)
    
    return port_list

get_comm_port()

class SettingWin(QDialog):
    def __init__(self, Dialog, used_ports):
        super().__init__()
        self.used_ports = used_ports
        print(" ## :: Used Ports :: ", self.used_ports)
        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        self.setGeometry(100, 100, 400, 500)
        font = QFont()
        font.setPointSize(12)

        self.channelnum = False
        self.ndp_flag = False
        self.selected = None
        self.ipaddress = None 
        self.ipport = None 

        self.gen_port = None
        self.com_speed = 115200
        self.com_data = 8
        self.com_parity = serial.PARITY_NONE
        self.com_stop = 1
        self.graph_type = None

        # Main 
        main_layer = QVBoxLayout()
        self.setLayout(main_layer)

        ## 1Ch, 2Ch 선택 
        self.grp_channel = QGroupBox("1Ch, 2Ch 선택")
        layout_channel = QHBoxLayout()

        self.rd_ch_num1 = QRadioButton("Ch #1")
        self.rd_ch_num1.clicked.connect(lambda:self.radioSelectClicked(self.rd_ch_num1))
        self.rd_ch_num1.setChecked(True)
        layout_channel.addWidget(self.rd_ch_num1)
        self.rd_ch_num2 = QRadioButton("Ch #2")
        self.rd_ch_num2.clicked.connect(lambda:self.radioSelectClicked(self.rd_ch_num2))
        layout_channel.addWidget(self.rd_ch_num2)
        self.grp_channel.setLayout(layout_channel)
        main_layer.addWidget(self.grp_channel)

        ## 선택 NDP ON/OFF 선택 
        self.grp_ndp = QGroupBox("NDP, PDF ON/OFF 선택")
        layout_ndp = QHBoxLayout()

        self.rd_ndpon = QRadioButton("NDP/PDP 사용")
        self.rd_ndpon.clicked.connect(lambda:self.ndpSelectClicked(self.rd_ndpon))
        layout_ndp.addWidget(self.rd_ndpon)
        self.rd_ndpoff = QRadioButton("NDP/PDP 없음")
        self.rd_ndpoff.clicked.connect(lambda:self.ndpSelectClicked(self.rd_ndpoff))
        self.rd_ndpoff.setChecked(True)
        
        layout_ndp.addWidget(self.rd_ndpoff)
        self.grp_ndp.setLayout(layout_ndp)
        main_layer.addWidget(self.grp_ndp)
        self.grp_ndp.hide()

        # 선택 TCP or RTU
        self.grp_select = QGroupBox("RTU or SCPI or TCP 선택")
        layout_select = QHBoxLayout()

        self.rd_rtu = QRadioButton("RTU")
        self.rd_rtu.setChecked(True)
        self.selected = 'RTU'
        self.rd_rtu.clicked.connect(lambda:self.radioSelectClicked(self.rd_rtu))
        layout_select.addWidget(self.rd_rtu)

        self.rd_scpi = QRadioButton("SCPI")
        self.rd_scpi.clicked.connect(lambda:self.radioSelectClicked(self.rd_scpi))
        layout_select.addWidget(self.rd_scpi)

        self.rd_semes = QRadioButton("SEMES")
        self.rd_semes.clicked.connect(lambda:self.radioSelectClicked(self.rd_semes))
        layout_select.addWidget(self.rd_semes)

        self.rd_tcp = QRadioButton("TCP")
        self.rd_tcp.clicked.connect(lambda:self.radioSelectClicked(self.rd_tcp))
        layout_select.addWidget(self.rd_tcp)

        self.grp_select.setLayout(layout_select)
        main_layer.addWidget(self.grp_select)

        # IP Address 
        self.grp_ipaddress = QGroupBox("IP ADDRESS")
        layout_ipaddress = QHBoxLayout()
        self.edit_ipaddress = QLineEdit("192.168.10.1")
        layout_ipaddress.addWidget(self.edit_ipaddress)
        self.grp_ipaddress.setLayout(layout_ipaddress)
        main_layer.addWidget(self.grp_ipaddress)
        self.grp_ipaddress.hide()

        # IP Port 
        self.grp_ipport = QGroupBox("Port")
        layout_ipport = QHBoxLayout()
        self.edit_ipport = QLineEdit("5000")
        layout_ipport.addWidget(self.edit_ipport)
        self.grp_ipport.setLayout(layout_ipport)
        main_layer.addWidget(self.grp_ipport)
        self.grp_ipport.hide()


        # 통신 포트  설정(Serial Port)"
        self.grp_port = QGroupBox("통신 포트  설정(Serial Port)")
        layout_port = QHBoxLayout()

        ports = get_comm_port()
        poss_ports = []
        for port in ports:
            if port not in self.used_ports:
                poss_ports.append(port)

        if poss_ports:
            if len(poss_ports) == 1:
                port = poss_ports[0]
                self.rd_port_1 = QRadioButton(port)
                self.rd_port_1.setChecked(True)
                self.gen_port = port
                self.rd_port_1.clicked.connect(lambda:self.radioPortClicked(self.rd_port_1))
                layout_port.addWidget(self.rd_port_1)
            elif len(poss_ports) == 2:
                port = poss_ports[0]
                self.rd_port_1 = QRadioButton(port)
                self.rd_port_1.setChecked(True)
                self.gen_port = port
                self.rd_port_1.clicked.connect(lambda:self.radioPortClicked(self.rd_port_1))
                layout_port.addWidget(self.rd_port_1)

                port = poss_ports[1]
                self.rd_port_2 = QRadioButton(port)
                self.rd_port_2.clicked.connect(lambda:self.radioPortClicked(self.rd_port_2))
                layout_port.addWidget(self.rd_port_2)

            elif len(poss_ports) == 3:
                port = poss_ports[0]
                self.rd_port_1 = QRadioButton(port)
                self.rd_port_1.setChecked(True)
                self.gen_port = port
                self.rd_port_1.clicked.connect(lambda:self.radioPortClicked(self.rd_port_1))
                layout_port.addWidget(self.rd_port_1)
                port = poss_ports[1]
                self.rd_port_2 = QRadioButton(port)
                self.rd_port_2.clicked.connect(lambda:self.radioPortClicked(self.rd_port_2))
                layout_port.addWidget(self.rd_port_2)
                port = poss_ports[2]
                self.rd_port_3 = QRadioButton(port)
                self.rd_port_3.clicked.connect(lambda:self.radioPortClicked(self.rd_port_3))
                layout_port.addWidget(self.rd_port_3)
            else:
                port = poss_ports[0]
                self.rd_port_1 = QRadioButton(port)
                self.rd_port_1.setChecked(True)
                self.gen_port = port
                self.rd_port_1.clicked.connect(lambda:self.radioPortClicked(self.rd_port_1))
                layout_port.addWidget(self.rd_port_1)
                port = poss_ports[1]
                self.rd_port_2 = QRadioButton(port)
                self.rd_port_2.clicked.connect(lambda:self.radioPortClicked(self.rd_port_2))
                layout_port.addWidget(self.rd_port_2)
                port = poss_ports[2]
                self.rd_port_3 = QRadioButton(port)
                self.rd_port_3.clicked.connect(lambda:self.radioPortClicked(self.rd_port_3))
                layout_port.addWidget(self.rd_port_3)
                print("Too many")
        else:
            self.label_result = QLabel("현재 사용 가능한 포트가 없습니다. 연결 선을 USB에 연결하세요.")
            layout_port.addWidget(self.label_result)

        self.grp_port.setLayout(layout_port)
        main_layer.addWidget(self.grp_port)
        # self.grp_port.hide()

        # 전송속도 (Baud Rate)"
        self.grp_speed = QGroupBox("전송속도 (Baud Rate)")
        self.grp_speed.setFont(font)
        layout_speed = QHBoxLayout()

        self.rd_speed_96 = QRadioButton("9600")
        self.rd_speed_96.clicked.connect(lambda:self.radioValueClicked(self.rd_speed_96, "SPEED"))
        layout_speed.addWidget(self.rd_speed_96)

        self.rd_speed_384 = QRadioButton("38400")
        self.rd_speed_384.clicked.connect(lambda:self.radioValueClicked(self.rd_speed_384, "SPEED"))
        layout_speed.addWidget(self.rd_speed_384)

        self.rd_speed_576 = QRadioButton("57600")
        self.rd_speed_576.clicked.connect(lambda:self.radioValueClicked(self.rd_speed_576, "SPEED"))
        layout_speed.addWidget(self.rd_speed_576)

        self.rd_speed_1152 = QRadioButton("115200")
        self.com_speed = 115200
        self.rd_speed_1152.clicked.connect(lambda:self.radioValueClicked(self.rd_speed_1152, "SPEED"))
        layout_speed.addWidget(self.rd_speed_1152)
        self.rd_speed_1152.setChecked(True)

        self.grp_speed.setLayout(layout_speed)
        main_layer.addWidget(self.grp_speed)
        # self.grp_speed.hide()

        # SELECT GRAPH
        grp_graph = QGroupBox("그래프 타입")
        grp_graph.setFont(font)

        layout_graph = QHBoxLayout()

        self.rd_graph_with = QRadioButton("WITH GRAPH")
        self.graph_type = GRAPH
        self.rd_graph_with.clicked.connect(lambda:self.radioSelectGraph(GRAPH))
        self.rd_graph_with.setChecked(True)
        layout_graph.addWidget(self.rd_graph_with)

        self.rd_graph_data = QRadioButton("DATA COUNT IMPORTANT")
        self.rd_graph_data.clicked.connect(lambda:self.radioSelectGraph(DATA))
        layout_graph.addWidget(self.rd_graph_data)

        grp_graph.setLayout(layout_graph)
        main_layer.addWidget(grp_graph)


        self.buttonBox = QDialogButtonBox()
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        main_layer.addWidget(self.buttonBox)

        self.buttonBox.accepted.connect(self.on_accepted)
        self.buttonBox.rejected.connect(self.reject)


    def on_accepted(self):
        if self.selected == 'RTU':
            if self.gen_port and self.com_speed and self.com_data and self.com_parity and self.com_stop:
                logging.info("All data meeted")
                self.accept()
            else:
                logging.info("ComPort/ Speed 를 모두 선택하시요.")
        else:
            self.ipaddress = self.edit_ipaddress.text()
            self.ipport = self.edit_ipport.text()
            if self.ipaddress and self.ipport:
                logging.info("All data meeted")
                self.accept()
            else:
                logging.info("IP Address and Port 를 모두 선택하시요.")


    def radioSelectClicked(self, btn):
        result = btn.text()
        if result == 'Ch #1':
            self.grp_ndp.hide()
            self.channelnum = False
        
        elif result == 'Ch #2':
            self.grp_ndp.show()
            self.channelnum = True
        
        elif result == 'RTU':
            self.selected = 'RTU'
            self.grp_ipaddress.hide()
            self.grp_ipport.hide()
            
            self.grp_port.show()
            self.grp_speed.show()

        elif result == 'SEMES':
            self.selected = 'SEMES'
            self.grp_ipaddress.hide()
            self.grp_ipport.hide()
            
            self.grp_port.show()
            self.grp_speed.show()

        elif result == 'SCPI':
            self.selected = 'SCPI'
            self.grp_ipaddress.hide()
            self.grp_ipport.hide()
            
            self.grp_port.show()
            self.grp_speed.show()

        elif result == 'TCP':
            self.selected = 'TCP'
            self.grp_port.hide()
            self.grp_speed.hide()

            self.grp_ipaddress.show()
            self.grp_ipport.show()


    def ndpSelectClicked(self, btn):
        result = btn.text()
        if result == 'NDP/PDP 사용':
            self.ndp_flag = True
        else:
            self.ndp_flag = False

        
    def radioPortClicked(self, btn):
        result = btn.text()
        self.gen_port = result


    def radioValueClicked(self, btn, ptype):
        result = check_stopbit(btn.text())
        if ptype == "SPEED":
            self.com_speed = int(result)
            print("SPEED :: ", self.com_speed)
        elif ptype == "DATA":
            self.com_data = result
        elif ptype == "PARITY":
            self.com_parity = result
        elif ptype == "STOP":
            self.com_stop = result


    def radioSelectGraph(self, gtype):
        self.graph_type = gtype

class SettingWinNOPort(QDialog):
    def __init__(self, Dialog):
        super().__init__()
        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        self.setGeometry(100, 100, 400, 500)
        font = QFont()
        font.setPointSize(12)

        self.channelnum = False
        self.ndp_flag = False
        self.selected = None
        self.ipaddress = None 
        self.ipport = None 

        self.gen_port = None
        self.com_speed = 115200
        self.com_data = 8
        self.com_parity = serial.PARITY_NONE
        self.com_stop = 1
        self.graph_type = None

        # Main 
        main_layer = QVBoxLayout()
        self.setLayout(main_layer)

        ## 1Ch, 2Ch 선택 
        self.grp_channel = QGroupBox("1Ch, 2Ch 선택")
        layout_channel = QHBoxLayout()

        self.rd_ch_num1 = QRadioButton("Ch #1")
        self.rd_ch_num1.clicked.connect(lambda:self.radioSelectClicked(self.rd_ch_num1))
        self.rd_ch_num1.setChecked(True)
        layout_channel.addWidget(self.rd_ch_num1)
        self.rd_ch_num2 = QRadioButton("Ch #2")
        self.rd_ch_num2.clicked.connect(lambda:self.radioSelectClicked(self.rd_ch_num2))
        layout_channel.addWidget(self.rd_ch_num2)
        self.grp_channel.setLayout(layout_channel)
        main_layer.addWidget(self.grp_channel)

        ## 선택 NDP ON/OFF 선택 
        self.grp_ndp = QGroupBox("NDP, PDF ON/OFF 선택")
        layout_ndp = QHBoxLayout()

        self.rd_ndpon = QRadioButton("NDP/PDP 사용")
        self.rd_ndpon.clicked.connect(lambda:self.ndpSelectClicked(self.rd_ndpon))
        layout_ndp.addWidget(self.rd_ndpon)
        self.rd_ndpoff = QRadioButton("NDP/PDP 없음")
        self.rd_ndpoff.clicked.connect(lambda:self.ndpSelectClicked(self.rd_ndpoff))
        self.rd_ndpoff.setChecked(True)
        
        layout_ndp.addWidget(self.rd_ndpoff)
        self.grp_ndp.setLayout(layout_ndp)
        main_layer.addWidget(self.grp_ndp)
        self.grp_ndp.hide()

        # 선택 TCP or RTU
        self.grp_select = QGroupBox("RTU or SCPI or TCP 선택")
        layout_select = QHBoxLayout()

        self.rd_rtu = QRadioButton("RTU")
        self.rd_rtu.setChecked(True)
        self.selected = 'RTU'
        self.rd_rtu.clicked.connect(lambda:self.radioSelectClicked(self.rd_rtu))
        layout_select.addWidget(self.rd_rtu)

        self.rd_scpi = QRadioButton("SCPI")
        self.rd_scpi.clicked.connect(lambda:self.radioSelectClicked(self.rd_scpi))
        layout_select.addWidget(self.rd_scpi)

        self.rd_semes = QRadioButton("SEMES")
        self.rd_semes.clicked.connect(lambda:self.radioSelectClicked(self.rd_semes))
        layout_select.addWidget(self.rd_semes)

        self.rd_tcp = QRadioButton("TCP")
        self.rd_tcp.clicked.connect(lambda:self.radioSelectClicked(self.rd_tcp))
        layout_select.addWidget(self.rd_tcp)

        self.grp_select.setLayout(layout_select)
        main_layer.addWidget(self.grp_select)

        # IP Address 
        self.grp_ipaddress = QGroupBox("IP ADDRESS")
        layout_ipaddress = QHBoxLayout()
        self.edit_ipaddress = QLineEdit("192.168.10.1")
        layout_ipaddress.addWidget(self.edit_ipaddress)
        self.grp_ipaddress.setLayout(layout_ipaddress)
        main_layer.addWidget(self.grp_ipaddress)
        self.grp_ipaddress.hide()

        # IP Port 
        self.grp_ipport = QGroupBox("Port")
        layout_ipport = QHBoxLayout()
        self.edit_ipport = QLineEdit("5000")
        layout_ipport.addWidget(self.edit_ipport)
        self.grp_ipport.setLayout(layout_ipport)
        main_layer.addWidget(self.grp_ipport)
        self.grp_ipport.hide()


        # 통신 포트  설정(Serial Port)"
        self.grp_port = QGroupBox("통신 포트  설정(Serial Port)")
        layout_port = QHBoxLayout()

        poss_ports = get_comm_port()

        if poss_ports:
            if len(poss_ports) == 1:
                port = poss_ports[0]
                self.rd_port_1 = QRadioButton(port)
                self.rd_port_1.setChecked(True)
                self.gen_port = port
                self.rd_port_1.clicked.connect(lambda:self.radioPortClicked(self.rd_port_1))
                layout_port.addWidget(self.rd_port_1)
            elif len(poss_ports) == 2:
                port = poss_ports[0]
                self.rd_port_1 = QRadioButton(port)
                self.rd_port_1.setChecked(True)
                self.gen_port = port
                self.rd_port_1.clicked.connect(lambda:self.radioPortClicked(self.rd_port_1))
                layout_port.addWidget(self.rd_port_1)

                port = poss_ports[1]
                self.rd_port_2 = QRadioButton(port)
                self.rd_port_2.clicked.connect(lambda:self.radioPortClicked(self.rd_port_2))
                layout_port.addWidget(self.rd_port_2)

            elif len(poss_ports) == 3:
                port = poss_ports[0]
                self.rd_port_1 = QRadioButton(port)
                self.rd_port_1.setChecked(True)
                self.gen_port = port
                self.rd_port_1.clicked.connect(lambda:self.radioPortClicked(self.rd_port_1))
                layout_port.addWidget(self.rd_port_1)
                port = poss_ports[1]
                self.rd_port_2 = QRadioButton(port)
                self.rd_port_2.clicked.connect(lambda:self.radioPortClicked(self.rd_port_2))
                layout_port.addWidget(self.rd_port_2)
                port = poss_ports[2]
                self.rd_port_3 = QRadioButton(port)
                self.rd_port_3.clicked.connect(lambda:self.radioPortClicked(self.rd_port_3))
                layout_port.addWidget(self.rd_port_3)
            else:
                port = poss_ports[0]
                self.rd_port_1 = QRadioButton(port)
                self.rd_port_1.setChecked(True)
                self.gen_port = port
                self.rd_port_1.clicked.connect(lambda:self.radioPortClicked(self.rd_port_1))
                layout_port.addWidget(self.rd_port_1)
                port = poss_ports[1]
                self.rd_port_2 = QRadioButton(port)
                self.rd_port_2.clicked.connect(lambda:self.radioPortClicked(self.rd_port_2))
                layout_port.addWidget(self.rd_port_2)
                port = poss_ports[2]
                self.rd_port_3 = QRadioButton(port)
                self.rd_port_3.clicked.connect(lambda:self.radioPortClicked(self.rd_port_3))
                layout_port.addWidget(self.rd_port_3)
                print("Too many")
        else:
            self.label_result = QLabel("현재 사용 가능한 포트가 없습니다. 연결 선을 USB에 연결하세요.")
            layout_port.addWidget(self.label_result)

        self.grp_port.setLayout(layout_port)
        main_layer.addWidget(self.grp_port)
        # self.grp_port.hide()

        # 전송속도 (Baud Rate)"
        self.grp_speed = QGroupBox("전송속도 (Baud Rate)")
        self.grp_speed.setFont(font)
        layout_speed = QHBoxLayout()

        self.rd_speed_96 = QRadioButton("9600")
        self.rd_speed_96.clicked.connect(lambda:self.radioValueClicked(self.rd_speed_96, "SPEED"))
        layout_speed.addWidget(self.rd_speed_96)

        self.rd_speed_384 = QRadioButton("38400")
        self.rd_speed_384.clicked.connect(lambda:self.radioValueClicked(self.rd_speed_384, "SPEED"))
        layout_speed.addWidget(self.rd_speed_384)

        self.rd_speed_576 = QRadioButton("57600")
        self.rd_speed_576.clicked.connect(lambda:self.radioValueClicked(self.rd_speed_576, "SPEED"))
        layout_speed.addWidget(self.rd_speed_576)

        self.rd_speed_1152 = QRadioButton("115200")
        self.com_speed = 115200
        self.rd_speed_1152.clicked.connect(lambda:self.radioValueClicked(self.rd_speed_1152, "SPEED"))
        layout_speed.addWidget(self.rd_speed_1152)
        self.rd_speed_1152.setChecked(True)

        self.grp_speed.setLayout(layout_speed)
        main_layer.addWidget(self.grp_speed)
        # self.grp_speed.hide()

        # SELECT GRAPH
        grp_graph = QGroupBox("그래프 타입")
        grp_graph.setFont(font)

        layout_graph = QHBoxLayout()

        self.rd_graph_with = QRadioButton("WITH GRAPH")
        self.graph_type = GRAPH
        self.rd_graph_with.clicked.connect(lambda:self.radioSelectGraph(GRAPH))
        self.rd_graph_with.setChecked(True)
        layout_graph.addWidget(self.rd_graph_with)

        self.rd_graph_data = QRadioButton("DATA COUNT IMPORTANT")
        self.rd_graph_data.clicked.connect(lambda:self.radioSelectGraph(DATA))
        layout_graph.addWidget(self.rd_graph_data)

        grp_graph.setLayout(layout_graph)
        main_layer.addWidget(grp_graph)


        self.buttonBox = QDialogButtonBox()
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        main_layer.addWidget(self.buttonBox)

        self.buttonBox.accepted.connect(self.on_accepted)
        self.buttonBox.rejected.connect(self.reject)


    def on_accepted(self):
        if self.selected == 'RTU':
            if self.gen_port and self.com_speed and self.com_data and self.com_parity and self.com_stop:
                logging.info("All data meeted")
                self.accept()
            else:
                logging.info("ComPort/ Speed 를 모두 선택하시요.")
        else:
            self.ipaddress = self.edit_ipaddress.text()
            self.ipport = self.edit_ipport.text()
            if self.ipaddress and self.ipport:
                logging.info("All data meeted")
                self.accept()
            else:
                logging.info("IP Address and Port 를 모두 선택하시요.")


    def radioSelectClicked(self, btn):
        result = btn.text()
        if result == 'Ch #1':
            self.grp_ndp.hide()
            self.channelnum = False
        
        elif result == 'Ch #2':
            self.grp_ndp.show()
            self.channelnum = True
        
        elif result == 'RTU':
            self.selected = 'RTU'
            self.grp_ipaddress.hide()
            self.grp_ipport.hide()
            
            self.grp_port.show()
            self.grp_speed.show()

        elif result == 'SEMES':
            self.selected = 'SEMES'
            self.grp_ipaddress.hide()
            self.grp_ipport.hide()
            
            self.grp_port.show()
            self.grp_speed.show()

        elif result == 'SCPI':
            self.selected = 'SCPI'
            self.grp_ipaddress.hide()
            self.grp_ipport.hide()
            
            self.grp_port.show()
            self.grp_speed.show()

        elif result == 'TCP':
            self.selected = 'TCP'
            self.grp_port.hide()
            self.grp_speed.hide()

            self.grp_ipaddress.show()
            self.grp_ipport.show()


    def ndpSelectClicked(self, btn):
        result = btn.text()
        if result == 'NDP/PDP 사용':
            self.ndp_flag = True
        else:
            self.ndp_flag = False

        
    def radioPortClicked(self, btn):
        result = btn.text()
        self.gen_port = result


    def radioValueClicked(self, btn, ptype):
        result = check_stopbit(btn.text())
        if ptype == "SPEED":
            self.com_speed = int(result)
            print("SPEED :: ", self.com_speed)
        elif ptype == "DATA":
            self.com_data = result
        elif ptype == "PARITY":
            self.com_parity = result
        elif ptype == "STOP":
            self.com_stop = result


    def radioSelectGraph(self, gtype):
        self.graph_type = gtype



if __name__ == "__main__":
    app = QApplication(sys.argv)
    Dialog = QDialog()
    mywindow = SettingWin(Dialog)
    mywindow.show()
    app.exec_()
