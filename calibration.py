#!/usr/bin/env python
# coding: utf-8

# 예제 내용
# * 기본 위젯을 사용하여 기본 창을 생성
# * 다양한 레이아웃 위젯 사용

import sys
from random import randint
from PyQt5.QtWidgets import QWidget, QFrame, QLabel, QLineEdit, QTextEdit, QPushButton
from PyQt5.QtWidgets import QGroupBox, QBoxLayout, QVBoxLayout, QHBoxLayout, QGridLayout
from PyQt5.QtWidgets import QApplication, QFormLayout, QDialog, QStatusBar
from PyQt5 import QtCore
from PyQt5.QtCore import QDate, Qt
from PyQt5.QtGui import QPixmap
from itertools import count

from setup_comm import SettingWin
from setup_qr import SettingQR
from login import LoginWin
from draw import GraphWindow

import pyqtgraph as pg
import pyqtgraph.exporters
import serial_client as device

import numpy as np
import serial
import time

# import logging
# FORMAT = ('%(asctime)-15s %(threadName)-15s '
#           '%(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
# logging.basicConfig(format=FORMAT)
# log = logging.getLogger()
# log.setLevel(logging.DEBUG)

class MainWindow(QWidget):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.initUI()

    def initUI(self):

        self.setWindowTitle("PSTEK 융착기 전원 장치 테스트용")
        self.setMinimumSize(1240, 800) 
        # now = QDate.currentDate()
        # self.status = 'OFF'
        # self.message = "PSTEK 융착기 전원 장치, 상태 : {}             {}".format(self.status, now.toString(Qt.ISODate))
        # self.statusBar().showMessage(self.message)
        self.timer = QtCore.QTimer()

        # self.freq = None

        self.com_setting_flag = False
        self.com_open_flag = False

        self.client = None
        self.qr_client = None
        self.gen_port = None
        self.qr_port = None
        self.com_speed = None
        self.com_data = None
        self.com_parity = None
        self.com_stop = None

        self.product_serial = None

        self.stopFlag = True
        self.graph_flag = False
        self.index = np.array([])
        sself.Powers = np.array([])
        self.DcVoltages = np.array([])
        self.DcCurrents = np.array([])
        sself.Currents = np.array([])
        sself.Voltages = np.array([])
        self.Impedences = np.array([])
        self.Qvalues = np.array([])
        self.impedence = 0
        self.Phases = np.array([])
        self.Frequencys = np.array([])

        self.btn_comm = QPushButton("통신 설정과 연결(RTU)")
        self.btn_start = QPushButton("단말기 Start")
        self.btn_start.clicked.connect(self.start_device)

        self.btn_reset = QPushButton("단말기 Reset")
        self.btn_status = QPushButton("단말 상태 정보")
        self.btn_warning = QPushButton("단말 경고 정보")
        self.btn_fail = QPushButton("단말 오류 정보")

    ## Main Layout 설정
        main_layer = QVBoxLayout()
        self.setLayout(main_layer)

    ## replace #1st ###############
    # 전원 장치 연결
        layout_setup = QHBoxLayout()
        grp_generator = QGroupBox("전원 장치 연결(485/ModBus)")
        layout_generator = QHBoxLayout()
        self.btn_com_gen = QPushButton("전원 장치 연결 포트 선택")
        self.label_gen_port = QLabel("N/A")
        self.btn_com_gen.clicked.connect(self.setting_generator)
        layout_generator.addWidget(self.btn_com_gen)
        layout_generator.addWidget(self.label_gen_port)
        grp_generator.setLayout(layout_generator)
        layout_setup.addWidget(grp_generator)

        # QR 연결 
        grp_qrscanner = QGroupBox("QR 스캐너 연결(232통신)")
        layout_qrscanner = QHBoxLayout()
        self.btn_com_qr = QPushButton("QR 연결 포트 선택")
        self.label_qr_port = QLabel("N/A")
        self.btn_com_qr.clicked.connect(self.setting_qr)
        layout_qrscanner.addWidget(self.btn_com_qr)
        layout_qrscanner.addWidget(self.label_qr_port)
        grp_qrscanner.setLayout(layout_qrscanner)
        layout_setup.addWidget(grp_qrscanner)

        # Login 사용자 정보 보여 주기
        grp_user_info = QGroupBox("사용자 정보")
        layout_user_info = QHBoxLayout()
        self.btn_user_info = QPushButton("Login")
        self.label_user_info = QLabel("N/A")
        self.btn_user_info.clicked.connect(self.login_info)
        layout_user_info.addWidget(self.btn_user_info)
        layout_user_info.addWidget(self.label_user_info)
        grp_user_info.setLayout(layout_user_info)
        layout_setup.addWidget(grp_user_info)

        # Logo Image
        labelLogo = QLabel("")
        pixmap = QPixmap("img/logo.png")
        labelLogo.setAlignment(Qt.AlignRight)
        labelLogo.setPixmap(pixmap)
        layout_setup.addWidget(labelLogo)        

        main_layer.addLayout(layout_setup)


    ## 2nd ########################
    # 통신 설정과 통신 상황을 보여주기 위한 두번째 그룹
        layout_comm = QHBoxLayout()

        # DISPLAY START/STOP Button
        # 제품 번호 읽기 QR Scan
        grp_product_serial = QGroupBox("제품 번호(Product Serial)")
        layout_product_serial = QHBoxLayout()

        self.btn_qr_scan = QPushButton("제품번호 읽기(QR Scan)")
        self.label_product_serial = QLabel("N/A")
        self.btn_qr_scan.clicked.connect(self.read_qr)
        layout_product_serial.addWidget(self.btn_qr_scan)
        layout_product_serial.addWidget(self.label_product_serial)
        grp_product_serial.setLayout(layout_product_serial)
        layout_comm.addWidget(grp_product_serial)


        # 통신 시작
        grp_startstop = QGroupBox("START/STOP")
        layout_startstop = QHBoxLayout()
        self.btn_start_data = QPushButton("단말기 정보 요청")
        self.btn_start_data.clicked.connect(self.get_start_data)
        self.btn_data_stop = QPushButton("정보 요청 Stop")
        self.btn_data_stop.clicked.connect(self.stop_communication)
        layout_startstop.addWidget(self.btn_start_data)
        layout_startstop.addWidget(self.btn_data_stop)
        grp_startstop.setLayout(layout_startstop)
        layout_comm.addWidget(grp_startstop)


        # Display Result 
        grp_result = QGroupBox("이미지 저장")
        layout_result = QHBoxLayout()
        self.btn_save_image = QPushButton("이미지 저장")
        self.btn_save_image.clicked.connect(self.save_image)
        layout_result.addWidget(self.btn_save_image)
        grp_result.setLayout(layout_result)
        layout_comm.addWidget(grp_result)

        main_layer.addLayout(layout_comm)


    ## 3rd ########################
    # 그래프 범례
        layout_legend = QHBoxLayout()

        # 전류 out current 
        grp_current = QGroupBox("")
        layout_current = QVBoxLayout()

        btn_current = QPushButton("CURRENT(mA)")
        btn_current.setStyleSheet("font-weight: bold; color: #0000ff;")
        btn_current.clicked.connect(lambda:self.draw_graph('Current', color=(0, 0, 255)))
        self.label_current = QLabel("0 mA")
        self.label_current.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        layout_current.addWidget(btn_current)
        layout_current.addWidget(self.label_current)
        grp_current.setLayout(layout_current)


        # 전압 out voltage
        grp_voltage = QGroupBox("")
        layout_voltage = QVBoxLayout()

        btn_voltage = QPushButton("VOLTAGE(V)")
        btn_voltage.setStyleSheet("font-weight: bold; color: #00ff00")
        btn_voltage.clicked.connect(lambda:self.draw_graph('Voltage', color=(0, 255,0)))
        self.label_voltage = QLabel("0 V")
        self.label_voltage.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        layout_voltage.addWidget(btn_voltage)
        layout_voltage.addWidget(self.label_voltage)
        grp_voltage.setLayout(layout_voltage)


        # 출력 out power
        grp_power = QGroupBox("")
        layout_power = QVBoxLayout()

        btn_power = QPushButton("POWER(W)")
        btn_power.setStyleSheet("font-weight: bold; color: #cc0000")
        btn_power.clicked.connect(lambda:self.draw_graph('Power', color=(204, 0, 0)))
        self.label_power = QLabel("0 V")
        self.label_power.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        layout_power.addWidget(btn_power)
        layout_power.addWidget(self.label_power)
        grp_power.setLayout(layout_power)


        # 출력 Impedences
        grp_impedance = QGroupBox("")
        layout_impedance = QVBoxLayout()

        btn_impedance = QPushButton("IMPEDENCE")
        btn_impedance.setStyleSheet("font-weight: bold; color: #cc9900")
        btn_impedance.clicked.connect(lambda:self.draw_graph('Impedence', color=(204, 153, 0)))
        self.label_impedance = QLabel("0")
        self.label_impedance.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        layout_impedance.addWidget(btn_impedance)
        layout_impedance.addWidget(self.label_impedance)
        grp_impedance.setLayout(layout_impedance)

        # 위상 Phase
        grp_phase = QGroupBox("")
        layout_phase = QVBoxLayout()

        btn_phase = QPushButton("PHASE")
        btn_phase.setStyleSheet("font-weight: bold; color: #cc0099")
        btn_phase.clicked.connect(lambda:self.draw_graph('Phase', color=(204, 0, 153)))
        self.label_phase = QLabel("0")
        self.label_phase.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        layout_phase.addWidget(btn_phase)
        layout_phase.addWidget(self.label_phase)
        grp_phase.setLayout(layout_phase)

        # 주파수 Frequency
        grp_freq = QGroupBox("")
        layout_freq = QVBoxLayout()

        btn_freq = QPushButton("Frequency(Hz)")
        btn_freq.setStyleSheet("font-weight: bold; color: #006699")
        btn_freq.clicked.connect(lambda:self.draw_graph('Frequency', color=(102, 0, 204)))
        self.label_freq = QLabel("0 Hz")
        self.label_freq.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        layout_freq.addWidget(btn_freq)
        layout_freq.addWidget(self.label_freq)
        grp_freq.setLayout(layout_freq)


        grp_legend = QGroupBox("데이터 범례")
        main_layer.addWidget(grp_legend)
        
        layout_legend.addWidget(grp_current)
        layout_legend.addWidget(grp_voltage)
        layout_legend.addWidget(grp_power)
        layout_legend.addWidget(grp_impedance)
        layout_legend.addWidget(grp_phase)
        layout_legend.addWidget(grp_freq)

        grp_legend.setLayout(layout_legend)
        main_layer.addWidget(grp_legend)

    ## 4th ########################
    # 그래프 디스플레이
        grp_display = QGroupBox("그래프 디스플레이")
        layout_display = QHBoxLayout()
        self.plotWidget = pg.PlotWidget()
        self.plotWidget.setBackground('w')
        layout_display.addWidget(self.plotWidget)
        grp_display.setLayout(layout_display)
        main_layer.addWidget(grp_display)
    

    ## 5th ########################
    # Status
        self.statusbar = QStatusBar()
        self.statusbar.setObjectName("statusbar")
        
        self.statusmessage = 'PSTEK, {},  {}'
        now = QDate.currentDate()
        displaymessage = self.statusmessage.format(now.toString(Qt.ISODate), 'Ready !!')
        self.statusbar.showMessage(displaymessage)
        main_layer.addWidget(self.statusbar)

        # self.showFullScreen()
        self.graph()


    # 데이터 전송 중단
    def stop_communication(self):
        device.stop_comm(self.client)
        self.stopFlag = True
        now = QDate.currentDate()
        displaymessage = self.statusmessage.format(now.toString(Qt.ISODate), '데이터 전송 중지')
        self.statusbar.showMessage(displaymessage)

        self.timer.stop()


    # Save Image
    def save_image(self):
        exporter = pg.exporters.ImageExporter(self.plotWidget.plotItem)
        now = str(time.time()).split('.')[0]
        filename = "{}.png".format(now)
        exporter.export(filename)


    # Graph 개별 그래프 
    def draw_graph(self, category, color):
        if self.graph_flag == False:
            self.graph_flag = True
            Dialog = QDialog()
            if category == 'Current':
                values = sself.Currents
            elif category == 'Voltage':
                values = sself.Voltages
            elif category == 'Power':
                values = sself.Powers
            elif category == 'Impedence':
                values = self.Impedences
            elif category == 'Frequency':
                values = self.Frequencys
            elif category == 'Phase':
                values = self.Phases

            dialog = GraphWindow(Dialog, category, color, self.index, values)
            dialog.show()
            response = dialog.exec_()

            if response == QDialog.Accepted or response == QDialog.Rejected:
                self.graph_flag = False



    # 전원 장치와 송신을 위한 설정
    def setting_generator(self):
        if self.com_open_flag == False:
            Dialog = QDialog()
            self.com_open_flag == True
            dialog = SettingWin(Dialog)
            dialog.show()
            response = dialog.exec_()

            # OK 를 하면 설정 값을 읽어와서 통신을 한다.
            if response == QDialog.Accepted:
                print("setting_generator response == QDialog.Accepted")
                self.gen_port = dialog.gen_port
                self.com_speed = dialog.com_speed
                self.com_data = dialog.com_data
                self.com_parity = dialog.com_parity
                self.com_stop = dialog.com_stop

                self.com_open_flag = False 
                self.client = device.make_connect(
                    port=self.gen_port, ptype='rtu',
                    speed=self.com_speed, bytesize=self.com_data, 
                    parity=self.com_parity, stopbits=self.com_stop
                )
                print(self.client)
                if self.client:
                    self.label_gen_port.setText(self.gen_port)
                    self.btn_com_gen.setEnabled(False)

        else:
            print("Open Dialog")


    # 전원 장치와 송신을 위한 설정
    def setting_qr(self):
        if self.com_open_flag == False:
            Dialog = QDialog()
            self.com_open_flag == True
            dialog = SettingQR(Dialog)
            dialog.show()
            response = dialog.exec_()

            # OK 를 하면 설정 값을 읽어와서 통신을 한다.
            if response == QDialog.Accepted:
                print("setting_qr response == QDialog.Accepted")
                port = dialog.qr_port
                self.com_open_flag = False
                self.qr_client = serial.Serial(port)
                
                if self.qr_client:
                    self.label_qr_port.setText(port)
                    self.btn_com_qr.setEnabled(False)
                    
        else:
            print("Open Dialog")
    

    # 전원 장치와 송신을 위한 설정
    def login_info(self):
        if self.com_open_flag == False:
            Dialog = QDialog()
            self.com_open_flag == True
            dialog = LoginWin(Dialog)
            dialog.show()
            response = dialog.exec_()

            # OK 를 하면 설정 값을 읽어와서 통신을 한다.
            if response == QDialog.Accepted:
                print("login_info response == QDialog.Accepted")
                userName = dialog.userName
                self.com_open_flag = False
                if userName:
                    self.label_user_info.setText(userName)
                    self.btn_user_info.setEnabled(False)
                    
        else:
            print("Open Dialog")

    def read_qr(self):
        if not self.qr_client:
            self.setting_qr()

        while True:
            bytesToRead = self.qr_client.inWaiting()

            if bytesToRead:
                data = self.qr_client.read(bytesToRead)
                data = data.decode('utf-8')
                value = data[3:]
                if value:
                    self.label_product_serial.setText(value)
                    break;
            time.sleep(1)


    # 데이터를 가져오는 오는 시작점
    def get_start_data(self):

        self.initial_data()

        if not self.client:
            self.setting_generator()
            
        self.stopFlag = False
        
        now = QDate.currentDate()
        displaymessage = self.statusmessage.format(now.toString(Qt.ISODate), '데이터 전송 중')
        self.statusbar.showMessage(displaymessage)

        self.count = count()

        # self.get_data()
        self.timer.setInterval(100)
        self.timer.timeout.connect(self.get_data)
        self.timer.start()


    def get_data(self):
        if not self.stopFlag:
            
            # response = device.write_freq(self.client, freq)
            i = next(self.count)
            result = device.read_registers(self.client)
            # result = device.read_impedance(self.client)

            if result:
                self.index = np.append(self.index, i)
                print(result[0], result[1], result[2], result[3], result[4], result[5])
                
                self.label_current.setText(str(result[3]))
                self.label_voltage.setText(str(result[4]))
                self.label_power.setText(str(result[0]))
                self.label_impedance.setText(str(result[5]))
                self.label_freq.setText(str(result[7]))
                self.label_phase.setText(str(result[8]))


                sself.Powers = np.append(sself.Powers, result[0])
                sself.Currents = np.append(sself.Currents, result[3])
                sself.Voltages = np.append(sself.Voltages, result[4])
                self.Impedences = np.append(self.Impedences, result[5])
                self.Frequencys = np.append(self.Frequencys, result[7])
                self.Phases = np.append(self.Phases, result[8])

                self.graph()

        else:
            self.timer.stop()

        
    def start_device(self):
        result = device.start_power(self.client)
        message = "Result : {}".format(result, dir(result))

        now = QDate.currentDate()
        displaymessage = self.statusmessage.format(now.toString(Qt.ISODate), message)
        self.statusbar.showMessage(displaymessage)

        self.get_start_data()
        

    def graph(self):
        if any(self.Impedences):
            pen_current = pg.mkPen(width=2, color=(0, 0, 255))
            pen_voltage = pg.mkPen(width=2, color=(0, 255,0))
            pen_power = pg.mkPen(width=2, color=(204, 0, 0))
            pen_impedance = pg.mkPen(width=2, color=(204, 153, 0))
            pen_phase = pg.mkPen(width=2, color=(204, 0, 153))
            pen_freq = pg.mkPen(width=2, color=(102, 0, 204))

            self.data_oc =  self.plotWidget.plot(self.index, sself.Currents, pen=pen_current, name="Currents")
            self.data_ov =  self.plotWidget.plot(self.index, sself.Voltages, pen=pen_voltage, name="Voltages")
            self.data_imp =  self.plotWidget.plot(self.index, self.Impedences, pen=pen_impedance, name="Impedences")
            self.data_phase =  self.plotWidget.plot(self.index, self.Phases, pen=pen_phase, name="Phase")


    def initial_data(self):
        self.index = np.array([])
        sself.Currents = np.array([])
        sself.Voltages = np.array([])
        sself.Powers = np.array([])
        self.Impedences = np.array([])
        self.Phases = np.array([])
        self.Freqs = np.array([])


    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Escape:
            pass
        elif e.key() == Qt.Key_F:
            self.showFullScreen()
        elif e.key() == Qt.Key_N:
            self.showNormal()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    form = MainWindow()
    form.show()
    sys.exit(app.exec_())