import json, codecs
import numpy as np
import pandas as pd
from openpyxl.utils.dataframe import dataframe_to_rows
from random import randint
from datetime import datetime

from PyQt5.QtWidgets import QWidget, QLabel, QPushButton, QDesktopWidget, QMainWindow
from PyQt5.QtWidgets import QGroupBox,QVBoxLayout, QHBoxLayout, QGridLayout
from PyQt5.QtWidgets import QApplication, QDialog, QTabWidget
from PyQt5.QtWidgets import QLCDNumber, QStatusBar, QFileDialog
from PyQt5.QtCore import QDate, Qt, QTimer
from PyQt5.QtGui import QPixmap
import pyqtgraph as pyGraph

import pulse40.pulse_serial as device
from pulse40.setup_device import SettingWin
from pulse40.pulsebasic import PulseModule, NAMES
from pulse40.block import resource_path, DataGroupBlockNoButton, SetVal
from pulse40.block import LineView, Status, Module, Field, DataGroupBlockRange
from pulse40.zoom import GraphWindowTwo

TPERIOD = 1000
DATA_TIME = 1000
FAULT_TIME = 2000
ALARM_RATIO = 50

RTU = 'RTU'
TCP = 'TCP'

import logging
FORMAT = ('%(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.DEBUG)

def CLed(title):
    btn = QPushButton(title)
    btn.setFixedSize(80, 80)
    style = "border: 3px solid lightgray;border-radius: 40px;background-color: gray;color: white;font-size: 16px;font-weight: bold;"
    btn.setStyleSheet(style)
    return btn

class PulseDCMonitoring(QMainWindow):
    def __init__(self):
        super().__init__()

        self.gen_port = None
        self.countN = 0
        self.client = None
        # self.client = 1
        self.ptype = RTU
        self.com_open_flag = False
        pyGraph.setConfigOptions(background='w')  # 흰색 배경 

        self.btn_default = "QPushButton{font-size: 12pt; color: gray; background-color: #ddd;}"
        self.btn_alarm = "QPushButton{font-size: 12pt; font-weight: bold; color: blue; background-color: white;}"
        self.main_label = "QLabel{font-size: 20pt; font-weight: bold}"
        self.font_green = 'QLabel{margin: -1px; font-size: 16pt; padding: 0px; color: green;font-weight: bold;}'
        self.font_red = 'QLabel{margin: -1px; font-size: 16pt; padding: 0px; color: red;font-weight: bold;}'

        self.pen_po = pyGraph.mkPen(width=2, color=(0, 255,0))
        self.pen_vo = pyGraph.mkPen(width=2, color=(204, 0, 0))
        self.pen_io = pyGraph.mkPen(width=2, color=(0, 0, 255))

        self.indexs = []
        self.po_list = []
        self.vo_list = []
        self.io_list = []
        self.fixed_indexes = list(range(0, 100))

        self.mods = []
        self.modslows = []

        self.sfields = []
        self.run_flag = False 

        self.time = 0
        self.timer = QTimer()
        self.dtimer = QTimer()
        self.faulttimer = QTimer()
        self.start_time = None

        self.initUI()

    def initUI(self):
        self.setWindowTitle("PSTEK PULSEDC V4.0 MONITORING")
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width(), screensize.height()) 

        mainwidget = QWidget()              # 위젯의 인스턴스 생성만으로도 QMainWindow에 붙는다.
        # mainwidget.setStyleSheet(open('canon/mainstyle.css').read());
        self.setCentralWidget(mainwidget)
        main_layout = QVBoxLayout()
        mainwidget.setLayout(main_layout)

        ################### MENU
        grp_menu = self.displayMenu()
        main_layout.addWidget(grp_menu)

        ## GRAPH MENU
        grp_graph = self.displayMenuGraph()
        main_layout.addWidget(grp_graph)

        ##### TAB Layout
        # Tab 생성
        mainTab = QWidget()
        tab_graph = QWidget()
        tab_alarm = QWidget()
        tab_setting = QWidget()
        tab_update = QWidget()

        tabs = QTabWidget()
        tabs.setStyleSheet(open('canon/style.css').read());
        tabs.addTab(mainTab, 'MAIN')
        tabs.addTab(tab_graph, 'GRAPH')
        tabs.addTab(tab_alarm, 'ALARM')
        tabs.addTab(tab_setting, 'SETTING')
        tabs.addTab(tab_update, 'UPDATE')

        self.displayMain(mainTab)
        self.displayPoVoIoGraph(tab_graph)
        self.displayAlarm(tab_alarm)
        self.displaySetValueDisplay(tab_setting)
        self.displaySetUpdate(tab_update)

        main_layout.addWidget(tabs)

        ## 4th ########################
        # Status
        self.statusbar = QStatusBar()
        self.setStatusBar(self.statusbar)
        self.statusbar.setObjectName("statusbar")
        
        self.statusmessage = 'PSTEK, {},  {}'
        now = QDate.currentDate()
        displaymessage = self.statusmessage.format(now.toString(Qt.ISODate), 'PSTEK is aiming for a global leader. !!')
        self.statusbar.showMessage(displaymessage)

        self.show()


    def displayMenu(self):
        grp_menu = QGroupBox("")
        grp_menu.setStyleSheet("QGroupBox{margin:2px;padding:2px;}")
        layout_menu = QGridLayout()
        grp_menu.setLayout(layout_menu)

        #### 통신 체크 
        grp_comm = QGroupBox("")
        grp_comm.setStyleSheet("QGroupBox{margin:2px;padding:2px;}")
        layout_comm = QVBoxLayout()
        grp_comm.setLayout(layout_comm)

        self.btn_com_gen = QPushButton("전원 장치 연결")
        self.btn_com_gen.clicked.connect(self.setting_device)
        self.lbl_com_gen = QLabel("")

        layout_comm.addWidget(self.btn_com_gen)
        layout_comm.addWidget(self.lbl_com_gen)
        layout_menu.addWidget(grp_comm, 0, 0)

        #### Title
        grp_title = QGroupBox("")
        layout_title = QVBoxLayout()
        grp_title.setLayout(layout_title)

        title1 = QLabel("HIGH VOLTAGE")
        title2 = QLabel("PULSE DC (V4.0)")
        title1.setStyleSheet(self.main_label)
        title2.setStyleSheet(self.main_label)
        layout_title.addWidget(title1)
        layout_title.addWidget(title2)
        layout_menu.addWidget(grp_title, 0, 1)

        #### MAIN 설정 
        grp_limit = QGroupBox("")
        layout_limit = QGridLayout()
        grp_limit.setLayout(layout_limit)

        ## 1, 2, 3 Address
        self.pow_limit = DataGroupBlockNoButton(self, "PO LIMIT", 16, 0, 'kW', 
                                            layout_limit, 0)
        self.vol_limit = DataGroupBlockNoButton(self, "VO LIMIT", 17, 0, 'kV', 
                                            layout_limit, 1)
        self.cur_limit = DataGroupBlockNoButton(self, "IO LIMIT", 18, 0, 'mA', 
                                            layout_limit, 2)
        layout_menu.addWidget(grp_limit, 0, 3, 1, 2)

        ####  RUN/STOP 
        grp_run = QGroupBox("")
        layout_run = QGridLayout()
        grp_run.setLayout(layout_run)

        self.btn_run = CLed('RUN/STOP')
        self.btn_run.clicked.connect(self.start_pulldata)
        layout_run.addWidget(self.btn_run, 0, 0, 2, 1)
        layout_menu.addWidget(grp_run, 0, 6)

        #### ON/OFF 
        grp_btn = QGroupBox("")
        layout_btn = QGridLayout()
        grp_btn.setLayout(layout_btn)

        self.btn_on = QPushButton("ON")
        self.btn_on.setStyleSheet("QPushButton{font-size: 16pt; font-weight: bold; color: red}")
        self.btn_on.clicked.connect(self.deviceOn)
        
        layout_btn.addWidget(self.btn_on, 0, 0, 1, 3)

        self.data_time = SetVal("데이터 간격", DATA_TIME, "m sec", layout_btn, 1)
        self.fault_time = SetVal("fault 간격", FAULT_TIME, "m sec", layout_btn, 2)

        layout_menu.addWidget(grp_btn, 0, 7)

        #### SETTING 
        grp_setup = QGroupBox("")
        layout_setup = QVBoxLayout()
        grp_setup.setLayout(layout_setup)

        # Timer Display
        self.display_time = QLCDNumber()
        self.display_time.setDigitCount(8)
        self.display_time.setStyleSheet('background: white')
        timedisplay = '{:02d}:{:02d}:{:02d}'.format(
                (self.time // 60) // 60, (self.time // 60) % 60, self.time % 60)
        self.display_time.display(timedisplay)

        self.btn_readdata = QPushButton("READ")
        self.btn_readdata.clicked.connect(self.read_data)
        layout_setup.addWidget(self.btn_readdata)

        self.label_start_time = QLabel("")
        layout_setup.addWidget(self.label_start_time)


        layout_setup.addWidget(self.display_time)
        layout_menu.addWidget(grp_setup, 0, 8)

        # Logo
        grp_logo = QGroupBox("")
        layout_logo = QVBoxLayout()
        grp_logo.setLayout(layout_logo)


        labelLogo = QLabel("")
        pixmap = QPixmap(resource_path("logo.png"))
        labelLogo.setAlignment(Qt.AlignCenter)
        labelLogo.setPixmap(pixmap)
        layout_logo.addWidget(labelLogo)

        
        self.message = QLabel("")
        self.message.setStyleSheet(self.font_green)
        layout_logo.addWidget(self.message)
        layout_menu.addWidget(grp_logo, 0, 9)

        return grp_menu


    def displayMenuGraph(self):
        grp_graph = QGroupBox("")
        layout_graph = QGridLayout()
        grp_graph.setLayout(layout_graph)

        self.vo_btn_comp = QPushButton("VO")
        self.vo_btn_comp.clicked.connect(lambda:self.displayZoomGraph("VO"))
        layout_graph.addWidget(self.vo_btn_comp, 0, 0)
        self.io_btn_comp = QPushButton("IO")
        self.io_btn_comp.clicked.connect(lambda:self.displayZoomGraph("IO"))
        layout_graph.addWidget(self.io_btn_comp, 0, 1)
        self.po_btn_comp = QPushButton("PO")
        self.po_btn_comp.clicked.connect(lambda:self.displayZoomGraph("PO"))
        layout_graph.addWidget(self.po_btn_comp, 0, 2)
        self.ro_btn_comp = QPushButton("RO")
        self.ro_btn_comp.clicked.connect(lambda:self.displayZoomGraph("RO"))
        layout_graph.addWidget(self.ro_btn_comp, 0, 3)

        self.po_slow_comp = QPushButton("PO(s)")
        self.po_slow_comp.clicked.connect(lambda:self.displayZoomGraph("PO(s)"))
        layout_graph.addWidget(self.po_slow_comp, 0, 4)
        self.vo_slow_comp = QPushButton("VO(s)")
        self.vo_slow_comp.clicked.connect(lambda:self.displayZoomGraph("VO(s)"))
        layout_graph.addWidget(self.vo_slow_comp, 0, 5)
        self.io_slow_comp = QPushButton("IO(s)")
        self.io_slow_comp.clicked.connect(lambda:self.displayZoomGraph("IO(s)"))
        layout_graph.addWidget(self.io_slow_comp, 0, 6)

        self.v16_btn_comp = QPushButton("V16")
        self.v16_btn_comp.clicked.connect(lambda:self.displayZoomGraph("V16"))
        layout_graph.addWidget(self.v16_btn_comp, 1, 0)
        self.i16_btn_comp = QPushButton("I16")
        self.i16_btn_comp.clicked.connect(lambda:self.displayZoomGraph("I16"))
        layout_graph.addWidget(self.i16_btn_comp, 1, 1)

        self.v16_slow_comp = QPushButton("V16(s)")
        self.v16_slow_comp.clicked.connect(lambda:self.displayZoomGraph("V16(s)"))
        layout_graph.addWidget(self.v16_slow_comp, 1, 2)
        self.i16_slow_comp = QPushButton("I16(s)")
        self.i16_slow_comp.clicked.connect(lambda:self.displayZoomGraph("I16(s)"))
        layout_graph.addWidget(self.i16_slow_comp, 1, 3)

        return grp_graph

    
    def displayMain(self, tab):
        main_layer = QHBoxLayout()
        tab.setLayout(main_layer)

        self.mod01 = PulseModule(self, 'Module #1', bcolor="#F2D7D5")
        self.mods.append(self.mod01)
        self.mod02 = PulseModule(self, 'Module #2', bcolor="#D4E6F1")
        self.mods.append(self.mod02)
        self.mod03 = PulseModule(self, 'Module #3', bcolor="#FCF3CF")
        self.mods.append(self.mod03)
        self.mod04 = PulseModule(self, 'Module #4', bcolor="#E8F8F5")
        self.mods.append(self.mod04)
        self.mod05 = PulseModule(self, 'Module #5', bcolor="#F6DDCC")
        self.mods.append(self.mod05)
        self.mod06 = PulseModule(self, 'Module #6', bcolor="#F2D7D5")
        self.mods.append(self.mod06)

        main_layer.addWidget(self.mod01.display())
        main_layer.addWidget(self.mod02.display())
        main_layer.addWidget(self.mod03.display())
        main_layer.addWidget(self.mod04.display())
        main_layer.addWidget(self.mod05.display())
        main_layer.addWidget(self.mod06.display())


    def displayPoVoIoGraph(self, tab):
        main_layer = QVBoxLayout()
        tab.setLayout(main_layer)

        self.dview_po = LineView('PO(kW)', self.indexs, self.po_list, '#ED6032')
        self.dview_vo = LineView('VO(V)', self.indexs, self.vo_list, '#B84EED')
        self.dview_io = LineView('IO(A)', self.indexs, self.io_list, '#34ABF7')

        self.graph_po = self.drawLineChart(self.dview_po)
        main_layer.addWidget(self.graph_po)
        self.graph_vo = self.drawLineChart(self.dview_vo)
        main_layer.addWidget(self.graph_vo)
        self.graph_io = self.drawLineChart(self.dview_io)
        main_layer.addWidget(self.graph_io)


    def drawGraphFull(self):
        try:
            self.dview_po.reDraw(self.indexs, self.po_list)
            self.dview_vo.reDraw(self.indexs, self.vo_list)
            self.dview_io.reDraw(self.indexs, self.io_list)

            # self.temp_graph.reDraw(self.mods)
                
        except Exception as e:
            print(" !!!!! drawGraphFull ", e)


    def drawGraph(self):
        try:
            if self.countN > 100:
                po_list = self.po_list[-100:]
                vo_list = self.vo_list[-100:]
                io_list = self.io_list[-100:]

                self.dview_po.reDraw(self.fixed_indexes, po_list)
                self.dview_vo.reDraw(self.fixed_indexes, vo_list)
                self.dview_io.reDraw(self.fixed_indexes, io_list)

            else:
                self.dview_po.reDraw(self.indexs, self.po_list)
                self.dview_vo.reDraw(self.indexs, self.vo_list)
                self.dview_io.reDraw(self.indexs, self.io_list)

            # self.temp_graph.reDraw(self.mods)

        except Exception as e:
            print(" !!!!! drawGraph ", e)


    def displayZoomGraph(self, gtype):
        data = []
        if gtype == 'PO':
            title = "POWER"
            for idx, mod in enumerate(self.mods):
                for subidx, ch in enumerate(mod.channels):
                    sub = {}
                    name = f"mod{idx+1}_ch{subidx +1}"
                    sub['name'] = name
                    sub['data'] = mod.channels[subidx].po_list
                    data.append(sub)

        elif gtype == 'VO':
            title = "VOLTAGE"
            for idx, mod in enumerate(self.mods):
                for subidx, ch in enumerate(mod.channels):
                    sub = {}
                    name = f"mod{idx+1}_ch{subidx +1}"
                    sub['name'] = name
                    sub['data'] = mod.channels[subidx].vo_list
                    data.append(sub)

        elif gtype == 'IO':
            title = "CURRENT"
            for idx, mod in enumerate(self.mods):
                for subidx, ch in enumerate(mod.channels):
                    sub = {}
                    name = f"mod{idx+1}_ch{subidx +1}"
                    sub['name'] = name
                    sub['data'] = mod.channels[subidx].io_list
                    data.append(sub)

        elif gtype == 'RO':
            title = "RO"
            for idx, mod in enumerate(self.mods):
                for subidx, ch in enumerate(mod.channels):
                    sub = {}
                    name = f"mod{idx+1}_ch{subidx +1}"
                    sub['name'] = name
                    sub['data'] = mod.channels[subidx].ro_list
                    data.append(sub)
        
        ## SLOW
        elif gtype == 'VO(s)':
            title = "SLOW VOLTAGE"
            for idx, mod in enumerate(self.mods):
                for subidx, ch in enumerate(mod.channels):
                    sub = {}
                    name = f"mod{idx+1}_ch{subidx +1}"
                    sub['name'] = name
                    sub['data'] = mod.channels[subidx].voslow_list
                    data.append(sub)

        elif gtype == 'IO(s)':
            title = "SLOW CURRENT"
            for idx, mod in enumerate(self.mods):
                for subidx, ch in enumerate(mod.channels):
                    sub = {}
                    name = f"mod{idx+1}_ch{subidx +1}"
                    sub['name'] = name
                    sub['data'] = mod.channels[subidx].ioslow_list
                    data.append(sub)

        elif gtype == 'PO(s)':
            title = "SLOW POWER"
            for idx, mod in enumerate(self.mods):
                for subidx, ch in enumerate(mod.channels):
                    sub = {}
                    name = f"mod{idx+1}_ch{subidx +1}"
                    sub['name'] = name
                    sub['data'] = mod.channels[subidx].poslow_list
                    data.append(sub)

        ## COMM
        elif gtype == 'V16':
            title = "VOLTAGE 16"
            for idx, mod in enumerate(self.mods):
                sub = {}
                name = f"mod{idx+1}"
                sub['name'] = name
                sub['data'] = mod.comm.v16_list
                data.append(sub)

        elif gtype == 'I16':
            title = "CURRENT 16"
            for idx, mod in enumerate(self.mods):
                sub = {}
                name = f"mod{idx+1}"
                sub['name'] = name
                sub['data'] = mod.comm.i16_list
                data.append(sub)

        elif gtype == 'V16(s)':
            title = "SLOW VOLTAGE 16"
            for idx, mod in enumerate(self.mods):
                sub = {}
                name = f"mod{idx+1}"
                sub['name'] = name
                sub['data'] = mod.comm.v16slow_list
                data.append(sub)

        elif gtype == 'I16(s)':
            title = "SLOW CURRENT 16"
            for idx, mod in enumerate(self.mods):
                sub = {}
                name = f"mod{idx+1}"
                sub['name'] = name
                sub['data'] = mod.comm.i16slow_list
                data.append(sub)


        Dialog = QDialog()
        # print(data)
        dialog = GraphWindowTwo(Dialog, title, data)
        dialog.show()
        response = dialog.exec_()
        if response == QDialog.Accepted or response == QDialog.Rejected:
            self.graph_flag = False


    def displayAlarm(self, tab):
        main_layer = QGridLayout()
        tab.setLayout(main_layer)

        # self.sfields
        ## COLUMN 0
        self.remote = Status("Remote/Local", "Local", "Remote")
        group_remote = self.remote.display()
        main_layer.addWidget(group_remote, 0, 0)

        self.ready = Status("Stand by", "Off", "Ready")
        group_ready = self.ready.display()
        main_layer.addWidget(group_ready, 1, 0)

        self.pulse = Status("POWER Ready", "Off", "POWER Ready")
        group_pulse = self.pulse.display()
        main_layer.addWidget(group_pulse, 2, 0)

        self.sync = Status("Sync Lock", "Not OK", "Sync OK")
        group_sync = self.sync.display()
        main_layer.addWidget(group_sync, 3, 0)

        # 2023. 4. 7 추가 
        self.interlock = Status("INTERLOCK", "NORMAL", "INTERLOCK")
        group_interlock = self.interlock.display()
        main_layer.addWidget(group_interlock, 4, 0)
        # 2023. 4. 7 추가 
        self.master = Status("Sync MASTER/SLAVE", "SLAVE", "MASTER")
        group_master = self.master.display()
        main_layer.addWidget(group_master, 5, 0)

        self.warning = Status("WARNING INDICATOR", "No", "WARNING")
        group_warning = self.warning.display()
        main_layer.addWidget(group_warning, 6, 0)

        self.fault = Status("FAULT INDICATOR", "No", "FAULT")
        group_fault = self.fault.display()
        main_layer.addWidget(group_fault, 7, 0)


        ## COLUMN 1
        self.fault_scr = Status("FAULT SCR", "No", "FAULT")
        group_fault_scr = self.fault_scr.display()
        main_layer.addWidget(group_fault_scr, 0, 1)

        self.md1_fault = Status("MD1_FAULT", "No", "FAULT")
        group_md1_fault = self.md1_fault.display()
        main_layer.addWidget(group_md1_fault, 1, 1)

        self.md2_fault = Status("MD2_FAULT", "No", "FAULT")
        group_md2_fault = self.md2_fault.display()
        main_layer.addWidget(group_md2_fault, 2, 1)

        self.md3_fault = Status("MD3_FAULT", "No", "FAULT")
        group_md3_fault = self.md3_fault.display()
        main_layer.addWidget(group_md3_fault, 3, 1)

        self.md4_fault = Status("MD4_FAULT", "No", "FAULT")
        group_md4_fault = self.md4_fault.display()
        main_layer.addWidget(group_md4_fault, 4, 1)

        self.md5_fault = Status("MD5_FAULT", "No", "FAULT")
        group_md5_fault = self.md5_fault.display()
        main_layer.addWidget(group_md5_fault, 5, 1)

        self.md6_fault = Status("MD6_FAULT", "No", "FAULT")
        group_md6_fault = self.md6_fault.display()
        main_layer.addWidget(group_md6_fault, 6, 1)

        self.sync_error = Status("EXT_SYNC_ERROR", "No", "FAULT")
        group_sync_error = self.sync_error.display()
        main_layer.addWidget(group_sync_error, 7, 1)

        self.fault_arc = Status("FAULT ARC", "No", "FAULT")
        group_fault_arc = self.fault_arc.display()
        main_layer.addWidget(group_fault_arc, 8, 1)

        self.tcp_comm = Status("TCP COMM ERROR", "No", "FAULT")
        group_tcp_comm = self.tcp_comm.display()
        main_layer.addWidget(group_tcp_comm, 9, 1)

        # self.internal_error = Status("INTERNAL ERROR", "No", "FAULT")
        # group_internal_error = self.internal_error.display()
        # main_layer.addWidget(group_internal_error, 10, 1)

        # self.eep_wr = Status("EEP_WR_ERROR", "No", "FAULT")
        # group_eep_wr = self.eep_wr.display()
        # main_layer.addWidget(group_eep_wr, 11, 1)

        # self.sec_shutdown = Status("SECONDARY SHUTDOWN", "No", "FAULT")
        # group_sec_shutdown = self.sec_shutdown.display()
        # main_layer.addWidget(group_sec_shutdown, 12, 1)

        # self.comm_error = Status("SECONDARY SHUTDOWN", "No", "FAULT")
        # group_comm_error = self.comm_error.display()
        # main_layer.addWidget(group_comm_error, 13, 1)

        ## COLUMN 2
        self.fault_modules = []
        for idx in range(1, 7):
            mname = f"Moduel {idx}"
            mod = Module(mname)

            for name in NAMES:
                field = Field(name, 'No', 'FAULT', 0)
                mod.add_field(field)

            group = mod.display()
            if idx < 4:
                main_layer.addWidget(group, 0, idx+1, 5, 1)
            else:
                main_layer.addWidget(group, 5, idx-2, 5, 1)

            self.fault_modules.append(mod)


    def displaySetValueDisplay(self, tab):
        main_layer = QGridLayout()
        tab.setLayout(main_layer)

        btn_setting = QPushButton("Setting 값 읽어 오기")
        btn_setting.clicked.connect(self.get_setting_value)
        main_layer.addWidget(btn_setting, 0, 0, 1, 2)


        group_00 = QGroupBox()
        layout_00 = QGridLayout()
        group_00.setLayout(layout_00)

        self.set_out_duty = DataGroupBlockNoButton(self, "Out Duty(Macro)", 19, 0, "%", layout_00, 0)
        self.set_ro = DataGroupBlockNoButton(self, "RO(측정저항값)", 20, 0, "Ohm", layout_00, 1)
        self.set_sync_freq = DataGroupBlockNoButton(self, "Sync Freq", 21, 0, "Hz", layout_00, 2)
        # self.set_temperature = DataGroupBlockNoButton(self, "Temperature(온도)", 22, 0, "℃", layout_00, 3)
        self.set_runtime = DataGroupBlockNoButton(self, "RUN TIME", 23, 0, "HOUR", layout_00, 4)
        self.set_freq_micro_set = DataGroupBlockNoButton(self, "Frequency (micro) SET", 24, 0, "kHz", layout_00, 5)
        self.set_vo_rising_t = DataGroupBlockNoButton(self, "VO Rising Time", 25, 0, "us", layout_00, 6)

        main_layer.addWidget(group_00, 1, 0)

        group_01 = QGroupBox()
        layout_01 = QGridLayout()
        group_01.setLayout(layout_01)

        self.set_sync_delay = DataGroupBlockNoButton(self, "Sync From Delay Time", 26, 0, "us", layout_01, 0)
        self.set_pulse_freq = DataGroupBlockNoButton(self, "Pulse Freq", 27, 0, "Hz", layout_01, 1)
        self.set_out_duty_set = DataGroupBlockNoButton(self, "Pulse Ratio", 28, 0, "Hz", layout_01, 2)
        self.set_freq_micro = DataGroupBlockNoButton(self, "Frequency (micro)", 29, 0, "kHz", layout_01, 3)
        self.set_vo_rising_set = DataGroupBlockNoButton(self, "VO Rising Time SET", 30, 0, "", layout_01, 4)
        self.set_sync_delay_set = DataGroupBlockNoButton(self, "Sync From Delay Time SET", 31, 0, "", layout_01, 5)
        self.set_pluse_freq_set = DataGroupBlockNoButton(self, "Pulse Freq (Macro)", 32, 0, "", layout_01, 6)

        main_layer.addWidget(group_01, 1, 1)
        # main_layer.addWidget(grp_second, 0, 1)

        grp_first = QGroupBox()
        first_layout = QGridLayout()
        grp_first.setLayout(first_layout)

        self.set_po = DataGroupBlockNoButton(self, "SET POWER", 38, 0, "KW", first_layout, 0)
        self.set_vo = DataGroupBlockNoButton(self, "SET VOLTAGE", 39, 0, "kV", first_layout, 1)
        self.set_modnum = DataGroupBlockNoButton(self, "SET MODULE NUM", 40, 0, "#", first_layout, 2)
        self.set_syncdelay = DataGroupBlockNoButton(self, "E SYNC DELAY", 41, 0, "clk", first_layout, 3)
        self.set_syncduty = DataGroupBlockNoButton(self, "E SYNC DUTY", 42, 0, "us", first_layout, 4)
        self.set_syncrising = DataGroupBlockNoButton(self, "E SYNC RISING", 43, 0, "us", first_layout, 5)
        self.set_risingqrc = DataGroupBlockNoButton(self, "RISING QRC NUM", 44, 9.00, "", first_layout, 6)
        self.set_pluse_p = DataGroupBlockNoButton(self, "SET PULSE P num", 45, 9.00, "", first_layout, 7)
        self.set_pluse_n = DataGroupBlockNoButton(self, "SET PULSE N num", 46, 9.00, "", first_layout, 8)

        grp_second = QGroupBox()
        second_layout = QGridLayout()
        grp_second.setLayout(second_layout)

        self.set_pulse_qrc = DataGroupBlockNoButton(self, "SET PULSE QRC", 47, 0, "clk", second_layout, 0)
        self.set_pulse_duty = DataGroupBlockNoButton(self, "PLUSE ON DUTY", 48, 0, "per", second_layout, 1)
        self.set_qrc_num = DataGroupBlockNoButton(self, "SET QRC NUMBER", 49, 0, "", second_layout, 2)
        self.set_ocp_level = DataGroupBlockNoButton(self, "IDC OCP LEVEL", 50, 0, "A", second_layout, 3)
        self.set_ovp_level = DataGroupBlockNoButton(self, "VDC OVP LEVEL", 51, 0, "V", second_layout, 4)
        self.set_ro_min = DataGroupBlockNoButton(self, "RO MIN LEVEL", 29, 52, "Ohm", second_layout, 5)
        self.set_pgain = DataGroupBlockNoButton(self, "P GAIN", 53, 0.005, "", second_layout, 6)
        self.set_igain = DataGroupBlockNoButton(self, "I GAIN", 54, 0.001, "", second_layout, 7)

        main_layer.addWidget(grp_first, 2, 0)
        main_layer.addWidget(grp_second, 2, 1)


    def displaySetUpdate(self, tab):
        main_layer = QGridLayout()
        tab.setLayout(main_layer)

        group_00 = QGroupBox()
        layout_00 = QGridLayout()
        group_00.setLayout(layout_00)

        self.udp_po = DataGroupBlockRange(self, "SET POWER", 1, 0, "kW (0 ~ 800)", layout_00, 0, vmin=0, vmax=800)
        self.udp_vo = DataGroupBlockRange(self, "SET S VOLTAGE", 2, 0, "Ohm", layout_00, 1, vmin=0, vmax=800)
        self.udp_mod_num = DataGroupBlockRange(self, "SET MODULE NUM", 3, 4, "#", layout_00, 2, vmin=2, vmax=32)
        self.udp_2khz = DataGroupBlockRange(self, "SET 2KHZ FREQ", 4, 1000, "Hz", layout_00, 3, vmin=500, vmax=3000)
        self.udp_2khz_delay = DataGroupBlockRange(self, "SET 2KHZ DELAY", 5, 0, "u sec", layout_00, 4, vmin=0, vmax=100)
        self.udp_2khz_duty = DataGroupBlockRange(self, "SET_2KHZ_DUTY", 6, 50, "u sec", layout_00, 5, vmin=50, vmax=250)
        self.udp_2khz_rising = DataGroupBlockRange(self, "SET_2KHZ_RISING", 7, 30, "u sec", layout_00, 6, vmin=20, vmax=50)

        main_layer.addWidget(group_00, 0, 0)

        group_01 = QGroupBox()
        layout_01 = QGridLayout()
        group_01.setLayout(layout_01)

        self.udp_comm_timeout = DataGroupBlockRange(self, "SET COMM TIMEOUT", 8, 5, "sec", layout_01, 0, vmin=1, vmax=99)
        self.udp_onoff_mode = DataGroupBlockRange(self, "SET ONOFF MODE", 9, 0, "", layout_01, 1, vmin=0, vmax=1)
        self.udp_rising_qrc = DataGroupBlockRange(self, "SET RISING QRC NUM", 10, 2, "#", layout_01, 2, vmin=2, vmax=20)
        self.udp_pulse_posi = DataGroupBlockRange(self, "SET PULSE_POSI NUM", 11, 0, "#", layout_01, 3, vmin=0, vmax=9)
        self.udp_pulse_nega = DataGroupBlockRange(self, "SET PULSE NEGA NUM", 12, 0, "#", layout_01, 4, vmin=0, vmax=9)
        self.udp_pulse_qrc = DataGroupBlockRange(self, "SET PULSE QRC", 13, 50, "clk", layout_01, 5, vmin=0, vmax=100)
        self.udp_pluse_on_duty = DataGroupBlockRange(self, "SET PULSE ON DUTY", 14, 50, "%", layout_01, 6, vmin=40, vmax=50)

        main_layer.addWidget(group_01, 0, 1)
        # main_layer.addWidget(grp_second, 0, 1)

        group10 = QGroupBox()
        layout10 = QGridLayout()
        group10.setLayout(layout10)

        self.udp_qrc_num = DataGroupBlockRange(self, "SET QRC NUM", 15, 2, "#", layout10, 0, vmin=0, vmax=32)
        self.udp_idc_ocp = DataGroupBlockRange(self, "SET IDC OCP LEVEL", 16, 6000, "mA", layout10, 1, vmin=0, vmax=7000)
        self.udp_vdc_ovp = DataGroupBlockRange(self, "SET VDC OVP LEVEL", 17, 500, "V", layout10, 2, vmin=0, vmax=600)
        self.udp_ro_min = DataGroupBlockRange(self, "SET RO_MIN LEVEL", 18, 0, "Ohm", layout10, 3, vmin=0, vmax=1000)
        self.udp_p_gain = DataGroupBlockRange(self, "SET P GAIN", 19, 0, "", layout10, 4, vmin=0, vmax=10.0)
        self.udp_i_gain = DataGroupBlockRange(self, "SET I GAIN", 20, 1, "", layout10, 6, vmin=0, vmax=100)
        self.udp_mod_reset = DataGroupBlockRange(self, "SET MODULE RESET", 21, 0, "us", layout10, 5, vmin=0, vmax=100)

        grp_second = QGroupBox()
        second_layout = QGridLayout()
        grp_second.setLayout(second_layout)

        self.udp_pulse_qrc = DataGroupBlockRange(self, "SET_OUTPUT_COUNT", 22, 0, "clk", second_layout, 0)
        self.udp_pulse_duty = DataGroupBlockRange(self, "SET_NETWORK_IP1", 23, 0, "per", second_layout, 1)
        self.udp_qrc_num = DataGroupBlockRange(self, "SET_NETWORK_IP2", 24, 0, "", second_layout, 2)
        self.udp_ocp_level = DataGroupBlockRange(self, "SET_NETWORK_IP3", 25, 0, "A", second_layout, 3)
        self.udp_ovp_level = DataGroupBlockRange(self, "VSET_NETWORK_IP4", 26, 0, "V", second_layout, 4)
        self.udp_ro_min = DataGroupBlockRange(self, "SET_MODEL", 27, 100, "Ohm", second_layout, 5)
        self.udp_pgain = DataGroupBlockRange(self, "SET_VERSION", 28, 221, "", second_layout, 6)
        
        main_layer.addWidget(group10, 1, 0)
        main_layer.addWidget(grp_second, 1, 1)


    def drawLineChart(self, dt):
        grp = QGroupBox("")
        layout = QVBoxLayout()
        grp.setLayout(layout)
        layout.addWidget(dt.btn)
        layout.addWidget(dt.plotWidget)
        return grp

    # BIT 연산 
    def bit_check(self, x):
        data = []
        for idx in range(16):
            if (x & (1<<idx)):
                data.append(idx)
        return data


    def modifyDeviceValue(self, address, value):
        if self.client:
            device.write_registers(self.client, address, value, self.ptype)
        else:
            self.message.setText("전원 장치와 연결이 필요합니다.")

    def deviceOff(self):
        if self.client:
            device.standOff(self.client, self.ptype)
        else:
            self.message.setText("전원 장치와 연결이 필요합니다.")

    def deviceOn(self):
        if self.client:
            device.standBy(self.client, self.ptype)
        else:
            self.message.setText("전원 장치와 연결이 필요합니다.")

    # 전원 장치와 송신을 위한 설정
    def setting_device(self):
        if self.com_open_flag == False:
            Dialog = QDialog()
            self.com_open_flag == True
            dialog = SettingWin(Dialog)
            dialog.show()
            response = dialog.exec_()

            # OK 를 하면 설정 값을 읽어와서 통신을 한다.
            if response == QDialog.Accepted:
                if dialog.selected == 'RTU':
                    self.gen_port = dialog.gen_port
                    self.com_speed = dialog.com_speed
                    self.com_data = dialog.com_data
                    self.com_parity = dialog.com_parity
                    self.com_stop = dialog.com_stop
                    self.com_open_flag = False

                    self.client = device.connect_rtu(
                        port=self.gen_port, ptype='rtu',
                        speed=self.com_speed, bytesize=self.com_data, 
                        parity=self.com_parity, stopbits=self.com_stop
                    )
                    self.ptype = RTU
                else:
                    self.gen_port = dialog.ipaddress
                    self.ipport = int(dialog.ipport)
                    # print(self.gen_port, self.ipport)

                    self.client = device.connect_tcp(self.gen_port, self.ipport)
                    self.ptype = TCP

                # Graph
                self.graph_type = dialog.graph_type

                if self.client:
                    self.lbl_com_gen.setText(self.gen_port)
                    self.btn_com_gen.hide()
                    self.btn_run.setEnabled(True)
                    
        else:
            print("Open Dialog")


    def start_pulldata(self):
        if self.client:
            if self.run_flag:
                self.run_flag = False 
                self.btn_run.setStyleSheet("border: 3px solid lightgray;border-radius: 40px;background-color: gray;color: white;font-size: 16px;font-weight: bold;")
                self.btn_run.setText('RUN')
                
                if not self.timer:
                    self.timer = QTimer()
                    self.dtimer = QTimer()
                    self.faulttimer = QTimer()

                # setting timer
                self.timer.stop()
                self.dtimer.stop()
                self.faulttimer.stop()

                self.timer = None
                self.dtimer = None
                self.faulttimer = None

                self.save_data_as_json()

            else:
                if self.countN:
                    self.countN = 0
                    self.time = 0

                    self.indexs = []
                    self.po_list = []
                    self.vo_list = []
                    self.io_list = []

                    self.mod01.reset()
                    self.mod02.reset()
                    self.mod03.reset()
                    self.mod04.reset()
                    self.mod05.reset()
                    self.mod06.reset()


                self.start_time = datetime.now()
                start_time = datetime.strftime(self.start_time, '%Y-%m-%d %H:%M:%S')
                self.label_start_time.setText(start_time)

                self.run_flag = True 
                self.btn_run.setStyleSheet("border: 3px solid lightgray;border-radius: 40px;background-color: green;color: white;font-size: 16px;font-weight: bold;")
                self.btn_run.setText('STOP')

                if not self.timer:
                    self.timer = QTimer()
                    self.dtimer = QTimer()
                    self.faulttimer = QTimer()

                self.active_normal()
                # setting timer
                self.timer.setInterval(TPERIOD)
                self.timer.timeout.connect(self.get_time)
                self.timer.start()

                DATA_PERIOD = self.data_time.get_val()
                FAULT_PERIOD = self.fault_time.get_val()

                self.dtimer.setInterval(DATA_PERIOD)
                self.dtimer.timeout.connect(self.get_data)
                self.dtimer.start()

                self.faulttimer.setInterval(FAULT_PERIOD)
                self.faulttimer.timeout.connect(self.get_fault)
                self.faulttimer.start()
        else:
            self.message.setText("전원 장치를 먼저 연결하세요.")
            self.message.setStyleSheet(self.font_green)


    def stop_pulldata(self):
        self.run_flag = False 
        self.btn_run.setStyleSheet("border: 3px solid lightgray;border-radius: 40px;background-color: gray;color: white;font-size: 16px;font-weight: bold;")
        self.btn_run.setText('RUN')

        # setting timer
        self.timer.stop()
        self.dtimer.stop()
        self.faulttimer.stop()

        self.timer = None
        self.dtimer = None
        self.faulttimer = None


    def updateSetting(self, setting):
        self.pow_limit.setValue(setting[0])
        self.vol_limit.setValue(setting[1])
        self.cur_limit.setValue(setting[2])

        self.set_out_duty.setValue(setting[3])
        self.set_ro.setValue(setting[4])
        self.set_sync_freq.setValue(setting[5])
        # self.set_temperature.setValue(setting[6])
        self.set_runtime.setValue(setting[7])
        self.set_freq_micro_set.setValue(setting[8])
        self.set_vo_rising_t.setValue(setting[9])
        
        self.set_sync_delay.setValue(setting[10])
        self.set_pulse_freq.setValue(setting[11])
        self.set_out_duty_set.setValue(setting[12])
        self.set_freq_micro.setValue(setting[13])
        self.set_vo_rising_set.setValue(setting[14])
        self.set_sync_delay_set.setValue(setting[15])
        self.set_pluse_freq_set.setValue(setting[16])

        self.set_po.setValue(setting[20])
        self.set_vo.setValue(setting[21])
        self.set_modnum.setValue(setting[22])
        self.set_syncdelay.setValue(setting[23])
        self.set_syncduty.setValue(setting[24])
        self.set_syncrising.setValue(setting[25])
        self.set_risingqrc.setValue(setting[26])
        self.set_pluse_p.setValue(setting[27])
        self.set_pluse_n.setValue(setting[28])

        self.set_pulse_qrc.setValue(setting[29])
        self.set_pulse_duty.setValue(setting[30])
        self.set_qrc_num.setValue(setting[31])
        self.set_ocp_level.setValue(setting[32])
        self.set_ovp_level.setValue(setting[33])
        self.set_ro_min.setValue(setting[34])
        self.set_pgain.setValue(setting[35])
        self.set_igain.setValue(setting[36])


    def get_setting_value(self):
        if self.client:
            # data = device.read_setting_from_device(self.client, self.ptype)
            if data['working']:
                setting = data['setting']
                # print(setting)
                self.updateSetting(setting)
            else:
                self.message.setText("통신 에레 발생")
                self.message.setStyleSheet(self.font_red)
        else:
            self.message.setText("전원 장치를 먼저 연결하세요.")
            self.message.setStyleSheet(self.font_green)


    def get_data(self):
        data = device.read_data_from_device(self.client, self.ptype)
        if data['working']:
            self.countN = self.countN  + 1
            self.indexs.append(self.countN)

            setting = data['setting']
            logging.info(setting)
            self.po_list.append(setting[0])
            self.vo_list.append(setting[1])
            self.io_list.append(setting[2])

            self.pow_limit.setValue(setting[0])
            self.vol_limit.setValue(setting[1])
            self.cur_limit.setValue(setting[2])

            try:
                mod01 = data['mod01']
                dat01 = mod01[:21]
                dat02 = mod01[21:]

                mod03 = data['mod03']
                dat03 = mod03[:21]
                dat04 = mod03[21:]

                mod05 = data['mod05']
                dat05 = mod05[:21]
                dat06 = mod05[21:]
                
                self.mod01.update_data(dat01)
                self.mod02.update_data(dat02)
                self.mod03.update_data(dat03)
                self.mod04.update_data(dat04)
                self.mod05.update_data(dat05)
                self.mod06.update_data(dat06)

            except Exception as e:
                print(e)

            if self.countN % 20 == 2:
                self.drawGraph()

            if self.countN % 20 == 12:
                self.updateSetting(setting)
        else:
            self.message.setText("통신 에레 발생")
            self.message.setStyleSheet(self.font_red)


    def status_update(self, condition):
        if 0 in condition:
            self.remote.change_warning()
        if 1 in condition:
            self.ready.change_warning()
        if 2 in condition:
            self.pulse.change_warning()
        if 3 in condition:
            self.sync.change_warning()
        if 4 in condition:
            self.interlock.change_warning()
        if 5 in condition:
            self.master.change_warning()
        if 6 in condition:
            self.warning.change_warning()
        if 7 in condition:
            self.fault.change_warning()


    def moduel_update(self, condition):
        if 0 in condition:
            self.fault_scr.change_warning()
        if 1 in condition:
            self.md1_fault.change_warning()
        if 2 in condition:
            self.md2_fault.change_warning()
        if 3 in condition:
            self.md3_fault.change_warning()
        if 4 in condition:
            self.md4_fault.change_warning()
        if 5 in condition:
            self.md5_fault.change_warning()
        if 6 in condition:
            self.md6_fault.change_warning()
        if 7 in condition:
            self.sync_error.change_warning()
        if 8 in condition:
            self.fault_arc.change_warning()
        if 9 in condition:
            self.tcp_comm.change_warning()
        # if 10 in condition:
        #     self.internal_error.change_warning()
        # if 11 in condition:
        #     self.eep_wr.change_warning()
        # if 12 in condition:
        #     self.sec_shutdown.change_warning()
        # if 13 in condition:
        #     self.comm_error.change_warning()

            
    def save_data_as_json(self):
        data = {}
        file = f"pulsedcv4_{self.start_time.year}_{self.start_time.month}_{self.start_time.day}_{self.start_time.hour}_{self.start_time.minute}_{self.start_time.second}"

        sub = {}
        sub['indexs'] = self.indexs
        sub['POWER'] = self.po_list
        sub['VOLTAGE'] = self.vo_list
        sub['CURRENT'] = self.io_list
        sub['start_time'] = datetime.strftime(self.start_time, '%Y-%m-%d %H:%M:%S')
        sub['during'] = self.time
        data['summary'] = sub 

        for midx, mod in enumerate(self.mods):
            channel = {}
            channel['V16'] = mod.comm.v16_list
            channel['I16'] = mod.comm.i16_list
            channel['V16_SLOW'] = mod.comm.v16slow_list
            channel['I16_SLOW'] = mod.comm.i16slow_list
            
            for cidx, ch in enumerate(mod.channels):
                mdch_name = f"MD#{midx+1}_#CH{cidx+1}"
                vo_name = f"{mdch_name}_VO"
                channel[vo_name] = ch.vo_list
                io_name = f"{mdch_name}_IO"
                channel[io_name] = ch.io_list
                po_name = f"{mdch_name}_PO"
                channel[po_name] = ch.po_list
                ro_name = f"{mdch_name}_RO"
                channel[ro_name] = ch.ro_list

                voslow_name = f"{mdch_name}_VO(SLOW)"
                channel[voslow_name] = ch.voslow_list
                ioslow_name = f"{mdch_name}_IO(SLOW)"
                channel[ioslow_name] = ch.ioslow_list
                poslow_name = f"{mdch_name}_PO(SLOW)"
                channel[poslow_name] = ch.poslow_list
                    
            data[mdch_name] = channel


        jsonfile = f"data/{file}.json"
        with open(jsonfile, 'wb') as afile:
            json.dump(data, codecs.getwriter('utf-8')(afile))


    # READ DATA 
    def read_data(self):
        fname, _ = QFileDialog.getOpenFileName(self, 'Open file', 
         './data',"Json files (*.json)")

        with open(fname, 'r', encoding='utf-8') as json_file:
            data = json.load(json_file)

            self.po_list = data['summary']['POWER']
            self.vo_list = data['summary']['VOLTAGE']
            self.io_list = data['summary']['CURRENT']
            self.indexs = list(range(len(self.po_list)))

            try:
                start_time = data['summary']['start_time']
                self.label_start_time.setText(start_time)
                self.start_time = datetime.strptime(start_time, '%Y-%m-%d %H:%M:%S')
            except Exception as e:
                self.start_time = None 

            try:
                self.time = data['summary']['time']
            except Exception as e:
                self.time = len(self.indexs)

            for midx in range(5):
                for cidx in range(4):
                    mdch_name = f"MD#{midx+1}_#CH{cidx+1}"
                    self.mods[midx].channels[cidx].po_list = data[mdch_name]['PO']
                    self.mods[midx].channels[cidx].vo_list = data[mdch_name]['VO']
                    self.mods[midx].channels[cidx].io_list = data[mdch_name]['IO']
                    self.mods[midx].channels[cidx].v16_list = data[mdch_name]['V16']
                    self.mods[midx].channels[cidx].i16_list = data[mdch_name]['I16']
                    self.mods[midx].channels[cidx].duty_list = data[mdch_name]['DUTY']
                    self.mods[midx].channels[cidx].ro_list = data[mdch_name]['RO']
                    self.mods[midx].channels[cidx].temp1_list = data[mdch_name]['TEMP1']
                    self.mods[midx].channels[cidx].temp2_list = data[mdch_name]['TEMP2']
                    self.mods[midx].channels[cidx].temp3_list = data[mdch_name]['TEMP3']
                    self.mods[midx].channels[cidx].temp4_list = data[mdch_name]['TEMP4']

        self.drawGraphFull()


    def get_fault(self):
        data = device.read_fault_from_device(self.client, self.ptype)
        for idx, val in enumerate(data):
            if val:
                self.fault_reset()
                logging.info(f"get_fault :: {val}")
                faults = self.bit_check(val)
                if idx == 0:
                    self.status_update(faults)
                elif idx == 3:
                    self.moduel_update(faults)
                elif idx >= 5:
                    self.fault_modules[idx-5].update_warning(faults)


    def get_time(self):
        self.time += 1
        timedisplay = '{:02d}:{:02d}:{:02d}'.format((self.time // 60) // 60, 
                        (self.time // 60) % 60, self.time % 60)
        self.display_time.display(timedisplay)


    def fault_reset(self):
        for mod in self.fault_modules:
            mod.reset()


    def active_normal(self):
        self.mod01.active_normal()
        self.mod02.active_normal()
        self.mod03.active_normal()
        self.mod04.active_normal()
        self.mod05.active_normal()
        self.mod06.active_normal()


if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    ex = PulseDCMonitoring()
    sys.exit(app.exec_())

