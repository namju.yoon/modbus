#!/usr/bin/env python
# coding: utf-8

# 예제 내용
# * 기본 위젯을 사용하여 기본 창을 생성
# * 다양한 레이아웃 위젯 사용
import os
import re
import sys
import serial
import threading

from PyQt5.QtWidgets import QWidget, QLabel, QPushButton, QDesktopWidget, QMainWindow
from PyQt5.QtWidgets import QGroupBox, QVBoxLayout, QHBoxLayout, QGridLayout, QSpinBox
from PyQt5.QtWidgets import QApplication, QDialog,  QDialogButtonBox, QLineEdit
from PyQt5.QtWidgets import QTableWidgetItem, QCheckBox, QLCDNumber, QDoubleSpinBox, QRadioButton
from PyQt5 import QtCore
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap
import pyqtgraph as pyGraph

# from modbus import semes as semes
from semes.setup import SettingWin

GRAPH = 1
DATA = 2
TIME_PERIOD = 1000
DATA_PERIOD = 1000

RTU = 'RTU'
SCPI = 'SCPI'
SEMES = 'SEMES'
TCP = 'TCP'

FORWARD = 0 
REVERSE = 1 
TIME_INTERVAL= 0.3

import logging
FORMAT = ('%(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.INFO)


def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)

class GBlueLabel(QLineEdit):
    def __init__(self, val, *args, **kwargs):
        super(GBlueLabel, self).__init__(*args, **kwargs)
        self.setAlignment(Qt.AlignRight)
        self.setStyleSheet("background-color: #82E0AA; font-size: 14px")
        self.setText(str(val))

class Field:
    def __init__(self, name, unit):
        self.name = name
        self.label = QLabel(name)
        self.val = GBlueLabel(0)
        self.unit = QLabel(unit)

    def display(self):
        group = QGroupBox()
        layout = QHBoxLayout()
        group.setLayout(layout)

        layout.addWidget(self.label)
        layout.addWidget(self.val)
        layout.addWidget(self.unit)

        return group 

    def setValue(self, val):
        self.val.setText(str(val))

class Status:
    grayfont = "color: #222;"
    bluefont = "color: #00ff00;"
    redfont = "color: #ff0000;font-weight: bold;"

    def __init__(self, parent, name, status1, status2, cmd):
        self.parent = parent
        self.name = name
        self.cmd = cmd
        self.t_period = 10

        self.rd_enable = QRadioButton(status1)
        self.rd_enable.clicked.connect(lambda:self.radioPortClicked(self.rd_enable))

        self.rd_disable = QRadioButton(status2)
        self.rd_disable.clicked.connect(lambda:self.radioPortClicked(self.rd_disable))
        
        if cmd == 'ENABLE':
            self.rd_enable.setChecked(True)
        else:
            self.rd_disable.setChecked(True)

        self.time = QSpinBox()
        self.time.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self.time.setRange(0, 30)
        self.time.setValue(10)
        self.time.valueChanged.connect(self.time_changed)

        self.btn_commchange = QPushButton("MODE CHANGE")
        self.btn_commchange.clicked.connect(lambda:self.parent.change_comm_error_mode(self.cmd, self.t_period))

    def __str__(self):
        return f"{self.name}"

    def time_changed(self):
        self.t_period = self.time.value()
        logging.info(f"time_changed :: {self.t_period}")

    def display(self):
        group = QGroupBox(self.name)
        layout = QGridLayout()
        group.setLayout(layout)

        # layout.addWidget(self.lbl_name)
        label = QLabel('초')
        layout.addWidget(self.rd_enable, 0, 0, 1, 2)
        layout.addWidget(self.rd_disable, 1, 0, 1, 2)
        layout.addWidget(self.time, 2, 0)
        layout.addWidget(label, 2, 1)
        layout.addWidget(self.btn_commchange, 3, 0, 1, 2)

        return group
    
    def radioPortClicked(self, btn):
        result = btn.text()
        self.cmd = result
        logging.info(f"radioPortClicked :: {self.cmd}")

class VoltageBlock(QWidget):
    def __init__(self, parent, text, value, 
                minval, maxval, unit):
        super(VoltageBlock, self).__init__()
        self.parent = parent
        self.text = text
        self.value = value 
        self.minval = minval 
        self.maxval = maxval 
        self.unit = unit

    def display(self):
        group = QGroupBox()
        layout = QHBoxLayout()
        group.setLayout(layout)

        text_block = QLabel(self.text)
        unit_block = QLabel(self.unit)

        self.edit_block = QSpinBox()
        self.edit_block.setAlignment(Qt.AlignRight)
        self.edit_block.setStyleSheet('margin: -1px; padding: 0px; color: red')
        self.edit_block.setRange(self.minval, self.maxval)
        self.edit_block.setValue(self.value)
        range_value = f"{self.minval} ~ {self.maxval}"
        range_block = QLabel(range_value)
        self.btn_block = QPushButton("변 경")
        self.btn_block.clicked.connect(self.change_voltage)

        layout.addWidget(text_block)
        layout.addWidget(self.edit_block)
        layout.addWidget(range_block)
        layout.addWidget(unit_block)
        layout.addWidget(self.btn_block)
        
        return group

    def change_voltage(self):
        vol = self.edit_block.value()
        command = f"SV{vol}.000000\r".encode()
        logging.info(command)
        try:
            self.parent.client.write(command)
            while True:
                try:
                    feedback = self.parent.client.readline()
                    if feedback:
                        bitestr = feedback.decode('utf-8')
                        try:
                            result = re.findall('[-0-9]+', str(bitestr))
                            logging.info(f"device_write :: {command} :: {result}")
                            return result
                        except Exception as e:
                            return None
                        break
                except KeyboardInterrupt:
                    feedback = None
                    break
            return None
        except Exception as e:
            logging.error(f"change_current :: {e}")

class CurrentBlock(QWidget):
    def __init__(self, parent, text, value, 
                minval, maxval, unit):
        super(CurrentBlock, self).__init__()
        self.parent = parent
        self.text = text
        self.value = value 
        self.minval = minval 
        self.maxval = maxval 
        self.unit = unit

    def display(self):
        group = QGroupBox()
        layout = QHBoxLayout()
        group.setLayout(layout)

        text_block = QLabel(self.text)
        unit_block = QLabel(self.unit)

        self.edit_block = QDoubleSpinBox()
        self.edit_block.setAlignment(Qt.AlignRight)
        self.edit_block.setSingleStep(0.1)
        self.edit_block.setStyleSheet('margin: -1px; padding: 0px; color: red')
        self.edit_block.setRange(self.minval, self.maxval)
        self.edit_block.setDecimals(2)
        self.edit_block.setValue(self.value)
        range_value = f"{self.minval} ~ {self.maxval}"
        range_block = QLabel(range_value)
        self.btn_block = QPushButton("변 경")
        self.btn_block.clicked.connect(self.change_current)

        layout.addWidget(text_block)
        layout.addWidget(self.edit_block)
        layout.addWidget(range_block)
        layout.addWidget(unit_block)
        layout.addWidget(self.btn_block)
        
        return group

    def change_current(self):
        current = self.edit_block.value()
        val = int(current * 1000)
        logging.info(f"change_current :: {current} :: {val}")
        command = f"SI{val}.000000\r".encode()
        logging.info(command)
        try:
            self.parent.client.write(command)
            while True:
                try:
                    feedback = self.parent.client.readline()
                    if feedback:
                        bitestr = feedback.decode('utf-8')
                        try:
                            result = re.findall('[-0-9]+', str(bitestr))
                            logging.info(f"device_write :: {command} :: {result}")
                            return result
                        except Exception as e:
                            return None
                        break
                except KeyboardInterrupt:
                    feedback = None
                    break
            return None
        except Exception as e:
            logging.error(f"change_current :: {e}")

class WarningDialog(QDialog):
    def __init__(self, message):
        super().__init__()
        self.message = message
        self.setupUI()

    def setupUI(self):
        self.setGeometry(100, 100, 400, 200)

        self.setWindowTitle("Warning !!")
        layout = QVBoxLayout()
        self.setLayout(layout)
        self.label = QLabel(self.message)
        layout.addWidget(self.label)

        self.buttonBox = QDialogButtonBox()
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        layout.addWidget(self.buttonBox)

        self.buttonBox.accepted.connect(self.on_accepted)
        self.buttonBox.rejected.connect(self.reject)

    def on_accepted(self):
        print("WarningDialog :: OK")
        self.accept()

class ReadFunctionSEMES:
    grayfont = "color: #222;"
    bluefont = 'margin: -1px; padding: 0px; color: blue'
    btnblue = "QPushButton{font-size: 12pt; font-weight: bold; color: blue}"
    btnred = "QPushButton{font-size: 12pt; font-weight: bold; color: red}"

    def __init__(self, parent, name):
        self.name = name
        self.parent = parent
        self.indexs = []
        self.data1 = []
        self.data2 = []
        self.data3 = []
        self.event = threading.Event()

        self.dtimer = QtCore.QTimer()
        self.displaytimer = QtCore.QTimer()
        self.toogletimer = QtCore.QTimer()
        self.time = 0
        self.toggle = FORWARD

        self.voltage = Field('VOLTAGE', 'V')
        self.current = Field('CURRENT', 'mA')
        self.leak_curr = Field('LEAK CURRENT', 'uA')

        self.btn_run = QPushButton("RUN")
        self.btn_run.setEnabled(False)
        self.btn_run.setStyleSheet(ReadFunctionSEMES.btnblue)
        self.btn_run.clicked.connect(self.start_data)

        self.btn_stop = QPushButton("Stop")
        self.btn_stop.setStyleSheet(ReadFunctionSEMES.btnred)
        self.btn_stop.clicked.connect(self.stop_data)

        # Toggle 기능 
        self.auto_toggle = QCheckBox("Auto Toggle")
        self.toogle_time = QLineEdit("10")
        self.label2 = QLabel("초 (토글 시간)")

        ## READ DATA TIME
        self.data_time = QLineEdit("500")
        self.label3 = QLabel("m sec")

        # Graph #1
        self.graphWidget1 = pyGraph.PlotWidget()
        self.graphWidget1.setBackground('w')
        self.graphWidget1.setMouseEnabled(x=False, y=False)
        self.pen1 = pyGraph.mkPen(width=2, color=(255, 0, 0))
        self.graphWidget1.plot(self.indexs, self.data1, pen=self.pen1, name="VOLTAGE")

        # Graph #2
        self.graphWidget2 = pyGraph.PlotWidget()
        self.graphWidget2.setBackground('w')
        self.graphWidget2.setMouseEnabled(x=False, y=False)
        self.pen2 = pyGraph.mkPen(width=2, color=(0, 255, 0))
        self.graphWidget2.plot(self.indexs, self.data2, pen=self.pen2, name="CURRENT")

        # Graph #3
        self.graphWidget3 = pyGraph.PlotWidget()
        self.graphWidget3.setBackground('w')
        self.graphWidget3.setMouseEnabled(x=False, y=False)
        self.pen3 = pyGraph.mkPen(width=2, color=(0, 0, 255))
        self.graphWidget3.plot(self.indexs, self.data3, pen=self.pen3, name="Leak Current")

    def enable_start(self):
        self.btn_run.setEnabled(True)

    def display(self):
        group = QGroupBox(self.name)
        layout = QGridLayout()
        group.setLayout(layout)

        group_vol = self.voltage.display()
        group_cur = self.current.display()
        group_leak_curr = self.leak_curr.display()
        layout.addWidget(group_vol, 0, 0)
        layout.addWidget(group_cur, 0, 1)
        layout.addWidget(group_leak_curr, 0, 2)

        cmd_run = QLabel('EV')
        group_run = self.display_sub("RUN", self.btn_run, cmd_run)
        layout.addWidget(group_run, 0, 3)

        cmd_stop = QLabel('DV')
        group_stop = self.display_sub("STOP", self.btn_stop, cmd_stop)
        layout.addWidget(group_stop, 0, 4)

        layout.addWidget(self.auto_toggle, 1, 0)
        layout.addWidget(self.toogle_time, 1, 1)
        layout.addWidget(self.label2, 1, 2)
        layout.addWidget(self.data_time, 1, 3)  
        layout.addWidget(self.label3, 1, 4)


        layout.addWidget(self.graphWidget1, 2, 0, 1, 5)
        layout.addWidget(self.graphWidget2, 3, 0, 1, 5)
        layout.addWidget(self.graphWidget3, 4, 0, 1, 5)
        return group
    
    def display_sub(self, name, btn, label):
        group = QGroupBox(name)
        layout = QVBoxLayout()
        group.setLayout(layout)

        layout.addWidget(btn)
        layout.addWidget(label)

        return group

    def get_time(self):
        self.time += 1
        timedisplay = '{:02d}:{:02d}:{:02d}'.format((self.time // 60) // 60, 
                        (self.time // 60) % 60, self.time % 60)
        self.parent.display_time.display(timedisplay)


    def start_data(self):
        self.data1 = []
        self.data2 = []
        self.data3 = []

        self.graphWidget1.clear()
        self.graphWidget2.clear()
        self.graphWidget3.clear()

        date_time = self.data_time.value()
        data_time = int(date_time)

        self.dtimer.setInterval(data_time)
        self.dtimer.timeout.connect(self.get_data_semes)
        self.dtimer.start()

        self.displaytimer.setInterval(TIME_PERIOD)
        self.displaytimer.timeout.connect(self.get_time)
        self.displaytimer.start()

        command = "V-"
        mcommand = f"{command}\r".encode()
        logging.info(f"START TOGGLE :: {mcommand}")
        self.device_write(mcommand)

        command = "EV"
        mcommand = f"{command}\r".encode()
        self.device_write(mcommand)

        if self.auto_toggle.isChecked():
            toogle_time = int(self.toogle_time.text())
            logging.info(f"toogle_time :: {toogle_time}")
            TOOGLE_TIME = toogle_time * 1000
            self.toogletimer.setInterval(TOOGLE_TIME)
            self.toogletimer.timeout.connect(self.device_toggle)
            self.toogletimer.start()
        else:
            print("self.auto_toggle.isChecked():")


    def device_toggle(self):
        self.dtimer.stop()
        self.dtimer = None 
        if self.toggle == FORWARD:
            self.toggle = REVERSE
            command = "V+"
            mcommand = f"{command}\r".encode()

        else:
            self.toggle = FORWARD
            command = "V-"
            mcommand = f"{command}\r".encode()
        self.device_write(mcommand)
        # logging.info(f"device_toggle :: {self.toggle} :: {mcommand}\n\n")

        self.dtimer = QtCore.QTimer()
        self.dtimer.setInterval(DATA_PERIOD)
        self.dtimer.timeout.connect(self.get_data_semes)
        self.dtimer.start()

    def device_comm_error(self, cmd, period):
        command = f"FT{period}"
        mcommand = f"{command}\r".encode()
        self.device_write(mcommand)

        if cmd == 'ENABLE':
            command = 'EW'
        elif cmd == 'DISABLE':
            command = 'DW'
        mcommand = f"{command}\r".encode()
        self.device_write(mcommand)


    def device_write(self, command):
        logging.info(f"device_write :: {command}\n\n")
        self.parent.client.write(command)
        while True:
            try:
                feedback = self.parent.client.readline()
                if feedback:
                    bitestr = feedback.decode('utf-8')
                    try:
                        result = re.findall('[-0-9]+', str(bitestr))
                        # logging.info(f"device_write :: {command} :: {result}")
                        return result
                    except Exception as e:
                        return None
                    break
            except KeyboardInterrupt:
                feedback = None
                break
        return None

    def stop_data(self):
        command = "DV"
        mcommand = f"{command}\r".encode()
        self.device_write(mcommand)

        self.dtimer.stop()
        self.dtimer = None 
        self.dtimer = QtCore.QTimer()

        self.displaytimer.stop()
        self.displaytimer = None 
        self.displaytimer = QtCore.QTimer()

        self.toogletimer.stop()
        self.toogletimer = None 
        self.toogletimer = QtCore.QTimer()

    def get_data_semes(self):
        if self.parent.client:
            result = self.semes_get_measure('VOLTAGE')
            if result:
                # logging.info(f"get_data_semes :: VOLTAGE :: {result}")
                voltage = int(result[0])
                self.data1.append(voltage)
                self.voltage.setValue(voltage)
            
            self.event.wait(TIME_INTERVAL)

            result = self.semes_get_measure('CURRENT')
            if result:
                # logging.info(f"get_data_semes :: CURRENT :: {result}")
                current = float(int(result[0]) / 1000)
                self.data2.append(current)
                self.current.setValue(current)
                
                leak_current = int(result[0]) % 1000
                self.data3.append(leak_current)
                self.leak_curr.setValue(leak_current)
                logging.info(f"get_data_semes :: {voltage} :: {current} :: {leak_current}")

                self.draw_graph()

    def semes_get_measure(self, ptype):
        if ptype == 'VOLTAGE':
            command = 'RV\r'
        elif ptype == 'CURRENT':
            command = 'R+\r'
        self.parent.client.write(command.encode())

        while True:
            try:
                feedback = self.parent.client.readline()
                if feedback:
                    bitestr = feedback.decode('utf-8')
                    try:
                        result = re.findall('[-0-9]+', str(bitestr))
                        # logging.info(f"semes_get_measure :: {ptype} :: {result}")
                        return result
                    except Exception as e:
                        return None
                    break
            except KeyboardInterrupt:
                feedback = None
                break
        return None
    

    def draw_graph(self):
        index1 = list(range(len(self.data1)))
        self.graphWidget1.plot(index1, self.data1, pen=self.pen1, name="VOLTAGE")

        index2 = list(range(len(self.data2)))
        self.graphWidget2.plot(index2, self.data2, pen=self.pen2, name="CURRENT")

        index3 = list(range(len(self.data3)))
        self.graphWidget3.plot(index3, self.data3, pen=self.pen3, name="LEAK CURRENT")


    def send_enable(self):
        self.btn_run.setEnabled(True)


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()

        self.stimer = QtCore.QTimer()
        self.dtimer = QtCore.QTimer()

        self.com_setting_flag = False
        self.com_open_flag = False
        self.run_flag = False
        self.ch_ptype = False

        self.client = None
        # self.client = 1

        self.ptype = RTU
        self.time = 0

        self.initUI()


    def eventFilter(self, watched, event):
        pass

    
    def displayVerticalLayout(self, parent_layout, glabel, btn, second=None, third=None):
        grp_sub = QGroupBox(glabel)
        layout_sub = QVBoxLayout()
        grp_sub.setLayout(layout_sub)
        layout_sub.addWidget(btn)
        if second:
            layout_sub.addWidget(second)
        if third:
            layout_sub.addWidget(third)
        parent_layout.addWidget(grp_sub)


    def initUI(self):
        self.setWindowTitle("PSTEK MONITORING PROGRAM for SEMES")
        # self.setMinimumSize(1024, 800) 

        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width(), screensize.height()) 

        mainwidget = QWidget()                # 위젯의 인스턴스 생성만으로도 QMainWindow에 붙는다.
        self.setCentralWidget(mainwidget)

        ## Main Layout 설정
        self.main_layer = QVBoxLayout()
        mainwidget.setLayout(self.main_layer)

        bluefont = "color: #0000ff;"
        redfont = "color: #ff0000;"
        
        layout_setup = QHBoxLayout()
        self.main_layer.addLayout(layout_setup)
        
        ## replace #1st ###############
        # 전원 장치 연결
        self.btn_com_gen = QPushButton("CONNECT")
        self.btn_com_gen.clicked.connect(self.setting_device)
        self.label_gen_port = QLabel("N/A")
        self.label_ptype = QLabel("N/A")
        self.displayVerticalLayout(layout_setup, "Connect(232 9Pin)", 
                self.btn_com_gen, self.label_gen_port, self.label_ptype)
        
        group = QGroupBox("VOLTAGE & CURRENT CHANGE")
        layout = QVBoxLayout()
        group.setLayout(layout)

        self.vol_block = VoltageBlock(self, 'VOLTAGE', 2000, 0, 5000, 'V')
        group_vol = self.vol_block.display()
        layout.addWidget(group_vol)

        self.cur_block = CurrentBlock(self, 'CURRENT', 1.0, 0.1, 2.00, 'mA')
        group_cur = self.cur_block.display()
        layout.addWidget(group_cur)
        layout_setup.addWidget(group)

        ## TOGGLE
        self.btn_toggle = QPushButton("TOGGLE")
        self.btn_toggle.clicked.connect(self.device_toggle)
        self.displayVerticalLayout(layout_setup, "TOGGLE", self.btn_toggle)

        ## COMM ERROR 2024. 6. 20
        self.comm_check = Status(self, "COMM ERROR CHECK", "ENABLE", "DISABLE", "DISABLE")
        group_comm = self.comm_check.display()
        layout_setup.addWidget(group_comm)
        
        ## TIMER
        self.display_time = QLCDNumber()
        self.display_time.setDigitCount(8)
        self.display_time.setStyleSheet('background: white')
        timedisplay = '{:02d}:{:02d}:{:02d}'.format(
                (self.time // 60) // 60, (self.time // 60) % 60, self.time % 60)
        self.display_time.display(timedisplay)
        layout_setup.addWidget(self.display_time)

        # Logo Image
        labelLogo = QLabel("")
        pixmap = QPixmap(resource_path("logo.png"))
        labelLogo.setAlignment(Qt.AlignRight)
        labelLogo.setPixmap(pixmap)
        layout_setup.addWidget(labelLogo)
        self.setup_read_menu()


    def device_toggle(self):
        self.read_semes.device_toggle()

    def setup_read_menu(self):
        self.grp_read_semes = QGroupBox("SEMES : READ COMMAND")
        layout_semes = QVBoxLayout()
        self.grp_read_semes.setLayout(layout_semes)
        self.main_layer.addWidget(self.grp_read_semes)
        
        self.read_semes = ReadFunctionSEMES(self, 'Read SEMES')
        
        layout_semes.addWidget(self.read_semes.display())
        self.main_layer.addWidget(self.grp_read_semes)


    def change_comm_error_mode(self, cmd, period):
        self.read_semes.device_comm_error(cmd, period)


    # 전원 장치와 송신을 위한 설정
    def setting_device(self):
        if self.com_open_flag == False:
            Dialog = QDialog()
            self.com_open_flag == True
            dialog = SettingWin(Dialog)
            dialog.show()
            response = dialog.exec_()

            # OK 를 하면 설정 값을 읽어와서 통신을 한다.
            if response == QDialog.Accepted:
                if dialog.selected == SEMES:
                    gen_port = dialog.gen_port
                    com_speed = dialog.com_speed
                    com_data = dialog.com_data
                    self.ch_ptype = dialog.ch_ptype
                    self.com_open_flag = False

                    self.client = serial.Serial(
                        port=gen_port,
                        baudrate=com_speed,
                        parity=serial.PARITY_NONE,
                        stopbits=serial.STOPBITS_ONE,
                        bytesize=com_data,
                        timeout=2
                    )
                    self.ptype = SEMES
                    self.label_gen_port.setText(gen_port)
                    self.label_ptype.setText(self.ptype)
                    self.btn_com_gen.hide()
                    self.read_semes.enable_start()

                logging.info(f"setting_device :: {self.ptype} :: {self.client}")
                logging.info(f"setting_device :: self.ch_ptype :: {self.ch_ptype}")

        else:
            logging.info("Open Dialog")


if __name__ == "__main__":
    app = QApplication(sys.argv)
    form = MainWindow()
    form.show()
    sys.exit(app.exec_())