using System;
using System.IO.Ports;
using System.Collections.Generic;
using System.Threading;
using System.IO;
using System.Text;

namespace ModbusDataLogger
{
    class Program
    {
        // MODBUS RTU Slave 장치의 포트와 주소
        const string SERIAL_PORT = "/dev/cu.usbserial-AC024JHG";  // 포트명은 실제 사용하는 시스템에 맞게 수정해야 합니다.
        const int SERIAL_BAUDRATE = 115200;
        const Parity SERIAL_PARITY = Parity.None;
        const int SERIAL_DATABITS = 8;
        const StopBits SERIAL_STOPBITS = StopBits.One;
        const int SLAVE_ADDRESS = 1;

        // 데이터를 저장할 리스트
        static List<byte[]> dataList = new List<byte[]>();
        static List<double> timeList = new List<double>();

        static SerialPort serialPort;

        static void Main(string[] args)
        {
            Console.CancelKeyPress += delegate {
                // Ctrl+C 입력 시 종료
                Console.WriteLine("Exiting...");
                if (serialPort != null && serialPort.IsOpen)
                    serialPort.Close();
            };

            // 시리얼 포트 설정
            serialPort = new SerialPort(SERIAL_PORT, SERIAL_BAUDRATE, SERIAL_PARITY, SERIAL_DATABITS, SERIAL_STOPBITS);

            try
            {
                serialPort.Open();
                Console.WriteLine("Serial port opened successfully.");

                while (true)
                {
                    try
                    {
                        // 레지스터 값 읽기
                        string result = serialPort.ReadLine();
                        // 읽어온 데이터를 리스트에 추가
                        if (!string.IsNullOrEmpty(result))
                        {
                            timeList.Add(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
                            dataList.Add(Encoding.ASCII.GetBytes(result));

                            Console.WriteLine(DateTime.Now.ToString() + " " + result);

                            // 데이터 개수가 3000개 이상이면 파일에 저장
                            if (dataList.Count >= 3000)
                                SaveDataToFile();
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error :: " + e.Message);
                    }
                    Thread.Sleep(100);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error opening serial port: " + ex.Message);
            }
        }

        static void SaveDataToFile()
        {
            // 파일에 데이터 저장
            string file = $"asmk_{DateTime.Now:yyyy_MM_dd_HH_mm_ss}";

            List<string> tMarks = new List<string>();
            foreach (double tm in timeList)
            {
                DateTime tmDate = DateTimeOffset.FromUnixTimeSeconds((long)tm).DateTime.ToLocalTime();
                string tmString = tmDate.ToString("HH:mm:ss");
                tMarks.Add(tmString);
            }

            string filePath = Path.Combine("data", file + ".csv");

            using (StreamWriter writer = new StreamWriter(filePath))
            {
                writer.WriteLine("TIMES,DATAS");
                for (int i = 0; i < timeList.Count; i++)
                {
                    writer.WriteLine($"{tMarks[i]},{BitConverter.ToString(dataList[i]).Replace("-", " ")}");
                }
            }

            Console.WriteLine("Data saved to file: " + filePath);

            // 저장된 데이터 초기화
            dataList.Clear();
            timeList.Clear();
        }
    }
}
