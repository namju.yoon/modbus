import os
import sys
import pyqtgraph as pyGraph

from random import randint
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QLabel, QPushButton, QVBoxLayout, QDialogButtonBox, QLineEdit
from PyQt5.QtWidgets import QGroupBox, QSpinBox, QGridLayout, QCheckBox, QDialog, QHBoxLayout

from .zoom import GraphWindow

NORMAL = 0
WARNING = 1

class Field:
    grayfont = "color: #222;"
    bluefont = "color: #0000ff;"
    yellowfont = "color: #FFC300;font-weight: bold;"
    redfont = "color: #ff0000;font-weight: bold;"

    def __init__(self, labelname, status1, status2, value):
        self.status1 = status1 
        self.status2 = status2 
        self.value = value

        self.lbl_name = QLabel(labelname)
        self.rd_status1 = QCheckBox(self.status1)
        self.rd_status2 = QCheckBox(self.status2)

    def set_value(self, value):
        self.value = value


    def change_normal(self):
        self.value = NORMAL
        self.rd_status1.setChecked(True)
        self.rd_status1.setStyleSheet(Field.bluefont)

        self.rd_status2.setChecked(False)
        self.rd_status2.setStyleSheet(Field.grayfont)


    def change_warning(self):
        self.value = WARNING
        self.rd_status1.setChecked(False)
        self.rd_status1.setStyleSheet(Field.grayfont)

        self.rd_status2.setChecked(True)
        self.rd_status2.setStyleSheet(Field.redfont)


    def reset(self):
        self.rd_status1.setChecked(False)
        self.rd_status1.setStyleSheet(Field.grayfont)

        self.rd_status2.setChecked(False)
        self.rd_status2.setStyleSheet(Field.grayfont)

class Status:
    grayfont = "color: #222;"
    bluefont = "color: #0000ff;"
    yellowfont = "color: #FFC300;font-weight: bold;"
    redfont = "color: #ff0000;font-weight: bold;"

    def __init__(self, name, status1, status2):
        self.name = name
        self.cb_status1 = QCheckBox(status1)
        self.cb_status1.setStyleSheet(Status.grayfont)

        self.cb_status2 = QCheckBox(status2)
        self.cb_status2.setStyleSheet(Status.grayfont)

    def display(self):
        group = QGroupBox(self.name)
        layout = QHBoxLayout()
        group.setLayout(layout)

        layout.addWidget(self.cb_status1)
        layout.addWidget(self.cb_status2)
        
        return group

    def display2low(self):
        group = QGroupBox(self.name)
        layout = QGridLayout()
        group.setLayout(layout)

        layout.addWidget(self.cb_status1, 0, 0)
        layout.addWidget(self.cb_status2, 0, 1)
        
        return group

    def change_warning(self):
        self.cb_status1.setChecked(False)
        self.cb_status1.setStyleSheet(Status.grayfont)

        self.cb_status2.setChecked(True)
        self.cb_status2.setStyleSheet(Status.redfont)

    def change_status(self, val):
        try:
            if val == 0:
                self.cb_status1.setChecked(True)
                self.cb_status1.setStyleSheet(Status.grayfont)

                self.cb_status2.setChecked(False)
                self.cb_status2.setStyleSheet(Status.grayfont)
                
            elif val == 1:
                self.cb_status1.setChecked(False)
                self.cb_status1.setStyleSheet(Status.grayfont)

                self.cb_status2.setChecked(True)
                self.cb_status2.setStyleSheet(Status.redfont)

        except Exception as e:
            logging.debug(e)

class Module:
    cyanfont = "border: 3px solid lightgray;border-radius: 10px;background-color:#ccffff;color: black;font-size: 14px;font-weight: bold;"
    yellowfont = "border: 3px solid lightgray;border-radius: 10px;background-color: #FFC300;color: black;font-size: 14px;font-weight: bold;"
    redfont = "border: 3px solid lightgray;border-radius: 10px;background-color: #FF0000;color: black;font-size: 14px;font-weight: bold;"

    def __init__(self, name):
        self.name = name 
        self.fields = []

    def add_field(self, field):
        self.fields.append(field)

    def display(self):
        group = QGroupBox(self.name)
        # group.setStyleSheet("padding: 12px 0px 0px 0px")
        layout = QGridLayout()
        group.setLayout(layout)

        for idx, field in enumerate(self.fields):
            layout.addWidget(field.lbl_name, idx, 0)
            layout.addWidget(field.rd_status1, idx, 1)
            layout.addWidget(field.rd_status2, idx, 2)

        return group

    def update_warning(self, faults):
        for idx, val in enumerate(faults):
            if val:
                self.fields[idx].change_warning()

    def reset(self):
        for idx, field in enumerate(self.fields):
            field.reset()

    def __str__(self):
        return f"#### Module Name = {self.name}"

class WarningDialog(QDialog):
    def __init__(self, message):
        super().__init__()
        self.message = message
        self.setupUI()

    def setupUI(self):
        self.setGeometry(100, 100, 400, 200)

        self.setWindowTitle("Warning !!")
        layout = QVBoxLayout()
        self.setLayout(layout)
        self.label = QLabel(self.message)
        layout.addWidget(self.label)

        self.buttonBox = QDialogButtonBox()
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        layout.addWidget(self.buttonBox)

        self.buttonBox.accepted.connect(self.on_accepted)
        self.buttonBox.rejected.connect(self.reject)

    def on_accepted(self):
        print("WarningDialog :: OK")
        self.accept()

def ALed():
    btn = QPushButton()
    btn.setFixedSize(40, 40)
    style = 'QPushButton{border: 3px solid white;border-radius: 20px; background-color: gray;}'
    btn.setStyleSheet(style)
    return btn

def CLed(name, width, height, color):
    half = int(width/2)
    btn = QPushButton(name)
    btn.setFixedSize(width, height)
    style = f"border: 1px solid lightgray;border-radius: {half}px;background-color: {color};color: black;font-size: 14px;font-weight: bold;"
    btn.setStyleSheet(style)
    return btn

def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)

class MyLabel(QLabel):
    def __init__(self, *args, **kwargs):
        super(MyLabel, self).__init__(*args, **kwargs)
        self.setFixedHeight(15)
        self.setMaximumHeight(15)
        self.setStyleSheet('margin: -1px; padding: 0px; font-size: 14pt; color: blue;background-color: white')
        self.setAlignment(Qt.AlignRight | Qt.AlignVCenter)

class ChLabel(QLabel):
    def __init__(self, *args, **kwargs):
        super(ChLabel, self).__init__(*args, **kwargs)
        self.setFixedHeight(15)
        self.setMaximumHeight(15)
        self.setStyleSheet('margin: -1px; padding: 0px; font-size: 14pt; color: red;background-color: white')

class NumLabel(QSpinBox):
    def __init__(self, *args, **kwargs):
        super(NumLabel, self).__init__(*args, **kwargs)
        self.setFixedHeight(15)
        self.setMaximumHeight(15)
        self.setStyleSheet('margin: -1px; padding: 0px; font-size: 14pt; color: blue')
        self.setAlignment(Qt.AlignRight | Qt.AlignVCenter)

class AlarmBlock:
    def __init__(self, text, row, col, layout):
        super(AlarmBlock, self).__init__()
        self.text = text
        self.row = row
        self.col = col
        self.playout = layout
        self.aled = ALed()

        self.default = 'border: 3px solid white;border-radius: 20px; background-color: gray;'
        self.red = "border: 3px solid white;border-radius: 20px; background-color: red;"
        self.initUI()

    def initUI(self):
        text_block = QLabel(self.text)
        text_block.setStyleSheet("font-weight: bold; font-size: 24px")
        self.playout.addWidget(self.aled, self.row, self.col)
        self.playout.addWidget(text_block, self.row, self.col+1, 1, 2)

    def turnRed(self):
        self.aled.setStyleSheet(self.red)

    def resetAlarm(self):
        self.aled.setStyleSheet(self.default)


class SetVal(QWidget):
    def __init__(self, text, value, unit, layout, nth, minval=200, maxval=5000):
        super(SetVal, self).__init__()
        self.text = text
        self.value = value 
        self.unit = unit 
        self.minval = minval 
        self.maxval = maxval 
        # self.setGeometry(0, 0, 300, 100)
        text_block = QLabel(self.text)
        unit_block = QLabel(self.unit)

        self.edit_block = QSpinBox()
        self.edit_block.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self.edit_block.setSingleStep(100)
        self.edit_block.setStyleSheet('margin: -1px; padding: 0px; color: red')
        self.edit_block.setRange(self.minval, self.maxval)
        self.edit_block.setValue(self.value)

        layout.addWidget(text_block, nth, 0)
        layout.addWidget(self.edit_block, nth, 1)
        layout.addWidget(unit_block, nth, 2)

    def save_data(self):
        print(f"write_data : {self.address} : {self.edit_block.value()}")

    def get_val(self):
        return self.edit_block.value()



class DataBlock(QWidget):
    def __init__(self, text, address, value, 
                minval, maxval, unit, layout, nth):
        super(DataBlock, self).__init__()
        self.text = text
        self.address = address 
        self.value = value 
        self.minval = minval 
        self.maxval = maxval 
        self.unit = unit
        self.playout = layout
        self.nth = nth 
        self.initUI()

    def initUI(self):
        # self.setGeometry(0, 0, 300, 100)
        text_block = QLabel(self.text)
        unit_block = QLabel(self.unit)

        self.edit_block = QSpinBox()
        self.edit_block.setStyleSheet('margin: -1px; padding: 0px; color: red')
        self.edit_block.setRange(self.minval, self.maxval)
        self.edit_block.setValue(self.value)
        range_value = f"{self.minval} ~ {self.maxval}"
        range_block = QLabel(range_value)
        self.btn_block = QPushButton("변 경")
        self.btn_block.setStyleSheet("color: rgb(58, 134, 255);"
                    "background-color: white;"
                      "border-style: solid;"
                      "border-width: 3px;"
                      "border-radius: 20px")
        self.btn_block.clicked.connect(self.save_data)

        self.playout.addWidget(text_block, self.nth, 0)
        self.playout.addWidget(self.edit_block, self.nth, 1)
        self.playout.addWidget(range_block, self.nth, 2)
        self.playout.addWidget(unit_block, self.nth, 3)
        self.playout.addWidget(self.btn_block, self.nth, 4)

    def save_data(self):
        print(f"write_data : {self.address} : {self.edit_block.value()}")

class DataGroupBlock(QWidget):
    def __init__(self, parent, text, address, value, 
                unit, layout, nth, vmin=0, vmax=1000):
        super(DataGroupBlock, self).__init__()
        self.parent = parent
        self.address = address
        self.value = value 

        black = 'QLabel{font-size: 12pt; font-weight: bold}'
        gray = 'QLabel{font-size: 12pt; font-weight: bold; color: gray;}'
        btncolor = "QPushButton{font-size: 12pt; font-weight: bold; color: blue}"

        text_block = QLabel(text)
        text_block.setStyleSheet(black)
        unit_block = QLabel(unit)
        unit_block.setStyleSheet(black)

        self.edit_block = QSpinBox()
        self.edit_block.setStyleSheet('margin: -1px;font-size: 14pt;padding: 0px; color: red')
        self.edit_block.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self.edit_block.setRange(vmin, vmax)
        self.edit_block.setValue(self.value)
        
        self.btn_block = QPushButton("변 경")
        self.btn_block.setStyleSheet(btncolor)
        self.btn_block.clicked.connect(self.settingValue)

        layout.addWidget(text_block, nth, 0, 1, 3)
        layout.addWidget(self.edit_block, nth, 3, 1, 2)
        layout.addWidget(unit_block, nth, 5)
        layout.addWidget(self.btn_block, nth, 6)

    def setValue(self, val):
        self.value = val
        self.edit_block.setValue(val)

    def settingValue(self):
        val = self.edit_block.value()
        # print("settingValue : ", val)
        self.parent.settingValue(self.address, val)

class DataGroupBlockRange(QWidget):
    def __init__(self, parent, text, address, value, 
                unit, layout, nth, vmin=0, vmax=1000):
        super(DataGroupBlockRange, self).__init__()
        self.parent = parent
        self.address = address
        self.value = value 

        black = 'QLabel{font-size: 12pt; font-weight: bold}'
        gray = 'QLabel{font-size: 12pt; font-weight: bold; color: gray;}'
        btncolor = "QPushButton{font-size: 12pt; font-weight: bold; color: blue}"

        text_block = QLabel(text)
        text_block.setStyleSheet(black)
        unit_block = QLabel(unit)
        unit_block.setStyleSheet(black)
        rtxt_block = QLabel('min ~ max')
        range_block = QLabel(f'{vmin} ~ {vmax}')

        self.edit_block = QSpinBox()
        self.edit_block.setStyleSheet('margin: -1px;font-size: 14pt;padding: 0px; color: red')
        self.edit_block.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self.edit_block.setRange(vmin, vmax)
        self.edit_block.setValue(self.value)
        
        self.btn_block = QPushButton("변 경")
        self.btn_block.setStyleSheet(btncolor)
        self.btn_block.clicked.connect(self.modifyDeviceValue)

        layout.addWidget(text_block, nth, 0, 1, 3)
        layout.addWidget(self.edit_block, nth, 3, 1, 2)
        layout.addWidget(unit_block, nth, 5)
        layout.addWidget(self.btn_block, nth, 6)
        layout.addWidget(rtxt_block, nth, 7)
        layout.addWidget(range_block, nth, 8)

    def setValue(self, val):
        self.value = val
        self.edit_block.setValue(val)

    def modifyDeviceValue(self):
        val = self.edit_block.value()
        print("modifyDeviceValue : ", val)
        self.parent.modifyDeviceValue(self.address, val)

class DataGroupBlockNoButton(QWidget):
    def __init__(self, parent, text, address, value, 
                unit, layout, nth):
        super(DataGroupBlockNoButton, self).__init__()
        self.parent = parent
        self.title = text 
        self.address = address 
        self.value = value
        self.value_list = [value]

        black = 'QLabel{font-size: 12pt; font-weight: bold}'
        btncolor = "QPushButton{font-size: 12pt; font-weight: bold; color: blue}"

        text_block = QLabel(text)
        text_block.setStyleSheet(black)
        unit_block = QLabel(unit)
        unit_block.setStyleSheet(black)

        self.line_edit = QLineEdit()
        self.line_edit.setStyleSheet('margin: -1px;background-color: #ddd;font-size: 14pt;font-weight: bold;padding: 0px; color: blue')
        self.line_edit.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self.line_edit.setText(str(self.value))

        # self.btn_block = QPushButton("Graph")
        # self.btn_block.setStyleSheet(btncolor)
        # self.btn_block.clicked.connect(self.displayGraph)

        layout.addWidget(text_block, nth, 0, 1, 3)
        layout.addWidget(self.line_edit, nth, 3, 1, 2)
        layout.addWidget(unit_block, nth, 5)
        # layout.addWidget(self.btn_block, nth, 6)

    def setValue(self, val):
        self.value_list.append(val)
        self.line_edit.setText(str(val))

    def displayGraph(self):
        indexs = list(range(len(self.value_list)))
        Dialog = QDialog()
        dialog = GraphWindow(Dialog, self.title, (255, 0, 0), indexs, self.value_list)
        dialog.show()
        response = dialog.exec_()
        if response == QDialog.Accepted or response == QDialog.Rejected:
            self.graph_flag = False

class DataBtnBlock(QWidget):
    def __init__(self, text, address,
                layout):
        super(DataGroupBlock, self).__init__()
        self.text = text
        self.address = address 
        self.playout = layout

        self.initUI()

    def initUI(self):
        black = 'QLabel{font-size: 20pt; font-weight: bold}'
        gray = 'QLabel{font-size: 20pt; font-weight: bold; color: gray;}'
        btncolor = "QPushButton{font-size: 20pt; font-weight: bold; color: blue}"

        grp = QGroupBox("")
        layout = QGridLayout()
        grp.setLayout(layout)

        text_block = QLabel(self.text)
        text_block.setStyleSheet(black)
        unit_block = QLabel(self.unit)
        unit_block.setStyleSheet(black)

        self.edit_block = QSpinBox()
        self.edit_block.setStyleSheet('margin: -1px;font-size: 20pt;padding: 0px; color: red')
        self.edit_block.setRange(self.minval, self.maxval)
        self.edit_block.setValue(self.value)
        range_value = f"{self.minval} ~ {self.maxval}"
        range_block = QLabel(range_value)
        range_block.setStyleSheet(gray)
        self.btn_block = QPushButton("변 경")
        self.btn_block.setStyleSheet(btncolor)
        self.btn_block.clicked.connect(self.save_data)

        layout.addWidget(text_block, 0, 0, 1, 5)
        layout.addWidget(self.edit_block, 0, 5)
        layout.addWidget(range_block, 0, 6)
        layout.addWidget(unit_block, 0, 7)
        layout.addWidget(self.btn_block, 0, 8)

        self.playout.addWidget(grp)

    def save_data(self):
        print(f"write_data : {self.address} : {self.edit_block.value()}")

class SimpleBlock(QWidget):
    def __init__(self, client, text, address, value, 
                layout, nth, unit=None):
        super(SimpleBlock, self).__init__()
        self.client = client
        self.text = text
        self.address = address 
        self.value = value 
        self.playout = layout
        self.nth = nth 
        self.unit = unit 

        self.initUI()

    def initUI(self):
        black = 'QLabel{font-size: 16pt; font-weight: bold}'
        btncolor = "QPushButton{font-size: 16pt; font-weight: bold; color: blue}"
        text_block = QLabel(self.text)
        text_block.setStyleSheet(black)
        
        if self.unit:
            unit = QLabel(self.unit)
            unit.setStyleSheet(black)

        self.edit_block = QSpinBox()
        self.edit_block.setRange(-999, 999)
        self.edit_block.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self.edit_block.setStyleSheet('margin: -1px; font-size: 16pt;padding: 0px; color: red')
        self.edit_block.setValue(self.value)

        self.btn_block = QPushButton("변 경")
        self.btn_block.setStyleSheet(btncolor)
        self.btn_block.clicked.connect(self.calibration)

        self.playout.addWidget(text_block, self.nth, 0, 1, 2)
        self.playout.addWidget(self.edit_block, self.nth, 2)
        if self.unit:
            self.playout.addWidget(unit, self.nth, 3)
        self.playout.addWidget(self.btn_block, self.nth, 4)


    def update(self, client, value):
        # print("Update : ", client, value)
        self.client = client
        self.edit_block.setValue(value)


    def calibration(self):
        print(f"device calibration : {self.address} : {self.edit_block.value()}")
        value = self.edit_block.value()
        if value <= 0:
            value = value & 0xffff
        device.write_registers(self.client, self.address, value)

class SimpleDisplay(QWidget):
    def __init__(self, text, unit, value, 
                layout, nth):
        super(SimpleDisplay, self).__init__()
        self.text = text
        self.unit = unit
        self.value = value 
        self.playout = layout
        self.nth = nth 
        self.black = 'QLabel{font-size: 16pt;}'
        self.initUI()

    def initUI(self):
        text_block = QLabel(self.text)
        text_block.setStyleSheet(self.black)

        self.edit_block = QLabel()
        self.edit_block.setStyleSheet('margin: -1px; padding: 0px; font-size: 16pt; color: blue')
        self.edit_block.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self.edit_block.setText(str(self.value))

        label_unit = QLabel(self.unit)

        self.playout.addWidget(text_block, self.nth, 0)
        self.playout.addWidget(self.edit_block, self.nth, 1, 1, 3)
        self.playout.addWidget(label_unit, self.nth, 4)

    def setValue(self, value):
        self.value = value 

    def update(self):
        self.edit_block.setText(str(self.value))

class DataView:
    def __init__(self, title, x, y, maxval, color):
        self.title = title
        self.x = x
        self.y = y
        self.maxval = maxval
        self.color = color

        self.btn = QPushButton(self.title)
        self.btn.setEnabled(False)
        self.btn.setStyleSheet(f"background-color: white;color: {self.color}; font-size: 14px;font-weight: bold;")

        self.btn1 = QPushButton(self.title)
        self.btn1.setEnabled(False)
        self.btn1.setStyleSheet(f"background-color: white;color: {self.color}; font-size: 18px;font-weight: bold;")

        self.pw = pg.PlotWidget()
        self.pw.getPlotItem().hideAxis('bottom')
        self.pw.showGrid(x=False, y=True)
        self.pw.setXRange(0, 0.5, padding=0)   # padding=0  --> y축 공백 제거함.
        self.pw.setYRange(0, self.maxval, padding=0)   # padding=0  --> x축 공백 제거함.
        self.barchar = pg.BarGraphItem(x=self.x, height=self.y, width=1.0, brush=self.color)
        self.pw.addItem(self.barchar)

        self.label = QLabel(str(y))
        self.label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.label.setStyleSheet("QLabel{font-size: 20pt;}")

        self.pen = pg.mkPen(width=2, color=color)
        self.graph = pg.PlotWidget()
        self.graph.setBackground('w')
        self.graph_plot =  self.graph.plot(self.x, 
                        self.y, pen=self.pen, name=title)

    def setY(self, count, val):
        self.label.setText(str(val))
        self.x = np.append(self.x, count)
        self.y = np.append(self.y, val)

    def reDraw(self, val):
        self.barchar = pg.BarGraphItem(x=[0], height=[self.maxval], width=0.9, brush='w')
        self.pw.addItem(self.barchar)
        self.pw.showGrid(x=False, y=True)
        self.barchar = pg.BarGraphItem(x=[0], height=[val], width=0.9, brush=self.color)
        self.pw.addItem(self.barchar)

    def drawPlot(self):
        self.graph_plot = self.graph.plot(self.x, 
                        self.y, pen=self.pen, name=self.title)
        
    def mousePressEvent(self, ev):
        print(ev)

## PulseDC 
class LineView:
    def __init__(self, title, x, y, color):
        self.title = title
        self.x = x
        self.y = y
        self.color = color
        self.pen = pyGraph.mkPen(width=1, color=color)

        self.btn = QPushButton(self.title)
        self.btn.setEnabled(False)
        self.btn.setStyleSheet(f"background-color: white;color: {self.color}; font-size: 14px;font-weight: bold;")

        self.plotWidget = pyGraph.PlotWidget()
        self.plotWidget.setBackground('w')

        self.axisA = self.plotWidget.plotItem
        self.axisA.setLabels(left=title)
        self.axisA.showAxis('right')
        self.graph = self.axisA.plot(self.x, self.y, pen=self.pen, name=self.title)

    def reDraw(self, x, y):
        # print("reDraw !!!!", x, y)
        self.x = x
        self.y = y
        self.graph = self.axisA.plot(self.x, self.y, pen=self.pen, name=self.title)

     
    def mousePressEvent(self, ev):
        print(ev)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = DataBlock("OUTPUT POWER", 1, 42, 0, 42.00, "KW", 0)
    ex.show()
    sys.exit(app.exec_())




