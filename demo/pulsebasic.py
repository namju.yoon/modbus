import numpy as np
import pandas as pd
from random import randint, choices

from PyQt5.QtWidgets import QGroupBox, QHBoxLayout, QGridLayout, QLabel, QDialog
from .block import SimpleDisplay, DataView, MyLabel, ChLabel, Field, QPushButton
from .zoom import GraphWindow

NAMES = [
    "VDC OVP HW",
    "IDC OVP HW",
    "VDC OVP1 SW",
    "IDC OCP1 SW",
    "VDC OVP2 SW",
    "IDC OCP2 SW",
    "RO MIN 1",
    "RO MIN 2",
    "VDC UVP1 SW",
    "VDC UVP2 SW",
]


class Channel:
    def __init__(self, name):
        self.name = name 

        self.label_status = Field('Status', '정상', '경고', 0)
        self.label_fault = ChLabel("")

        self.po_list = [0]
        self.po_btn = QLabel("PO")
        # self.po_btn.clicked.connect(lambda:self.displayGraph("PO"))
        self.po_label = MyLabel()

        self.vo_list = [0]
        self.vo_btn = QLabel("VO")
        # self.vo_btn.clicked.connect(lambda:self.displayGraph("VO"))
        self.vo_label = MyLabel()

        self.io_list = [0]
        self.io_btn = QLabel("IO")
        # self.io_btn.clicked.connect(lambda:self.displayGraph("IO"))
        self.io_label = MyLabel()

        self.v24_list = [0]
        self.v24_btn = QLabel("V24")
        # self.v24_btn.clicked.connect(lambda:self.displayGraph("V24"))
        self.v24_label = MyLabel()

        self.i24_list = [0]
        self.i24_btn = QLabel("I24")
        # self.i24_btn.clicked.connect(lambda:self.displayGraph("I24"))
        self.i24_label = MyLabel()

        self.duty_list = [0] # us
        self.duty_btn = QLabel("DUTY")
        # self.duty_btn.clicked.connect(lambda:self.displayGraph("DUTY"))
        self.duty_label = MyLabel()

        self.ro_list = [0] # Ohm
        self.ro_btn = QLabel("RO")
        # self.ro_btn.clicked.connect(lambda:self.displayGraph("RO"))
        self.ro_label = MyLabel()

        self.grp_channel = QGroupBox(self.name)
        self.layout_channel = QGridLayout()
        self.grp_channel.setLayout(self.layout_channel)


    def status_normal(self):
        self.label_status.change_normal()


    def change_channel(self, dchannel):
        try:
            # self.label_status.change_normal()
            self.fault = dchannel[1]
            if dchannel[1]:
                self.label_status.change_warning()
                self.label_fault.setText(str(dchannel[1]))

            self.po_list.append(dchannel[2])
            self.po_label.setText(str(dchannel[2]))

            self.vo_list.append(dchannel[3])
            self.vo_label.setText(str(dchannel[3]))

            self.io_list.append(dchannel[4])
            self.io_label.setText(str(dchannel[4]))

            self.v24_list.append(dchannel[5])
            self.v24_label.setText(str(dchannel[5]))

            self.i24_list.append(dchannel[6])
            self.i24_label.setText(str(dchannel[6]))

            self.duty_list.append(dchannel[7])
            self.duty_label.setText(str(dchannel[7]))

            self.ro_list.append(dchannel[8])
            self.ro_label.setText(str(dchannel[8]))
        except Exception as e:
            print(e)


    def display(self):
        self.displayLoutField(0, self.label_status)
        self.displayLout(1, QLabel('Fault'), self.label_fault)
        self.displayLout(2, self.po_btn, self.po_label, QLabel('kW'))
        self.displayLout(3, self.vo_btn, self.vo_label, QLabel('V'))
        self.displayLout(4, self.io_btn, self.io_label, QLabel('mA'))
        self.displayLout(5, self.v24_btn, self.v24_label, QLabel('mV'))
        self.displayLout(6, self.i24_btn, self.i24_label, QLabel('mA'))
        self.displayLout(7, self.duty_btn, self.duty_label, QLabel('us'))
        self.displayLout(8, self.ro_btn, self.ro_label, QLabel('Ohm'))
        return self.grp_channel


    def displayLout(self, nth, label, value, unit=''):
        if unit:
            self.layout_channel.addWidget(label, nth, 0)
            self.layout_channel.addWidget(value, nth, 1, 1, 2)
            self.layout_channel.addWidget(unit, nth, 3)
        else:
            self.layout_channel.addWidget(label, nth, 0)
            self.layout_channel.addWidget(value, nth, 1, 1, 3)


    def displayLoutField(self, nth, field):
        self.layout_channel.addWidget(field.lbl_name, nth, 0, 1, 2)
        self.layout_channel.addWidget(field.rd_status1, nth, 2)
        self.layout_channel.addWidget(field.rd_status2, nth, 3)


    def displayGraph(self, ptype):
        if ptype == 'PO':
            vals = self.po_list
            title = "Power"
        elif ptype == 'VO':
            vals = self.vo_list
            title = "Voltage"
        elif ptype == 'IO':
            vals = self.io_list
            title = "Current"
        elif ptype == 'V24':
            vals = self.v24_list
            title = "V24 Voltage"
        elif ptype == 'I24':
            vals = self.i24_list
            title = "I24 Current"
        elif ptype == 'DUTY':
            vals = self.duty_list
            title = "Primary DUTY"
        elif ptype == 'RO':
            vals = self.ro_list
            title = "RO"
            
        indexs = list(range(len(vals)))
        Dialog = QDialog()
        dialog = GraphWindow(Dialog, title, (255, 0, 0), indexs, vals)
        dialog.show()
        response = dialog.exec_()
        if response == QDialog.Accepted or response == QDialog.Rejected:
            self.graph_flag = False


    def update_last(self):
        self.po_label.setText(str(self.po_list[-1]))
        self.vo_label.setText(str(self.vo_list[-1]))
        self.io_label.setText(str(self.io_list[-1]))
        self.v24_label.setText(str(self.v24_list[-1]))
        self.i24_label.setText(str(self.i24_list[-1]))
        self.duty_label.setText(str(self.duty_list[-1]))
        self.ro_label.setText(str(self.ro_list[-1]))



class PulseModule:
    def __init__(self, title, bcolor="#fff"):
        self.title = title
        self.ch1 = Channel('Channel #1')
        self.ch2 = Channel('Channel #2')
        self.bcolor = bcolor
        self.main_label = "QLabel{font-size: 14pt; font-weight: bold}"


    def initUI(self):
        grp_module = QGroupBox(self.title)
        layout_module = QHBoxLayout()
        grp_module.setStyleSheet(f"background-color: {self.bcolor}")
        grp_module.setLayout(layout_module)

        ch_group1 = self.ch1.display()
        layout_module.addWidget(ch_group1)
        ch_group2 = self.ch2.display()
        layout_module.addWidget(ch_group2)

        return grp_module

    def active_normal(self):
        self.ch1.status_normal()
        self.ch2.status_normal()

    def update_module(self, mod):
        self.ch1.change_channel(mod[:9])
        self.ch2.change_channel(mod[9:])

    def update_last(self):
        self.ch1.update_last()
        self.ch2.update_last()



class SlowChannel:
    def __init__(self, name):
        self.name = name 

        self.po_list = [0]
        self.po_btn = QPushButton("S PO")
        self.po_btn.clicked.connect(lambda:self.displayGraph("PO"))
        self.po_label = MyLabel()

        self.vo_list = [0]
        self.vo_btn = QPushButton("S VO")
        self.vo_btn.clicked.connect(lambda:self.displayGraph("VO"))
        self.vo_label = MyLabel()

        self.io_list = [0]
        self.io_btn = QPushButton("S IO")
        self.io_btn.clicked.connect(lambda:self.displayGraph("IO"))
        self.io_label = MyLabel()

        self.grp_channel = QGroupBox(self.name)
        self.layout_channel = QGridLayout()
        self.grp_channel.setLayout(self.layout_channel)

    
    def change_channel(self, dchannel):
        try:
            self.vo_list.append(dchannel[0])
            self.vo_label.setText(str(dchannel[0]))

            self.io_list.append(dchannel[1])
            self.io_label.setText(str(dchannel[1]))

            self.po_list.append(dchannel[2])
            self.po_label.setText(str(dchannel[2]))

        except Exception as e:
            print(e)


    def display(self):
        self.displayLout(0, self.vo_btn, self.vo_label, QLabel('V'))
        self.displayLout(1, self.io_btn, self.io_label, QLabel('mA'))
        self.displayLout(2, self.po_btn, self.po_label, QLabel('kW'))
        return self.grp_channel


    def displayLout(self, nth, label, value, unit=''):
        if unit:
            self.layout_channel.addWidget(label, nth, 0)
            self.layout_channel.addWidget(value, nth, 1, 1, 2)
            self.layout_channel.addWidget(unit, nth, 3)
        else:
            self.layout_channel.addWidget(label, nth, 0)
            self.layout_channel.addWidget(value, nth, 1, 1, 3)


    def displayGraph(self, ptype):
        if ptype == 'PO':
            vals = self.po_list
            title = "Power"
        elif ptype == 'VO':
            vals = self.vo_list
            title = "Voltage"
        elif ptype == 'IO':
            vals = self.io_list
            title = "Current"
        
            
        indexs = list(range(len(vals)))
        Dialog = QDialog()
        dialog = GraphWindow(Dialog, title, (255, 0, 0), indexs, vals)
        dialog.show()
        response = dialog.exec_()
        if response == QDialog.Accepted or response == QDialog.Rejected:
            self.graph_flag = False

    def update_last(self):
        self.po_label.setText(str(self.po_list[-1]))
        self.vo_label.setText(str(self.vo_list[-1]))
        self.io_label.setText(str(self.io_list[-1]))


class PulseModuleAdd:
    def __init__(self, title, bcolor="#fff"):
        self.title = title
        
        self.ch1 = SlowChannel('Channel #1')
        self.ch2 = SlowChannel('Channel #2')
        self.bcolor = bcolor
        self.main_label = "QLabel{font-size: 14pt; font-weight: bold}"

        self.v24_list = [0]
        self.v24_btn = QPushButton("S V24")
        self.v24_btn.clicked.connect(lambda:self.displayGraph("V24"))
        self.v24_label = MyLabel()

        self.i24_list = [0]
        self.i24_btn = QPushButton("S I24")
        self.i24_btn.clicked.connect(lambda:self.displayGraph("I24"))
        self.i24_label = MyLabel()


    def initUI(self):
        grp_module = QGroupBox(self.title)
        layout_module = QGridLayout()
        grp_module.setStyleSheet(f"background-color: {self.bcolor}")
        grp_module.setLayout(layout_module)

        ch_group1 = self.ch1.display()
        layout_module.addWidget(ch_group1, 0, 0)

        ch_group2 = self.ch2.display()
        layout_module.addWidget(ch_group2, 0, 1)

        self.grp_common = QGroupBox("Common")
        self.layout_common = QGridLayout()
        self.grp_common.setLayout(self.layout_common)

        self.displayLout(0, self.v24_btn, self.v24_label, QLabel('V'))
        self.displayLout(1, self.i24_btn, self.i24_label, QLabel('mA'))

        layout_module.addWidget(self.grp_common, 1, 0, 1, 2)

        return grp_module

    def displayLout(self, nth, label, value, unit=''):
        if unit:
            self.layout_common.addWidget(label, nth, 0)
            self.layout_common.addWidget(value, nth, 1, 1, 2)
            self.layout_common.addWidget(unit, nth, 3)
        else:
            self.layout_common.addWidget(label, nth, 0)
            self.layout_common.addWidget(value, nth, 1, 1, 3)


    def active_normal(self):
        self.ch1.status_normal()
        self.ch2.status_normal()

    def update_module(self, mod):
        self.ch1.change_channel(mod[:3])
        self.ch2.change_channel(mod[3:6])

        self.v24_list.append(mod[6])
        self.v24_label.setText(str(mod[6]))

        self.i24_list.append(mod[7])
        self.i24_label.setText(str(mod[7]))


    def displayGraph(self, ptype):
        if ptype == 'V24':
            title = "Slow V24 Voltage"
            vals = self.v24_list
        elif ptype == 'I24':
            title = "Slow I24 Current"
            vals = self.i24_list

        indexs = list(range(len(vals)))
        Dialog = QDialog()
        dialog = GraphWindow(Dialog, title, (255, 0, 0), indexs, vals)
        dialog.show()
        response = dialog.exec_()
        if response == QDialog.Accepted or response == QDialog.Rejected:
            self.graph_flag = False

    def update_last(self):
        self.ch1.update_last()
        self.ch2.update_last()
        self.v24_label.setText(str(self.v24_list[-1]))
        self.i24_label.setText(str(self.i24_list[-1]))



