###########################################################
   START SHIPPING
###########################################################
<PyQt5.QtWidgets.QRadioButton object at 0x0000029D9F58AE50> COM18
COM18
Possibel Port ::  ['COM3', 'COM4']
COM4
All data meeted
COM4 rtu 115200 8 N 1
******************* DUN SUCCESS ********************* ModbusSerialClient(rtu baud[115200])

Possibel Port ::  ['COM3']
All data meeted
******************* DUN SUCCESS ********************* ModbusSerialClient(rtu baud[115200])

read_product_serial ::  202206020240
check_password ModbusSerialClient(rtu baud[115200]) 1999 43981
select_calibration ::  ['VO_IO', 'IR_LEAK', 'VBIAS', 'CAP', 'ZCS', 'AOUT', 'D_IN']

####  VOLTAGE1OFFSET  START ####

VOLTAGE1OFFSET  ::: FWD VAL :      2493 REV VAL :       -2495  ****************>  -2
VOLTAGE1OFFSET :: 0th, OLD DIFF = 0, CUR DIFF = 2,  NEW VALUE : 271 :: 2

VOLTAGE1OFFSET  ::: FWD VAL :      2492 REV VAL :       -2493  ****************>  -1
**************************************************
***** :: OFFSET finish value :  271
**************************************************

####  VOLTAGE1GAIN  START ####

VOLTAGE1GAIN  ::: ESC VAL :      2499 JIG VAL :       2491  ******************       8
VOLTAGE1GAIN :: 0th, OLD DIFF = 0, CUR DIFF = 8,  NEW VALUE : 94 :: 8

VOLTAGE1GAIN  ::: ESC VAL :      2499 JIG VAL :       2496  ******************       3
VOLTAGE1GAIN :: 1th, OLD DIFF = 8, CUR DIFF = 3,  NEW VALUE : 91 :: 3

VOLTAGE1GAIN  ::: ESC VAL :      2499 JIG VAL :       2497  ******************       2
VOLTAGE1GAIN :: 2th, OLD DIFF = 3, CUR DIFF = 2,  NEW VALUE : 89 :: 2

VOLTAGE1GAIN  ::: ESC VAL :      2499 JIG VAL :       2498  ******************       1
**************************************************
***** :: :: GAIN finish value :  89
**************************************************

####  VOLTAGE2OFFSET  START ####

VOLTAGE2OFFSET  ::: FWD VAL :      -2508 REV VAL :       2501  ****************>  7
VOLTAGE2OFFSET :: 0th, OLD DIFF = 0, CUR DIFF = 7,  NEW VALUE : -18 :: 7

VOLTAGE2OFFSET  ::: FWD VAL :      -2510 REV VAL :       2499  ****************>  11
DIR CHANGE MINUS  11 7
VOLTAGE2OFFSET :: 1th, OLD DIFF = 7, CUR DIFF = 11,  NEW VALUE : -23 :: 11

VOLTAGE2OFFSET  ::: FWD VAL :      -2508 REV VAL :       2500  ****************>  8
VOLTAGE2OFFSET :: 2th, OLD DIFF = 11, CUR DIFF = 8,  NEW VALUE : -31 :: 8

VOLTAGE2OFFSET  ::: FWD VAL :      -2506 REV VAL :       2501  ****************>  5
VOLTAGE2OFFSET :: 3th, OLD DIFF = 8, CUR DIFF = 5,  NEW VALUE : -36 :: 5

VOLTAGE2OFFSET  ::: FWD VAL :      -2505 REV VAL :       2502  ****************>  3
VOLTAGE2OFFSET :: 4th, OLD DIFF = 5, CUR DIFF = 3,  NEW VALUE : -39 :: 3

VOLTAGE2OFFSET  ::: FWD VAL :      -2504 REV VAL :       2503  ****************>  1
**************************************************
***** :: OFFSET finish value :  -39
**************************************************

####  VOLTAGE2GAIN  START ####

VOLTAGE2GAIN  ::: ESC VAL :      -2500 JIG VAL :       -2504  ******************       -4
VOLTAGE2GAIN :: 0th, OLD DIFF = 0, CUR DIFF = 4,  NEW VALUE : 32 :: 4

VOLTAGE2GAIN  ::: ESC VAL :      -2500 JIG VAL :       -2500  ******************       0
**************************************************
***** :: :: GAIN finish value :  32
**************************************************

####  CURRENT1OFFSET  START ####

CURRENT1OFFSET  ::: FWD VAL :      2000 REV VAL :       1952  ****************>  48
CURRENT1OFFSET :: 0th, OLD DIFF = 0, CUR DIFF = 48,  NEW VALUE : -37 :: 24

CURRENT1OFFSET  ::: FWD VAL :      1977 REV VAL :       1975  ****************>  2
CURRENT1OFFSET :: 1th, OLD DIFF = 48, CUR DIFF = 2,  NEW VALUE : -38 :: 2

CURRENT1OFFSET  ::: FWD VAL :      1976 REV VAL :       1976  ****************>  0
**************************************************
***** :: OFFSET finish value :  -38
**************************************************

####  CURRENT1GAIN  START ####

CURRENT1GAIN  ::: ESC VAL :      1976 JIG VAL :       1988  ******************       -12
CURRENT1GAIN :: 0th, OLD DIFF = 0, CUR DIFF = 12,  NEW VALUE : 129 :: 6

CURRENT1GAIN  ::: ESC VAL :      1978 JIG VAL :       1988  ******************       -10
CURRENT1GAIN :: 1th, OLD DIFF = 12, CUR DIFF = 10,  NEW VALUE : 139 :: 10

CURRENT1GAIN  ::: ESC VAL :      1980 JIG VAL :       1988  ******************       -8
CURRENT1GAIN :: 2th, OLD DIFF = 10, CUR DIFF = 8,  NEW VALUE : 147 :: 8

CURRENT1GAIN  ::: ESC VAL :      1982 JIG VAL :       1988  ******************       -6
CURRENT1GAIN :: 3th, OLD DIFF = 8, CUR DIFF = 6,  NEW VALUE : 153 :: 6

CURRENT1GAIN  ::: ESC VAL :      1984 JIG VAL :       1989  ******************       -5
CURRENT1GAIN :: 4th, OLD DIFF = 6, CUR DIFF = 5,  NEW VALUE : 158 :: 5

CURRENT1GAIN  ::: ESC VAL :      1985 JIG VAL :       1988  ******************       -3
CURRENT1GAIN :: 5th, OLD DIFF = 5, CUR DIFF = 3,  NEW VALUE : 159 :: 3

CURRENT1GAIN  ::: ESC VAL :      1985 JIG VAL :       1988  ******************       -3
CURRENT1GAIN :: 6th, OLD DIFF = 3, CUR DIFF = 3,  NEW VALUE : 160 :: 3

CURRENT1GAIN  ::: ESC VAL :      1986 JIG VAL :       1988  ******************       -2
CURRENT1GAIN :: 7th, OLD DIFF = 3, CUR DIFF = 2,  NEW VALUE : 161 :: 2

CURRENT1GAIN  ::: ESC VAL :      1986 JIG VAL :       1988  ******************       -2
CURRENT1GAIN :: 8th, OLD DIFF = 2, CUR DIFF = 2,  NEW VALUE : 162 :: 2

CURRENT1GAIN  ::: ESC VAL :      1986 JIG VAL :       1988  ******************       -2
CURRENT1GAIN :: 9th, OLD DIFF = 2, CUR DIFF = 2,  NEW VALUE : 163 :: 2

CURRENT1GAIN  ::: ESC VAL :      1987 JIG VAL :       1988  ******************       -1
**************************************************
***** :: :: GAIN finish value :  163
**************************************************

####  CURRENT2OFFSET  START ####

CURRENT2OFFSET  ::: FWD VAL :      1939 REV VAL :       1991  ****************>  -52
CURRENT2OFFSET :: 0th, OLD DIFF = 0, CUR DIFF = 52,  NEW VALUE : 13 :: 26

CURRENT2OFFSET  ::: FWD VAL :      1914 REV VAL :       2015  ****************>  -101
DIR CHANGE MINUS  101 52
CURRENT2OFFSET :: 1th, OLD DIFF = 52, CUR DIFF = 101,  NEW VALUE : -37 :: 101

CURRENT2OFFSET  ::: FWD VAL :      1961 REV VAL :       1968  ****************>  -7
CURRENT2OFFSET :: 2th, OLD DIFF = 101, CUR DIFF = 7,  NEW VALUE : -44 :: 7

CURRENT2OFFSET  ::: FWD VAL :      1968 REV VAL :       1962  ****************>  6
CURRENT2OFFSET :: 3th, OLD DIFF = 7, CUR DIFF = 6,  NEW VALUE : -42 :: 6

CURRENT2OFFSET  ::: FWD VAL :      1966 REV VAL :       1964  ****************>  2
CURRENT2OFFSET :: 4th, OLD DIFF = 6, CUR DIFF = 2,  NEW VALUE : -41 :: 2

CURRENT2OFFSET  ::: FWD VAL :      1965 REV VAL :       1964  ****************>  1
**************************************************
***** :: OFFSET finish value :  -41
**************************************************

####  CURRENT2GAIN  START ####

CURRENT2GAIN  ::: ESC VAL :      1965 JIG VAL :       1984  ******************       -19
CURRENT2GAIN :: 0th, OLD DIFF = 0, CUR DIFF = 19,  NEW VALUE : 122 :: 9

CURRENT2GAIN  ::: ESC VAL :      1968 JIG VAL :       1984  ******************       -16
CURRENT2GAIN :: 1th, OLD DIFF = 19, CUR DIFF = 16,  NEW VALUE : 138 :: 16

CURRENT2GAIN  ::: ESC VAL :      1971 JIG VAL :       1984  ******************       -13
CURRENT2GAIN :: 2th, OLD DIFF = 16, CUR DIFF = 13,  NEW VALUE : 151 :: 13

CURRENT2GAIN  ::: ESC VAL :      1975 JIG VAL :       1983  ******************       -8
CURRENT2GAIN :: 3th, OLD DIFF = 13, CUR DIFF = 8,  NEW VALUE : 159 :: 8

CURRENT2GAIN  ::: ESC VAL :      1977 JIG VAL :       1984  ******************       -7
CURRENT2GAIN :: 4th, OLD DIFF = 8, CUR DIFF = 7,  NEW VALUE : 166 :: 7

CURRENT2GAIN  ::: ESC VAL :      1979 JIG VAL :       1983  ******************       -4
CURRENT2GAIN :: 5th, OLD DIFF = 7, CUR DIFF = 4,  NEW VALUE : 170 :: 4

CURRENT2GAIN  ::: ESC VAL :      1980 JIG VAL :       1984  ******************       -4
CURRENT2GAIN :: 6th, OLD DIFF = 4, CUR DIFF = 4,  NEW VALUE : 171 :: 4

CURRENT2GAIN  ::: ESC VAL :      1979 JIG VAL :       1984  ******************       -5
CURRENT2GAIN :: 7th, OLD DIFF = 4, CUR DIFF = 5,  NEW VALUE : 173 :: 5

CURRENT2GAIN  ::: ESC VAL :      1981 JIG VAL :       1984  ******************       -3
CURRENT2GAIN :: 8th, OLD DIFF = 5, CUR DIFF = 3,  NEW VALUE : 174 :: 3

CURRENT2GAIN  ::: ESC VAL :      1981 JIG VAL :       1984  ******************       -3
CURRENT2GAIN :: 9th, OLD DIFF = 3, CUR DIFF = 3,  NEW VALUE : 175 :: 3

CURRENT2GAIN  ::: ESC VAL :      1981 JIG VAL :       1984  ******************       -3
CURRENT2GAIN :: 10th, OLD DIFF = 3, CUR DIFF = 3,  NEW VALUE : 176 :: 3

CURRENT2GAIN  ::: ESC VAL :      1981 JIG VAL :       1984  ******************       -3
CURRENT2GAIN :: 11th, OLD DIFF = 3, CUR DIFF = 3,  NEW VALUE : 177 :: 3

CURRENT2GAIN  ::: ESC VAL :      1981 JIG VAL :       1984  ******************       -3
CURRENT2GAIN :: 12th, OLD DIFF = 3, CUR DIFF = 3,  NEW VALUE : 178 :: 3

CURRENT2GAIN  ::: ESC VAL :      1982 JIG VAL :       1983  ******************       -1
**************************************************
***** :: :: GAIN finish value :  178
**************************************************
Exception ERROR :  duplicate key value violates unique constraint "converter_esccalivalue_serialNo_key"
DETAIL:  Key ("serialNo")=(202206020240) already exists.

Manually Calibration VCS, ICS
WarningDialog :: OK
check_password ModbusSerialClient(rtu baud[115200]) 1999 43981
##################################################
### IR LEAK START ########
##################################################

####  IRLEAK1OFFSET  START ####

IRLEAK1OFFSET  ::: FWD VAL :      1410 REV VAL :       1415  ****************>  -5
IRLEAK1OFFSET :: 0th, OLD DIFF = 0, CUR DIFF = 5,  NEW VALUE : -105 :: 2

IRLEAK1OFFSET  ::: FWD VAL :      1411 REV VAL :       1415  ****************>  -4
IRLEAK1OFFSET :: 1th, OLD DIFF = 5, CUR DIFF = 4,  NEW VALUE : -101 :: 4

IRLEAK1OFFSET  ::: FWD VAL :      1411 REV VAL :       1413  ****************>  -2
IRLEAK1OFFSET :: 2th, OLD DIFF = 4, CUR DIFF = 2,  NEW VALUE : -99 :: 2

IRLEAK1OFFSET  ::: FWD VAL :      1412 REV VAL :       1413  ****************>  -1
**************************************************
***** :: OFFSET finish value :  -99
**************************************************

####  IRLEAK1GAIN  START ####

IRLEAK1GAIN  ::: ESC VAL :      1414 JIG VAL :       1406  ******************       8
IRLEAK1GAIN :: 0th, OLD DIFF = 0, CUR DIFF = 8,  NEW VALUE : 43 :: 4

IRLEAK1GAIN  ::: ESC VAL :      1414 JIG VAL :       1406  ******************       8
IRLEAK1GAIN :: 1th, OLD DIFF = 8, CUR DIFF = 8,  NEW VALUE : 51 :: 8

IRLEAK1GAIN  ::: ESC VAL :      1418 JIG VAL :       1406  ******************       12
DIR CHANGE MINUS  12 8
IRLEAK1GAIN :: 2th, OLD DIFF = 8, CUR DIFF = 12,  NEW VALUE : 45 :: 12

IRLEAK1GAIN  ::: ESC VAL :      1415 JIG VAL :       1406  ******************       9
IRLEAK1GAIN :: 3th, OLD DIFF = 12, CUR DIFF = 9,  NEW VALUE : 36 :: 9

IRLEAK1GAIN  ::: ESC VAL :      1409 JIG VAL :       1406  ******************       3
IRLEAK1GAIN :: 4th, OLD DIFF = 9, CUR DIFF = 3,  NEW VALUE : 33 :: 3

IRLEAK1GAIN  ::: ESC VAL :      1406 JIG VAL :       1406  ******************       0
**************************************************
***** :: :: GAIN finish value :  33
**************************************************

####  IRLEAK2OFFSET  START ####

IRLEAK2OFFSET  ::: FWD VAL :      1428 REV VAL :       1432  ****************>  -4
IRLEAK2OFFSET :: 0th, OLD DIFF = 0, CUR DIFF = 4,  NEW VALUE : 20 :: 2

IRLEAK2OFFSET  ::: FWD VAL :      1427 REV VAL :       1435  ****************>  -8
DIR CHANGE MINUS  8 4
IRLEAK2OFFSET :: 1th, OLD DIFF = 4, CUR DIFF = 8,  NEW VALUE : 16 :: 8

IRLEAK2OFFSET  ::: FWD VAL :      1429 REV VAL :       1433  ****************>  -4
IRLEAK2OFFSET :: 2th, OLD DIFF = 8, CUR DIFF = 4,  NEW VALUE : 12 :: 4

IRLEAK2OFFSET  ::: FWD VAL :      1430 REV VAL :       1432  ****************>  -2
IRLEAK2OFFSET :: 3th, OLD DIFF = 4, CUR DIFF = 2,  NEW VALUE : 10 :: 2

IRLEAK2OFFSET  ::: FWD VAL :      1429 REV VAL :       1431  ****************>  -2
IRLEAK2OFFSET :: 4th, OLD DIFF = 2, CUR DIFF = 2,  NEW VALUE : 8 :: 2

IRLEAK2OFFSET  ::: FWD VAL :      1429 REV VAL :       1431  ****************>  -2
IRLEAK2OFFSET :: 5th, OLD DIFF = 2, CUR DIFF = 2,  NEW VALUE : 6 :: 2

IRLEAK2OFFSET  ::: FWD VAL :      1430 REV VAL :       1430  ****************>  0
**************************************************
***** :: OFFSET finish value :  6
**************************************************

####  IRLEAK2GAIN  START ####

IRLEAK2GAIN  ::: ESC VAL :      1428 JIG VAL :       1416  ******************       12
IRLEAK2GAIN :: 0th, OLD DIFF = 0, CUR DIFF = 12,  NEW VALUE : 55 :: 6

IRLEAK2GAIN  ::: ESC VAL :      1432 JIG VAL :       1416  ******************       16
DIR CHANGE MINUS  16 12
IRLEAK2GAIN :: 1th, OLD DIFF = 12, CUR DIFF = 16,  NEW VALUE : 47 :: 16

IRLEAK2GAIN  ::: ESC VAL :      1428 JIG VAL :       1416  ******************       12
IRLEAK2GAIN :: 2th, OLD DIFF = 16, CUR DIFF = 12,  NEW VALUE : 35 :: 12

IRLEAK2GAIN  ::: ESC VAL :      1421 JIG VAL :       1416  ******************       5
IRLEAK2GAIN :: 3th, OLD DIFF = 12, CUR DIFF = 5,  NEW VALUE : 30 :: 5

IRLEAK2GAIN  ::: ESC VAL :      1418 JIG VAL :       1417  ******************       1
**************************************************
***** :: :: GAIN finish value :  30
**************************************************
select_calibration ::  ['VBIAS', 'CAP', 'ZCS', 'AOUT', 'D_IN']
check_password ModbusSerialClient(rtu baud[115200]) 1999 43981
##################################################
##### VBIAS OFFSET ######
##################################################
     ### VBIAS OFFSET :: FORWARD VAL : 130 REVERSE VAL : -127
Start Value :  264
VBIAOFFSET :: 0th :: FWD :: 130 :: REV :: -127 :: NEW GAIN :: 263 :: 1 :: abs(DIFF) :: 3

     ### VBIAS OFFSET :: FORWARD VAL : 130 REVERSE VAL : -127
Start Value :  263
VBIAOFFSET :: 0th :: FWD :: 130 :: REV :: -127 :: NEW GAIN :: 262 :: 1 :: abs(DIFF) :: 3

     ### VBIAS OFFSET :: FORWARD VAL : 130 REVERSE VAL : -127
Start Value :  262
VBIAOFFSET :: 0th :: FWD :: 130 :: REV :: -127 :: NEW GAIN :: 261 :: 1 :: abs(DIFF) :: 3

     ### VBIAS OFFSET :: FORWARD VAL : 130 REVERSE VAL : -127
Start Value :  261
VBIAOFFSET :: 0th :: FWD :: 130 :: REV :: -127 :: NEW GAIN :: 260 :: 1 :: abs(DIFF) :: 3

     ### VBIAS OFFSET :: FORWARD VAL : 130 REVERSE VAL : -127
Start Value :  260
VBIAOFFSET :: 0th :: FWD :: 130 :: REV :: -127 :: NEW GAIN :: 259 :: 1 :: abs(DIFF) :: 3

 STOP ALL DEVICE
