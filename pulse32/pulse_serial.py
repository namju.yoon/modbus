import time
import logging
import serial
from sys import platform
from random import randint, choices

import socket
from umodbus import conf
from umodbus.client import tcp
from ctypes import c_uint16


## RTU
PTYPE = 'RTU'
# PTYPE = 'tcp'

if PTYPE == 'tcp':
    PORT = 1502
    PTYPE = 'tcp'

UNIT = 0x1
END_VALUE = 0x00ed

FAULT_ADDRESS = 0x00
PULSE_PO = 0x10
SET_POWER = 0x24

MODULE_DATA_1 = 0x35 ## 26개 ## 53
MODULE_DATA_2 = 0x4f ## 26개 ## 79
MODULE_DATA_3 = 0x69 ## 26개 ## 105
MODULE_DATA_4 = 0x83 ## 26개 ## 131
MODULE_DATA_5 = 0x9d ## 26개 ## 157

SLOW_DATA_1 = 0xb7 ## 183 
# SLOW_DATA_2 = 0x147 ## 
# SLOW_DATA_3 = 0x155
# SLOW_DATA_4 = 0x163
# SLOW_DATA_5 = 0x171

SLOW_V24 = 0xd5 ## 213

# ZVS_MOD_1 = 0x17f
# ZVS_MOD_2 = 0x18f
# ZVS_MOD_3 = 0x19f
# ZVS_MOD_4 = 0x1af
# ZVS_MOD_5 = 0x1bf


POWER_ONOFF = 0x00


from pymodbus.constants import Defaults
Defaults.RetryOnEmpty = True
Defaults.Timeout = 5
Defaults.Retries = 3

# from pyModbusTCP.client import ModbusClient
# client = ModbusClient(host="192.168.10.10", port=5000, unit_id=1, auto_open=True)
# regs = client.read_input_registers(5, 10)


def connect_rtu(port, ptype='rtu', speed=38400, bytesize=8, parity='N', stopbits=1):
    from pymodbus.client.sync import ModbusSerialClient as ModbusClient
    client = ModbusClient(method=ptype, port=port, timeout=1,
                      baudrate=speed, bytesize=bytesize, parity=parity, stopbits=stopbits)
    client.connect()

    if client:
        logging.debug('*'*50)
        logging.debug(f"******************* RTU DUN {client} SUCCESS ********************* ")
        logging.debug('*'*50)

    return client

def connect_tcp(ipaddress='192.168.10.100', ipport=5000):
    conf.SIGNED_VALUES = True
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect((ipaddress, ipport))

    if client:
        logging.debug('#'*50)
        logging.debug(f"******************* TCP {client} SUCCESS *********************")
        logging.debug('#'*50)

    return client


def make_qrport(port):
    client = serial.Serial(port)
    return client


# Device Main Data 읽어오기 
def read_data_from_device(client, ptype):
    data = {}
    if ptype == 'RTU':
        try:
            setting = read_registers(client, ptype, PULSE_PO, 37)
            # logging.info(f"setting :: {setting}")
            if setting:
                data['setting'] = setting
                
                mod01 = read_registers(client, ptype, MODULE_DATA_1, 78)
                # logging.info(f"mod01 :: {mod01}")
                data['mod01'] = mod01
                
                # mod02 = read_registers(client, ptype, MODULE_DATA_2, 26)
                # data['mod02'] = mod02
                # mod03 = read_registers(client, ptype, MODULE_DATA_3, 26)
                # data['mod03'] = mod03

                mod04 = read_registers(client, ptype, MODULE_DATA_4, 52)
                data['mod04'] = mod04
                # mod05 = read_registers(client, ptype, MODULE_DATA_5, 26)
                # data['mod05'] = mod05

                slow_data = read_registers(client, ptype, SLOW_DATA_1, 40)
                # logging.info(f"slow_data :: {slow_data}")
                data['slow_data'] = slow_data

                # slow_v24 = read_registers(client, ptype, SLOW_V24, 10)
                # # logging.info(f"slow_v24 :: {slow_v24}")
                # data['slow_v24'] = slow_v24

                data['working'] = True 
                
            else:
                data['working'] = False
                data['error'] = setting['result']
            return data

        except Exception as e:
            logging.warning(f"** RTU :: Error read_input_registers : {e}")
    else:
        try:
            message = tcp.read_input_registers(slave_id=1, 
                        starting_address=READ_VOLTAGE1, quantity=10)
            result = tcp.send_message(message, client)

        except Exception as e:
            logging.warning(f"## TCP :: Error read_input_registers : {e}")

    return data


def read_setting_from_device(client, ptype):
    data = {}
    if ptype == 'RTU':
        try:
            setting = read_registers(client, ptype, PULSE_PO, 37)
            # print(setting)
            if setting:
                data['working'] = True 
                data['setting'] = setting
            else:
                data['working'] = False
            return data

        except Exception as e:
            logging.warning(f"** RTU :: Error read_input_registers : {e}")
            data['working'] = False
    else:
        try:
            message = tcp.read_input_registers(slave_id=1, 
                        starting_address=READ_VOLTAGE1, quantity=10)
            result = tcp.send_message(message, client)

        except Exception as e:
            logging.warning(f"## TCP :: Error read_input_registers : {e}")

    return data


def read_registers(client, ptype, address, count):
    try:
        response = client.read_input_registers(address, count, unit=UNIT)
        result = response.registers

        # # Simulation
        # result = []
        # temp_range = range(200, 303)
        # for idx in range(count):
        #     divmo = idx % 13
        #     if divmo in [9, 10, 11, 12]:
        #         result.append(choices(temp_range)[0])
        #     else:
        #         num = randint(1, 1000)
        #         result.append(num)         

    except Exception as e:
        logging.warning(f"** RTU :: Error read_input_registers : {e}")
        result = []
    return result


def read_fault_from_device(client, ptype):
    if ptype == 'RTU':
        try:
            response = client.read_input_registers(FAULT_ADDRESS, 12, unit=UNIT)
            result = response.registers

            # # simulation
            # result = []
            # faults = [1, 2, 4, 8, 16, 32, 64]
            # for idx in range(12):
            #     num = randint(1, 256)
            #     if num % 19 == 0:
            #         result.append(choices(faults)[0])
            #     else:
            #         result.append(0)

            return result
        except Exception as e:
            logging.warning(f"** RTU :: Error read_fault_from_device : {e}")
            return []
    else:
        try:
            message = tcp.read_input_registers(slave_id=1, 
                        starting_address=READ_VOLTAGE1, quantity=10)
            result = tcp.send_message(message, client)
            return result
        except Exception as e:
            print("## TCP :: Error read_input_registers : ", e)
            return []

def standBy(client, ptype):
    logging.debug("standBy : POWER_ON :: ", 1, "\n\n")
    client.write_registers(POWER_ONOFF, 2, unit=UNIT)

def standOff(client, ptype):
    logging.debug("standOff : POWER_OFF :: ", 0, "\n\n")
    client.write_registers(POWER_ONOFF, 0, unit=UNIT)


# Write Register
def write_registers(client, address, val, ptype):
    logging.debug("WRITE : ", address, val, ptype, "\n\n")
    if ptype == 'RTU':
        try:
            value = c_uint16(val).value
            result = client.write_registers(address, value, unit=UNIT)
        except Exception as e:
            result = []
            logging.warning(f"Error write_registers : {e}")
    else:
        message = tcp.write_single_register(slave_id=1, 
                    address=address, value=val)
        result = tcp.send_message(message, client)

    return result


# Read FeedBack Data
def read_feedback(client, address):
    try:
        result = client.read_input_registers(address, 1, unit=UNIT)
        if result.function_code >= 0x80:
            logging.warning("Error Happened : ", )
        response = result.registers
    except Exception as e:
        response = []
        logging.warning("Error read_input_registers : ", e)
    return response

