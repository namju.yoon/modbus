from PyQt5.QtWidgets import QWidget, QLabel, QTextEdit, QPushButton, QDesktopWidget
from PyQt5.QtWidgets import QGroupBox, QBoxLayout, QVBoxLayout, QHBoxLayout, QDialogButtonBox
from PyQt5.QtWidgets import QApplication, QDialog, QStatusBar, QFileDialog, QRadioButton
from PyQt5 import QtCore
from PyQt5.QtCore import QDate, Qt
from PyQt5.QtGui import QPixmap, QFont
import sys
import serial

GRAPH = 1
DATA = 2

# print(dir(serial))
def check_stopbit(x): 
    return {'7 Bit': 7, '8 Bit': 8, '1 Bit': 1, '2 Bit': 2, '9600': 9600, '19200': 19200, '38400': 38400, '57600':57600, '115200': 115200, 'NONE': serial.PARITY_NONE, 'ODD': serial.PARITY_ODD, 'EVEN': serial.PARITY_EVEN, 'MARK': serial.PARITY_MARK, 'SPACE': serial.PARITY_SPACE,}[x]

def get_comm_port():
    port_list = []
    import serial.tools.list_ports
    ports = serial.tools.list_ports.comports()
    for port, desc, hwid in sorted(ports):
        if desc != 'n/a':
            port_list.append(port)
    return port_list


class SettingWin(QDialog):
    def __init__(self, Dialog):
        super().__init__()
        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        self.setGeometry(100, 100, 400, 200)
        font = QFont()
        font.setPointSize(12)

        self.gen_port = None
        self.com_speed = 115200
        self.com_data = 8
        self.com_parity = serial.PARITY_NONE
        self.com_stop = 1
        self.selected = 'RTU'
        self.graph_type = GRAPH

    # Main 
        main_layer = QVBoxLayout()
        self.setLayout(main_layer)

    # 통신 포트  설정(Serial Port)"
        grp_port = QGroupBox("통신 포트  설정(Serial Port)")
        grp_port.setFont(font)

        layout_port = QHBoxLayout()

        ports = get_comm_port()
        if ports:
            if len(ports) == 1:
                port = ports[0]
                self.rd_port_1 = QRadioButton(port)
                self.rd_port_1.setChecked(True)
                self.gen_port = port
                self.rd_port_1.clicked.connect(lambda:self.radioPortClicked(self.rd_port_1))
                layout_port.addWidget(self.rd_port_1)
            elif len(ports) == 2:
                port = ports[0]
                self.rd_port_1 = QRadioButton(port)
                self.rd_port_1.setChecked(True)
                self.gen_port = port
                self.rd_port_1.clicked.connect(lambda:self.radioPortClicked(self.rd_port_1))
                layout_port.addWidget(self.rd_port_1)

                port = ports[1]
                self.rd_port_2 = QRadioButton(port)
                self.rd_port_2.clicked.connect(lambda:self.radioPortClicked(self.rd_port_2))
                layout_port.addWidget(self.rd_port_2)
            else:
                print("Too many")
        else:
            self.label_result = QLabel("현재 사용 가능한 포트가 없습니다. 연결 선을 USB에 연결하세요.")
            layout_port.addWidget(self.label_result)

        grp_port.setLayout(layout_port)
        main_layer.addWidget(grp_port)

    # 전송속도 (Baud Rate)"
        grp_speed = QGroupBox("전송속도 (Baud Rate)")
        grp_speed.setFont(font)

        layout_speed = QHBoxLayout()

        self.rd_speed_384 = QRadioButton("38400")
        # self.com_speed = 38400
        self.rd_speed_384.clicked.connect(lambda:self.radioValueClicked(self.rd_speed_384, "SPEED"))
        # self.rd_speed_384.setChecked(True)
        layout_speed.addWidget(self.rd_speed_384)

        self.rd_speed_576 = QRadioButton("57600")
        self.rd_speed_576.clicked.connect(lambda:self.radioValueClicked(self.rd_speed_576, "SPEED"))
        layout_speed.addWidget(self.rd_speed_576)

        self.rd_speed_1152 = QRadioButton("115200")
        self.com_speed = 115200
        self.rd_speed_1152.setChecked(True)
        self.rd_speed_1152.clicked.connect(lambda:self.radioValueClicked(self.rd_speed_1152, "SPEED"))
        layout_speed.addWidget(self.rd_speed_1152)

        grp_speed.setLayout(layout_speed)
        main_layer.addWidget(grp_speed)


        self.buttonBox = QDialogButtonBox()
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        main_layer.addWidget(self.buttonBox)

        self.buttonBox.accepted.connect(self.on_accepted)
        self.buttonBox.rejected.connect(self.reject)


    def on_accepted(self):
        if self.gen_port and self.com_speed and self.com_data and self.com_parity and self.com_stop:
            print("All data meeted")
            self.accept()
        else:
            print("COM PORT를 선택하시요.")
        
    def radioPortClicked(self, btn):
        result = btn.text()
        self.gen_port = result

    def radioValueClicked(self, btn, ptype):
        result = check_stopbit(btn.text())
        if ptype == "SPEED":
            self.com_speed = result
        elif ptype == "DATA":
            self.com_data = result
        elif ptype == "PARITY":
            self.com_parity = result
        elif ptype == "STOP":
            self.com_stop = result

            

if __name__ == "__main__":
    app = QApplication(sys.argv)
    Dialog = QDialog()
    mywindow = SettingWin(Dialog)
    mywindow.show()
    app.exec_()