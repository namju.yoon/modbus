#!/usr/bin/env python
# coding: utf-8

# 예제 내용
# * 기본 위젯을 사용하여 기본 창을 생성
# * 다양한 레이아웃 위젯 사용
import os
import sys
import pandas as pd
import time
import json, codecs
from datetime import datetime

from PyQt5.QtWidgets import QWidget, QLabel, QPushButton, QDesktopWidget, QMainWindow
from PyQt5.QtWidgets import QGroupBox, QVBoxLayout, QHBoxLayout, QGridLayout, QLineEdit
from PyQt5.QtWidgets import QApplication, QDialog, QStatusBar, QFileDialog, QCheckBox, QLCDNumber
from PyQt5 import QtCore
from PyQt5.QtCore import QDate, Qt
from PyQt5.QtGui import QPixmap
import pyqtgraph as pyGraph

from esc import semes as semes
from esc import scpicommon as scpi

from esc.esc_setup import SettingWin
from esc.esc_params import ESCParams, ESCParams1U, ESCParamsSCPI, ESCParamsSEMES
import esc.esc_serial as device
from esc.zoom import GraphWindow, ZoomWindow, InspectWindow

from ctypes import c_int16

GRAPH = 1
DATA = 2

RTU = 'RTU'
SCPI = 'SCPI'
SEMES = 'SEMES'
TCP = 'TCP'

# SAVE_COUNT = 720 # 30초 * 60분 * 24시간
# SAVE_COUNT = 100
SAVE_COUNT = 3 * 60 * 60


import logging
FORMAT = ('%(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.INFO)

def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)

class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()

        if not os.path.exists("data"):
            logging.info("CTEATE :: data/")
            os.makedirs("data") 

        self.used_ports = []
        self.toggle = device.FORWARD

        self.faultmessage = {
            0 : "Fault01-0 : FAULT LEAK CURRENT OVER  ",
            1 : "Fault01-1 : FAULT HW OUT CURRENT OVER1  ",
            2 : "Fault01-2 : FAULT HW OUT CURRENT OVER2  ",
            3 : "Fault01-3 : FAULT HW OUT VOLTAGE OVER1  ",
            4 : "Fault01-4 : FAULT HW OUT VOLTAGE OVER2  ",
            5 : "Fault01-5 : FAULT EXT INTERLOCK  ",
            6 : "Fault01-6 : FAULT OUT CURRENT OVER1  ",
            7 : "Fault01-7 : FAULT OUT CURRENT OVER2  ",
            8 : "Fault01-8 : FAULT OUT VOLTAGE OVER1  ",
            9 : "Fault01-9 : FAULT OUT VOLTAGE OVER2  ",
            10: "Fault01-10 : FAULT COMMUNICATION ERROR  ",
            11: "Fault01-11 : FAULT TOO MANY ARC  ",
            12: "Fault01-12 : FAULT RO MIN   ",
            13: "Fault01-13 : RESONANT CURRENT OVER1  ",
            14: "Fault01-14 : RESONANT CURRENT OVER2  ",
        }

        self.stimer = None
        self.dtimer = None
        self.faulttimer = None
        self.toogletimer = None
        self.runstoptimer = None

        self.com_setting_flag = False
        self.com_open_flag = False
        self.run_flag = False
        self.ndp_flag = False
        self.channelnum = True

        self.voltage1 = 2000
        self.RUNSTOP_TIME = 5000

        ## 2022-02-14
        ## data only, or graph 
        self.graph_type = GRAPH

        self.client1 = None
        self.client2 = None
        # self.client1 = 1

        self.ptype = RTU
        self.qr_client = None
        self.gen_port = None

        self.start_time = None

        self.graph_flag = False
        self.countN = 0
        self.countN2 = 0

        self.data = {}
        self.time = 0

        self.view_vol_11 = True
        self.view_cur_11 = False
        self.view_vol_12 = True
        self.view_cur_12 = False
        self.view_cps_1 = True

        self.indexs = []
        self.tmarks = []

        self.vol_11s = []
        self.cur_11s = []
        self.vol_12s = []
        self.cur_12s = []
        self.cpss_1 = []

        self.vol_21s = []
        self.cur_21s = []
        self.vol_22s = []
        self.cur_22s = []
        self.cpss_2 = []

        self.pen_vol1 = pyGraph.mkPen(width=2, color=(255, 0, 0))
        self.pen_cur1 = pyGraph.mkPen(width=2, color=(0, 0, 255))
        self.pen_vol2 = pyGraph.mkPen(width=2, color=(255, 191, 0))
        self.pen_cur2 = pyGraph.mkPen(width=2, color=(26, 175, 51))
        self.pen_vbia = pyGraph.mkPen(width=2, color=(0,0,128))
        self.pen_cps = pyGraph.mkPen(width=2, color=(108, 52, 131))
        self.pen_leak_cur1 = pyGraph.mkPen(width=2, color=(204, 0, 153))
        self.pen_leak_cur2 = pyGraph.mkPen(width=2, color=(102, 0, 204))

        self.temp_indexs = list(range(100))

        self.initUI()

    def eventFilter(self, watched, event):
        pass

    def displayUnitLayout(self, parent_layout, btn, cbx, label, unit):
        grp_sub = QGroupBox("")
        layout_sub = QGridLayout()
        grp_sub.setLayout(layout_sub)

        label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        unit.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)

        layout_sub.addWidget(btn, 0, 0, 1, 3)
        layout_sub.addWidget(cbx, 0, 4)
        layout_sub.addWidget(label, 1, 0, 1, 3)
        layout_sub.addWidget(unit, 1, 4)
        parent_layout.addWidget(grp_sub)
        return grp_sub

    def displayHorizontalLayout(self, parent_layout, glabel, btn, second=None, third=None):
        grp_sub = QGroupBox(glabel)
        layout_sub = QHBoxLayout()
        grp_sub.setLayout(layout_sub)
        layout_sub.addWidget(btn)
        if second:
            layout_sub.addWidget(second)
        if third:
            layout_sub.addWidget(third)
        parent_layout.addWidget(grp_sub)

    def displayVerticalLayout(self, parent_layout, glabel, btn, second=None, third=None):
        grp_sub = QGroupBox(glabel)
        layout_sub = QVBoxLayout()
        grp_sub.setLayout(layout_sub)
        layout_sub.addWidget(btn)
        if second:
            layout_sub.addWidget(second)
        if third:
            layout_sub.addWidget(third)
        parent_layout.addWidget(grp_sub)

    def displayGridLayout(self, parent_layout, glabel, edit1, label1, edit2, label2, edit3=None, label3=None):
        grp_sub = QGroupBox(glabel)
        layout_sub = QGridLayout()
        grp_sub.setLayout(layout_sub)

        layout_sub.addWidget(edit1, 0, 0)
        layout_sub.addWidget(label1, 0, 1)
        layout_sub.addWidget(edit2, 1, 0)
        layout_sub.addWidget(label2, 1, 1)

        if edit3:
            layout_sub.addWidget(edit3, 2, 0)
            layout_sub.addWidget(label3, 2, 1)

        parent_layout.addWidget(grp_sub)

    def initUI(self):
        self.setWindowTitle("PSTEK ELECTROSTATIC CHUCK MONITORING for ASMK")
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width(), screensize.height()) 

        mainwidget = QWidget()                # 위젯의 인스턴스 생성만으로도 QMainWindow에 붙는다.
        self.setCentralWidget(mainwidget)

        ## Main Layout 설정
        self.main_layer = QVBoxLayout()
        mainwidget.setLayout(self.main_layer)

        bluefont = "color: #0000ff;"
        redfont = "color: #ff0000;"
        
        layout_setup = QHBoxLayout()
        self.main_layer.addLayout(layout_setup)
        
        ## replace #1st ###############
        # 전원 장치 연결
        self.btn_com_gen1 = QPushButton("CONNECT #1")
        self.btn_com_gen1.clicked.connect(self.setting_device1)
        self.label_gen_port1 = QLabel("N/A")
        self.label_ptype1 = QLabel("N/A")
        self.displayVerticalLayout(layout_setup, "Connect(485/ModBus)", 
                self.btn_com_gen1, self.label_gen_port1, self.label_ptype1)
        
        ## CONNECT #2nd ###############
        self.btn_com_gen2 = QPushButton("CONNECT #2")
        self.btn_com_gen2.clicked.connect(self.setting_device2)
        self.label_gen_port2 = QLabel("N/A")
        self.label_ptype2 = QLabel("N/A")
        self.displayVerticalLayout(layout_setup, "Connect(485/ModBus)", 
                self.btn_com_gen2, self.label_gen_port2, self.label_ptype2)
        
        # 단말기 ON/OFF
        self.btn_esc_run = QPushButton("ON")
        self.btn_esc_run.setEnabled(False)
        self.btn_esc_run.setStyleSheet(bluefont)
        self.btn_esc_run.clicked.connect(self.esc_device_run)

        self.btn_esc_stop = QPushButton("OFF")
        self.btn_esc_stop.setStyleSheet(redfont)
        self.btn_esc_stop.clicked.connect(self.esc_device_stop)
        self.btn_esc_stop.setEnabled(False)
        self.displayVerticalLayout(layout_setup, "ON/OFF", self.btn_esc_run, self.btn_esc_stop)

        # GET DATA
        self.btn_datastart = QPushButton("DATA START")
        # self.btn_datastart.setEnabled(False)
        self.btn_datastart.setStyleSheet(bluefont)
        self.btn_datastart.clicked.connect(self.data_pullstart)

        self.btn_datastop = QPushButton("DATA STOP")
        self.btn_datastop.setEnabled(False)
        self.btn_datastop.setStyleSheet(bluefont)
        self.btn_datastop.clicked.connect(self.data_pullstop)

        self.displayVerticalLayout(layout_setup, "DATA", self.btn_datastart, self.btn_datastop)
        

        # 단말기 WRITE
        self.btn_params = QPushButton("MODIFY PARAMS")
        self.btn_params.setEnabled(False)
        self.btn_params.clicked.connect(self.device_params_modify)
        self.auto_runstop = QCheckBox("Auto RUN/STOP")
        self.auto_runstop.setChecked(True)
        self.displayVerticalLayout(layout_setup, "Parameters", self.btn_params, self.auto_runstop)

        # Toggle 기능 
        self.btn_toggle = QPushButton("TOGGLE")
        self.btn_toggle.setEnabled(False)
        self.btn_toggle.clicked.connect(self.device_toggle)
        self.auto_toggle = QCheckBox("Auto Toggle")
        self.displayVerticalLayout(layout_setup, "TOGGLE", self.btn_toggle, self.auto_toggle)

        # Toggle 기능 
        self.btn_num = QLineEdit("30")
        label1 = QLabel("초 (데이터저장)")
        self.toogle_time = QLineEdit("10")
        label2 = QLabel("초 (토글 시간)")
        self.runstop_time = QLineEdit("30")
        label3 = QLabel("분 (RUN/STOP 시간)")
        self.displayGridLayout(layout_setup, "데이터횟수", self.btn_num, label1, self.toogle_time, label2, self.runstop_time, label3)

        # Logo Image
        labelLogo = QLabel("")
        pixmap = QPixmap(resource_path("logo.png"))
        labelLogo.setAlignment(Qt.AlignRight)
        labelLogo.setPixmap(pixmap)
        layout_setup.addWidget(labelLogo)

        self.setup_Legend()
        self.setup_message()

    def setup_Legend(self):
        ## 2rd ########################
        # 그래프 범례
        grp_legend = QGroupBox("LEGEND")
        layout_legend = QHBoxLayout()
        grp_legend.setLayout(layout_legend)
        self.main_layer.addWidget(grp_legend)

        # 1 vol1 
        btn_vol1 = QPushButton("VOLTAGE1")
        btn_vol1.setStyleSheet("color: #ff0000;")
        btn_vol1.clicked.connect(lambda:self.draw_graph_each('VOLTAGE1', color=(255, 0, 0)))
        self.cbx_vol1 = QCheckBox("")
        self.cbx_vol1.setChecked(True)
        self.cbx_vol1.stateChanged.connect(lambda:self.checkBoxChanged("VOLTAGE1"))
        self.label_vol1 = QLabel("0")
        label_vol1_unit = QLabel("V")
        self.group_vol1 = self.displayUnitLayout(layout_legend, btn_vol1, self.cbx_vol1, self.label_vol1, label_vol1_unit)

        # 2 cur1 
        btn_cur1 = QPushButton("CURRENT1")
        btn_cur1.setStyleSheet("color: #0000ff;")
        btn_cur1.clicked.connect(lambda:self.draw_graph_each('CURRENT1', color=(0, 0, 255)))
        self.cbx_cur1 = QCheckBox("")
        self.cbx_cur1.setChecked(False)
        self.cbx_cur1.stateChanged.connect(lambda:self.checkBoxChanged("CURRENT1"))
        self.label_cur1 = QLabel("0")
        label_cur1_unit = QLabel("uA")
        self.group_cur1 = self.displayUnitLayout(layout_legend, btn_cur1, self.cbx_cur1, self.label_cur1, label_cur1_unit)

        # 3 vol2 
        btn_vol2 = QPushButton("VOLTAGE2")
        btn_vol2.setStyleSheet("color: #ffbf00;")
        btn_vol2.clicked.connect(lambda:self.draw_graph_each('VOLTAGE2', color=(255, 191, 0)))
        self.cbx_vol2 = QCheckBox("")
        self.cbx_vol2.setChecked(True)
        self.cbx_vol2.stateChanged.connect(lambda:self.checkBoxChanged("VOLTAGE2"))
        self.label_vol2 = QLabel("0")
        label_vol2_unit = QLabel("V")
        self.group_vol2 = self.displayUnitLayout(layout_legend, btn_vol2, self.cbx_vol2, self.label_vol2, label_vol2_unit)

        # 4 cur2 
        btn_cur2 = QPushButton("CURRENT2")
        btn_cur2.setStyleSheet("color: #1AAF33;")
        btn_cur2.clicked.connect(lambda:self.draw_graph_each('CURRENT2', color=(26, 175, 51)))
        self.cbx_cur2 = QCheckBox("")
        self.cbx_cur2.setChecked(False)
        self.cbx_cur2.stateChanged.connect(lambda:self.checkBoxChanged("CURRENT2"))
        self.label_cur2 = QLabel("0")
        label_cur2_unit = QLabel("uA")
        self.group_cur2 = self.displayUnitLayout(layout_legend, btn_cur2, self.cbx_cur2, self.label_cur2, label_cur2_unit)

        # 6 cps 
        btn_cps = QPushButton("CPS")
        btn_cps.setStyleSheet("color: #6C3483;")
        btn_cps.clicked.connect(lambda:self.draw_graph_each('CPS', color=(108, 52, 131)))
        self.cbx_cps = QCheckBox("")
        self.cbx_cps.setChecked(True)
        self.cbx_cps.stateChanged.connect(lambda:self.checkBoxChanged("CPS"))
        self.label_cps = QLabel("0")
        label_cps_unit = QLabel("pF")
        self.group_cps = self.displayUnitLayout(layout_legend, btn_cps, self.cbx_cps, self.label_cps, label_cps_unit)

        self.display_time = QLCDNumber()
        self.display_time.setDigitCount(11)
        self.display_time.setStyleSheet('background: white')
        timedisplay = '{:02d}:{:02d}:{:02d}:{:02d}'.format(((self.time // 60) // 60) // 24, 
                        (self.time // 60) // 60, (self.time // 60) % 60, self.time % 60)
        self.display_time.display(timedisplay)
        layout_legend.addWidget(self.display_time)
    
    def setup_message(self):
        ## 2-1rd Fault Message ########
        grp_fault = QGroupBox("에러 메시지")
        layout_fault = QHBoxLayout()
        grp_fault.setLayout(layout_fault)

        self.label_fault = QLabel("")
        layout_fault.addWidget(self.label_fault)

        self.label_start_time = QLabel("")
        layout_fault.addWidget(self.label_start_time)

        self.main_layer.addWidget(grp_fault)

        ## 3th ########################
        # 그래프 디스플레이
        grp_display = QGroupBox("GRAPH :: CHANNLE #1")
        layout_display = QVBoxLayout()

        ################ #1 ################
        ## create graph #1
        self.graphWidget1 = pyGraph.PlotWidget()
        self.graphWidget1.setBackground('w')
        self.graphWidget1.setMouseEnabled(x=False, y=False)
        
        ## create mainAxis
        self.axisA1 = self.graphWidget1.plotItem
        self.axisA1.setLabels(left='Voltage')
        self.axisA1.showAxis('right')

        ## Second ViewBox
        self.axisB1 = pyGraph.ViewBox()
        self.axisA1.scene().addItem(self.axisB1)
        self.axisA1.getAxis('right').linkToView(self.axisB1)
        self.axisB1.setXLink(self.axisA1)
        self.axisA1.getAxis('right').setLabel('Current', color='#0000ff')

        ## create Third ViewBox. 
        self.axisC1 = pyGraph.ViewBox()
        ax3 = pyGraph.AxisItem('right')
        self.axisC1.setYRange(7000, 9500)
        self.axisA1.layout.addItem(ax3, 2, 3)
        self.axisA1.scene().addItem(self.axisC1)
        ax3.linkToView(self.axisC1)
        self.axisC1.setXLink(self.axisA1)
        ax3.setLabel('CPS', color='#000080')

        # mainAxis
        self.graph_vol1 = self.axisA1.plot(self.indexs, 
                    self.vol_11s, pen=self.pen_vol1, name="Voltage1s")
        self.graph_vol2 = self.axisA1.plot(self.indexs, 
                    self.vol_12s, pen=self.pen_vol2, name="Voltage2s")
        
        # axisB
        self.itemCurrent1 = pyGraph.PlotCurveItem(self.indexs, 
                    self.cur_11s, pen=self.pen_cur1, name="Current1s")
        self.graph_cur1s = self.axisB1.addItem(self.itemCurrent1)
        
        self.itemCurrent2 = pyGraph.PlotCurveItem(self.indexs, 
                    self.cur_12s, pen=self.pen_cur2, name="Current2s")
        self.graph_cur2s = self.axisB1.addItem(self.itemCurrent2)

        self.itemCps1 = pyGraph.PlotCurveItem(self.indexs, 
                    self.cpss_1, pen=self.pen_cps, name="CPS")
        self.graph_cpss1 = self.axisC1.addItem(self.itemCps1)
        
        layout_display.addWidget(self.graphWidget1)

        grp_display.setLayout(layout_display)
        self.main_layer.addWidget(grp_display)

        ################ #2 ################
        ## create graph #2
        grp_display2 = QGroupBox("GRAPH :: CHANNLE #2")
        layout_display2 = QVBoxLayout()

        self.graphWidget2 = pyGraph.PlotWidget()
        self.graphWidget2.setBackground('w')
        self.graphWidget2.setMouseEnabled(x=False, y=False)
        
        ## create mainAxis
        self.axisA2 = self.graphWidget2.plotItem
        self.axisA2.setLabels(left='Voltage')
        self.axisA2.showAxis('right')

        ## Second ViewBox
        self.axisB2 = pyGraph.ViewBox()
        self.axisA2.scene().addItem(self.axisB2)
        self.axisA2.getAxis('right').linkToView(self.axisB2)
        self.axisB2.setXLink(self.axisA2)
        self.axisA2.getAxis('right').setLabel('Current', color='#0000ff')

        ## create Third ViewBox. 
        self.axisC2 = pyGraph.ViewBox()
        ax4 = pyGraph.AxisItem('right')
        self.axisC2.setYRange(7000, 9500)
        self.axisA2.layout.addItem(ax4, 2, 3)
        self.axisA2.scene().addItem(self.axisC2)
        ax4.linkToView(self.axisC2)
        self.axisC2.setXLink(self.axisA2)
        ax4.setLabel('CPS', color='#000080')

        self.updateViews()
        self.axisA2.vb.sigResized.connect(self.updateViews)

        # mainAxis
        self.graph_vol3 = self.axisA2.plot(self.indexs, 
                    self.vol_21s, pen=self.pen_vol1, name="Voltage1s")
        self.graph_vol4 = self.axisA2.plot(self.indexs, 
                    self.vol_22s, pen=self.pen_vol2, name="Voltage2s")
        
        # axisB
        self.itemCurrent3 = pyGraph.PlotCurveItem(self.indexs, 
                    self.cur_21s, pen=self.pen_cur1, name="Current2s")
        self.graph_cur2s = self.axisB2.addItem(self.itemCurrent3)
        
        self.itemCurrent4 = pyGraph.PlotCurveItem(self.indexs, 
                    self.cur_22s, pen=self.pen_cur2, name="Current2s")
        self.graph_cur2s = self.axisB2.addItem(self.itemCurrent4)

        self.itemCps2 = pyGraph.PlotCurveItem(self.indexs, 
                    self.cpss_2, pen=self.pen_cps, name="CPS")
        self.graph_cpss2 = self.axisC2.addItem(self.itemCps2)
        
        layout_display2.addWidget(self.graphWidget2)

        grp_display2.setLayout(layout_display2)
        self.main_layer.addWidget(grp_display2)
    
        ## 4th ########################
        # Status
        self.statusbar = QStatusBar()
        self.setStatusBar(self.statusbar)
        self.statusbar.setObjectName("statusbar")
        
        self.statusmessage = 'PSTEK, {},  {}'
        now = QDate.currentDate()
        displaymessage = self.statusmessage.format(now.toString(Qt.ISODate), 'PSTEK is aiming for a global leader. !!')
        self.statusbar.showMessage(displaymessage)

        # self.showFullScreen()

    def updateViews(self):
        self.axisB1.setGeometry(self.axisA1.vb.sceneBoundingRect())
        self.axisC1.setGeometry(self.axisA1.vb.sceneBoundingRect())
        self.axisB1.linkedViewChanged(self.axisA1.vb, self.axisB1.XAxis)
        self.axisC1.linkedViewChanged(self.axisA1.vb, self.axisC1.XAxis)

        self.axisB2.setGeometry(self.axisA2.vb.sceneBoundingRect())
        self.axisC2.setGeometry(self.axisA2.vb.sceneBoundingRect())
        self.axisB2.linkedViewChanged(self.axisA2.vb, self.axisB2.XAxis)
        self.axisC2.linkedViewChanged(self.axisA2.vb, self.axisC2.XAxis)

    # 전원 장치와 송신을 위한 설정
    def device_params_modify(self):
        Dialog = QDialog()
        if self.ptype == RTU or self.ptype == TCP:
            if self.channelnum:
                self.data = device.read_setting_value(self.client1, self.ptype)
                logging.info("RTU :: 2CH :: ", self.data)
                dialog = ESCParams(Dialog, self.client1, self.ptype, self.data, self.ndp_flag)
            else:
                self.data = device.read_setting_value_1u(self.client1, self.ptype)
                logging.info("RTU :: 1CH :: ", self.data)
                dialog = ESCParams1U(Dialog, self.client1, self.ptype, self.data, self.ndp_flag)

        elif self.ptype == SCPI:
                self.data = scpi.read_configure(self.client1)
                self.toggle = self.data['TOGGLE']
                logging.info(f'device_params_modify :: {self.data}')
                dialog = ESCParamsSCPI(self, Dialog, self.client1, self.data)

        elif self.ptype == SEMES:
                self.voltage1 = semes.semes_get_measure(self.client1, 'VOLTAGE')
                # logging.info('device_params_modify', self.voltage1)
                dialog = ESCParamsSEMES(Dialog, self.client1, self.voltage1)
        else:
            self.data = device.read_setting_value_1u(self.client1, self.ptype)
            dialog = ESCParams1U(Dialog, self.client1, self.ptype, self.data, self.ndp_flag)

        dialog.show()
        response = dialog.exec_()

    def feedBack(self, ptype):
        while True:
            try:
                feedback = self.client1.readline()
                if feedback:
                    logging.info(f"{ptype} :: {feedback}")
                    break
            except KeyboardInterrupt:
                logging.info(f"KeyboardInterrupt :: {ptype}")
                break
            except Exception as e:
                logging.info(f"Exception ERROR !! :: {ptype} :: {e}")

    # 전원 장치 Toggle
    def device_toggle(self):
        if self.ptype == SCPI:
            if self.toggle == device.FORWARD:
                self.toggle = device.REVERSE
                command = scpi.scpi_command_2ch('SET_TOGGLE_FWD')
                logging.info(f"Toggle :: {command}")
                self.client1.write(command)
                # self.feedBack('REVERSE')

            else:
                self.toggle = device.FORWARD
                command = scpi.scpi_command_2ch('SET_TOGGLE_BACK')
                logging.info(f"Toggle :: {command}")
                self.client1.write(command)
                # self.feedBack('FORWARD')

        elif self.ptype == SEMES:
            if self.toggle == device.FORWARD:
                self.toggle = device.REVERSE
                command = semes.semes_write('REVERSEOFF')
                logging.info(f"Toggle :: {command}")
                self.client1.write(command)
                self.feedBack('REVERSEOFF')

            else:
                self.toggle = device.FORWARD
                command = semes.semes_write('REVERSEON')
                logging.info(f"Toggle :: {command}")
                self.client1.write(command)
                self.feedBack('REVERSEON')

        else:
            if self.toggle == device.FORWARD:
                self.toggle = device.REVERSE
                if self.channelnum:
                    device.write_registers(self.client1, device.WRITE_TOGGLE, device.REVERSE, self.ptype)
                else:
                    device.write_registers(self.client1, device.WRITE_TOGGLE_1U, device.REVERSE, self.ptype)

            else:
                self.toggle = device.FORWARD
                if self.channelnum:
                    device.write_registers(self.client1, device.WRITE_TOGGLE, device.FORWARD, self.ptype)
                else:
                    device.write_registers(self.client1, device.WRITE_TOGGLE_1U, device.FORWARD, self.ptype)

    # Bit return 함수 
    def bit_check(self, x):
        data = []
        for idx in range(15):
            if (x & (1<<idx)):
                data.append(idx)
        return data

    # Falult 오류 확인 
    def fault_check(self):
        x = device.read_fault(self.client1, self.ptype)
        faults = self.bit_check(x)

        if faults:
            message = ""

            for fault in faults:
                message += self.faultmessage[fault]

            self.label_fault.setText(message)
        
        try:
            x = device.read_fault(self.client2, self.ptype)
            faults = self.bit_check(x)

            if faults:
                message = ""

                for fault in faults:
                    message += self.faultmessage[fault]

                self.label_fault.setText(message)
        except Exception as e:
            print("self.client2 ::", e)
            
    # 데이터를 가져오는 오는 시작점
    def data_pullstart(self):
        if not self.client1:
            self.setting_device()

        # self.btn_savedata.setEnabled(True)
        if not self.dtimer:
            self.dtimer = QtCore.QTimer()
            self.stimer = QtCore.QTimer()
            self.faulttimer = QtCore.QTimer()
            self.toggletimer = QtCore.QTimer()
            self.runstoptimer = QtCore.QTimer()

        self.btn_datastart.setEnabled(False)
        self.btn_datastop.setEnabled(True)
        self.start_time = datetime.now()
        start_time = datetime.strftime(self.start_time, '%Y-%m-%d %H:%M:%S')
        self.label_start_time.setText(start_time)

        ## TOGGLE
        toogle_time = int(self.toogle_time.text())
        if self.auto_toggle.isChecked():
            TOOGLE_TIME = toogle_time * 1000
            self.toggletimer.setInterval(TOOGLE_TIME)
            self.toggletimer.timeout.connect(self.device_toggle)
            self.toggletimer.start()

        ## AUTO RUNSTOP :: 30분 :: 30 * 60 * 1000
        runstop_time = int(self.runstop_time.text())
        if self.auto_runstop.isChecked():
            self.RUNSTOP_TIME = runstop_time * 60 * 1000
            self.runstop = False
            self.runstoptimer.setInterval(self.RUNSTOP_TIME)
            self.runstoptimer.timeout.connect(self.device_auto_runstop)
            self.runstoptimer.start()

        self.stimer.setInterval(1000)
        self.stimer.timeout.connect(self.get_time)
        self.stimer.start()

        # setting timer
        num = int(self.btn_num.text())
        DATA_PERIOD = num * 1000
        # DATA_PERIOD = 2000
        self.dtimer.setInterval(DATA_PERIOD)
        self.dtimer.timeout.connect(self.get_data)
        self.dtimer.start()

        if self.ptype == RTU:
            self.faulttimer.setInterval(60000)
            self.faulttimer.timeout.connect(self.fault_check)
            self.faulttimer.start()

    # 데이터 가져 오기
    def get_data(self):
        if self.ptype == RTU or self.ptype == TCP:
            result = device.read_data(self.client1, self.ptype)
            logging.info(f"Client #1 :: {result}")
            if result:
                self.countN = self.countN  + 1
                tm = time.time()

                # 데이터 값을 Array 에 저장하기 
                self.indexs.append(self.countN)
                self.tmarks.append(tm)
                voltage1 = c_int16(result[0]).value
                voltage2 = c_int16(result[2]).value

                self.vol_11s.append(voltage1)
                self.cur_11s.append(result[1])
                self.vol_12s.append(voltage2)
                self.cur_12s.append(result[3])
                self.cpss_1.append(result[7])

                ## LABEL
                self.label_vol1.setText(str(voltage1))
                self.label_cur1.setText(str(result[1]))
                self.label_vol2.setText(str(voltage2))
                self.label_cur2.setText(str(result[3]))
                self.label_cps.setText(str(result[7]))

                ## CLINENT #2
                try:
                    result = device.read_data(self.client2, self.ptype)
                    logging.info(f"Client #2 :: {result}")
                    voltage1 = c_int16(result[0]).value
                    voltage2 = c_int16(result[2]).value

                    self.vol_21s.append(voltage1)
                    self.cur_21s.append(result[1])
                    self.vol_22s.append(voltage2)
                    self.cur_22s.append(result[3])
                    self.cpss_2.append(result[7])

                except Exception as e:
                    logging.info(f"READ CLIENT #2 :: {self.client2} :: {e}")
                    self.vol_21s.append(0)
                    self.cur_21s.append(0)
                    self.vol_22s.append(0)
                    self.cur_22s.append(0)
                    self.cpss_2.append(0)
    
                self.draw_graph()

        # if self.countN % SAVE_COUNT == 0:
        if self.countN and self.countN % SAVE_COUNT == 0:
            self.dtimer.stop()
            self.regular_save_json()
            self.reset_data()
            self.dtimer.start()


    def get_time(self):
        self.time += 1
        dtime = self.time
        try:
            day = ((dtime // 60) // 60) // 24
            if day:
                dtime = dtime - day * 24 * 60 * 60
            hour = (dtime // 60) // 60
            if hour:
                dtime = dtime - hour * 60 * 60
            minute = (dtime // 60)
            second = dtime % 60
        except Exception as e:
            print(e)
        timedisplay = '{:02d}:{:02d}:{:02d}:{:02d}'.format(day, hour, minute, second)
        self.display_time.display(timedisplay)

    # draw_graph using all graph
    def draw_graph(self):
        # VOLTAGE 1
        if self.view_vol_11:
            if self.countN >= 100:
                temp_vol_11s = self.vol_11s[-100:]
                temp_vol_21s = self.vol_21s[-100:]
                temp_indexs = self.temp_indexs
            else:
                temp_vol_11s = self.vol_11s
                temp_vol_21s = self.vol_21s
                temp_indexs = list(range(len(self.vol_11s)))

            self.graph_vol1.setData(temp_indexs, temp_vol_11s)
            self.graph_vol3.setData(temp_indexs, temp_vol_21s)
            
        else:
            self.graph_vol1.hide()
            self.graph_vol3.hide()

        # CURRENT 1
        if self.view_cur_11:
            self.itemCurrent1.clear()
            self.itemCurrent3.clear()
            if self.countN >= 100:
                temp_cur_11s = self.cur_11s[-100:]
                temp_cur_21s = self.cur_21s[-100:]
                temp_indexs = self.temp_indexs
            else:
                temp_cur_11s = self.cur_11s
                temp_cur_21s = self.cur_21s
                temp_indexs = list(range(len(self.cur_11s)))

            self.itemCurrent1.setData(temp_indexs, temp_cur_11s)
            self.itemCurrent3.setData(temp_indexs, temp_cur_21s)

        else:
            self.itemCurrent1.hide()
            self.itemCurrent3.hide()

        # VOLTAGE 2
        if self.view_vol_12 and self.channelnum:
            if self.countN >= 100:
                temp_vol_12s = self.vol_12s[-100:]
                temp_vol_22s = self.vol_22s[-100:]
                temp_indexs = self.temp_indexs
            else:
                temp_vol_12s = self.vol_12s
                temp_vol_22s = self.vol_22s
                temp_indexs = list(range(len(self.vol_12s)))

            self.graph_vol2.setData(temp_indexs, temp_vol_12s)
            self.graph_vol4.setData(temp_indexs, temp_vol_22s)
        else:
            self.graph_vol2.hide()
            self.graph_vol4.hide()

        # CURRENT 2 
        if self.view_cur_12 and self.channelnum:
            self.itemCurrent2.clear()
            self.itemCurrent4.clear()

            if self.countN >= 100:
                temp_cur_12s = self.cur_12s[-100:]
                temp_cur_22s = self.cur_22s[-100:]
                temp_indexs = self.temp_indexs
            else:
                temp_cur_12s = self.cur_12s
                temp_cur_22s = self.cur_22s
                temp_indexs = list(range(len(self.cur_12s)))

            self.itemCurrent2.setData(temp_indexs, temp_cur_12s)
            self.itemCurrent4.setData(temp_indexs, temp_cur_22s)

        else:
            self.itemCurrent2.hide()
            self.itemCurrent4.hide()

        # CPS
        if self.view_cps_1 and self.channelnum:
            self.itemCps1.clear()
            self.itemCps2.clear()

            if self.countN >= 100:
                temp_cpss_1 = self.cpss_1[-100:]
                temp_cpss_2 = self.cpss_2[-100:]
                temp_indexs = self.temp_indexs
            else:
                temp_cpss_1 = self.cpss_1
                temp_cpss_2 = self.cpss_2
                temp_indexs = list(range(len(self.cpss_1)))
            try:
                self.itemCps1.setData(temp_indexs, temp_cpss_1)
                self.itemCps2.setData(temp_indexs, temp_cpss_2)
            except Exception as e:
                print("GRAPH ERROR :: ", self.countN, e)
                print("temp_indexs :: ", self.countN, temp_indexs)
                print("temp_cpss_1 :: ", self.countN, temp_cpss_1, temp_cpss_2)

        else:
            self.itemCps1.hide()
            self.itemCps2.hide()

        self.updateViews()
        self.axisA1.vb.sigResized.connect(self.updateViews)
        self.axisA2.vb.sigResized.connect(self.updateViews)


    def draw_graph_full(self):
        # VOLTAGE 1
        if self.view_vol_11:
            self.graph_vol1.clear()

            temp_vol1s = self.vol_11s
            temp_indexs = list(range(len(self.vol_11s)))
            self.graph_vol1 = self.axisA.plot(temp_indexs, 
                    temp_vol1s, pen=self.pen_vol1, name="Voltage1s")
            self.graph_vol1.show()
        else:
            self.graph_vol1.hide()

        # CURRENT 1
        if self.view_cur_11:
            self.itemCurrent1.clear()

            temp_cur1s = self.cur_11s
            temp_indexs = list(range(len(self.cur_11s)))

            self.graph_cur1s = self.axisB.addItem(pyGraph.PlotCurveItem(temp_indexs, 
                    temp_cur1s, pen=self.pen_cur1, name="Current1s"))
            # self.graph_cur1s.show()

        else:
            self.axisB.hide()

        # VOLTAGE 2
        if self.view_vol_12 and self.channelnum:
            self.graph_vol2.clear()

            temp_vol2s = self.vol_12s
            temp_indexs = list(range(len(self.vol_12s)))

            self.graph_vol2 = self.axisA.plot(temp_indexs, 
                    temp_vol2s, pen=self.pen_vol2, name="Voltage2s")
            self.graph_vol2.show()
        else:
            self.graph_vol2.hide()

        # CURRENT 2 
        if self.view_cur_12 and self.channelnum:
            self.itemCurrent2.clear()

            temp_cur2s = self.cur_12s
            temp_indexs = list(range(len(self.cur_12s)))

            self.graph_cur2s = self.axisB.addItem(pyGraph.PlotCurveItem(temp_indexs, 
                    temp_cur2s, pen=self.pen_cur2, name="Current2s"))
            # self.graph_cur2s.show()

        else:
            self.axisB.hide()

        # # VBIAS
        # if self.view_vbia and self.channelnum:
        #     self.itemVbia.clear()

        #     temp_vbias = self.vbias
        #     temp_indexs = list(range(len(self.vbias)))

        #     self.itemVbia = pyGraph.PlotCurveItem(temp_indexs, 
        #             temp_vbias, pen=self.pen_vbia, name="Vbias")
        #     self.graph_vbias = self.axisC.addItem(self.itemVbia)
        #     # self.graph_vbias.show()

        # else:
        #     self.view_vbia = False 
        #     # self.axisC.removeItem(self.itemVbia)
        #     self.axisC.hide()

        # CPS
        if self.view_cps_1 and self.channelnum:
            self.itemCps.clear()
            temp_cpss = self.cpss
            temp_indexs = list(range(len(self.cpss)))
            # logging.info("CPS ::", temp_cpss)
            self.itemCps = pyGraph.PlotCurveItem(temp_indexs, 
                    temp_cpss, pen=self.pen_cps, name="CPS")
            self.graph_cpss = self.axisC.addItem(self.itemCps)
            # self.graph_cpss.show()

        else:
            self.view_cps_1 = False  
            # self.axisC.removeItem(self.itemCps)
            self.axisC.hide()

        # Leak_Cur1
        if self.view_leak_cur1 and self.channelnum:
            self.itemLeakCur1.clear()

            temp_leak_cur1s = self.leak_cur1s
            temp_indexs = list(range(len(self.leak_cur1s)))

            self.itemLeakCur1 = pyGraph.PlotCurveItem(temp_indexs, 
                    temp_leak_cur1s, pen=self.pen_leak_cur1, name="Leak_Cur1")
            self.graph_leak_cur1s = self.axisD.addItem(self.itemLeakCur1)
            # self.graph_leak_cur1s.show()

        else:
            self.view_leak_cur1 = False 
            # self.axisD.removeItem(self.itemLeakCur1)
            self.axisD.hide()


        if self.view_leak_cur2 and self.channelnum:
            self.itemLeakCur2.clear()

            temp_leak_cur2s = self.leak_cur2s
            temp_indexs = list(range(len(self.leak_cur2s)))

            self.itemLeakCur2 = pyGraph.PlotCurveItem(temp_indexs, 
                    temp_leak_cur2s, pen=self.pen_leak_cur2, name="Leak_Cur2")
            self.graph_leak_cur2s = self.axisD.addItem(self.itemLeakCur2)
            # self.graph_leak_cur2s.show()

        else:
            self.view_leak_cur2 = False 
            # self.axisD.removeItem(self.itemLeakCur2)
            self.axisD.hide()

    ## CLIENT #1
    # Run Device (중간에 Run 을 눌러서 디바이스 시작시킴)
    def esc_device_run(self):
        self.run_flag1 = True 
        # 전원 장치를 시작하고, 데이터를 읽어옴
        if self.channelnum: 
            self.data = device.read_setting_value(self.client1, self.ptype)
        else:
            self.data = device.read_setting_value_1u(self.client1, self.ptype)

        if self.data:
            logging.info(self.data)
            self.toggle = self.data['rd_toggle']

        device.write_registers(self.client1, 
            device.WRITE_BIT_POWER_ON, device.RUN_VALUE, self.ptype)
        
        device.write_registers(self.client2, 
            device.WRITE_BIT_POWER_ON, device.RUN_VALUE, self.ptype)

        self.btn_toggle.setEnabled(True)
        self.btn_esc_stop.setEnabled(True)
        # self.btn_stopdata.setEnabled(True)

        now = QDate.currentDate()
        displaymessage = self.statusmessage.format(now.toString(Qt.ISODate), "DEVICE RUN")
        self.statusbar.showMessage(displaymessage)


    ## CLIENT #2
    def esc_device_run2(self):
        self.run_flag1 = True 
        # 전원 장치를 시작하고, 데이터를 읽어옴
        
        if self.channelnum: 
            self.data = device.read_setting_value(self.client2, self.ptype)
        else:
            self.data = device.read_setting_value_1u(self.client2, self.ptype)

        if self.data:
            logging.info(self.data)
            self.toggle = self.data['rd_toggle']

        device.write_registers(self.client2, 
            device.WRITE_BIT_POWER_ON, device.RUN_VALUE, self.ptype)

        self.btn_toggle.setEnabled(True)
        self.btn_esc_stop.setEnabled(True)
        # self.btn_stopdata.setEnabled(True)

        now = QDate.currentDate()
        displaymessage = self.statusmessage.format(now.toString(Qt.ISODate), "DEVICE RUN")
        self.statusbar.showMessage(displaymessage)

        if not self.dtimer:
            # setting timer
            self.start_pulldata()

    ## AUTO RUN STOP
    def device_auto_runstop(self):
        self.runstoptimer.stop()
        self.runstoptimer = None 

        if self.runstop:
            device.write_registers(self.client1, 
                    device.WRITE_BIT_POWER_ON, device.STOP_VALUE, self.ptype)
            device.write_registers(self.client2, 
                    device.WRITE_BIT_POWER_ON, device.STOP_VALUE, self.ptype)
            self.runstop = False
        else:
            device.write_registers(self.client1, 
                device.WRITE_BIT_POWER_ON, device.RUN_VALUE, self.ptype)
            device.write_registers(self.client2, 
                device.WRITE_BIT_POWER_ON, device.RUN_VALUE, self.ptype)
            self.runstop = True 

        self.runstoptimer = QtCore.QTimer()
        self.runstoptimer.setInterval(self.RUNSTOP_TIME)
        self.runstoptimer.timeout.connect(self.device_auto_runstop)
        self.runstoptimer.start()

    # 단말기 STOP #1
    def esc_device_stop(self):
        self.btn_toggle.setEnabled(False)
        device.write_registers(self.client1, 
                device.WRITE_BIT_POWER_ON, device.STOP_VALUE, self.ptype)
        device.write_registers(self.client2, 
                device.WRITE_BIT_POWER_ON, device.STOP_VALUE, self.ptype)

        try:
            self.toogletimer.stop()
            self.toogletimer = None 

            self.runstoptimer.stop()
            self.runstoptimer = None 

            self.dtimer.stop()
            self.dtimer = None

        except Exception as e:
            pass

    # 데이터 전송 중단
    def data_pullstop(self):
        self.run_flag = False

        self.regular_save_json()
        self.draw_graph()
        self.reset_data()

        self.btn_datastart.setEnabled(True)
        self.btn_datastop.setEnabled(False)

        # self.btn_savedata.setEnabled(True)
        # self.btn_pulldata.setEnabled(True)
        self.btn_esc_run.setEnabled(True)

        # self.btn_zoomA.setEnabled(True)
        # self.btn_zoom6.setEnabled(True)
        # self.btn_esc_stop.setEnabled(False)
        
        try:
            if self.dtimer:
                self.dtimer.stop()
                self.dtimer = None
                self.stimer.stop()
                self.stimer = None
                self.faulttimer.stop()
                self.faulttimer = None
                self.toogletimer.stop()
                self.toogletimer = None
                self.runstoptimer.stop()
                self.runstoptimer = None 
        except Exception as e:
            pass

        now = QDate.currentDate()
        displaymessage = self.statusmessage.format(now.toString(Qt.ISODate), "DEVICE STOP")
        self.statusbar.showMessage(displaymessage)

    # Keyboard Event
    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Escape:
            pass
        elif e.key() == Qt.Key_F:
            self.showFullScreen()
        elif e.key() == Qt.Key_N:
            self.showNormal()

    # RESET 
    def reset_data(self):
        # Reser Data
        
        self.data = {}
        self.countN = 0
        self.countN2 = 0

        self.view_vol_11 = True
        self.view_cur_11 = False
        self.view_vol_12 = True
        self.view_cur_12 = False
        self.view_cps_1 = True

        self.indexs = []
        self.tmarks = []

        self.vol_11s = []
        self.cur_11s = []
        self.vol_12s = []
        self.cur_12s = []
        self.cpss_1 = []

        self.vol_21s = []
        self.cur_21s = []
        self.vol_22s = []
        self.cur_22s = []
        self.cpss_2 = []

        # Reset 그림 그리기
        self.draw_graph()

    # Graph 개별 그래프 
    def draw_graph_each(self, category, color):
        if self.graph_flag == False:
            self.graph_flag = True
            Dialog = QDialog()
            if category == 'VOLTAGE1':
                values = self.vol_11s
            elif category == 'CURRENT1':
                values = self.cur_11s
            elif category == 'VOLTAGE2':
                values = self.vol_12s
            elif category == 'CURRENT2':
                values = self.cur_12s
            elif category == 'VBIAS':
                values = self.vbias
            elif category == 'CPS':
                values = self.cpss
            elif category == 'LEAK CURRENT1':
                values = self.leak_cur1s
            elif category == 'LEAK CURRENT2':
                values = self.leak_cur2s

            dialog = GraphWindow(Dialog, category, color, self.indexs, values)
            dialog.show()
            response = dialog.exec_()

            if response == QDialog.Accepted or response == QDialog.Rejected:
                self.graph_flag = False

    # zoom_graph
    def zoom_graph(self):
        Dialog = QDialog()
        # logging.info("zoom_graph", len(self.indexs), len(self.vol_11s))
        if self.channelnum:
            dialog = ZoomWindow(Dialog, self.indexs, self.vol_11s, self.cur_11s, self.vol_12s, self.cur_12s, self.vbias, self.cpss, self.leak_cur1s, self.leak_cur2s)
        else:
            dummy_list = []
            for tm in self.indexs:
                dummy_list.append(0)
            dialog = ZoomWindow(Dialog, self.indexs, self.vol_11s, self.cur_11s, dummy_list, dummy_list, dummy_list, dummy_list, dummy_list, dummy_list)
        
        dialog.show()
        response = dialog.exec_()

        if response == QDialog.Accepted or response == QDialog.Rejected:
            self.graph_flag = False

    # 전원 장치와 송신을 위한 설정
    def setting_device1(self):
        if self.com_open_flag == False:
            Dialog = QDialog()
            self.com_open_flag == True
            dialog = SettingWin(Dialog, self.used_ports)
            dialog.show()
            response = dialog.exec_()

            # OK 를 하면 설정 값을 읽어와서 통신을 한다.
            if response == QDialog.Accepted:
                self.ndp_flag = dialog.ndp_flag
                self.channelnum = dialog.channelnum
                logging.info(f"self.ndp_flag  ::  {self.ndp_flag}")
                logging.info(f"self.channelnum  ::  {self.channelnum}")

                if not self.channelnum:
                    self.group_vol2.hide()
                    self.group_cur2.hide()
                    # self.group_vbia.hide()
                    self.group_cps.hide()
                    self.group_leak2.hide()

                if dialog.selected == 'RTU':
                    gen_port = dialog.gen_port
                    com_speed = dialog.com_speed
                    com_data = dialog.com_data
                    com_parity = dialog.com_parity
                    com_stop = dialog.com_stop
                    self.com_open_flag = False

                    self.client1 = device.connect_rtu(
                        port=gen_port, ptype='rtu',
                        speed=com_speed, bytesize=com_data, 
                        parity=com_parity, stopbits=com_stop
                    )
                    self.ptype = RTU
                    self.used_ports.append(gen_port)
                    logging.info(f"connect :: {self.client1}")

                else:
                    gen_port = dialog.ipaddress
                    self.ipport = int(dialog.ipport)
                    logging.info(gen_port)

                    self.client1 = device.connect_tcp(gen_port, self.ipport)
                    self.ptype = TCP

                logging.info(self.client1)
                logging.info(self.ptype)

                # Graph
                self.graph_type = dialog.graph_type

                if self.client1:
                    self.label_gen_port1.setText(gen_port)
                    self.label_ptype1.setText(self.ptype)
                    self.btn_com_gen1.hide()
                    self.btn_esc_run.setEnabled(True)
                    
                # self.data = device.read_setting_value(self.client1, self.ptype)
                # if self.data:
                #     self.toggle = self.data['rd_toggle']

        else:
            logging.info("Open Dialog")

    # 전원 장치와 송신을 위한 설정
    def setting_device2(self):
        if self.com_open_flag == False:
            Dialog = QDialog()
            self.com_open_flag == True
            dialog = SettingWin(Dialog, self.used_ports)
            dialog.show()
            response = dialog.exec_()

            # OK 를 하면 설정 값을 읽어와서 통신을 한다.
            if response == QDialog.Accepted:
                self.ndp_flag = dialog.ndp_flag
                self.channelnum = dialog.channelnum
                logging.info(f"self.ndp_flag  ::  {self.ndp_flag}")
                logging.info(f"self.channelnum  ::  {self.channelnum}")

                if dialog.selected == 'RTU':
                    gen_port = dialog.gen_port
                    com_speed = dialog.com_speed
                    com_data = dialog.com_data
                    com_parity = dialog.com_parity
                    com_stop = dialog.com_stop
                    self.com_open_flag = False

                    self.client2 = device.connect_rtu(
                        port=gen_port, ptype='rtu',
                        speed=com_speed, bytesize=com_data, 
                        parity=com_parity, stopbits=com_stop
                    )
                    self.ptype = RTU
                    self.used_ports.append(gen_port)
                    logging.info(f"connect :: {self.client2}")

                else:
                    gen_port = dialog.ipaddress
                    self.ipport = int(dialog.ipport)
                    logging.info(gen_port)

                    self.client2 = device.connect_tcp(gen_port, self.ipport)
                    self.ptype = TCP

                if self.client2:
                    self.label_gen_port2.setText(gen_port)
                    self.label_ptype2.setText(self.ptype)
                    self.btn_com_gen2.hide()
                    
        else:
            logging.info("Open Dialog")


    # CheckBox clicked
    def checkBoxChanged(self, category):
        if category == 'VOLTAGE1':
            if self.cbx_vol1.isChecked() == True:
                self.view_vol_11 = True
                self.graph_vol1.show()
                self.graph_vol3.show()
            else:
                self.view_vol_11 = False
                self.graph_vol1.hide()
                self.graph_vol3.hide()

        elif category == 'CURRENT1':
            if self.cbx_cur1.isChecked() == True:
                self.view_cur_11 = True
                self.itemCurrent1.show()
                self.itemCurrent3.show()
            else:
                self.view_cur_11 = False 
                self.itemCurrent1.hide()
                self.itemCurrent3.hide()

        elif category == 'VOLTAGE2':
            if self.cbx_vol2.isChecked() == True:
                self.view_vol_12 = True
                self.graph_vol2.show()
                self.graph_vol4.show()
            else:
                self.view_vol_12 = False 
                self.graph_vol2.hide()
                self.graph_vol4.hide()

        elif category == 'CURRENT2':
            if self.cbx_cur2.isChecked() == True:
                self.view_cur_12 = True
                self.itemCurrent2.show()
                self.itemCurrent4.show()
            else:
                self.view_cur_12 = False
                self.itemCurrent2.hide()
                self.itemCurrent4.hide()

        elif category == 'CPS':
            logging.info("CPS")
            if self.cbx_cps.isChecked() == True:
                self.view_cps_1 = True
                self.itemCps1.show()
                self.itemCps2.show()
            else:
                self.view_cps_1 = False  
                self.itemCps1.hide()
                self.itemCps2.hide()

        else:
            logging.info("No Category")

    # SAVE DATA as JSON FILE
    def save_data(self):
        data = {}
        
        FileSave = QFileDialog.getSaveFileName(self, '파일 저장', './data')
        file = FileSave[0]

        time_list = []
        dummy_list = []
        for tm in self.tmarks:
            if isinstance(tm, str):
                time_list.append(tm)
            else:
                tm = time.localtime(tm)
                string = time.strftime('%H:%M:%S', tm)
                time_list.append(string)
            dummy_list.append(0)


        if self.channelnum:
            dataset = pd.DataFrame({'indexs': self.indexs,
                    "times": time_list,
                    'vol1s': self.vol_11s,
                    'cur1s(PHASE2)': self.cur_11s,
                    'vol2s': self.vol_12s,
                    'cur2s': self.cur_12s,
                    'vbias(PHASE1)': self.vbias,
                    'VCS': self.cpss_1,
                    'ICS': self.icss,
                    'cpss': self.cpss,
                    'leak_cur1s': self.leak_cur1s,
                    'leak_cur2s': self.leak_cur2s,
            })
        else:
            dataset = pd.DataFrame({'indexs': self.indexs,
                    "times": time_list,
                    'vol1s': self.vol_11s,
                    'cur1s(PHASE2)': self.cur_11s,
                    'vol2s': dummy_list,
                    'cur2s': dummy_list,
                    'vbias(PHASE)': dummy_list,
                    'VCS': dummy_list,
                    'ICS': dummy_list,
                    'cpss': dummy_list,
                    'leak_cur1s': dummy_list,
                    'leak_cur2s': dummy_list,
            })

        product = 'PSES-50250PBN'
        user = 'USER'

        # CSS FILE
        csvfile = f"{file}.csv"
        dataset.to_csv(csvfile, sep=',', index=False)


        ## JSON   파일
        data['product'] = 'PSES-50250PBN'
        data['user'] = 'USER'
        start_time = datetime.strftime(self.start_time, '%Y-%m-%d %H:%M:%S')
        data['start_time'] = start_time

        if self.channelnum:
            data['indexs'] = self.indexs
            data['times'] = time_list
            data['vol1s'] = self.vol_11s
            data['cur1s'] = self.cur_11s
            data['vol2s'] = self.vol_12s
            data['cur2s'] = self.cur_12s
            data['vbias'] = self.vbias
            data['vcss'] = self.cpss_1
            data['icss'] = self.icss
            data['cpss'] = self.cpss
            data['leak_cur1s'] = self.leak_cur1s
            data['leak_cur2s'] = self.leak_cur2s
        else:
            data['indexs'] = self.indexs
            data['times'] = time_list
            data['vol1s'] = self.vol_11s
            data['cur1s'] = self.cur_11s
            data['vol2s'] = dummy_list
            data['cur2s'] = dummy_list
            data['vbias'] = dummy_list
            data['vcss'] = dummy_list
            data['icss'] = dummy_list
            data['cpss'] = dummy_list
            data['leak_cur1s'] = self.leak_cur1s
            data['leak_cur2s'] = dummy_list
        
        # JSON FILE
        jsonfile = f"{file}.json"
        with open(jsonfile, 'wb') as afile:
            json.dump(data, codecs.getwriter('utf-8')(afile))


    def regular_save_json(self):
        data = {}
        start_time = datetime.now()
        file = f"esc_{start_time.year:04d}_{start_time.month:02d}_{start_time.day:02d}_{start_time.hour:02d}_{start_time.minute:02d}_{start_time.second:02d}"
        
        start_time = datetime.strftime(self.start_time, '%Y-%m-%d %H:%M:%S')
        data['product'] = 'PSES-50250PBN'
        data['user'] = 'USER'
        data['start_time'] = start_time
        data['num'] = self.btn_num.text()

        time_list = []
        dummy_list = []
        for tm in self.tmarks:
            if isinstance(tm, str):
                time_list.append(tm)
            else:
                tm = time.localtime(tm)
                string = time.strftime('%H:%M:%S', tm)
                time_list.append(string)
            dummy_list.append(0)

        if self.channelnum:
            dataset = pd.DataFrame({'indexs': self.indexs,
                    "times": time_list,
                    'Ch1_vol1': self.vol_11s,
                    'Ch1_cur1s': self.cur_11s,
                    'Ch1_vol2s': self.vol_12s,
                    'Ch1_cur2s': self.cur_12s,
                    'Ch1_CAP1': self.cpss_1,

                    'Ch2_vol1': self.vol_21s,
                    'Ch2_cur1s': self.cur_21s,
                    'Ch2_vol2s': self.vol_22s,
                    'Ch2_cur2s': self.cur_22s,
                    'Ch2_CAP1': self.cpss_2,
            })
        else:
            dataset = pd.DataFrame({'indexs': self.indexs,
                    "times": time_list,
                    'vol1s': self.vol_11s,
                    'cur1s': self.cur_11s,
                    'vol2s': dummy_list,
                    'cur2s': dummy_list,
                    'vbias': dummy_list,
                    'vcss': dummy_list,
                    'icss': dummy_list,
                    'cpss': dummy_list,
                    'leak_cur1s': dummy_list,
                    'leak_cur2s': dummy_list,
            })

        # CSS FILE
        csvfile = f"data/{file}.csv"
        dataset.to_csv(csvfile, sep=',')
        self.countN = 0
        logging.info(f"########## regular_save_json :: {csvfile} :: self.countN = {self.countN}")


    # Read File DATA (JSON)
    def read_data(self):
        try:
            fname, _ = QFileDialog.getOpenFileName(self, 'Open file', 
             './data', "Json files (*.json)")

            with open(fname, 'r', encoding='utf-8') as json_file:
                data = json.load(json_file)
                self.indexs.extend(data['indexs'])
                logging.info("self.indexs :: ", len(self.indexs))

                if not self.start_time:
                    # self.btn_savedata.setEnabled(True)
                    # self.btn_zoomA.setEnabled(True)
                    # self.btn_zoom6.setEnabled(True)
                    
                    try:
                        start_time = data['start_time']
                        self.label_start_time.setText(start_time)
                        self.start_time = datetime.strptime(start_time, '%Y-%m-%d %H:%M:%S')
                    except Exception as e:
                        logging.info(e)

                    try:
                        self.btn_num.setText(str(data['num']))
                    except:
                        self.btn_num.setText(str(10))

                self.tmarks.extend(data['times'])
                self.vol_11s.extend(data['vol1s'])

                try:
                    self.cur_11s.extend(data['cur1s'])
                except:
                    self.cur_11s.extend(data['curt1s'])

                self.vol_12s.extend(data['vol2s'])

                try:
                    self.cur_12s.extend(data['cur2s'])
                except:
                    self.cur_12s.extend(data['curt2s'])

                self.vbias.extend(data['vbias'])
                self.cpss_1.extend(data['vcss'])
                self.icss.extend(data['icss'])
                self.cpss.extend(data['cpss'])

                try:
                    self.leak_cur1s.extend(data['leak_cur1s'])
                except:
                    self.leak_cur1s.extend(data['dechuck_starts'])
                
                try:
                    self.leak_cur2s.extend(data['leak_cur2s'])
                except:
                    self.leak_cur2s.extend(data['dechuck_ends'])

                s_time = self.label_start_time.text()
                if not s_time:
                    logging.info(data['start_time'])
                    try:
                        self.label_start_time.setText(data['start_time'])
                    except:
                        self.label_start_time.setText()

                product =  data.get('product')
                user =  data.get('user', '이름없음')
                title = "PRODUCT : {} / WRITER : {}".format(product, user)

                num = int(self.btn_num.text())
                self.get_time()

            self.graphWidget.setTitle(title, color="b", size="20pt")

            self.draw_graph_full()

        except Exception as e:
            logging.info(e)


    def inspect_func(self):
        if not self.client1:
            self.setting_device1()
        else:
            Dialog = QDialog()
            dialog = InspectWindow(Dialog, self.client1, self.channelnum)
            dialog.show()
            response = dialog.exec_()

            if response == QDialog.Accepted or response == QDialog.Rejected:
                self.graph_flag = False
      

if __name__ == "__main__":
    app = QApplication(sys.argv)
    form = MainWindow()
    form.show()
    sys.exit(app.exec_())