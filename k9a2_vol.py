#!/usr/bin/env python
# coding: utf-8

# 예제 내용
# * 기본 위젯을 사용하여 기본 창을 생성
# * 다양한 레이아웃 위젯 사용
import os
import sys
import serial
import common.serial_v2 as device
from common.setup_device import SettingWin

from pymodbus.payload import BinaryPayloadBuilder
from pymodbus.constants import Endian

from PyQt5.QtWidgets import QWidget, QLabel, QPushButton, QDesktopWidget, QMainWindow
from PyQt5.QtWidgets import QGroupBox, QHBoxLayout, QGridLayout, QSpinBox, QDoubleSpinBox
from PyQt5.QtWidgets import QApplication, QDialog, QStatusBar, QFileDialog, QCheckBox, QLCDNumber
from PyQt5 import QtCore
from PyQt5.QtCore import QDate, Qt
from PyQt5.QtGui import QPixmap

from ctypes import c_int16

V24 = 1
V36 = 2

ADDRESS = 250

import logging
FORMAT = ('%(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.INFO)

def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)


class DataBlock(QWidget):
    def __init__(self, label, val, vmin=0, vmax=48):
        super(DataBlock, self).__init__()
        black = 'QLabel{font-size: 32pt; font-weight: bold}'
        self.vmin = vmin
        self.vmax = vmax

        self.label = QLabel(label)
        self.label.setStyleSheet(black)

        self.edit_val = QDoubleSpinBox()
        self.edit_val.setStyleSheet('margin: -1px;font-size: 32pt;padding: 0px; color: blue')
        self.edit_val.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self.edit_val.setRange(vmin, vmax)
        self.edit_val.setValue(val)

    def display(self, layout, row):
        layout.addWidget(self.label, row, 0)
        layout.addWidget(self.edit_val, row, 1, 1, 2)

    def getVal(self):
        return self.edit_val.value()


class DataBlockInt(QWidget):
    def __init__(self, label, val, vmin=0, vmax=300):
        super(DataBlockInt, self).__init__()
        black = 'QLabel{font-size: 32pt; font-weight: bold}'
        self.vmin = vmin
        self.vmax = vmax

        self.label = QLabel(label)
        self.label.setStyleSheet(black)

        self.edit_val = QSpinBox()
        self.edit_val.setStyleSheet('margin: -1px;font-size: 32pt;padding: 0px; color: blue')
        self.edit_val.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self.edit_val.setRange(vmin, vmax)
        self.edit_val.setValue(val)

    def display(self, layout, row):
        layout.addWidget(self.label, row, 0)
        layout.addWidget(self.edit_val, row, 1, 1, 2)

    def getVal(self):
        return self.edit_val.value()
    
class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.used_ports = []
        self.com_open_flag = False
        self.toggle = V24
        self.dtimer = None
        self.font_green = 'QLabel{margin: -1px; font-size: 16pt; padding: 0px; color: green;font-weight: bold;}'

        self.com_setting_flag = False
        self.client = None

        self.initUI()

    def initUI(self):
        self.setWindowTitle("PSTEK K9A2 POWER SOURCE")
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width() - 300, screensize.height() - 300) 

        mainwidget = QWidget()                # 위젯의 인스턴스 생성만으로도 QMainWindow에 붙는다.
        self.setCentralWidget(mainwidget)

        ## Main Layout 설정
        main_layer = QGridLayout()
        mainwidget.setLayout(main_layer)

        self.btn_com_gen = QPushButton("전원 장치 연결")
        self.btn_com_gen.clicked.connect(self.setting_device)
        self.lbl_com_gen = QLabel("")
        self.lbl_com_gen.setStyleSheet(self.font_green)
        main_layer.addWidget(self.btn_com_gen, 0, 0)

        self.lbl_com_gen = QLabel("PORT")
        self.lbl_com_gen.setStyleSheet(self.font_green)
        main_layer.addWidget(self.lbl_com_gen, 1, 0)

        # Logo Image
        labelLogo = QLabel("")
        pixmap = QPixmap(resource_path("logo.png"))
        labelLogo.setAlignment(Qt.AlignRight)
        labelLogo.setPixmap(pixmap)
        main_layer.addWidget(labelLogo, 0, 2, 2, 1)

        # DataBlock
        self.data_block1 = DataBlock("VOLTAGE1", 24, 0, 48)
        self.data_block1.display(main_layer, 2)

        self.data_block2 = DataBlock("VOLTAGE2", 36, 0, 48)
        self.data_block2.display(main_layer, 3)

        self.data_cycle = DataBlockInt("변경 주기(초)", 60, 0, 300)
        self.data_cycle.display(main_layer, 4)

        self.btn_start = QPushButton("시 작")
        self.btn_start.clicked.connect(self.device_start)
        self.btn_start.setFixedSize(300, 40)

        self.btn_stop = QPushButton("종 료")
        self.btn_stop.clicked.connect(self.device_stop)
        self.btn_stop.setFixedSize(300, 40)

        main_layer.addWidget(self.btn_start, 5, 0)
        main_layer.addWidget(self.btn_stop, 5, 1)

    def device_start(self):
        ## 초기 설정
        if not self.client:
            self.setting_device()
            return

        self.vol1 = self.data_block1.getVal()
        self.vol2 = self.data_block2.getVal()
        self.cycle = self.data_cycle.getVal() * 1000

        logging.info("device_start :: %s %s %s", self.vol1, self.vol2, self.cycle)
        builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Big)
        builder.add_32bit_float(self.vol1)
        registers = builder.to_registers()

        try:
            result = self.client.write_registers(ADDRESS, registers, unit=2)
            if result.isError():
                logging.info(f"device_start :: ERROR :: {ADDRESS}에 값 {self.vol1} :: {registers} :: {result}")
            else:
                logging.info(f"주소 {ADDRESS}에 값 {self.vol1} :: {registers}를 성공적으로 썼습니다.")
        except Exception as e:  
            logging.info(f"device_start :: ERROR :: {ADDRESS}에 값 {self.vol1} :: {registers} :: {e}")

        self.toggle = V24
        self.dtimer = QtCore.QTimer()
        self.dtimer.setInterval(self.cycle)
        self.dtimer.timeout.connect(self.device_toggle)
        self.dtimer.start()

    # 전원 장치 Toggle
    def device_toggle(self):
        if self.toggle == V24:
            builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Big)
            builder.add_32bit_float(self.vol2)
            registers = builder.to_registers()
            self.toggle = V36

            try:
                result = self.client.write_registers(ADDRESS, registers, unit=2)
                if result.isError():
                    logging.info(f"device_toggle :: ERROR :: {ADDRESS}에 값 {self.vol2} :: {registers} :: {result}")
                else:
                    logging.info(f"주소 {ADDRESS}에 값 {self.vol2} :: {registers}를 성공적으로 썼습니다.")
            except Exception as e:  
                logging.info(f"device_toggle :: ERROR :: {ADDRESS}에 값 {self.vol2} :: {registers} :: {e}")

        elif self.toggle == V36:
            builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Big)
            builder.add_32bit_float(self.vol1)
            registers = builder.to_registers()
            self.toggle = V24

            try:
                result = self.client.write_registers(ADDRESS, registers, unit=2)
                if result.isError():
                    logging.info(f"device_toggle :: ERROR :: {ADDRESS}에 값 {self.vol1} :: {registers} :: {result}")
                else:
                    logging.info(f"주소 {ADDRESS}에 값 {self.vol1} :: {registers}를 성공적으로 썼습니다.")
            except Exception as e:  
                logging.info(f"device_toggle :: ERROR :: {ADDRESS}에 값 {self.vol1} :: {registers} :: {e}")


    def device_stop(self):
        logging.info("device_stop :: ")
        self.dtimer.stop()
        self.dtimer = None

    def setting_device(self):
        if not self.com_open_flag:
            Dialog = QDialog()
            self.com_open_flag == True
            dialog = SettingWin(Dialog, self.used_ports)
            dialog.show()
            response = dialog.exec_()

            # OK 를 하면 설정 값을 읽어와서 통신을 한다.
            if response == QDialog.Accepted:
                if dialog.selected == 'RTU':
                    self.gen_port = dialog.gen_port
                    try:
                        _, port = dialog.gen_port.split('__')
                    except Exception as e:
                        port = dialog.gen_port
                        logging.info("setting_device :: %s", e)
                    com_speed = dialog.com_speed
                    com_data = dialog.com_data
                    com_parity = serial.PARITY_EVEN
                    com_stop = dialog.com_stop
                    self.com_open_flag = False

                    self.client = device.connect_rtu(
                        port=self.gen_port, ptype='rtu',
                        speed=com_speed, bytesize=com_data, 
                        parity=com_parity, stopbits=com_stop
                    )
                    self.lbl_com_gen.setText(port)
                    self.btn_com_gen.hide()
                    logging.info("setting_device :: %s", self.client)
                    
        else:
            logging.info("Open Dialog")
        
      

if __name__ == "__main__":
    app = QApplication(sys.argv)
    form = MainWindow()
    form.show()
    sys.exit(app.exec_())