#!/usr/bin/env python
# coding: utf-8

# 예제 내용
# * 기본 위젯을 사용하여 기본 창을 생성
# * 다양한 레이아웃 위젯 사용
import os
import sys
import numpy as np
import socket
from datetime import datetime
import threading

from PyQt5.QtWidgets import QWidget, QLabel, QPushButton, QDesktopWidget, QMainWindow
from PyQt5.QtWidgets import QGroupBox, QVBoxLayout, QHBoxLayout, QGridLayout, QDialog
from PyQt5.QtWidgets import QApplication, QStatusBar, QTabWidget, QLineEdit, QTableWidgetItem
from PyQt5.QtWidgets import QHeaderView, QTableWidget, QAbstractItemView, QRadioButton, QSpinBox
from PyQt5 import QtCore
from PyQt5.QtCore import QDate, Qt, pyqtSlot
from PyQt5.QtGui import QPixmap

from canon.mcode2 import *
from common.block import MyLabel, resource_path, WarningDialog
from canon.msg2 import MsgProtocol2, SubSystem, Module, Field, Status, PField, SModule, SField, OnOffStatus, OnOffPartStatus, OneStatus

import logging
FORMAT = ('%(module)-15s:%(lineno)-8s:%(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.INFO)


class CLed(QPushButton):
    def __init__(self, name, width):
        super(CLed, self).__init__()
        self.setText(name)
        self.setFixedSize(width, width)
        self.radius = int(width/2)
        style = f"border: 1px solid lightgray;border-radius: {self.radius}px;background-color: #ccc;color: black;font-size: 14px;font-weight: bold;"
        self.setStyleSheet(style)

    def changeColor(self, color):
        style = f"border: 1px solid lightgray;border-radius: {self.radius}px;background-color: {color};color: black;font-size: 14px;font-weight: bold;"
        self.setStyleSheet(style)

    def changeColorText(self, color, text):
        style = f"border: 1px solid lightgray;border-radius: {self.radius}px;background-color: {color};color: black;font-size: 14px;font-weight: bold;"
        self.setStyleSheet(style)
        self.setText(text)


class CanonMainWindow(QMainWindow):
    cyanfont = "border: 1px solid lightgray;border-radius: 25px;background-color: #ccffff;color: black;font-size: 14px;font-weight: bold;"
    skyfont = "border: 1px solid lightgray;border-radius: 25px;background-color: #33acff;color: black;font-size: 14px;font-weight: bold;"
    purplefont = "border: 1px solid lightgray;border-radius: 25px;background-color: #F991FC;color: black;font-size: 14px;font-weight: bold;"
    grayfont = "border: 1px solid lightgray;border-radius: 25px;background-color: #ccc;color: black;font-size: 14px;font-weight: bold;"
    
    cyan = "#ccffff"
    sky = "#33acff"
    purple = "#F991FC"

    def __init__(self):
        super(CanonMainWindow, self).__init__()
        self.index = 0
        self.timeStamp = 0
        self.event = threading.Event()
        self.__udpServer = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        self.__bootflag = False
        self.cflag = False
        self.pflag = False
        self.iflag = False
        self.controlonoff = None

        self.emergency_stop_flag = False
        self.tpc_status = []
        self.tpc_fields = []

        self.pcstimer = None
        self.checktimer = None
        self.ibittime = None
        self.cbit_period = 3000
        self.ibit_period = 5000

        self.pbitControlSubs = {}
        self.pbitPowerSubs = {}
        self.cbitControlSubs = {}
        self.cbitPowerSubs = {}
        self.ibitControlSubs = {}
        self.ibitPowerSubs = {}
        self.__ipAddress = None
        self.__port = None
        self.__fromAddress = None

        self.pbit_mods = []
        self.cbit_mods = []
        self.ibit_mods = []

        ## PBIT
        self.pbit_status = np.zeros((37, 16), dtype=np.int8)
        self.cbit_status = np.zeros((37, 16), dtype=np.int8)
        self.ibit_status = np.zeros((37, 16), dtype=np.int8)
        self.zeroarray = np.zeros(16, dtype=np.int8)

        self.cbit_flag = False
        self.ibit_flag = False 
        self.fbit_flag = False
        self.pcs_flag = False

        self.btn_boot = None
        self.btn_stop = None

        self.initUI()

    def __str__(self):
        return f"CanonMainWindow :: __str__ "

    ## Process and Logic
    def run_pcs_boot(self):
        # 최초 BOOT 할 때,
        self.__ipAddress = self.edit_address.text()
        self.__port = int(self.edit_port.text())
        index = 1

        try:
            self.__udpServer.bind((self.__ipAddress, self.__port))
            logging.info("###################################################################")
            logging.info(f"###### SERVER BIND :: {self.__udpServer} :: ######")
            logging.info("###################################################################\n\n")
            self.__thread = threading.Thread(target=self.__listenSocket, args=())
            self.__thread.start()

        except Exception as e:
            index += 1
            logging.info("INDEX :: %d :: %s", self.index, e)
        
        while True:
            self.event.wait(1)
            logging.info(f"Waiting Connection ......... : {index}")
            index += 1

            if self.__bootflag:
                logging.info("###################################################################")
                logging.info("################ TPC_TCC_BOOT_DONE_UPD :: CONNECTED ################")
                logging.info("###################################################################\n\n")
                self.bootflag = True
                self.btn_boot.setEnabled(False)
                self.btn_boot.changeColor("#ccc")

                self.btn_pbit.setEnabled(True)
                self.btn_pbit.changeColor(CanonMainWindow.cyan)

                self.btn_cbit.setEnabled(True)
                self.btn_cbit.changeColor(CanonMainWindow.sky)

                self.btn_cbitperiod.setEnabled(True)
                self.btn_cbitperiod.setStyleSheet(CanonMainWindow.skyfont)

                self.btn_ibit.setEnabled(True)
                self.btn_ibit.changeColor(CanonMainWindow.purple)

                self.btn_ibitperiod.setEnabled(True)
                self.btn_ibitperiod.setStyleSheet(CanonMainWindow.purplefont)

                self.sf_reset.set_enable()
                self.sf_stop.set_enable()
                break

    def __listenSocket(self):
        logging.info("@@@@@@. UDPClient :: Start Listening .....")
        self.__stoplistening = False
        self.__receivedata = bytearray()
        try:
            while not self.__stoplistening:
                if len(self.__receivedata) == 0:
                    self.__receivedata = bytearray()
                    if self.__udpServer is not None:
                        self.__receivedata, self.__fromAddress \
                                = self.__udpServer.recvfrom(BUFFERSIZE)

                        self.__athread = threading.Thread(target=self.__action_service, args=())
                        self.__athread.start()
        except socket.timeout:
            logging.info("__listenSocket :: socket.timeout :: event")
        self.__receivedata = None

    def __action_service(self):
        message = self.__receivedata
        msg = MsgProtocol2(ptype=2)
        msg.make_protocol_header_from_msg(message)
        # logging.info("      ** DATA RECEIVED :: %s\n", msg)

        ####################################
        ######### DISPLAY MESSAGE ##########
        ####################################

        # self.updateResponse(msg.make_msgbox())
        ## self.__receivedata 를 비워야 새로운 데이터를 받을수 있음
        self.__receivedata = bytearray()

        ## FROM :: TPC_TCC_BOOT_DONE_UPD
        if msg.code == TPC_TCC_BOOT_DONE_UPD:
            if msg.ack == PACK:
                self.__bootflag = True
                ########################################################
                ## ## ACK 발송
                sendmsg = MsgProtocol2(2, msg.timeStamp, TCC_ID, TPC_ID, ACK_CODE, CACK, 1, 1, 4, [TPC_TCC_BOOT_DONE_UPD, 0])
                self.sendmsg_and_display(sendmsg, "ACK_CODE")

        ## FROM :: TPC_TCC_IBIT_DONE_UPD
        elif msg.code == TPC_TCC_IBIT_DONE_UPD:
            if msg.csum_flag:
                ########################################################
                ## ## ACK 발송
                sendmsg = MsgProtocol2(2, msg.timeStamp, TCC_ID, TPC_ID, ACK_CODE, CACK, 1, 1, 4, [TPC_TCC_IBIT_DONE_UPD, 0])
                self.sendmsg_and_display(sendmsg, "ACK_CODE")

        ## FROM :: TPC_TCC_PBIT_RESULT_REF
        elif msg.code == TPC_TCC_PBIT_RESULT_REF:
            if msg.csum_flag and msg.data:
                self.make_data_bit_result(msg.data, self.pbit_status)
                self.update_Pbit_result()

        ## FROM :: TPC_TCC_CBIT_RESULT_REF
        elif msg.code == TPC_TCC_CBIT_RESULT_REF:
            if msg.csum_flag and msg.data:
                self.make_data_bit_result(msg.data, self.cbit_status)
                self.update_Cbit_result()

        ## FROM :: TPC_TCC_IBIT_RESULT_REF
        elif msg.code == TPC_TCC_IBIT_RESULT_REF:
            if msg.csum_flag and msg.data:
                self.make_data_bit_result(msg.data, self.ibit_status)
                self.update_Ibit_result()

        ## FROM :: TPC_TCC_HARD_EMERGENCY_STOP_UPD
        elif msg.code == TPC_TCC_HARD_EMERGENCY_STOP_UPD:
            ########################################################
            ## ## ACK 발송
            sendmsg = MsgProtocol2(2, msg.timeStamp, TCC_ID, TPC_ID, ACK_CODE, CACK, 1, 1, 4, [TPC_TCC_HARD_EMERGENCY_STOP_UPD, 0])
            self.sendmsg_and_display(sendmsg, "ACK_CODE")
            # ========================================================
            self.update_hard_emergency_stop(msg.data)

        ## FROM :: TPC_TCC_CTRL_POWER_UPD
        elif msg.code == TPC_TCC_CTRL_POWER_UPD:
            ########################################################
            ## ## ACK 발송
            sendmsg = MsgProtocol2(2, msg.timeStamp, TCC_ID, TPC_ID, ACK_CODE, CACK, 1, 1, 4, [TPC_TCC_CTRL_POWER_UPD, 0])
            self.sendmsg_and_display(sendmsg, "ACK_CODE")
            # ========================================================
            self.update_control_onoff_status(msg.data)

        ## FROM :: TPC_TCC_CBIT_SET_UPD
        elif msg.code == TPC_TCC_CBIT_SET_UPD:
            if msg.csum_flag:
                if msg.ack == PACK:
                    self.cbit_setup_flag = True

        ## FROM :: TPC_TCC_STATE_INFO_REF
        elif msg.code == TPC_TCC_STATE_INFO_REF:
            # if msg.csum_flag:
                ## ACK 체크 ACK에 대한 RESPONSE 인지?
            self.tpc_status_info = msg.data
            self.update_pcs_stateinfo()

        ## FROM :: TPC_TCC_DRIVE_POWER_UPD
        elif msg.code == TPC_TCC_DRIVE_POWER_UPD:
            ########################################################
            ## ## ACK 발송
            sendmsg = MsgProtocol2(2, msg.timeStamp, TCC_ID, TPC_ID, ACK_CODE, CACK, 1, 1, 4, [TPC_TCC_DRIVE_POWER_UPD, 0])
            self.sendmsg_and_display(sendmsg, "ACK_CODE")
            # ========================================================
            self.update_upgrade_onoff_status(msg.data)

        elif msg.code == ACK_CODE:
            if msg.data:
                if msg.data[0] == TCC_TPC_PBIT_RESULT_REQ:
                    self.pbit_received_flag = True
                elif msg.data[0] == TCC_TPC_CBIT_RESULT_REQ:
                    self.cbit_setup_flag = True
                elif msg.data[0] == TCC_TPC_IBIT_RESULT_REQ:
                    self.ibit_setup_flag = True
            # logging.info("ACK RESPONSE DATA :: %s", msg)

    def make_data_bit_result(self, data, bit_status):
        for index, val in enumerate(data):
            if val:
                bits = self.bit_check(val)
                bit_status[index] = bits
            else:
                bit_status[index] = self.zeroarray

    # 2bit Check
    def bit_check(self, x):
        data = []
        for idx in range(16):
            if (x & (1<<idx)):
                data.append(1)
            else:
                data.append(0)
        return data

    def sendmsg_and_display(self, msg, ptype):
        msg.make_encode()
        self.send(msg.message, self.__fromAddress)
        # if ptype != ACK_CODE:

        ####################################
        ######### DISPLAY MESSAGE ##########
        ####################################
        # self.updateRequest(msg.make_msgbox())
        # logging.info("## REQUEST :: %s\n\n", msg)

    def send(self, data, address):
        try:
            self.__udpServer.sendto(data, address)
        except Exception as e:
            pass

    ## CMD :: TCC_TPC_PBIT_RESULT_REQ
    ## START PBIT
    def request_pbit(self):
        self.pbit_flag = True
        self.btn_state_info.changeColor(CanonMainWindow.sky)
        self.timeStamp += 1
        sendmsg = MsgProtocol2(1, self.timeStamp, TCC_ID, TPC_ID, TCC_TPC_PBIT_RESULT_REQ, PNACK, 1, 1, 0)
        self.sendmsg_and_display(sendmsg, "TCC_TPC_PBIT_RESULT_REQ")

    ## CMD : TCC_TPC_CBIT_SET_UPD
    ## CBIT 전송 주기 설정 setup_cbitperiod
    def setup_cbitperiod(self):
        cbitperiod = int(self.edit_cbitperiod.text())
        self.cbit_period = int(cbitperiod)
        self.cbit_setup_flag = True
        self.timeStamp += 1
        dataLength = 2
        period = [cbitperiod]
        msg = MsgProtocol2(1, self.timeStamp, TCC_ID, TPC_ID, TCC_TPC_CBIT_SET_UPD, PACK, 1, 1, dataLength, period)
        self.sendmsg_and_display(msg, "TCC_TPC_CBIT_SET_UPD")

    ## CMD : TCC_TPC_CBIT_RESULT_REQ
    def request_cbit(self):
        if not self.cbit_flag:
            self.cbit_flag = True
            data = [0xff]
            self.cbitflag.change_status(1)
        else:
            self.cbit_flag = False 
            data = [0x0]
            self.cbitflag.change_status(0)
        
        self.timeStamp += 1
        dataLength = 2
        sendmsg = MsgProtocol2(1, self.timeStamp, TCC_ID, TPC_ID, TCC_TPC_CBIT_RESULT_REQ, PACK, 1, 1, dataLength, data)
        self.sendmsg_and_display(sendmsg, "TCC_TPC_CBIT_RESULT_REQ")

    ## CMD : TCC_TPC_IBIT_RUN_CMD
    ## IBIT 전송 주기 설정 setup_ibitime
    def setup_ibitime(self):
        self.iflag = True
        ibitperiod = int(self.edit_ibitperiod.text())
        self.ibit_period = int(ibitperiod)
        
        self.timeStamp += 1
        dataLength = 2
        period = [ibitperiod]
        sendmsg = MsgProtocol2(1, self.timeStamp, TCC_ID, TPC_ID, TCC_TPC_IBIT_RUN_CMD, PACK, 1, 1, dataLength, period)
        self.sendmsg_and_display(sendmsg, "TCC_TPC_IBIT_RUN_CMD")
    
    ## CMD : TCC_TPC_IBIT_RESULT_REQ
    def request_ibit(self):
        self.ibitflag.change_status(1)

        self.timeStamp += 1
        sendmsg = MsgProtocol2(1, self.timeStamp, TCC_ID, TPC_ID, TCC_TPC_IBIT_RESULT_REQ, PACK, 1, 1, 0)
        self.sendmsg_and_display(sendmsg, "TCC_TPC_IBIT_RESULT_REQ")

    ## CMD :: TCC_TPC_RESET_UPD
    ## UI BUTTON COMMAND
    def setup_reset_device(self, val):
        self.timeStamp += 1
        dataLength = 2
        data = [val]
        sendmsg = MsgProtocol2(1, self.timeStamp, TCC_ID, TPC_ID, TCC_TPC_RESET_UPD, PACK, 1, 1, dataLength, data)
        self.sendmsg_and_display(sendmsg, "TCC_TPC_RESET_UPD")

    ## CMD :: TCC_TPC_SOFT_EMERGENCY_STOP_UPD
    ## UI BUTTON COMMAND
    def setup_stop_device(self, val):
        logging.info(f"setup_stop_device :: {val}")
        
        self.timeStamp += 1
        dataLength = 2
        data = [val]
        sendmsg = MsgProtocol2(1, self.timeStamp, TCC_ID, TPC_ID, TCC_TPC_SOFT_EMERGENCY_STOP_UPD, PACK, 1, 1, dataLength, data)
        self.sendmsg_and_display(sendmsg, "TCC_TPC_SOFT_EMERGENCY_STOP_UPD")
    
    ## CMD :: TCC_TPC_CTRL_POWER_CMD
    def setup_control_power(self, val):
        logging.info(f"setup_control_power :: {val}")

        self.timeStamp += 1
        dataLength = 4
        sendmsg = MsgProtocol2(1, self.timeStamp, TCC_ID, TPC_ID, TCC_TPC_CTRL_POWER_CMD, PACK, 1, 1, dataLength, [onoff, part])
        self.sendmsg_and_display(sendmsg, "TCC_TPC_CTRL_POWER_CMD")

    ## Bit update
    def update_Pbit_result(self):
        for index, data in enumerate(self.pbit_status):
            self.pbit_mods[index].update_mod(data)

    ## Bit update
    def update_Cbit_result(self):
        for index, data in enumerate(self.cbit_status):
            self.cbit_mods[index].update_mod(data)

    ## Bit update
    def update_Ibit_result(self):
        for index, data in enumerate(self.ibit_status):
            self.ibit_mods[index].update_mod(data)

    ## IBIT TIME CHECK
    def ibit_timecheck(self):
        now = datetime.now()
        secondtime = int((now - self.ibittime).total_seconds())
        # logging.info("ibit_timecheck :: %d", secondtime)
        
        if self.iflag:
            self.checktimer.stop()
            self.checktimer = None 
            return

        if secondtime >= self.ibit_period * 1.5:
            self.checktimer.stop()
            self.checktimer = None 

            dialog = WarningDialog("캘리브레이션을 할 제품의 번호를 입력하세요")
            dialog.show()
            response = dialog.exec_()

            # OK 를 하면 설정 값을 읽어와서 통신을 한다.
            if response == QDialog.Accepted:
                pass

    def update_ibit_flag(self):
        self.iflag = True

    def displayUnitLayout(self, parent_layout, btn, cbx, label, unit):
        grp_sub = QGroupBox("")
        layout_sub = QGridLayout()
        grp_sub.setLayout(layout_sub)

        label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        unit.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)

        layout_sub.addWidget(btn, 0, 0, 1, 3)
        layout_sub.addWidget(cbx, 0, 4)
        layout_sub.addWidget(label, 1, 0, 1, 3)
        layout_sub.addWidget(unit, 1, 4)
        parent_layout.addWidget(grp_sub)

    def displayMenuLayout(self, parent_layout, glabel, btn, second=None):
        grp_sub = QGroupBox(glabel)
        layout_sub = QHBoxLayout()
        grp_sub.setLayout(layout_sub)
        layout_sub.addWidget(btn)
        if second:
            layout_sub.addWidget(second)
        parent_layout.addWidget(grp_sub)

    ## SCREEN LAYOUT
    def initUI(self):
        self.setWindowTitle("PSTEK CANON SYSTEM MONITORING")
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width()-50, screensize.height()) 

        mainwidget = QWidget()                # 위젯의 인스턴스 생성만으로도 QCanonMainWindow에 붙는다.
        self.setCentralWidget(mainwidget)

        self.main_layout = QGridLayout()
        mainwidget.setLayout(self.main_layout)

        ## Main Layout 설정 ########################
        self.displayConnect()
        self.displayReset()
        self.displayRequest()
        self.displayResponse()
        self.displayPSTEKLogo()

        ## Q Tabs ########################
        tabs = QTabWidget()
        mainControlTab = QWidget()
        statusTab = QWidget()
        subPcsInfoTab = QWidget()
        tabs.addTab(mainControlTab, 'MAIN CONTROL')
        tabs.addTab(statusTab, 'STATUS')
        tabs.addTab(subPcsInfoTab, 'PCS STATUS INFO')
        self.displayMain(mainControlTab)
        self.displayTPCStatus(statusTab)
        self.displayTPCStatusDetail(subPcsInfoTab)

        tabCbit = QWidget()
        tab_control_cbit = QWidget()
        tab_power_cbit1 = QWidget()
        tab_power_cbit2 = QWidget()
        tabs.addTab(tabCbit, 'MAIN CBIT')
        tabs.addTab(tab_control_cbit, '전원조절기')
        tabs.addTab(tab_power_cbit1, '승압기 #1')
        tabs.addTab(tab_power_cbit2, '승압기 #2')
        
        tabPbit = QWidget()
        tab_control_pbit = QWidget()
        tab_power_pbit1 = QWidget()
        tab_power_pbit2 = QWidget()
        tabs.addTab(tabPbit, 'MAIN PBIT')
        tabs.addTab(tab_control_pbit, '전원조절기')
        tabs.addTab(tab_power_pbit1, '승압기 #1')
        tabs.addTab(tab_power_pbit2, '승압기 #2')

        tabIbit = QWidget()
        tab_control_ibit1 = QWidget()
        tab_power_ibit1 = QWidget()
        tab_power_ibit2 = QWidget()
        tabs.addTab(tabIbit, 'MAIN IBIT')
        tabs.addTab(tab_control_ibit1, '전원조절기')
        tabs.addTab(tab_power_ibit1, '승압기 #1')
        tabs.addTab(tab_power_ibit2, '승압기 #2')
        
        self.displayControlSubMod(tab_control_pbit, self.pbit_mods, self.pbitControlSubs)
        self.displayPowerSubMod1(tab_power_pbit1, self.pbit_mods, self.pbitPowerSubs)
        self.displayPowerSubMod2(tab_power_pbit2, self.pbit_mods, self.pbitPowerSubs)
        self.displayMainSummary(tabPbit, self.pbitControlSubs, self.pbitPowerSubs)

        self.displayControlSubMod(tab_control_cbit, self.cbit_mods, self.cbitControlSubs)
        self.displayPowerSubMod1(tab_power_cbit1, self.cbit_mods, self.cbitPowerSubs)
        self.displayPowerSubMod2(tab_power_cbit2, self.cbit_mods, self.cbitPowerSubs)
        self.displayMainSummary(tabCbit, self.cbitControlSubs, self.cbitPowerSubs)

        self.displayControlSubMod(tab_control_ibit1, self.ibit_mods, self.ibitControlSubs)
        self.displayPowerSubMod1(tab_power_ibit1,  self.ibit_mods, self.ibitPowerSubs)
        self.displayPowerSubMod2(tab_power_ibit2,  self.ibit_mods, self.ibitPowerSubs)
        self.displayMainSummary(tabIbit, self.ibitControlSubs, self.ibitPowerSubs)

        self.main_layout.addWidget(tabs, 1, 0, 1, 6)

        ## Status BAR ########################
        self.statusbar = QStatusBar()
        self.setStatusBar(self.statusbar)
        self.statusbar.setObjectName("statusbar")
        
        self.statusmessage = 'PSTEK, {},  {}'
        now = QDate.currentDate()
        displaymessage = self.statusmessage.format(now.toString(Qt.ISODate), 'Ready !!')
        self.statusbar.showMessage(displaymessage)

        self.show()

    #################################################
    ## Main Display
    #################################################
    def displayMainSummary(self, tab, controlSubs, powerSubs):
        # logging.info(f"displayMainSummary :: {powerSubs}")
        main_layout = QHBoxLayout()
        tab.setLayout(main_layout)

        controlgroup = QGroupBox("전원조절기")
        controllayout = QGridLayout()
        controlgroup.setLayout(controllayout)

        index = 0
        for subname, mods in controlSubs.items():
            group = QGroupBox(subname)
            layout = QHBoxLayout()
            group.setLayout(layout)

            for mod in mods:
                layout.addWidget(mod.btn)

            controllayout.addWidget(group)
            index += 1

        main_layout.addWidget(controlgroup)

        powergroup = QGroupBox("승압기")
        powerlayout = QGridLayout()
        powergroup.setLayout(powerlayout)

        index = 0
        for subname, mods in powerSubs.items():
            group = QGroupBox(subname)
            layout = QHBoxLayout()
            group.setLayout(layout)

            for mod in mods:
                layout.addWidget(mod.btn)

            powerlayout.addWidget(group)
            index += 1
        
        main_layout.addWidget(powergroup)

    def layoutStatus2(self, layout, row, col, edit, name, comment1, comment2=None):
        subgroup = QGroupBox(name)
        sublayout = QVBoxLayout()
        subgroup.setLayout(sublayout)

        # sublayout.addWidget(QLabel(name))
        sublayout.addWidget(edit)
        sublayout.addWidget(QLabel(comment1))
        if comment2:
            sublayout.addWidget(QLabel(comment2))

        layout.addWidget(subgroup, row, col)

    ## MAIN CONTROL
    # displayMain
    def displayMain(self, tab):
        main_layout = QHBoxLayout()
        tab.setLayout(main_layout)

        ## TAB 0 :: 1st column
        group_pcsibit = self.mainVersion()
        main_layout.addWidget(group_pcsibit)

        ## TAB 0 :: 2nd column
        grp_controller = self.mainControlStart()
        main_layout.addWidget(grp_controller)

        ## TAB 0 :: 3rd column
        grp_controller_module = self.mainControlStatus()
        main_layout.addWidget(grp_controller_module)

        # ## TAB 0 :: 4rd column
        # grp_lowpower = self.displayLowPower()
        # main_layout.addWidget(grp_lowpower)

        ## TAB 0 :: 5th column
        grp_upgradevol = self.mainPowerStart()
        main_layout.addWidget(grp_upgradevol)

        ## TAB 0 :: 6th column
        grp_controller_upvol = self.mainPowerStatus()
        main_layout.addWidget(grp_controller_upvol)

    ## TAB 0 :: 1st column
    def mainVersion(self):
        group = QGroupBox("PCS 상태 정보")
        layout = QGridLayout()
        group.setLayout(layout)

        ## 0 th
        self.cbitflag = Status("CBIT 전송 Flag", "미전송 상태", "전송상태")
        group_cbitflag = self.cbitflag.display()
        layout.addWidget(group_cbitflag, 0, 0)

        ## 1th
        self.ibitflag = Status("IBIT 수행 상태", "미전송 상태", "수행중")
        group_ibitflag = self.ibitflag.display()
        layout.addWidget(group_ibitflag, 1, 0)

        grp_hwstop = QGroupBox("H/W STOP")
        layout_hwstop = QHBoxLayout()
        grp_hwstop.setLayout(layout_hwstop)

        self.btn_hwstop = CLed("", 80)
        layout_hwstop.addWidget(self.btn_hwstop)

        layout.addWidget(grp_hwstop, 2, 0)
        
        return group
    
    def update_hard_emergency_stop(self, data):
        if data[0] == 1:
            text = "H/W STOP"
        elif data[0] == 2:
            text = "Release"
        else:
            text = "Unknown"
        self.btn_hwstop.changeColorText("red", text)

    ## 1-1 포탑 전원 조절기
    def mainControlStart(self):
        grp_controller = QGroupBox("포탑 전원 조절기")
        layout_controller = QGridLayout()
        grp_controller.setLayout(layout_controller)

        ######################################3
        ## 포탑 전원 조절기 
        ######################################3
        grp_onoff = QGroupBox("ON / OFF 선택")
        layout_onoff  = QHBoxLayout()
        grp_onoff.setLayout(layout_onoff)

        self.rd_power_on = QRadioButton("ON")
        self.rd_power_on.clicked.connect(lambda:self.radioBtnClicked("CONTROL", 'ON'))
        self.rd_power_off = QRadioButton("OFF")
        self.rd_power_off.clicked.connect(lambda:self.radioBtnClicked("CONTROL", 'OFF'))
        layout_onoff.addWidget(self.rd_power_on)
        layout_onoff.addWidget(self.rd_power_off)
        layout_controller.addWidget(grp_onoff)

        ## 부분 선택
        grp_part = QGroupBox("부분 선택")
        layout_control_parts  = QVBoxLayout()
        grp_part.setLayout(layout_control_parts)
        self.control_parts = []
        self.control_val = None

        for part, name in TPC_PARTS2.items():
            rd_btn = QRadioButton(name)
            rd_btn.clicked.connect(lambda:self.radioBtnClicked("CPART", part))
            layout_control_parts.addWidget(rd_btn)
            self.control_parts.append(rd_btn)
        layout_controller.addWidget(grp_part)
        
        ## Button 
        grp_btn = QGroupBox("")
        layout_btn  = QHBoxLayout()
        grp_btn.setLayout(layout_btn)

        self.btn_control_cmd = QPushButton("전송")
        self.btn_control_cmd.setEnabled(False)
        self.btn_control_cmd.clicked.connect(self.control_onoff_cmd)
        layout_btn.addWidget(self.btn_control_cmd)
        layout_controller.addWidget(grp_btn)

        # Table
        grp_msg = QGroupBox("")
        layout_msg  = QHBoxLayout()
        grp_msg.setLayout(layout_msg)
        
        self.tableMsg = QTableWidget()
        self.tableMsg.setColumnCount(1)
        self.tableMsg.setEditTriggers(QAbstractItemView.NoEditTriggers)

        column_headers = ['MESSAGE']
        self.tableMsg.setHorizontalHeaderLabels(column_headers)
        self.tableMsg.setRowCount(0)

        header = self.tableMsg.horizontalHeader()       
        header.setSectionResizeMode(0, QHeaderView.Stretch)

        layout_msg.addWidget(self.tableMsg)
        layout_controller.addWidget(grp_msg)

        return grp_controller
    
    ## CMD :: TCC_TPC_CTRL_POWER_CMD
    # control_onoff_cmd
    def control_onoff_cmd(self):
        part = None
        val = None
        for fd in self.control_parts:
            if fd.isChecked():
                part = fd.text()
                break

        if part:
            for key, value in TPC_PARTS2.items():
                if part == value:
                    val = key
                    break
        if val:
            logging.info(f"control_onoff_cmd :: {self.controlonoff}, {val}")

            self.timeStamp += 1
            dataLength = 4
            data = [self.controlonoff, val]
            sendmsg = MsgProtocol2(1, self.timeStamp, TCC_ID, TPC_ID, TCC_TPC_CTRL_POWER_CMD, PACK, 1, 1, dataLength, data)
            self.sendmsg_and_display(sendmsg, "TCC_TPC_CTRL_POWER_CMD")


    def mainControlStatus(self):
        group = QGroupBox("포탑 전원 조절기 운영 상태")
        layout = QGridLayout()
        group.setLayout(layout)

        # 0x01 : ON완료
        # 0x02 : OFF완료
        # 0x03 : ON미완료(실패)
        # 0x04 : OFF미완료(실패)"

        ######################################3
        ## 포탑 전원 조절기 
        ######################################3
        self.control_onoff = OnOffStatus('포탑 전원 조절기 상태', 'ON완료', 'OFF완료', 'ON미완료(실패)', 'OFF미완료(실패)')
        grp_onoff = self.control_onoff.display()
        layout.addWidget(grp_onoff)

        # 0x01 : 포탄제어기 (PCU)
        # 0x02 : 장약제어기 (MCCU)
        # 0x03 : 장전제어기
        # 0x04 : 포/포탑 구동제어기 (GTCU)
        # 0x05 : 무장제어기 (CCU)
        # 0xFF : ALL"

        ## 부분 선택
        self.control_parts_onoff = OnOffPartStatus('대상 장치', '포탄제어기 (PCU)', '장약제어기 (MCCU)', '장전제어기', '포/포탑 구동제어기 (GTCU)', '무장제어기 (CCU)')
        grp_part = self.control_parts_onoff.display()
        layout.addWidget(grp_part)

        return group
    
    ## FEEDBACK : TPC_TCC_CTRL_POWER_UPD
    def update_control_onoff_status(self, data):
        onoff = data[0]
        parts = data[1]
        self.control_onoff.set_value(onoff)
        self.control_parts_onoff.set_value(parts)

    ## TAB 0 :: 3nd column
    def mainControlStatusBackup(self):
        group = QGroupBox("포탑 전원조절기")
        layout = QVBoxLayout()
        group.setLayout(layout)

        self.tcc_mod1 = Status("모듈1상태", "미사용(스위치 차단 상태)", "동작중(정상 운용가능 상태)", "정지(고장 등 비정상 상태)")
        group_tcc_mod1 = self.tcc_mod1.display()
        layout.addWidget(group_tcc_mod1)

        self.tcc_mod2 = Status("모듈2상태", "미사용(스위치 차단 상태)", "동작중(정상 운용가능 상태)", "정지(고장 등 비정상 상태)")
        group_tcc_mod2 = self.tcc_mod2.display()
        layout.addWidget(group_tcc_mod2)

        self.tcc_mod3 = Status("모듈3상태", "미사용(스위치 차단 상태)", "동작중(정상 운용가능 상태)", "정지(고장 등 비정상 상태)")
        group_tcc_mod3 = self.tcc_mod3.display()
        layout.addWidget(group_tcc_mod3)

        return group
    
    ## TAB 0 :: 4th column
    def displayLowPower(self):
        group = QGroupBox("저전원 출력상태")
        layout = QVBoxLayout()
        group.setLayout(layout)

        self.tcc_lowpo1 = Status("포/포탑구동장치", "미출력", "켬 진행중", "출력", "끔 진행중")
        group_tcc_lowpo1 = self.tcc_lowpo1.display2row()
        layout.addWidget(group_tcc_lowpo1)

        self.tcc_lowpo2 = Status("포탄적재이송장치", "미출력", "켬 진행중", "출력", "끔 진행중")
        group_tcc_lowpo2 = self.tcc_lowpo2.display2row()
        layout.addWidget(group_tcc_lowpo2)

        self.tcc_lowpo3 = Status("장약적재이송장치", "미출력", "켬 진행중", "출력", "끔 진행중")
        group_tcc_lowpo3 = self.tcc_lowpo3.display2row()
        layout.addWidget(group_tcc_lowpo3)

        self.tcc_lowpo4 = Status("탄약장전장치", "미출력", "켬 진행중", "출력", "끔 진행중")
        group_tcc_lowpo4 = self.tcc_lowpo4.display2row()
        layout.addWidget(group_tcc_lowpo4)

        self.tcc_lowpo5 = Status("포미개폐장치", "미출력", "켬 진행중", "출력", "끔 진행중")
        group_tcc_lowpo5 = self.tcc_lowpo5.display2row()
        layout.addWidget(group_tcc_lowpo5)

        self.tcc_lowpo6 = Status("항법장치", "미출력", "켬 진행중", "출력", "끔 진행중")
        group_tcc_lowpo6 = self.tcc_lowpo6.display2row()
        layout.addWidget(group_tcc_lowpo6)

        self.tcc_lowpo7 = Status("자동시한장입장치", "미출력", "켬 진행중", "출력", "끔 진행중")
        group_tcc_lowpo7 = self.tcc_lowpo7.display2row()
        layout.addWidget(group_tcc_lowpo7)

        self.tcc_lowpo8 = Status("통신장치(무전기)", "미출력", "켬 진행중", "출력", "끔 진행중")
        group_tcc_lowpo8 = self.tcc_lowpo8.display2row()
        layout.addWidget(group_tcc_lowpo8)

        return group

    ## TAB 0 :: 5th column
    def mainPowerStart(self):
        grp_controller = QGroupBox("포탑 승압기")
        layout_controller = QGridLayout()
        grp_controller.setLayout(layout_controller)

        ## Sub 전원 통제 
        grp_onoff = QGroupBox("ON / OFF 선택")
        layout_onoff  = QHBoxLayout()
        grp_onoff.setLayout(layout_onoff)

        self.rd_upvol_on = QRadioButton("ON")
        self.rd_upvol_on.clicked.connect(lambda:self.radioBtnClicked("POWER", 'ON'))
        self.rd_upvol_off = QRadioButton("OFF")
        self.rd_upvol_off.clicked.connect(lambda:self.radioBtnClicked("POWER", 'OFF'))
        layout_onoff.addWidget(self.rd_upvol_on)
        layout_onoff.addWidget(self.rd_upvol_off)
        layout_controller.addWidget(grp_onoff)

        ## 
        grp_part = QGroupBox("부분 선택")
        layout_upgrade_parts  = QVBoxLayout()
        grp_part.setLayout(layout_upgrade_parts)
        self.power_parts = []
        self.upgrade_val = None 

        for part, name in TPC_PARTS2.items():
            rd_btn = QRadioButton(name)
            rd_btn.clicked.connect(lambda:self.radioBtnClicked("PPART", part))
            self.power_parts.append(rd_btn)
            layout_upgrade_parts.addWidget(rd_btn)

        layout_controller.addWidget(grp_part)
        
        # Button 
        grp_btn = QGroupBox("")
        layout_btn  = QHBoxLayout()
        grp_btn.setLayout(layout_btn)

        self.btn_upgrade_cmd = QPushButton("전송")
        self.btn_upgrade_cmd.setEnabled(False)
        self.btn_upgrade_cmd.clicked.connect(self.upgrade_onoff_cmd)
        layout_btn.addWidget(self.btn_upgrade_cmd)
        layout_controller.addWidget(grp_btn)

        # Table
        grp_msg = QGroupBox("")
        layout_msg  = QHBoxLayout()
        grp_msg.setLayout(layout_msg)

        self.tableUpVolMsg = QTableWidget()
        self.tableUpVolMsg.setColumnCount(1)
        self.tableUpVolMsg.setEditTriggers(QAbstractItemView.NoEditTriggers)

        column_headers = ['MESSAGE']
        self.tableUpVolMsg.setHorizontalHeaderLabels(column_headers)
        self.tableUpVolMsg.setRowCount(0)

        header = self.tableUpVolMsg.horizontalHeader()       
        header.setSectionResizeMode(0, QHeaderView.Stretch)

        layout_msg.addWidget(self.tableUpVolMsg)
        layout_controller.addWidget(grp_msg)

        return grp_controller
    
    def radioBtnClicked(self, ptype, val):
        logging.info(f"radioBtnClicked :: {ptype} :: {val}")
        if ptype == "CONTROL":
            if val == 'ON':
                self.controlonoff = ON
            else:
                self.controlonoff = OFF

            if self.bootflag and self.controlonoff and self.control_val:
                self.btn_control_cmd.setEnabled(True)

        if ptype == 'CPART':
            self.control_val = val

            if self.bootflag and self.controlonoff and self.control_val:
                self.btn_control_cmd.setEnabled(True)

        if ptype == "POWER":
            if val == 'ON':
                self.upgradeonoff = ON
            else:
                self.upgradeonoff = OFF

            if self.bootflag and self.upgradeonoff and self.upgrade_val:
                self.btn_upgrade_cmd.setEnabled(True)

        if ptype == 'PPART':
            self.upgrade_val = val

            if self.bootflag and self.upgradeonoff and self.upgrade_val:
                self.btn_upgrade_cmd.setEnabled(True)
    
    ## CMD :: TCC_TPC_DRIVE_POWER_CMD
    # upgrade_onoff_cmd
    def upgrade_onoff_cmd(self):
        part = None
        val = None
        for fd in self.power_parts:
            if fd.isChecked():
                part = fd.text()
                break

        if part:
            for key, value in TPC_PARTS2.items():
                if part == value:
                    val = key
                    break
        if val:
            logging.info(f"upgrade_onoff_cmd :: {self.upgradeonoff}, {val}")
            self.timeStamp += 1
            dataLength = 4
            data = [self.upgradeonoff, val]
            sendmsg = MsgProtocol2(1, self.timeStamp, TCC_ID, TPC_ID, TCC_TPC_DRIVE_POWER_CMD, PACK, 1, 1, dataLength, data)
            self.sendmsg_and_display(sendmsg, "TCC_TPC_DRIVE_POWER_CMD")

    
    def mainPowerStatus(self):
        group = QGroupBox("승압기 운영 상태")
        layout = QGridLayout()
        group.setLayout(layout)

        # 0x01 : ON완료
        # 0x02 : OFF완료
        # 0x03 : ON미완료(실패)
        # 0x04 : OFF미완료(실패)"

        ######################################3
        ## 승압기 
        ######################################3
        self.power_onoff = OnOffStatus('승압기 상태', 'ON완료', 'OFF완료', 'ON미완료(실패)', 'OFF미완료(실패)')
        grp_onoff = self.power_onoff.display()
        layout.addWidget(grp_onoff)

        # 0x01 : 포탄제어기 (PCU)
        # 0x02 : 장약제어기 (MCCU)
        # 0x03 : 장전제어기
        # 0x04 : 포/포탑 구동제어기 (GTCU)
        # 0x05 : 무장제어기 (CCU)
        # 0xFF : ALL"

        ## 부분 선택
        self.power_parts_onoff = OnOffPartStatus('대상 장치', '포탄제어기 (PCU)', '장약제어기 (MCCU)', '장전제어기', '포/포탑 구동제어기 (GTCU)', '무장제어기 (CCU)')
        grp_part = self.power_parts_onoff.display()
        layout.addWidget(grp_part)

        return group
    
    ## FEEDBACK : TPC_TCC_DRIVE_POWER_UPD
    def update_upgrade_onoff_status(self, data):
        onoff = data[0]
        parts = data[1]
        self.power_onoff.set_value(onoff)
        self.power_parts_onoff.set_value(parts)

    ## TAB 0 :: 6th column
    def mainPowerStatusBackup(self):
        group = QGroupBox("포탑 승압기")
        layout = QVBoxLayout()
        group.setLayout(layout)

        self.upvol_mod1 = Status("모듈1상태", "미사용(스위치 차단 상태)", "동작중(정상 운용가능 상태)", "정지(고장 등 비정상 상태)")
        group_upvol_mod1 = self.upvol_mod1.display()
        layout.addWidget(group_upvol_mod1)

        self.upvol_mod2 = Status("모듈2상태", "미사용(스위치 차단 상태)", "동작중(정상 운용가능 상태)", "정지(고장 등 비정상 상태)")
        group_upvol_mod2 = self.upvol_mod2.display()
        layout.addWidget(group_upvol_mod2)

        self.upvol_mod3 = Status("모듈3상태", "미사용(스위치 차단 상태)", "동작중(정상 운용가능 상태)", "정지(고장 등 비정상 상태)")
        group_upvol_mod3 = self.upvol_mod3.display()
        layout.addWidget(group_upvol_mod3)

        self.upvol_mod4 = Status("모듈4상태", "미사용(스위치 차단 상태)", "동작중(정상 운용가능 상태)", "정지(고장 등 비정상 상태)")
        group_upvol_mod4 = self.upvol_mod4.display()
        layout.addWidget(group_upvol_mod4)

        return group
    
    def displayTPCStatus(self, tab):
        status_layout = QHBoxLayout()
        tab.setLayout(status_layout)
        self.status_mods = []

        for key, pfields in TPCSTATUS.items():
            smod = SModule(key)
            for pfield in pfields:
                if pfield['ftype'] == 'Status':
                    field = Status(pfield['name'], "OFF", "ON")
                else:
                    field = PField(pfield['name'])
                # logging.info(field)
                smod.fields.append(field)
                self.tpc_status.append(field)
            
            self.status_mods.append(smod)

        for idx, mod in enumerate(self.status_mods):
            group = mod.display()
            status_layout.addWidget(group)
            
    def displayTPCStatusDetail(self, tab):
        main_layout = QGridLayout()
        tab.setLayout(main_layout)

        mods = []

        index = 0
        for subsys, submods in STATUS_INFO.items():
            subsystem = SubSystem(subsys)
            subs = []

            for sub in submods:
                for subname, funs in sub.items():

                    mod = Module(subname)
                    mods.append(mod)
                    subs.append(mod)

                    for fun in funs:
                        field = PField(fun)
                        # logging.info(field)
                        mod.fields.append(field)
                        self.tpc_fields.append(field)

                    subsystem.add_module(mod)

            group = subsystem.displayField()
            main_layout.addWidget(group, 0, index)
            index += 1

    ## PCS State
    def update_pcs_stateinfo(self):
        if self.tpc_status_info:
            data = self.tpc_status_info
            self.tpc_status_info = None

            for index, dt in enumerate(data[:23]):
                self.tpc_status[index].change_status(dt)
                # logging.info(f"update_pcs_stateinfo :: {st} :: {dt}")

            for index, dt in enumerate(data[23:]):
                if dt:
                    self.tpc_fields[index].set_value(dt)

    # 전원 조절기 인터페이스 
    def displayControlSubMod(self, tab, mods, subsystems):
        main_layout = QHBoxLayout()
        tab.setLayout(main_layout)

        subsysnum = 0
        self.nth = 0
        index = 1

        ## Layout 을 위한 모듈 
        for subsys, submods in CONTROL_NAME.items():
            subsystem = SubSystem(subsys)
            subs = []

            ## Sub Module 
            for sub in submods:
                for subname, funs in sub.items():
                    mod = Module(subname)
                    mods.append(mod)
                    subs.append(mod)

                    ## Function 단위 
                    for fun in funs:
                        if fun['ftype'] == 'OneStatus':
                            field = OneStatus(mod, fun['name'])
                        else:
                            if subname.startswith('차단정보상세'):
                                field = Field(mod, fun['name'], 0, "정상", "차단")
                            else:
                                field = Field(mod, fun['name'], 0)
                        mod.add_field(field)
                        index += 1

                    subsystem.add_module(mod)
            subsystems[subsys] = subs

            subsysnum += 1

            group = subsystem.display()
            main_layout.addWidget(group)

    # displayPowerSubMod #1
    def displayPowerSubMod1(self, tab, mods, subsystems):
        main_layout = QHBoxLayout()
        tab.setLayout(main_layout)

        subsysnum = 0
        index = 1

        ## Layout 을 위한 모듈 
        for subsys, submods in POWER_NAME.items():
            subsystem = SubSystem(subsys)
            subs = []
            ## Sub Module 
            for sub in submods:
                for subname, funs in sub.items():
                    mod = Module(subname)
                    mods.append(mod)
                    subs.append(mod)

                    ## Function 단위 
                    for fun in funs:
                        if fun['ftype'] == 'OneStatus':
                            field = OneStatus(mod, fun['name'])
                        else:
                            if subname.startswith('차단정보상세'):
                                field = Field(mod, fun['name'], 0, "정상", "차단")
                            else:
                                field = Field(mod, fun['name'], 0)
                        mod.add_field(field)
                        index += 1

                    subsystem.add_module(mod)
            subsystems[subsys] = subs

            subsysnum += 1

            group = subsystem.display()
            main_layout.addWidget(group)

            if subsysnum == 5:
                break

    # displayPowerSubMod #2
    def displayPowerSubMod2(self, tab, mods, subsystems):
        main_layout = QHBoxLayout()
        tab.setLayout(main_layout)

        subsysnum = 0
        index = 1

        ## Layout 을 위한 모듈 
        for subsys, submods in POWER_NAME.items():

            if subsysnum < 5:
                subsysnum += 1
                continue

            subsystem = SubSystem(subsys)
            subs = []
            ## Sub Module 
            for sub in submods:
                for subname, funs in sub.items():
                    mod = Module(subname)
                    mods.append(mod)
                    subs.append(mod)

                    ## Function 단위 
                    for fun in funs:
                        if fun['ftype'] == 'OneStatus':
                            field = OneStatus(mod, fun['name'])
                        else:
                            if subname.startswith('차단정보상세'):
                                field = Field(mod, fun['name'], 0, "정상", "차단")
                            else:
                                field = Field(mod, fun['name'], 0)
                        mod.add_field(field)
                        index += 1

                    subsystem.add_module(mod)
            subsystems[subsys] = subs

            subsysnum += 1

            group = subsystem.display()
            main_layout.addWidget(group)

    ## Menu Connect
    def displayConnect(self):
        grp_conn = QGroupBox("연결 확인")
        layout_conn = QGridLayout()
        grp_conn.setLayout(layout_conn)

        lbl_address = QLabel("포탑전력변환장치(IP)")
        lbl_port = QLabel("포트(Port)")
        lbl_cbitperiod = QLabel("CBIT 전송주기(m sec)")
        lbl_ibitperiod = QLabel("IBIT 전송주기(m sec)")

        self.edit_address = QLineEdit(TCC_HOST_IP)
        self.edit_port = QLineEdit(str(TCC_HOST_PORT))
        self.edit_cbitperiod = QLineEdit(str(self.cbit_period))
        self.edit_ibitperiod = QLineEdit(str(self.ibit_period))

        self.btn_boot = CLed("BOOT", 50)
        self.btn_boot.changeColor("#ccffff")
        self.btn_boot.clicked.connect(self.run_pcs_boot)

        self.btn_pbit = CLed("PBIT", 50)
        self.btn_pbit.setEnabled(False)
        self.btn_pbit.clicked.connect(self.request_pbit)

        self.btn_cbit = CLed("CBIT", 50)
        self.btn_cbit.setEnabled(False)
        self.btn_cbit.clicked.connect(self.request_cbit)

        self.btn_ibit = CLed("iBIT", 50)
        self.btn_ibit.setEnabled(False)
        self.btn_ibit.clicked.connect(self.request_ibit)

        self.btn_cbitperiod = QPushButton("CBIT 주기 설정")
        self.btn_cbitperiod.setEnabled(False)
        self.btn_cbitperiod.setStyleSheet(CanonMainWindow.grayfont)
        self.btn_cbitperiod.clicked.connect(self.setup_cbitperiod)

        self.btn_ibitperiod = QPushButton("IBIT 주기 설정")
        self.btn_ibitperiod.setEnabled(False)
        self.btn_ibitperiod.setStyleSheet(CanonMainWindow.grayfont)
        self.btn_ibitperiod.clicked.connect(self.setup_ibitime)

        layout_conn.addWidget(lbl_address, 0, 0)
        layout_conn.addWidget(self.edit_address, 0, 1)

        layout_conn.addWidget(self.btn_boot, 0, 2, 2, 1)
        layout_conn.addWidget(self.btn_pbit, 0, 3, 2, 1)

        layout_conn.addWidget(lbl_port, 1, 0)
        layout_conn.addWidget(self.edit_port, 1, 1)

        layout_conn.addWidget(lbl_cbitperiod, 2, 0)
        layout_conn.addWidget(self.edit_cbitperiod, 2, 1)
        layout_conn.addWidget(self.btn_cbitperiod, 2, 2)
        layout_conn.addWidget(self.btn_cbit, 2, 3)

        layout_conn.addWidget(lbl_ibitperiod, 3, 0)
        layout_conn.addWidget(self.edit_ibitperiod, 3, 1)
        layout_conn.addWidget(self.btn_ibitperiod, 3, 2)
        layout_conn.addWidget(self.btn_ibit, 3, 3)

        self.main_layout.addWidget(grp_conn, 0, 0)

    def displayReset(self):
        ## RESET & STOP
        grp_reset = QGroupBox("RESET & STOP")
        layout_reset = QVBoxLayout()
        grp_reset.setLayout(layout_reset)

        self.sf_reset = SField(self, "RESET", "FAULT", 1, "CPU", 2)
        self.sf_stop = SField(self, "STOP", "STOP", 1, "RELEASE", 2)

        group1 = self.sf_reset.display()
        group2 = self.sf_stop.display()

        layout_reset.addWidget(group1)
        layout_reset.addWidget(group2)

        self.main_layout.addWidget(grp_reset, 0, 1)

        grp_info = QGroupBox("STATUS INFO")
        layout_info = QVBoxLayout()
        grp_info.setLayout(layout_info)

        self.btn_state_info = CLed("STATUS INFO", 80)
        self.btn_state_info.clicked.connect(self.request_tpc_status_info)
       
        layout_info.addWidget(self.btn_state_info)

        self.main_layout.addWidget(grp_info, 0, 2)

    ## CMD :: TCC_TPC_STATE_INFO_REQ
    ## STATUS
    def request_tpc_status_info(self):
        self.btn_state_info.setEnabled(False)
        self.pcstimer = QtCore.QTimer()
        self.pcstimer.timeout.connect(self.request_status)
        self.pcstimer.start(5000)

    def request_status(self):
        self.timeStamp += 1
        sendmsg = MsgProtocol2(1, self.timeStamp, TCC_ID, TPC_ID, TCC_TPC_STATE_INFO_REQ, PNACK, 1, 1, 0)
        self.sendmsg_and_display(sendmsg, "TCC_TPC_STATE_INFO_REQ")

    ## Menu Request
    def displayRequest(self):
        grp_command = QGroupBox("명령메시지")
        layout_command = QVBoxLayout()
        grp_command.setLayout(layout_command)

        ## display Table
        self.tableQ = QTableWidget()
        self.tableQ.setColumnCount(6)
        self.tableQ.setEditTriggers(QAbstractItemView.NoEditTriggers)

        column_headers = ['SID', 'DID', 'CODE', 'ACK', 'DATA LEN', 'DATA']
        self.tableQ.setHorizontalHeaderLabels(column_headers)
        self.tableQ.setRowCount(0)

        header = self.tableQ.horizontalHeader()       
        header.setSectionResizeMode(0, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(1, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(2, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(2, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(4, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(5, QHeaderView.Stretch)

        # layout_command.addWidget(self.edit_request)
        layout_command.addWidget(self.tableQ)
        self.main_layout.addWidget(grp_command, 0, 3)

    ## Display Request Info
    def updateRequest(self, msg):
        msgBox = msg.split(':')
        rowPosition = self.tableQ.rowCount()
        code = TCC_CODE2.get(msgBox[2], 'NOT REGISTED OR RESPONSE')
        self.tableQ.insertRow(rowPosition)
        self.tableQ.setItem(rowPosition, 0, QTableWidgetItem(msgBox[0]))
        self.tableQ.setItem(rowPosition, 1, QTableWidgetItem(msgBox[1]))
        self.tableQ.setItem(rowPosition, 2, QTableWidgetItem(code))
        self.tableQ.setItem(rowPosition, 3, QTableWidgetItem(msgBox[3]))
        self.tableQ.setItem(rowPosition, 4, QTableWidgetItem(msgBox[4]))
        self.tableQ.setItem(rowPosition, 5, QTableWidgetItem(msgBox[5]))

        if int(msgBox[4]):
            rowPosition = self.tableQ.rowCount()
            self.tableQ.insertRow(rowPosition)
            self.tableQ.setItem(rowPosition, 1, QTableWidgetItem("PAYLOAD"))
            self.tableQ.setItem(rowPosition, 2, QTableWidgetItem(msgBox[5]))

        self.tableQ.resizeColumnsToContents()
        self.tableQ.resizeRowsToContents()
        self.tableQ.selectRow(rowPosition)

    ## Menu Response
    def displayResponse(self):
        grp_command = QGroupBox("응답메시지")
        layout_command = QVBoxLayout()
        grp_command.setLayout(layout_command)

        ## display Table
        self.tableRes = QTableWidget()
        self.tableRes.setColumnCount(6)
        self.tableRes.setEditTriggers(QAbstractItemView.NoEditTriggers)

        column_headers = ['SID', 'DID', 'CODE', 'ACK', 'DATA LEN', 'DATA']
        self.tableRes.setHorizontalHeaderLabels(column_headers)
        self.tableRes.setRowCount(0)

        header = self.tableRes.horizontalHeader()       
        header.setSectionResizeMode(0, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(1, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(2, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(3, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(4, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(5, QHeaderView.Stretch)

        layout_command.addWidget(self.tableRes)
        self.main_layout.addWidget(grp_command, 0, 4)

    ## Display Response Result
    def updateResponse(self, msg):
        msgBox = msg.split(':')
        # logging.info(f"updateResponse :: {msg} : {type(msgBox[2])}")
        rowPosition = self.tableRes.rowCount()
        code = TPC_CODE2.get(msgBox[2], 'NOT REGISTED OR RESPONSE')
        self.tableRes.insertRow(rowPosition)
        self.tableRes.setItem(rowPosition, 0, QTableWidgetItem(msgBox[0]))
        self.tableRes.setItem(rowPosition, 1, QTableWidgetItem(msgBox[1]))
        self.tableRes.setItem(rowPosition, 2, QTableWidgetItem(code))
        self.tableRes.setItem(rowPosition, 3, QTableWidgetItem(msgBox[3]))
        self.tableRes.setItem(rowPosition, 4, QTableWidgetItem(msgBox[4]))
        self.tableRes.setItem(rowPosition, 5, QTableWidgetItem(msgBox[5]))

        if int(msgBox[4]):
            rowPosition = self.tableRes.rowCount()
            self.tableRes.insertRow(rowPosition)
            self.tableRes.setItem(rowPosition, 1, QTableWidgetItem("PAYLOAD"))
            self.tableRes.setItem(rowPosition, 2, QTableWidgetItem(msgBox[5]))

        self.tableRes.resizeColumnsToContents()
        self.tableRes.resizeRowsToContents()
        self.tableRes.selectRow(rowPosition)

    ## Display Response Result
    def updateNormalMessage(self, msg):
        rowPosition = self.tableMsg.rowCount()
        self.tableMsg.insertRow(rowPosition)
        self.tableMsg.setItem(rowPosition, 0, QTableWidgetItem(msg))

        self.tableMsg.resizeColumnsToContents()
        self.tableMsg.resizeRowsToContents()
        self.tableMsg.selectRow(rowPosition)

    ## Display Response Result
    def updateHighMessage(self, msg):
        rowPosition = self.tableHighMsg.rowCount()
        self.tableHighMsg.insertRow(rowPosition)
        self.tableHighMsg.setItem(rowPosition, 0, QTableWidgetItem(msg))

        self.tableHighMsg.resizeColumnsToContents()
        self.tableHighMsg.resizeRowsToContents()
        self.tableHighMsg.selectRow(rowPosition)

    ## LOGO
    def displayPSTEKLogo(self):
        # Logo Image
        labelLogo = QLabel("")
        pixmap = QPixmap(resource_path("logo.png"))
        labelLogo.setAlignment(Qt.AlignRight)
        labelLogo.setPixmap(pixmap)
        self.main_layout.addWidget(labelLogo, 0, 5)

    def LayoutRadioBoxGrid(self, layout_grid, row, label, rdb1, rdb2, btn=None):
        layout_grid.addWidget(MyLabel(label), row, 0)
        layout_grid.addWidget(rdb1, row, 1)
        layout_grid.addWidget(rdb2, row, 2)
        if btn:
            layout_grid.addWidget(btn, row, 3)

    def LayoutDropCombBoxGrid(self, layout_grid, row, label, comb, btn=None):
        layout_grid.addWidget(MyLabel(label), row, 0)
        layout_grid.addWidget(comb, row, 1)
        if btn:
            layout_grid.addWidget(btn, row, 2)

        if isinstance(comb, QSpinBox):
            comb.setRange(0, 1000)
            comb.setValue(0)

    # Keyboard Event
    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Escape:
            pass
        elif e.key() == Qt.Key_F:
            self.showFullScreen()
        elif e.key() == Qt.Key_N:
            self.showNormal()

    def closeEvent(self, event):
        try:
            sys.exit()
        except Exception as e:
            logging.info(f"closeEvent ERROR :: {e}")

if __name__ == "__main__":
    app = QApplication(sys.argv)
    form = CanonMainWindow()
    sys.exit(app.exec_())