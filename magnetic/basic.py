
from ctypes import c_int16


from PyQt5.QtWidgets import QGroupBox, QGridLayout, QLabel
from .common import DisplayOnly, CheckField
from .common import DataDisplay, DataUpdateRange, DataUpdateSingle

class HLink:
    def __init__(self):
        self.hv = DataDisplay('DC LINK HV', 0, 'V', vmin=0, vmax=350, ratio=10)
        self.lv = DataDisplay('DC LINK LV', 0, 'V', vmin=0, vmax=100, ratio=10)
        self.freq = DataDisplay('PULSE FREQ', 0, 'kHz', vmin=0.5, vmax=10, ratio=10)
        
        self.errtime = DataDisplay('COMM ERR TIME', 0, 'sec', vmin=0, vmax=100)
        self.onoffmode = CheckField('ON/OFF MODE', 'OFF', 'ON', 0)
        self.ipaddress = DisplayOnly('IP ADDRESS', "0.0.0.0")

        self.ovplevel1 = DataDisplay('VDC1 OVP LEVEL', 0, 'V', vmin=0, vmax=430, ratio=10)
        self.ovplevel2 = DataDisplay('VDC2 OVP LEVEL', 0, 'V', vmin=0, vmax=150, ratio=10)
        self.romin = DataDisplay('RO MIN FAULT LEVEL', 0, 'Ω', vmin=0, vmax=800)
        self.pgain = DataDisplay('P GAIN', 0, '', vmin=0, vmax=800)
        self.igain = DataDisplay('I GAIN', 0, '', vmin=0, vmax=800)
        self.reserved = DataDisplay('Reserved', 0, '', vmin=0, vmax=800)
        self.model = DisplayOnly('MODEL', 0)
        self.version = DisplayOnly('Version', 0)

    def update(self, data):
        self.hv.setValue(data[0])
        self.lv.setValue(data[1])
        self.freq.setValue(data[2])

        self.errtime.setValue(data[43])
        self.onoffmode.setValue(data[44])

        ipaddress = f"{data[45]}.{data[46]}.{data[47]}.{data[48]}"
        self.ipaddress.setValue(ipaddress)

        self.ovplevel1.setValue(data[49])
        self.ovplevel2.setValue(data[50])
        self.romin.setValue(data[51])
        self.pgain.setValue(data[52])
        self.igain.setValue(data[53])
        self.reserved.setValue(data[54])
        self.model.setValue(data[55])
        self.version.setValue(data[56])

    def display(self):
        grp_state = QGroupBox()
        layout_state = QGridLayout()
        grp_state.setLayout(layout_state)

        self.hv.display(layout_state, 0)
        self.lv.display(layout_state, 1)
        self.freq.display(layout_state, 2)

        self.errtime.display(layout_state, 4)
        self.onoffmode.display(layout_state, 5)

        self.ovplevel1.display(layout_state, 7)
        self.ovplevel2.display(layout_state, 8)
        # self.romin.display(layout_state, 9)
        self.pgain.display(layout_state, 10)
        self.igain.display(layout_state, 11)
        self.reserved.display(layout_state, 12)
        self.model.display(layout_state, 13)
        self.version.display(layout_state, 14)

        self.ipaddress.display(layout_state, 15)

        return grp_state


class State:
    def __init__(self, name, dmax=50):
        self.name = name
        self.duty = DataDisplay('DUTY', 0, '%', vmin=0, vmax=dmax, ratio=10)
        self.current = DataDisplay('CURRENT', 0, 'A', vmin=-20, vmax=20, ratio=100)

    def update(self, data):
        self.duty.setValue(data[0])
        val_current = c_int16(data[1]).value
        self.current.setValue(val_current)

    def display(self):
        grp_state = QGroupBox(self.name)
        layout_state = QGridLayout()
        grp_state.setLayout(layout_state)

        self.duty.display(layout_state, 0)
        self.current.display(layout_state, 1)
        return grp_state

    def gridDisplay(self, layout, row, col):
        label = QLabel(self.name)
        label.setStyleSheet('QLabel{font-size: 12pt; font-weight: bold}')
        layout.addWidget(label, row, col, 1, 6)
        self.duty.gridDisplay(layout, row+1, col)
        self.current.gridDisplay(layout, row+2, col)


class MDStatus:
    def __init__(self, name, bcolor="#fff"):
        self.name = name
        self.state1 = State('State #1', dmax=100)
        self.state2 = State('State #2')
        self.state3 = State('State #3')
        self.state4 = State('State #4')
        self.state5 = State('State #5')

        self.bcolor = bcolor
        self.main_label = "QLabel{font-size: 14pt; font-weight: bold}"

    def update_module(self, mod):
        self.state1.update(mod[:2])
        self.state2.update(mod[2:4])
        self.state3.update(mod[4:6])
        self.state4.update(mod[6:8])
        self.state5.update(mod[8:10])

    def display(self):
        grp_status = QGroupBox(self.name)
        grp_status.setStyleSheet(f"background-color: {self.bcolor}")
        layout_status = QGridLayout()
        grp_status.setLayout(layout_status)

        group1 = self.state1.display()
        layout_status.addWidget(group1, 0, 0)
        group2 = self.state2.display()
        layout_status.addWidget(group2, 0, 1)
        group3 = self.state3.display()
        layout_status.addWidget(group3, 1, 0)
        group4 = self.state4.display()
        layout_status.addWidget(group4, 1, 1)
        group5 = self.state5.display()
        layout_status.addWidget(group5, 2, 0)

        return grp_status

    def displayNoGroup(self):
        grp_status = QGroupBox(self.name)
        grp_status.setStyleSheet(f"background-color: {self.bcolor}")
        layout_status = QGridLayout()
        grp_status.setLayout(layout_status)

        group1 = self.state1.gridDisplay(layout_status, 0, 0)
        group2 = self.state2.gridDisplay(layout_status, 0, 6)
        group3 = self.state3.gridDisplay(layout_status, 3, 0)
        group4 = self.state4.gridDisplay(layout_status, 3, 6)
        group5 = self.state5.gridDisplay(layout_status, 6, 0)

        return grp_status


class VUState:
    def __init__(self, parent, name, address, dmax=50.0):
        self.parent = parent
        self.name = name
        self.address = address
        self.duty = DataUpdateRange(self, 'DUTY', address, 0, '%', vmin=0.0, vmax=dmax, ratio=10)
        self.current = DataUpdateRange(self, 'CURRENT', address+1, 0, 'A', vmin=-20.00, vmax=20.00, ratio=100)

    def update(self, data):
        self.duty.setValue(data[0])
        val_current = c_int16(data[1]).value
        self.current.setValue(val_current)

    def display(self):
        grp_state = QGroupBox(self.name)
        layout_state = QGridLayout()
        grp_state.setLayout(layout_state)

        self.duty.display(layout_state, 0)
        self.current.display(layout_state, 1)
        return grp_state

    def gridDisplay(self, layout, row, col):
        label = QLabel(self.name)
        label.setStyleSheet('QLabel{font-size: 12pt; font-weight: bold}')
        layout.addWidget(label, row, col, 1, 7)

        self.duty.gridDisplay(layout, row+1, col)
        self.current.gridDisplay(layout, row+2, col)


class VUModule:
    def __init__(self, parent, name, address, loadadd, bcolor="#fff"):
        self.parent = parent
        self.name = name
        self.state1 = VUState(self, 'State #1', address, dmax=100.0)
        self.state2 = VUState(self, 'State #2', address+2)
        self.state3 = VUState(self, 'State #3', address+4)
        self.state4 = VUState(self, 'State #4', address+6)
        self.state5 = VUState(self, 'State #5', address+8)
        self.load_indu = DataUpdateSingle(self, 'LOAD_INDUCTANCE', loadadd, 0, 'uH', 1, 9999)

        self.bcolor = bcolor
        self.main_label = "QLabel{font-size: 14pt; font-weight: bold}"

    def update_module(self, mod):
        self.state1.update(mod[:2])
        self.state2.update(mod[2:4])
        self.state3.update(mod[4:6])
        self.state4.update(mod[6:8])
        self.state5.update(mod[8:10])

    def display(self):
        grp_status = QGroupBox(self.name)
        grp_status.setStyleSheet(f"background-color: {self.bcolor}")
        layout_status = QGridLayout()
        grp_status.setLayout(layout_status)

        group1 = self.state1.display()
        layout_status.addWidget(group1, 0, 0)
        group2 = self.state2.display()
        layout_status.addWidget(group2, 0, 1)
        group3 = self.state3.display()
        layout_status.addWidget(group3, 1, 0)
        group4 = self.state4.display()
        layout_status.addWidget(group4, 1, 1)
        group5 = self.state5.display()
        layout_status.addWidget(group5, 2, 0)

        group6 = self.load_indu.groupdisplay()
        layout_status.addWidget(group6, 2, 1)
        return grp_status

    def displayNoGroup(self):
        grp_status = QGroupBox(self.name)
        grp_status.setStyleSheet(f"background-color: {self.bcolor}")
        layout_status = QGridLayout()
        grp_status.setLayout(layout_status)

        self.state1.gridDisplay(layout_status, 0, 0)
        self.state2.gridDisplay(layout_status, 0, 7)
        self.state3.gridDisplay(layout_status, 3, 0)
        self.state4.gridDisplay(layout_status, 3, 7)
        self.state5.gridDisplay(layout_status, 6, 0)

        self.load_indu.gridDisplay(layout_status, 6, 7)

        return grp_status


class ModuleStatus:
    def __init__(self, name, bcolor="#fff"):
        self.name = name 
        self.bcolor = bcolor

        self.label_status = CheckField('Status', '정상', '경고', 0)
        self.label_fault = DisplayOnly("Fault", "")

        self.vo_ch1 = DataDisplay('Voltage Ch #1', 0, 'V', vmin=0, vmax=350, ratio=10)
        self.vo_ch2 = DataDisplay('Voltage Ch #2', 0, 'V', vmin=0, vmax=100, ratio=10)
        self.cur_st1 = DataDisplay('Pulse IO State #1', 0, 'A', vmin=-20, vmax=20, ratio=100)
        self.cur_st2 = DataDisplay('Pulse IO State #2', 0, 'A', vmin=-20, vmax=20, ratio=100)
        self.cur_st3 = DataDisplay('Pulse IO State #3', 0, 'A', vmin=-20, vmax=20, ratio=100)
        self.cur_st4 = DataDisplay('Pulse IO State #4', 0, 'A', vmin=-20, vmax=20, ratio=100)
        self.cur_st5 = DataDisplay('Pulse IO State #5', 0, 'A', vmin=-20, vmax=20, ratio=100)
        self.sw = DisplayOnly('S/W Version', 0)
        self.load_indu = DataDisplay('Load Inductance', 0, 'uH', vmin=1, vmax=9999)


    def status_normal(self):
        self.label_status.change_normal()


    def status_warning(self):
        self.label_status.change_warning()


    def change_value(self, data):
        # print("ModuleStatus :: change_value :: ", self.name, data)
        try:
            if data[0]:
                self.label_status.change_warning()
            else:
                self.label_status.change_normal()

            if data[1]:
                self.label_fault.setValue(data[1])

            self.vo_ch1.setValue(data[2])
            self.vo_ch2.setValue(data[3])
            
            st1_val = c_int16(data[4]).value
            self.cur_st1.setValue(st1_val)

            st2_val = c_int16(data[5]).value
            self.cur_st2.setValue(st2_val)

            st3_val = c_int16(data[6]).value
            self.cur_st3.setValue(st3_val)

            st4_val = c_int16(data[7]).value
            self.cur_st4.setValue(st4_val)

            st5_val = c_int16(data[8]).value
            self.cur_st5.setValue(st5_val)

            self.sw.setValue(data[9])
            self.load_indu.setValue(data[10])

        except Exception as e:
            print(e)

    def display(self):
        grp_module = QGroupBox(self.name)
        grp_module.setStyleSheet(f"background-color: {self.bcolor}")
        layout_module = QGridLayout()
        grp_module.setLayout(layout_module)

        self.label_status.display(layout_module, 0)
        self.label_fault.display(layout_module, 1)

        self.vo_ch1.display(layout_module, 2)
        self.vo_ch2.display(layout_module, 3)
        self.cur_st1.display(layout_module, 4)
        self.cur_st2.display(layout_module, 5)
        self.cur_st3.display(layout_module, 6)
        self.cur_st4.display(layout_module, 7)
        self.cur_st5.display(layout_module, 8)
        self.sw.display(layout_module, 9)
        self.load_indu.display(layout_module, 10)

        return grp_module

    def reset(self):
        self.vo_ch1.reset()
        self.vo_ch2.reset()
        self.cur_st1.reset()
        self.cur_st2.reset()
        self.cur_st3.reset()
        self.cur_st4.reset()
        self.cur_st5.reset()
        self.sw.reset()
        self.load_indu.reset()


class DeviceSetting:
    def __init__(self, name):
        self.name = name
        self.remote = CheckField("Remote/Local", "Local", "Remote")
        self.ready = CheckField("Stand by", "Off", "Ready")
        self.pulse = CheckField("Pulse Ready", "Off", "Pulse Out")
        self.sync = CheckField("Sync Lock", "Not OK", "Sync OK")
        self.warning = CheckField("WARNING INDICATOR", "NO", "WARNING")
        self.fault = CheckField("FAULT INDICATOR", "NO", "FAULT")

    def display(self):
        group = QGroupBox(self.name)
        layout = QGridLayout()
        group.setLayout(layout)

        self.remote.display(layout, 0)
        self.ready.display(layout, 1)
        self.pulse.display(layout, 2)
        self.sync.display(layout, 3)
        self.warning.display(layout, 4)
        self.fault.display(layout, 5)

        return group

    def status_update(self, condition):
        if 0 in condition:
            self.remote.change_warning()
        else:
            self.remote.change_normal()

        if 1 in condition:
            self.ready.change_warning()
        else:
            self.ready.change_normal()

        if 2 in condition:
            self.pulse.change_warning()
        else:
            self.pulse.change_normal()

        if 3 in condition:
            self.sync.change_warning()
        else:
            self.sync.change_normal()

        if 6 in condition:
            self.warning.change_warning()
        else:
            self.warning.change_normal()

        if 7 in condition:
            self.fault.change_warning()
        else:
            self.fault.change_normal()

                
class ModuleFault:
    cyanfont = "border: 3px solid lightgray;border-radius: 10px;background-color:#ccffff;color: black;font-size: 14px;font-weight: bold;"
    yellowfont = "border: 3px solid lightgray;border-radius: 10px;background-color: #FFC300;color: black;font-size: 14px;font-weight: bold;"
    redfont = "border: 3px solid lightgray;border-radius: 10px;background-color: #FF0000;color: black;font-size: 14px;font-weight: bold;"

    def __init__(self, name, bcolor="#fff"):
        self.name = name 
        self.bcolor = bcolor
        self.fields = []

    def add_field(self, field):
        self.fields.append(field)

    def display(self):
        group = QGroupBox(self.name)
        group.setStyleSheet(f"background-color: {self.bcolor}")
        layout = QGridLayout()
        group.setLayout(layout)

        for idx, field in enumerate(self.fields):
            layout.addWidget(field.lbl_name, idx, 0)
            layout.addWidget(field.rd_status1, idx, 1)
            layout.addWidget(field.rd_status2, idx, 2)

        return group

    def update_warning(self, faults):
        for idx, val in enumerate(faults):
            self.fields[val].change_warning()

    def reset(self):
        for idx, field in enumerate(self.fields):
            field.reset()

    def __str__(self):
        return f"#### ModuleFault = {self.name}"