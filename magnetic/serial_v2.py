import serial
import random
import socket
import logging
from umodbus import conf
from umodbus.client import tcp
from ctypes import c_uint16

from .address_v2 import *


## RTU
PTYPE = 'RTU'
# PTYPE = 'tcp'

if PTYPE == 'tcp':
    PORT = 1502
    PTYPE = 'tcp'

UNIT = 0x1
RUN_VALUE = 2
STOP_VALUE = 0

def connect_rtu(port, ptype='rtu', speed=38400, bytesize=8, parity='N', stopbits=1):
    from pymodbus.client.sync import ModbusSerialClient as ModbusClient
    client = ModbusClient(method=ptype, port=port, timeout=1,
                      baudrate=speed, bytesize=bytesize, parity=parity, stopbits=stopbits)
    client.connect()

    if client:
        print('*'*50)
        print("******************* RTU DUN SUCCESS *********************", client)
        print('*'*50)

    return client


def connect_tcp(ipaddress='192.168.1.30', ipport=5000):
    conf.SIGNED_VALUES = True
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        client.connect((ipaddress, ipport))
        if client:
            print('#'*50)
            print(f"******************* TCP SUCCESS :: {client}  *********************")
            print('#'*50)
        return client
    except Exception as e:
        print(e)
        return None


def make_qrport(port):
    client = serial.Serial(port)
    return client


def read_data_v2(client, ptype):
    data = {}
    # ################### 테스트용 ###################
    # test = []
    # for idx in range(75):
    #     test.append(random.randint(100, 280))
    # data['mod12345'] = test
    # data['working'] = True
    # return data

    ################### REAL ###################
    if ptype == 'RTU':
        try:
            mod12345 = read_registers(client, ptype, READ_MD1_STATE1_DUTY, 75)
            if mod12345:
                data['mod12345'] = mod12345
                data['working'] = True 
        except Exception as e:
            logging.info("##################### RTU :: Error read_input_registers : %s", e)
            data['mod12345'] = False
            data['working'] = False 
    else:
        try:
            message = tcp.read_input_registers(slave_id=1, 
                        starting_address=READ_MD1_STATE1_DUTY, quantity=75)
            mod12345 = tcp.send_message(message, client)
            if mod12345:
                data['mod12345'] = mod12345
                data['working'] = True 
        except Exception as e:
            logging.info("##################### TCP :: Error read_input_registers : %s", e)
            data['mod12345'] = False
            data['working'] = False 
    # logging.info(f"#### read_data_v2 :: {data}")
    return data


def read_graphdata(client, ptype):
    data = {}
    if ptype == 'RTU':
        try:
            result1 = []
            for idx in range(7):
                setting = read_registers(client, ptype, MOD1_GRAPH + (idx * 50), 50)
                result1.extend(setting)
            data['mod1'] = result1

            result2 = []
            for idx in range(7):
                setting = read_registers(client, ptype, MOD2_GRAPH + (idx * 50), 50)
                result2.extend(setting)
            data['mod2'] = result2

            result3 = []
            for idx in range(7):
                setting = read_registers(client, ptype, MOD3_GRAPH + (idx * 50), 50)
                result3.extend(setting)
            data['mod3'] = result3

            result4 = []
            for idx in range(7):
                setting = read_registers(client, ptype, MOD4_GRAPH + (idx * 50), 50)
                result4.extend(setting)
            data['mod4'] = result4

            data['working'] = True

            return data

        except Exception as e:
            logging.info("** RTU :: Error read_input_registers : %s", e)
            data['working'] = False
    else:
        try:
            message = tcp.read_input_registers(slave_id=1, 
                        starting_address=READ_VOLTAGE1, quantity=10)
            result = tcp.send_message(message, client)

        except Exception as e:
            logging.info("## TCP :: Error read_input_registers : %s", e)

    return data


def read_registers(client, ptype, address, count):
    try:
        response = client.read_input_registers(address, count, unit=UNIT)
        result = response.registers

        # ## Simulation
        # result = []
        # for idx in range(count):
        #     result.append(randint(1, 100))

    except Exception as e:
        print("** RTU :: Error read_input_registers : ", e)
        result = []
    return result


def read_fault_v2(client, ptype):
    ################### 테스트용 ###################
    # test = []
    # vals = [1, 2, 4, 12, 16, 32, 64]
    # flag = False
    # for idx in range(10):
    #     rd = random.randint(1000, 9000)
    #     if rd > 8800:
    #         flag = True
    #         test.append(random.choice(vals))
    #         logging.info("          !!!! FAULT")
    #     else:
    #         test.append(0)
    # if flag:
    #     print("GET FAULT :: ", test)
    # return test

    ################### REAL ###################
    if ptype == 'RTU':
        try:
            response = client.read_input_registers(SETTING, 10, unit=UNIT)
            result = response.registers
            return result
        except Exception as e:
            print("** RTU :: Error read_fault_from_device : ", e)
            return []
    else:
        try:
            message = tcp.read_input_registers(slave_id=1, 
                        starting_address=SETTING, quantity=10)
            result = tcp.send_message(message, client)
            return result
        
        except Exception as e:
            print("## TCP :: Error read_input_registers : ", e)
            return []


# Write Register
def write_registers(client, address, val, ptype):
    result = []
    if ptype == 'RTU':
        try:
            value = c_uint16(val).value
            result = client.write_registers(address, value, unit=UNIT)
        except Exception as e:
            result = []
            logging.info("RTU :: Error write_registers : %s", e)
    else:
        try:
            # values = [c_uint16(val).value]
            values = []
            values.append(val)
            message = tcp.write_multiple_registers(slave_id=1, 
                        starting_address=address, values=values)
            result = tcp.send_message(message, client)
            logging.info("TCP WRITE :: %s :: %s :: RESULT :: %s \n\n", address, values, result)
        except Exception as e:
            result = 0
            logging.info("TCP :: Error write_registers : %s", e)

    return result

