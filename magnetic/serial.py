import serial

import socket
from umodbus import conf
from umodbus.client import tcp
from ctypes import c_uint16

from .address import *


## RTU
PTYPE = 'RTU'
# PTYPE = 'tcp'

if PTYPE == 'tcp':
    PORT = 1502
    PTYPE = 'tcp'

UNIT = 0x1
RUN_VALUE = 2
STOP_VALUE = 0

def connect_rtu(port, ptype='rtu', speed=38400, bytesize=8, parity='N', stopbits=1):
    from pymodbus.client.sync import ModbusSerialClient as ModbusClient
    client = ModbusClient(method=ptype, port=port, timeout=1,
                      baudrate=speed, bytesize=bytesize, parity=parity, stopbits=stopbits)
    client.connect()

    if client:
        print('*'*50)
        print("******************* RTU DUN SUCCESS *********************", port, client)
        print('*'*50)

    return client

def connect_tcp(ipaddress='192.168.10.100', ipport=5000):
    conf.SIGNED_VALUES = True
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect((ipaddress, ipport))

    if client:
        print('#'*50)
        print(f"******************* TCP {client} SUCCESS *********************")
        print('#'*50)

    return client


def make_qrport(port):
    client = serial.Serial(port)
    return client

# Device Main Data 읽어오기 
def read_data(client, ptype):
    data = {}
    if ptype == 'RTU':
        try:
            setting = read_registers(client, ptype, SET_DC_LINK_HV, 57)
            if setting:
                data['setting'] = setting
                mod1234 = read_registers(client, ptype, MOD1_System_Status, 48)
                data['mod1234'] = mod1234
                data['working'] = True 
                
            else:
                data['working'] = False
                data['error'] = setting['result']
            return data

        except Exception as e:
            print("** RTU :: Error read_input_registers : ", e)
    else:
        try:
            message = tcp.read_input_registers(slave_id=1, 
                        starting_address=READ_VOLTAGE1, quantity=10)
            result = tcp.send_message(message, client)

        except Exception as e:
            print("## TCP :: Error read_input_registers : ", e)

    return data


def read_data_v2(client, ptype):
    data = {}
    if ptype == 'RTU':
        try:
            setting = read_registers(client, ptype, SET_DC_LINK_HV, 57)
            if setting:
                data['setting'] = setting
                mod1234 = read_registers(client, ptype, MOD1_System_Status, 48)
                data['mod1234'] = mod1234
                data['working'] = True 
                
            else:
                data['working'] = False
                data['error'] = setting['result']
            return data

        except Exception as e:
            print("** RTU :: Error read_input_registers : ", e)
    else:
        try:
            message = tcp.read_input_registers(slave_id=1, 
                        starting_address=READ_VOLTAGE1, quantity=10)
            result = tcp.send_message(message, client)

        except Exception as e:
            print("## TCP :: Error read_input_registers : ", e)

    return data

def read_graphdata(client, ptype):
    data = {}
    if ptype == 'RTU':
        try:
            result1 = []
            for idx in range(7):
                setting = read_registers(client, ptype, MOD1_GRAPH + (idx * 50), 50)
                result1.extend(setting)
            data['mod1'] = result1

            result2 = []
            for idx in range(7):
                setting = read_registers(client, ptype, MOD2_GRAPH + (idx * 50), 50)
                result2.extend(setting)
            data['mod2'] = result2

            result3 = []
            for idx in range(7):
                setting = read_registers(client, ptype, MOD3_GRAPH + (idx * 50), 50)
                result3.extend(setting)
            data['mod3'] = result3

            result4 = []
            for idx in range(7):
                setting = read_registers(client, ptype, MOD4_GRAPH + (idx * 50), 50)
                result4.extend(setting)
            data['mod4'] = result4

            data['working'] = True

            return data

        except Exception as e:
            print("** RTU :: Error read_input_registers : ", e)
            data['working'] = False
    else:
        try:
            message = tcp.read_input_registers(slave_id=1, 
                        starting_address=READ_VOLTAGE1, quantity=10)
            result = tcp.send_message(message, client)

        except Exception as e:
            print("## TCP :: Error read_input_registers : ", e)

    return data


def read_registers(client, ptype, address, count):
    try:
        response = client.read_input_registers(address, count, unit=UNIT)
        result = response.registers

        # ## Simulation
        # result = []
        # for idx in range(count):
        #     result.append(randint(1, 1000))

    except Exception as e:
        print("** RTU :: Error read_input_registers : ", e)
        result = []
    return result


def read_fault(client, ptype):
    if ptype == 'RTU':
        try:
            response = client.read_input_registers(SETTING, 8, unit=UNIT)
            result = response.registers
            return result

            # # simulation
            # result = [15]
            # faults = [1, 2, 4, 8, 16, 32, 64]
            # for idx in range(7):
            #     num = randint(1, 1000)
            #     if num > 900:
            #         result.append(choices(faults)[0])
            #     else:
            #         result.append(0)
            # return result

        except Exception as e:
            print("** RTU :: Error read_fault_from_device : ", e)
            return []
    else:
        try:
            message = tcp.read_input_registers(slave_id=1, 
                        starting_address=READ_VOLTAGE1, quantity=10)
            result = tcp.send_message(message, client)
            return result
        except Exception as e:
            print("## TCP :: Error read_input_registers : ", e)
            return []


# Write Register
def write_registers(client, address, val, ptype):
    print("WRITE : ", address, val, ptype, "\n\n")
    value = c_uint16(val).value
    if ptype == 'RTU':
        try:
            result = client.write_registers(address, value, unit=UNIT)
        except Exception as e:
            result = []
            print("Error write_registers : ", e)
    else:
        message = tcp.write_single_register(slave_id=1, 
                    address=address, value=val)
        result = tcp.send_message(message, client)

    return result

