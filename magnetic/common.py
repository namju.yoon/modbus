import os
import sys
import logging

from PyQt5.QtWidgets import QWidget, QLabel, QPushButton, QDoubleSpinBox, QSpinBox
from PyQt5.QtWidgets import QCheckBox, QHBoxLayout, QGroupBox, QLineEdit, QGridLayout
from PyQt5.QtCore import Qt

import pyqtgraph as pyGraph

from ctypes import c_uint16

NORMAL = 0
WARNING = 1

def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)


class WarningLabel(QLabel):
    def __init__(self, *args, **kwargs):
        super(WarningLabel, self).__init__(*args, **kwargs)
        self.setFixedHeight(15)
        self.setMaximumHeight(15)
        self.setStyleSheet('margin: -1px; padding: 0px; font-size: 11pt; color: red;background-color: white')


class CheckField:
    grayfont = "color: #222;"
    blackfont = "color: #000;font-weight: bold;"
    bluefont = "color: #0000ff;"
    yellowfont = "color: #FFC300;font-weight: bold;"
    redfont = "color: #ff0000;font-weight: bold;"

    def __init__(self, labelname, status1, status2, value=0):
        self.labelname = labelname
        self.status1 = status1 
        self.status2 = status2 
        self.value = value

        self.lbl_name = QLabel(labelname)
        self.lbl_name.setStyleSheet(CheckField.blackfont)
        self.rd_status1 = QCheckBox(self.status1)
        self.rd_status1.setStyleSheet(CheckField.bluefont)
        self.rd_status1.setChecked(True)
        self.rd_status1.setEnabled(False)

        self.rd_status2 = QCheckBox(self.status2)
        self.rd_status2.setEnabled(False)

    def setValue(self, value):
        self.value = value

    def change_normal(self):
        self.value = NORMAL
        self.rd_status1.setChecked(True)
        self.rd_status1.setStyleSheet(CheckField.bluefont)

        self.rd_status2.setChecked(False)
        self.rd_status2.setStyleSheet(CheckField.grayfont)

    def change_warning(self):
        self.value = WARNING
        self.rd_status1.setChecked(False)
        self.rd_status1.setStyleSheet(CheckField.grayfont)

        self.rd_status2.setChecked(True)
        self.rd_status2.setStyleSheet(CheckField.redfont)

    def reset(self):
        # logging.info(f"CheckField :: reset :: {self.labelname}")
        self.rd_status1.setChecked(False)
        self.rd_status1.setStyleSheet(CheckField.grayfont)

        self.rd_status2.setChecked(False)
        self.rd_status2.setStyleSheet(CheckField.grayfont)

    def change_status(self, val):
        self.value = val
        try:
            if val == 0:
                self.change_normal()
            elif val == 1:
                self.change_warning()

        except Exception as e:
            logging.debug(e)


    def display(self, layout, nth):
        layout.addWidget(self.lbl_name, nth, 0)
        layout.addWidget(self.rd_status1, nth, 1)
        layout.addWidget(self.rd_status2, nth, 2)

    def displayGrid(self, layout, nth, col):
        layout.addWidget(self.lbl_name, nth, col)
        layout.addWidget(self.rd_status1, nth, col+1)
        layout.addWidget(self.rd_status2, nth, col+2)

    def groupdisplay(self):
        group = QGroupBox("")
        layout = QHBoxLayout()
        group.setLayout(layout)

        layout.addWidget(self.lbl_name)
        layout.addWidget(self.rd_status1)
        layout.addWidget(self.rd_status2)

        return group


class DataIntUpdate(QWidget):
    def __init__(self, parent, name, address, value, 
                unit, vmin=0, vmax=10000):
        super(DataIntUpdate, self).__init__()
        self.parent = parent
        self.address = address
        self.value = value 
        self.vmin = vmin 
        self.vmax = vmax 

        btncolor = "QPushButton{font-size: 10pt; font-weight: bold; color: blue}"
        red = 'margin: -1px;font-size: 11pt;padding: 0px; color: red'
        
        self.name_block = QLabel(name)
        self.unit_block = QLabel(unit)
        self.range_block = QLabel(f'{vmin} ~ {vmax}')
        # self.range_block.setStyleSheet('font-size: 6pt;')

        self.edit_block = QLineEdit()
        self.edit_block.setStyleSheet(red)
        self.edit_block.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self.edit_block.textEdited.connect(self.value_changed)
        self.edit_block.setText(str(self.vmin))
        
        self.btn_block = QPushButton("변 경")
        self.btn_block.setStyleSheet(btncolor)
        self.btn_block.clicked.connect(self.modifyDeviceValue)
        self.btn_block.setMaximumWidth(80)

    def display(self, layout, nth):
        layout.addWidget(self.name_block, nth, 0, 1, 3)
        layout.addWidget(self.edit_block, nth, 3, 1, 2)
        layout.addWidget(self.unit_block, nth, 5)
        layout.addWidget(self.btn_block, nth, 6)
        layout.addWidget(self.range_block, nth, 7)

    def setValue(self, val):
        self.value = val
        self.edit_block.setValue(val)

    def value_changed(self):
        try:
            val = float(self.edit_block.text())
        except Exception as e:
            print(e)
            val = self.vmin
            
        if val > self.vmax:
            self.edit_block.setText(str(self.vmax))

        if val < self.vmin:
            self.edit_block.setText(str(self.vmin))

    def modifyDeviceValue(self):
        val = int(self.edit_block.text())
        print("modifyDeviceValue : ", val)
        self.parent.modifyDeviceValue(self.address, val)


    def groupdisplay(self):
        group = QGroupBox("")
        layout = QHBoxLayout()
        group.setLayout(layout)

        layout.addWidget(self.name_block)
        layout.addWidget(self.edit_block)
        layout.addWidget(self.unit_block)
        layout.addWidget(self.btn_block)
        layout.addWidget(self.range_block)

        return group

    def gridDisplay(self, layout, row, col):
        layout.addWidget(self.name_block, row, col, 1, 2)
        layout.addWidget(self.edit_block, row, col+2, 1, 2)
        layout.addWidget(self.unit_block, row, col+4)
        layout.addWidget(self.btn_block, row, col+5)
        layout.addWidget(self.range_block, row, col+6)


class DataUpdate(QWidget):
    def __init__(self, parent, name, address, value, 
                unit, vmin=0, vmax=10000):
        super(DataUpdate, self).__init__()
        self.parent = parent
        self.address = address
        self.value = value 
        self.vmin = vmin 
        self.vmax = vmax 

        btncolor = "QPushButton{font-size: 10pt; font-weight: bold; color: blue}"
        red = 'margin: -1px;font-size: 11pt;padding: 0px; color: red'
        
        self.name_block = QLabel(name)
        self.unit_block = QLabel(unit)
        self.range_block = QLabel(f'{vmin} ~ {vmax}')
        # self.range_block.setStyleSheet('font-size: 6pt;')

        self.edit_block = QLineEdit()
        self.edit_block.setStyleSheet(red)
        self.edit_block.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self.edit_block.textEdited.connect(self.value_changed)
        self.edit_block.setText(str(self.vmin))
        
        self.btn_block = QPushButton("변 경")
        self.btn_block.setStyleSheet(btncolor)
        self.btn_block.clicked.connect(self.modifyDeviceValue)
        self.btn_block.setMaximumWidth(80)

    def display(self, layout, nth):
        layout.addWidget(self.name_block, nth, 0, 1, 3)
        layout.addWidget(self.edit_block, nth, 3, 1, 2)
        layout.addWidget(self.unit_block, nth, 5)
        layout.addWidget(self.btn_block, nth, 6)
        layout.addWidget(self.range_block, nth, 7)

    def setValue(self, val):
        self.value = round(val / 10, 1)
        self.edit_block.setText(str(val))

    def value_changed(self):
        try:
            val = float(self.edit_block.text())
        except Exception as e:
            print(e)
            val = self.vmin

        if val > self.vmax:
            self.edit_block.setText(str(self.vmax))

        if val < self.vmin:
            self.edit_block.setText(str(self.vmin))

    def modifyDeviceValue(self):
        val = float(self.edit_block.text())
        val10 = int(val * 10)
        print("modifyDeviceValue : ", val10)
        self.parent.modifyDeviceValue(self.address, val10)


    def groupdisplay(self):
        group = QGroupBox("")
        layout = QHBoxLayout()
        group.setLayout(layout)

        layout.addWidget(self.name_block)
        layout.addWidget(self.edit_block)
        layout.addWidget(self.unit_block)
        layout.addWidget(self.btn_block)
        layout.addWidget(self.range_block)

        return group

    def gridDisplay(self, layout, row, col):
        layout.addWidget(self.name_block, row, col, 1, 2)
        layout.addWidget(self.edit_block, row, col+2, 1, 2)
        layout.addWidget(self.unit_block, row, col+4)
        layout.addWidget(self.btn_block, row, col+5)
        layout.addWidget(self.range_block, row, col+6)


class DataDisplay(QWidget):
    def __init__(self, name, value="", 
                unit="", vmin=0, vmax=10000, ratio=1):
        
        super(DataDisplay, self).__init__()

        blue = 'margin: -1px;font-size: 11pt;padding: 0px; color: blue'

        self.name = name
        self.name_block = QLabel(name)
        self.unit_block = QLabel(unit)
        self.range_block = QLabel(f'{vmin} ~ {vmax}')
        # self.range_block.setStyleSheet('font-size: 6pt;')
        self.vmin = vmin 
        self.vmax = vmax 
        self.ratio = ratio

        self.edit_block = QDoubleSpinBox()
        self.edit_block.setStyleSheet(blue)
        self.edit_block.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self.edit_block.setRange(vmin, vmax)
        self.edit_block.setSingleStep(0.1)
        if ratio == 100:
            self.edit_block.setDecimals(2)
        elif ratio == 10:
            self.edit_block.setDecimals(1)
        else:
            self.edit_block.setDecimals(0)

        self.edit_block.setValue(value)
        
    def setValue(self, val):
        value = round(val / self.ratio, 2)
        self.edit_block.setValue(value)
        # print("DataDisplay :: setValue :: ", self.name, self.ratio, val, value)

    def display(self, layout, nth):
        layout.addWidget(self.name_block, nth, 0)
        layout.addWidget(self.edit_block, nth, 1, 1, 2)
        layout.addWidget(self.unit_block, nth, 3)
        # layout.addWidget(self.range_block, nth, 4)

    def displayWithOutRange(self, layout, nth):
        layout.addWidget(self.name_block, nth, 0)
        layout.addWidget(self.edit_block, nth, 1, 1, 2)
        layout.addWidget(self.unit_block, nth, 3)

    def gridDisplay(self, layout, row, col):
        layout.addWidget(self.name_block, row, col)
        layout.addWidget(self.edit_block, row, col+1, 1, 2)
        layout.addWidget(self.unit_block, row, col+3)
        layout.addWidget(self.range_block, row, col+4)

    def reset(self):
        self.edit_block.setValue(0)


class DataDisplayOnly(QWidget):
    def __init__(self, name, value=0, 
                unit="", vmin=0, vmax=10000, ratio=1):
        
        super(DataDisplayOnly, self).__init__()
        blue = 'margin: -1px;font-size: 11pt;padding: 0px; color: blue'
        self.name = name
        self.value = value
        self.name_block = QLabel(name)
        self.unit_block = QLabel(unit)
        self.range_block = QLabel(f'{vmin} ~ {vmax}')
        self.vmin = vmin 
        self.vmax = vmax 
        self.ratio = ratio

        self.edit_block = QLabel()
        self.edit_block.setStyleSheet(blue)
        self.edit_block.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        if value:
            value = round(value / self.ratio, 2)
        self.edit_block.setText(str(value))
        
    def setValue(self, val):
        value = round(val / self.ratio, 2)
        self.edit_block.setText(str(value))
        # print("DataDisplay :: setValue :: ", self.name, self.ratio, val, value)

    def display(self, layout, nth):
        layout.addWidget(self.name_block, nth, 0)
        layout.addWidget(self.edit_block, nth, 1, 1, 2)
        layout.addWidget(self.unit_block, nth, 3)
        # layout.addWidget(self.range_block, nth, 4)

    def displayWithOutRange(self, layout, nth):
        layout.addWidget(self.name_block, nth, 0)
        layout.addWidget(self.edit_block, nth, 1, 1, 2)
        layout.addWidget(self.unit_block, nth, 3)

    def gridDisplay(self, layout, row, col):
        layout.addWidget(self.name_block, row, col)
        layout.addWidget(self.edit_block, row, col+1, 1, 2)
        layout.addWidget(self.unit_block, row, col+3)
        layout.addWidget(self.range_block, row, col+4)

    def reset(self):
        if self.value:
            value = round(self.value / self.ratio, 2)
        else:
            value = self.value
        self.edit_block.setText(str(value))


class DataUpdateRange(QWidget):
    def __init__(self, parent, name, address, value, 
                unit, vmin=0, vmax=10000, ratio=1, flag=False):
        super(DataUpdateRange, self).__init__()
        self.parent = parent
        self.address = address
        self.name = name 
        self.value = value / ratio
        self.vmin = vmin / ratio
        self.vmax = vmax / ratio
        self.ratio = ratio
        self.flag = flag

        btncolor = "QPushButton{font-size: 10pt; font-weight: bold; color: blue}"
        red = 'margin: -1px;font-size: 11pt;padding: 0px; color: blue'
        
        self.name_block = QLabel(self.name)
        self.unit_block = QLabel(unit)
        if self.ratio == 1000:
            self.range_block = QLabel(f'{vmin/ratio:.3f} ~ {vmax/ratio:.3f}')
        elif self.ratio == 100:
            self.range_block = QLabel(f'{vmin/ratio:.2f} ~ {vmax/ratio:.2f}')
        elif self.ratio == 10:
            self.range_block = QLabel(f'{vmin/ratio:.1f} ~ {vmax/ratio:.1f}')
        else:
            self.range_block = QLabel(f'{vmin} ~ {vmax}')

        self.range_block.setStyleSheet('font-size: 8pt;')

        self.edit_block = QDoubleSpinBox()
        self.edit_block.setStyleSheet(red)
        self.edit_block.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self.edit_block.valueChanged.connect(self.value_changed)
        self.edit_block.setRange(self.vmin, self.vmax)
        
        if self.ratio == 1000:
            self.edit_block.setDecimals(3)
            self.edit_block.setSingleStep(0.001)
            self.edit_block.setValue(self.value)
        elif self.ratio == 100:
            self.edit_block.setDecimals(2)
            self.edit_block.setSingleStep(0.01)
            self.edit_block.setValue(self.value)
        elif self.ratio == 10:
            self.edit_block.setDecimals(1)
            self.edit_block.setSingleStep(0.1)
            self.edit_block.setValue(self.value)
        else:
            self.edit_block.setDecimals(0)
            self.edit_block.setSingleStep(1)
            self.edit_block.setValue(self.value)
        
        self.btn_block = QPushButton("변 경")
        self.btn_block.setStyleSheet(btncolor)
        self.btn_block.resize(30, 20)
        self.btn_block.clicked.connect(self.modifyDeviceValue)
        self.btn_block.setMaximumWidth(40)

        self.cflag = False


    def display(self, layout, nth):
        layout.addWidget(self.name_block, nth, 0, 1, 2)
        layout.addWidget(self.edit_block, nth, 2, 1, 2)
        layout.addWidget(self.unit_block, nth, 4)
        layout.addWidget(self.btn_block, nth, 5)
        layout.addWidget(self.range_block, nth, 6)

    def gridDisplay(self, layout, row, col):
        layout.addWidget(self.name_block, row, col)
        layout.addWidget(self.edit_block, row, col+1)
        layout.addWidget(self.unit_block, row, col+2)
        layout.addWidget(self.btn_block, row, col+3)
        layout.addWidget(self.range_block, row, col+4)

    def displayGroup(self):
        group = QGroupBox("")
        layout = QGridLayout()
        group.setLayout(layout)

        layout.addWidget(self.name_block, 0, 0, 1, 2)
        layout.addWidget(self.edit_block, 0, 2, 1, 2)
        layout.addWidget(self.unit_block, 0, 4)
        layout.addWidget(self.btn_block, 0, 5)
        layout.addWidget(self.range_block, 0, 6)

        return group

    def setValue(self, val):
        if self.ratio == 1000:
            value = round(val / self.ratio, 3)
            self.edit_block.setValue(value)
        elif self.ratio == 100:
            value = round(val / self.ratio, 2)
            self.edit_block.setValue(value)
        elif self.ratio == 10:
            value = round(val / self.ratio, 1)
            self.edit_block.setValue(value)
        else:
            value = round(val / self.ratio, 0)
            self.edit_block.setValue(value)

    def value_changed(self):
        self.cflag = True
        try:
            val = float(self.edit_block.value())
        except Exception as e:
            print(e)
            val = self.vmin

        if val > self.vmax:
            self.edit_block.setValue(str(self.vmax))

        if val < self.vmin:
            self.edit_block.setValue(str(self.vmin))

    def hasChanged(self):
        return self.cflag

    def modifyDeviceValue(self):
        val = float(self.edit_block.text())
        vratio = int(val * self.ratio)
        if self.flag:
            quoti = vratio // self.ratio
            remain = vratio % self.ratio
            logging.info(f"quoti : {quoti}, remain : {remain}")
            try:
                self.parent.parent.parent.modifyDeviceValue(self.address, quoti)
                self.parent.parent.parent.modifyDeviceValue(74, remain)
            except Exception as e:
                self.parent.parent.modifyDeviceValue(self.address, quoti)
                self.parent.parent.modifyDeviceValue(74, remain)
        else:
            try:
                self.parent.parent.parent.modifyDeviceValue(self.address, vratio)
            except Exception as e:
                self.parent.parent.modifyDeviceValue(self.address, vratio)


class DataUpdateSingle(QWidget):
    def __init__(self, parent, name, address, value, 
                unit, vmin=0, vmax=10000, ratio=1):
        super(DataUpdateSingle, self).__init__()
        self.parent = parent
        self.address = address
        self.name = name 
        self.value = value 
        self.vmin = vmin 
        self.vmax = vmax
        self.ratio = ratio

        btncolor = "QPushButton{font-size: 10pt; font-weight: bold; color: blue}"
        red = 'margin: -1px;font-size: 11pt;padding: 0px; color: red'
        
        self.name_block = QLabel(name)
        self.unit_block = QLabel(unit)
        self.range_block = QLabel(f'{vmin} ~ {vmax}')
        # self.range_block.setStyleSheet('font-size: 6pt;')

        self.edit_block = QLineEdit()
        self.edit_block.setStyleSheet(red)
        self.edit_block.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self.edit_block.textEdited.connect(self.value_changed)
        self.edit_block.setText(str(self.vmin))
        
        self.btn_block = QPushButton("변 경")
        self.btn_block.setStyleSheet(btncolor)
        self.btn_block.clicked.connect(self.modifyDeviceValue)
        self.btn_block.setMaximumWidth(40)

    def display(self, layout, row, col):
        layout.addWidget(self.name_block, row, col, 1, 2)
        layout.addWidget(self.edit_block, row, col+2, 1, 2)
        layout.addWidget(self.unit_block, row, col+4)
        layout.addWidget(self.btn_block, row, col+5)
        layout.addWidget(self.range_block, row, col+6)

    def setValue(self, val):
        value = round(val / self.ratio, 1)
        self.edit_block.setText(str(value))

    def value_changed(self):
        try:
            val = float(self.edit_block.text())
        except Exception as e:
            print(e)
            val = self.vmin

        if val > self.vmax:
            self.edit_block.setText(str(self.vmax))

        if val < self.vmin:
            self.edit_block.setText(str(self.vmin))


    def modifyDeviceValue(self):
        val = float(self.edit_block.text())
        vratio = int(val * self.ratio)
        print("modifyDeviceValue : ", vratio)
        self.parent.parent.modifyDeviceValue(self.address, vratio)

    def groupdisplay(self):
        group = QGroupBox("")
        layout = QHBoxLayout()
        group.setLayout(layout)

        layout.addWidget(self.name_block)
        layout.addWidget(self.edit_block)
        layout.addWidget(self.unit_block)
        layout.addWidget(self.btn_block)
        layout.addWidget(self.range_block)

        return group

    def gridDisplay(self, layout, row, col):
        label = QLabel(self.name)
        label.setStyleSheet('QLabel{font-size: 12pt; font-weight: bold}')
        layout.addWidget(label, row, col, 1, 6)

        layout.addWidget(self.name_block, row+1, col, 1, 2)
        layout.addWidget(self.edit_block, row+1, col+2, 1, 2)
        layout.addWidget(self.unit_block, row+1, col+4)
        layout.addWidget(self.btn_block, row+1, col+5)
        layout.addWidget(self.range_block, row+1, col+6)


class DisplayOnly:
    def __init__(self, name, value, unit=""):
        super(DisplayOnly, self).__init__()
        blue = 'margin: -1px;font-size: 11pt;padding: 0px; color: red; font-weight: bold'
        self.name = name 
        self.value = value 
        self.name_block = QLabel(name)
        self.value_block = QLabel(str(value))
        self.value_block.setStyleSheet(blue)
        self.value_block.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)

    def setValue(self, val):
        self.value_block.setText(str(val))

    def setValues(self, vals):
        message = ""
        for val in vals:
            message += str(val) + " "
        self.value_block.setText(message)

    def display(self, layout, nth):
        layout.addWidget(self.name_block, nth, 0)
        layout.addWidget(self.value_block, nth, 1, 1, 4)

    def reset(self):
        # logging.info(f"DisplayOnly :: reset :: {self.name}")
        self.value_block.setText("")


class SetVal(QWidget):
    def __init__(self, name, value, unit, layout, nth, minval=200, maxval=10000):
        super(SetVal, self).__init__()
        self.name = name
        self.value = value 
        self.unit = unit 
        self.minval = minval 
        self.maxval = maxval 
        # self.setGeometry(0, 0, 300, 100)
        name_block = QLabel(self.name)
        unit_block = QLabel(self.unit)

        self.edit_block = QSpinBox()
        self.edit_block.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        self.edit_block.setSingleStep(100)
        self.edit_block.setStyleSheet('margin: -1px; padding: 0px; color: red')
        self.edit_block.setRange(self.minval, self.maxval)
        self.edit_block.setValue(self.value)

        layout.addWidget(name_block, nth, 0)
        layout.addWidget(self.edit_block, nth, 1)
        layout.addWidget(unit_block, nth, 2)

    def save_data(self):
        print(f"write_data : {self.address} : {self.edit_block.value()}")

    def get_val(self):
        return self.edit_block.value()


class LineView:
    def __init__(self, title, x, y, color):
        self.title = title
        self.x = x
        self.y = y
        self.color = color
        self.pen_1 = pyGraph.mkPen(width=2, color=(255, 0, 0))
        self.pen_2 = pyGraph.mkPen(width=2, color=(0, 0, 255))
        self.pen_3 = pyGraph.mkPen(width=2, color=(255, 191, 0))
        self.pen_4 = pyGraph.mkPen(width=2, color=(26, 175, 51))
        self.pen_5 = pyGraph.mkPen(width=2, color=(0,0,128))


        self.btn = QPushButton(self.title)
        self.btn.setEnabled(False)
        self.btn.setStyleSheet(f"background-color: white;color: {self.color}; font-size: 14px;font-weight: bold;")

        self.plotWidget = pyGraph.PlotWidget()
        self.plotWidget.setBackground('w')

        self.axisA = self.plotWidget.plotItem
        self.axisA.setLabels(left=title)
        self.axisA.showAxis('right')
        self.graph = self.axisA.plot(self.x, self.y, pen=self.pen_1, name=self.title)

    def reDraw(self, title, dlist):
        # print("reDraw !!!!", title, dlist)
        self.axisA.clear()
        self.axisA.plot(dlist, pen=self.pen_1, name=title)


    def reDrawMulti(self, title1, list1, title2=None, list2=None, title3=None, list3=None, 
                    title4=None, list4=None, title5=None, list5=None):
        
        self.axisA.clear()
        self.axisA.plot(list1, pen=self.pen_1, name=title1)
        if list2:
            self.axisA.plot(list2, pen=self.pen_2, name=title2)
        if list3:
            self.axisA.plot(list3, pen=self.pen_3, name=title3)
        if list4:
            self.axisA.plot(list4, pen=self.pen_4, name=title4)
        if list5:
            self.axisA.plot(list5, pen=self.pen_5, name=title5)

     
    def mousePressEvent(self, ev):
        print(ev)


