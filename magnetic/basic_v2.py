
import logging
from ctypes import c_int16


from PyQt5.QtWidgets import QGroupBox, QGridLayout, QLabel, QPushButton, QDialog
from PyQt5.QtWidgets import QVBoxLayout, QDialogButtonBox
from PyQt5.QtCore import Qt
from .common import DisplayOnly, CheckField, DataDisplayOnly
from .common import DataDisplay, DataUpdateRange

class CLed(QPushButton):
    def __init__(self, num):
        super(CLed, self).__init__()
        self.title = f"#{num} RUN"
        self.status = False
        self.setText(self.title)
        self.setFixedSize(60, 60)
        style = f"border: 1px solid lightgray;border-radius: 30px;background-color: #ccc;color: black;font-size: 14px;font-weight: bold;"
        self.setStyleSheet(style)

    def changeRun(self):
        style = f"border: 1px solid lightgray;border-radius: 30px;background-color: #0f0;color: black;font-size: 14px;font-weight: bold;"
        self.status = True
        self.setStyleSheet(style)

    def changeStop(self):
        style = f"border: 1px solid lightgray;border-radius: 30px;background-color: #ccc;color: black;font-size: 14px;font-weight: bold;"
        self.status = False
        self.setStyleSheet(style)

    def getStatus(self):
        return self.status


class WarningDialog(QDialog):
    def __init__(self, message):
        super().__init__()
        self.message = message
        self.setupUI()

    def setupUI(self):
        self.setGeometry(100, 100, 400, 200)

        self.setWindowTitle("Warning !!")
        layout = QVBoxLayout()
        self.setLayout(layout)
        self.label = QLabel(self.message)
        layout.addWidget(self.label)

        self.buttonBox = QDialogButtonBox()
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        layout.addWidget(self.buttonBox)

        self.buttonBox.accepted.connect(self.on_accepted)
        self.buttonBox.rejected.connect(self.reject)

    def on_accepted(self):
        print("WarningDialog :: OK")
        self.accept()


class HLink:
    def __init__(self):
        self.hv = DataDisplay('DC LINK HV', 0, 'V', vmin=0, vmax=350, ratio=10)
        self.lv = DataDisplay('DC LINK LV', 0, 'V', vmin=0, vmax=100, ratio=10)
        self.freq = DataDisplay('PULSE FREQ', 0, 'kHz', vmin=0.5, vmax=10, ratio=10)
        
        self.errtime = DataDisplay('COMM ERR TIME', 0, 'sec', vmin=0, vmax=100)
        self.onoffmode = CheckField('ON/OFF MODE', 'OFF', 'ON', 0)
        self.ipaddress = DisplayOnly('IP ADDRESS', "0.0.0.0")

        self.ovplevel1 = DataDisplay('VDC1 OVP LEVEL', 0, 'V', vmin=0, vmax=430, ratio=10)
        self.ovplevel2 = DataDisplay('VDC2 OVP LEVEL', 0, 'V', vmin=0, vmax=150, ratio=10)
        self.romin = DataDisplay('RO MIN FAULT LEVEL', 0, 'Ω', vmin=0, vmax=800)
        self.pgain = DataDisplay('P GAIN', 0, '', vmin=0, vmax=800)
        self.igain = DataDisplay('I GAIN', 0, '', vmin=0, vmax=800)
        self.reserved = DataDisplay('Reserved', 0, '', vmin=0, vmax=800)
        self.model = DisplayOnly('MODEL', 0)
        self.version = DisplayOnly('Version', 0)

    def update(self, data):
        self.hv.setValue(data[0])
        self.lv.setValue(data[1])
        self.freq.setValue(data[2])

        self.errtime.setValue(data[43])
        self.onoffmode.setValue(data[44])

        ipaddress = f"{data[45]}.{data[46]}.{data[47]}.{data[48]}"
        self.ipaddress.setValue(ipaddress)

        self.ovplevel1.setValue(data[49])
        self.ovplevel2.setValue(data[50])
        self.romin.setValue(data[51])
        self.pgain.setValue(data[52])
        self.igain.setValue(data[53])
        self.reserved.setValue(data[54])
        self.model.setValue(data[55])
        self.version.setValue(data[56])

    def display(self):
        grp_state = QGroupBox()
        layout_state = QGridLayout()
        grp_state.setLayout(layout_state)

        self.hv.display(layout_state, 0)
        self.lv.display(layout_state, 1)
        self.freq.display(layout_state, 2)

        self.errtime.display(layout_state, 4)
        self.onoffmode.display(layout_state, 5)

        self.ovplevel1.display(layout_state, 7)
        self.ovplevel2.display(layout_state, 8)
        # self.romin.display(layout_state, 9)
        self.pgain.display(layout_state, 10)
        self.igain.display(layout_state, 11)
        self.reserved.display(layout_state, 12)
        self.model.display(layout_state, 13)
        self.version.display(layout_state, 14)

        self.ipaddress.display(layout_state, 15)

        return grp_state


class State:
    def __init__(self, name, dmax=50):
        self.name = name
        self.duty = DataDisplay('DUTY', 0, '%', vmin=0, vmax=dmax, ratio=10)
        self.current = DataDisplay('CURRENT', 0, 'A', vmin=-20, vmax=20, ratio=100)

    def update(self, data):
        self.duty.setValue(data[0])
        val_current = c_int16(data[1]).value
        self.current.setValue(val_current)

    def display(self):
        grp_state = QGroupBox(self.name)
        layout_state = QGridLayout()
        grp_state.setLayout(layout_state)

        self.duty.display(layout_state, 0)
        self.current.display(layout_state, 1)
        return grp_state

    def gridDisplay(self, layout, row, col):
        label = QLabel(self.name)
        label.setStyleSheet('QLabel{font-size: 12pt; font-weight: bold}')
        layout.addWidget(label, row, col, 1, 6)
        self.duty.gridDisplay(layout, row+1, col)
        self.current.gridDisplay(layout, row+2, col)


class MDStatus:
    def __init__(self, name, bcolor="#fff"):
        self.name = name
        self.state1 = State('State #1', dmax=100)
        self.state2 = State('State #2')
        self.state3 = State('State #3')
        self.state4 = State('State #4')
        self.state5 = State('State #5')

        self.bcolor = bcolor
        self.main_label = "QLabel{font-size: 14pt; font-weight: bold}"

    def update_module(self, mod):
        self.state1.update(mod[:2])
        self.state2.update(mod[2:4])
        self.state3.update(mod[4:6])
        self.state4.update(mod[6:8])
        self.state5.update(mod[8:10])

    def display(self):
        grp_status = QGroupBox(self.name)
        grp_status.setStyleSheet(f"background-color: {self.bcolor}")
        layout_status = QGridLayout()
        grp_status.setLayout(layout_status)

        group1 = self.state1.display()
        layout_status.addWidget(group1, 0, 0)
        group2 = self.state2.display()
        layout_status.addWidget(group2, 0, 1)
        group3 = self.state3.display()
        layout_status.addWidget(group3, 1, 0)
        group4 = self.state4.display()
        layout_status.addWidget(group4, 1, 1)
        group5 = self.state5.display()
        layout_status.addWidget(group5, 2, 0)

        return grp_status

    def displayNoGroup(self):
        grp_status = QGroupBox(self.name)
        grp_status.setStyleSheet(f"background-color: {self.bcolor}")
        layout_status = QGridLayout()
        grp_status.setLayout(layout_status)

        group1 = self.state1.gridDisplay(layout_status, 0, 0)
        group2 = self.state2.gridDisplay(layout_status, 0, 6)
        group3 = self.state3.gridDisplay(layout_status, 3, 0)
        group4 = self.state4.gridDisplay(layout_status, 3, 6)
        group5 = self.state5.gridDisplay(layout_status, 6, 0)

        return grp_status


class VUState:
    def __init__(self, parent, name, address, dmax=500):
        self.parent = parent
        self.name = name
        self.address = address
        self.duty = DataUpdateRange(self, 'DUTY', address, 300, '%', vmin=0, vmax=dmax, ratio=10)
        self.current = DataUpdateRange(self, 'CURRENT', address+1, 2000, 'A', vmin=-2000, vmax=2000, ratio=100)

    def update(self, data):
        self.duty.setValue(data[0])
        val_current = c_int16(data[1]).value
        self.current.setValue(val_current)

    def display(self):
        grp_state = QGroupBox(self.name)
        layout_state = QGridLayout()
        grp_state.setLayout(layout_state)

        self.duty.display(layout_state, 0)
        self.current.display(layout_state, 1)

        return grp_state

    def gridDisplay(self, layout, row, col):
        label = QLabel(self.name)
        label.setStyleSheet('QLabel{font-size: 12pt; font-weight: bold}')
        layout.addWidget(label, row, col, 1, 7)

        self.duty.gridDisplay(layout, row+1, col)
        self.current.gridDisplay(layout, row+2, col)

    def updateChangedValue(self):
        if self.duty.hasChanged():
            self.duty.modifyDeviceValue()

        if self.current.hasChanged():
            self.current.modifyDeviceValue()


class VUModule:
    def __init__(self, parent, name, address, loadadd, md_add, bcolor="#fff"):
        self.parent = parent
        self.name = name
        self.state1 = VUState(self, 'State #1', address, dmax=1000)
        self.state2 = VUState(self, 'State #2', address+2)
        self.state3 = VUState(self, 'State #3', address+4)
        self.state4 = VUState(self, 'State #4', address+6)
        self.state5 = VUState(self, 'State #5', address+8)
        self.load_indu = DataUpdateRange(self, 'LOAD_INDUC', loadadd, 0, 'uH', 1, 9999, ratio=1)
        self.load_ro = DataUpdateRange(self, 'LOAD_RO', md_add, 1000, 'Ohm', 1, 3000, ratio=1000)

        self.bcolor = bcolor
        self.main_label = "QLabel{font-size: 14pt; font-weight: bold}"

    def update_module(self, mod):
        self.state1.update(mod[:2])
        self.state2.update(mod[2:4])
        self.state3.update(mod[4:6])
        self.state4.update(mod[6:8])
        self.state5.update(mod[8:10])

    def display(self):
        grp_status = QGroupBox(self.name)
        grp_status.setStyleSheet(f"background-color: {self.bcolor}")
        layout_status = QVBoxLayout()
        grp_status.setLayout(layout_status)

        group1 = self.state1.display()
        layout_status.addWidget(group1)
        group2 = self.state2.display()
        layout_status.addWidget(group2)
        group3 = self.state3.display()
        layout_status.addWidget(group3)
        group4 = self.state4.display()
        layout_status.addWidget(group4)
        group5 = self.state5.display()
        layout_status.addWidget(group5)

        group6 = QGroupBox("")
        layout = QGridLayout()
        group6.setLayout(layout)

        self.load_indu.display(layout, 0)
        self.load_ro.display(layout, 1)

        layout_status.addWidget(group6)

        return grp_status

    def displayNoGroup(self):
        grp_status = QGroupBox(self.name)
        grp_status.setStyleSheet(f"background-color: {self.bcolor}")
        layout_status = QGridLayout()
        grp_status.setLayout(layout_status)

        self.state1.gridDisplay(layout_status, 0, 0)
        self.state2.gridDisplay(layout_status, 0, 7)
        self.state3.gridDisplay(layout_status, 3, 0)
        self.state4.gridDisplay(layout_status, 3, 7)
        self.state5.gridDisplay(layout_status, 6, 0)

        self.load_indu.gridDisplay(layout_status, 6, 7)

        return grp_status
    
    def updateChangedValue(self):
        self.state1.updateChangedValue()
        self.state2.updateChangedValue()
        self.state3.updateChangedValue()
        self.state4.updateChangedValue()
        self.state5.updateChangedValue()

        if self.load_indu.hasChanged():
            self.load_indu.modifyDeviceValue()
        if self.load_ro.hasChanged():
            self.load_ro.modifyDeviceValue()


class ValueSetting:
    def __init__(self, parent):
        self.parent = parent
        # self.vol1 = DataUpdateRange(self, 'VOLTAGE 1', 1, 1000, 'V', vmin=10, vmax=1500, ratio=1)
        # self.vol2 = DataUpdateRange(self, 'VOLTAGE 2', 2, 1000, 'V', vmin=10, vmax=1500, ratio=1)
        # self.vol3 = DataUpdateRange(self, 'VOLTAGE 3', 3, 1000, 'V', vmin=10, vmax=1500, ratio=1)
        self.idc = DataUpdateRange(self, 'IDC', 1, 1000, 'A', vmin=10, vmax=2926, ratio=100)
        self.ir = DataUpdateRange(self, 'IR', 2, 1000, 'A', vmin=10, vmax=3000, ratio=100)
        self.freq = DataUpdateRange(self, 'FREQ', 3, 1000, 'Hz', vmin=1, vmax=50000, ratio=10, flag=True)
        self.delay = DataUpdateRange(self, 'DELAY', 4, 0, 'm sec', vmin=0, vmax=100, ratio=1)
        self.timeout = DataUpdateRange(self, 'COMM TIMEOUT', 55, 5, 'S', vmin=1, vmax=99, ratio=1)
        
        self.sync_select = DataUpdateRange(self, 'SYNC SELECT', 56, 0, '', vmin=0, vmax=1, ratio=1)
        self.sync_fault = DataUpdateRange(self, 'SYNC FAULT', 57, 10, 'Hz', vmin=1, vmax=1000, ratio=1)
        self.vdc_ovp = DataUpdateRange(self, 'VDC OVP', 58, 2380, 'V', vmin=0, vmax=2380, ratio=10)
        self.idc_ovp = DataUpdateRange(self, 'IDC OCP', 59, 2926, 'A', vmin=1, vmax=2926, ratio=100)
        self.io_posi_ocp = DataUpdateRange(self, 'IO POSI OCP', 60, 2860, 'A', vmin=0, vmax=2860, ratio=100)
        
        self.io_nega_ocp = DataUpdateRange(self, 'IO NEGA OCP', 61, 2860, 'A', vmin=0, vmax=2860, ratio=100)
        self.io_ro_min = DataUpdateRange(self, 'RO MIN', 62, 2860, '', vmin=0, vmax=9999, ratio=10)
        self.io_ro_max = DataUpdateRange(self, 'RO MAX', 63, 2860, '', vmin=0, vmax=9999, ratio=10)

        self.btn_update_all = QPushButton("전체 변경 값 적용")
        self.btn_update_all.clicked.connect(self.updateAll)

    def updateAll(self):
        self.parent.updateAllChangedValue()


    def updateChangedValue(self):
        if self.idc.hasChanged():
            self.idc.modifyDeviceValue()
        if self.ir.hasChanged():
            self.ir.modifyDeviceValue()
        if self.freq.hasChanged():
            self.freq.modifyDeviceValue()
        if self.delay.hasChanged():
            self.delay.modifyDeviceValue()
        if self.timeout.hasChanged():
            self.timeout.modifyDeviceValue()
        
        if self.sync_select.hasChanged():
            self.sync_select.modifyDeviceValue()
        if self.sync_fault.hasChanged():
            self.sync_fault.modifyDeviceValue()
        if self.vdc_ovp.hasChanged():
            self.vdc_ovp.modifyDeviceValue()
        if self.idc_ovp.hasChanged():
            self.idc_ovp.modifyDeviceValue()
        if self.io_posi_ocp.hasChanged():
            self.io_posi_ocp.modifyDeviceValue()

        if self.io_nega_ocp.hasChanged():
            self.io_nega_ocp.modifyDeviceValue()
        if self.io_ro_min.hasChanged():
            self.io_ro_min.modifyDeviceValue()
        if self.io_ro_max.hasChanged():
            self.io_ro_max.modifyDeviceValue()

    def display(self):
        group = QGroupBox("")
        layout = QGridLayout()
        group.setLayout(layout)
        
        # self.vol1.gridDisplay(layout, 0, 0)
        # self.vol2.gridDisplay(layout, 0, 5)
        # self.vol3.gridDisplay(layout, 0, 10)
        self.idc.gridDisplay(layout, 0, 0)
        self.ir.gridDisplay(layout, 0, 5)
        self.freq.gridDisplay(layout, 0, 10)
        self.delay.gridDisplay(layout, 0, 15)
        self.timeout.gridDisplay(layout, 0, 20)
        
        self.sync_select.gridDisplay(layout, 1, 0)
        self.sync_fault.gridDisplay(layout, 1, 5)
        self.vdc_ovp.gridDisplay(layout, 1, 10)
        self.idc_ovp.gridDisplay(layout, 1, 15)
        self.io_posi_ocp.gridDisplay(layout, 1, 20)

        self.io_nega_ocp.gridDisplay(layout, 2, 0)
        self.io_ro_min.gridDisplay(layout, 2, 5)
        self.io_ro_max.gridDisplay(layout, 2, 10)

        layout.addWidget(self.btn_update_all, 2, 15, 1, 10)

        return group


class Channel:
    def __init__(self, num):
        self.num = num
        self.vdc = DataDisplayOnly('VDC', 0, 'V', vmin=0.0, vmax=23.80, ratio=100)
        self.idc = DataDisplayOnly('IDC', 0, 'A', vmin=0.0, vmax=29.26, ratio=100)
        self.ir = DataDisplayOnly('IR', 0, 'A', vmin=0.0, vmax=30.00, ratio=100)

        self.vdc_list = []
        self.idc_list = []
        self.ir_list = []

    def update(self, data):
        self.vdc.setValue(data[0])
        self.idc.setValue(data[1])
        self.ir.setValue(data[2])

        self.vdc_list.append(data[0])
        self.idc_list.append(data[1])
        self.ir_list.append(data[2])

    def display(self):
        grp_state = QGroupBox(f"Channel #{self.num}")
        layout_state = QGridLayout()
        grp_state.setLayout(layout_state)

        self.vdc.displayWithOutRange(layout_state, 0)
        self.idc.displayWithOutRange(layout_state, 1)
        self.ir.displayWithOutRange(layout_state, 2)
        return grp_state

    def gridDisplay(self, layout, row, col):
        label = QLabel(f"Channel #{self.num}")
        label.setStyleSheet('QLabel{font-size: 12pt; font-weight: bold}')
        layout.addWidget(label, row, col, 1, 7)

        self.vdc.gridDisplay(layout, row+1, col)
        self.idc.gridDisplay(layout, row+2, col)
        self.ir.gridDisplay(layout, row+3, col)

    def reset(self):
        self.vdc.reset()
        self.idc.reset()
        self.ir.reset()


class ModuleStatus:
    def __init__(self, name, bcolor="#fff"):
        self.name = name 
        self.bcolor = bcolor

        self.label_status = CheckField('Status', '정상', '경고', 0)
        self.label_fault = DisplayOnly("Fault", "")

        self.vo_ch1 = DataDisplay('Voltage Ch #1', 0, 'V', vmin=0, vmax=350, ratio=10)
        self.vo_ch2 = DataDisplay('Voltage Ch #2', 0, 'V', vmin=0, vmax=100, ratio=10)
        self.cur_st1 = DataDisplay('Pulse IO State #1', 0, 'A', vmin=-20, vmax=20, ratio=100)
        self.cur_st2 = DataDisplay('Pulse IO State #2', 0, 'A', vmin=-20, vmax=20, ratio=100)
        self.cur_st3 = DataDisplay('Pulse IO State #3', 0, 'A', vmin=-20, vmax=20, ratio=100)
        self.cur_st4 = DataDisplay('Pulse IO State #4', 0, 'A', vmin=-20, vmax=20, ratio=100)
        self.cur_st5 = DataDisplay('Pulse IO State #5', 0, 'A', vmin=-20, vmax=20, ratio=100)
        self.sw = DisplayOnly('S/W Version', 0)
        self.load_indu = DataDisplay('Load Inductance', 0, 'uH', vmin=1, vmax=9999)


    def status_normal(self):
        self.label_status.change_normal()

    def status_warning(self):
        self.label_status.change_warning()

    def change_value(self, data):
        # print("ModuleStatus :: change_value :: ", self.name, data)
        try:
            if data[0]:
                self.label_status.change_warning()
            else:
                self.label_status.change_normal()

            if data[1]:
                self.label_fault.setValue(data[1])

            self.vo_ch1.setValue(data[2])
            self.vo_ch2.setValue(data[3])
            
            st1_val = c_int16(data[4]).value
            self.cur_st1.setValue(st1_val)

            st2_val = c_int16(data[5]).value
            self.cur_st2.setValue(st2_val)

            st3_val = c_int16(data[6]).value
            self.cur_st3.setValue(st3_val)

            st4_val = c_int16(data[7]).value
            self.cur_st4.setValue(st4_val)

            st5_val = c_int16(data[8]).value
            self.cur_st5.setValue(st5_val)

            self.sw.setValue(data[9])
            self.load_indu.setValue(data[10])

        except Exception as e:
            print(e)

    def display(self):
        grp_module = QGroupBox(self.name)
        grp_module.setStyleSheet(f"background-color: {self.bcolor}")
        layout_module = QGridLayout()
        grp_module.setLayout(layout_module)

        self.label_status.display(layout_module, 0)
        self.label_fault.display(layout_module, 1)

        self.vo_ch1.display(layout_module, 2)
        self.vo_ch2.display(layout_module, 3)
        self.cur_st1.display(layout_module, 4)
        self.cur_st2.display(layout_module, 5)
        self.cur_st3.display(layout_module, 6)
        self.cur_st4.display(layout_module, 7)
        self.cur_st5.display(layout_module, 8)
        self.sw.display(layout_module, 9)
        self.load_indu.display(layout_module, 10)

        return grp_module

    def reset(self):
        self.vo_ch1.reset()
        self.vo_ch2.reset()
        self.cur_st1.reset()
        self.cur_st2.reset()
        self.cur_st3.reset()
        self.cur_st4.reset()
        self.cur_st5.reset()
        self.sw.reset()
        self.load_indu.reset()


class ModuleStatus2:
    def __init__(self, name, bcolor="#fff"):
        self.name = name 
        self.bcolor = bcolor

        self.label_status = CheckField('Status', '정상', '경고', 0)
        self.label_fault = DisplayOnly("Fault", "")

        self.channel1 = Channel(1)
        self.channel2 = Channel(2)
        self.channel3 = Channel(3)

        self.io = DataDisplayOnly('IO(A)', 0, 'A', vmin=-28.60, vmax=28.60, ratio=100)
        
        self.io1_avg = DataDisplayOnly('T1_IO_AVG', 0, 'A', vmin=-28.60, vmax=28.60, ratio=100)
        self.io2_avg = DataDisplayOnly('T2_IO_AVG', 0, 'A', vmin=-28.60, vmax=28.60, ratio=100)
        self.io3_avg = DataDisplayOnly('T3_IO_AVG', 0, 'A', vmin=-28.60, vmax=28.60, ratio=100)
        self.io4_avg = DataDisplayOnly('T4_IO_AVG', 0, 'A', vmin=-28.60, vmax=28.60, ratio=100)
        self.io5_avg = DataDisplayOnly('T5_IO_AVG', 0, 'A', vmin=-28.60, vmax=28.60, ratio=100)

        self.io_list = []
        self.io1_avg_list = []
        self.io2_avg_list = []
        self.io3_avg_list = []
        self.io4_avg_list = []
        self.io5_avg_list = []

    def status_normal(self):
        self.label_status.change_normal()

    def status_warning(self):
        self.label_status.change_warning()

    def change_value(self, data, io_avg):
        # print("ModuleStatus :: change_value :: ", self.name, data)
        try:
            # if data[0]:
            #     self.label_status.change_warning()
            # else:
            #     self.label_status.change_normal()
            # if data[1]:
            #     self.label_fault.setValue(data[1])

            self.channel1.update(data[:3])
            self.channel2.update(data[3:6])
            self.channel3.update(data[6:9])

            self.io.setValue(data[9])

            self.io1_avg.setValue(io_avg[0])
            self.io2_avg.setValue(io_avg[1])
            self.io3_avg.setValue(io_avg[2])
            self.io4_avg.setValue(io_avg[3])
            self.io5_avg.setValue(io_avg[4])

            self.io_list.append(data[9])

            self.io1_avg_list.append(io_avg[0])
            self.io2_avg_list.append(io_avg[1])
            self.io3_avg_list.append(io_avg[2])
            self.io4_avg_list.append(io_avg[3])
            self.io5_avg_list.append(io_avg[4])

        except Exception as e:
            print(e)

    def get_ch1_vdcs(self):
        return self.channel1.vdc_list
    
    def get_ch1_idcs(self):
        return self.channel1.idc_list
    
    def get_ch1_irs(self):
        return self.channel1.ir_list
    
    def get_ch2_vdcs(self):
        return self.channel2.vdc_list
    
    def get_ch2_idcs(self):
        return self.channel2.idc_list
    
    def get_ch2_irs(self):
        return self.channel2.ir_list
    
    def get_ch2_irs(self):
        return self.channel2.ir_list
    
    def get_ch3_vdcs(self):
        return self.channel3.vdc_list
    
    def get_ch3_idcs(self):
        return self.channel3.idc_list
    
    def get_ch3_irs(self):
        return self.channel3.ir_list
    
    def get_ios(self):
        return self.io_list
    
    def get_io1_avgs(self):
        return self.io1_avg_list
    
    def get_io2_avgs(self):
        return self.io2_avg_list
    
    def get_io3_avgs(self):
        return self.io3_avg_list
    
    def get_io4_avgs(self):
        return self.io4_avg_list
    
    def get_io5_avgs(self):
        return self.io5_avg_list

    def change_warning_label(self, label):
        self.label_fault.setValue(label)

    def change_warning_labels(self, labels):
        self.label_fault.setValue(labels)

    def display(self):
        grp_module = QGroupBox(self.name)
        grp_module.setStyleSheet(f"background-color: {self.bcolor}")
        layout_module = QGridLayout()
        grp_module.setLayout(layout_module)

        self.label_status.display(layout_module, 0)
        self.label_fault.display(layout_module, 1)

        group1 = self.channel1.display()
        layout_module.addWidget(group1, 2, 0, 1, 5)
        group2 = self.channel2.display()
        layout_module.addWidget(group2, 3, 0, 1, 5)
        group3 = self.channel3.display()
        layout_module.addWidget(group3, 4, 0, 1, 5)

        self.io.display(layout_module, 5)

        self.io1_avg.display(layout_module, 6)
        self.io2_avg.display(layout_module, 7)
        self.io3_avg.display(layout_module, 8)
        self.io4_avg.display(layout_module, 9)
        self.io5_avg.display(layout_module, 10)

        return grp_module

    def reset(self):
        self.label_status.reset()
        self.label_fault.reset()

        self.channel1.reset()
        self.channel2.reset()
        self.channel3.reset()

        self.io.reset()


class HeadAlarm:
    def __init__(self, name):
        self.name = name
        self.remote = CheckField("Remote/Local", "Local", "Remote")
        self.ready = CheckField("Stand by", "Off", "Ready")
        self.pulse = CheckField("Pulse Ready", "Off", "Pulse Out")
        self.sync = CheckField("Sync Lock", "Not OK", "Sync OK")
        self.warning = CheckField("WARNING INDICATOR", "NO", "WARNING")
        self.fault = CheckField("FAULT INDICATOR", "NO", "FAULT")

    def display(self):
        group = QGroupBox(self.name)
        layout = QGridLayout()
        group.setLayout(layout)

        self.remote.displayGrid(layout, 0, 0)
        self.ready.displayGrid(layout, 0, 3)
        self.pulse.displayGrid(layout, 0, 6)
        self.sync.displayGrid(layout, 1, 0)
        self.warning.displayGrid(layout, 1, 3)
        self.fault.displayGrid(layout, 1, 6)
        return group


    def status_update(self, condition):
        logging.info(f"HeadAlarm :: status_update :: {condition}")
        if 0 in condition:
            self.remote.change_warning()
        else:
            self.remote.change_normal()

        if 1 in condition:
            self.ready.change_warning()
        else:
            self.ready.change_normal()

        if 2 in condition:
            self.pulse.change_warning()
        else:
            self.pulse.change_normal()

        if 3 in condition:
            self.sync.change_warning()
        else:
            self.sync.change_normal()

        if 6 in condition:
            self.warning.change_warning()
        else:
            self.warning.change_normal()

        if 7 in condition:
            self.fault.change_warning()
        else:
            self.fault.change_normal()

    def reset(self):
        logging.info("HeadAlarm :: reset")
        self.remote.change_normal()
        self.ready.change_normal()
        self.pulse.change_normal()
        self.sync.change_normal()
        self.warning.change_normal()
        self.fault.change_normal()


class DeviceAlarm:
    def __init__(self, name):
        self.name = name
        self.fault_scr = CheckField("FAULT SCR", "정상", "경고")
        self.fault_eep_wr = CheckField("FAULT EEP WR", "정상", "경고")
        self.fault_tcp_comm = CheckField("FAULT TCP", "정상", "경고")
        self.fault_internal = CheckField("FAULT INTERNAL", "정상", "경고")
        self.fault_uvlo = CheckField("FAULT UVLO", "정상", "경고")
        self.fault_ext_sync = CheckField("FAULT SYNC ERROR", "정상", "경고")
        self.fault_ethercat = CheckField("FAULT ETHERCAT", "정상", "경고")
        self.fault_comm = CheckField("FAULT COMM", "정상", "경고")


    def display(self):
        group = QGroupBox(self.name)
        layout = QGridLayout()
        group.setLayout(layout)

        self.fault_scr.display(layout, 0)
        self.fault_eep_wr.display(layout, 1)
        self.fault_tcp_comm.display(layout, 2)
        self.fault_internal.display(layout, 3)
        self.fault_uvlo.display(layout, 4)
        self.fault_ext_sync.display(layout, 5)
        self.fault_ethercat.display(layout, 6)
        self.fault_comm.display(layout, 7)

        return group

    def status_update(self, condition):
        logging.info(f"DeviceAlarm :: status_update :: {condition}")
        if 0 in condition:
            self.fault_scr.change_warning()
        else:
            self.fault_scr.change_normal()

        # if 1 in condition:
        #     self.ready.change_warning()
        # else:
        #     self.ready.change_normal()

        # if 2 in condition:
        #     self.pulse.change_warning()
        # else:
        #     self.pulse.change_normal()

        # if 3 in condition:
        #     self.sync.change_warning()
        # else:
        #     self.sync.change_normal()

        if 6 in condition:
            self.fault_eep_wr.change_warning()
        else:
            self.fault_eep_wr.change_normal()

        if 7 in condition:
            self.fault_tcp_comm.change_warning()
        else:
            self.fault_tcp_comm.change_normal()

        if 8 in condition:
            self.fault_internal.change_warning()
        else:
            self.fault_internal.change_normal()

        if 9 in condition:
            self.fault_uvlo.change_warning()
        else:
            self.fault_uvlo.change_normal()

        if 10 in condition:
            self.fault_ext_sync.change_warning()
        else:
            self.fault_ext_sync.change_normal()

        if 11 in condition:
            self.fault_ethercat.change_warning()
        else:
            self.fault_ethercat.change_normal()

        if 12 in condition:
            self.fault_comm.change_warning()
        else:
            self.fault_comm.change_normal()

    def reset(self):
        logging.info("DeviceAlarm :: reset")
        self.fault_scr.change_normal()
        self.fault_eep_wr.change_normal()
        self.fault_tcp_comm.change_normal()
        self.fault_internal.change_normal()
        self.fault_uvlo.change_normal()
        self.fault_ext_sync.change_normal()
        self.fault_ethercat.change_normal()
        self.fault_comm.change_normal()


class ModuleFault:
    cyanfont = "border: 3px solid lightgray;border-radius: 10px;background-color:#ccffff;color: black;font-size: 14px;font-weight: bold;"
    yellowfont = "border: 3px solid lightgray;border-radius: 10px;background-color: #FFC300;color: black;font-size: 14px;font-weight: bold;"
    redfont = "border: 3px solid lightgray;border-radius: 10px;background-color: #FF0000;color: black;font-size: 14px;font-weight: bold;"

    def __init__(self, name, bcolor="#fff"):
        self.name = name 
        self.bcolor = bcolor
        self.fields = []

    def add_field(self, field):
        self.fields.append(field)

    def display(self):
        group = QGroupBox(self.name)
        group.setStyleSheet(f"background-color: {self.bcolor}")
        layout = QGridLayout()
        group.setLayout(layout)

        for idx, field in enumerate(self.fields):
            layout.addWidget(field.lbl_name, idx * 2, 0, 1, 2)
            layout.addWidget(field.rd_status1, idx * 2 + 1, 0)
            layout.addWidget(field.rd_status2, idx * 2 + 1, 1)

        return group

    def update_warning(self, faults):
        for idx, val in enumerate(faults):
            self.fields[val].change_warning()

    def reset(self):
        # logging.info(f"ModuleFault RESET :: {self.name}")
        for idx, field in enumerate(self.fields):
            field.reset()

    def __str__(self):
        return f"#### ModuleFault = {self.name}"
    