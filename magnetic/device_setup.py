from PyQt5.QtWidgets import QLabel, QLineEdit
from PyQt5.QtWidgets import QGroupBox, QVBoxLayout, QHBoxLayout, QDialogButtonBox
from PyQt5.QtWidgets import QApplication, QDialog, QRadioButton
from PyQt5 import QtCore
from PyQt5.QtGui import QFont
import sys
import serial

GRAPH = 1
DATA = 2

# print(dir(serial))
def check_stopbit(x): 
    return {'7 Bit': 7, '8 Bit': 8, '1 Bit': 1, '2 Bit': 2, '9600': 9600, '19200': 19200, '38400': 38400, '57600':57600, '115200': 115200, 'NONE': serial.PARITY_NONE, 'ODD': serial.PARITY_ODD, 'EVEN': serial.PARITY_EVEN, 'MARK': serial.PARITY_MARK, 'SPACE': serial.PARITY_SPACE,}[x]

def get_comm_port():
    port_list = []
    import serial.tools.list_ports
    ports = serial.tools.list_ports.comports()
    for port, desc, hwid in sorted(ports):
        if desc != 'n/a':
            port_list.append(port)
    return port_list

class SettingWin(QDialog):
    def __init__(self, Dialog):
        super().__init__()
        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        self.setGeometry(100, 100, 400, 300)
        font = QFont()
        font.setPointSize(12)

        self.selected = None
        self.ipaddress = None 
        self.ipport = None 

        self.gen_port = None
        self.com_speed = 115200
        self.com_data = 8
        self.com_parity = serial.PARITY_NONE
        self.com_stop = 1
        self.graph_type = None

    # Main 
        main_layer = QVBoxLayout()
        self.setLayout(main_layer)

    # 선택 TCP or RTU
        self.grp_select = QGroupBox("RTU or TCP 선택")
        layout_select = QHBoxLayout()

        self.rd_rtu = QRadioButton("RTU")
        self.rd_rtu.clicked.connect(lambda:self.radioSelectClicked(self.rd_rtu))
        layout_select.addWidget(self.rd_rtu)
        self.rd_tcp = QRadioButton("TCP")
        self.rd_tcp.clicked.connect(lambda:self.radioSelectClicked(self.rd_tcp))
        layout_select.addWidget(self.rd_tcp)
        self.grp_select.setLayout(layout_select)
        main_layer.addWidget(self.grp_select)

    # IP Address 
        self.grp_ipaddress = QGroupBox("IP ADDRESS")
        layout_ipaddress = QHBoxLayout()
        self.edit_ipaddress = QLineEdit("192.168.10.1")
        layout_ipaddress.addWidget(self.edit_ipaddress)
        self.grp_ipaddress.setLayout(layout_ipaddress)
        main_layer.addWidget(self.grp_ipaddress)
        self.grp_ipaddress.hide()

    # IP Port 
        self.grp_ipport = QGroupBox("Port")
        layout_ipport = QHBoxLayout()
        self.edit_ipport = QLineEdit("5000")
        layout_ipport.addWidget(self.edit_ipport)
        self.grp_ipport.setLayout(layout_ipport)
        main_layer.addWidget(self.grp_ipport)
        self.grp_ipport.hide()


    # 통신 포트  설정(Serial Port)"
        self.grp_port = QGroupBox("통신 포트  설정(Serial Port)")
        layout_port = QHBoxLayout()

        ports = get_comm_port()
        if ports:
            if len(ports) == 1:
                port = ports[0]
                self.rd_port_1 = QRadioButton(port)
                self.rd_port_1.setChecked(True)
                self.gen_port = port
                self.rd_port_1.clicked.connect(lambda:self.radioPortClicked(self.rd_port_1))
                layout_port.addWidget(self.rd_port_1)
            elif len(ports) == 2:
                port = ports[0]
                self.rd_port_1 = QRadioButton(port)
                self.rd_port_1.setChecked(True)
                self.gen_port = port
                self.rd_port_1.clicked.connect(lambda:self.radioPortClicked(self.rd_port_1))
                layout_port.addWidget(self.rd_port_1)

                port = ports[1]
                self.rd_port_2 = QRadioButton(port)
                self.rd_port_2.clicked.connect(lambda:self.radioPortClicked(self.rd_port_2))
                layout_port.addWidget(self.rd_port_2)
            else:
                print("Too many")
        else:
            self.label_result = QLabel("현재 사용 가능한 포트가 없습니다. 연결 선을 USB에 연결하세요.")
            layout_port.addWidget(self.label_result)

        self.grp_port.setLayout(layout_port)
        main_layer.addWidget(self.grp_port)
        self.grp_port.hide()

    # 전송속도 (Baud Rate)"
        self.grp_speed = QGroupBox("전송속도 (Baud Rate)")
        self.grp_speed.setFont(font)
        layout_speed = QHBoxLayout()

        self.rd_speed_96 = QRadioButton("9600")
        self.rd_speed_96.clicked.connect(lambda:self.radioValueClicked(self.rd_speed_96, "SPEED"))
        layout_speed.addWidget(self.rd_speed_96)

        self.rd_speed_384 = QRadioButton("38400")
        self.rd_speed_384.clicked.connect(lambda:self.radioValueClicked(self.rd_speed_384, "SPEED"))
        layout_speed.addWidget(self.rd_speed_384)

        self.rd_speed_576 = QRadioButton("57600")
        self.rd_speed_576.clicked.connect(lambda:self.radioValueClicked(self.rd_speed_576, "SPEED"))
        layout_speed.addWidget(self.rd_speed_576)

        self.rd_speed_1152 = QRadioButton("115200")
        self.com_speed = 115200
        self.rd_speed_1152.clicked.connect(lambda:self.radioValueClicked(self.rd_speed_1152, "SPEED"))
        layout_speed.addWidget(self.rd_speed_1152)
        self.rd_speed_1152.setChecked(True)

        self.grp_speed.setLayout(layout_speed)
        main_layer.addWidget(self.grp_speed)
        self.grp_speed.hide()

    # SELECT GRAPH
        grp_graph = QGroupBox("그래프 타입")
        grp_graph.setFont(font)

        layout_graph = QHBoxLayout()

        self.rd_graph_with = QRadioButton("WITH GRAPH")
        self.graph_type = GRAPH
        self.rd_graph_with.clicked.connect(lambda:self.radioSelectGraph(GRAPH))
        self.rd_graph_with.setChecked(True)
        layout_graph.addWidget(self.rd_graph_with)

        self.rd_graph_data = QRadioButton("DATA COUNT IMPORTANT")
        self.rd_graph_data.clicked.connect(lambda:self.radioSelectGraph(DATA))
        layout_graph.addWidget(self.rd_graph_data)

        grp_graph.setLayout(layout_graph)
        main_layer.addWidget(grp_graph)


        self.buttonBox = QDialogButtonBox()
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        main_layer.addWidget(self.buttonBox)

        self.buttonBox.accepted.connect(self.on_accepted)
        self.buttonBox.rejected.connect(self.reject)


    def on_accepted(self):
        if self.selected == 'RTU':
            if self.gen_port and self.com_speed and self.com_data and self.com_parity and self.com_stop:
                print("All data meeted")
                self.accept()
            else:
                print("ComPort/ Speed 를 모두 선택하시요.")
        else:
            self.ipaddress = self.edit_ipaddress.text()
            self.ipport = self.edit_ipport.text()
            if self.ipaddress and self.ipport:
                print("All data meeted")
                self.accept()
            else:
                print("IP Address and Port 를 모두 선택하시요.")

    def radioSelectClicked(self, btn):
        result = btn.text()
        if result == 'RTU':
            self.selected = 'RTU'
            self.grp_ipaddress.hide()
            self.grp_ipport.hide()
            
            self.grp_port.show()
            self.grp_speed.show()
        else:
            self.selected = 'TCP'
            self.grp_port.hide()
            self.grp_speed.hide()

            self.grp_ipaddress.show()
            self.grp_ipport.show()
        
    def radioPortClicked(self, btn):
        result = btn.text()
        self.gen_port = result

    def radioValueClicked(self, btn, ptype):
        result = check_stopbit(btn.text())
        if ptype == "SPEED":
            self.com_speed = int(result)
            print("SPEED :: ", self.com_speed)
        elif ptype == "DATA":
            self.com_data = result
        elif ptype == "PARITY":
            self.com_parity = result
        elif ptype == "STOP":
            self.com_stop = result

    def radioSelectGraph(self, gtype):
        self.graph_type = gtype


if __name__ == "__main__":
    app = QApplication(sys.argv)
    Dialog = QDialog()
    mywindow = SettingWin(Dialog)
    mywindow.show()
    app.exec_()
