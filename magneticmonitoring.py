from datetime import datetime
from ctypes import c_int16

from PyQt5.QtWidgets import QWidget, QLabel, QPushButton, QDesktopWidget
from PyQt5.QtWidgets import QGroupBox,QVBoxLayout, QHBoxLayout, QGridLayout
from PyQt5.QtWidgets import QApplication, QDialog, QTabWidget
from PyQt5.QtWidgets import QLCDNumber, QStatusBar, QMainWindow
from PyQt5.QtCore import QDate, Qt, QTimer
from PyQt5.QtGui import QPixmap
import pyqtgraph as pyGraph

# import magnetic.serial as device
import magnetic.serial as device
from magnetic.setup_device import SettingWin
from magnetic.common import SetVal, resource_path, LineView
from magnetic.common import DataUpdate, CheckField, DataIntUpdate
from magnetic.basic import ModuleStatus, MDStatus, HLink, ModuleFault, DeviceSetting, VUModule
from magnetic.address import FAULT_NAMES

TPERIOD = 1000
DATA_TIME = 3000
FAULT_TIME = 10000
GRAPH_PERIOD = 5000

RTU = 'RTU'

# import logging
# FORMAT = ('%(asctime)-15s %(threadName)-15s '
#           '%(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
# logging.basicConfig(format=FORMAT)
# log = logging.getLogger()
# log.setLevel(logging.DEBUG)

def CLed(title):
    btn = QPushButton(title)
    btn.setFixedSize(80, 80)
    style = "border: 3px solid lightgray;border-radius: 40px;background-color: gray;color: white;font-size: 16px;font-weight: bold;"
    btn.setStyleSheet(style)
    return btn


class MagneticMonitoring(QMainWindow):
    def __init__(self):
        super().__init__()

        self.gen_port = None
        self.countN = 0
        self.client = None
        # self.client = 1

        self.ptype = RTU
        self.com_open_flag = False
        pyGraph.setConfigOptions(background='w')  # 흰색 배경 

        self.btn_default = "QPushButton{font-size: 12pt; color: gray; background-color: #ddd;}"
        self.btn_alarm = "QPushButton{font-size: 12pt; font-weight: bold; color: blue; background-color: white;}"
        self.main_label = "QLabel{font-size: 20pt; font-weight: bold}"
        self.font_green = 'QLabel{margin: -1px; font-size: 16pt; padding: 0px; color: green;font-weight: bold;}'
        self.font_red = 'QLabel{margin: -1px; font-size: 16pt; padding: 0px; color: red;font-weight: bold;}'

        self.indexs = []
        self.mod1_list = []
        self.mod2_list = []
        self.mod3_list = []
        self.mod4_list = []

        self.time = 0
        self.timer = QTimer()
        self.dtimer = QTimer()
        self.faulttimer = QTimer()
        self.graphtimer = QTimer()
        self.start_time = None

        self.run_flag = False 

        self.initUI()

    def initUI(self):
        self.setWindowTitle("PSTEK MAGNETIC MONITORING")
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width(), screensize.height()) 

        mainwidget = QWidget()              # 위젯의 인스턴스 생성만으로도 QMainWindow에 붙는다.
        # mainwidget.setStyleSheet(open('canon/mainstyle.css').read());
        self.setCentralWidget(mainwidget)
        main_layout = QVBoxLayout()
        mainwidget.setLayout(main_layout)

        ################### MENU
        grp_menu = QGroupBox("")
        grp_menu.setStyleSheet("QGroupBox{margin:2px;padding:2px;}")
        layout_menu = QGridLayout()
        grp_menu.setLayout(layout_menu)
        
        #### 통신 체크 
        grp_comm = QGroupBox("")
        grp_comm.setStyleSheet("QGroupBox{margin:2px;padding:2px;}")
        layout_comm = QVBoxLayout()
        grp_comm.setLayout(layout_comm)

        self.btn_com_gen = QPushButton("전원 장치 연결")
        self.btn_com_gen.clicked.connect(self.setting_device)
        self.lbl_com_gen = QLabel("")
        self.lbl_com_gen.setStyleSheet(self.font_green)

        layout_comm.addWidget(self.btn_com_gen)
        layout_comm.addWidget(self.lbl_com_gen)
        layout_menu.addWidget(grp_comm, 0, 0)

        #### Title
        grp_title = QGroupBox("")
        layout_title = QVBoxLayout()
        grp_title.setLayout(layout_title)

        title1 = QLabel("MAGNETIC")
        title2 = QLabel("MONITORING")
        title1.setStyleSheet(self.main_label)
        title2.setStyleSheet(self.main_label)
        layout_title.addWidget(title1)
        layout_title.addWidget(title2)

        layout_menu.addWidget(grp_title, 0, 1)

        ####  RUN/STOP 
        grp_run = QGroupBox("")
        layout_run = QGridLayout()
        grp_run.setLayout(layout_run)

        self.btn_run = CLed('RUN/STOP')
        # self.btn_run.setEnabled(False)
        self.btn_run.clicked.connect(self.start_pulldata)
        layout_run.addWidget(self.btn_run, 0, 0, 2, 1)
        layout_menu.addWidget(grp_run, 0, 6)


        grp_runstop = QGroupBox("")
        layout_runstop = QVBoxLayout()
        grp_runstop.setLayout(layout_runstop)

        self.btn_device_run = QPushButton("RUN")
        # self.btn_device_run.setEnabled(False)
        self.btn_device_run.setStyleSheet("QPushButton{font-size: 14pt; font-weight: bold; color: blue}")
        self.btn_device_run.clicked.connect(self.device_run)

        self.btn_device_stop = QPushButton("STOP")
        self.btn_device_stop.setStyleSheet("QPushButton{font-size: 14pt; font-weight: bold; color: red}")
        self.btn_device_stop.clicked.connect(self.device_stop)
        self.btn_device_stop.setEnabled(False)

        layout_runstop.addWidget(self.btn_device_run)
        layout_runstop.addWidget(self.btn_device_stop)

        layout_menu.addWidget(grp_runstop, 0, 7)

        #### ON/OFF 
        grp_btn = QGroupBox("")
        layout_btn = QGridLayout()
        grp_btn.setLayout(layout_btn)

        self.data_time = SetVal("데이터 간격", DATA_TIME, "m sec", layout_btn, 0)
        self.fault_time = SetVal("fault 간격", FAULT_TIME, "m sec", layout_btn, 1)

        layout_menu.addWidget(grp_btn, 0, 8)

        #### TIME 
        grp_time = QGroupBox("")
        layout_time = QVBoxLayout()
        grp_time.setLayout(layout_time)

        # Timer Display
        self.display_time = QLCDNumber()
        self.display_time.setDigitCount(8)
        self.display_time.setStyleSheet('background: white')
        timedisplay = '{:02d}:{:02d}:{:02d}'.format(
                (self.time // 60) // 60, (self.time // 60) % 60, self.time % 60)
        self.display_time.display(timedisplay)

        layout_time.addWidget(self.display_time)
        layout_menu.addWidget(grp_time, 0, 9)


        grp_setup = QGroupBox("")
        layout_setup = QVBoxLayout()
        grp_setup.setLayout(layout_setup)

        self.btn_faultclear = QPushButton("FAULT CLEAR")
        self.btn_faultclear.clicked.connect(self.fault_clear)
        layout_setup.addWidget(self.btn_faultclear)

        self.btn_graphdata = QPushButton("LOG UPDATE CMD")
        self.btn_graphdata.clicked.connect(self.req_graphdata)
        layout_setup.addWidget(self.btn_graphdata)

        self.label_start_time = QLabel("")
        layout_setup.addWidget(self.label_start_time)
        layout_menu.addWidget(grp_setup, 0, 10)

        # Logo
        grp_logo = QGroupBox("")
        layout_logo = QVBoxLayout()
        grp_logo.setLayout(layout_logo)


        labelLogo = QLabel("")
        pixmap = QPixmap(resource_path("logo.png"))
        labelLogo.setAlignment(Qt.AlignCenter)
        labelLogo.setPixmap(pixmap)
        layout_logo.addWidget(labelLogo)

        
        self.message = QLabel("")
        self.message.setStyleSheet(self.font_green)
        layout_logo.addWidget(self.message)
        layout_menu.addWidget(grp_logo, 0, 11)

        main_layout.addWidget(grp_menu)

    ##### MODULE FAULT
        grp_fault = QGroupBox("")
        layout_fault = QHBoxLayout()
        grp_fault.setLayout(layout_fault)

        self.md1_fault = CheckField("MD1_FAULT", "NORMAL", "FAULT")
        layout_fault.addWidget(self.md1_fault.groupdisplay())
        self.md2_fault = CheckField("MD2_FAULT", "NORMAL", "FAULT")
        layout_fault.addWidget(self.md2_fault.groupdisplay())
        self.md3_fault = CheckField("MD3_FAULT", "NORMAL", "FAULT")
        layout_fault.addWidget(self.md3_fault.groupdisplay())
        self.md4_fault = CheckField("MD4_FAULT", "NORMAL", "FAULT")
        layout_fault.addWidget(self.md4_fault.groupdisplay())
        
        main_layout.addWidget(grp_fault)

    ##### TAB Layout
        # Tab 생성
        tab_main = QWidget()
        tab_module = QWidget()
        tab_graph = QWidget()
        tab_alarm = QWidget()
        tab_update = QWidget()
        tab_append = QWidget()

        tabs = QTabWidget()
        tabs.setStyleSheet(open('style.css').read());
        tabs.addTab(tab_main, 'MAIN')
        tabs.addTab(tab_module, 'MODULES')
        tabs.addTab(tab_graph, 'GRAPH')
        tabs.addTab(tab_alarm, 'ALARM')
        tabs.addTab(tab_update, 'VALUE CHANGE #1')
        tabs.addTab(tab_append, 'VALUE CHANGE #2')
        main_layout.addWidget(tabs)

        ## 4th ########################
        # Status
        self.statusbar = QStatusBar()
        self.setStatusBar(self.statusbar)
        self.statusbar.setObjectName("statusbar")
        
        self.statusmessage = 'PSTEK, {},  {}'
        now = QDate.currentDate()
        displaymessage = self.statusmessage.format(now.toString(Qt.ISODate), 'PSTEK is aiming for a global leader. !!')
        self.statusbar.showMessage(displaymessage)

        self.displaySetting(tab_main)
        self.displayModules(tab_module)
        self.displayGraph(tab_graph)
        self.displayAlarm(tab_alarm)
        self.displayValueChange1(tab_update)
        self.displayValueChange2(tab_append)

        self.show()

    def displaySetting(self, tab):
        main_layer = QHBoxLayout()
        tab.setLayout(main_layer)

        grp_main = QGroupBox("")
        layout_main = QGridLayout()
        grp_main.setLayout(layout_main)

        self.hlink = HLink()
        group0 = self.hlink.display()
        layout_main.addWidget(group0, 0, 0, 2, 1)

        self.mdstatus1 = MDStatus('#1 Module', bcolor="#F2D7D5")
        group1 = self.mdstatus1.displayNoGroup()
        layout_main.addWidget(group1, 0, 1)

        self.mdstatus2 = MDStatus('#2 Module', bcolor="#D4E6F1")
        group2 = self.mdstatus2.displayNoGroup()
        layout_main.addWidget(group2, 0, 2)

        self.mdstatus3 = MDStatus('#3 Module', bcolor="#FCF3CF")
        group3 = self.mdstatus3.displayNoGroup()
        layout_main.addWidget(group3, 1, 1)

        self.mdstatus4 = MDStatus('#4 Module', bcolor="#E8F8F5")
        group4 = self.mdstatus4.displayNoGroup()
        layout_main.addWidget(group4, 1, 2)

        main_layer.addWidget(grp_main)


    def displayModules(self, tab):
        main_layer = QGridLayout()
        tab.setLayout(main_layer)

        self.mod1 = ModuleStatus('#1 MODULE', bcolor="#F2D7D5")
        group1 = self.mod1.display()
        main_layer.addWidget(group1, 0, 0)

        self.mod2 = ModuleStatus('#2 MODULE', bcolor="#D4E6F1")
        group2 = self.mod2.display()
        main_layer.addWidget(group2, 0, 1)

        self.mod3 = ModuleStatus('#3 MODULE', bcolor="#FCF3CF")
        group3 = self.mod3.display()
        main_layer.addWidget(group3, 1, 0)

        self.mod4 = ModuleStatus('#4 MODULE', bcolor="#E8F8F5")
        group4 = self.mod4.display()
        main_layer.addWidget(group4, 1, 1)


    def displayGraph(self, tab):
        main_layer = QHBoxLayout()
        tab.setLayout(main_layer)

        grp_graph = QGroupBox("Graph")
        layout_graph = QGridLayout()
        grp_graph.setLayout(layout_graph)
        main_layer.addWidget(grp_graph)

        self.view_mod1 = LineView('#1 MODULE', self.indexs, self.mod1_list, '#DE3163')
        self.view_mod2 = LineView('#2 MODULE', self.indexs, self.mod2_list, '#FF7F50')
        self.view_mod3 = LineView('#3 MODULE', self.indexs, self.mod3_list, '#008080')
        self.view_mod4 = LineView('#4 MODULE', self.indexs, self.mod4_list, '#6495ED')

        self.graph_mod1 = self.drawLineChart(self.view_mod1)
        layout_graph.addWidget(self.graph_mod1, 1, 0)
        self.graph_mod2 = self.drawLineChart(self.view_mod2)
        layout_graph.addWidget(self.graph_mod2, 1, 1)
        self.graph_mod3 = self.drawLineChart(self.view_mod3)
        layout_graph.addWidget(self.graph_mod3, 2, 0)
        self.graph_mod4 = self.drawLineChart(self.view_mod4)
        layout_graph.addWidget(self.graph_mod4, 2, 1)


    def drawLineChart(self, dt):
        grp = QGroupBox("")
        layout = QVBoxLayout()
        grp.setLayout(layout)
        layout.addWidget(dt.btn)
        layout.addWidget(dt.plotWidget)
        return grp


    def displayAlarm(self, tab):
        main_layer = QGridLayout()
        tab.setLayout(main_layer)

        grp_alarm = QGroupBox("Alarm")
        layout_alarm = QGridLayout()
        grp_alarm.setLayout(layout_alarm)

        # self.sfields
        self.devicesetting = DeviceSetting("Device Setting")
        group_setup = self.devicesetting.display()
        main_layer.addWidget(group_setup, 0, 0)

        ## COLUMN 2
        self.fault_modules = []
        bcolors = ['#F2D7D5', '#D4E6F1', '#FCF3CF', '#E8F8F5']
        for idx in range(1, 5):
            mname = f"#{idx} MODULE"
            mod = ModuleFault(mname, bcolors[idx-1])

            for name in FAULT_NAMES:
                field = CheckField(name, 'NORMAL', 'FAULT', 0)
                mod.add_field(field)

            group = mod.display()
            main_layer.addWidget(group, 0, idx)

            self.fault_modules.append(mod)


    def displayValueChange1(self, tab):
        main_layer = QGridLayout()
        tab.setLayout(main_layer)

        self.vumod1 = VUModule(self, '#1 MODULE', address=4, loadadd=44, bcolor="#F2D7D5")
        group1 = self.vumod1.displayNoGroup()
        main_layer.addWidget(group1, 0, 0)

        self.vumod2 = VUModule(self, '#2 MODULE', address=14, loadadd=45, bcolor="#D4E6F1")
        group2 = self.vumod2.displayNoGroup()
        main_layer.addWidget(group2, 0, 1)

        self.vumod3 = VUModule(self, '#3 MODULE', address=24, loadadd=46, bcolor="#FCF3CF")
        group3 = self.vumod3.displayNoGroup()
        main_layer.addWidget(group3, 1, 0)

        self.vumod4 = VUModule(self, '#4 MODULE', address=34, loadadd=47, bcolor="#E8F8F5")
        group4 = self.vumod4.displayNoGroup()
        main_layer.addWidget(group4, 1, 1)


    def displayValueChange2(self, tab):
        main_layer = QGridLayout()
        tab.setLayout(main_layer)

        grp_left = QGroupBox("")
        layout_left = QGridLayout()
        grp_left.setLayout(layout_left)

        self.vdc_hv = DataUpdate(self, "SET VDC HV", 1, 0, 'V', 1.0, 800)
        self.vdc_hv.gridDisplay(layout_left, 0, 0)

        self.vdc_lv = DataUpdate(self, "SET VDC LV", 2, 0, 'V', 1.0, 200)
        self.vdc_lv.gridDisplay(layout_left, 1, 0)

        self.pluse_freq = DataUpdate(self, "PULSE FREQ", 3, 0, 'kHz', 0.5, 10)
        self.pluse_freq.gridDisplay(layout_left, 2, 0)

        main_layer.addWidget(grp_left, 0, 0)


        grp_right = QGroupBox("")
        layout_right = QGridLayout()
        grp_right.setLayout(layout_right)

        self.lm_level01 = DataIntUpdate(self, "LM_OFF_LEVEL1", 48, 1, '%', 1, 100)
        self.lm_level01.gridDisplay(layout_right, 1, 0)

        self.lm_level02 = DataIntUpdate(self, "LM_OFF_LEVEL2", 49, 1, '%', 1, 100)
        self.lm_level02.gridDisplay(layout_right, 2, 0)

        self.lm_level03 = DataIntUpdate(self, "LM_OFF_LEVEL3", 50, 1, '%', 1, 100)
        self.lm_level03.gridDisplay(layout_right, 3, 0)

        self.lm_level04 = DataIntUpdate(self, "LM_OFF_LEVEL4", 51, 1, '%', 1, 100)
        self.lm_level04.gridDisplay(layout_right, 4, 0)

        self.lm_level05 = DataIntUpdate(self, "LM_OFF_LEVEL5", 52, 1, '%', 1, 100)
        self.lm_level05.gridDisplay(layout_right, 5, 0)

        self.lm_level06 = DataIntUpdate(self, "LM_OFF_LEVEL6", 53, 1, '%', 1, 100)
        self.lm_level06.gridDisplay(layout_right, 6, 0)

        self.lm_level07 = DataIntUpdate(self, "LM_OFF_LEVEL7", 54, 1, '%', 1, 100)
        self.lm_level07.gridDisplay(layout_right, 7, 0)

        self.lm_level08 = DataIntUpdate(self, "LM_OFF_LEVEL8", 55, 1, '%', 1, 100)
        self.lm_level08.gridDisplay(layout_right, 8, 0)

        self.lm_level09 = DataIntUpdate(self, "LM_OFF_LEVEL9", 56, 1, '%', 1, 100)
        self.lm_level09.gridDisplay(layout_right, 9, 0)

        self.lm_level10 = DataIntUpdate(self, "LM_OFF_LEVEL10", 57, 1, '%', 1, 100)
        self.lm_level10.gridDisplay(layout_right, 10, 0)

        main_layer.addWidget(grp_right, 0, 1)


    def drawGraph(self):
        try:
            indexs = list(range(len(self.mod1_list)))
            self.view_mod1.reDraw(indexs, self.mod1_list)
            indexs = list(range(len(self.mod2_list)))
            self.view_mod2.reDraw(indexs, self.mod2_list)
            indexs = list(range(len(self.mod3_list)))
            self.view_mod3.reDraw(indexs, self.mod3_list)
            indexs = list(range(len(self.mod4_list)))
            self.view_mod4.reDraw(indexs, self.mod4_list)
        except Exception as e:
            print(" !!!!! drawGraph ", e)


    # BIT 연산 
    def bit_check(self, x):
        data = []
        for idx in range(16):
            if (x & (1<<idx)):
                data.append(idx)
        return data


    def modifyDeviceValue(self, address, value):
        print("main :: modifyDeviceValue :: ",  address, value)
        if self.client:
            device.write_registers(self.client, address, value, self.ptype)
        else:
            self.lbl_com_gen.setText("전원 장치와 연결이 필요.")


    # 전원 장치와 송신을 위한 설정
    def setting_device(self):
        if self.com_open_flag == False:
            Dialog = QDialog()
            self.com_open_flag == True
            dialog = SettingWin(Dialog)
            dialog.show()
            response = dialog.exec_()

            # OK 를 하면 설정 값을 읽어와서 통신을 한다.
            if response == QDialog.Accepted:
                if dialog.selected == 'RTU':
                    self.gen_port = dialog.gen_port
                    com_speed = dialog.com_speed
                    com_data = dialog.com_data
                    com_parity = dialog.com_parity
                    com_stop = dialog.com_stop
                    self.com_open_flag = False

                    self.client = device.connect_rtu(
                        port=self.gen_port, ptype='rtu',
                        speed=com_speed, bytesize=com_data, 
                        parity=com_parity, stopbits=com_stop
                    )
                    self.ptype = RTU
                    print("setting_device ::", self.client)
                else:
                    self.gen_port = dialog.ipaddress
                    self.ipport = int(dialog.ipport)
                    # print(self.gen_port, self.ipport)

                    self.client = device.connect_tcp(self.gen_port, self.ipport)
                    self.ptype = TCP

                # Graph
                self.graph_type = dialog.graph_type

                if self.client:
                    self.lbl_com_gen.setText(self.gen_port)
                    self.btn_com_gen.hide()
                    self.btn_run.setEnabled(True)
                    
        else:
            print("Open Dialog")


    def start_pulldata(self):
        if self.client:
            if self.run_flag:
                self.run_flag = False 
                self.btn_run.setStyleSheet("border: 3px solid lightgray;border-radius: 40px;background-color: gray;color: white;font-size: 16px;font-weight: bold;")
                self.btn_run.setText('RUN')
                if not self.timer:
                    self.timer = QTimer()
                    self.dtimer = QTimer()
                    self.faulttimer = QTimer()

                # setting timer
                self.timer.stop()
                self.dtimer.stop()
                self.faulttimer.stop()

                self.timer = None
                self.dtimer = None
                self.faulttimer = None
                self.graphtimer = None

                # self.save_data_as_json()
                
            else:

                if self.countN:
                    self.countN = 0
                    self.time = 0

                    self.indexs = []
                    self.mod1_list = []
                    self.mod2_list = []
                    self.mod3_list = []
                    self.mod4_list = []


                self.start_time = datetime.now()
                start_time = datetime.strftime(self.start_time, '%Y-%m-%d %H:%M:%S')
                self.label_start_time.setText(start_time)

                self.run_flag = True 
                self.btn_run.setStyleSheet("border: 3px solid lightgray;border-radius: 40px;background-color: green;color: white;font-size: 16px;font-weight: bold;")
                self.btn_run.setText('STOP')

                # self.btn_savedata.setEnabled(True)

                if not self.timer:
                    self.timer = QTimer()
                    self.dtimer = QTimer()
                    self.faulttimer = QTimer()
                    self.graphtimer = QTimer()

                # setting timer
                self.timer.setInterval(TPERIOD)
                self.timer.timeout.connect(self.get_time)
                self.timer.start()

                DATA_PERIOD = self.data_time.get_val()
                FAULT_PERIOD = self.fault_time.get_val()

                self.get_data()
                self.dtimer.setInterval(DATA_PERIOD)
                self.dtimer.timeout.connect(self.get_data)
                self.dtimer.start()

                self.faulttimer.setInterval(FAULT_PERIOD)
                self.faulttimer.timeout.connect(self.get_fault)
                self.faulttimer.start()

        else:
            self.lbl_com_gen.setText("전원 장치를 먼저 연결하세요.")
            self.lbl_com_gen.setStyleSheet(self.font_green)


    def stop_pulldata(self):
        self.run_flag = False 
        self.btn_run.setStyleSheet("border: 3px solid lightgray;border-radius: 40px;background-color: gray;color: white;font-size: 16px;font-weight: bold;")
        self.btn_run.setText('RUN')

        # setting timer
        self.timer.stop()
        self.dtimer.stop()
        self.faulttimer.stop()
        self.graphtimer.stop()

        self.timer = None
        self.dtimer = None
        self.faulttimer = None
        self.graphtimer = None


    def updateSetting(self, setting):
        # print("updateSetting :: ", setting)
        self.hlink.update(setting)
        self.mdstatus1.update_module(setting[3:13])
        self.mdstatus2.update_module(setting[13:23])
        self.mdstatus3.update_module(setting[23:33])
        self.mdstatus4.update_module(setting[33:43])


    def initialSetting(self, setting):
        self.vumod1.update_module(setting[3:13])
        self.vumod2.update_module(setting[13:23])
        self.vumod3.update_module(setting[23:33])
        self.vumod4.update_module(setting[33:43])


    def get_setting_value(self):
        if self.client:
            data = device.read_setting_from_device(self.client, self.ptype)
            if data['working']:
                setting = data['setting']
                # print(setting)
                self.updateSetting(setting)
            else:
                self.lbl_com_gen.setText("통신 에레 발생")
                self.lbl_com_gen.setStyleSheet(self.font_red)
        else:
            self.lbl_com_gen.setText("전원 장치를 먼저 연결하세요.")
            self.lbl_com_gen.setStyleSheet(self.font_green)


    def get_data(self):
        data = device.read_data(self.client, self.ptype)
        if data['working']:
            self.countN = self.countN  + 1
            self.indexs.append(self.countN)

            setting = data['setting']

            try:
                self.updateSetting(setting)

                mod1234 = data['mod1234']
                self.mod1.change_value(mod1234[:12])
                self.mod2.change_value(mod1234[12:23])
                self.mod3.change_value(mod1234[24:35])
                self.mod4.change_value(mod1234[36:])

                if self.countN == 1:
                    self.initialSetting(setting)

            except Exception as e:
                print(e)

            if self.countN % 20 == 0:
                self.drawGraph()
        else:
            self.lbl_com_gen.setText("통신 에레 발생")
            self.lbl_com_gen.setStyleSheet(self.font_red)


    def get_graphdata(self):
        self.graphtimer.stop()
        self.graphtimer = None

        self.mod1_list = []
        self.mod2_list = []
        self.mod3_list = []
        self.mod4_list = []

        data = device.read_graphdata(self.client, self.ptype)
        if data['working']:
            for val in data['mod1']:
                value = c_int16(val).value
                self.mod1_list.append(value)

            for val in data['mod2']:
                value = c_int16(val).value
                self.mod2_list.append(value)

            for val in data['mod3']:
                value = c_int16(val).value
                self.mod3_list.append(value)

            for val in data['mod4']:
                value = c_int16(val).value
                self.mod4_list.append(value)

            self.drawGraph()


    def module_update(self, condition):
        if 1 in condition:
            self.md1_fault.change_warning()
        if 2 in condition:
            self.md2_fault.change_warning()
        if 3 in condition:
            self.md3_fault.change_warning()
        if 4 in condition:
            self.md4_fault.change_warning()


    def save_data(self):
        pass


    def save_data_as_json(self):
        pass

    # READ DATA 
    def read_data(self):
       pass

    def get_fault(self):
        data = device.read_fault(self.client, self.ptype)
        print("GET FAULT :: ", data)
        if data:
            for idx, val in enumerate(data):
                if val:
                    faults = self.bit_check(val)
                    if idx == 0:
                        self.devicesetting.status_update(faults)
                    elif idx == 3:
                        self.module_update(faults)
                    elif idx == 4:
                        self.fault_modules[0].update_warning(faults)
                        self.md1_fault.change_warning()
                    elif idx == 5:
                        self.fault_modules[1].update_warning(faults)
                        self.md2_fault.change_warning()
                    elif idx == 6:
                        self.fault_modules[2].update_warning(faults)
                        self.md3_fault.change_warning()
                    elif idx == 7:
                        self.fault_modules[3].update_warning(faults)
                        self.md4_fault.change_warning()


    def fault_clear(self):
        if self.client:
            device.write_registers(self.client, 0, 4, self.ptype)
        else:
            self.lbl_com_gen.setText("전원 장치와 연결이 필요.")

        self.fault_reset()


    def req_graphdata(self):
        if self.client:
            device.write_registers(self.client, 0, 32770, self.ptype)
        else:
            self.lbl_com_gen.setText("전원 장치와 연결이 필요.")

        self.graphtimer = QTimer()
        self.graphtimer.setInterval(GRAPH_PERIOD)
        self.graphtimer.timeout.connect(self.get_graphdata)
        self.graphtimer.start()


    def get_time(self):
        self.time += 1
        print(self.time)
        timedisplay = '{:02d}:{:02d}:{:02d}'.format((self.time // 60) // 60, 
                        (self.time // 60) % 60, self.time % 60)
        self.display_time.display(timedisplay)


    def fault_reset(self):
        self.md1_fault.change_normal()
        self.md2_fault.change_normal()
        self.md3_fault.change_normal()
        self.md4_fault.change_normal()

        self.devicesetting.warning.change_normal()
        self.devicesetting.fault.change_normal()

        for mod in self.fault_modules:
            mod.reset()


    # Run Device (중간에 Run 을 눌러서 디바이스 시작시킴)
    def device_run(self):
        # 전원 장치를 시작하고, 데이터를 읽어옴
        device.write_registers(self.client, 
                device.SETTING, device.RUN_VALUE, self.ptype)
        self.btn_device_stop.setEnabled(True)

        if not self.run_flag:
            # setting timer
            self.start_pulldata()

    # 단말기 STOP
    def device_stop(self):
        device.write_registers(self.client, 
                device.SETTING, device.STOP_VALUE, self.ptype)


if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    ex = MagneticMonitoring()
    sys.exit(app.exec_())

