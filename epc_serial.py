from sys import platform
from random import randint
import serial
import time
from ctypes import c_int16

from epc_address import *

UNIT = 0x1

## RTU
PTYPE = 'rtu'
# PTYPE = 'tcp'

if PTYPE == 'tcp':
    PORT = 1502
    PTYPE = 'tcp'


def make_connect(port, ptype='rtu', speed=115200, bytesize=8, parity='N', stopbits=1):
    print(port, ptype, speed, bytesize, parity, stopbits)
    if ptype == 'rtu':
        from pymodbus.client.sync import ModbusSerialClient as ModbusClient
        client = ModbusClient(method=ptype, port=port, timeout=1,
                          baudrate=speed, bytesize=bytesize, parity=parity, stopbits=stopbits)
    else: 
        from pymodbus.client.sync import ModbusTcpClient as ModbusClient
        client = ModbusClient('localhost', port=PORT)
    client.connect()

    if client:
        print("{}".format('*'*40))
        print("******************* DUN SUCCESS *********************", client)
        print("{}".format('*'*40))

    return client


def make_qrport(port):
    client = serial.Serial(port)
    return client


# Device Main Data 읽어오기 
def read_registers(client, address, count):
    try:
        result = client.read_input_registers(address, count, unit=UNIT)
        if result.function_code >= 0x80:
            print("Error Happened : ", )
        response = result.registers
    except Exception as e:
        response = []
        print("Error read_input_registers : ", e)
    return response


# TEST 용 SIMU
from random import randint
def read_registers_main(client, address):
    print("read_registers_main")
    data = []
    data.append(10500 + randint(-50, 60))    #result[0]
    data.append(7000 + randint(-50, 60))    #result[1]
    data.append(1500 + randint(-50, 60))    #result[2]
    data.append(800 + randint(-50, 60))    #result[3]
    # 105.00  KW
    # 70.00   KV
    # 1500    mA
    # 80.0    oC
  
    return data


def read_registers_simu(client, address):
    print("read_registers_simu")
    data = []
    data.append(1365 + randint(-50, 60))    #result[0]
    data.append(2726 + randint(-50, 60))    #result[1]
    data.append(4087 + randint(-50, 60))    #result[2]
    data.append(5448 + randint(-50, 60))    #result[3]
    data.append(6810 + randint(-50, 60))    #result[4]
    data.append(8171 + randint(-50, 60))    #result[5]
    data.append(9532 + randint(-50, 60))    #result[6]
    data.append(10893 + randint(-5, 5))     #result[7]
    data.append(5893 + randint(-50, 50))   #result[8]
    data.append(1300 + randint(-30, 30))    #result[9]

    data.append(1365 + randint(-50, 60))    #result[10]
    data.append(2726 + randint(-50, 60))    #result[11]
    data.append(4087 + randint(-50, 60))    #result[12]
    data.append(5448 + randint(-50, 60))    #result[13]
    data.append(6810 + randint(-50, 60))    #result[14]
    data.append(8171 + randint(-50, 60))    #result[15]
    data.append(9532 + randint(-50, 60))    #result[16]
    data.append(10893 + randint(-5, 5))     #result[17]
    data.append(7893 + randint(-50, 50))   #result[18]
    data.append(1400 + randint(-30, 30))    #result[19]

    data.append(1365 + randint(-50, 60))    #result[20]
    data.append(2726 + randint(-50, 60))    #result[21]
    data.append(4087 + randint(-50, 60))    #result[22]
    data.append(5448 + randint(-50, 60))    #result[23]
    data.append(6810 + randint(-50, 60))    #result[24]
    data.append(8171 + randint(-50, 60))    #result[25]
    data.append(9532 + randint(-50, 60))    #result[26]
    data.append(10893 + randint(-5, 5))     #result[27]
    data.append(8893 + randint(-50, 50))   #result[28]
    data.append(1500 + randint(-30, 30))    #result[29]

    data.append(440 + randint(-30, 30))       #result[30]
    data.append(440 + randint(-30, 30))       #result[31]
    data.append(440 + randint(-30, 30))       #result[32]
    data.append(440 + randint(-30, 30))       #result[33]
    data.append(440 + randint(-30, 30))       #result[34]
    data.append(440 + randint(-30, 30))       #result[35]
    data.append(440 + randint(-30, 30))       #result[36]
    data.append(440 + randint(-30, 30))       #result[37]

    # ISMPS
    data.append(150 + randint(-50, 60))     #result[38]
    data.append(10500 + randint(-50, 60))   #result[39]
    data.append(120 + randint(-50, 60))     #result[40]
    data.append(800 + randint(-50, 60))     #result[41]
    data.append(484 + randint(-50, 60))     #result[42]
    data.append(484 + randint(-50, 60))     #result[43]
    data.append(randint(0, 1))              #result[44]
    data.append(20893 + randint(-50, 50))   #result[45]

    return data


def read_fault(client):
    # return random.randint(2450, 2500)
    try:
        result = client.read_input_registers(READ_ADDRESS_FAULT, 1, unit=UNIT)
        if result.function_code >= 0x80:
            print("Error Happened : ", )
        response = result.registers
        result = c_int16(response[0]).value
    
    except Exception as e:
        print("read_fault ", e)
        result = 0
    return result


# Device Initial Setting Value 읽어오기 
def read_setting_value(client):
    data = {}
    try:
        response = client.read_input_registers(READ_WRITE_VOLTAGE1, 22, unit=UNIT)
        if response.function_code >= 0x80:
            print("Error Happened : ", )
            return data

        result = response.registers
        voltage1 = c_int16(result[0]).value
        voltage2 = c_int16(result[2]).value

        data = {}
        data['voltage1'] = voltage1
        data['current1'] = result[1] / 1000
        data['voltage2'] = voltage2
        data['current2'] = result[3] / 1000
        data['leak_fault_level'] = result[4] / 1000
        data['ro_min_fault'] = result[5] / 1000
        data['up_time'] = result[6] / 10
        data['down_time'] = result[7] / 10
        data['rd_mode'] = result[8]
        data['toggle_count'] = result[9]
        data['slope'] = result[10]
        data['coeff'] = result[11]
        data['rd_select'] = result[13]
        data['local_address'] = result[14]
        data['arc_delay'] = result[15] / 100
        data['arc_rate'] = result[16]
        data['rd_toggle'] = result[17]
        data['rd_arc'] = result[18]
        data['rd_ocp'] = result[19]
        data['target_cap'] = result[20]
        data['cap_deviation'] = result[21]
        return data

    except Exception as e:
        response = []
        print("Error read_setting_value : ", e)
    return response

# Write Register
def write_registers(client, start_address, value):
    print(start_address, value)
    try:
        response = client.write_registers(start_address, value, unit=UNIT)
        print(start_address, value)
    except Exception as e:
        response = []
        print("Error write_registers : ", e)
    return response


# Read FeedBack Data
def read_feedback(client, address):
    try:
        result = client.read_input_registers(address, 1, unit=UNIT)
        if result.function_code >= 0x80:
            print("Error Happened : ", )
        response = result.registers
    except Exception as e:
        response = []
        print("Error read_input_registers : ", e)
    return response

def device_run(client):
    response = write_registers(client, WRITE_BIT_POWER_ON, RUN_VALUE)
    return response


def device_stop(client):
    response = write_registers(client, WRITE_BIT_POWER_ON, STOP_VALUE)
    return response
