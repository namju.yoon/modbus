from sys import platform
from random import randint
import serial
import time
from ctypes import c_int16

from .smilekey_address import *

## RTU
PTYPE = 'rtu'
# PTYPE = 'tcp'

if PTYPE == 'tcp':
    PORT = 1502
    PTYPE = 'tcp'

from pymodbus.constants import Defaults
Defaults.RetryOnEmpty = True
Defaults.Timeout = 5
Defaults.Retries = 3

def make_connect(port, method='rtu', speed=38400, bytesize=8, parity='N', stopbits=1):
    print(port, method, speed, bytesize, parity, stopbits)
    if method == 'rtu':
        from pymodbus.client.sync import ModbusSerialClient as ModbusClient
        client = ModbusClient(method=method, port=port, timeout=1,
                          baudrate=speed, bytesize=bytesize, parity=parity, stopbits=stopbits)

        ## Multiple board 
        # client = serial.Serial(port=port, baudrate=speed, parity=parity,
        #           stopbits=stopbits, bytesize=8, timeout=1)
        # client = serial.Serial(port=port, 
        #             baudrate=speed, parity='N',
        #           stopbits=1, bytesize=8, timeout=1)

    else: 
        from pymodbus.client.sync import ModbusTcpClient as ModbusClient
        client = ModbusClient('localhost', port=PORT)
    client.connect()

    if client:
        print("{}".format('*'*40))
        print("******************* DUN SUCCESS *********************", client)
        print("{}".format('*'*40))

    return client


# JIG CALIBRATION VALUE 읽어오기 
def read_jig_cal_value(client, address):
    try:
        response = client.read_input_registers(address, 12, unit=UNIT)
        if response.function_code >= 0x80:
            print("Error Happened : ", )
        result = response.registers
        data = {}

        resonantcurrent = c_int16(result[0]).value
        data['resonantcurrent'] = resonantcurrent

        batteryvoltage = c_int16(result[1]).value
        data['batteryvoltage'] = batteryvoltage

        v5 = c_int16(result[2]).value
        data['v5'] = v5

        v33 = c_int16(result[3]).value
        data['v33'] = v33

        v18 = c_int16(result[4]).value
        data['v18'] = v18

        v6 = c_int16(result[5]).value
        data['v6'] = v6

        dccurrent = c_int16(result[6]).value
        data['dccurrent'] = dccurrent

        chargingcurrent1 = c_int16(result[7]).value
        data['chargingcurrent1'] = chargingcurrent1

        chargingcurrent2 = c_int16(result[8]).value
        data['chargingcurrent2'] = chargingcurrent2

        chargingcurrent3 = c_int16(result[9]).value
        data['chargingcurrent3'] = chargingcurrent3

        bias25 = c_int16(result[10]).value
        data['bias25'] = bias25

        bias165 = c_int16(result[11]).value
        data['bias165'] = bias165

        return data
    except Exception as e:
        response = {}
        print("Error read_input_registers : ", e)
    return response


# SMILEKEY CALIBRATION VALUE 읽어오기 
def read_smilekey_cal_value(client):
    try:
        response = client.read_input_registers(TARGET1_POWER, 12, unit=UNIT)

        if response.function_code >= 0x80:
            print("Error Happened : ", )
        result = response.registers
        data = {}

        power = c_int16(result[0]).value
        data['power'] = power

        voltage = c_int16(result[1]).value
        data['voltage'] = voltage

        dc_current = c_int16(result[2]).value
        data['dc_current'] = dc_current

        resonant_current = c_int16(result[3]).value
        data['resonant_current'] = resonant_current

        battery_voltage = c_int16(result[4]).value
        data['battery_voltage'] = battery_voltage

        charging_current = c_int16(result[5]).value
        data['charging_current'] = charging_current

        phase = c_int16(result[6]).value
        data['phase'] = phase

        version = c_int16(result[7]).value
        data['version'] = version

        otp1 = c_int16(result[8]).value
        data['otp1'] = otp1

        otp2 = c_int16(result[9]).value
        data['otp2'] = otp2

        id_lsw = c_int16(result[10]).value
        data['id_lsw'] = id_lsw

        id_msw = c_int16(result[11]).value
        data['id_msw'] = id_msw

        return data

    except Exception as e:
        response = {}
        print("Error read_input_registers : ", e)
    return response


def read_smilekey_common(client):
    try:
        response = client.read_input_registers(JIG_5V_TARGET1, 4, unit=UNIT)
        
        if response.function_code >= 0x80:
            print("Error Happened : ", )
        result = response.registers
        data = {}

        v5 = c_int16(result[0]).value
        data['v5'] = v5

        v33 = c_int16(result[1]).value
        data['v33'] = v33

        v18 = c_int16(result[2]).value
        data['v18'] = v18

        v6 = c_int16(result[3]).value
        data['v6'] = v6

        return data

    except Exception as e:
        response = {}
        print("Error read_input_registers : ", e)
    return response


def read_smilekey_offsetgain(client):
    try:
        response = client.read_input_registers(REPORT_CALI_IR_OFFSET, 7, unit=UNIT)
        
        if response.function_code >= 0x80:
            print("Error Happened : ", )
        result = response.registers
        data = {}

        ir_offset = c_int16(result[0]).value
        data['ir_offset'] = ir_offset

        ir_gain = c_int16(result[1]).value
        data['ir_gain'] = ir_gain

        idc_offset = c_int16(result[2]).value
        data['idc_offset'] = idc_offset

        idc_gain = c_int16(result[3]).value
        data['idc_gain'] = idc_gain

        battery_voltage_gain = c_int16(result[4]).value
        data['battery_voltage_gain'] = battery_voltage_gain

        charging_current_offset = c_int16(result[5]).value
        data['charging_current_offset'] = charging_current_offset

        charging_current_gain = c_int16(result[6]).value
        data['charging_current_gain'] = charging_current_gain

        return data

    except Exception as e:
        response = {}
        print("Error read_input_registers : ", e)
    return response

# Device Main Data 읽어오기 
def read_registers_simu(client, address):
    try:
        data = {}
        data['resonantcurrent'] = randint(-999, 999)
        data['batteryvoltage'] = randint(-999, 999)
        data['v5'] = randint(-999, 999)
        data['v33'] = randint(-999, 999)
        data['v18'] = randint(-999, 999)
        data['v6'] = randint(-999, 999)
        data['dccurrent'] = randint(-999, 999)
        data['chargingcurrent1'] = randint(-999, 999)
        data['chargingcurrent2'] = randint(-999, 999)
        data['chargingcurrent3'] = randint(-999, 999)
        data['bias25'] = randint(-999, 999)
        data['bias165'] = randint(-999, 999)
        return data

    except Exception as e:
        response = []
        print("Error read_input_registers : ", e)
    return response


def read_fault(client):
    # return random.randint(2450, 2500)
    try:
        result = client.read_input_registers(READ_ADDRESS_FAULT, 1, unit=UNIT)
        if result.function_code >= 0x80:
            print("Error Happened : ", )
        response = result.registers
        result = c_int16(response[0]).value
    
    except Exception as e:
        print("read_fault ", e)
        result = 0
    return result


def read_fault(client, address):
    # return random.randint(2450, 2500)
    try:
        result = client.read_input_registers(address, 1, unit=UNIT)
        if result.function_code >= 0x80:
            print("Error Happened : ", )
        response = result.registers
        result = c_int16(response[0]).value
    
    except Exception as e:
        print("read_fault ", e)
        result = 0
    return result


# Device Initial Setting Value 읽어오기 
def read_board_setting_value(client, address):
    data = {}
    try:
        response = client.read_input_registers(address, 24, unit=UNIT)
        if response.function_code >= 0x80:
            print("Error Happened : ", )
            return data

        data = {}
        result = response.registers

        iroffset = c_int16(result[0]).value
        data['iroffset'] = iroffset

        vbatoffset = c_int16(result[1]).value
        data['vbatoffset'] = vbatoffset

        v5offset = c_int16(result[2]).value
        data['v5offset'] = v5offset

        v33offset = c_int16(result[3]).value
        data['v33offset'] = v33offset

        v18offset = c_int16(result[4]).value
        data['v18offset'] = v18offset

        v6offset = c_int16(result[5]).value
        data['v6offset'] = v6offset

        chargingcurrent1offset = c_int16(result[6]).value
        data['chargingcurrent1offset'] = chargingcurrent1offset

        dccurrentoffset = c_int16(result[7]).value
        data['dccurrentoffset'] = dccurrentoffset

        chargingcurrent2offset = c_int16(result[8]).value
        data['chargingcurrent2offset'] = chargingcurrent2offset

        bias25voffset = c_int16(result[9]).value
        data['bias25voffset'] = bias25voffset

        biasv165voffset = c_int16(result[10]).value
        data['biasv165voffset'] = biasv165voffset

        chargingcurrent3offset = c_int16(result[11]).value
        data['chargingcurrent3offset'] = chargingcurrent3offset

        irgain = c_int16(result[12]).value
        data['irgain'] = irgain

        vbatgain = c_int16(result[13]).value
        data['vbatgain'] = vbatgain

        v5gain = c_int16(result[14]).value
        data['v5gain'] = v5gain

        v33gain = c_int16(result[15]).value
        data['v33gain'] = v33gain

        v18gain = c_int16(result[16]).value
        data['v18gain'] = v18gain

        v6gain = c_int16(result[17]).value
        data['v6gain'] = v6gain

        chargingcurrent1gain = c_int16(result[18]).value
        data['chargingcurrent1gain'] = chargingcurrent1gain

        dccurrentgain = c_int16(result[19]).value
        data['dccurrentgain'] = dccurrentgain

        chargingcurrent2gain = c_int16(result[20]).value
        data['chargingcurrent2gain'] = chargingcurrent2gain

        bias25vgain = c_int16(result[21]).value
        data['bias25vgain'] = bias25vgain

        biasv165vgain = c_int16(result[22]).value
        data['biasv165vgain'] = biasv165vgain

        chargingcurrent3gain = c_int16(result[23]).value
        data['chargingcurrent3gain'] = chargingcurrent3gain

        return data

    except Exception as e:
        data = {}
        print("Error read_setting_value : ", e)
    return data


def read_board_setting_value_simu(client, address):
    data = {}
    try:
        
        data = {}
        data['iroffset'] = randint(1, 999)
        data['vbatoffset'] = randint(1, 999)
        data['v5offset'] = randint(1, 999)
        data['v33offset'] = randint(1, 999)
        data['v18offset'] = randint(1, 999)
        data['v6offset'] = randint(1, 999)
        data['chargingcurrent1offset'] = randint(1, 999)
        data['dccurrentoffset'] = randint(1, 999)
        data['chargingcurrent2offset'] = randint(1, 999)
        data['bias25voffset'] = randint(1, 999)
        data['biasv165voffset'] = randint(1, 999)
        data['chargingcurrent3offset'] = randint(1, 999)
        data['irgain'] = randint(1, 999)
        data['vbatgain'] = randint(1, 999)
        data['v5gain'] = randint(1, 999)
        data['v33gain'] = randint(1, 999)
        data['v18gain'] = randint(1, 999)
        data['v6gain'] = randint(1, 999)
        data['chargingcurrent1gain'] = randint(1, 999)
        data['dccurrentgain'] = randint(1, 999)
        data['chargingcurrent2gain'] = randint(1, 999)
        data['bias25vgain'] = randint(1, 999)
        data['biasv165vgain'] = randint(1, 999)
        data['chargingcurrent3gain'] = randint(1, 999)
        return data

    except Exception as e:
        data = {}
        print("Error read_setting_value : ", e)
    return data


def device_start_calibration(client):
    write_registers(client, JIG_CALIBRATION, 1)


def start_agecharging_real(client):
    write_registers(client, JIG_CALIBRATION, 2)


def stop_agecharging_real(client):
    write_registers(client, JIG_CALIBRATION, 0)


def read_status_real(client):
    try:
        result = client.read_input_registers(JIG_CALIBRATION, 1, unit=UNIT)
        if result.function_code >= 0x80:
            print("Error Happened : " )

        response = result.registers
        result = c_int16(response[0]).value

    except Exception as e:
        print("read_status ", e)
        result = 0
    return result


def read_faultdevice_real(client):
    try:
        result = client.read_input_registers(JIG_FAULT, 1, unit=UNIT)
        if result.function_code >= 0x80:
            print("Error Happened : " )

        response = result.registers
        result = c_int16(response[0]).value

    except Exception as e:
        print("read_status ", e)
        result = 0
    return result



def read_swversion_real(client, nth):
    try:
        if nth == 1:
            result = client.read_input_registers(TARGET1_VERSION, 2, unit=UNIT)
            
        elif nth == 2:
            result = client.read_input_registers(TARGET2_VERSION, 2, unit=UNIT)

        elif nth == 3:
            result = client.read_input_registers(TARGET3_VERSION, 2, unit=UNIT)
        
        if result.function_code >= 0x80:
            print("Error Happened : " )

        response = result.registers
        ver = c_int16(response[0]).value

    except Exception as e:
        print("read_swversion ", e)
        ver = 0
    return ver


def read_uniqueid_real(client, nth):
    data = {}
    try:
        if nth == 1:
            result = client.read_input_registers(TARGET1_UNIQUE_ID_LSW, 2, unit=UNIT)
            
        elif nth == 2:
            result = client.read_input_registers(TARGET2_UNIQUE_ID_LSW, 2, unit=UNIT)

        elif nth == 3:
            result = client.read_input_registers(TARGET3_UNIQUE_ID_LSW, 2, unit=UNIT)
        
        if result.function_code >= 0x80:
            print("Error Happened : " )

        response = result.registers
    
    except Exception as e:
        print("read_uniqueid ", e)
        response = 0
    return response


def read_temperature_real(client, nth):
    data = {}
    try:
        if nth == 1:
            result = client.read_input_registers(TARGET1_OTP1, 2, unit=UNIT)
        elif nth == 2:
            result = client.read_input_registers(TARGET2_OTP1, 2, unit=UNIT)
        elif nth == 3:
            result = client.read_input_registers(TARGET3_OTP1, 2, unit=UNIT)
            
        response = result.registers
        otp1 = c_int16(response[0]).value
        data['otp1'] = otp1

        otp2 = c_int16(response[1]).value
        data['otp2'] = otp2

        return data

    except Exception as e:
        print("read_temperature ", e)
    return data


def read_current_real(client, nth):
    data = {}
    try:
        if nth == 1:
            result = client.read_input_registers(TARGET1_RESONANT_CURRENT, 3, unit=UNIT)
        elif nth == 2:
            result = client.read_input_registers(TARGET2_RESONANT_CURRENT, 3, unit=UNIT)
        elif nth == 3:
            result = client.read_input_registers(TARGET3_RESONANT_CURRENT, 3, unit=UNIT)
            
        response = result.registers
        ir = c_int16(response[0]).value
        ir = round(ir / 100, 2)
        data['ir'] = ir

        bv = c_int16(response[1]).value
        bv = round(bv / 100, 2)
        data['bv'] = bv

        cc = c_int16(response[2]).value
        data['cc'] = cc

        return data

    except Exception as e:
        print("read_current ", e)
    return data


def read_calistep_real(client, nth):
    data = {}
    try:
        if nth == 1:
            result = client.read_input_registers(AUTO_CALIBRATION_STEP1, 1, unit=UNIT)
        
        response = result.registers
        val = c_int16(response[0]).value

        return val

    except Exception as e:
        print("read_calistep ", e)
    return data


def read_calfault_real(client, nth):
    data = {}
    try:
        if nth == 1:
            result = client.read_input_registers(TARGET1_FAULT, 1, unit=UNIT)
        
        response = result.registers
        val = c_int16(response[0]).value

        return val

    except Exception as e:
        print("read_calfault ", e)
    return data


def read_agingstep_real(client, nth):
    data = {}
    try:
        if nth == 1:
            result = client.read_input_registers(AUTO_AGING_STEP1, 1, unit=UNIT)
        elif nth == 2:
            result = client.read_input_registers(AUTO_AGING_STEP2, 1, unit=UNIT)
        elif nth == 3:
            result = client.read_input_registers(AUTO_AGING_STEP3, 1, unit=UNIT)
            
        response = result.registers
        val = c_int16(response[0]).value

        return val

    except Exception as e:
        print("read_agingstep ", e)
    return data


def read_faultmessage_real(client, nth):
    data = {}
    try:
        if nth == 1:
            result = client.read_input_registers(TARGET1_FAULT, 1, unit=UNIT)
        elif nth == 2:
            result = client.read_input_registers(TARGET2_FAULT, 1, unit=UNIT)
        elif nth == 3:
            result = client.read_input_registers(TARGET3_FAULT, 1, unit=UNIT)
        
        response = result.registers
        val = c_int16(response[0]).value

        return val

    except Exception as e:
        print("read_faultmessage ", e)
    return data


# Write Register
def write_registers(client, address, value):
    try:
        response = client.write_registers(address, value, unit=UNIT)
        print(address, value, response)
    except Exception as e:
        response = []
        print("Error write_registers : ", e)
    return response


# Read FeedBack Data
def read_feedback(client, address):
    try:
        result = client.read_input_registers(address, 1, unit=UNIT)
        if result.function_code >= 0x80:
            print("Error Happened : ", )
        response = result.registers
    except Exception as e:
        response = []
        print("Error read_input_registers : ", e)
    return response


def device_run(client):
    response = write_registers(client, WRITE_BIT_POWER_ON, RUN_VALUE)
    return response


def device_stop(client):
    response = write_registers(client, WRITE_BIT_POWER_ON, STOP_VALUE)
    return response


def read_aging_voltage(client):
    data = {}
    try:
        result = client.read_input_registers(TARGET1_AGING_VOLTAGE, 3, unit=UNIT)
        
        response = result.registers
        vol1 = c_int16(response[0]).value
        data['vol1'] = round(vol1 / 100, 2)

        vol2 = c_int16(response[1]).value
        data['vol2'] = round(vol2 / 100, 2)

        vol3 = c_int16(response[2]).value
        data['vol3'] = round(vol3 / 100, 2)
        
        return data

    except Exception as e:
        print("read_status ", e)
    return data



from PyQt5.QtWidgets import QLineEdit, QLabel, QPushButton
from PyQt5.QtCore import pyqtSignal, pyqtSlot, Qt, QThread

class MyLineEdit(QLineEdit):
    def __init__(self, *args, **kwargs):
        super(MyLineEdit, self).__init__(*args, **kwargs)
        self.setAlignment(Qt.AlignRight)
        self.setFixedHeight(20)
        self.setFixedWidth(80)
        self.setMaximumHeight(20)
        self.setStyleSheet("color: blue; font-weight: bold;")

class MenuButton(QPushButton):
    def __init__(self, *args, **kwargs):
        super(MenuButton, self).__init__(*args, **kwargs)
        self.setMaximumHeight(30)
        self.setStyleSheet("background-color: silver;color: black; font-size: 14px;font-weight: bold;")

class MyButton(QPushButton):
    def __init__(self, *args, **kwargs):
        super(MyButton, self).__init__(*args, **kwargs)
        self.setMaximumHeight(100)
        self.setStyleSheet("background-color: silver;color: white; font-size: 14px;font-weight: bold;")

class MyLabel(QLabel):
    def __init__(self, *args, **kwargs):
        super(MyLabel, self).__init__(*args, **kwargs)
        self.setAlignment(Qt.AlignRight)
        self.setFixedWidth(60)
        self.setMaximumHeight(20)
        self.setStyleSheet("color: blue; font-weight: bold;")

class QWorker(QThread):
    finished = pyqtSignal(str)

    def __init__(self, qrreader):
        super(QThread, self).__init__()
        self.qrreader = qrreader

    def run(self):
        while True:
            bytesToRead = self.qrreader.inWaiting()
            if bytesToRead:
                try:
                    data = self.qrreader.read(bytesToRead)
                    data = data.decode('utf-8')
                    value = data[3:].strip()
                    if value:
                        print(value)
                        self.finished.emit(value)
                except Exception as e:
                    print(e)
