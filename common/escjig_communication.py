import serial
import time
import random
import numpy as np 
from sys import platform
from random import randint
from ctypes import c_int16
from .address_esc import *
from .address_jig import *


## RTU
PTYPE = 'rtu'
# PTYPE = 'tcp'

if PTYPE == 'tcp':
    PORT = 1502
    PTYPE = 'tcp'


from pymodbus.constants import Defaults
Defaults.RetryOnEmpty = True
Defaults.Timeout = 5
Defaults.Retries = 3


def cal_log(x):
    return np.log(x)


def bit_check(x):
    data = []
    for idx in range(15):
        if (x & (1<<idx)):
            data.append(idx)
    return data

def read_fault(client, address):
    # return random.randint(2450, 2500)
    try:
        result = client.read_input_registers(address, 1, unit=UNIT)
        if result.function_code >= 0x80:
            print("Error Happened : ", result.function_code)
            return 0
        response = result.registers
        result = c_int16(response[0]).value
        
        fault = bit_check(result)
    except Exception as e:
        fault = [0]
        print("read_value ", hex(address), e)
    return fault


def get_comm_port():
    port_list = []
    import serial.tools.list_ports
    ports = serial.tools.list_ports.comports()
    for port, desc, hwid in sorted(ports):
        if desc != 'n/a':
            port_list.append(port)
    return port_list


def make_connect(port, ptype='rtu', speed=38400, bytesize=8, parity='N', stopbits=1):
    if ptype == 'rtu':
        from pymodbus.client.sync import ModbusSerialClient as ModbusClient
        client = ModbusClient(method=ptype, port=port, timeout=1,
                          baudrate=speed, bytesize=bytesize, parity=parity, stopbits=stopbits)
    else: 
        from pymodbus.client.sync import ModbusTcpClient as ModbusClient
        client = ModbusClient('localhost', port=PORT)
    client.connect()

    if client:
        print("******************* DUN SUCCESS *********************", client)
        print(" ")

    return client


# ESC Device Main Data 읽어오기 
def read_esc_registers(client):
    try:
        result = client.read_input_registers(ESC_READ_VOLTAGE1, 10, unit=UNIT)
        if result.function_code >= 0x80:
            print("Error Happened : ", )
        response = result.registers
    except Exception as e:
        response = []
        print("Error read_input_registers : ", e)
    return response


def read_value(client, address):
    # return random.randint(2450, 2500)
    # print("read_value :: address == ", address)
    try:
        result = client.read_input_registers(address, 1, unit=UNIT)
        if result.function_code >= 0x80:
            print("Error Happened : ", result.function_code)
            return 0
        response = result.registers
        result = c_int16(response[0]).value
        # print("read_value ", address, result)
    except Exception as e:
        result = 0
        print("read_value ", hex(address), e)
    return result


# Write Register
def write_registers(client, start_address, value):
    try:
        response = client.write_registers(start_address, value, unit=UNIT)
    except Exception as e:
        response = []
        print("Error write_registers : ", e)
    return response


# Write Register
def check_password(client):
    print("check_password", client)
    try:
        response = client.write_registers(ESC_DEV_PASSWORD, 0xabcd, unit=UNIT)
    except Exception as e:
        response = []
        print("Error write_registers : ", e)
    return response

# ESC Device Initial Setting Value 읽어오기 
def read_esc_setting_value(client):
    data = {}
    try:
        response = client.read_input_registers(ESC_READ_SET_VOLTAGE1, 22, unit=UNIT)
        if response.function_code >= 0x80:
            print("Error Happened : ", )
            return data

        result = response.registers
        voltage1 = c_int16(result[0]).value # - 값을 읽어 들이는 방법 
        voltage2 = c_int16(result[2]).value # - 값을 읽어 들이는 방법 

        data = {}
        data['voltage1'] = voltage1
        data['current1'] = result[1] / 1000
        data['voltage2'] = voltage2
        data['current2'] = result[3] / 1000
        data['leak_fault_level'] = result[4] / 1000
        data['ro_min_fault'] = result[5] / 1000
        data['up_time'] = result[6] / 10
        data['down_time'] = result[7] / 10
        data['rd_mode'] = result[8]
        data['toggle_count'] = result[9]
        data['slope'] = result[10]
        data['coeff'] = result[11]
        data['rd_select'] = result[13]
        data['local_address'] = result[14]
        data['arc_delay'] = result[15] / 100
        data['arc_rate'] = result[16]
        data['rd_toggle'] = result[17]
        data['rd_arc'] = result[18]
        data['rd_ocp'] = result[19]
        data['target_cap'] = result[20]
        data['cap_deviation'] = result[21]
        return data

    except Exception as e:
        response = []
        print("Error read_setting_value : ", e)
    return response

    # data['voltage1'] = randint(2480, 2500)
    # data['current1'] = randint(10, 20)
    # data['voltage2'] = randint(-2500, -2480)
    # data['current2'] = randint(10, 20)

    # data['leak_fault_level'] = randint(10, 2000)
    # data['ro_min_fault'] = randint(10, 2000)
    # data['up_time'] = randint(10, 2000)
    # data['down_time'] = randint(10, 2000)
    # data['rd_mode'] = randint(0, 1)
    # data['toggle_count'] = randint(10, 2000)
    # data['slope'] = randint(10, 2000)
    # data['coeff'] = randint(10, 2000)
    # data['rd_select'] = randint(0, 1)
    # data['local_address'] = randint(10, 2000)
    # data['arc_delay'] = randint(10, 2000)
    # data['arc_rate'] = randint(10, 2000)
    # data['rd_toggle'] = randint(0, 1)
    # data['rd_arc'] = randint(0, 1)
    # data['rd_ocp'] = randint(0, 1)
    # data['target_cap'] = randint(10, 2000)
    # data['cap_deviation'] = randint(10, 2000)
    # return data

# ESC Device Initial Setting Value 읽어오기 
def read_esc_values(client):
    data = {}
    try:
        response = client.read_input_registers(ESC_READ_VOLTAGE1, 10, unit=UNIT)
        if response.function_code >= 0x80:
            print("Error Happened : ", )
            return data

        result = response.registers
        voltage1 = c_int16(result[0]).value # - 값을 읽어 들이는 방법 
        voltage2 = c_int16(result[2]).value # - 값을 읽어 들이는 방법 

        data['voltage1'] = voltage1
        data['current1'] = result[1] / 1000
        data['voltage2'] = voltage2
        data['current2'] = result[3] / 1000

        data['vbias'] = result[4]
        data['vcs'] = result[5] / 1000
        data['ics'] = result[6] / 1000
        data['cp'] = result[7] 
        data['ioleak1'] = result[8]
        data['ioleak2'] = result[9]
        
        return data

    except Exception as e:
        response = []
        print("Error read_esc_values : ", e)
    return response

    # data['voltage1'] = randint(2480, 2500)
    # data['current1'] = randint(10, 20)
    # data['voltage2'] = randint(-2500, -2480)
    # data['current2'] = randint(10, 20)

    # data['vbias'] = randint(0, 1250)
    # data['vcs'] = randint(0, 3300) / 1000
    # data['ics'] = randint(0, 3300) / 1000
    # data['cp'] = randint(0, 15000)
    # data['ioleak1'] = randint(0, 226) / 10
    # data['ioleak2'] = randint(0, 226) / 10

    # return data


# ESC Device Initial Setting Value 읽어오기 
def read_esc_offsetgain_values(client):
    data = {}
    try:
        response = client.read_input_registers(ESC_DEV_CAP_OFFSET, 27, unit=UNIT)
        if response.function_code >= 0x80:
            print("Error Happened : ", )
            return data

        result = response.registers

        data['cap_offset'] = c_int16(result[0]).value
        
        data['vo1_gain'] = c_int16(result[1]).value
        data['io1_gain'] = c_int16(result[2]).value
        data['vo1_offset'] = c_int16(result[3]).value
        data['io1_offset'] = c_int16(result[4]).value

        data['vo2_gain'] = c_int16(result[5]).value
        data['io2_gain'] = c_int16(result[6]).value
        data['vo2_offset'] = c_int16(result[7]).value
        data['io2_offset'] = c_int16(result[8]).value

        data['zcs1_gain'] = c_int16(result[11]).value
        data['zcs2_gain'] = c_int16(result[12]).value

        data['vbia_gain'] = c_int16(result[16]).value
        data['vbia_offset'] = c_int16(result[17]).value
        
        data['vbia_offset'] = c_int16(result[17]).value

        data['ir1_offset'] = c_int16(result[23]).value
        data['ir2_offset'] = c_int16(result[24]).value
        data['ir1_gain'] = c_int16(result[25]).value
        data['ir2_gain'] = c_int16(result[26]).value
        
        return data

    except Exception as e:
        response = []
        print("Error read_esc_values : ", e)
    return response


def esc_device_run(client):
    # print("ESC RUN : ", ESC_WRITE_BIT_POWER_ON, RUN_VALUE)
    response = write_registers(client, ESC_WRITE_BIT_POWER_ON, RUN_VALUE)
    return response


def esc_device_stop(client):
    # print("ESC STOP : ", ESC_WRITE_BIT_POWER_ON, STOP_VALUE)
    response = write_registers(client, ESC_WRITE_BIT_POWER_ON, STOP_VALUE)
    return response


def jig_device_run(client):
    response = write_registers(client, JIG_WRITE_BIT_POWER_ON, RUN_VALUE)
    return response


def jig_device_stop(client):
    response = write_registers(client, JIG_WRITE_BIT_POWER_ON, STOP_VALUE)
    return response


# ESC Device Main Data 읽어오기 
def read_jig_registers(client):
    print("read_jig_registers ", client, address)
    # return random.randint(2450, 2500)
    try:
        result = client.read_input_registers(JIG_READ_VOLTAGE1, 10, unit=UNIT)
        if result.function_code >= 0x80:
            print("Error Happened : ", )
        response = result.registers
    except Exception as e:
        response = []
        print("Error read_input_registers : ", e)
    return response


# JIG Device Initial Setting Value 읽어오기 
def read_jig_setting_value(client):
    data = {}
    try:
        response = client.read_input_registers(JIG_READ_SET_CAP_ON1, 13, unit=UNIT)
        if response.function_code >= 0x80:
            print("Error Happened : ", )
            return data

        data = {}
        result = response.registers
        data['cap_on1'] = result[0]
        data['cap_on2'] = result[1]
        data['cap_on3'] = result[2]
        data['cap_on4'] = result[3]
        data['cap_on5'] = result[4]
        data['cap_on6'] = result[5]
        data['cap_on7'] = result[6]
        data['cap_on8'] = result[7]
        data['cap_on9'] = result[8]
        data['cap_on10'] = result[9]
        data['cap_on11'] = result[10]
        data['cap_on12'] = result[11]
        data['cap_on13'] = result[12]
        
        return data

    except Exception as e:
        response = []
        print("Error read_jig_setting_value : ", e)
    return response
