# -*- coding: utf-8 -*-

# Python imports
import numpy as np
# import pyautogui

# PyQt5 imports
from PyQt5.QtCore import QSize
from PyQt5.QtWidgets import QPushButton

class Led(QPushButton):
    # common variable
    capsule = 1
    circle = 2
    rectangle = 3

    def __init__(self, parent, on_color='green', off_color='grey', 
                 second_color='blue', third_color='purple',
                 shape=circle, build='release', text=""):
        super().__init__(text)
        if build == 'release':
            self.setDisabled(True)
        else:  # For example 'debug'
            self.setEnabled(True)

        self._qss = 'QPushButton {{ \
                                   border: 3px solid lightgray; \
                                   border-radius: {}px; \
                                   background-color: {}; \
                                   color: white; \
                                   font-size: 14px; \
                                   font-weight: bold; \
                                 }}'
        self._on_qss = ''
        self._off_qss = ''
        self._third_qss = ''

        self._status = False
        self._end_radius = 5

        # Properties that will trigger changes on qss.
        self.__on_color = None
        self.__off_color = None
        self.__second_color = None
        self.__third_color = None
        self.__shape = None
        self.__height = 0
        # self._text = text

        self._on_color = on_color
        self._off_color = off_color
        self._second_color = second_color
        self._third_color = third_color
        self._shape = shape
        self._height = self.sizeHint().height()

        self.set_status(False)

    # =================================================== Reimplemented Methods
    def mousePressEvent(self, event):
        QPushButton.mousePressEvent(self, event)
        if self._status is False:
            self.set_status(True)
            self._update_off_qss()
        else:
            self.set_status(False)
            self._update_on_qss()

    def sizeHint(self):
        # res_w, res_h = pyautogui.size()  # Available resolution geometry
        res_w, res_h = 1080, 800
        if self._shape == Led.capsule:
            base_w = 50
            base_h = 30
        elif self._shape == Led.circle:
            base_w = 30
            base_h = 30
        elif self._shape == Led.rectangle:
            base_w = 40
            base_h = 30
        width = int(base_w * res_h/1080)
        height = int(base_h * res_h/1080)
        return QSize(width, height)

    def resizeEvent(self, event):
        self._height = self.size().height()
        QPushButton.resizeEvent(self, event)

    def setFixedSize(self, width=60, height=60):
        self._height = height
        if self._shape == Led.circle:
            QPushButton.setFixedSize(self, height, height)
        else:
            QPushButton.setFixedSize(self, width, height)

    # ============================================================== Properties
    @property
    def _on_color(self):
        return self.__on_color

    @_on_color.setter
    def _on_color(self, color):
        self.__on_color = color
        self._update_on_qss()

    @property
    def _off_color(self):
        return self.__off_color

    @_off_color.setter
    def _off_color(self, color):
        self.__off_color = color
        self._update_off_qss()

    @property
    def _second_color(self):
        return self.__second_color

    @_second_color.setter
    def _second_color(self, color):
        self.__second_color = color
        self._update_second_qss()

    @property
    def _third_color(self):
        return self.__third_color

    @_third_color.setter
    def _third_color(self, color):
        self.__third_color = color
        self._update_third_qss()

    @property
    def _shape(self):
        return self.__shape

    @_shape.setter
    def _shape(self, shape):
        self.__shape = shape
        self._update_end_radius()
        self._update_off_qss()
        self._update_on_qss()
        self._update_second_qss()
        self._update_third_qss()
        self.set_status(self._status)

    @_shape.deleter
    def _shape(self):
        del self.__shape

    @property
    def _height(self):
        return self.__height

    @_height.setter
    def _height(self, height):
        self.__height = height
        self._update_end_radius()
        self._update_off_qss()
        self._update_on_qss()
        self._update_second_qss()
        self._update_third_qss()
        self.set_status(self._status)

    # ================================================================= Methods
    def _update_on_qss(self):
        self._on_qss = self._qss.format(self._end_radius, self._on_color)

    def _update_second_qss(self):
        self._update_end_radius()
        self._on_second = self._qss.format(self._end_radius, self._second_color)

    def _update_third_qss(self):
        self._update_end_radius()
        self._on_third = self._qss.format(self._end_radius, self._third_color)

    def _update_off_qss(self):
        self._off_qss = self._qss.format(self._end_radius, self._off_color)

    def _update_end_radius(self):
        if self.__shape == Led.circle:
            self._end_radius = int(self.__height / 2)
            # print("Circle :: _update_end_radius", self.__shape, self.__height, self._end_radius)
        else:
            self._end_radius = int(self.__height / 10)
            # print("ETC :: _update_end_radius", self.__shape, self.__height, self._end_radius)

    def _toggle_on(self):
        self.setStyleSheet(self._on_qss)

    def _toggle_off(self):
        self.setStyleSheet(self._off_qss)

    def _second_on(self):
        self.setStyleSheet(self._on_second)

    def _third_on(self):
        self.setStyleSheet(self._on_third)

    def set_on_color(self, color):
        self._on_color = color

    def set_off_color(self, color):
        self._off_color = color

    def set_shape(self, shape):
        self._shape = shape

    def set_status(self, status):
        self._status = True if status else False
        if self._status is True:
            self._toggle_on()
        else:
            self._toggle_off()

    def set_change(self, status):
        if status == 'aging':
            self._toggle_on()
        elif status == 'charging':
            self._second_on()
        else:
            self._third_on()

