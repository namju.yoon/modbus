from random import randint
import pyqtgraph as pg
import numpy as np
from PyQt5.QtWidgets import *
from PyQt5 import QtCore

from .block import SimpleDisplay, DataView

class EpcModule:
    def __init__(self, title):
        # bit0  not used
        ## bit1 control ready
        ## bit2  run
        # bit3   not used
        # bit4   not used
        # bit5   not used
        # bit6   warning
        ## bit7  fault
        ## bit8  ocp1
        ## bit9  ocp2
        ## bit10 ocp3
        ## bit11 ocp4
        ## bit12 ocp5
        ## bit13 ocp6
        self.title = title
        self.status = None 
        self.rs_phase_cur = 0 
        self.st_phase_cur = 0 
        self.rs_phase_vol = 0 
        self.st_phase_vol = 0 

        self.ir1 = 0
        self.ir2 = 0
        self.ir3 = 0
        self.ir4 = 0
        self.ir5 = 0
        self.ir6 = 0

        self.temp = 0 
        self.vdc = 0 
        self.sag_count = 0 
        self.sag_fault = 0 
        self.version = 0
        self.initUI()

    def initUI(self):
        grp_mod = QGroupBox(self.title)
        layout_mod = QGridLayout()
        grp_mod.setLayout(layout_mod)

        self.d_status = SimpleDisplay("Status", "", self.status, layout_mod, 0)
        self.d_rs_phase_cur = SimpleDisplay("iins", "", self.rs_phase_cur, layout_mod, 1)
        self.d_st_phase_cur = SimpleDisplay("iinr", "", self.st_phase_cur, layout_mod, 2)
        self.d_rs_phase_vol = SimpleDisplay("vrs", "", self.rs_phase_vol, layout_mod, 3)
        self.d_st_phase_vol = SimpleDisplay("vst", "", self.st_phase_vol, layout_mod, 4)

        self.d_ir1 = SimpleDisplay("ir1", "", self.ir1, layout_mod, 6)
        self.d_ir2 = SimpleDisplay("ir2", "", self.ir2, layout_mod, 7)
        self.d_ir3 = SimpleDisplay("ir3", "", self.ir3, layout_mod, 8)
        self.d_ir4 = SimpleDisplay("ir4", "", self.ir4, layout_mod, 9)
        self.d_ir5 = SimpleDisplay("ir5", "", self.ir5, layout_mod, 10)
        self.d_ir6 = SimpleDisplay("ir6", "", self.ir6, layout_mod, 11)
        
        self.d_temp = SimpleDisplay("temp", "", self.temp, layout_mod, 13)
        self.d_vdc = SimpleDisplay("vdc", "", self.vdc, layout_mod, 14)
        self.d_sag_count = SimpleDisplay("SAG Cnt", "", self.sag_count, layout_mod, 15)
        self.d_sag_fault = SimpleDisplay("SAG Falut", "", self.sag_fault, layout_mod, 16)
        self.d_version = SimpleDisplay("Version", "", self.version, layout_mod, 17)

        return grp_mod

    def setEpcData(self, data):
        self.status.setValue(data['status'])
        self.rs_phase_cur.setValue(data['rs_phase_cur'])
        self.st_phase_cur.setValue(data['st_phase_cur'])
        self.rs_phase_vol.setValue(data['rs_phase_vol'])
        self.st_phase_vol.setValue(data['st_phase_vol'])

        self.ir1.setValue(data['ir1'])
        self.ir2.setValue(data['ir2'])
        self.ir3.setValue(data['ir3'])
        self.ir4.setValue(data['ir4'])
        self.ir5.setValue(data['ir5'])
        self.ir6.setValue(data['ir6'])

        self.temp.setValue(data['temp'])
        self.vdc.setValue(data['vdc'])
        self.sag_count.setValue(data['sag_count'])
        self.sag_fault.setValue(data['sag_fault'])
        self.version.setValue(data['version'])

        self.display()

    def display(self):
        self.d_status.update()
        self.d_rs_phase_cur.update()
        self.d_st_phase_cur.update()
        self.d_rs_phase_vol.update()
        self.d_st_phase_vol.update()

        self.d_ir1.update()
        self.d_ir2.update()
        self.d_ir3.update()
        self.d_ir4.update()
        self.d_ir5.update()
        self.d_ir6.update()
        
        self.d_temp.update()
        self.d_vdc.update()
        self.d_sag_count.update()
        self.d_sag_fault.update()
        self.d_version.update()


    def randomData(self):
        self.d_status.setValue(randint(1, 100))
        self.d_rs_phase_cur.setValue(randint(1, 100))
        self.d_st_phase_cur.setValue(randint(1, 100))
        self.d_rs_phase_vol.setValue(randint(1, 100))
        self.d_st_phase_vol.setValue(randint(1, 100))

        self.d_ir1.setValue(randint(1, 100))
        self.d_ir2.setValue(randint(1, 100))
        self.d_ir3.setValue(randint(1, 100))
        self.d_ir4.setValue(randint(1, 100))
        self.d_ir5.setValue(randint(1, 100))
        self.d_ir6.setValue(randint(1, 100))
        
        self.d_temp.setValue(randint(1, 100))
        self.d_vdc.setValue(randint(1, 100))
        self.d_sag_count.setValue(randint(1, 100))
        self.d_sag_fault.setValue(randint(1, 100))
        self.d_version.setValue(randint(1, 100))

        self.display()



