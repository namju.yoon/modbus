import sys  # We need sys so that we can pass argv to QApplication
import os
import time
import pyqtgraph as pyGraph

from PyQt5.QtWidgets import QDialog, QHBoxLayout, QDialogButtonBox, QDialogButtonBox, QGroupBox
from PyQt5.QtWidgets import QDialogButtonBox, QApplication, QVBoxLayout, QDesktopWidget, QCheckBox, QPushButton
from PyQt5 import QtCore
from pyqtgraph import PlotWidget, plot


class GraphWindow(QDialog):
    def __init__(self, Dialog, title, color, x, y):
        super().__init__()
        self.title = title
        self.x = x 
        self.y = y
        self.color = color
        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        # self.setGeometry(100, 100, 1200, 800)
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width(), screensize.height()) 

        main_layer = QVBoxLayout()
        self.setWindowTitle("GRAPH FOR EACH")
        self.setLayout(main_layer)

        self.graphWidget = pyGraph.PlotWidget()
        main_layer.addWidget(self.graphWidget)

        self.graphWidget.setBackground('w')
        self.graphWidget.setTitle(self.title, color=self.color, size="30pt")
    
        self.plot(self.x, self.y, self.color)

        self.buttonBox = QDialogButtonBox()
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        main_layer.addWidget(self.buttonBox)

        self.buttonBox.accepted.connect(self.on_accepted)
        self.buttonBox.rejected.connect(self.reject)


    def plot(self, x, y, color):
        pen = pyGraph.mkPen(color=color, width=2)
        self.graphWidget.plot(x, y, pen=pen, symbolSize=3, symbolBrush=('b'))

    def on_accepted(self):
        self.accept()


class ZoomWindow(QDialog):
    def __init__(self, Dialog, indexs, voltage1s, current1s, voltage2s, current2s, vbias, cpss, dechuck_starts, dechuck_ends):
        super().__init__()
        self.title = "ZOOM IN DATA"
        self.indexs = indexs
        self.voltage1s = voltage1s 
        self.current1s = current1s 
        self.voltage2s = voltage2s 
        self.current2s = current2s 
        self.vbias = vbias 
        self.cpss = cpss 
        self.dechuck_starts = dechuck_starts 
        self.dechuck_ends = dechuck_ends 
        # print("ZoomWindow :: __init__ :: len(voltage1s) = ", len(voltage1s))

        self.pen_voltage1 = pyGraph.mkPen(width=2, color=(255, 0, 0))
        self.pen_current1 = pyGraph.mkPen(width=2, color=(0, 0, 255))
        self.pen_voltage2 = pyGraph.mkPen(width=2, color=(255, 191, 0))
        self.pen_current2 = pyGraph.mkPen(width=2, color=(26, 175, 51))
        self.pen_vbia = pyGraph.mkPen(width=2, color=(0,0,128))
        self.pen_cps = pyGraph.mkPen(width=2, color=(204, 153, 0))
        self.pen_dechuckstart = pyGraph.mkPen(width=2, color=(204, 0, 153))
        self.pen_dechuckend = pyGraph.mkPen(width=2, color=(102, 0, 204))

        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width(), screensize.height()) 

        main_layer = QVBoxLayout()
        self.setWindowTitle("ZOOM")
        self.setLayout(main_layer)

        self.main = pyGraph.PlotWidget()
        self.main.setBackground('w')
        self.sub = pyGraph.PlotWidget()
        self.sub.setBackground('w')

        pyGraph.setConfigOptions(antialias=True)
        main_layer.addWidget(self.main)
        main_layer.addWidget(self.sub)

    ## create mainAxis
        # First ViewBox
        self.axisA = self.main.plotItem
        self.axisA.setLabels(left='Voltage')
        self.axisA.showAxis('right')

        ## Second ViewBox
        self.axisB = pyGraph.ViewBox()
        self.axisB.setXLink(self.axisA)

        self.axisA.scene().addItem(self.axisB)
        self.axisA.getAxis('right').linkToView(self.axisB)
        self.axisA.getAxis('right').setLabel('Current', color='#0000ff')

        ## create Third ViewBox. 
        self.axisC = pyGraph.ViewBox()
        ax3 = pyGraph.AxisItem('right')
        self.axisA.layout.addItem(ax3, 2, 3)
        self.axisA.scene().addItem(self.axisC)
        ax3.linkToView(self.axisC)
        self.axisC.setXLink(self.axisA)
        ax3.setLabel('Vais/CPS', color='#00ff00')

        ## create Forth ViewBox. 
        self.axisD = pyGraph.ViewBox()
        ax4 = pyGraph.AxisItem('right')
        self.axisA.layout.addItem(ax4, 2, 4)
        self.axisA.scene().addItem(self.axisD)
        ax4.linkToView(self.axisD)
        self.axisD.setXLink(self.axisA)
        ax4.setLabel('Leak Current', color='#cc9900')

        ## Handle view resizing 
        def updateViews():
            ## view has resized; update auxiliary views to match
            # global self.axisA, self.axisB, self.axisC
            self.axisB.setGeometry(self.axisA.vb.sceneBoundingRect())
            self.axisC.setGeometry(self.axisA.vb.sceneBoundingRect())
            self.axisD.setGeometry(self.axisA.vb.sceneBoundingRect())
            self.axisB.linkedViewChanged(self.axisA.vb, self.axisB.XAxis)
            self.axisC.linkedViewChanged(self.axisA.vb, self.axisC.XAxis)
            self.axisD.linkedViewChanged(self.axisA.vb, self.axisD.XAxis)

        updateViews()
        self.axisA.vb.sigResized.connect(updateViews)

        self.axisA.plot(self.indexs, self.voltage1s, pen=self.pen_voltage1, name="Voltage1s")
        self.axisA.plot(self.indexs, self.voltage2s, pen=self.pen_voltage2, name="voltage2s")
        
        self.axisB.addItem(pyGraph.PlotCurveItem(self.indexs, self.current2s, pen=self.pen_current2, name="Current2s"))
        self.axisB.addItem(pyGraph.PlotCurveItem(self.indexs, self.current1s, pen=self.pen_current1, name="Current1s"))

        self.axisC.addItem(pyGraph.PlotCurveItem(self.indexs, self.cpss, pen=self.pen_cps, name="CPS"))
        self.axisC.addItem(pyGraph.PlotCurveItem(self.indexs, self.vbias, pen=self.pen_vbia, name="Vbias"))
        
        self.axisD.addItem(pyGraph.PlotCurveItem(self.indexs, self.dechuck_starts, pen=self.pen_dechuckstart, name="Leak_Current1"))
        self.axisD.addItem(pyGraph.PlotCurveItem(self.indexs, self.dechuck_ends, pen=self.pen_dechuckend, name="Leak_Current2"))

        linZone = pyGraph.LinearRegionItem([100,150])
        linZone.setZValue(-10)
        self.axisA.addItem(linZone)

        ## create subWin
        # First ViewBox
        self.subWin = self.sub.plotItem
        self.subWin.setLabels(left='Voltage')
        self.subWin.showAxis('right')

        ## Second ViewBox
        self.subxisB = pyGraph.ViewBox()
        self.subxisB.setXLink(self.subWin)

        self.subWin.scene().addItem(self.subxisB)
        self.subWin.getAxis('right').linkToView(self.subxisB)
        self.subWin.getAxis('right').setLabel('Current', color='#0000ff')

        ## create Third ViewBox. 
        self.subxisC = pyGraph.ViewBox()
        subax3 = pyGraph.AxisItem('right')
        self.subWin.layout.addItem(subax3, 2, 3)
        self.subWin.scene().addItem(self.subxisC)
        subax3.linkToView(self.subxisC)
        self.subxisC.setXLink(self.subWin)
        subax3.setLabel('Vais/CPS', color='#000080')

        ## create Third ViewBox. 
        self.subxisD = pyGraph.ViewBox()
        subax4 = pyGraph.AxisItem('right')
        self.subWin.layout.addItem(subax4, 2, 4)
        self.subWin.scene().addItem(self.subxisD)
        subax4.linkToView(self.subxisD)
        self.subxisD.setXLink(self.subWin)
        subax4.setLabel('Dechuck', color='#cc9900')


        def updateSubViews():
            # global self.axisA, self.subxisB, self.subxisC
            self.subxisB.setGeometry(self.subWin.vb.sceneBoundingRect())
            self.subxisC.setGeometry(self.subWin.vb.sceneBoundingRect())
            self.subxisD.setGeometry(self.subWin.vb.sceneBoundingRect())
            
            self.subxisB.linkedViewChanged(self.subWin.vb, self.subxisB.XAxis)
            self.subxisC.linkedViewChanged(self.subWin.vb, self.subxisC.XAxis)
            self.subxisD.linkedViewChanged(self.subWin.vb, self.subxisD.XAxis)

        updateSubViews()
        self.axisA.vb.sigResized.connect(updateSubViews)

        self.subWin.plot(self.indexs, self.voltage1s, pen=self.pen_voltage1, name="Voltage1s")
        self.subWin.plot(self.indexs, self.voltage2s, pen=self.pen_voltage2, name="voltage2s")
        
        self.subxisB.addItem(pyGraph.PlotCurveItem(self.indexs, self.current1s, pen=self.pen_current1, name="Current1s"))
        self.subxisB.addItem(pyGraph.PlotCurveItem(self.indexs, self.current2s, pen=self.pen_current2, name="Current2s"))

        self.subxisC.addItem(pyGraph.PlotCurveItem(self.indexs, self.vbias, pen=self.pen_vbia, name="Vbias"))
        self.subxisC.addItem(pyGraph.PlotCurveItem(self.indexs, self.cpss, pen=self.pen_cps, name="CPS"))

        self.subxisD.addItem(pyGraph.PlotCurveItem(self.indexs, self.dechuck_starts, pen=self.pen_dechuckstart, name="Leak_Current1"))
        self.subxisD.addItem(pyGraph.PlotCurveItem(self.indexs, self.dechuck_ends, pen=self.pen_dechuckend, name="Leak_Current2"))
        

        # Region 이 변경되면 오른쪽 그래프를 변경해 줌
        def updateChangePlot():
            self.subWin.setXRange(*linZone.getRegion(), padding=0)

        def updateRegion():
            linZone.setRegion(self.subWin.getViewBox().viewRange()[0])

        linZone.sigRegionChanged.connect(updateChangePlot)
        self.subWin.sigXRangeChanged.connect(updateRegion)

        updateChangePlot()

    def on_accepted(self):
        self.accept()


class ZoomWindow6(QDialog):

    def __init__(self, Dialog, indexs, voltage1s, current1s, voltage2s, current2s, vbias, cpss, dechuck_starts, dechuck_ends):
        super().__init__()
        self.title = "ZOOM IN DATA"
        self.indexs = indexs 
        self.voltage1s = voltage1s 
        self.current1s = current1s 
        self.voltage2s = voltage2s 
        self.current2s = current2s 
        self.vbias = vbias 
        self.cpss = cpss 
        self.dechuck_starts = dechuck_starts 
        self.dechuck_ends = dechuck_ends 

        self.pen_voltage1 = pyGraph.mkPen(width=2, color=(255, 0, 0))
        self.pen_current1 = pyGraph.mkPen(width=2, color=(0, 0, 255))
        self.pen_voltage2 = pyGraph.mkPen(width=2, color=(255, 191, 0))
        self.pen_current2 = pyGraph.mkPen(width=2, color=(0,255, 0))
        self.pen_vbia = pyGraph.mkPen(width=2, color=(26, 175, 51))
        self.pen_cps = pyGraph.mkPen(width=2, color=(204, 153, 0))
        self.pen_dechuckstart = pyGraph.mkPen(width=2, color=(204, 0, 153))
        self.pen_dechuckend = pyGraph.mkPen(width=2, color=(102, 0, 204))

        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width(), screensize.height()) 

        main_layer = QVBoxLayout()
        self.setWindowTitle("6 Windows")
        self.setLayout(main_layer)

        self.window = pyGraph.GraphicsLayoutWidget(show=True, title='8 Window Plot')
        pyGraph.setConfigOptions(antialias=True)
        main_layer.addWidget(self.window)

        voltage_plot = self.window.addPlot(title='OUT VOLTAGE 1, 2')
        voltage_plot.plot(x=self.indexs, y=self.voltage1s, pen=self.pen_voltage1, name="OUT VOLTAGE1")
        voltage_plot.plot(x=self.indexs, y=self.voltage2s, pen=self.pen_voltage2, name="OUT VOLTAGE2")

        current_plot = self.window.addPlot(title='OUT CURRENT 1, 2')
        current_plot.plot(x=self.indexs, y=self.current1s, pen=self.pen_current1, name="OUT CURRENT1")
        current_plot.plot(x=self.indexs, y=self.current2s, pen=self.pen_current2, name="OUT CURRENT2")

        self.window.nextRow()

        vias = self.window.addPlot(title='VBIAS')
        vias.plot(x=self.indexs, y=self.vbias, pen=self.pen_vbia, name="VBIAS")

        cps = self.window.addPlot(title='CPS')
        cps.plot(x=self.indexs, y=self.cpss, pen=self.pen_vbia, name="CPS")

        self.window.nextRow()

        d_start = self.window.addPlot(title='LEAK CURRENT1')
        d_start.plot(x=self.indexs, y=self.dechuck_starts, pen=self.pen_dechuckstart, name="LEAK CURRENT1")

        cps = self.window.addPlot(title='LEAK CURRENT2')
        cps.plot(x=self.indexs, y=self.dechuck_ends, pen=self.pen_dechuckend, name="LEAK CURRENT2")
        
    def on_accepted(self):
        self.accept()


class ZoomWindowFull(QDialog):

    def __init__(self, Dialog, indexs, voltage1s, current1s, voltage2s, current2s, vbias, cpss, dechuck_starts, dechuck_ends):
        super().__init__()
        self.title = "ZOOM IN DATA"
        self.indexs = indexs 
        self.voltage1s = voltage1s 
        self.current1s = current1s 
        self.voltage2s = voltage2s 
        self.current2s = current2s 
        self.vbias = vbias 
        self.cpss = cpss 
        self.dechuck_starts = dechuck_starts 
        self.dechuck_ends = dechuck_ends 

        self.pen_voltage1 = pyGraph.mkPen(width=2, color=(255, 0, 0))
        self.pen_current1 = pyGraph.mkPen(width=2, color=(0, 0, 255))
        self.pen_voltage2 = pyGraph.mkPen(width=2, color=(255, 191, 0))
        self.pen_current2 = pyGraph.mkPen(width=2, color=(26, 175, 51))
        self.pen_vbia = pyGraph.mkPen(width=2, color=(0,0,128))
        self.pen_cps = pyGraph.mkPen(width=2, color=(204, 153, 0))
        self.pen_dechuckstart = pyGraph.mkPen(width=2, color=(204, 0, 153))
        self.pen_dechuckend = pyGraph.mkPen(width=2, color=(102, 0, 204))

        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        self.setGeometry(0, 0, 1200, 800)

        main_layer = QVBoxLayout()
        self.setLayout(main_layer)

        self.window = pyGraph.PlotWidget()
        pyGraph.setConfigOptions(antialias=True)
        main_layer.addWidget(self.window)

        ## create mainAxis
        self.mainAxis = self.window.plotItem
        self.mainAxis.setLabels(left='Voltage')
        self.mainAxis.showAxis('right')

        ## Second ViewBox
        self.axisB = pyGraph.ViewBox()
        self.mainAxis.scene().addItem(self.axisB)
        self.mainAxis.getAxis('right').linkToView(self.axisB)
        self.axisB.setXLink(self.mainAxis)
        self.mainAxis.getAxis('right').setLabel('Current', color='#0000ff')

        ## create Third ViewBox. 
        self.axisC = pyGraph.ViewBox()
        ax3 = pyGraph.AxisItem('right')
        self.mainAxis.layout.addItem(ax3, 2, 3)
        self.mainAxis.scene().addItem(self.axisC)
        ax3.linkToView(self.axisC)
        self.axisC.setXLink(self.mainAxis)
        ax3.setLabel('Vais', color='#000080')

        ## Handle view resizing 
        def updateViews():
            ## view has resized; update auxiliary views to match
            # global self.mainAxis, self.axisB, self.axisC
            self.axisB.setGeometry(self.mainAxis.vb.sceneBoundingRect())
            self.axisC.setGeometry(self.mainAxis.vb.sceneBoundingRect())
            
            self.axisB.linkedViewChanged(self.mainAxis.vb, self.axisB.XAxis)
            self.axisC.linkedViewChanged(self.mainAxis.vb, self.axisC.XAxis)

        updateViews()
        self.mainAxis.vb.sigResized.connect(updateViews)

        self.graph_voltage1s =  self.mainAxis.plot(self.indexs, self.voltage1s, pen=self.pen_voltage1, name="Voltage1s")
        self.graph_current1s =  self.axisB.addItem(pyGraph.PlotCurveItem(self.indexs, self.current1s, pen=self.pen_current1, name="Current1s"))
        self.graph_voltage2s =  self.mainAxis.plot(self.indexs, self.voltage2s, pen=self.pen_voltage2, name="voltage2s")
        self.graph_current2s =  self.axisB.addItem(pyGraph.PlotCurveItem(self.indexs, self.current2s, pen=self.pen_current2, name="Current2s"))

        self.graph_vbias =  self.axisC.addItem(pyGraph.PlotCurveItem(self.indexs, self.vbias, pen=self.pen_vbia, name="Vbias"))
        self.graph_cpss =  self.mainAxis.plot(self.indexs, self.cpss, pen=self.pen_cps, name="Cps")
        self.graph_dechuck_starts =  self.mainAxis.plot(self.indexs, self.dechuck_starts, pen=self.pen_dechuckstart, name="Leak_Current1")
        self.graph_dechuck_ends =  self.axisC.addItem(pyGraph.PlotCurveItem(self.indexs, self.dechuck_ends, pen=self.pen_dechuckend, name="Leak_Current2"))

        
    def on_accepted(self):
        self.accept()


## Pulse DC Two Channel Compare
class GraphWindowTwo(QDialog):

    def __init__(self, Dialog, title, indexs, data):
        super().__init__()
        self.title = title
        self.indexs = indexs 
        self.data = data

        self.pen_mod1 = pyGraph.mkPen(width=2, color="#800000")
        self.pen_mod2 = pyGraph.mkPen(width=2, color="#00FF00")
        self.pen_mod3 = pyGraph.mkPen(width=2, color="#0000FF")
        self.pen_mod4 = pyGraph.mkPen(width=2, color="#808000")

        self.pen_mod5 = pyGraph.mkPen(width=2, color=(255, 127, 80))
        self.pen_mod6 = pyGraph.mkPen(width=2, color=(100, 149, 237))
        self.pen_mod7 = pyGraph.mkPen(width=2, color=(222, 49, 99))
        self.pen_mod8 = pyGraph.mkPen(width=2, color=(204, 204, 255))

        self.view_mod1 = True 
        self.view_mod2 = True 
        self.view_mod3 = True 
        self.view_mod4 = True 
        self.view_mod5 = True 
        self.view_mod6 = True 
        self.view_mod7 = True 
        self.view_mod8 = True 

        self.setupUI()

    def setupUI(self):
        self.setGeometry(0, 0, 1200, 800)

        main_layer = QVBoxLayout()
        self.setLayout(main_layer)

        btn_title = QPushButton(self.title)
        btn_title.setStyleSheet(f"background-color: white;color: #ED6032; font-size: 14px;font-weight: bold;")
        main_layer.addWidget(btn_title)

        grp_graph = QGroupBox("")
        layout_graph = QHBoxLayout()
        grp_graph.setLayout(layout_graph)

        self.chbox_mod1 = QCheckBox("#1 Mod")
        self.chbox_mod1.setStyleSheet('color:#800000;')
        self.chbox_mod1.setChecked(True)
        layout_graph.addWidget(self.chbox_mod1)

        self.chbox_mod2 = QCheckBox("#2 Mod")
        self.chbox_mod2.setStyleSheet('color:#00FF00;')
        self.chbox_mod2.setChecked(True)
        layout_graph.addWidget(self.chbox_mod2)

        self.chbox_mod3 = QCheckBox("#3 Mod")
        self.chbox_mod3.setStyleSheet('color:#0000FF;')
        self.chbox_mod3.setChecked(True)
        layout_graph.addWidget(self.chbox_mod3)

        self.chbox_mod4 = QCheckBox("#4 Mod")
        self.chbox_mod4.setStyleSheet('color:#808000;')
        self.chbox_mod4.setChecked(True)
        layout_graph.addWidget(self.chbox_mod4)

        self.chbox_mod5 = QCheckBox("#5 Mod")
        self.chbox_mod5.setStyleSheet('color:#FF7F50;')
        self.chbox_mod5.setChecked(True)
        layout_graph.addWidget(self.chbox_mod5)

        self.chbox_mod6 = QCheckBox("#6 Mod")
        self.chbox_mod6.setStyleSheet('color:#6495ED;')
        self.chbox_mod6.setChecked(True)
        layout_graph.addWidget(self.chbox_mod6)

        self.chbox_mod7 = QCheckBox("#7 Mod")
        self.chbox_mod7.setStyleSheet('color:#DE3163;')
        self.chbox_mod7.setChecked(True)
        layout_graph.addWidget(self.chbox_mod7)

        self.chbox_mod8 = QCheckBox("#8 Mod")
        self.chbox_mod8.setStyleSheet('color:#CCCCFF;')
        self.chbox_mod8.setChecked(True)
        layout_graph.addWidget(self.chbox_mod8)
        
        self.btn_update = QPushButton("UPDATE GRAPH")
        self.btn_update.clicked.connect(self.UpdateGraph)
        layout_graph.addWidget(self.btn_update)

        main_layer.addWidget(grp_graph)


        self.window_1 = pyGraph.PlotWidget()
        pyGraph.setConfigOptions(antialias=True)
        main_layer.addWidget(self.window_1)

        ## create mainAxis
        self.mainAxis = self.window_1.plotItem
        self.mainAxis.setLabels(left=self.title)
        self.mainAxis.showAxis('right')

        # print("GRAPHT :: ", self.data['mod5ch1'])
        min_val = min(len(self.data['mod1ch1']), len(self.data['mod2ch1']), 
                      len(self.data['mod3ch1']), len(self.data['mod4ch1']),
                      len(self.data['mod5ch1']), len(self.data['mod6ch1']),
                      len(self.data['mod7ch1']), len(self.data['mod8ch1']))

        indexs = list(range(min_val))
        mod1ch1 = self.data['mod1ch1'][:min_val]
        mod2ch1 = self.data['mod2ch1'][:min_val]
        mod3ch1 = self.data['mod3ch1'][:min_val]
        mod4ch1 = self.data['mod4ch1'][:min_val]
        mod5ch1 = self.data['mod5ch1'][:min_val]
        mod6ch1 = self.data['mod6ch1'][:min_val]
        mod7ch1 = self.data['mod7ch1'][:min_val]
        mod8ch1 = self.data['mod8ch1'][:min_val]

        self.graph_mod1ch1 =  self.mainAxis.plot(indexs, mod1ch1, pen=self.pen_mod1, name="mod1ch1")
        self.graph_mod2ch1 =  self.mainAxis.plot(indexs, mod2ch1, pen=self.pen_mod2, name="mod2ch1")
        self.graph_mod3ch1 =  self.mainAxis.plot(indexs, mod3ch1, pen=self.pen_mod3, name="mod3ch1")
        self.graph_mod4ch1 =  self.mainAxis.plot(indexs, mod4ch1, pen=self.pen_mod4, name="mod4ch1")
        self.graph_mod5ch1 =  self.mainAxis.plot(indexs, mod5ch1, pen=self.pen_mod5, name="mod5ch1")
        self.graph_mod6ch1 =  self.mainAxis.plot(indexs, mod6ch1, pen=self.pen_mod6, name="mod6ch1")
        self.graph_mod7ch1 =  self.mainAxis.plot(indexs, mod7ch1, pen=self.pen_mod7, name="mod7ch1")
        self.graph_mod8ch1 =  self.mainAxis.plot(indexs, mod8ch1, pen=self.pen_mod8, name="mod8ch1")

        try:
            if self.data['mod1ch2']:
                self.window_2 = pyGraph.PlotWidget()
                pyGraph.setConfigOptions(antialias=True)
                main_layer.addWidget(self.window_2)

                ## create mainAxis
                self.mainAxis = self.window_2.plotItem
                self.mainAxis.setLabels(left=self.title)
                self.mainAxis.showAxis('right')

                min_val = min(len(self.data['mod1ch2']), len(self.data['mod2ch2']), 
                              len(self.data['mod3ch2']), len(self.data['mod4ch2']),
                              len(self.data['mod5ch2']), len(self.data['mod6ch2']),
                              len(self.data['mod7ch2']), len(self.data['mod8ch2']))

                indexs = list(range(min_val))
                mod1ch2 = self.data['mod1ch2'][:min_val]
                mod2ch2 = self.data['mod2ch2'][:min_val]
                mod3ch2 = self.data['mod3ch2'][:min_val]
                mod4ch2 = self.data['mod4ch2'][:min_val]
                mod5ch2 = self.data['mod5ch2'][:min_val]
                mod6ch2 = self.data['mod6ch2'][:min_val]
                mod7ch2 = self.data['mod7ch2'][:min_val]
                mod8ch2 = self.data['mod8ch2'][:min_val]

                self.graph_mod1ch2 =  self.mainAxis.plot(indexs, mod1ch2, pen=self.pen_mod1, name="mod1ch2")
                self.graph_mod2ch2 =  self.mainAxis.plot(indexs, mod2ch2, pen=self.pen_mod2, name="mod2ch2")
                self.graph_mod3ch2 =  self.mainAxis.plot(indexs, mod3ch2, pen=self.pen_mod3, name="mod3ch2")
                self.graph_mod4ch2 =  self.mainAxis.plot(indexs, mod4ch2, pen=self.pen_mod4, name="mod4ch2")
                self.graph_mod5ch2 =  self.mainAxis.plot(indexs, mod5ch2, pen=self.pen_mod5, name="mod5ch2")
                self.graph_mod6ch2 =  self.mainAxis.plot(indexs, mod6ch2, pen=self.pen_mod6, name="mod6ch2")
                self.graph_mod7ch2 =  self.mainAxis.plot(indexs, mod7ch2, pen=self.pen_mod7, name="mod7ch2")
                self.graph_mod8ch2 =  self.mainAxis.plot(indexs, mod8ch2, pen=self.pen_mod8, name="mod8ch2")

        except Exception as e:
            print(e)

    def UpdateGraph(self):
        # Mod #1
        if self.chbox_mod1.isChecked() == True:
            self.view_mod1 = True
            self.graph_mod1ch1.show()
            self.graph_mod1ch2.show()
        else:
            self.view_mod1 = False
            self.graph_mod1ch1.hide()
            self.graph_mod1ch2.hide()
        ## Mod #2
        if self.chbox_mod2.isChecked() == True:
            self.view_mod2 = True
            self.graph_mod2ch1.show()
            self.graph_mod2ch2.show()
        else:
            self.view_mod2 = False
            self.graph_mod2ch1.hide()
            self.graph_mod2ch2.hide()
        ## Mod #3
        if self.chbox_mod3.isChecked() == True:
            self.view_mod3 = True
            self.graph_mod3ch1.show()
            self.graph_mod3ch2.show()
        else:
            self.view_mod3 = False
            self.graph_mod3ch1.hide()
            self.graph_mod3ch2.hide()
        ## Mod #4
        if self.chbox_mod4.isChecked() == True:
            self.view_mod4 = True
            self.graph_mod4ch1.show()
            self.graph_mod4ch2.show()
        else:
            self.view_mod4 = False
            self.graph_mod4ch1.hide()
            self.graph_mod4ch2.hide()
        ## Mod #5
        if self.chbox_mod5.isChecked() == True:
            self.view_mod5 = True
            self.graph_mod5ch1.show()
            self.graph_mod5ch2.show()
        else:
            self.view_mod5 = False
            self.graph_mod5ch1.hide()
            self.graph_mod5ch2.hide()
        ## Mod #6
        if self.chbox_mod6.isChecked() == True:
            self.view_mod6 = True
            self.graph_mod6ch1.show()
            self.graph_mod6ch2.show()
        else:
            self.view_mod6 = False
            self.graph_mod6ch1.hide()
            self.graph_mod6ch2.hide()
        ## Mod #7
        if self.chbox_mod7.isChecked() == True:
            self.view_mod7 = True
            self.graph_mod7ch1.show()
            self.graph_mod7ch2.show()
        else:
            self.view_mod7 = False
            self.graph_mod7ch1.hide()
            self.graph_mod7ch2.hide()
        ## Mod #8
        if self.chbox_mod8.isChecked() == True:
            self.view_mod8 = True
            self.graph_mod8ch1.show()
            self.graph_mod8ch2.show()
        else:
            self.view_mod8 = False
            self.graph_mod8ch1.hide()
            self.graph_mod8ch2.hide()
        
        


    def on_accepted(self):
        self.accept()





def main():
    app = QApplication(sys.argv)
    main = ZoomWindow()
    main.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
