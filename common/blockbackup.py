import os
import sys
import time
import random
from random import randint

from PyQt5.QtWidgets import QWidget, QLabel, QLineEdit, QPushButton, QDesktopWidget, QMainWindow
from PyQt5.QtWidgets import QGroupBox, QSpinBox, QGridLayout, QApplication


def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)
    

class DataBlock(QWidget):
    def __init__(self, text, address, value, 
                minval, maxval, unit, layout, *args, **kwargs):
        super(DataBlock, self).__init__(*args, **kwargs)
        self.text = text
        self.address = address 
        self.value = value 
        self.minval = minval 
        self.maxval = maxval 
        self.unit = unit
        self.parent_layout = layout
        # print(layout)
        self.initUI()


    def initUI(self):
        # self.setGeometry(0, 0, 300, 100)
        grp_block = QGroupBox()
        grp_block.setStyleSheet('margin: -1px; padding: 0px;')
        layout_block = QGridLayout()
        grp_block.setLayout(layout_block)

        text_block = QLabel(self.text)
        unit_block = QLabel(self.unit)

        self.edit_block = QSpinBox()
        self.edit_block.setStyleSheet('margin: -1px; padding: 0px; color: red')
        self.edit_block.setRange(self.minval, self.maxval)
        self.edit_block.setValue(self.value)
        range_value = f"{self.minval} ~ {self.maxval}"
        range_block = QLabel(range_value)
        self.btn_block = QPushButton("변 경")
        self.btn_block.setStyleSheet("color: rgb(58, 134, 255);"
                    "background-color: white;"
                      "border-style: solid;"
                      "border-width: 3px;"
                      "border-radius: 20px")
        self.btn_block.clicked.connect(self.save_data)

        layout_block.addWidget(text_block, 0, 0, 1, 3)
        layout_block.addWidget(self.edit_block, 0, 4)
        layout_block.addWidget(range_block, 0, 5)
        layout_block.addWidget(unit_block, 0, 6)
        layout_block.addWidget(self.btn_block, 0, 7)

        # self.setLayout(layout_block)
        self.parent_layout.addWidget(grp_block)


    def save_data(self):
        print(f"write_data : {self.address} : {self.edit_block.value()}")



if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = DataBlock("OUTPUT POWER", 1, 42, 0, 42.00, "KW")
    ex.show()
    sys.exit(app.exec_())