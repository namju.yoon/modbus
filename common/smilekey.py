import sys
import os
import time
import serial

import numpy as np
import pandas as pd

from django.utils import timezone
from django.db import close_old_connections

from PyQt5.QtWidgets import QWidget, QLabel, QPushButton, QDesktopWidget, QMainWindow
from PyQt5.QtWidgets import QGroupBox,QVBoxLayout, QHBoxLayout, QGridLayout
from PyQt5.QtWidgets import QApplication, QDialog, QTabWidget
from PyQt5.QtWidgets import QLCDNumber, QDialogButtonBox
from PyQt5.QtCore import pyqtSlot, Qt

from PyQt5 import QtCore
from PyQt5.QtGui import QPixmap
import pyqtgraph as pg

from smilekey.models import CaliValue, InspItem
from converter.models import Converter 
from crm.models import ModelSpec

from .block import resource_path,  QWorker
from .smilekey_communication import *

CLOCK_TIMER = 1000
DATA_TIMER = 500

def CLed():
    btn = QPushButton('')
    btn.setFixedSize(60, 60)
    style = "border: 3px solid lightgray;border-radius: 30px;background-color: gray;color: white;font-size: 14px;font-weight: bold;"
    btn.setStyleSheet(style)
    return btn

def RLed():
    btn = QPushButton('')
    btn.setFixedSize(120, 120)
    style = "border: 3px solid lightgray;border-radius: 3px;background-color: gray;color: white;font-size: 14px;font-weight: bold;"
    btn.setStyleSheet(style)
    return btn

## OK No
class ConfirmDialog(QDialog):
    def __init__(self, message):
        super().__init__()
        self.setWindowTitle("사실 확인!")

        main_layer = QVBoxLayout()
        self.setLayout(main_layer)

        self.label_message = QLabel(message)
        main_layer.addWidget(self.label_message)

        QBtn = QDialogButtonBox.Ok | QDialogButtonBox.Cancel

        self.buttonBox = QDialogButtonBox(QBtn)
        self.buttonBox.accepted.connect(self.on_accepted)
        self.buttonBox.rejected.connect(self.on_reject)

        main_layer.addWidget(self.buttonBox)

    def on_accepted(self):
        self.accept()

    def on_reject(self):
        self.reject()


class SmileKey:
    def __init__(self, board, nth):
        self.flag = False 
        self.end = False 
        self.chg = False
        self.client = None
        self.board = board
        self.nth = nth
        self.unique = None

        self.serial = None
        self.sw = None

        self.ir_current = None
        self.bat_voltage = None
        self.chg_current = None

        self.indexs = np.array([])
        self.otp1 = np.array([])
        self.otp2 = np.array([])

        self.blue = blue = "background-color: white;color: blue; font-size: 14px;font-weight: bold;"
        self.green = "font-size: 14px;font-weight: bold;color: white;border-radius: 30px;background-color: green"
        self.red = "font-size: 14px;font-weight: bold;color: white;border-radius: 30px;background-color: red"
        self.orange = "font-size: 14px;font-weight: bold;color: white;border-radius: 30px;background-color: orange"
        self.purple = "font-size: 14px;font-weight: bold;color: white;border-radius: 30px;background-color: purple"
        self.lime = "font-size: 14px;font-weight: bold;color: white;border-radius: 30px;background-color: lime"
        self.gray = "font-size: 14px;font-weight: bold;color: white;border-radius: 30px;background-color: gray"

        self.pen_otp1 = pg.mkPen(width=2, color=(0, 255,0))
        self.pen_otp2 = pg.mkPen(width=2, color=(204, 0, 0))

        self.skbtn_detail = "제품 # : {} \n UNIQUE : {} \n S/W : {}"

        skbtn_detail = self.skbtn_detail.format('N/A', 'N/A', 'N/A')
        self.btn_serial = MyButton(skbtn_detail)
        self.btn_serial.setEnabled(False)

        self.lbl_ircurrent = MyLabel("0")
        self.lbl_batvoltage = MyLabel("0")
        self.lbl_chgcurrent = MyLabel("0")

        # Group Connect, Status
        self.tab_connect = CLed()
        self.tab_status = CLed()
        self.tab_result = RLed()

        self.graph = pg.PlotWidget()
        self.graph.setBackground('w')

        self.graph_otp1 =  self.graph.plot(self.indexs, 
                        self.otp1, pen=self.pen_otp1, name="OTP 1")
        self.graph_otp2 =  self.graph.plot(self.indexs, 
                        self.otp2, pen=self.pen_otp2, name="OTP 2")
        
        self.skbtn_main = "{}"
        skbtn_text = self.skbtn_main.format('N/A')
        self.main_btn = MyButton(skbtn_text)
        self.main_conn = CLed()
        self.main_status = CLed()

    def setClient(self, client):
        self.client = client

    # QR ??
    def questionSerial(self):
        skbtn_display = self.skbtn_detail.format('??????', 'N/A', 'N/A')
        self.btn_serial.setText(skbtn_display)
        skbtn_text = self.skbtn_main.format('??????')
        self.main_btn.setText(skbtn_text)

    ## QR Reader Serial # 매칭
    def findSerial(self, code):
        close_old_connections()
        try:
            smkey = CaliValue.objects.get(serialNo=code)
            # print(f"{code} :: status :: {smkey.stype}")

            if smkey.stype == CaliValue.PASS:
                message = f"{code}는 이미 Aging & Charging을 마친 상태입니다. Aging & Charging을 다시 하시겠습니까?"
                dialog = ConfirmDialog(message)
                dialog.show()
                response = dialog.exec_()

                if response != QDialog.Accepted:
                    print("No 가 선택되었습니다.")
                    return

        except Exception as e:
            pass
        

        self.serial = code
        skbtn_display = self.skbtn_detail.format(self.serial, 'N/A', 'N/A')
        self.btn_serial.setText(skbtn_display)
        self.btn_serial.setStyleSheet(self.blue)
        self.btn_serial.setEnabled(False)

        skbtn_text = self.skbtn_main.format(self.serial)
        self.main_btn.setText(skbtn_text)
        self.main_btn.setStyleSheet(self.blue)        

    ## Unique
    def addUniqueFirmware(self):
        skbtn_display = self.skbtn_detail.format(self.serial, self.unique, self.sw)
        self.btn_serial.setText(skbtn_display)

    ## Connect
    def btnConnectGreen(self):
        self.tab_connect.setStyleSheet(self.green)
        self.main_conn.setStyleSheet(self.green)

    def btnNotConnect(self):
        self.tab_connect.setStyleSheet(self.red)
        self.main_conn.setStyleSheet(self.red)
        
        self.tab_status.setText('')
        self.main_status.setText('')
        self.tab_status.setStyleSheet(self.gray)
        self.main_status.setStyleSheet(self.gray)

    ## Error
    def errorHappen(self, msg):
        self.tab_result.setStyleSheet(self.red)
        self.tab_result.setText(msg)
        self.main_status.setStyleSheet(self.red)
        self.main_status.setText(msg)

    ## Aging
    def btnStartAging(self):
        self.tab_status.setStyleSheet(self.orange)
        self.tab_status.setText('Aging')
        self.main_status.setStyleSheet(self.orange)
        self.main_status.setText('Aging')

    ## Charging
    def btnStartCharging(self):
        self.tab_status.setStyleSheet(self.purple)
        self.tab_status.setText('Charging')
        self.main_status.setStyleSheet(self.purple)
        self.main_status.setText('Charging')

    ## End
    def btnEndCharging(self):
        self.tab_status.setStyleSheet(self.lime)
        self.tab_status.setText('End')
        self.tab_result.setStyleSheet(self.lime)
        self.tab_result.setText('End')
        
        self.main_status.setStyleSheet(self.lime)
        self.main_status.setText('End')

    ## 온도 
    def addTemperature(self, count, data):
        try:
            self.indexs = np.append(self.indexs, count)
            self.otp1 = np.append(self.otp1, data['otp1'])
            self.otp2 = np.append(self.otp2, data['otp2'])
        except Exception as e:
            print(e)

    ## 전류/전압
    def displayCurrentData(self, data):
        try:
            self.lbl_ircurrent.setText(str(data['ir']))
            self.lbl_batvoltage.setText(str(data['bv']))
            self.lbl_chgcurrent.setText(str(data['cc']))
        except Exception as e:
            print(e)


    def addDataAndDisplay(self, count):
        main = read_temperature_real(self.client, self.nth)
        if count % 10 == 0:
            self.addTemperature(count // 10 , main)

        # BOARD 1 AGING 중에 전원 장치의 전압 전류 보여 주기 
        main = read_current_real(self.client, self.nth)
        self.displayCurrentData(main)

        # 에러가 발생하면 보여 주기 
        val = read_agingstep_real(self.client, self.nth)

        if val in [8, 9]:
            print("ERROR HAPPENDED !!!")
            val = read_faultmessage_real(self.client, self.nth)
            message = ERROR_MESSAGE[val]
            self.errorHappen(message)

        # 1분 마다 그림 그리기 
        if count % 100 == 20:
            self.drawGraph()

    def addDeviceData(self):
        ver = read_swversion_real(self.client, self.nth)
        unique = read_uniqueid_real(self.client, self.nth)

        ver = round(ver / 100, 2)
        self.sw = ver 
        uniqueid = (unique[1] * 65536 + unique[0])
        self.unique = uniqueid
        self.addUniqueFirmware()


    ## 그래프
    def drawGraph(self):
        try:
            self.graph_otp1 =  self.graph.plot(self.indexs, 
                            self.otp1, pen=self.pen_otp1, name="OTP 1")
            self.graph_otp2 =  self.graph.plot(self.indexs, 
                            self.otp2, pen=self.pen_otp2, name="OTP 2")
        except Exception as e:
            idx_count = self.indexs.size
            otp1_size = self.otp1.size 
            otp2_size = self.otp2.size 
            # print("Graph : ", idx_count, otp1_size, otp2_size)

            if idx_count > otp1_size:
                self.indexs = self.indexs[:otp1_size]
            elif otp1_size > idx_count:
                self.otp1 = self.otp1[:idx_count]
                self.otp2 = self.otp1[:idx_count]

            self.graph_otp1 =  self.graph.plot(self.indexs, 
                            self.otp1, pen=self.pen_otp1, name="OTP 1")
            self.graph_otp2 =  self.graph.plot(self.indexs, 
                            self.otp2, pen=self.pen_otp2, name="OTP 2")

    
    def print_message(self, ptype, result):
        print("\n\n", "#"*50)
        print(f"########### {self.board} :: {self.nth} :: {self.serial} :: {ptype} START #### :: {result}")
        print("#"*50)

    ## Reset
    def resetGraph(self):
        self.indexs = np.array([])
        self.otp1 = np.array([])
        self.otp2 = np.array([])

        self.graph.clear()

        self.graph_otp1 =  self.graph.plot(self.indexs, 
                        self.otp1, pen=self.pen_otp1, name="OTP 1")
        self.graph_otp2 =  self.graph.plot(self.indexs, 
                        self.otp2, pen=self.pen_otp2, name="OTP 2")

    ## all
    def resetAll(self):
        self.serial = None
        self.unique = None
        self.sw = None

        self.indexs = np.array([])
        self.otp1 = np.array([])
        self.otp2 = np.array([])

        self.graph.clear()

        self.graph_otp1 = self.graph.plot(self.indexs, 
                        self.otp1, pen=self.pen_otp1, name="OTP 1")
        self.graph_otp2 = self.graph.plot(self.indexs, 
                        self.otp2, pen=self.pen_otp2, name="OTP 2")

        self.lbl_ircurrent.setText("0")
        self.lbl_batvoltage.setText("0")
        self.lbl_chgcurrent.setText("0")

        self.tab_connect.setStyleSheet(self.gray)
        self.tab_status.setStyleSheet(self.gray)
        self.tab_result.setStyleSheet(self.gray)
        self.tab_result.setText("")
        self.btn_serial.setEnabled(True)

        skbtn_detail = self.skbtn_detail.format('N/A', 'N/A', 'N/A')
        self.btn_serial.setText(skbtn_detail)
        self.btn_serial.setStyleSheet(self.gray)

        skbtn_text = self.skbtn_main.format('N/A')
        self.main_btn.setText(skbtn_text)
        self.main_conn.setStyleSheet(self.gray)
        self.main_status.setStyleSheet(self.gray)
        self.main_status.setText("")


class Board:
    def __init__(self, parent, board):
        self.stopFlag = True
        self.client = None
        self.time = 0
        self.user = parent.user
        self.model = parent.model
        self.timer = QtCore.QTimer()
        self.dtimer = QtCore.QTimer()
        self.check_flag = False
        self.countN = 1
        self.serial_list = []

        self.smkey_flag01 = False
        self.smkey_age01 = False
        self.smkey_chg01 = False
        self.smkey_end01 = False

        self.smkey_flag02 = False
        self.smkey_age02 = False
        self.smkey_chg02 = False
        self.smkey_end02 = False

        self.smkey_flag03 = False
        self.smkey_age03 = False
        self.smkey_chg03 = False
        self.smkey_end03 = False

        self.blue = "background-color: white;color: blue; font-size: 14px;font-weight: bold;"
        self.red = "background-color: white;color: red; font-size: 14px;font-weight: bold;"
        self.bold = "font-size: 14px; font-weight: bold;"

        # Port 연결 Button
        self.btn_connport = QPushButton("전원 장치 연결 포트 선택")
        self.btn_connport.clicked.connect(lambda:parent.setting_generator(board))
        self.label_connport = QLabel("N/A")

        # Aging Button
        self.btn_agingstart = QPushButton("AGING/CHARGING START")
        self.btn_agingstart.setEnabled(False)
        self.btn_agingstart.clicked.connect(self.boardStartAging)

        # Timer Display
        self.display_time = QLCDNumber()
        self.display_time.setDigitCount(8)
        self.display_time.setStyleSheet('background: white')
        timedisplay = '{:02d}:{:02d}:{:02d}'.format(
                (self.time // 60) // 60, (self.time // 60) % 60, self.time % 60)
        self.display_time.display(timedisplay)

        # Stop
        self.btn_agingstop = QPushButton(f"{board} STOP")
        self.btn_agingstop.setEnabled(False)
        self.btn_agingstop.clicked.connect(self.boardStop)

        # Display
        self.label_display = QLabel('')

        # 데이터 저장
        self.btn_savedata = QPushButton("데이터 저장")
        self.btn_savedata.setStyleSheet(self.bold)
        
        # RESET 새로운 단말기 연결
        self.btn_reset = QPushButton("RESET 새로운 단말기 연결")
        self.btn_reset.setStyleSheet(self.bold)
        self.btn_reset.setEnabled(False)
        self.btn_reset.clicked.connect(self.boardReset)

        self.smilekey01 = SmileKey(board, 1)
        self.smilekey01.btn_serial.clicked.connect(lambda:parent.readQR(f'{board}:1'))
        self.smilekey02 = SmileKey(board, 2) 
        self.smilekey02.btn_serial.clicked.connect(lambda:parent.readQR(f'{board}:2'))
        self.smilekey03 = SmileKey(board, 3)
        self.smilekey03.btn_serial.clicked.connect(lambda:parent.readQR(f'{board}:3'))

    # ??
    def readSmileKeySerial(self, nth):
        if nth == 0:
            self.smilekey01.questionSerial()
        elif nth == 1:
            self.smilekey02.questionSerial()
        elif nth == 2:
            self.smilekey03.questionSerial()

    # matching #
    def findSmileKeySerial(self, nth, code):
        self.serial_list.append(code)
        # print(f"findSmileKeySerial :: {nth} :: {code}")
        if nth == 0:
            self.smilekey01.findSerial(code)
        elif nth == 1:
            self.smilekey02.findSerial(code)
        elif nth == 2:
            self.smilekey03.findSerial(code)

        # 보드가 연결되고, 스마일키가 연결이 되면 AGING 가능해짐
        if self.client:
            self.changeButton(self.btn_agingstart, "AGING / CHARGING START")

    def displayErrorSerial(self, nth, code):
        message = f"{code}는 등록할 수 없는 단말기 입니다."
        self.label_display.setText(message)
        self.label_display.setStyleSheet(self.red)


    ## Start Button
    def changeButton(self, btn, display_text):
        btn.setText(display_text)
        btn.setStyleSheet(self.blue)
        btn.setEnabled(True)


    def setCommPort(self, client, port):
        # print(f"\n{client} :: {port}")
        self.client = client

        self.label_connport.setText(port)
        self.btn_connport.hide()
        self.btn_agingstart.setEnabled(True)

        # Enable Serial QR
        self.smilekey01.setClient(client)
        self.smilekey02.setClient(client)
        self.smilekey03.setClient(client)

        self.smilekey01.btn_serial.setEnabled(True)
        self.smilekey02.btn_serial.setEnabled(True)
        self.smilekey03.btn_serial.setEnabled(True)


    # 우선 flag = False
    # 연결된 것이 없으면 True로 놓음
    def boardStartAging(self):
        # 전원 장치를 시작하고, 데이터를 읽어옴
        self.smkey_end01 = False
        self.smkey_end02 = False
        self.smkey_end03 = False

        start_agecharging_real(self.client)
        self.btn_agingstop.setEnabled(True)
        time.sleep(2)

        # 데이터를 가져오기 시작하면 STOP 버턴 활성화
        self.stopFlag = False

        # 상태에 따라 변화를 줌
        status = read_status_real(self.client)
        result = self.bit_check(status)

        # 체크 status
        self.first_check(result)

        if self.smkey_flag01 == True:
            self.smilekey01.addDeviceData()
            
        if self.smkey_flag02 == True:
            self.smilekey02.addDeviceData()

        if self.smkey_flag03 == True:
            self.smilekey03.addDeviceData()
            
        if 0 not in result:
            self.smkey_end01 = True 

        if 1 not in result:
            self.smkey_end02 = True 

        if 2 not in result:
            self.smkey_end03 = True 

        self.timer.setInterval(CLOCK_TIMER)
        self.timer.timeout.connect(lambda:self.get_time())
        self.timer.start()

        self.dtimer.setInterval(DATA_TIMER)
        self.dtimer.timeout.connect(lambda:self.get_data())
        self.dtimer.start()

    # GET TIME
    def get_time(self):
        self.time += 1
        timedisplay = '{:02d}:{:02d}:{:02d}'.format((self.time // 60) // 60, 
                        (self.time // 60) % 60, self.time % 60)
        self.display_time.display(timedisplay)

    # GET DATA
    def get_data(self):
        # 상태에 따라 변화를 줌
        status = read_status_real(self.client)
        result = self.bit_check(status)

        # 상태 값을 읽어 오기
        self.status_check(result)

        if self.smkey_flag01 == True:
            # BOARD 1 온도 보여 주기 
            self.smilekey01.addDataAndDisplay(self.countN)


        if self.smkey_flag02 == True:
            # BOARD 1 온도 보여 주기 
            self.smilekey02.addDataAndDisplay(self.countN)


        if self.smkey_flag03 == True:
            # BOARD 1 온도 보여 주기 
            self.smilekey03.addDataAndDisplay(self.countN)

        # 모두 완료 되었으면 정지
        if self.smkey_end01 and self.smkey_end02 and self.smkey_end03:
            self.stopFlag = True
            self.boardStop()

        self.countN += 1


    # BIT 연산 
    def bit_check(self, x):
        data = []
        for idx in range(16):
            if (x & (1<<idx)):
                data.append(idx)
        return data
        
    # Aging Data Save 
    def saveAgingData(self, serial, nth):
        try:
            close_old_connections()
            data = read_aging_voltage(self.client)
            tmp = read_temperature_real(self.client, nth)
            # print("마지막 온도 ", tmp, data)
            
            smkey = CaliValue.objects.get(serialNo=serial)
            smkey.aging_voltage = data[f'vol{nth}']
            smkey.aging_at = timezone.now()
            smkey.charge_start = timezone.now()
            smkey.charge_end = None
            smkey.chg_voltage = None
            smkey.stype = smkey.NA
            smkey.otp1 = tmp['otp1']
            smkey.otp2 = tmp['otp2']
            smkey.save()
            
            message = f"{serial} : CHARGING START"
            print(message)
        except Exception as e:
            print("#"*5, serial, e)

    # Charging Data Save 
    def saveChargingData(self, serial, nth):
        try:
            close_old_connections()
            smkey = CaliValue.objects.get(serialNo=serial)
            smkey.charge_end = timezone.now()
            smkey.save()

            data = read_current_real(self.client, nth)
            smkey = CaliValue.objects.get(serialNo=serial)
            smkey.chg_voltage = data['bv']
            smkey.save()

            chg_time = smkey.charging_time

            if 4.06 <= data['bv'] <= 4.14 and \
                chg_time <= 150:
                
                smkey.stype = CaliValue.PASS
                smkey.save()

                ## Converter 제품 정보 생성
                conv, _ = Converter.objects.get_or_create(
                    author=self.user, serialNo=serial,
                    model=self.model, swver=smkey.fw_version
                )
                message = f"{serial} 전압 저장"

            else:
                smkey.stype = CaliValue.FAIL
                smkey.save()

                message = f"@ => FAIL : {serial}\n충전 VOL : {data['bv']}\n 시간 : {chg_time} "
                
                if nth == 1:
                    self.smilekey01.errorHappen(message)
                elif nth == 2:
                    self.smilekey02.errorHappen(message)
                elif nth == 3:
                    self.smilekey03.errorHappen(message)
                print(message)
        
        except Exception as e:
            print("*"*5, serial, e)


    # field status check
    def first_check(self, result):
        ##1 Connected
        if 0 in result:
            self.smilekey01.btnConnectGreen()
            self.smkey_flag01 = True

        # Target #2 Connected
        if 1 in result:
            self.smilekey02.btnConnectGreen()
            self.smkey_flag02 = True
        
        # Target #3 Connected
        if 2 in result:
            self.smilekey03.btnConnectGreen()
            self.smkey_flag03 = True
        
        # Fault 
        if 6 in result:
            status = read_faultdevice_real(self.client)
            errstatus = self.bit_check(status)

            if 0 in errstatus:
                val = read_faultmessage_real(self.client, 1)
                message = ERROR_MESSAGE[val]
                print("FAUTL HAPPENDED !!!", message)
                self.smilekey01.errorHappen(message)

            if 1 in errstatus:
                val = read_faultmessage_real(self.client, 2)
                message = ERROR_MESSAGE[val]
                print("FAUTL HAPPENDED !!!", message)
                self.smilekey02.errorHappen(message)

            if 2 in errstatus:
                val = read_faultmessage_real(self.client, 3)
                message = ERROR_MESSAGE[val]
                print("FAUTL HAPPENDED !!!", message)
                self.smilekey03.errorHappen(message)

        # Aging
        # Target #1 Aging
        if 7 in result:
            if not self.smkey_age01:
                self.smkey_age01 = True
                self.smilekey01.btnStartAging()
                self.smilekey01.print_message("FIRST TRY :: AGING", result)
            
        # Target #2 Aging
        if 8 in result:
            if not self.smkey_age02:
                self.smkey_age02 = True
                self.smilekey02.btnStartAging()
                self.smilekey02.print_message("FIRST TRY :: AGING", result)

        # Target #3 Aging
        if 9 in result:
            if not self.smkey_age03:
                self.smkey_age03 = True
                self.smilekey03.btnStartAging()
                self.smilekey03.print_message("FIRST TRY :: AGING", result)

        # Charging
        # SmileKey  #1 Charging
        if 10 in result:
            self.smilekey01.btnStartCharging()

            # *** Chart Start Time Save
            if not self.smkey_chg01:
                self.smkey_chg01 = True
                self.smilekey01.print_message("FIRST TRY :: CHARING", result)

                time.sleep(2)
                self.saveAgingData(self.smilekey01.serial, 1)

        # SmileKey  #2 Charging
        if 11 in result:
            self.smilekey02.btnStartCharging()

            # *** Chart Start Time Save
            if not self.smkey_chg02:
                self.smkey_chg02 = True
                self.smilekey02.print_message("FIRST TRY :: CHARING", result)

                time.sleep(2)
                self.saveAgingData(self.smilekey02.serial, 2)

            
        # SmileKey #3 Charging
        if 12 in result:
            self.smilekey03.btnStartCharging()

            # *** Chart Start Time Save
            if not self.smkey_chg03:
                self.smkey_chg03 = True
                self.smilekey03.print_message("FIRST TRY :: CHARING", result)

                time.sleep(2)
                self.saveAgingData(self.smilekey03.serial, 3)

        # SmileKey 1 FINISH
        if 13 in result:
            self.smilekey01.btnEndCharging()

            # *** Chart Start Time Save
            if self.smkey_chg01 and not self.smkey_end01:
                self.smkey_end01 = True
                self.smkey_flag01 = False
                self.smilekey01.print_message("FIRST TRY :: END", result)

                self.saveChargingData(self.smilekey01.serial, 1)

        if 14 in result:
            self.smilekey02.btnEndCharging()

            # *** Chart Start Time Save
            if self.smkey_chg02 and not self.smkey_end02:
                self.smkey_end02 = True
                self.smkey_flag02 = False
                self.smilekey02.print_message("FIRST TRY :: END", result)

                self.saveChargingData(self.smilekey02.serial, 2)

        if 15 in result:
            self.smilekey03.btnEndCharging()

            # *** Chart Start Time Save
            if self.smkey_chg03 and not self.smkey_end03:
                self.smkey_end03 = True
                self.smkey_flag03 = False
                self.smilekey03.print_message("END", result)

                self.saveChargingData(self.smilekey03.serial, 3)


    def status_check(self, result):
        ##1 Connected
        if self.smkey_flag01 and 0 in result:
            pass
        elif self.smkey_flag01 and 0 not in result:
            self.smilekey01.btnNotConnect()
            self.smilekey01.print_message("NOT FIRST :: CONN CUT", result)

        # Target #2 Connected
        if self.smkey_flag02 and 1 in result:
            pass
        elif self.smkey_flag02 and 1 not in result:
            self.smilekey02.btnNotConnect()
            self.smilekey02.print_message("NOT FIRST :: CONN CUT", result)
        
        # Target #3 Connected
        if self.smkey_flag03 and 2 in result:
            pass
        elif self.smkey_flag03 and 2 not in result:
            self.smilekey03.btnNotConnect()
            self.smilekey03.print_message("NOT FIRST :: CONN CUT", result)
        

        # Fault 
        if 6 in result:
            status = read_faultdevice_real(self.client)
            errstatus = self.bit_check(status)

            if 0 in errstatus:
                val = read_faultmessage_real(self.client, 1)
                message = ERROR_MESSAGE[val]
                print("FAUTL HAPPENDED !!!", message)
                self.smilekey01.errorHappen(message)

            if 1 in errstatus:
                val = read_faultmessage_real(self.client, 2)
                message = ERROR_MESSAGE[val]
                print("FAUTL HAPPENDED !!!", message)
                self.smilekey02.errorHappen(message)

            if 2 in errstatus:
                val = read_faultmessage_real(self.client, 3)
                message = ERROR_MESSAGE[val]
                print("FAUTL HAPPENDED !!!", message)
                self.smilekey03.errorHappen(message)

        # Aging
        # Target #1 Aging
        if 7 in result:
            if not self.smkey_age01:
                self.smkey_age01 = True
                self.smilekey01.btnStartAging()
                self.smilekey01.print_message("NOT FIRST :: AGING", result)
            
        # Target #2 Aging
        if 8 in result:
            if not self.smkey_age02:
                self.smkey_age02 = True
                self.smilekey02.btnStartAging()
                self.smilekey02.print_message("NOT FIRST :: AGING", result)

        # Target #3 Aging
        if 9 in result:
            if not self.smkey_age03:
                self.smkey_age03 = True
                self.smilekey03.btnStartAging()
                self.smilekey03.print_message("NOT FIRST :: AGING", result)


        # Charging
        # SmileKey  #1 Charging
        if 10 in result:
            self.smilekey01.btnStartCharging()

            # *** Chart Start Time Save
            if not self.smkey_chg01:
                self.smkey_chg01 = True 
                self.smilekey01.print_message("NOT FIRST :: CHARING", result)

                time.sleep(2)
                self.saveAgingData(self.smilekey01.serial, 1)

        # SmileKey  #2 Charging
        if 11 in result:
            self.smilekey02.btnStartCharging()

            # *** Chart Start Time Save
            if not self.smkey_chg02:
                self.smkey_chg02 = True 
                self.smilekey02.print_message("NOT FIRST :: CHARING", result)

                time.sleep(2)
                self.saveAgingData(self.smilekey02.serial, 2)

            
        # SmileKey #3 Charging
        if 12 in result:
            self.smilekey03.btnStartCharging()

            # *** Chart Start Time Save
            if not self.smkey_chg03:
                self.smkey_chg03 = True
                self.smilekey03.print_message("NOT FIRST :: CHARING", result)

                time.sleep(2)
                self.saveAgingData(self.smilekey03.serial, 3)

        # FINISH
        # SmileKey 1 
        if 13 in result:
            self.smilekey01.btnEndCharging()

            # *** Chart Start Time Save
            if self.smkey_chg01 and not self.smkey_end01:
                self.smkey_end01 = True
                self.smkey_flag01 = False
                self.smilekey01.print_message("NOT FIRST :: END", result)

                self.saveChargingData(self.smilekey01.serial, 1)

        if 14 in result:
            self.smilekey02.btnEndCharging()

            # *** Chart Start Time Save
            if self.smkey_chg02 and not self.smkey_end02:
                self.smkey_end02 = True
                self.smkey_flag02 = False
                self.smilekey02.print_message("NOT FIRST :: END", result)

                self.saveChargingData(self.smilekey02.serial, 2)

        if 15 in result:
            self.smilekey03.btnEndCharging()

            # *** Chart Start Time Save
            if self.smkey_chg03 and not self.smkey_end03:
                self.smkey_end03 = True
                self.smkey_flag03 = False
                self.smilekey03.print_message("NOT FIRST :: END", result)

                self.saveChargingData(self.smilekey03.serial, 3)


    # Reste
    def boardReset(self):
        self.stopFlag = True
        self.time = 0
        self.timer = QtCore.QTimer()
        self.dtimer = QtCore.QTimer()
        self.check_flag = False
        self.countN = 1
        self.serial_list = []

        self.smkey_flag01 = False
        self.smkey_chg01 = False
        self.smkey_end01 = False
        self.smkey_age01 = False

        self.smkey_flag02 = False
        self.smkey_chg02 = False
        self.smkey_end02 = False
        self.smkey_age02 = False

        self.smkey_flag03 = False
        self.smkey_chg03 = False
        self.smkey_end03 = False
        self.smkey_age03 = False

        timedisplay = '{:02d}:{:02d}:{:02d}'.format(
                (self.time // 60) // 60, (self.time // 60) % 60, self.time % 60)
        self.display_time.display(timedisplay)

        self.smilekey01.resetAll()
        self.smilekey02.resetAll()
        self.smilekey03.resetAll()

    # Stop 
    def boardStop(self):
        stop_agecharging_real(self.client)
        self.time = 0
        self.timer.stop()
        self.timer = None 
        self.timer = QtCore.QTimer()
        self.dtimer.stop()
        self.dtimer = None
        self.dtimer = QtCore.QTimer()
        self.countN = 1

        timedisplay = '{:02d}:{:02d}:{:02d}'.format(
                (self.time // 60) // 60, (self.time // 60) % 60, self.time % 60)
        self.display_time.display(timedisplay)

        self.smilekey01.resetGraph()
        self.smilekey02.resetGraph()
        self.smilekey03.resetGraph()

        self.btn_reset.setEnabled(True)

