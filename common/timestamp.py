import time 

def compress_timestamp(year, month, day, hour, minute, second):
    # 연도는 2000년 기준으로 압축
    year -= 2000
    compressed = (year << 25) | (month << 21) | (day << 16) | (hour << 11) | (minute << 5) | second
    return compressed

def timestamp():
    # 연도는 2000년 기준으로 압축
    year, month, day, hour, minute, second = time.localtime()[0:6]
    year -= 2000
    compressed = (year << 25) | (month << 21) | (day << 16) | (hour << 11) | (minute << 5) | second
    return compressed

def decompress_timestamp(compressed):
    # 복원
    year = ((compressed >> 25) & 0x7F) + 2000
    month = (compressed >> 21) & 0x0F
    day = (compressed >> 16) & 0x1F
    hour = (compressed >> 11) & 0x1F
    minute = (compressed >> 5) & 0x3F
    second = compressed & 0x3F
    return year, month, day, hour, minute, second


if __name__ == "__main__":
    # 예시
    compressed_value = compress_timestamp(2024, 10, 5, 15, 30, 45)
    print(f"Compressed: {compressed_value}")

    timestamp_value = timestamp()
    print(f"Timestamp: {timestamp_value}")

    year, month, day, hour, minute, second = decompress_timestamp(timestamp_value)
    print(f"Decompressed: {year}-{month}-{day} {hour}:{minute}:{second}")