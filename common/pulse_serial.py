import time
import serial
from sys import platform
from random import randint, choices

import socket
from umodbus import conf
from umodbus.client import tcp
from ctypes import c_uint16


## RTU
PTYPE = 'RTU'
# PTYPE = 'tcp'

if PTYPE == 'tcp':
    PORT = 1502
    PTYPE = 'tcp'

UNIT = 0x1
END_VALUE = 0x00ed

FAULT_ADDRESS = 0x00
PULSE_PO = 0x10
SET_POWER = 0x24
CHANNEL_STATUS_1 = 0x35 ## 18개 
CHANNEL_STATUS_2 = 0x47 ## 18개 
CHANNEL_STATUS_3 = 0x59 ## 18개 
CHANNEL_STATUS_4 = 0x6b ## 18개 
CHANNEL_STATUS_5 = 0x7d ## 18개 
CHANNEL_STATUS_6 = 0x8f ## 18개 
CHANNEL_STATUS_7 = 0xa1 ## 18개 
CHANNEL_STATUS_8 = 0xb3 ## 18개 

ADDCHANNEL_SLOW_VOLTAGE_1 = 0xc5
ADDCHANNEL_SLOW_VOLTAGE_2 = 0xcd
ADDCHANNEL_SLOW_VOLTAGE_3 = 0xd5
ADDCHANNEL_SLOW_VOLTAGE_4 = 0xdd
ADDCHANNEL_SLOW_VOLTAGE_5 = 0xe5
ADDCHANNEL_SLOW_VOLTAGE_6 = 0xed
ADDCHANNEL_SLOW_VOLTAGE_7 = 0xf5
ADDCHANNEL_SLOW_VOLTAGE_8 = 0xfd


POWER_ONOFF = 0x00


from pymodbus.constants import Defaults
Defaults.RetryOnEmpty = True
Defaults.Timeout = 5
Defaults.Retries = 3

# from pyModbusTCP.client import ModbusClient
# client = ModbusClient(host="192.168.10.10", port=5000, unit_id=1, auto_open=True)
# regs = client.read_input_registers(5, 10)


def connect_rtu(port, ptype='rtu', speed=38400, bytesize=8, parity='N', stopbits=1):
    from pymodbus.client.sync import ModbusSerialClient as ModbusClient
    client = ModbusClient(method=ptype, port=port, timeout=1,
                      baudrate=speed, bytesize=bytesize, parity=parity, stopbits=stopbits)
    client.connect()

    if client:
        print('*'*50)
        print("******************* RTU DUN SUCCESS *********************", client)
        print('*'*50)

    return client

def connect_tcp(ipaddress='192.168.10.100', ipport=5000):
    conf.SIGNED_VALUES = True
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect((ipaddress, ipport))

    if client:
        print('#'*50)
        print(f"******************* TCP {client} SUCCESS *********************")
        print('#'*50)

    return client


def make_qrport(port):
    client = serial.Serial(port)
    return client

# Device Main Data 읽어오기 
def read_data_from_device(client, ptype):
    data = {}
    if ptype == 'RTU':
        try:
            setting = read_registers(client, ptype, PULSE_PO, 37)
            # print(setting)
            if setting:
                data['setting'] = setting
                mod12 = read_registers(client, ptype, CHANNEL_STATUS_1, 36)
                data['mod12'] = mod12
                mod34 = read_registers(client, ptype, CHANNEL_STATUS_3, 36)
                data['mod34'] = mod34

                mod56 = read_registers(client, ptype, CHANNEL_STATUS_5, 36)
                data['mod56'] = mod56
                mod78 = read_registers(client, ptype, CHANNEL_STATUS_7, 36)
                data['mod78'] = mod78

                add1234 = read_registers(client, ptype, ADDCHANNEL_SLOW_VOLTAGE_1, 32)
                data['add1234'] = add1234
                add5678 = read_registers(client, ptype, ADDCHANNEL_SLOW_VOLTAGE_5, 32)
                data['add5678'] = add5678
                data['working'] = True 
                
            else:
                data['working'] = False
                data['error'] = setting['result']
            return data

        except Exception as e:
            print("** RTU :: Error read_input_registers : ", e)
    else:
        try:
            message = tcp.read_input_registers(slave_id=1, 
                        starting_address=READ_VOLTAGE1, quantity=10)
            result = tcp.send_message(message, client)

        except Exception as e:
            print("## TCP :: Error read_input_registers : ", e)

    return data


def read_setting_from_device(client, ptype):
    data = {}
    if ptype == 'RTU':
        try:
            setting = read_registers(client, ptype, PULSE_PO, 37)
            # print(setting)
            if setting:
                data['working'] = True 
                data['setting'] = setting
            else:
                data['working'] = False
            return data

        except Exception as e:
            print("** RTU :: Error read_input_registers : ", e)
            data['working'] = False
    else:
        try:
            message = tcp.read_input_registers(slave_id=1, 
                        starting_address=READ_VOLTAGE1, quantity=10)
            result = tcp.send_message(message, client)

        except Exception as e:
            print("## TCP :: Error read_input_registers : ", e)

    return data


def read_registers(client, ptype, address, count):
    try:
        response = client.read_input_registers(address, count, unit=UNIT)
        result = response.registers

        # # Simulation
        # result = []
        # for idx in range(count):
        #     if idx % 9 == 1:
        #         num = randint(1, 1000)
        #         if num > 998:
        #             result.append(3)
        #         else:
        #             result.append(0)
        #     else:
        #         result.append(randint(1, 1000))

    except Exception as e:
        print("** RTU :: Error read_input_registers : ", e)
        result = []
    return result


def read_fault_from_device(client, ptype):
    if ptype == 'RTU':
        try:
            response = client.read_input_registers(FAULT_ADDRESS, 12, unit=UNIT)
            result = response.registers

            # # simulation
            # result = []
            # faults = [1, 2, 4, 8, 16, 32, 64]
            # for idx in range(12):
            #     num = randint(1, 256)
            #     if num % 19 == 0:
            #         result.append(choices(faults)[0])
            #     else:
            #         result.append(0)

            return result
        except Exception as e:
            print("** RTU :: Error read_fault_from_device : ", e)
            return []
    else:
        try:
            message = tcp.read_input_registers(slave_id=1, 
                        starting_address=READ_VOLTAGE1, quantity=10)
            result = tcp.send_message(message, client)
            return result
        except Exception as e:
            print("## TCP :: Error read_input_registers : ", e)
            return []

def standBy(client, ptype):
    print("standBy : POWER_ON :: ", 1, "\n\n")
    client.write_registers(POWER_ONOFF, 2, unit=UNIT)

def standOff(client, ptype):
    print("standOff : POWER_OFF :: ", 0, "\n\n")
    client.write_registers(POWER_ONOFF, 0, unit=UNIT)


# Write Register
def write_registers(client, address, val, ptype):
    print("WRITE : ", address, val, ptype, "\n\n")
    if ptype == 'RTU':
        try:
            value = c_uint16(val).value
            result = client.write_registers(address, value, unit=UNIT)
        except Exception as e:
            result = []
            print("Error write_registers : ", e)
    else:
        message = tcp.write_single_register(slave_id=1, 
                    address=address, value=val)
        result = tcp.send_message(message, client)

    return result


# Read FeedBack Data
def read_feedback(client, address):
    try:
        result = client.read_input_registers(address, 1, unit=UNIT)
        if result.function_code >= 0x80:
            print("Error Happened : ", )
        response = result.registers
    except Exception as e:
        response = []
        print("Error read_input_registers : ", e)
    return response

