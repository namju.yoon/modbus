#!/usr/bin/env python
# coding: utf-8

# 예제 내용
# * 기본 위젯을 사용하여 기본 창을 생성
# * 다양한 레이아웃 위젯 사용
import os
import sys
import time
import random
from random import randint
from PyQt5.QtWidgets import QWidget, QLabel, QLineEdit, QPushButton, QDesktopWidget, QMainWindow
from PyQt5.QtWidgets import QGroupBox, QBoxLayout, QVBoxLayout, QHBoxLayout, QGridLayout, QSpinBox
from PyQt5.QtWidgets import QApplication, QDialog, QStatusBar, QLineEdit, QCheckBox, QSizePolicy
from PyQt5.QtWidgets import QRadioButton
from PyQt5 import QtCore
from PyQt5.QtCore import QDate, Qt
from PyQt5.QtGui import QPixmap
from itertools import count
from setup_esc import SettingWin
from zoom import GraphWindow, ZoomWindow, ZoomWindow6
from save_esc import DataWin
import pyqtgraph as pyGraph
import pyqtgraph.exporters
import escjig_communication as device
import numpy as np
import pandas as pd
import serial
import json, codecs
from ctypes import c_uint16
from threading import Timer
from scipy.optimize import curve_fit

# import logging
# FORMAT = ('%(asctime)-15s %(threadName)-15s '
#           '%(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
# logging.basicConfig(format=FORMAT)
# log = logging.getLogger()
# log.setLevel(logging.DEBUG)

# PLUSE_DIFF = 10 
# MINUS_DIFF = -10

PLUS_OFFSET = 1
MINUS_OFFSET = -1

PLUS_OFFSET4 = 4
MINUS_OFFSET4 = -4

PLUS = 7
MINUS = -7

UP = 8
DOWN = -8

NORMAL = 2
UNNORMAL = 0
REPEATN = 2

# 시간 변경
READ_TIME = 2000
CAPON_TIME = 1000
TIME_INTERVAL = 2

ACOEFF = 0
BPOW = 1

def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)

def fit_func(x, a, b):
    return a*pow(x, b)

# repeat function control timer of threading

class MyLineEdit(QLineEdit):
    def __init__(self, *args, **kwargs):
        super(MyLineEdit, self).__init__(*args, **kwargs)
        self.setAlignment(Qt.AlignRight)
        self.setFixedHeight(20)
        self.setMaximumHeight(20)

class GLineEdit(MyLineEdit):
    def __init__(self, *args, **kwargs):
        super(GLineEdit, self).__init__(*args, **kwargs)
        self.setAlignment(Qt.AlignRight)
        self.setStyleSheet("background-color: #cdcdcd;")

class MyLabel(QLabel):
    def __init__(self, *args, **kwargs):
        super(MyLabel, self).__init__(*args, **kwargs)
        self.setFixedHeight(20)
        self.setMaximumHeight(20)

class GLabel(MyLabel):
    def __init__(self, *args, **kwargs):
        super(GLabel, self).__init__(*args, **kwargs)
        self.setAlignment(Qt.AlignRight)

class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()

        self.com_open_flag = False
        self.esc_client = None
        self.jig_client = None
        self.stopFlag = True

        self.timer = QtCore.QTimer()

        self.jig_fwd = 0
        self.jig_rev = 0

        self.set_offset = 0
        self.prev_gain = 0

        self.cal_index = 0
        self.gain_dir = None

        self.cal_params = BPOW
        self.old_acoeff = 0
        self.old_bpow = 0

        self.initUI()

    def displayMenuLayout(self, parent_layout, glabel, btn, second=None):
        grp_sub = QGroupBox(glabel)
        layout_sub = QHBoxLayout()
        grp_sub.setLayout(layout_sub)
        layout_sub.addWidget(btn)
        if second:
            layout_sub.addWidget(second)
        parent_layout.addWidget(grp_sub)

    def displayVerLayout(self, parent_layout, glabel, btn, second=None):
        grp_sub = QGroupBox(glabel)
        layout_sub = QVBoxLayout()
        grp_sub.setLayout(layout_sub)
        layout_sub.addWidget(btn)
        if second:
            layout_sub.addWidget(second)
        parent_layout.addWidget(grp_sub)

    def LayoutCalGrid(self, layout_grid, nth, btn_item1, btn_offset1,
                btn_gain1, btn_tar1, btn_unit1,
                btn_item2, btn_offset2,
                btn_gain2, btn_tar2, btn_unit2):
    ## 
        layout_grid.addWidget(btn_item1, nth, 0)
        layout_grid.addWidget(btn_offset1, nth, 1)
        layout_grid.addWidget(btn_gain1, nth, 2)
        layout_grid.addWidget(btn_tar1, nth, 3)
        layout_grid.addWidget(btn_unit1, nth, 4)

        layout_grid.addWidget(btn_item2, nth, 5)
        layout_grid.addWidget(btn_offset2, nth, 6)
        layout_grid.addWidget(btn_gain2, nth, 7)
        layout_grid.addWidget(btn_tar2, nth, 8)
        layout_grid.addWidget(btn_unit2, nth, 9)


    def LayoutValueGrid(self, grid_box, nth, label1, edit1, range1, unit1, 
                                        label2, edit2, range2, unit2):
    ##
        grid_box.addWidget(MyLabel(label1), nth, 0)
        grid_box.addWidget(edit1, nth, 1)
        grid_box.addWidget(MyLabel(range1), nth, 2)
        grid_box.addWidget(MyLabel(unit1), nth, 3)
        grid_box.addWidget(MyLabel(label2), nth, 4)
        grid_box.addWidget(edit2, nth, 5)
        grid_box.addWidget(MyLabel(range2), nth, 6)
        grid_box.addWidget(MyLabel(unit2), nth, 7)

    def LayoutThreeGrid(self, layout_esc, nth, label, value, unit):
        layout_esc.addWidget(MyLabel(label), nth, 0)
        layout_esc.addWidget(value, nth, 1)
        layout_esc.addWidget(unit, nth, 2)

    def LayoutRadioBoxGrid(self, layout_grid, row, col, label, 
                                btn1, btn2, comment):
    ##
        grou_sub = QGroupBox("")
        layout_sub = QHBoxLayout()
        grou_sub.setLayout(layout_sub)
        layout_sub.addWidget(MyLabel(label))
        layout_sub.addWidget(btn1)
        layout_sub.addWidget(btn2)
        layout_sub.addWidget(MyLabel(comment))
        layout_grid.addWidget(grou_sub, row, col)

    def initUI(self):
        self.setWindowTitle("PSTEK ELECTROSTATIC CHUCK INSPECTION AUTOMATION")
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width(), screensize.height()) 
 
        mainwidget = QWidget()                # 위젯의 인스턴스 생성만으로도 QMainWindow에 붙는다.
        self.setCentralWidget(mainwidget)

    ## Main Layout 설정
        main_layer = QVBoxLayout()
        mainwidget.setLayout(main_layer)

        bluefont = "font-weight: bold; color: #0000ff;"
        redfont = "font-weight: bold; color: #ff0000;"
        greenfont = "font-weight: bold; color: #00ff00;"
        btnback = "font-weight: bold; background-color: #cdcdcd;"
        boldfont = "font-weight: bold"
        escback = "font-weight: bold; background-color: #ffcccc"
        jigback = "font-weight: bold; background-color: #ccffeb"
        unitback = "font-weight: bold; background-color: #e0ebeb"

        layout_setup = QHBoxLayout()
        
    ## replace #1st ###############
        # 전원 장치 연결
        self.btn_esc_gen = QPushButton("ESC 전원장치 연결")
        self.btn_esc_gen.clicked.connect(self.connect_esc_gen)
        self.label_esc_gen = QLabel("Not Connected")
        self.displayMenuLayout(layout_setup, "Connect Device(485/ModBus)", 
                self.btn_esc_gen, self.label_esc_gen)

        # JIG 연결
        # self.btn_esc_gen.setStyleSheet("color: #ff0000;")
        self.btn_jig = QPushButton("JIG 연결")
        self.btn_jig.clicked.connect(self.connect_jig_gen)
        self.label_jig = QLabel("Not Connected")
        self.displayMenuLayout(layout_setup, "Connect JIG", 
                self.btn_jig, self.label_jig)


        # 단말기 Calibration
        self.btn_calib = QPushButton("Calibration 시작")
        self.btn_calib.setStyleSheet(bluefont)
        self.btn_calib.clicked.connect(self.esc_calibration)
        # ZCS ACOF BPOW START
        self.btn_zcs3_calib = QPushButton("ZCS ACOF BPOW START")
        self.btn_zcs3_calib.setStyleSheet(bluefont)
        self.btn_zcs3_calib.clicked.connect(self.start_new_pow_coeff)
        self.displayVerLayout(layout_setup, "Calibration", 
                self.btn_calib, self.btn_zcs3_calib)
    

        # 단말기 STOP
        self.btn_stop = QPushButton("ESC / JIG Stop")
        self.btn_stop.setStyleSheet(redfont)
        self.btn_stop.clicked.connect(self.device_stop)
        self.displayMenuLayout(layout_setup, "Stop", 
                self.btn_stop)

        # ESC RESET 
        self.btn_reset = QPushButton("ESC Reset")
        self.btn_reset.clicked.connect(self.device_reset)
        self.displayMenuLayout(layout_setup, "Reset", 
                self.btn_reset)

        # Logo Image
        labelLogo = QLabel("")
        pixmap = QPixmap(resource_path("logo.png"))
        labelLogo.setPixmap(pixmap)
        labelLogo.setAlignment(Qt.AlignRight)
        layout_setup.addWidget(labelLogo)        
        main_layer.addLayout(layout_setup)


    ## 2nd ########################
        # 그래프 범례
        self.greenback = "font-weight: bold; background-color: #00ff00"
        grp_legend = QGroupBox("Calibration 완료")
        layout_legend = QGridLayout()

        # button
        btn_calib_finish = QPushButton("CALIBRATION 완료")
        btn_calib_finish.setStyleSheet(bluefont)
        layout_legend.addWidget(btn_calib_finish, 0, 0, 2, 1)

        # voltage 
        self.btn_voltage1_offset = QPushButton("VO 1 OFFSET")
        self.btn_voltage1_gain = QPushButton("VO 1 GAIN")
        self.btn_voltage2_offset = QPushButton("VO 2 OFFSET")
        self.btn_voltage2_gain = QPushButton("VO 2 GAIN")
        layout_legend.addWidget(self.btn_voltage1_offset, 0, 1)
        layout_legend.addWidget(self.btn_voltage1_gain, 1, 1)
        layout_legend.addWidget(self.btn_voltage2_offset, 0, 2)
        layout_legend.addWidget(self.btn_voltage2_gain, 1, 2)

        # current 
        self.btn_current1_offset = QPushButton("IO 1 OFFSET")
        self.btn_current1_gain = QPushButton("IO 1 GAIN")
        self.btn_current2_offset = QPushButton("IO 2 OFFSET")
        self.btn_current2_gain = QPushButton("IO 2 GAIN")
        layout_legend.addWidget(self.btn_current1_offset, 0, 3)
        layout_legend.addWidget(self.btn_current1_gain, 1, 3)
        layout_legend.addWidget(self.btn_current2_offset, 0, 4)
        layout_legend.addWidget(self.btn_current2_gain, 1, 4)

        # IR 
        self.btn_irleak1_offset = QPushButton("IR 1 OFFSET")
        self.btn_irleak1_gain = QPushButton("IR 1 GAIN")
        self.btn_irleak2_offset = QPushButton("IR 2 OFFSET")
        self.btn_irleak2_gain = QPushButton("IR 2 GAIN")

        layout_legend.addWidget(self.btn_irleak1_offset, 0, 5)
        layout_legend.addWidget(self.btn_irleak1_gain, 1, 5)
        layout_legend.addWidget(self.btn_irleak2_offset, 0, 6)
        layout_legend.addWidget(self.btn_irleak2_gain, 1, 6)

        # CAP
        self.btn_vbia_offset = QPushButton("VBIA OFFSET")
        self.btn_vbia_gain = QPushButton("VBIA GAIN")

        self.btn_cap_offset = QPushButton("CAP OFFSET")
        self.btn_zcs1 = QPushButton("ZCS1 GAIN")

        self.btn_zcs2 = QPushButton("ZCS2 GAIN")
        self.btn_zcs3 = QPushButton("ZCS3 GAIN")

        layout_legend.addWidget(self.btn_vbia_offset, 0, 7)
        layout_legend.addWidget(self.btn_vbia_gain, 1, 7)

        layout_legend.addWidget(self.btn_cap_offset, 0, 8)
        layout_legend.addWidget(self.btn_zcs1, 1, 8)

        layout_legend.addWidget(self.btn_zcs2, 0, 9)
        layout_legend.addWidget(self.btn_zcs3, 1, 9)

        grp_legend.setLayout(layout_legend)
        main_layer.addWidget(grp_legend)
        # main_layer.addLayout(layout_legend)

    ## 3rd ########################
        ## Calibration Data Display
        grp_datadisplay = QGroupBox("Calibration Data Display")
        layout_datadisplay = QGridLayout()

        btn_item1 = QPushButton("구분")
        btn_offset1 = QPushButton("OFFSET")
        btn_gain1 = QPushButton("GAIN")
        btn_tar1 = QPushButton("TARGET")
        btn_offset1.setStyleSheet(jigback)
        btn_gain1.setStyleSheet(jigback)
        btn_tar1.setStyleSheet(jigback)
        btn_unit1 = QPushButton("단위")
        btn_unit1.setStyleSheet(unitback)

        btn_item2 = QPushButton("구분")
        btn_offset2 = QPushButton("OFFSET")
        btn_gain2 = QPushButton("GAIN")
        btn_tar2 = QPushButton("TARGET")
        btn_offset2.setStyleSheet(jigback)
        btn_gain2.setStyleSheet(jigback)
        btn_tar2.setStyleSheet(jigback)
        btn_unit2 = QPushButton("단위")
        btn_unit2.setStyleSheet(unitback)

        self.LayoutCalGrid(layout_datadisplay, 0,
            btn_item1, btn_offset1, btn_gain1, btn_tar1, btn_unit1,
            btn_item2, btn_offset2, btn_gain2, btn_tar2, btn_unit2)

        # Voltage 
        btn_vol1 = QLabel("OUT VOLTAGE 1")
        self.edit_vol_offset1 = MyLineEdit("0")
        self.edit_vol_gain1 = MyLineEdit("0")
        self.edit_vol_tar1 = MyLineEdit("")
        self.unit_vol1 = QLabel("V")

        btn_vol2 = QLabel("OUT VOLTAGE 2")
        self.edit_vol_offset2 = MyLineEdit("0")
        self.edit_vol_gain2 = MyLineEdit("0")
        self.edit_vol_tar2 = MyLineEdit("")
        self.unit_vol2 = QLabel("V")

        self.LayoutCalGrid(layout_datadisplay, 1,
            btn_vol1, self.edit_vol_offset1, self.edit_vol_gain1, self.edit_vol_tar1, self.unit_vol1,
            btn_vol2, self.edit_vol_offset2, self.edit_vol_gain2, self.edit_vol_tar2, self.unit_vol2)

        # Current 
        btn_cur1 = QLabel("OUT CURRENT 1")
        self.edit_cur_offset1 = MyLineEdit("0")
        self.edit_cur_gain1 = MyLineEdit("0")
        self.edit_cur_tar1 = MyLineEdit("")
        self.unit_cur1 = QLabel("mA")

        btn_cur2 = QLabel("OUT CURRENT 2")
        self.edit_cur_offset2 = MyLineEdit("0")
        self.edit_cur_gain2 = MyLineEdit("0")
        self.edit_cur_tar2 = MyLineEdit("")
        self.unit_cur2 = QLabel("mA")

        self.LayoutCalGrid(layout_datadisplay, 2,
            btn_cur1, self.edit_cur_offset1, self.edit_cur_gain1, self.edit_cur_tar1, self.unit_cur1,
            btn_cur2, self.edit_cur_offset2, self.edit_cur_gain2, self.edit_cur_tar2, self.unit_cur2)

        # IR 
        btn_irleak1 = QLabel("IO LEAK 1")
        self.edit_irleak_offset1 = MyLineEdit("0")
        self.edit_irleak_gain1 = MyLineEdit("0")
        self.edit_irleak_tar1 = MyLineEdit("")
        self.unit_irleak1 = QLabel("uA")

        btn_irleak2 = QLabel("IO LEAK 2")
        self.edit_irleak_offset2 = MyLineEdit("0")
        self.edit_irleak_gain2 = MyLineEdit("0")
        self.edit_irleak_tar2 = MyLineEdit("")
        self.unit_irleak2 = QLabel("uA")

        self.LayoutCalGrid(layout_datadisplay, 3,
            btn_irleak1, self.edit_irleak_offset1, self.edit_irleak_gain1, self.edit_irleak_tar1, self.unit_irleak1,
            btn_irleak2, self.edit_irleak_offset2, self.edit_irleak_gain2, self.edit_irleak_tar2, self.unit_irleak2)

        # VBIAS 
        btn_vbia = QLabel("VBIAS")
        self.edit_vbia_offset = MyLineEdit("0")
        self.edit_vbia_gain = MyLineEdit("0")
        self.edit_vbia_tar = MyLineEdit("")
        self.unit_vbia = QLabel("pF")

        # CAP 
        btn_cap = QLabel("CAP OFFSET")
        self.edit_cap_offset = MyLineEdit("0")
        self.edit_cap_gain = MyLineEdit("")
        self.edit_cap_tar = MyLineEdit("")
        self.unit_cap = QLabel("pF")

        self.LayoutCalGrid(layout_datadisplay, 4,
            btn_vbia, self.edit_vbia_offset, self.edit_vbia_gain, self.edit_vbia_tar, self.unit_vbia,
            btn_cap, self.edit_cap_offset, self.edit_cap_gain, self.edit_cap_tar, self.unit_cap)


        btn_zcsacoeff = QLabel("ZCS A COE")
        self.edit_zcsacoeff = MyLineEdit("0")
        self.unit_zcsacoeff = QLabel("pF")

        # CAP 2
        btn_zcsbpow = QLabel("ZCS B POW")
        btn_zcsbpow.setStyleSheet(btnback)
        self.edit_zcsbpow = MyLineEdit("0")
        self.unit_zcs2 = QLabel("pF")

        layout_datadisplay.addWidget(btn_zcsacoeff, 5, 0)
        layout_datadisplay.addWidget(self.edit_zcsacoeff, 5, 1, 1, 3)
        layout_datadisplay.addWidget(self.unit_zcsacoeff, 5, 4)

        layout_datadisplay.addWidget(btn_zcsbpow, 5, 5)
        layout_datadisplay.addWidget(self.edit_zcsbpow, 5, 6, 1, 3)
        layout_datadisplay.addWidget(self.unit_zcs2, 5, 9)

        grp_datadisplay.setLayout(layout_datadisplay)
        main_layer.addWidget(grp_datadisplay)

    ## 4th ########################
        ## ZCS Calibration
        grp_zcscalib = QGroupBox("ZCS Calibration")
        layout_zcscalib = QGridLayout()

        label_cap0 = QLabel("CAP0 :: 108")
        self.edit_val_cap0 = GLineEdit("")
        self.edit_rate0 = GLineEdit("")

        label_cap1 = QLabel("CAP1 :: 255")
        self.edit_val_cap1 = GLineEdit("")
        self.edit_rate1 = GLineEdit("")

        label_cap2 = QLabel("CAP2 :: 516")
        self.edit_val_cap2 = GLineEdit("")
        self.edit_rate2 = GLineEdit("")

        label_cap3 = QLabel("CAP3 :: 763")
        self.edit_val_cap3 = GLineEdit("")
        self.edit_rate3 = GLineEdit("")

        label_cap4 = QLabel("CAP4 :: 1078")
        self.edit_val_cap4 = GLineEdit("")
        self.edit_rate4 = GLineEdit("")

        label_cap5 = QLabel("CAP5 :: 1497")
        self.edit_val_cap5 = GLineEdit("")
        self.edit_rate5 = GLineEdit("")

        label_cap6 = QLabel("CAP6 :: 2760")
        self.edit_val_cap6 = GLineEdit("")
        self.edit_rate6 = GLineEdit("")

        label_cap7 = QLabel("CAP7 :: 4918")
        self.edit_val_cap7 = GLineEdit("")
        self.edit_rate7 = GLineEdit("")

        label_cap8 = QLabel("CAP8 :: 7451")
        self.edit_val_cap8 = GLineEdit("")
        self.edit_rate8 = GLineEdit("")

        label_cap9 = QLabel("CAP9 :: 10051")
        self.edit_val_cap9 = GLineEdit("")
        self.edit_rate9 = GLineEdit("")

        layout_zcscalib.addWidget(label_cap0, 0, 0)
        layout_zcscalib.addWidget(self.edit_val_cap0, 0, 1)
        layout_zcscalib.addWidget(self.edit_rate0, 0, 2)

        layout_zcscalib.addWidget(label_cap1, 0, 3)
        layout_zcscalib.addWidget(self.edit_val_cap1, 0, 4)
        layout_zcscalib.addWidget(self.edit_rate1, 0, 5)

        layout_zcscalib.addWidget(label_cap2, 0, 6)
        layout_zcscalib.addWidget(self.edit_val_cap2, 0, 7)
        layout_zcscalib.addWidget(self.edit_rate2, 0, 8)

        layout_zcscalib.addWidget(label_cap3, 0, 9)
        layout_zcscalib.addWidget(self.edit_val_cap3, 0, 10)
        layout_zcscalib.addWidget(self.edit_rate3, 0, 11)

        layout_zcscalib.addWidget(label_cap4, 0, 12)
        layout_zcscalib.addWidget(self.edit_val_cap4, 0, 13)
        layout_zcscalib.addWidget(self.edit_rate4, 0, 14)

        layout_zcscalib.addWidget(label_cap5, 1, 0)
        layout_zcscalib.addWidget(self.edit_val_cap5, 1, 1)
        layout_zcscalib.addWidget(self.edit_rate5, 1, 2)

        layout_zcscalib.addWidget(label_cap6, 1, 3)
        layout_zcscalib.addWidget(self.edit_val_cap6, 1, 4)
        layout_zcscalib.addWidget(self.edit_rate6, 1, 5)

        layout_zcscalib.addWidget(label_cap7, 1, 6)
        layout_zcscalib.addWidget(self.edit_val_cap7, 1, 7)
        layout_zcscalib.addWidget(self.edit_rate7, 1, 8)

        layout_zcscalib.addWidget(label_cap8, 1, 9)
        layout_zcscalib.addWidget(self.edit_val_cap8, 1, 10)
        layout_zcscalib.addWidget(self.edit_rate8, 1, 11)

        layout_zcscalib.addWidget(label_cap9, 1, 12)
        layout_zcscalib.addWidget(self.edit_val_cap9, 1, 13)
        layout_zcscalib.addWidget(self.edit_rate9, 1, 14)
        
        grp_zcscalib.setLayout(layout_zcscalib)
        main_layer.addWidget(grp_zcscalib)

    ## 5 th ########################
    ## 5-1 ########################
        ## ESC 전원장치 설정값
        grp_escvalue = QGroupBox("ESC 전원장치 설정값")
        
        # 전체 Group 내 배치 
        layout_group = QGridLayout()
        grp_escvalue.setLayout(layout_group)

        group_setting = QGroupBox("")
        grid_box = QGridLayout()
        
        # Control Commands 
        # First Region
        self.voltage1 = MyLineEdit("0")
        self.current1 = MyLineEdit("0")
        self.LayoutValueGrid(grid_box, 0,
            "OUT VOLTAGE1: ", self.voltage1, "-2500 ~ 2500", "V",
            "OUT CURRENT1: ", self.current1, "0 ~ 10.00", "mA")
        
        self.voltage2 = MyLineEdit("0")
        self.current2 = MyLineEdit("0")
        self.LayoutValueGrid(grid_box, 1,
            "OUT VOLTAGE2: ", self.voltage2, "-2500 ~ 2500", "V",
            "OUT CURRENT2: ", self.current2, "0 ~ 10.00", "mA")

        self.leak_fault_level = MyLineEdit("0")
        self.ro_min_fault = MyLineEdit("0")
        self.LayoutValueGrid(grid_box, 2,
            "LEAK FAULT LEVEL: ", self.leak_fault_level, "0 ~ 1.00", "mA",
            "RO MIN FALUT: ", self.ro_min_fault, "0 ~ 999", "Kohm")


        self.up_time = MyLineEdit("0")
        self.down_time = MyLineEdit("0")
        self.LayoutValueGrid(grid_box, 3,
            "RAMP UP TIME: ", self.up_time, "0.3 ~ 9.9 ", "sec",
            "RAMP DOWN TIME: ", self.down_time, "0.3 ~ 9.9 ", "sec")


        self.slope = MyLineEdit("0")
        self.toggle_count = MyLineEdit("0")
        self.LayoutValueGrid(grid_box, 4,
            "SLOPE: ", self.slope, "10 ~ 100", "",
            "AUTO TOGGLE COUNT: ", self.toggle_count, "1 ~ 99", "")


        self.arc_delay = MyLineEdit("0")
        self.coeff = MyLineEdit("0")
        self.LayoutValueGrid(grid_box, 5,
            "ARC DELAY: ", self.arc_delay, "10.00 ~ 50.00 ", "m sec",
            "COEFF: ", self.coeff, "1 ~ 10", "")


        self.arc_rate = MyLineEdit("0")
        self.local_address = MyLineEdit("0")
        self.LayoutValueGrid(grid_box, 6,
            "ARC RATE: ", self.arc_rate, "1 ~ 1000 ", "a/s",
            "LOCAL ADDRESS: ", self.local_address, "1 ~ 31", "")

        self.target_cap = MyLineEdit("0")
        self.cap_deviation = MyLineEdit("0")
        self.LayoutValueGrid(grid_box, 7,
            "TARGET CAPACITOR: ", self.target_cap, "1 ~ 15000 ", "pF",
            "CAP DEVIATION: ", self.cap_deviation, "0 ~ 100", "%")

        self.time_delay = MyLineEdit("0")
        grid_box.addWidget(MyLabel("TIME DELAY"), 8, 0)
        grid_box.addWidget(self.time_delay, 8, 1)
        grid_box.addWidget(MyLabel("0 ~ 100"), 8, 2)
        grid_box.addWidget(MyLabel("Sec"), 8, 3)

        group_setting.setLayout(grid_box)
        layout_group.addWidget(group_setting, 0, 0, 1, 2)

    ## 5-2 ########################
        ## ESC 전원장치 설정값
        grp_esc = QGroupBox("")
        layout_esc = QGridLayout()

        # esc value
        btn_item3 = QPushButton("구분")
        btn_esc_dis1 = QPushButton("ESC 전원 장치 출력 값")
        btn_esc_dis1.setStyleSheet(escback)
        lbl_unit = QPushButton("단위")
        lbl_unit.setStyleSheet(unitback)

        self.LayoutThreeGrid(layout_esc, 0, btn_item3, 
                btn_esc_dis1, lbl_unit)

        # voltage 1
        self.lbl_voltage1 = MyLineEdit("0")
        unit_voltage1 = MyLabel("V")
        self.LayoutThreeGrid(layout_esc, 1, "OUT VOLTAGE 1", 
                self.lbl_voltage1, unit_voltage1)

        # current 1
        self.lbl_current1 = MyLineEdit("0")
        unit_current1 = MyLabel("mA")
        self.LayoutThreeGrid(layout_esc, 2, "OUT CURRENT 1", 
                self.lbl_current1, unit_current1)

        # voltage 2
        self.lbl_voltage2 = MyLineEdit("0")
        unit_voltage2 = MyLabel("V")
        self.LayoutThreeGrid(layout_esc, 3, "OUT VOLTAGE 2", 
                self.lbl_voltage2, unit_voltage2)

        # current 2
        self.lbl_current2 = MyLineEdit("0")
        unit_current2 = MyLabel("mA")
        self.LayoutThreeGrid(layout_esc, 4, "OUT CURRENT 2", 
                self.lbl_current2, unit_current2)

        # VBIAS
        self.lbl_vbias = MyLineEdit("0")
        unit_vbias = MyLabel("V")
        self.LayoutThreeGrid(layout_esc, 5, "VBIAS", 
                self.lbl_vbias, unit_vbias)

        # VCS
        self.lbl_vcs = MyLineEdit("0")
        unit_vcs = MyLabel("V")
        self.LayoutThreeGrid(layout_esc, 6, "VCS", 
                self.lbl_vcs, unit_vcs)

        # ICS
        self.lbl_ics = MyLineEdit("0")
        unit_ics = MyLabel("V")
        self.LayoutThreeGrid(layout_esc, 7, "ICS", 
                self.lbl_ics, unit_ics)

        # CP
        self.lbl_cp = MyLineEdit("0")
        unit_cp = MyLabel("pF")
        self.LayoutThreeGrid(layout_esc, 8, "CAP", 
                self.lbl_cp, unit_cp)

        # IO LEAK 1
        self.lbl_leak1 = MyLineEdit("0")
        unit_leak1 = MyLabel("uA")
        self.LayoutThreeGrid(layout_esc, 9, "IO LEAK 1", 
                self.lbl_leak1, unit_leak1)


        # IO LEAK 2
        self.lbl_leak2 = MyLineEdit("0")
        unit_leak2 = MyLabel("uA")
        self.LayoutThreeGrid(layout_esc, 10, "IO LEAK 2", 
                self.lbl_leak2, unit_leak2)
    
        # grp_esc
        grp_esc.setLayout(layout_esc)
        layout_group.addWidget(grp_esc, 0, 2)

    ## 5-3
    # # Radio Button 
        self.rd_mode_off = QRadioButton("OFF")
        self.rd_mode_off.setChecked(True)
        self.rd_mode_on = QRadioButton("ON")
        self.LayoutRadioBoxGrid(layout_group, 1, 0, "AUTO TOGGLE MODE:",
                self.rd_mode_off, self.rd_mode_on, "(O:OFF, 1:ON)")


        self.rd_select_internal = QRadioButton("INTERNAL")
        self.rd_select_internal.setChecked(True)
        self.rd_select_remote = QRadioButton("REMOTE")
        self.LayoutRadioBoxGrid(layout_group, 1, 1, "ON/OFF SELECTION:",
                self.rd_select_internal, self.rd_select_remote, "(O:INTERNAL, 1:REMOTE)")

        
        # Third Region
        self.rd_toggle_off = QRadioButton("FORWARD")
        self.rd_toggle_off.setChecked(True)
        self.rd_toggle_on = QRadioButton("REVERSE")
        self.LayoutRadioBoxGrid(layout_group, 1, 2, "TOGGLE:",
                self.rd_toggle_off, self.rd_toggle_on, "(0:FORWARD, 1:REVERSE)")


        self.rd_arc_off = QRadioButton("OFF")
        self.rd_arc_off.setChecked(True)
        self.rd_arc_on = QRadioButton("ON")
        self.LayoutRadioBoxGrid(layout_group, 2, 0, "ARC CONTROL:",
                self.rd_arc_off, self.rd_arc_on, "(O:OFF, 1:ON)")

        
        self.rd_ocp_off = QRadioButton("OFF")
        self.rd_ocp_off.setChecked(True)
        self.rd_ocp_on = QRadioButton("ON")
        self.LayoutRadioBoxGrid(layout_group, 2, 1, "OCP CONTROL:",
                self.rd_ocp_off, self.rd_ocp_on, "(O:OFF, 1:ON)")

        
        grp_escvalue.setLayout(layout_group)

        main_layer.addWidget(grp_escvalue)

    ## 6th ########################
        # Status
        self.statusbar = QStatusBar()
        self.setStatusBar(self.statusbar)
        self.statusbar.setObjectName("statusbar")
        
        self.statusmessage = 'PSTEK, {},  {}'
        now = QDate.currentDate()
        displaymessage = self.statusmessage.format(now.toString(Qt.ISODate), 'Ready !!')
        self.statusbar.showMessage(displaymessage)

    ## TEMP
        self.temp_setup_connection()

    # NORMAL
    def offset_updown(self):
        if abs(self.jig_fwd) > abs(self.jig_rev):
            return UP
        else:
            return DOWN

    # esc_calibration
    def esc_calibration(self):
        device.check_password(self.esc_client)

        ## ONONON
        # PC에서 JIG에 CAP_ON10, CAP_ON11, CAP_ON14을 ON 설정하고, RUN 지령을 한다.
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON10, device.ON)
        time.sleep(TIME_INTERVAL)

        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON11, device.ON)
        time.sleep(TIME_INTERVAL)

        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON14, device.ON)
        time.sleep(TIME_INTERVAL)

        # Voltage 
        # PC에서 ESC에 VO1 = 2500V, VO2 = -2500V로 설정하고, RUN 지령
        device.write_registers(self.esc_client, device.ESC_WRITE_VOLTAGE1, 2500)
        value = c_uint16(-2500).value
        device.write_registers(self.esc_client, device.ESC_WRITE_VOLTAGE2, value)
        # Display 2500
        self.lbl_voltage1.setText("2500")
        self.lbl_voltage2.setText("-2500")
        
        # DEVICE RUN
        self.esc_device_run()
        
        # PC에서 ESC에 FORWARD와 REVERSE 지령을 반복하면서 JIG의 VO1 값을 확인한다.
        # JIG의 VO1 FORWARD와 REVERSE 값이 같아지도록 ESC에 VO1 OFFSET을 조정한다.

        # CAll Function for Voltage 1 
        # client            : self.esc_client
        # value_read_address   : device.JIG_READ_VOLTAGE1
        # offset_address    : device.ESC_DEV_VO1_OFFSET
        # dis_value          : self.edit_vol_gain1
        # edit_rev          : self.edit_vol_tar1
        # btn               : self.btn_voltage1_offset
        # ptype             : VOLTAGE1OFFSET
        
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(lambda:self.start_offset_calib(self.jig_client, 
                    device.JIG_READ_VOLTAGE1, device.ESC_DEV_VO1_OFFSET, 
                    self.edit_vol_offset1, self.btn_voltage1_offset, "VOLTAGE1OFFSET"))
        self.timer.start(CAPON_TIME)
    

    # PC에서 ESC에 FORWARD와 REVERSE 지령을 반복하면서 JIG의 VO1 값을 확인한다.
    # JIG의 VO1 FORWARD와 REVERSE 값이 같아지도록 ESC에 VO1 OFFSET을 조정한다.

    # CAll Function for Voltage 1 
    # client            : JIG
    # value_read_address   : 읽어드릴 값의 주소
    # offset_address    : ESC의 값 조정 주소 
    # dis_value          : 해당 변화 값을 보여주는 Edit
    # btn               : 완료 후 색 변환
    # ptype             : 해당 type
    def start_offset_calib(self, client, value_read_address, offset_address, dis_value, btn, ptype):
        # Timer 정지
        self.timer.stop()
        self.timer = None

        print("\n#### ", ptype, " START ####\n")
        # 기존 OFFSET Value 를 읽어 온다.
        self.prev_gain = device.read_value(self.esc_client, offset_address)
        dis_value.setText(str(self.prev_gain))

        self.gain_dir = None
        self.cal_index = 0
        self.old_diff = 0
        self.before_dir = PLUS
        self.repeatP = 0
        self.repeatN = 0

        # FORWARD
        device.write_registers(self.esc_client, device.ESC_WRITE_TOGGLE, device.FORWARD)
    
        # setting timer
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(lambda:self.step1_read_first(client, value_read_address, 
                    offset_address, dis_value, btn, ptype))
        self.timer.start(READ_TIME)


    ## FORWARD 값을 읽고, REVERSE 로 TOGGLE함
    def step1_read_first(self, client, value_read_address, offset_address, dis_value, btn, ptype):
        # Timer 정지
        self.timer.stop()
        self.timer = None

        # READ_TIME 2초 후에 ESC 똔 JIG의 VO1 값을 읽어와 해당 값을 저장함 
        self.jig_fwd = device.read_value(client, value_read_address)

        # ESC에 REVERSE 로 출력 명령을 내림
        device.write_registers(self.esc_client, device.ESC_WRITE_TOGGLE, device.REVERSE)

        # setting timer
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(lambda:self.step2_read_second(client, value_read_address, 
                    offset_address, dis_value, btn, ptype))
        self.timer.start(READ_TIME)


    ## REVERSE 값을 읽고, 값을 비교하면서 값을 조정함
    def step2_read_second(self, client, value_read_address, offset_address, dis_value, btn, ptype):
        # Timer 정지
        self.timer.stop()
        self.timer = None 

        # 1초 후에 JIG의 VO1 값을 읽어와 해당 값을 저장함 : JIG_REV_VO1 = 2450
        self.jig_rev = device.read_value(client, value_read_address)

        # JIG_FWD_VO1 과 JIG_REV_VO1 값의 차이를 구함 : DIFF_VO1 = JIG_REV_VO1 - JIG_FWD_VO1
        diff_vo1 = abs(self.jig_fwd) - abs(self.jig_rev)
        print(ptype, " ::: FWD V :     ", self.jig_fwd, "REV V :      ",  
                    self.jig_rev, " ****************> ", diff_vo1)

        # OFFSET 조정 완료 
        if MINUS_OFFSET <= diff_vo1 and diff_vo1 <= PLUS_OFFSET:
            print("&&&&& finish value : ", diff_vo1)
            btn.setStyleSheet(self.greenback)
            dis_value.setStyleSheet(self.greenback)
            self.get_esc_settings_value()
            self.get_esc_params()

            if ptype == 'VOLTAGE1OFFSET':
                # ESC의 VO1 FORWARD값이 JIG의 VO1 값과 같아지도록 ESC에 VO1 GAIN을 조정한다.
                # ESC의 VO1 FORWARD값이 JIG의 VO1 값과 편차 1V 이내가 되면 VO1 GAIN은 완료.
                # CAll Function for Voltage 1 GAIN
                # client            : self.esc_client
                # esc_vol_address    : device.ESC_READ_VOLTAGE1
                # jig_vol_address    : device.JIG_READ_VOLTAGE1
                # gain_address      : device.ESC_DEV_VO1_GAIN
                # dis_value          : 해당 변화 값을 보여주는 Edit
                # edit_rev          : 해당 변화 값을 보여주는 Edit
                # btn               : 완료 후 색 변환
                # ptype             : 해당 type
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(lambda:self.start_gain_calib(self.esc_client, 
                        device.ESC_READ_VOLTAGE1, device.JIG_READ_VOLTAGE1, device.ESC_DEV_VO1_GAIN,
                        self.edit_vol_gain1, self.btn_voltage1_gain, "VOLTAGE1GAIN"))
                self.timer.start(READ_TIME)

            elif ptype == 'VOLTAGE2OFFSET':
                # setting timer
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(lambda:self.start_gain_calib(self.esc_client, 
                        device.ESC_READ_VOLTAGE2, device.JIG_READ_VOLTAGE2, device.ESC_DEV_VO2_GAIN,
                        self.edit_vol_gain2, self.btn_voltage2_gain, "VOLTAGE2GAIN"))
                self.timer.start(READ_TIME)

            elif ptype == 'CURRENT1OFFSET':
                # ESC의 IO1 FORWARD값이 JIG의 IO1 값과 같아지도록 ESC에 IO1 GAIN을 조정한다.
                # ESC의 IO1 FORWARD값이 JIG의 IO1 값과 편차 0.001mA 이내가 되면 IO1 GAIN은 완료.
                # CURRENT1GAIN
                # client            : self.esc_client
                # esc_vol_address    : device.ESC_READ_VOLTAGE2
                # jig_vol_address    : device.JIG_READ_VOLTAGE2
                # gain_address      : device.ESC_DEV_VO2_GAIN
                # dis_value         : 해당 변화 값을 보여주는 Edit
                # edit_rev          : 해당 변화 값을 보여주는 Edit
                # btn               : 완료 후 색 변환
                # ptype             : 해당 type
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(lambda:self.start_gain_calib(self.esc_client, 
                        device.ESC_READ_CURRENT1, device.JIG_READ_CURRENT1, device.ESC_DEV_IO1_GAIN,
                        self.edit_cur_gain1, self.btn_current1_gain, "CURRENT1GAIN"))
                self.timer.start(READ_TIME)

            elif ptype == 'CURRENT2OFFSET':
                # CURRENT2GAIN
                # setting timer
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(lambda:self.start_gain_calib(self.esc_client, 
                        device.ESC_READ_CURRENT2, device.JIG_READ_CURRENT2, device.ESC_DEV_IO2_GAIN,
                        self.edit_cur_gain2, self.btn_current2_gain, "CURRENT2GAIN"))
                self.timer.start(READ_TIME)

            elif ptype == 'IRLEAK1OFFSET':

                # ESC의 IR1 FORWARD값이 JIG의 IR1 값과 같아지도록 ESC에 IR1 GAIN을 조정한다.
                # ESC의 IR1 FORWARD값이 JIG의 IR1 값과 편차 0.01uA 이내가 되면 IR1 GAIN은 완료.
                # CAll Function for Voltage 2 GAIN
                # client            : self.esc_client
                # esc_vol_address    : device.ESC_READ_VOLTAGE2
                # jig_vol_address    : device.JIG_READ_VOLTAGE2
                # gain_address      : device.ESC_DEV_VO2_GAIN
                # dis_value         : 해당 변화 값을 보여주는 Edit
                # btn               : 완료 후 색 변환
                # ptype             : 해당 type
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(lambda:self.start_gain_calib(self.esc_client, 
                        device.ESC_READ_IO_LEAK1, device.JIG_READ_IO_LEAK1, device.ESC_DEV_LEAK1_GAIN,
                        self.edit_irleak_gain1, self.btn_irleak1_gain, "IRLEAK1GAIN"))
                self.timer.start(READ_TIME)

            elif ptype == 'IRLEAK2OFFSET':
                # setting timer
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(lambda:self.start_gain_calib(self.esc_client, 
                        device.ESC_READ_IO_LEAK2, device.JIG_READ_IO_LEAK2, device.ESC_DEV_LEAK2_GAIN,
                        self.edit_irleak_gain2, self.btn_irleak2_gain, "IRLEAK2GAIN"))
                self.timer.start(READ_TIME)

        # OFFSET 조정 필요
        else:
            abs_diff_vo1 = abs(diff_vo1)
            self.calib_offset_gain(ptype, dis_value)

            # Offset 값 정리 
            self.cal_index += 1
            
            device.write_registers(self.esc_client, device.ESC_WRITE_TOGGLE, device.FORWARD)
            time.sleep(TIME_INTERVAL)
            # Offset 값 정리 

            div_value = c_uint16(self.prev_gain).value
            device.write_registers(self.esc_client, offset_address, div_value)
            # setting timer
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(lambda:self.step1_read_first(client, vo_read_address, 
                            offset_address, dis_value, btn, ptype))
            self.timer.start(READ_TIME)
    

    # PC에서 ESC에 FORWARD와 REVERSE 지령을 반복하면서 JIG의 VO1 값을 확인한다.
    # JIG의 VO1 FORWARD와 REVERSE 값이 같아지도록 ESC에 VO1 OFFSET을 조정한다.
    # client            : ESC or JIG
    # esc_vol_address    : ESC 읽어드릴 값의 주소
    # jig_vol_address    : JIG 읽어드릴 값의 주소
    # gain_address      : ESC의 값 조정 주소 
    # dis_value          : 해당 변화 값을 보여주는 Edit
    # edit_rev          : 해당 변화 값을 보여주는 Edit
    # btn               : 완료 후 색 변환
    # ptype             : 해당 type
    def start_gain_calib(self, client, esc_vol_address, jig_vol_address, gain_address, dis_value, btn, ptype):
        # Timer 정지
        self.timer.stop()
        self.timer = None 

        print("\n#### ", ptype, " START ####\n")
        # 기존 GAIN Value 를 읽어 온다.
        self.prev_gain = device.read_value(self.esc_client, gain_address)
        dis_value.setText(str(self.prev_gain))

        self.cal_index = 0
        self.old_diff = 0
        self.before_dir = PLUS
        self.repeatN = 0

        device.write_registers(self.esc_client, device.ESC_WRITE_TOGGLE, device.FORWARD)
    
        # setting timer
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(lambda:self.gain_read_esc_vo(client, esc_vol_address, 
                                    jig_vol_address, gain_address, dis_value, btn, ptype))
        self.timer.start(READ_TIME)

    def gain_read_esc_vo(self, client, esc_vol_address, jig_vol_address, gain_address, dis_value, btn, ptype):
        # Timer 정지
        self.timer.stop()
        self.timer = None 

        # 1초 후에 JIG의 VO1 값을 읽어와 해당 값을 저장함 : JIG_REV_VO1 = 2450
        self.jig_fwd = device.read_value(client, esc_vol_address)

        # setting timer
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(lambda:self.gain_read_jig_vo(self.jig_client, esc_vol_address, 
                                    jig_vol_address, gain_address, dis_value, btn, ptype))
        self.timer.start(READ_TIME)


    def gain_read_jig_vo(self, client, esc_vol_address, jig_vol_address, gain_address, dis_value, btn, ptype):
        # Timer 정지
        self.timer.stop()
        self.timer = None 

        # 1초 후에 JIG의 VO1 값을 읽어와 해당 값을 저장함 : JIG_REV_VO1 = 2450
        self.jig_rev = device.read_value(client, jig_vol_address)

        # JIG_FWD_VO1 과 JIG_REV_VO1 값의 차이를 구함 : DIFF_VO1 = JIG_REV_VO1 - JIG_FWD_VO1
        diff_vo1 = abs(self.jig_fwd) - abs(self.jig_rev)
        print(ptype, " ::: ESC V :     ", self.jig_fwd, "JIG V :      ",  self.jig_rev, " ************************>      ", diff_vo1)

        # GAIN 조정 완료 
        if MINUS_OFFSET <= diff_vo1 and diff_vo1 <= PLUS_OFFSET:
            print("finish value : ", diff_vo1)
            btn.setStyleSheet(self.greenback)  
            dis_value.setStyleSheet(self.greenback)
            self.get_esc_settings_value()
            self.get_esc_params()

            if ptype == 'VOLTAGE1GAIN':

                # VOLTAGE2OFFSET
                # client            : self.jig_client
                # vo_read_address   : device.JIG_READ_VOLTAGE2
                # offset_address    : device.ESC_DEV_VO2_OFFSET
                # dis_value          : self.edit_vol_gain2
                # btn               : self.btn_voltage2_offset
                # ptype             : VOLTAGE2OFFSET
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(lambda:self.start_offset_calib(self.jig_client, 
                        device.JIG_READ_VOLTAGE2, device.ESC_DEV_VO2_OFFSET,
                        self.edit_vol_offset2, self.btn_voltage2_offset, "VOLTAGE2OFFSET"))
                self.timer.start(READ_TIME)

            elif ptype == 'VOLTAGE2GAIN':

                self.esc_device_stop()

                ## OFFOFF
                device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON10, device.OFF)
                time.sleep(TIME_INTERVAL)
                device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON11, device.OFF)
                time.sleep(TIME_INTERVAL)
                device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON14, device.OFF)
                time.sleep(TIME_INTERVAL)

                #############################################################
                ######### CURRNET   #########################################
                #############################################################

                # Start Current
                # Display 2500
                self.lbl_voltage1.setText("2500")
                self.lbl_voltage2.setText("-2500")

                ## ONON
                # JIG에 CAP_ON12를 ON 설정하고, RUN 지령
                device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON12, device.ON)
                time.sleep(TIME_INTERVAL)

                # CAll Function for Current 1
                self.esc_device_run()

                # PC에서 ESC에 FORWARD와 REVERSE 지령을 반복하면서 ESC의 IO1 값을 확인한다.
                # ESC의 IO1 FORWARD와 REVERSE 값이 같아지도록 ESC의 IO1 OFFSET을 조정한다.

                # CURRENT 1 OFFSET
                # client            : self.esc_client
                # vo_read_address   : device.JIG_READ_CURRENT1
                # offset_address    : device.ESC_DEV_IO1_OFFSET
                # dis_value          : self.edit_vol_gain2
                # btn               : self.btn_voltage2_offset
                # ptype             : VOLTAGE2OFFSET
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(lambda:self.start_offset_calib(self.esc_client, 
                        device.ESC_READ_CURRENT1, device.ESC_DEV_IO1_OFFSET,
                        self.edit_cur_offset1, self.btn_current1_offset, "CURRENT1OFFSET"))
                self.timer.start(CAPON_TIME)

            elif ptype == 'CURRENT1GAIN':
                # CURRENT 2 OFFSET
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(lambda:self.start_offset_calib(self.esc_client, 
                        device.ESC_READ_CURRENT2, device.ESC_DEV_IO2_OFFSET,
                        self.edit_cur_offset2, self.btn_current2_offset, "CURRENT2OFFSET"))
                self.timer.start(READ_TIME)


            elif ptype == 'CURRENT2GAIN':

                self.esc_device_stop()

                device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON12, device.OFF)
                time.sleep(TIME_INTERVAL)

                #############################################################
                ######### IO LEAK   #########################################
                #############################################################

                # PC에서 ESC에 VO1 = 600V, VO2 = -600V로 설정하고, RUN 지령을 한다.
                device.write_registers(self.esc_client, device.ESC_WRITE_VOLTAGE1, 420)
                value = c_uint16(-420).value
                device.write_registers(self.esc_client, device.ESC_WRITE_VOLTAGE2, value)
                self.lbl_voltage1.setText("420")
                self.lbl_voltage2.setText("-420")

                ## ONON
                # PC에서 JIG에 CAP_ON13를 ON 설정하고, RUN 지령을 한다.
                device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON13, device.ON)
                time.sleep(TIME_INTERVAL)
                # CAll Function for Current 1
                self.esc_device_run()

                # PC에서 ESC에 FORWARD와 REVERSE 지령을 반복하면서 ESC의 IR1 값을 확인한다.
                # ESC의 IR1 FORWARD와 REVERSE 값이 같아지도록 ESC에 IR1 OFFSET을 조정한다.
                # IRLEAK 1 OFFSET
                # client            : self.esc_client
                # vo_read_address   : device.ESC_READ_IO_LEAK1
                # offset_address    : device.ESC_DEV_IO1_OFFSET
                # dis_value          : self.edit_irleak_gain1
                # edit_rev          : self.edit_irleak_rev_jig1
                # btn               : self.btn_irleak1_offset
                # ptype             : IRLEAK1OFFSET
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(lambda:self.start_offset_calib(self.esc_client, device.ESC_READ_IO_LEAK1, device.ESC_DEV_LEAK1_OFFSET,
                        self.edit_irleak_offset1, self.btn_irleak1_offset, "IRLEAK1OFFSET"))
                self.timer.start(CAPON_TIME)

            elif ptype == 'IRLEAK1GAIN':
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(lambda:self.start_offset_calib(self.esc_client, device.ESC_READ_IO_LEAK2, device.ESC_DEV_LEAK2_OFFSET,
                        self.edit_irleak_offset2, self.btn_irleak2_offset, "IRLEAK2OFFSET"))
                self.timer.start(READ_TIME)

            elif ptype == 'IRLEAK2GAIN':
                self.esc_device_stop()

                device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON13, device.OFF)

                ##############################
                ##############################
                ######### ORIGIN #############
                # setting timer
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(self.start_vbias)
                self.timer.start(READ_TIME)


        # GAIN 조정 필요
        else:
            self.calib_offset_gain(ptype, dis_value)

            # Gain Calibration
            self.cal_index += 1

            div_value = c_uint16(self.prev_gain).value
            device.write_registers(self.esc_client, gain_address, div_value)
            # setting timer
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(lambda:self.gain_read_esc_vo(self.esc_client, esc_vol_address, 
                            jig_vol_address, gain_address, dis_value, btn, ptype))
            self.timer.start(READ_TIME)


    #############################################################
    #########  4. START VBIAS ###################################
    #############################################################
    def start_vbias(self):
        # Timer 정지
        self.timer.stop()
        self.timer = None 
        # PC에서 JIG에 CAP_ON13을 OFF 설정하고, RUN 지령을 한다.
        
        ## ONON
        # PC에서 JIG에 CAP_ON13를 ON 설정하고, RUN 지령을 한다.
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON14, device.ON)
        time.sleep(TIME_INTERVAL)

        # - PC에서 ESC에 VO1 = 1250V, VO2 = -1250V로 설정하고, RUN 지령을 한다.
        device.write_registers(self.esc_client, device.ESC_WRITE_VOLTAGE1, 1250)
        value = c_uint16(-1250).value
        device.write_registers(self.esc_client, device.ESC_WRITE_VOLTAGE2, value)
        self.lbl_voltage1.setText("1250")
        self.lbl_voltage2.setText("-1250")

        # CAll Function for Current 1
        
        self.esc_device_run()
        time.sleep(TIME_INTERVAL)

        self.prev_gain = device.read_value(self.esc_client, device.ESC_DEV_VBIAS_OFFSET)
        self.edit_vbia_offset1.setText(str(self.prev_gain))
        
        self.cal_index = 0
        self.old_diff = 0
        print("\n@@@@ ", "VBIA OFFSET", " START @@@@\n")

        # setting timer
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.start_vbias_offset)
        self.timer.start(CAPON_TIME)


    #########  BIAS OFFSET 캘리브레이션 #############################
    def start_vbias_offset(self):
        # Timer 정지
        self.timer.stop()
        self.timer = None 

        # 1초 후에 ESC 똔 JIG의 VO1 값을 읽어와 해당 값을 저장함 
        vbia_val = device.read_value(self.esc_client, device.ESC_READ_VBIA)
        print(f"VBIAS OFFSET :: VBias Value = {vbia_val}")

        ############################################################
        ########  4. VBIA OFFSET 조정 완료  ##########################
        ############################################################
        if vbia_val == 0:
            print("VBIAS OFFSET finish value : ", vbia_val)
            self.btn_vbia_offset.setStyleSheet(self.greenback)
            self.edit_vbia_offset1.setStyleSheet(self.greenback)

            #########  BIAS GAIN. 캘리브레이션 #############################
            print("\n@@@@ ", "VBIA GAIN", " START @@@@\n")

            self.jig_device_run()
            self.prev_gain = device.read_value(self.esc_client, device.ESC_DEV_VBIAS_GAIN)
            self.edit_vbia_gain1.setText(str(self.prev_gain))
            self.cal_index = 0
            
            # setting timer
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(self.start_vbias_gain)
            self.timer.start(CAPON_TIME)

        # VBIA OFFSET 조정 
        else:
            self.calculate_gain("VBIAOFFSET", vbia_val, self.edit_vbia_offset1)
            # Offset 값 정리 
            self.cal_index += 1

            div_value = c_uint16(self.prev_gain).value
            device.write_registers(self.esc_client, device.ESC_DEV_VBIAS_OFFSET, div_value)

            # setting timer
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(self.start_vbias_offset)
            self.timer.start(READ_TIME)


    #########  BIAS GAIN 캘리브레이션 #############################
    def start_vbias_gain(self):
        # Timer 정지
        self.timer.stop()
        self.timer = None 

        # 1초 후에 ESC 똔 JIG의 VO1 값을 읽어와 해당 값을 저장함 
        self.jig_fwd = device.read_value(self.esc_client, device.ESC_READ_VBIA)
        self.jig_rev = device.read_value(self.jig_client, device.JIG_READ_VBIA)

        diff_val = abs(self.jig_fwd) - abs(self.jig_rev)
        print("VBIA GAIN", " ::: ESC V :     ", self.jig_fwd, "JIG V :      ",  
                self.jig_rev, " ******************>      ", diff_val)

        ############################################################
        ########  4. VBIA GAIN 조정 완료  ############################
        ############################################################
        if diff_val == 0:
            self.btn_vbia_gain.setStyleSheet(self.greenback)
            self.edit_vbia_gain1.setStyleSheet(self.greenback)

            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON14, device.OFF)
            time.sleep(TIME_INTERVAL)
            
            ########  CAP OFFSET 캘리브레이션  #############################
            print("\n@@@@ ", "CAP OFFSET CALIBARATION", " START @@@@\n")

            self.jig_device_stop()
            # - PC에서 ESC에 VO1 = 1250V, VO2 = -1250V로 설정하고, RUN 지령을 한다.
            device.write_registers(self.esc_client, device.ESC_WRITE_VOLTAGE1, 1250)
            value = c_uint16(-1250).value
            device.write_registers(self.esc_client, device.ESC_WRITE_VOLTAGE2, value)
            self.lbl_voltage1.setText("1250")
            self.lbl_voltage2.setText("-1250")

            self.esc_device_run()
            time.sleep(TIME_INTERVAL)
            
            self.prev_gain = device.read_value(self.esc_client, device.ESC_DEV_CAP_OFFSET)
            self.edit_cap_offset1.setText(str(self.prev_gain))
            self.cal_index = 0
            print(f"CAP OFFSET INITIAL VALUE :: {self.prev_gain}")
            
            # setting timer
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(lambda:self.start_cap())
            self.timer.start(CAPON_TIME)

        # VBIA GAIN 조정 필요 
        else:
            # PC에서 ESC에 VBIAS 값이 0~1V 사이가 되도록 VBIAS OFFSET을 조정한다
            abs_diff_vo1 = abs(diff_val)
            self.calib_offset_gain("VBIAGAIN", self.edit_vbia_gain1)

            # Offset 값 정리 
            self.cal_index += 1

            div_value = c_uint16(self.prev_gain).value
            device.write_registers(self.esc_client, device.ESC_DEV_VBIAS_GAIN, div_value)

            # setting timer
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(self.start_vbias_gain)
            self.timer.start(READ_TIME)

    #############################################################
    #########  5. CAP 캘리브레이션 #################################
    #############################################################
    def start_cap(self):

        # - PC에서 ESC에 VO1 = 1250V, VO2 = -1250V로 설정하고, RUN 지령을 한다.
        device.write_registers(self.esc_client, device.ESC_WRITE_VOLTAGE1, 1250)
        value = c_uint16(-1250).value
        device.write_registers(self.esc_client, device.ESC_WRITE_VOLTAGE2, value)
        self.lbl_voltage1.setText("1250")
        self.lbl_voltage2.setText("-1250")

        self.esc_device_run()
        time.sleep(TIME_INTERVAL)
        
        self.prev_gain = device.read_value(self.esc_client, device.ESC_DEV_CAP_OFFSET)
        self.edit_cap_offset1.setText(str(self.prev_gain))
        self.old_diff = 0
        self.cal_index = 0
        print(f"CAP OFFSET INITIAL VALUE :: {self.prev_gain}")
        
        # setting timer
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.start_cap_offset)
        self.timer.start(CAPON_TIME)


    #########  CAP OFFSET 캘리브레이션 #############################
    def start_cap_offset(self):
        # Timer 정지
        self.timer.stop()
        self.timer = None 

        # 1초 후에 ESC 똔 JIG의 VO1 값을 읽어와 해당 값을 저장함 
        vbia_val = device.read_value(self.esc_client, device.ESC_READ_CAP)
        print(f"CAP OFFSET :: Cap Value = {vbia_val}")
        # CAP OFFSET 조정 완료
        if vbia_val != 0 and vbia_val <= PLUS_OFFSET4:
            self.btn_cap_offset.setStyleSheet(self.greenback)
            self.edit_cap_offset1.setStyleSheet(self.greenback)
            print("finish value : ", vbia_val)
            #############################################################
            #########  ZCS1 GAIN. 캘리브레이션 #############################
            #############################################################
            self.esc_device_stop()
            # JIG에 CAP_ON0를 ON 설정하고, RUN 지령

            time.sleep(TIME_INTERVAL)
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON0, device.ON)
            time.sleep(TIME_INTERVAL)

            # PC에서 ESC에 VO1 = 1250V, VO2 = -1250V로 설정하고, RUN 지령을 한다
            device.write_registers(self.esc_client, device.ESC_WRITE_VOLTAGE1, 1250)
            value = c_uint16(-1250).value
            device.write_registers(self.esc_client, device.ESC_WRITE_VOLTAGE2, value)
            self.lbl_voltage1.setText("1250")
            self.lbl_voltage2.setText("-1250")

            time.sleep(TIME_INTERVAL)
            self.esc_device_run()
            time.sleep(TIME_INTERVAL)

            
            self.cal_index = 0
            self.old_diff = 0

            print("\n@@@@ ", "ZCS1 GAIN", " START @@@@\n")

            self.prev_gain = device.read_value(self.esc_client, device.ESC_DEV_ZCS1_GAIN)
            self.edit_zcs_gain1.setText(str(self.prev_gain))
            print(f"ZCS1 GAIN INITIAL VALUE :: {self.prev_gain}")
            
            # setting timer
            self.timer = QtCore.QTimer()
            # OLD 방식
            # self.timer.timeout.connect(lambda:self.start_zcs_gain(device.ESC_READ_CAP, 
            #                 device.ESC_DEV_ZCS1_GAIN, 94, self.btn_zcs1, "ZCS1GAIN", self.edit_zcs_gain1))

            self.timer.timeout.connect(self.start_new_pow_coeff)
            self.timer.start(CAPON_TIME)

        # CAP OFFSET 조정 
        else:
            # PC에서 ESC에 CAP 값이 0~1V 사이가 되도록 CAP OFFSET을 조정한다
            # PC에서 ESC에 VBIAS 값이 0~1V 사이가 되도록 VBIAS OFFSET을 조정한다
            abs_diff_vo1 = abs(vbia_val)
            self.calculate_gain("CAPOFFSET", abs_diff_vo1, self.edit_cap_offset1)
                
            # Offset 값 정리 
            self.cal_index += 1

            div_value = c_uint16(self.prev_gain).value
            device.write_registers(self.esc_client, device.ESC_DEV_CAP_OFFSET, div_value)

            # setting timer
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(lambda:self.start_cap_offset())
            self.timer.start(READ_TIME)


    #########  START ZCS GAIN #############################
    def start_zcs_gain(self, val_read_address, gain_address, setting_value, btn, ptype, dis_value):
        # Timer 정지
        self.timer.stop()
        self.timer = None 

        # 1초 후에 ESC 똔 JIG의 VO1 값을 읽어와 해당 값을 저장함 
        esc_val = device.read_value(self.esc_client, val_read_address)
        diff_val = abs(setting_value - esc_val)
        print(f"{ptype} :: setting value = {setting_value}, ESC Val = {esc_val}")
        # ZCS GAIN 조정 완료
        if MINUS_OFFSET <= diff_val and diff_val <= PLUS_OFFSET:
            
            if ptype == "ZCS1GAIN":
                btn.setStyleSheet(self.greenback)
                dis_value.setStyleSheet(self.greenback)
                
                self.esc_device_stop()
                # JIG에 CAP_ON0를 ON 설정하고, RUN 지령
                device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON0, device.OFF)
                time.sleep(TIME_INTERVAL)

                # PC에서 ESC에 VO1 = 1250V, VO2 = -1250V로 설정하고, RUN 지령을 한다
                device.write_registers(self.esc_client, device.ESC_WRITE_VOLTAGE1, 1250)
                value = c_uint16(-1250).value
                device.write_registers(self.esc_client, device.ESC_WRITE_VOLTAGE2, value)
                self.lbl_voltage1.setText("1250")
                self.lbl_voltage2.setText("-1250")

                #############################################################
                #########  ZCS2 GAIN. 캘리브레이션 #############################
                #############################################################
                print("@"*40)
                print("@@@@ ", "ZCS2 GAIN", " START @@@@")
                print("@"*40)
                # # CAll Function for Current 1
                device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON1, device.ON)
                time.sleep(TIME_INTERVAL)
                self.prev_gain = device.read_value(self.esc_client, device.ESC_DEV_ZCS2_GAIN)
                self.cal_index = 0
                print(f"ZCS1 GAIN INITIAL VALUE :: {self.prev_gain}")

                self.esc_device_run()

                # setting timer
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(lambda:self.start_zcs_gain(device.ESC_READ_CAP, 
                                device.ESC_DEV_ZCS2_GAIN, 250, self.btn_zcs2, "ZCS2GAIN", self.edit_zcs_gain2))
                self.timer.start(CAPON_TIME)

            elif ptype == "ZCS2GAIN":
                btn.setStyleSheet(self.greenback)
                dis_value.setStyleSheet(self.greenback)
                
                self.esc_device_stop()
                time.sleep(TIME_INTERVAL)
                # JIG에 CAP_ON0를 ON 설정하고, RUN 지령
                device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON1, device.OFF)
                print("#"*50)
                print("#"*20, " FINISH CALIBARATION ", "#"*20)
                print("#"*50)
                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(self.start_zcs3_gain)
                self.timer.start(READ_TIME)


        # ZCS GAIN 조정 
        else:
            # PC에서 ESC에 VBIAS 값이 0~1V 사이가 되도록 VBIAS OFFSET을 조정한다
            abs_diff_vo1 = abs(diff_val)
            self.calculate_gain(ptype, abs_diff_vo1, dis_value)
                
            # Offset 값 정리 
            self.cal_index += 1

            div_value = c_uint16(self.prev_gain).value
            device.write_registers(self.esc_client, gain_address, div_value)

            # setting timer
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(lambda:self.start_zcs_gain(val_read_address, 
                        gain_address, setting_value, btn, ptype, dis_value))
            self.timer.start(READ_TIME)

    #############################################################
    #########  ZCS3 GAIN. 캘리브레이션 #############################
    #############################################################
    def start_new_pow_coeff(self):
        device.check_password(self.esc_client)
        # Timer 정지
        self.timer.stop()
        self.timer = None 

        device.write_registers(self.esc_client, device.ESC_DEV_ZCS1_GAIN, 1000)
        device.write_registers(self.esc_client, device.ESC_DEV_ZCS2_GAIN, 1000)

        print("@"*40)
        print("@@@@ ", "NEW POW COEFF CALIBARATION", " START @@@@")
        print("@"*40)

        self.cap_list = np.array([255, 516, 763, 1078, 1497, 2760, 4918, 7451, 10051])
        self.cal_value = np.array([])
        self.rate_list = []
        self.zcs_index = 0
        self.zcs_cal_index = 0

        # 현재의 ACOEFF, BPOW 값을 읽어와 보여줌 
        val = device.read_value(self.esc_client, device.ESC_DEV_ZCS1_GAIN)
        self.old_acoeff = val
        self.edit_zcsacoeff.setText(str(val))
        val = device.read_value(self.esc_client, device.ESC_DEV_ZCS2_GAIN)
        self.old_bpow = val
        self.edit_zcsbpow.setText(str(val))

        # PC에서 ESC에 VO1 = 1250V, VO2 = -1250V로 설정하고, RUN 지령을 한다
        device.write_registers(self.esc_client, device.ESC_WRITE_VOLTAGE1, 1250)
        
        value = c_uint16(-1250).value
        device.write_registers(self.esc_client, device.ESC_WRITE_VOLTAGE2, value)
        self.lbl_voltage1.setText("1250")
        self.lbl_voltage2.setText("-1250")

        # setting timer
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.set_capon_on)
        self.timer.start(CAPON_TIME)


    # JIG_WRITE_CAP_ON0 ON
    def set_capon_on(self):
        # Timer 정지
        self.timer.stop()
        self.timer = None 

        if self.zcs_index == 0:
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON1, device.ON)
        elif self.zcs_index == 1:
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON2, device.ON)
        elif self.zcs_index == 2:
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON3, device.ON)
        elif self.zcs_index == 3:
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON4, device.ON)
        elif self.zcs_index == 4:
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON5, device.ON)
        elif self.zcs_index == 5:
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON6, device.ON)
        elif self.zcs_index == 6:
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON7, device.ON)
        elif self.zcs_index == 7:
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON8, device.ON)
        elif self.zcs_index == 8:
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON9, device.ON)

        time.sleep(TIME_INTERVAL)
        self.esc_device_run()

        # setting timer
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.read_cap_value)
        self.timer.start(CAPON_TIME)


    # JIG_WRITE_CAP_ON0 READ
    def read_cap_value(self):
        # Timer 정지
        self.timer.stop()
        self.timer = None 

        val = device.read_value(self.esc_client, device.ESC_READ_CAP)
        self.cal_value = np.append(self.cal_value, val)
        rate = round((val-self.cap_list[self.zcs_index])/self.cap_list[self.zcs_index]*100, 2)
        self.rate_list.append(rate)

        self.esc_device_stop()
        time.sleep(TIME_INTERVAL)

        if self.zcs_index == 0:
            self.edit_val_cap1.setText(str(val))
            self.edit_rate1.setText(str(rate))
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON1, device.OFF)
        elif self.zcs_index == 1:
            self.edit_val_cap2.setText(str(val))
            self.edit_rate2.setText(str(rate))
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON2, device.OFF)
        elif self.zcs_index == 2:
            self.edit_val_cap3.setText(str(val))
            self.edit_rate3.setText(str(rate))
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON3, device.OFF)
        elif self.zcs_index == 3:
            self.edit_val_cap4.setText(str(val))
            self.edit_rate4.setText(str(rate))
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON4, device.OFF)
        elif self.zcs_index == 4:
            self.edit_val_cap5.setText(str(val))
            self.edit_rate5.setText(str(rate))
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON5, device.OFF)
        elif self.zcs_index == 5:
            self.edit_val_cap6.setText(str(val))
            self.edit_rate6.setText(str(rate))
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON6, device.OFF)
        elif self.zcs_index == 6:
            self.edit_val_cap7.setText(str(val))
            self.edit_rate7.setText(str(rate))
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON7, device.OFF)
        elif self.zcs_index == 7:
            self.edit_val_cap8.setText(str(val))
            self.edit_rate8.setText(str(rate))
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON8, device.OFF)
        elif self.zcs_index == 8:
            self.edit_val_cap9.setText(str(val))
            self.edit_rate9.setText(str(rate))
            device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON9, device.OFF)


        if self.zcs_index == 8:
            print(self.cap_list)
            print(self.cal_value)
            print(self.rate_list)

            minv, maxv = -5, 5
            res = all(ele > minv and ele < maxv for ele in self.rate_list)

            if not res:
                params, _ = curve_fit(fit_func, self.cal_value, self.cap_list)
                acoeff, bpow = params
                acoeff = round(acoeff, 3)
                bpow = round(bpow, 3)

                value = int(acoeff * 1000)
                value = int((self.old_acoeff + value)/2)
                print("Write Value COEFF : ", acoeff, value)
                self.edit_zcsacoeff.setText(str(value))
                device.write_registers(self.esc_client, device.ESC_DEV_ZCS1_GAIN, value)
                self.old_acoeff = value

                value = int(bpow * 1000)
                if value >= self.old_bpow:
                    value = int((self.old_bpow + value + 2)/2)
                else:
                    value = int((self.old_bpow + value - 2)/2)

                print("Write Value POW : ", bpow, value)
                self.edit_zcsbpow.setText(str(value))
                device.write_registers(self.esc_client, device.ESC_DEV_ZCS2_GAIN, value)
                self.old_bpow = value 

                self.zcs_cal_index += 1
                print(self.zcs_cal_index, " 번째 조정")
                self.zcs_index = 0
                self.cal_value = np.array([])
                self.rate_list = []

                self.edit_val_cap1.setText("")
                self.edit_rate1.setText("")
                self.edit_val_cap2.setText("")
                self.edit_rate2.setText("")
                self.edit_val_cap3.setText("")
                self.edit_rate3.setText("")
                self.edit_val_cap4.setText("")
                self.edit_rate4.setText("")
                self.edit_val_cap5.setText("")
                self.edit_rate5.setText("")
                self.edit_val_cap6.setText("")
                self.edit_rate6.setText("")
                self.edit_val_cap7.setText("")
                self.edit_rate7.setText("")
                self.edit_val_cap8.setText("")
                self.edit_rate8.setText("")
                self.edit_val_cap9.setText("")
                self.edit_rate9.setText("")

                self.timer = QtCore.QTimer()
                self.timer.timeout.connect(self.set_capon_on)
                self.timer.start(CAPON_TIME)

            else:
                print("#"*40)
                print("Finish Calibration")
                print("#"*40)


        else:
            self.zcs_index += 1
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(self.set_capon_on)
            self.timer.start(CAPON_TIME)
    

    def zcs_calibration(self):
        self.btn_stop.setEnabled(True)
        self.prev_gain = int(self.zcs3_val.text())
        print(self.prev_gain)

        div_value = c_uint16(self.prev_gain).value
        device.write_registers(self.esc_client, device.ESC_DEV_ZCS3_GAIN, div_value)
        print(f"ZCS3 Modify GAIN value = {self.prev_gain}")

        # setting timer
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.start_zcs3_gain)
        self.timer.start(CAPON_TIME)


    def calib_offset_gain(self, ptype, dis_value):
        val_fwd = abs(self.jig_fwd)
        val_rev = abs(self.jig_rev)

        if val_fwd > val_rev:
            direction = UP
        else:
            direction = DOWN

        diff_val = abs(val_fwd - val_rev)
        new_gain = diff_val
        self.old_gain = self.prev_gain

        if self.cal_index == 0:
            self.repeatP = 0
            self.repeatN = 0
            self.old_updown = direction

            if ptype.startswith("VOLTAGE1") and ptype.endswith("OFFSET"):
                if direction == UP:
                    self.gain_dir = PLUS
                    self.prev_gain += new_gain
                else:
                    self.gain_dir = MINUS
                    self.prev_gain -= new_gain

            elif ptype.startswith("VOLTAGE1") and ptype.endswith("GAIN"):
                if direction == UP:
                    self.gain_dir = MINUS
                    self.prev_gain -= new_gain
                else:
                    self.gain_dir = PLUS
                    self.prev_gain += new_gain

            elif ptype.startswith("VOLTAGE2") and ptype.endswith("OFFSET"):
                if direction == UP:
                    self.gain_dir = MINUS
                    self.prev_gain -= new_gain
                else:
                    self.gain_dir = PLUS
                    self.prev_gain += new_gain

            elif ptype.startswith("VOLTAGE2") and ptype.endswith("GAIN"):
                if direction == UP:
                    self.gain_dir = PLUS
                    self.prev_gain -= new_gain
                else:
                    self.gain_dir = MINUS
                    self.prev_gain += new_gain

            elif ptype.startswith("CURRENT1") and ptype.endswith("OFFSET"):
                new_gain = int(new_gain/2)
                if direction == UP:
                    self.gain_dir = MINUS
                    self.prev_gain -= new_gain
                else:
                    self.gain_dir = PLUS
                    self.prev_gain += new_gain

            elif ptype.startswith("CURRENT1") and ptype.endswith("GAIN"):
                new_gain = int(new_gain/2)
                if direction == UP:
                    self.gain_dir = MINUS
                    self.prev_gain -= new_gain
                else:
                    self.gain_dir = PLUS
                    self.prev_gain += new_gain

            elif ptype.startswith("CURRENT2") and ptype.endswith("OFFSET"):
                new_gain = int(new_gain/2)
                if direction == UP:
                    self.gain_dir = PLUS
                    self.prev_gain += new_gain
                else:
                    self.gain_dir = MINUS
                    self.prev_gain -= new_gain

            elif ptype.startswith("CURRENT2") and ptype.endswith("GAIN"):
                new_gain = int(new_gain/2)
                if direction == UP:
                    self.gain_dir = MINUS
                    self.prev_gain -= new_gain
                else:
                    self.gain_dir = PLUS
                    self.prev_gain += new_gain

            elif ptype.startswith("IRLEAK1") and ptype.endswith("OFFSET"):
                new_gain = new_gain * 5
                if direction == UP:
                    self.gain_dir = MINUS
                    self.prev_gain -= new_gain
                else:
                    self.gain_dir = PLUS
                    self.prev_gain += new_gain

            elif ptype.startswith("IRLEAK1") and ptype.endswith("GAIN"):
                new_gain = new_gain * 5
                if direction == UP:
                    self.gain_dir = MINUS
                    self.prev_gain -= new_gain
                else:
                    self.gain_dir = PLUS
                    self.prev_gain += new_gain

            elif ptype.startswith("IRLEAK2") and ptype.endswith("OFFSET"):
                new_gain = new_gain * 5
                if direction == UP:
                    self.gain_dir = PLUS
                    self.prev_gain += new_gain
                else:
                    self.gain_dir = MINUS
                    self.prev_gain -= new_gain

            elif ptype.startswith("IRLEAK2") and ptype.endswith("GAIN"):
                new_gain = new_gain * 5
                if direction == UP:
                    self.gain_dir = MINUS
                    self.prev_gain -= new_gain
                else:
                    self.gain_dir = PLUS
                    self.prev_gain += new_gain

            elif ptype.startswith("VBIA") and ptype.endswith("GAIN"):
                if direction == UP:
                    self.gain_dir = MINUS
                    self.prev_gain -= new_gain
                else:
                    self.gain_dir = PLUS
                    self.prev_gain += new_gain

            else:
                self.prev_gain += new_gain

        else:
            # UP DOWN 방향이 바뀌지 않았다면
            if self.old_updown == direction:
                # PLUS 방향으로 작아 지고 있음. GOOD
                if diff_val < self.old_diff and self.gain_dir == PLUS:
                    if ptype.startswith("IRLEAK") and ptype.endswith("OFFSET") and diff_val > 10:
                        self.prev_gain += new_gain * 10
                    elif ptype.startswith("IRLEAK") and ptype.endswith("OFFSET") and diff_val > 5:
                        self.prev_gain += new_gain * 5
                    elif ptype.startswith("CURRENT") and diff_val <= 3:
                        self.prev_gain += 1
                    else:
                        self.prev_gain += new_gain
                    
                    if self.repeatP:
                        self.repeatP = 0

                # MINUS 방향으로 작아 지고 있음. GOOD
                elif diff_val < self.old_diff and self.gain_dir == MINUS:
                    if ptype.startswith("IRLEAK") and ptype.endswith("OFFSET") and diff_val > 10:
                        self.prev_gain -= new_gain * 10
                    elif ptype.startswith("IRLEAK") and ptype.endswith("OFFSET") and diff_val > 5:
                        self.prev_gain -= new_gain * 5
                    elif ptype.startswith("CURRENT") and diff_val <= 3:
                        self.prev_gain -= 1
                    else:
                        self.prev_gain -= new_gain
                    
                    if self.repeatN:
                        self.repeatN = 0

                # PLUS 로 가는데, 값이 커지고 있음. 
                elif diff_val > self.old_diff and self.gain_dir == PLUS:
                    # 이미 2회 이상이고, 반복 회수가 REPEATN (2) 회 이하이면
                    if self.cal_index > REPEATN and self.repeatP < REPEATN:
                        self.prev_gain += int(new_gain/2) or 1
                        self.repeatP += 1
                    # 방향을 바꿔어야 함.
                    else:
                        self.prev_gain -= int(new_gain/2) or 1
                        self.gain_dir = MINUS
                        self.repeatP = 0
                        print("DIR CHANGE MINUS ", diff_val, self.old_diff)
                
                # MINUS 로 가는데, 값이 커지고 있음.
                elif diff_val > self.old_diff and self.gain_dir == MINUS:
                    if self.cal_index > REPEATN and self.repeatN < REPEATN:
                        self.prev_gain -= int(new_gain/2) or 1
                        self.repeatN += 1
                    else:
                        self.prev_gain += int(new_gain/2) or 1
                        self.gain_dir = PLUS
                        self.repeatN = 0
                        print("DIR CHANGE PLUS ", diff_val, self.old_diff)

                # PLUS 방향으로 가다가 같은 값이 나오면.
                elif diff_val == self.old_diff and self.gain_dir == PLUS:
                    # 10회 이상 반복 했다면 방향을 바꿔 줌
                    if self.repeatP >= 30:
                        self.gain_dir = MINUS 
                        self.prev_gain = -20
                        self.repeatP = 0
                    # 지속적으로 작은 값을 제공함
                    else:
                        if diff_val > 3 and ptype.startswith("VOLTAGE") or ptype.startswith("IRLEAK") \
                                or ptype.startswith("VBIA") or ptype.startswith("CAP") or ptype.startswith("ZCS"):
                            self.prev_gain += new_gain
                        else:
                            self.prev_gain += 1
                        self.repeatP += 1

                # MINUS 방향으로 가다가 같은 값이 나오면.
                elif diff_val == self.old_diff and self.gain_dir == MINUS:
                    if self.repeatN >= 30:
                        self.gain_dir = PLUS 
                        self.prev_gain += 20
                        self.repeatN = 0
                    else:
                        if diff_val > 3 and ptype.startswith("VOLTAGE") or ptype.startswith("IRLEAK") \
                                or ptype.startswith("VBIA") or ptype.startswith("CAP") or ptype.startswith("ZCS"):
                            self.prev_gain -= new_gain
                        else:
                            self.prev_gain -= 1
                        self.repeatN += 1
                        
                else:
                    self.prev_gain += 5
                    print("             &&&& I don't know", diff_val, self.old_diff)

            # UP DOWN 발생
            else:
                self.old_updown = direction
                if self.gain_dir == PLUS:
                    self.gain_dir = MINUS 
                    self.prev_gain -= int(new_gain / 3)
                else:
                    self.gain_dir = PLUS 
                    self.prev_gain += int(new_gain / 3)

        print(f"    ^^^UPD = {direction} DIR = {self.gain_dir}::  {self.cal_index}th, OLD_GAIN = {self.old_gain},  New_GAIN = {self.prev_gain}")
        self.old_diff = diff_val

        #### Display
        dis_value.setText(str(self.prev_gain))


    #########  COMMON CALCULATE #############################
    def calculate_gain(self, ptype, abs_diff_vo1, dis_value):
        if ptype.startswith("VBIA"): # and ptype.endswith("GAIN"):
            origin_val = abs_diff_vo1
            abs_diff_vo1 = abs(abs_diff_vo1)
            if abs_diff_vo1 > PLUS_OFFSET * 10:
                div_value = 6
            elif abs_diff_vo1 > PLUS_OFFSET * 5:
                div_value = 4
            elif abs_diff_vo1 > PLUS_OFFSET * 3:
                div_value = 3
            elif abs_diff_vo1 >= PLUS_OFFSET * 2:
                div_value = 2
            else:
                div_value = 1 

        elif ptype.startswith("CAP"):
            if abs_diff_vo1 >= PLUS_OFFSET * 20:
                div_value = 4
            elif abs_diff_vo1 >= PLUS_OFFSET * 5:
                div_value = 2
            else:
                div_value = 1 

        elif ptype.startswith("ZCS"):
            if abs_diff_vo1 >= PLUS_OFFSET * 10:
                div_value = int(abs_diff_vo1/2)
            elif abs_diff_vo1 >= PLUS_OFFSET * 5:
                div_value = 2
            else:
                div_value = 1 

        else:
            if abs_diff_vo1 > PLUS_OFFSET * 30:
                div_value = 30
            elif abs_diff_vo1 > PLUS_OFFSET * 20:
                div_value = 10
            elif abs_diff_vo1 > PLUS_OFFSET * 10:
                div_value = 5
            elif abs_diff_vo1 > PLUS_OFFSET * 3:
                div_value = 2
            else:
                div_value = 1 

        new_updown = self.offset_updown()

        if self.cal_index == 0:
            print("Start Value : ", self.prev_gain)
            self.gain_updown = new_updown
            self.repeatP = 0
            self.repeatN = 0

            if ptype.startswith("IRLEAK"):
                if self.gain_updown == UP:
                    self.prev_gain -= div_value
                    self.gain_dir = MINUS 
                else:
                    self.prev_gain += div_value
                    self.gain_dir = PLUS

            elif ptype.startswith("CAP"):
                self.prev_gain += div_value
                self.gain_dir = PLUS 

            elif ptype.startswith("VBIA"):
                if origin_val < 0:
                    self.prev_gain += div_value
                    self.gain_dir = PLUS 
                else:
                    self.prev_gain -= div_value
                    self.gain_dir = MINUS 

            else:
                if self.gain_updown == UP:
                    self.prev_gain += div_value
                    self.gain_dir = PLUS 
                else:
                    self.prev_gain -= div_value
                    self.gain_dir = MINUS
            print("First : ", self.prev_gain, new_updown, self.gain_dir)

        # PLUS 방향으로 작아 지고 있음. GOOD
        elif abs_diff_vo1 < self.old_diff and self.gain_dir == PLUS:
            self.prev_gain += div_value
            if self.repeatP:
                self.repeatP = 0

        # MINUS 방향으로 작아 지고 있음. GOOD
        elif abs_diff_vo1 < self.old_diff and self.gain_dir == MINUS:
            self.prev_gain -= div_value
            if self.repeatN:
                self.repeatN = 0

        # PLUS 로 가는데, 값이 커지고 있음. 
        elif abs_diff_vo1 > self.old_diff and self.gain_dir == PLUS:
            # 이미 2회 이상이고, 반복 회수가 REPEATN (2) 회 이하이면
            if self.cal_index > REPEATN and self.repeatP < REPEATN:
                self.prev_gain += int(div_value/2) or 1
                self.repeatP += 1
            # 방향을 바꿔어야 함.
            else:
                self.prev_gain -= int(div_value/2) or 1
                self.gain_dir = MINUS
                self.repeatP = 0
        
        # MINUS 로 가는데, 값이 커지고 있음.
        elif abs_diff_vo1 > self.old_diff and self.gain_dir == MINUS:
            if self.cal_index > REPEATN and self.repeatN < REPEATN:
                self.prev_gain -= int(div_value/2) or 1
                self.repeatN += 1
            else:
                self.prev_gain += int(div_value/2) or 1
                self.gain_dir = PLUS
                self.repeatN = 0

        # PLUS 방향으로 가다가 같은 값이 나오면.
        elif abs_diff_vo1 == self.old_diff and self.gain_dir == PLUS:
            # 10회 이상 반복 했다면 방향을 바꿔 줌
            if self.repeatP >= 30:
                self.gain_dir = MINUS 
                self.prev_gain = -20
                print(f"     !!! Same Value PLUS :: Change Direction {self.prev_gain} {self.repeatP}th")
                self.repeatP = 0
            # 지속적으로 작은 값을 제공함
            else:
                if abs_diff_vo1 > 3 and ptype.startswith("VOLTAGE") or ptype.startswith("IRLEAK") \
                        or ptype.startswith("VBIA") or ptype.startswith("CAP") or ptype.startswith("ZCS"):
                    self.prev_gain += div_value
                else:
                    self.prev_gain += 1
                self.repeatP += 1
                print(f"     !!! Same Value PLUS :: {self.prev_gain} {self.repeatP}th")

        # MINUS 방향으로 가다가 같은 값이 나오면.
        elif abs_diff_vo1 == self.old_diff and self.gain_dir == MINUS:
            if self.repeatN >= 30:
                self.gain_dir = PLUS 
                self.prev_gain += 20
                print(f"     !!! Same Value MINUS :: Change Direction {self.prev_gain} {self.repeatN}th")
                self.repeatN = 0
            else:
                if abs_diff_vo1 > 3 and ptype.startswith("VOLTAGE") or ptype.startswith("IRLEAK") \
                        or ptype.startswith("VBIA") or ptype.startswith("CAP") or ptype.startswith("ZCS"):
                    self.prev_gain -= div_value
                else:
                    self.prev_gain -= 1
                self.repeatN += 1
                print(f"     !!! Same Value MINUS :: {self.prev_gain} {self.repeatN}th")
                
        else:
            self.prev_gain += 5
            print("             &&&& I don't know", abs_diff_vo1, self.old_diff, self.gain_updown)

        print(f"    ^^^^^{ptype} ::  {self.cal_index}th, Diff_Val = {self.old_diff},  Div Value = {div_value}, Offset Value : {self.prev_gain}")
        self.old_diff = abs_diff_vo1

        #### Display
        dis_value.setText(str(self.prev_gain))


    # OFFSET GAIN 완료 후 ESC 정보 읽어와 보여주기
    def get_esc_settings_value(self):
        # self.esc_device_run()
        # time.sleep(TIME_INTERVAL)
        result = device.read_esc_values(self.esc_client)
        # print("Settings : ", result)
        if result:
            self.lbl_voltage1.setText(str(result['voltage1']))
            self.lbl_voltage2.setText(str(result['voltage2']))
            self.lbl_current1.setText(str(result['current1']))
            self.lbl_current2.setText(str(result['current2']))
            self.lbl_vbias.setText(str(result['vbias']))
            self.lbl_vcs.setText(str(result['vcs']))
            self.lbl_ics.setText(str(result['ics']))
            self.lbl_cp.setText(str(result['cp']))
            self.lbl_leak1.setText(str(result['ioleak1']))
            self.lbl_leak2.setText(str(result['ioleak2']))


    # OFFSET GAIN 완료 후 ESC 정보 읽어와 보여주기
    def get_esc_params(self):
        result = device.read_esc_setting_value(self.esc_client)
        # print("Params : ", result)

        if result:
            self.voltage1.setText(str(result['voltage1']))
            self.current1.setText(str(result['current1']))
            self.voltage2.setText(str(result['voltage2']))
            self.current2.setText(str(result['current2']))
            self.leak_fault_level.setText(str(result['leak_fault_level']))
            self.ro_min_fault.setText(str(result['ro_min_fault']))
            self.up_time.setText(str(result['up_time']))
            self.down_time.setText(str(result['down_time']))
            
            if result['rd_mode']:
                self.rd_mode_on.setChecked(True)
            else:
                self.rd_mode_off.setChecked(True)

            self.toggle_count.setText(str(result['toggle_count']))
            self.slope.setText(str(result['slope']))
            self.coeff.setText(str(result['coeff']))

            if result['rd_select']:
                self.rd_select_remote.setChecked(True)
            else:
                self.rd_select_internal.setChecked(True)

            self.local_address.setText(str(result['local_address']))
            self.arc_delay.setText(str(result['arc_delay']))
            self.arc_rate.setText(str(result['arc_rate']))

            if result['rd_toggle']:
                self.rd_toggle_on.setChecked(True)
            else:
                self.rd_toggle_off.setChecked(True)

            if result['rd_arc']:
                self.rd_arc_on.setChecked(True)
            else:
                self.rd_arc_off.setChecked(True)

            if result['rd_ocp']:
                self.rd_ocp_on.setChecked(True)
            else:
                self.rd_ocp_off.setChecked(True)
            
            self.target_cap.setText(str(result['target_cap']))
            self.cap_deviation.setText(str(result['cap_deviation']))
            # self.time_delay.setText(str(result['time_delay']))


    # 임시용 ESC JIG Connection
    def temp_setup_connection(self):
        gen_port = None
        com_speed = 115200
        com_data = 8
        com_parity = serial.PARITY_NONE
        com_stop = 1

        ports = device.get_comm_port()
        print(ports)
        self.esc_client = device.make_connect(
            port=ports[0], ptype='rtu',
            speed=com_speed, bytesize=com_data, 
            parity=com_parity, stopbits=com_stop
        )

        if self.esc_client:
            self.label_esc_gen.setText(ports[0])
            self.btn_esc_gen.setEnabled(False)
            self.btn_esc_gen.hide()

        # self.jig_client = device.make_connect(
        #     port=ports[1], ptype='rtu',
        #     speed=com_speed, bytesize=com_data, 
        #     parity=com_parity, stopbits=com_stop
        # )

        # if self.jig_client:
        #     self.label_jig.setText(ports[1])
        #     self.label_jig_unit.setText("O")
        #     self.btn_jig.setEnabled(False)
    

    # ESC 전원 장치와 송신을 위한 설정
    def connect_esc_gen(self):
        if self.com_open_flag == False:
            Dialog = QDialog()
            self.com_open_flag == True
            dialog = SettingWin(Dialog)
            dialog.show()
            response = dialog.exec_()

            # OK 를 하면 설정 값을 읽어와서 통신을 한다.
            if response == QDialog.Accepted:
                self.gen_port = dialog.gen_port
                self.com_speed = dialog.com_speed
                self.com_data = dialog.com_data
                self.com_parity = dialog.com_parity
                self.com_stop = dialog.com_stop
                self.com_open_flag = False
                self.esc_client = device.make_connect(
                    port=self.gen_port, ptype='rtu',
                    speed=self.com_speed, bytesize=self.com_data, 
                    parity=self.com_parity, stopbits=self.com_stop
                )

                if self.esc_client:
                    self.label_esc_gen.setText(self.gen_port)
                    self.label_esc_gen_unit.setText("O")
                    self.btn_run.setEnabled(True)
                
                self.com_open_flag = False

        else:
            print("Already Open Dialog")


    # JIG 전원 장치와 송신을 위한 설정
    def connect_jig_gen(self):
        if self.com_open_flag == False:
            Dialog = QDialog()
            self.com_open_flag == True
            dialog = SettingWin(Dialog)
            dialog.show()
            response = dialog.exec_()

            # OK 를 하면 설정 값을 읽어와서 통신을 한다.
            if response == QDialog.Accepted:
                self.gen_port = dialog.gen_port
                self.com_speed = dialog.com_speed
                self.com_data = dialog.com_data
                self.com_parity = dialog.com_parity
                self.com_stop = dialog.com_stop
                self.com_open_flag = False
                self.jig_client = device.make_connect(
                    port=self.gen_port, ptype='rtu',
                    speed=self.com_speed, bytesize=self.com_data, 
                    parity=self.com_parity, stopbits=self.com_stop
                )

                if self.jig_client:
                    self.label_jig.setText(self.gen_port)
                    self.label_jig_unit.setText("O")
                
                self.com_open_flag = False

        else:
            print("Already Open Dialog")


    # ESC RESET
    def device_reset(self):
        print("#"*40)
        print("######## ESC RESET ##########################")
        print("#"*40)

        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON0, device.ON)

        # device.write_registers(self.esc_client, device.ESC_DEV_VO1_OFFSET, device.STOP_VALUE)
        # device.write_registers(self.esc_client, device.ESC_DEV_VO1_GAIN, device.STOP_VALUE)
        # device.write_registers(self.esc_client, device.ESC_DEV_VO2_OFFSET, device.STOP_VALUE)
        # device.write_registers(self.esc_client, device.ESC_DEV_VO2_GAIN, device.STOP_VALUE)

        # device.write_registers(self.esc_client, device.ESC_DEV_IO1_OFFSET, device.STOP_VALUE)
        # device.write_registers(self.esc_client, device.ESC_DEV_IO1_GAIN, device.STOP_VALUE)
        # device.write_registers(self.esc_client, device.ESC_DEV_IO2_OFFSET, device.STOP_VALUE)
        # device.write_registers(self.esc_client, device.ESC_DEV_IO2_GAIN, device.STOP_VALUE)

        # device.write_registers(self.esc_client, device.ESC_DEV_LEAK1_OFFSET, device.STOP_VALUE)
        # device.write_registers(self.esc_client, device.ESC_DEV_LEAK1_GAIN, device.STOP_VALUE)
        # device.write_registers(self.esc_client, device.ESC_DEV_LEAK2_OFFSET, device.STOP_VALUE)
        # device.write_registers(self.esc_client, device.ESC_DEV_LEAK2_GAIN, device.STOP_VALUE)


    # 단말기 STOP
    def device_stop(self):
        self.esc_device_stop()
        device.jig_device_stop(self.jig_client)
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON0, device.OFF)
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON1, device.OFF)
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON2, device.OFF)
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON3, device.OFF)
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON4, device.OFF)
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON5, device.OFF)
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON6, device.OFF)
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON7, device.OFF)
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON8, device.OFF)
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON9, device.OFF)
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON10, device.OFF)
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON11, device.OFF)
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON12, device.OFF)
        device.write_registers(self.jig_client, device.JIG_WRITE_CAP_ON13, device.OFF)

        # self.btn_stop.setEnabled(False)
        self.stopFlag = True
        if self.timer:
            self.timer.stop()
            self.timer = None
            self.timer = QtCore.QTimer()

    # Run Device (중간에 Run 을 눌러서 디바이스 시작시킴)
    def jig_device_run(self):
        device.jig_device_run(self.jig_client)

    def jig_device_stop(self):
        device.jig_device_stop(self.jig_client)

    def esc_device_run(self):
        device.esc_device_run(self.esc_client)

    def esc_device_stop(self):
        device.esc_device_stop(self.esc_client)


    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Escape:
            pass
        elif e.key() == Qt.Key_F:
            self.showFullScreen()
        elif e.key() == Qt.Key_N:
            self.showNormal()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    form = MainWindow()
    form.show()
    sys.exit(app.exec_())