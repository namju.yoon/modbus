#!/usr/bin/env python
# coding: utf-8

# 예제 내용
# * 기본 위젯을 사용하여 기본 창을 생성
# * 다양한 레이아웃 위젯 사용
import os
import sys
import pandas as pd
import time
import json, codecs
import serial
from datetime import datetime

from PyQt5.QtWidgets import QWidget, QLabel, QPushButton, QDesktopWidget, QMainWindow
from PyQt5.QtWidgets import QGroupBox, QVBoxLayout, QHBoxLayout, QGridLayout, QLineEdit
from PyQt5.QtWidgets import QApplication, QDialog, QStatusBar, QFileDialog, QCheckBox, QLCDNumber
from PyQt5 import QtCore
from PyQt5.QtCore import QDate, Qt
from PyQt5.QtGui import QPixmap
import pyqtgraph as pyGraph

from esc import semes as semes
from esc import scpicommon as scpi

from esc.esc_setup import SettingWin
from esc.esc_params import ESCParams, ESCParams1U, ESCParamsSCPI, ESCParamsSEMES
import esc.esc_serial as device
from esc.zoom import GraphWindow, ZoomWindow, InspectWindow

from ctypes import c_int16

GRAPH = 1
DATA = 2

RTU = 'RTU'
SCPI = 'SCPI'
SEMES = 'SEMES'
TCP = 'TCP'

import logging
FORMAT = ('%(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.INFO)

def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)

class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()

        if not os.path.exists("data"):
            logging.info("CTEATE :: data/")
            os.makedirs("data") 

        self.toggle = device.FORWARD

        self.faultmessage = {
            0 : "Fault01-0 : FAULT LEAK CURRENT OVER  ",
            1 : "Fault01-1 : FAULT HW OUT CURRENT OVER1  ",
            2 : "Fault01-2 : FAULT HW OUT CURRENT OVER2  ",
            3 : "Fault01-3 : FAULT HW OUT VOLTAGE OVER1  ",
            4 : "Fault01-4 : FAULT HW OUT VOLTAGE OVER2  ",
            5 : "Fault01-5 : FAULT EXT INTERLOCK  ",
            6 : "Fault01-6 : FAULT OUT CURRENT OVER1  ",
            7 : "Fault01-7 : FAULT OUT CURRENT OVER2  ",
            8 : "Fault01-8 : FAULT OUT VOLTAGE OVER1  ",
            9 : "Fault01-9 : FAULT OUT VOLTAGE OVER2  ",
            10: "Fault01-10 : FAULT COMMUNICATION ERROR  ",
            11: "Fault01-11 : FAULT TOO MANY ARC  ",
            12: "Fault01-12 : FAULT RO MIN   ",
            13: "Fault01-13 : RESONANT CURRENT OVER1  ",
            14: "Fault01-14 : RESONANT CURRENT OVER2  ",
        }

        self.stimer = None
        self.dtimer = None
        self.faulttimer = None
        self.toogletimer = None
        self.runstoptimer = None

        self.com_setting_flag = False
        self.com_open_flag = False
        self.run_flag = False
        self.ndp_flag = False
        self.channelnum = True

        self.voltage1 = 2000
        self.RUNSTOP_TIME = 5000

        ## 2022-02-14
        ## data only, or graph 
        self.graph_type = GRAPH

        self.client = None
        # self.client = 1

        self.ptype = RTU
        self.qr_client = None
        self.gen_port = None

        self.start_time = None

        self.graph_flag = False
        self.countN = 0

        self.data = {}
        self.time = 0

        self.view_vol1 = True
        self.view_cur1 = True
        self.view_vol2 = True
        self.view_cur2 = True

        self.view_vbia = False
        self.view_cps = True
        self.view_leak_cur1 = True
        self.view_leak_cur2 = True

        self.indexs = []
        self.tmarks = []
        self.vol1s = []
        self.cur1s = []
        self.vol2s = []
        self.cur2s = []
        self.vbias = []
        self.vcss = []
        self.icss = []
        self.cpss = []
        self.leak_cur1s = []
        self.leak_cur2s = []

        self.pen_vol1 = pyGraph.mkPen(width=2, color=(255, 0, 0))
        self.pen_cur1 = pyGraph.mkPen(width=2, color=(0, 0, 255))
        self.pen_vol2 = pyGraph.mkPen(width=2, color=(255, 191, 0))
        self.pen_cur2 = pyGraph.mkPen(width=2, color=(26, 175, 51))
        self.pen_vbia = pyGraph.mkPen(width=2, color=(0,0,128))
        self.pen_cps = pyGraph.mkPen(width=2, color=(108, 52, 131))
        self.pen_leak_cur1 = pyGraph.mkPen(width=2, color=(204, 0, 153))
        self.pen_leak_cur2 = pyGraph.mkPen(width=2, color=(102, 0, 204))

        self.temp_indexs = list(range(100))

        self.initUI()

    def eventFilter(self, watched, event):
        pass

    def displayUnitLayout(self, parent_layout, btn, cbx, label, unit):
        grp_sub = QGroupBox("")
        layout_sub = QGridLayout()
        grp_sub.setLayout(layout_sub)

        label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        unit.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)

        layout_sub.addWidget(btn, 0, 0, 1, 3)
        layout_sub.addWidget(cbx, 0, 4)
        layout_sub.addWidget(label, 1, 0, 1, 3)
        layout_sub.addWidget(unit, 1, 4)
        parent_layout.addWidget(grp_sub)
        return grp_sub

    def displayHorizontalLayout(self, parent_layout, glabel, btn, second=None, third=None):
        grp_sub = QGroupBox(glabel)
        layout_sub = QHBoxLayout()
        grp_sub.setLayout(layout_sub)
        layout_sub.addWidget(btn)
        if second:
            layout_sub.addWidget(second)
        if third:
            layout_sub.addWidget(third)
        parent_layout.addWidget(grp_sub)

    def displayVerticalLayout(self, parent_layout, glabel, btn, second=None, third=None):
        grp_sub = QGroupBox(glabel)
        layout_sub = QVBoxLayout()
        grp_sub.setLayout(layout_sub)
        layout_sub.addWidget(btn)
        if second:
            layout_sub.addWidget(second)
        if third:
            layout_sub.addWidget(third)
        parent_layout.addWidget(grp_sub)

    def displayGridLayout(self, parent_layout, glabel, edit1, label1, edit2, label2, edit3=None, label3=None):
        grp_sub = QGroupBox(glabel)
        layout_sub = QGridLayout()
        grp_sub.setLayout(layout_sub)

        layout_sub.addWidget(edit1, 0, 0)
        layout_sub.addWidget(label1, 0, 1)
        layout_sub.addWidget(edit2, 1, 0)
        layout_sub.addWidget(label2, 1, 1)

        if edit3:
            layout_sub.addWidget(edit3, 2, 0)
            layout_sub.addWidget(label3, 2, 1)

        parent_layout.addWidget(grp_sub)

    def initUI(self):
        self.setWindowTitle("PSTEK ELECTROSTATIC CHUCK MONITORING")
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width(), screensize.height()) 

        mainwidget = QWidget()                # 위젯의 인스턴스 생성만으로도 QMainWindow에 붙는다.
        self.setCentralWidget(mainwidget)

        ## Main Layout 설정
        self.main_layer = QVBoxLayout()
        mainwidget.setLayout(self.main_layer)

        bluefont = "color: #0000ff;"
        redfont = "color: #ff0000;"
        
        layout_setup = QHBoxLayout()
        self.main_layer.addLayout(layout_setup)
        
        ## replace #1st ###############
        # 전원 장치 연결
        self.btn_com_gen = QPushButton("CONNECT")
        self.btn_com_gen.clicked.connect(self.setting_device)
        self.label_gen_port = QLabel("N/A")
        self.label_ptype = QLabel("N/A")
        self.displayVerticalLayout(layout_setup, "Connect(485/ModBus)", 
                self.btn_com_gen, self.label_gen_port, self.label_ptype)

        # GET DATA
        self.btn_pulldata = QPushButton("PULL DATA")
        # self.btn_pulldata.setEnabled(False)
        self.btn_pulldata.setStyleSheet(bluefont)
        self.btn_pulldata.clicked.connect(self.start_pulldata)
        self.displayHorizontalLayout(layout_setup, "DATA", self.btn_pulldata)

        # 단말기 RUN/STOP
        self.btn_esc_run = QPushButton("RUN")
        self.btn_esc_run.setEnabled(False)
        self.btn_esc_run.setStyleSheet(bluefont)
        self.btn_esc_run.clicked.connect(self.esc_device_run)

        self.btn_esc_stop = QPushButton("STOP")
        self.btn_esc_stop.setStyleSheet(redfont)
        self.btn_esc_stop.clicked.connect(self.esc_device_stop)
        self.btn_esc_stop.setEnabled(False)
        self.displayVerticalLayout(layout_setup, "RUN/STOP", self.btn_esc_run, self.btn_esc_stop)

        # 단말기 WRITE
        self.btn_params = QPushButton("MODIFY PARAMS")
        self.btn_params.setEnabled(False)
        self.btn_params.clicked.connect(self.device_params_modify)
        self.auto_runstop = QCheckBox("Auto RUN/STOP")
        self.displayVerticalLayout(layout_setup, "Parameters", self.btn_params, self.auto_runstop)

        # Toggle 기능 
        self.btn_toggle = QPushButton("TOGGLE")
        self.btn_toggle.setEnabled(False)
        self.btn_toggle.clicked.connect(self.device_toggle)
        self.auto_toggle = QCheckBox("Auto Toggle")
        self.displayVerticalLayout(layout_setup, "TOGGLE", self.btn_toggle, self.auto_toggle)

        # Toggle 기능 
        # self.btn_num = QLineEdit("2")
        # label1 = QLabel("횟수/초")
        label1 = QLabel("횟수/초")
        self.toogle_time = QLineEdit("3")
        label2 = QLabel("초 (토글 시간)")
        self.runstop_time = QLineEdit("5")
        label3 = QLabel("초 (RUN/STOP 시간)")
        self.displayGridLayout(layout_setup, "데이터횟수", self.toogle_time, label2, self.runstop_time, label3)

        # STOP DATA
        self.btn_stopdata = QPushButton("STOP DATA")
        # self.btn_stopdata.setEnabled(False)
        self.btn_stopdata.setStyleSheet(bluefont)
        self.btn_stopdata.clicked.connect(self.stop_pulldata)
        self.displayHorizontalLayout(layout_setup, "STOP DATA", self.btn_stopdata)

        # 데이터 저장 / 데이터 불어오기
        self.btn_savedata = QPushButton("SAVE")
        # self.btn_savedata.setEnabled(False)
        self.btn_savedata.clicked.connect(self.save_data)
        self.btn_readdata = QPushButton("READ")
        self.btn_readdata.clicked.connect(self.read_data)
        self.displayVerticalLayout(layout_setup, "SAVE/READ", self.btn_savedata, self.btn_readdata)

        # ZOOM Function
        self.btn_zoomA = QPushButton("ZOOM IN")
        self.btn_zoomA.setEnabled(False)
        self.btn_zoomA.clicked.connect(self.zoom_graph)

        self.btn_inspect = QPushButton("출하검사")
        # self.btn_inspect.setEnabled(False)
        self.btn_inspect.clicked.connect(self.inspect_func)
        self.displayVerticalLayout(layout_setup, "ZOOM", self.btn_zoomA, self.btn_inspect)

        # Logo Image
        labelLogo = QLabel("")
        pixmap = QPixmap(resource_path("logo.png"))
        labelLogo.setAlignment(Qt.AlignRight)
        labelLogo.setPixmap(pixmap)
        layout_setup.addWidget(labelLogo)

        self.setup_Legend()
        self.setup_message()

    def setup_Legend(self):
        ## 2rd ########################
        # 그래프 범례
        grp_legend = QGroupBox("LEGEND")
        layout_legend = QHBoxLayout()
        grp_legend.setLayout(layout_legend)
        self.main_layer.addWidget(grp_legend)

        # 1 vol1 
        btn_vol1 = QPushButton("VOLTAGE1")
        btn_vol1.setStyleSheet("color: #ff0000;")
        btn_vol1.clicked.connect(lambda:self.draw_graph_each('VOLTAGE1', color=(255, 0, 0)))
        self.cbx_vol1 = QCheckBox("")
        self.cbx_vol1.setChecked(True)
        self.cbx_vol1.stateChanged.connect(lambda:self.checkBoxChanged("VOLTAGE1"))
        self.label_vol1 = QLabel("0")
        label_vol1_unit = QLabel("V")
        self.group_vol1 = self.displayUnitLayout(layout_legend, btn_vol1, self.cbx_vol1, self.label_vol1, label_vol1_unit)

        # 2 cur1 
        btn_cur1 = QPushButton("CURRENT1")
        btn_cur1.setStyleSheet("color: #0000ff;")
        btn_cur1.clicked.connect(lambda:self.draw_graph_each('CURRENT1', color=(0, 0, 255)))
        self.cbx_cur1 = QCheckBox("")
        self.cbx_cur1.setChecked(True)
        self.cbx_cur1.stateChanged.connect(lambda:self.checkBoxChanged("CURRENT1"))
        self.label_cur1 = QLabel("0")
        label_cur1_unit = QLabel("uA")
        self.group_cur1 = self.displayUnitLayout(layout_legend, btn_cur1, self.cbx_cur1, self.label_cur1, label_cur1_unit)

        # 3 vol2 
        btn_vol2 = QPushButton("VOLTAGE2")
        btn_vol2.setStyleSheet("color: #ffbf00;")
        btn_vol2.clicked.connect(lambda:self.draw_graph_each('VOLTAGE2', color=(255, 191, 0)))
        self.cbx_vol2 = QCheckBox("")
        self.cbx_vol2.setChecked(True)
        self.cbx_vol2.stateChanged.connect(lambda:self.checkBoxChanged("VOLTAGE2"))
        self.label_vol2 = QLabel("0")
        label_vol2_unit = QLabel("V")
        self.group_vol2 = self.displayUnitLayout(layout_legend, btn_vol2, self.cbx_vol2, self.label_vol2, label_vol2_unit)

        # 4 cur2 
        btn_cur2 = QPushButton("CURRENT2")
        btn_cur2.setStyleSheet("color: #1AAF33;")
        btn_cur2.clicked.connect(lambda:self.draw_graph_each('CURRENT2', color=(26, 175, 51)))
        self.cbx_cur2 = QCheckBox("")
        self.cbx_cur2.setChecked(True)
        self.cbx_cur2.stateChanged.connect(lambda:self.checkBoxChanged("CURRENT2"))
        self.label_cur2 = QLabel("0")
        label_cur2_unit = QLabel("uA")
        self.group_cur2 = self.displayUnitLayout(layout_legend, btn_cur2, self.cbx_cur2, self.label_cur2, label_cur2_unit)

        # 5 vbia 
        btn_vbia = QPushButton("VBIAS")
        btn_vbia.setStyleSheet("color: #000080;")
        btn_vbia.clicked.connect(lambda:self.draw_graph_each('VBIAS', color=(0,0,128)))
        self.cbx_vbia = QCheckBox("")
        self.cbx_vbia.setChecked(False)
        self.cbx_vbia.stateChanged.connect(lambda:self.checkBoxChanged("VBIAS"))
        self.label_vbia = QLabel("0")
        label_vbia_unit = QLabel("V")
        # self.group_vbia = self.displayUnitLayout(layout_legend, btn_vbia, self.cbx_vbia, self.label_vbia, label_vbia_unit)
        
        # 6 cps 
        btn_cps = QPushButton("CPS")
        btn_cps.setStyleSheet("color: #6C3483;")
        btn_cps.clicked.connect(lambda:self.draw_graph_each('CPS', color=(108, 52, 131)))
        self.cbx_cps = QCheckBox("")
        self.cbx_cps.setChecked(True)
        self.cbx_cps.stateChanged.connect(lambda:self.checkBoxChanged("CPS"))
        self.label_cps = QLabel("0")
        label_cps_unit = QLabel("pF")
        self.group_cps = self.displayUnitLayout(layout_legend, btn_cps, self.cbx_cps, self.label_cps, label_cps_unit)

        # 7_leak_cur1 
        btn_leakcurrent1 = QPushButton("LEAK CURR1")
        btn_leakcurrent1.setStyleSheet("color: #cc0099;")
        btn_leakcurrent1.clicked.connect(lambda:self.draw_graph_each('LEAK CURRENT1', color=(204, 0, 153)))
        self.cbx_leak_cur1 = QCheckBox("")
        self.cbx_leak_cur1.setChecked(True)
        self.cbx_leak_cur1.stateChanged.connect(lambda:self.checkBoxChanged("LEAK CURRENT1"))
        self.label_leakcurrent1 = QLabel("0")
        label_leakcurrent1_unit = QLabel("nA")
        self.group_leak1 = self.displayUnitLayout(layout_legend, btn_leakcurrent1, self.cbx_leak_cur1, self.label_leakcurrent1, label_leakcurrent1_unit)

        # 8_leak_cur2 
        btn_leakcurrent2 = QPushButton("LEAK CURR2")
        btn_leakcurrent2.setStyleSheet("color: #6600cc;")
        btn_leakcurrent2.clicked.connect(lambda:self.draw_graph_each('LEAK CURRENT2', color=(102, 0, 204)))
        self.cbx_leak_cur2 = QCheckBox("")
        self.cbx_leak_cur2.setChecked(True)
        self.cbx_leak_cur2.stateChanged.connect(lambda:self.checkBoxChanged("LEAK CURRENT2"))
        self.label_leakcurrent2 = QLabel("0")
        label_leakcurrent2_unit = QLabel("nA")
        self.group_leak2 = self.displayUnitLayout(layout_legend, btn_leakcurrent2, self.cbx_leak_cur2, self.label_leakcurrent2, label_leakcurrent2_unit)

        self.display_time = QLCDNumber()
        self.display_time.setDigitCount(8)
        self.display_time.setStyleSheet('background: white')
        timedisplay = '{:02d}:{:02d}:{:02d}'.format(
                (self.time // 60) // 60, (self.time // 60) % 60, self.time % 60)
        self.display_time.display(timedisplay)
        layout_legend.addWidget(self.display_time)
    
    def setup_message(self):
        ## 2-1rd Fault Message ########
        grp_fault = QGroupBox("에러 메시지")
        layout_fault = QHBoxLayout()
        grp_fault.setLayout(layout_fault)

        self.label_fault = QLabel("")
        layout_fault.addWidget(self.label_fault)

        self.label_start_time = QLabel("")
        layout_fault.addWidget(self.label_start_time)

        self.main_layer.addWidget(grp_fault)

        ## 3th ########################
        # 그래프 디스플레이
        grp_display = QGroupBox("GRAPH")
        layout_display = QHBoxLayout()

        self.graphWidget = pyGraph.PlotWidget()
        self.graphWidget.setBackground('w')
        self.graphWidget.setMouseEnabled(x=False, y=False)
        
        ## create mainAxis
        self.axisA = self.graphWidget.plotItem
        self.axisA.setLabels(left='Voltage')
        self.axisA.showAxis('right')

        ## Second ViewBox
        self.axisB = pyGraph.ViewBox()
        self.axisA.scene().addItem(self.axisB)
        self.axisA.getAxis('right').linkToView(self.axisB)
        self.axisB.setXLink(self.axisA)
        self.axisA.getAxis('right').setLabel('Current', color='#0000ff')

        ## create Third ViewBox. 
        self.axisC = pyGraph.ViewBox()
        ax3 = pyGraph.AxisItem('right')
        self.axisC.setYRange(0, 10000)
        self.axisA.layout.addItem(ax3, 2, 3)
        self.axisA.scene().addItem(self.axisC)
        ax3.linkToView(self.axisC)
        self.axisC.setXLink(self.axisA)
        ax3.setLabel('Vais/CPS', color='#000080')

        ## create Forth ViewBox. 
        self.axisD = pyGraph.ViewBox()
        ax4 = pyGraph.AxisItem('right')
        self.axisA.layout.addItem(ax4, 2, 4)
        self.axisA.scene().addItem(self.axisD)
        ax4.linkToView(self.axisD)
        self.axisD.setXLink(self.axisA)
        ax4.setLabel('Leak Current', color='#6C3483')

        self.updateViews()
        self.axisA.vb.sigResized.connect(self.updateViews)

        # mainAxis
        self.graph_vol1 = self.axisA.plot(self.indexs, 
                    self.vol1s, pen=self.pen_vol1, name="Voltage1s")
        self.graph_vol2 = self.axisA.plot(self.indexs, 
                    self.vol2s, pen=self.pen_vol2, name="Voltage2s")
        
        # axisB
        self.itemCurrent1 = pyGraph.PlotCurveItem(self.indexs, 
                    self.cur1s, pen=self.pen_cur1, name="Current1s")
        self.graph_cur1s = self.axisB.addItem(self.itemCurrent1)
        
        self.itemCurrent2 = pyGraph.PlotCurveItem(self.indexs, 
                    self.cur2s, pen=self.pen_cur2, name="Current2s")
        self.graph_cur2s = self.axisB.addItem(self.itemCurrent2)

        # axisC
        # self.itemVbia = pyGraph.PlotCurveItem(self.indexs, 
        #             self.vbias, pen=self.pen_vbia, name="Vbias")
        # self.graph_vbias = self.axisC.addItem(self.itemVbia)

        self.itemCps = pyGraph.PlotCurveItem(self.indexs, 
                    self.cpss, pen=self.pen_cps, name="CPS")
        self.graph_cpss = self.axisC.addItem(self.itemCps)
        
        # axisD
        self.itemLeakCur1 = pyGraph.PlotCurveItem(self.indexs, 
                    self.leak_cur1s, pen=self.pen_leak_cur1, name="Leak_Cur1")
        self.graph_leak_cur1s = self.axisD.addItem(self.itemLeakCur1)

        self.itemLeakCur2 = pyGraph.PlotCurveItem(self.indexs, 
                    self.leak_cur2s, pen=self.pen_leak_cur2, name="Leak_Cur2")
        self.graph_leak_cur2s = self.axisD.addItem(self.itemLeakCur2)

        layout_display.addWidget(self.graphWidget)
        grp_display.setLayout(layout_display)
        self.main_layer.addWidget(grp_display)
    
        ## 4th ########################
        # Status
        self.statusbar = QStatusBar()
        self.setStatusBar(self.statusbar)
        self.statusbar.setObjectName("statusbar")
        
        self.statusmessage = 'PSTEK, {},  {}'
        now = QDate.currentDate()
        displaymessage = self.statusmessage.format(now.toString(Qt.ISODate), 'PSTEK is aiming for a global leader. !!')
        self.statusbar.showMessage(displaymessage)

        # self.showFullScreen()

    def updateViews(self):
        # global self.axisA, self.axisB, self.axisC
        self.axisB.setGeometry(self.axisA.vb.sceneBoundingRect())
        self.axisC.setGeometry(self.axisA.vb.sceneBoundingRect())
        self.axisD.setGeometry(self.axisA.vb.sceneBoundingRect())
        self.axisB.linkedViewChanged(self.axisA.vb, self.axisB.XAxis)
        self.axisC.linkedViewChanged(self.axisA.vb, self.axisC.XAxis)
        self.axisD.linkedViewChanged(self.axisA.vb, self.axisD.XAxis)

    # 전원 장치와 송신을 위한 설정
    def device_params_modify(self):
        Dialog = QDialog()
        if self.ptype == RTU or self.ptype == TCP:
            if self.channelnum:
                self.data = device.read_setting_value(self.client, self.ptype)
                logging.info("RTU :: 2CH :: ", self.data)
                dialog = ESCParams(Dialog, self.client, self.ptype, self.data, self.ndp_flag)
            else:
                self.data = device.read_setting_value_1u(self.client, self.ptype)
                logging.info("RTU :: 1CH :: ", self.data)
                dialog = ESCParams1U(Dialog, self.client, self.ptype, self.data, self.ndp_flag)

        elif self.ptype == SCPI:
                self.data = scpi.read_configure(self.client)
                self.toggle = self.data['TOGGLE']
                logging.info(f'device_params_modify :: {self.data}')
                dialog = ESCParamsSCPI(self, Dialog, self.client, self.data)

        elif self.ptype == SEMES:
                self.voltage1 = semes.semes_get_measure(self.client, 'VOLTAGE')
                # logging.info('device_params_modify', self.voltage1)
                dialog = ESCParamsSEMES(Dialog, self.client, self.voltage1)
        else:
            self.data = device.read_setting_value_1u(self.client, self.ptype)
            dialog = ESCParams1U(Dialog, self.client, self.ptype, self.data, self.ndp_flag)

        dialog.show()
        response = dialog.exec_()

    def feedBack(self, ptype):
        while True:
            try:
                feedback = self.client.readline()
                if feedback:
                    logging.info(f"{ptype} :: {feedback}")
                    break
            except KeyboardInterrupt:
                logging.info(f"KeyboardInterrupt :: {ptype}")
                break
            except Exception as e:
                logging.info(f"Exception ERROR !! :: {ptype} :: {e}")

    # 전원 장치 Toggle
    def device_toggle(self):
        if self.ptype == SCPI:
            if self.toggle == device.FORWARD:
                self.toggle = device.REVERSE
                command = scpi.scpi_command_2ch('SET_TOGGLE_FWD')
                logging.info(f"Toggle :: {command}")
                self.client.write(command)
                # self.feedBack('REVERSE')

            else:
                self.toggle = device.FORWARD
                command = scpi.scpi_command_2ch('SET_TOGGLE_BACK')
                logging.info(f"Toggle :: {command}")
                self.client.write(command)
                # self.feedBack('FORWARD')

        elif self.ptype == SEMES:
            if self.toggle == device.FORWARD:
                self.toggle = device.REVERSE
                command = semes.semes_write('REVERSEOFF')
                logging.info(f"Toggle :: {command}")
                self.client.write(command)
                self.feedBack('REVERSEOFF')

            else:
                self.toggle = device.FORWARD
                command = semes.semes_write('REVERSEON')
                logging.info(f"Toggle :: {command}")
                self.client.write(command)
                self.feedBack('REVERSEON')

        else:
            if self.toggle == device.FORWARD:
                self.toggle = device.REVERSE
                if self.channelnum:
                    device.write_registers(self.client, device.WRITE_TOGGLE, device.REVERSE, self.ptype)
                else:
                    device.write_registers(self.client, device.WRITE_TOGGLE_1U, device.REVERSE, self.ptype)

            else:
                self.toggle = device.FORWARD
                if self.channelnum:
                    device.write_registers(self.client, device.WRITE_TOGGLE, device.FORWARD, self.ptype)
                else:
                    device.write_registers(self.client, device.WRITE_TOGGLE_1U, device.FORWARD, self.ptype)

    # Bit return 함수 
    def bit_check(self, x):
        data = []
        for idx in range(15):
            if (x & (1<<idx)):
                data.append(idx)
        return data

    # Falult 오류 확인 
    def fault_check(self):
        x = device.read_fault(self.client, self.ptype)
        faults = self.bit_check(x)

        if faults:
            self.faulttimer.stop()
            self.dtimer.stop()
            self.toogletimer.stop()

            message = ""

            for fault in faults:
                message += self.faultmessage[fault]

            self.label_fault.setText(message)

    # 데이터를 가져오는 오는 시작점
    def start_pulldata(self):
        if not self.client:
            self.setting_device()

        # 데이터를 가져오기 시작하면 STOP 버턴 활성화
        
        # self.btn_savedata.setEnabled(True)
        if not self.dtimer:
            self.dtimer = QtCore.QTimer()
            self.stimer = QtCore.QTimer()
            self.faulttimer = QtCore.QTimer()
            self.toggletimer = QtCore.QTimer()

        self.start_time = datetime.now()
        start_time = datetime.strftime(self.start_time, '%Y-%m-%d %H:%M:%S')
        self.label_start_time.setText(start_time)

        # num = int(self.btn_num.text())
        # DATA_PERIOD = int(1000/num)
        DATA_PERIOD = 800

        toogle_time = int(self.toogle_time.text())

        if self.auto_toggle.isChecked():
            TOOGLE_TIME = toogle_time * 1000
            self.toogletimer.setInterval(TOOGLE_TIME)
            self.toogletimer.timeout.connect(self.device_toggle)
            self.toogletimer.start()

        ## AUTO RUNSTOP 
        runstop_time = int(self.runstop_time.text())
        if self.auto_runstop.isChecked():
            self.RUNSTOP_TIME = runstop_time * 1000
            self.runstop = True
            self.runstoptimer.setInterval(self.RUNSTOP_TIME)
            self.runstoptimer.timeout.connect(self.device_auto_runstop)
            self.runstoptimer.start()

        self.stimer.setInterval(1000)
        self.stimer.timeout.connect(self.get_time)
        self.stimer.start()

        # setting timer
        self.dtimer.setInterval(DATA_PERIOD)
        self.dtimer.timeout.connect(self.get_data)
        self.dtimer.start()

        if self.ptype == RTU:
            self.faulttimer.setInterval(10000)
            self.faulttimer.timeout.connect(self.fault_check)
            self.faulttimer.start()

    # 데이터 가져 오기
    def get_data(self):
        if self.ptype == RTU or self.ptype == TCP:
            if self.channelnum:
                result = device.read_data(self.client, self.ptype)
                if result:
                    self.countN = self.countN  + 1
                    tm = time.time()

                    # 데이터 값을 Array 에 저장하기 
                    self.indexs.append(self.countN)
                    self.tmarks.append(tm)
                    voltage1 = c_int16(result[0]).value
                    voltage2 = c_int16(result[2]).value

                    self.vol1s.append(voltage1)
                    self.cur1s.append(result[1])
                    self.vol2s.append(voltage2)
                    self.cur2s.append(result[3])
                    self.vbias.append(result[4])
                    self.vcss.append(result[5])
                    self.icss.append(result[6])
                    self.cpss.append(result[7])
                    try:
                        leak_cur1 = result[8] * 10 
                        leak_cur2 = result[9] * 10
                    except Exception as e:
                        leak_cur1 = 0
                        leak_cur2 = 0

                    self.leak_cur1s.append(leak_cur1)
                    self.leak_cur2s.append(leak_cur2)

                    if self.countN % 5 == 1:
                        # Label 에 데이터 보여주기
                        self.label_vol1.setText(str(voltage1))
                        self.label_cur1.setText(str(result[1]))
                        self.label_vol2.setText(str(voltage2))
                        self.label_cur2.setText(str(result[3]))
                        
                        self.label_vbia.setText(str(result[4]))
                        self.label_cps.setText(str(result[7]))
                        self.label_leakcurrent1.setText(str(leak_cur1))
                        self.label_leakcurrent2.setText(str(leak_cur2))
            else:
                result = device.read_data_1u(self.client, self.ptype)
                if result:
                    self.countN = self.countN  + 1
                    tm = time.time()

                    # 데이터 값을 Array 에 저장하기 
                    self.indexs.append(self.countN)
                    self.tmarks.append(tm)
                    voltage1 = c_int16(result[0]).value

                    self.vol1s.append(voltage1)
                    self.cur1s.append(result[1])
                    self.vol2s.append(0)
                    self.cur2s.append(0)
                    self.vbias.append(0)
                    self.vcss.append(0)
                    self.icss.append(0)
                    self.cpss.append(0)

                    try:
                        leak_cur1 = result[2] * 10 
                        leak_cur2 = 0
                    except Exception as e:
                        leak_cur1 = 0
                        leak_cur2 = 0

                    self.leak_cur1s.append(leak_cur1)
                    self.leak_cur2s.append(leak_cur2)

                if self.countN % 5 == 1:
                    # Label 에 데이터 보여주기
                    self.label_vol1.setText(str(voltage1))
                    self.label_cur1.setText(str(result[1]))
                    # self.label_vol2.setText(str(voltage2))
                    # self.label_cur2.setText(str(result[3]))
                    
                    # self.label_vbia.setText(str(result[4]))
                    # self.label_cps.setText(str(result[7]))
                    self.label_leakcurrent1.setText(str(leak_cur1))
                    # self.label_leakcurrent2.setText(str(leak_cur2))

        elif self.ptype == SCPI:
            command = scpi.scpi_command_2ch('ALL_DATA')
            result = scpi.scpi_get_measure_2ch(self.client, command)
            if result:
                self.countN = self.countN  + 1
                tm = time.time()
                logging.info(f"ALL_DATA :: {result}")
                voltage1 = int(result[0])
                voltage2 = int(result[1])
                current1 = int(result[2])
                current2 = int(result[3])

                command = scpi.scpi_command_2ch('READ_LEAKAGE')
                result = scpi.scpi_get_measure_2ch(self.client, command)
                leak_cur1 = int(result[0]) * 10
                leak_cur2 = int(result[1]) * 10

                command = scpi.scpi_command_2ch('READ_CAPACITOR')
                result = scpi.scpi_get_measure_2ch(self.client, command)
                cps = int(result[0])

                # 데이터 값을 Array 에 저장하기 
                self.indexs.append(self.countN)
                self.tmarks.append(tm)

                self.vol1s.append(voltage1)
                self.cur1s.append(current1)
                self.vol2s.append(voltage2)
                self.cur2s.append(current2)
                self.vbias.append(0)
                self.vcss.append(0)
                self.icss.append(0)
                self.cpss.append(cps)
                self.leak_cur1s.append(leak_cur1)
                self.leak_cur2s.append(leak_cur2)

                if self.countN % 5 == 1:
                    # Label 에 데이터 보여주기
                    self.label_vol1.setText(str(voltage1))
                    self.label_cur1.setText(str(current1))
                    self.label_vol2.setText(str(voltage2))
                    self.label_cur2.setText(str(current2))
                    
                    # self.label_vbia.setText(str(result[4]))
                    self.label_cps.setText(str(cps))
                    self.label_leakcurrent1.setText(str(leak_cur1))
                    self.label_leakcurrent2.setText(str(leak_cur2))


        elif self.ptype == SEMES:
            self.countN = self.countN  + 1
            tm = time.time()
            self.indexs.append(self.countN)
            self.tmarks.append(tm)
            voltage1 = semes.semes_get_measure(self.client, 'VOLTAGE')
            current1 = semes.semes_get_measure(self.client, 'CURRENT+')
            # logging.info(voltage1, current1)

            self.vol1s.append(voltage1)
            self.cur1s.append(current1)

            self.vol2s.append(0)
            self.cur2s.append(0)
            self.vbias.append(0)
            self.vcss.append(0)
            self.icss.append(0)
            self.cpss.append(0)
            self.leak_cur1s.append(0)
            self.leak_cur2s.append(0)

            if self.countN % 5 == 1:
                # Label 에 데이터 보여주기
                self.label_vol1.setText(str(voltage1))
                self.label_cur1.setText(str(current1))
                # self.label_leakcurrent1.setText(str(result[2]))

        else:
            pass

        ## GRAPH
        if self.graph_type == GRAPH:
            if self.countN % 5 == 2:
                # 데이터 값 임시 값에 저장하기 Display 용
                self.draw_graph()

        # if self.countN % 7200 == 0:
        if self.countN and self.countN % 7200 == 0:
            self.dtimer.stop()
            self.regular_save_json()
            self.reset_data()
            self.dtimer.start()

    def get_time(self):
        self.time += 1
        timedisplay = '{:02d}:{:02d}:{:02d}'.format((self.time // 60) // 60, 
                        (self.time // 60) % 60, self.time % 60)
        self.display_time.display(timedisplay)

    # draw_graph using all graph
    def draw_graph(self):
        self.updateViews()
        self.axisA.vb.sigResized.connect(self.updateViews)

        # VOLTAGE 1
        if self.view_vol1:
            if self.countN >= 100:
                temp_vol1s = self.vol1s[-100:]
                temp_indexs = self.temp_indexs
            else:
                temp_vol1s = self.vol1s
                temp_indexs = list(range(len(self.vol1s)))

            self.graph_vol1.setData(temp_indexs, temp_vol1s)
            # self.graph_vol1 = self.axisA.plot(temp_indexs, 
            #         temp_vol1s, pen=self.pen_vol1, name="Voltage1s")
            # self.graph_vol1.show()
        else:
            self.graph_vol1.hide()

        # CURRENT 1
        if self.view_cur1:
            if self.countN >= 100:
                temp_cur1s = self.cur1s[-100:]
                temp_indexs = self.temp_indexs
            else:
                temp_cur1s = self.cur1s
                temp_indexs = list(range(len(self.cur1s)))
            self.itemCurrent1.setData(temp_indexs, temp_cur1s)
            # self.graph_cur1s = self.axisB.addItem(pyGraph.PlotCurveItem(temp_indexs, 
            #         temp_cur1s, pen=self.pen_cur1, name="Current1s"))
        else:
            self.itemCurrent1.hide()

        # VOLTAGE 2
        if self.view_vol2 and self.channelnum:
            if self.countN >= 100:
                temp_vol2s = self.vol2s[-100:]
                temp_indexs = self.temp_indexs
            else:
                temp_vol2s = self.vol2s
                temp_indexs = list(range(len(self.vol2s)))

            self.graph_vol2.setData(temp_indexs, temp_vol2s)
            # self.graph_vol2 = self.axisA.plot(temp_indexs, 
            #         temp_vol2s, pen=self.pen_vol2, name="Voltage2s")
            # self.graph_vol2.show()
        else:
            self.graph_vol2.hide()

        # CURRENT 2 
        if self.view_cur2 and self.channelnum:
            self.itemCurrent2.clear()

            if self.countN >= 100:
                temp_cur2s = self.cur2s[-100:]
                temp_indexs = self.temp_indexs
            else:
                temp_cur2s = self.cur2s
                temp_indexs = list(range(len(self.cur2s)))

            self.itemCurrent2.setData(temp_indexs, temp_cur2s)
            # self.graph_cur2s = self.axisB.addItem(pyGraph.PlotCurveItem(temp_indexs, 
            #         temp_cur2s, pen=self.pen_cur2, name="Current2s"))
            # self.graph_cur2s.show()

        else:
            self.itemCurrent2.hide()

        # VBIAS
        # if self.view_vbia and self.channelnum:
        #     self.itemVbia.clear()

        #     if self.countN >= 100:
        #         temp_vbias = self.vbias[-100:]
        #         temp_indexs = self.temp_indexs
        #     else:
        #         temp_vbias = self.vbias
        #         temp_indexs = list(range(len(self.vbias)))

        #     self.itemVbia = pyGraph.PlotCurveItem(temp_indexs, 
        #             temp_vbias, pen=self.pen_vbia, name="Vbias")
        #     self.graph_vbias = self.axisC.addItem(self.itemVbia)
        #     # self.graph_vbias.show()
        # else:
        #     self.view_vbia = False 
        #     # self.axisC.removeItem(self.itemVbia)
        #     self.axisC.hide()

        # CPS
        if self.view_cps and self.channelnum:
            if self.countN >= 100:
                temp_cpss = self.cpss[-100:]
                temp_indexs = self.temp_indexs
            else:
                temp_cpss = self.cpss
                temp_indexs = list(range(len(self.cpss)))
                
            self.itemCps.setData(temp_indexs, temp_cpss)
            # self.itemCps = pyGraph.PlotCurveItem(temp_indexs, 
            #         temp_cpss, pen=self.pen_cps, name="CPS")
            # self.graph_cpss = self.axisC.addItem(self.itemCps)
            # self.graph_cpss.show()

        else:
            self.itemCps.hide()

        # Leak_Cur1
        if self.view_leak_cur1 and self.channelnum:
            if self.countN >= 100:
                temp_leak_cur1s = self.leak_cur1s[-100:]
                temp_indexs = self.temp_indexs
            else:
                temp_leak_cur1s = self.leak_cur1s
                temp_indexs = list(range(len(self.leak_cur1s)))

            self.itemLeakCur1.setData(temp_indexs, temp_leak_cur1s)
            # self.itemLeakCur1 = pyGraph.PlotCurveItem(temp_indexs, 
            #         temp_leak_cur1s, pen=self.pen_leak_cur1, name="Leak_Cur1")
            # self.graph_leak_cur1s = self.axisD.addItem(self.itemLeakCur1)
            # self.graph_leak_cur1s.show()

        else:
            self.itemLeakCur1.hide()


        if self.view_leak_cur2 and self.channelnum:
            self.itemLeakCur2.clear()

            if self.countN >= 100:
                temp_leak_cur2s = self.leak_cur2s[-100:]
                temp_indexs = self.temp_indexs
            else:
                temp_leak_cur2s = self.leak_cur2s
                temp_indexs = list(range(len(self.leak_cur2s)))

            self.itemLeakCur2.setData(temp_indexs, temp_leak_cur2s)
            # self.itemLeakCur2 = pyGraph.PlotCurveItem(temp_indexs, 
            #         temp_leak_cur2s, pen=self.pen_leak_cur2, name="Leak_Cur2")
            # self.graph_leak_cur2s = self.axisD.addItem(self.itemLeakCur2)
            # self.graph_leak_cur2s.show()
        else:
            self.itemLeakCur2.hide()

    def draw_graph_full(self):
        self.updateViews()
        self.axisA.vb.sigResized.connect(self.updateViews)

        # VOLTAGE 1
        if self.view_vol1:
            self.graph_vol1.clear()

            temp_vol1s = self.vol1s
            temp_indexs = list(range(len(self.vol1s)))
            self.graph_vol1 = self.axisA.plot(temp_indexs, 
                    temp_vol1s, pen=self.pen_vol1, name="Voltage1s")
            self.graph_vol1.show()
        else:
            self.graph_vol1.hide()

        # CURRENT 1
        if self.view_cur1:
            self.itemCurrent1.clear()

            temp_cur1s = self.cur1s
            temp_indexs = list(range(len(self.cur1s)))

            self.graph_cur1s = self.axisB.addItem(pyGraph.PlotCurveItem(temp_indexs, 
                    temp_cur1s, pen=self.pen_cur1, name="Current1s"))
            # self.graph_cur1s.show()

        else:
            self.axisB.hide()

        # VOLTAGE 2
        if self.view_vol2 and self.channelnum:
            self.graph_vol2.clear()

            temp_vol2s = self.vol2s
            temp_indexs = list(range(len(self.vol2s)))

            self.graph_vol2 = self.axisA.plot(temp_indexs, 
                    temp_vol2s, pen=self.pen_vol2, name="Voltage2s")
            self.graph_vol2.show()
        else:
            self.graph_vol2.hide()

        # CURRENT 2 
        if self.view_cur2 and self.channelnum:
            self.itemCurrent2.clear()

            temp_cur2s = self.cur2s
            temp_indexs = list(range(len(self.cur2s)))

            self.graph_cur2s = self.axisB.addItem(pyGraph.PlotCurveItem(temp_indexs, 
                    temp_cur2s, pen=self.pen_cur2, name="Current2s"))
            # self.graph_cur2s.show()

        else:
            self.axisB.hide()

        # # VBIAS
        # if self.view_vbia and self.channelnum:
        #     self.itemVbia.clear()

        #     temp_vbias = self.vbias
        #     temp_indexs = list(range(len(self.vbias)))

        #     self.itemVbia = pyGraph.PlotCurveItem(temp_indexs, 
        #             temp_vbias, pen=self.pen_vbia, name="Vbias")
        #     self.graph_vbias = self.axisC.addItem(self.itemVbia)
        #     # self.graph_vbias.show()

        # else:
        #     self.view_vbia = False 
        #     # self.axisC.removeItem(self.itemVbia)
        #     self.axisC.hide()

        # CPS
        if self.view_cps and self.channelnum:
            self.itemCps.clear()
            temp_cpss = self.cpss
            temp_indexs = list(range(len(self.cpss)))
            # logging.info("CPS ::", temp_cpss)
            self.itemCps = pyGraph.PlotCurveItem(temp_indexs, 
                    temp_cpss, pen=self.pen_cps, name="CPS")
            self.graph_cpss = self.axisC.addItem(self.itemCps)
            # self.graph_cpss.show()

        else:
            self.view_cps = False  
            # self.axisC.removeItem(self.itemCps)
            self.axisC.hide()

        # Leak_Cur1
        if self.view_leak_cur1 and self.channelnum:
            self.itemLeakCur1.clear()

            temp_leak_cur1s = self.leak_cur1s
            temp_indexs = list(range(len(self.leak_cur1s)))

            self.itemLeakCur1 = pyGraph.PlotCurveItem(temp_indexs, 
                    temp_leak_cur1s, pen=self.pen_leak_cur1, name="Leak_Cur1")
            self.graph_leak_cur1s = self.axisD.addItem(self.itemLeakCur1)
            # self.graph_leak_cur1s.show()

        else:
            self.view_leak_cur1 = False 
            # self.axisD.removeItem(self.itemLeakCur1)
            self.axisD.hide()


        if self.view_leak_cur2 and self.channelnum:
            self.itemLeakCur2.clear()

            temp_leak_cur2s = self.leak_cur2s
            temp_indexs = list(range(len(self.leak_cur2s)))

            self.itemLeakCur2 = pyGraph.PlotCurveItem(temp_indexs, 
                    temp_leak_cur2s, pen=self.pen_leak_cur2, name="Leak_Cur2")
            self.graph_leak_cur2s = self.axisD.addItem(self.itemLeakCur2)
            # self.graph_leak_cur2s.show()

        else:
            self.view_leak_cur2 = False 
            # self.axisD.removeItem(self.itemLeakCur2)
            self.axisD.hide()

    # Run Device (중간에 Run 을 눌러서 디바이스 시작시킴)
    def esc_device_run(self):
        self.run_flag = True 
        # 전원 장치를 시작하고, 데이터를 읽어옴
        if self.ptype == SEMES:
            logging.info("SEMES RUN")
            self.client.readline()

            command = semes.semes_write('RUN')
            self.client.write(command)
            self.feedBack('RUN')
            # time.sleep(2)

        elif self.ptype == SCPI:
            logging.info("SCPI RUN")
            self.data = scpi.read_configure(self.client)
            self.toggle = self.data['TOGGLE']
            logging.info(f"esc_device_run :: {self.data}")

            command = scpi.scpi_command_2ch('RUN')
            self.client.write(command)

        else:
            if self.channelnum: 
                self.data = device.read_setting_value(self.client, self.ptype)
            else:
                self.data = device.read_setting_value_1u(self.client, self.ptype)

            if self.data:
                logging.info(self.data)
                self.toggle = self.data['rd_toggle']

            device.write_registers(self.client, 
                device.WRITE_BIT_POWER_ON, device.RUN_VALUE, self.ptype)

        self.btn_toggle.setEnabled(True)
        self.btn_esc_stop.setEnabled(True)
        self.btn_stopdata.setEnabled(True)

        now = QDate.currentDate()
        displaymessage = self.statusmessage.format(now.toString(Qt.ISODate), "DEVICE RUN")
        self.statusbar.showMessage(displaymessage)

        if not self.dtimer:
            # setting timer
            self.start_pulldata()

    ## AUTO RUN STOP
    def device_auto_runstop(self):
        self.runstoptimer.stop()
        self.runstoptimer = None 

        if self.runstop:
            device.write_registers(self.client, 
                    device.WRITE_BIT_POWER_ON, device.STOP_VALUE, self.ptype)
            self.runstop = False
        else:
            device.write_registers(self.client, 
                device.WRITE_BIT_POWER_ON, device.RUN_VALUE, self.ptype)
            self.runstop = True 

        self.runstoptimer = QtCore.QTimer()
        self.runstoptimer.setInterval(self.RUNSTOP_TIME)
        self.runstoptimer.timeout.connect(self.device_auto_runstop)
        self.runstoptimer.start()

    # 단말기 STOP
    def esc_device_stop(self):
        if self.ptype == SCPI:
            self.btn_toggle.setEnabled(False)
            command = scpi.scpi_command_2ch('STOP')
            self.client.write(command)
            # self.feedBack('STOP')

        elif self.ptype == SEMES:
            self.btn_toggle.setEnabled(False)
            command = semes.semes_write('STOP')
            self.client.write(command)
            self.feedBack('STOP')

        else:
            self.btn_toggle.setEnabled(False)
            device.write_registers(self.client, 
                    device.WRITE_BIT_POWER_ON, device.STOP_VALUE, self.ptype)

        try:
            self.toogletimer.stop()
            self.toogletimer = None 

            self.runstoptimer.stop()
            self.runstoptimer = None 
        except Exception as e:
            pass

    # 데이터 전송 중단
    def stop_pulldata(self):
        self.run_flag = False

        self.draw_graph()

        self.btn_savedata.setEnabled(True)
        self.btn_pulldata.setEnabled(True)
        self.btn_esc_run.setEnabled(True)

        self.btn_zoomA.setEnabled(True)
        # self.btn_zoom6.setEnabled(True)
        # self.btn_esc_stop.setEnabled(False)
        
        try:
            if self.dtimer:
                self.dtimer.stop()
                self.dtimer = None
                self.stimer.stop()
                self.stimer = None
                self.faulttimer.stop()
                self.faulttimer = None
                self.toogletimer.stop()
                self.toogletimer = None
                self.runstoptimer.stop()
                self.runstoptimer = None 
        except Exception as e:
            pass

        
        now = QDate.currentDate()
        displaymessage = self.statusmessage.format(now.toString(Qt.ISODate), "DEVICE STOP")
        self.statusbar.showMessage(displaymessage)

    # Keyboard Event
    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Escape:
            pass
        elif e.key() == Qt.Key_F:
            self.showFullScreen()
        elif e.key() == Qt.Key_N:
            self.showNormal()

    # RESET 
    def reset_data(self):
        # Reser Data
        self.countN = 0

        self.indexs = []
        self.tmarks = []
        self.vol1s = []
        self.cur1s = []
        self.vol2s = []
        self.cur2s = []
        self.vbias = []
        self.vcss = []
        self.icss = []
        self.cpss = []
        self.leak_cur1s = []
        self.leak_cur2s = []

       
        # Flag Value Reset
        self.view_vol1 = True
        self.view_cur1 = True
        self.view_vol2 = True
        self.view_cur2 = True
        self.view_vbia = True
        self.view_cps = True
        self.view_leak_cur1 = True
        self.view_leak_cur2 = True

        # Reset 그림 그리기
        self.draw_graph()

    # Graph 개별 그래프 
    def draw_graph_each(self, category, color):
        if self.graph_flag == False:
            self.graph_flag = True
            Dialog = QDialog()
            if category == 'VOLTAGE1':
                values = self.vol1s
            elif category == 'CURRENT1':
                values = self.cur1s
            elif category == 'VOLTAGE2':
                values = self.vol2s
            elif category == 'CURRENT2':
                values = self.cur2s
            elif category == 'VBIAS':
                values = self.vbias
            elif category == 'CPS':
                values = self.cpss
            elif category == 'LEAK CURRENT1':
                values = self.leak_cur1s
            elif category == 'LEAK CURRENT2':
                values = self.leak_cur2s

            dialog = GraphWindow(Dialog, category, color, self.indexs, values)
            dialog.show()
            response = dialog.exec_()

            if response == QDialog.Accepted or response == QDialog.Rejected:
                self.graph_flag = False

    # zoom_graph
    def zoom_graph(self):
        Dialog = QDialog()
        # logging.info("zoom_graph", len(self.indexs), len(self.vol1s))
        if self.channelnum:
            dialog = ZoomWindow(Dialog, self.indexs, self.vol1s, self.cur1s, self.vol2s, self.cur2s, self.vbias, self.cpss, self.leak_cur1s, self.leak_cur2s)
        else:
            dummy_list = []
            for tm in self.indexs:
                dummy_list.append(0)
            dialog = ZoomWindow(Dialog, self.indexs, self.vol1s, self.cur1s, dummy_list, dummy_list, dummy_list, dummy_list, dummy_list, dummy_list)
        
        dialog.show()
        response = dialog.exec_()

        if response == QDialog.Accepted or response == QDialog.Rejected:
            self.graph_flag = False

    # 전원 장치와 송신을 위한 설정
    def setting_device(self):
        if self.com_open_flag == False:
            Dialog = QDialog()
            self.com_open_flag == True
            dialog = SettingWin(Dialog)
            dialog.show()
            response = dialog.exec_()

            # OK 를 하면 설정 값을 읽어와서 통신을 한다.
            if response == QDialog.Accepted:
                self.ndp_flag = dialog.ndp_flag
                self.channelnum = dialog.channelnum
                logging.info(f"self.ndp_flag  ::  {self.ndp_flag}")
                logging.info(f"self.channelnum  ::  {self.channelnum}")

                if not self.channelnum:
                    self.group_vol2.hide()
                    self.group_cur2.hide()
                    # self.group_vbia.hide()
                    self.group_cps.hide()
                    self.group_leak2.hide()

                if dialog.selected == 'RTU':
                    gen_port = dialog.gen_port
                    com_speed = dialog.com_speed
                    com_data = dialog.com_data
                    com_parity = dialog.com_parity
                    com_stop = dialog.com_stop
                    self.com_open_flag = False

                    self.client = device.connect_rtu(
                        port=gen_port, ptype='rtu',
                        speed=com_speed, bytesize=com_data, 
                        parity=com_parity, stopbits=com_stop
                    )
                    self.ptype = RTU

                elif dialog.selected == SCPI:
                    gen_port = dialog.gen_port
                    com_speed = dialog.com_speed
                    com_data = dialog.com_data
                    com_parity = dialog.com_parity
                    com_stop = dialog.com_stop
                    self.com_open_flag = False

                    self.client = serial.Serial(
                        port=gen_port,
                        baudrate=com_speed,
                        parity=serial.PARITY_NONE,
                        stopbits=serial.STOPBITS_ONE,
                        bytesize=serial.EIGHTBITS,
                        timeout=2
                    )

                    self.ptype = SCPI
                    self.view_vol1 = True
                    self.view_cur1 = True
                    self.view_vol2 = True
                    self.view_cur2 = True
                    self.view_vbia = False
                    self.view_cps = True
                    self.view_leak_cur1 = True
                    self.view_leak_cur2 = True

                elif dialog.selected == SEMES:
                    gen_port = dialog.gen_port
                    com_speed = dialog.com_speed
                    com_data = dialog.com_data
                    com_parity = dialog.com_parity
                    com_stop = dialog.com_stop
                    self.com_open_flag = False

                    self.client = serial.Serial(
                        port=gen_port,
                        baudrate=com_speed,
                        parity=serial.PARITY_NONE,
                        stopbits=serial.STOPBITS_ONE,
                        bytesize=serial.EIGHTBITS,
                        timeout=2
                    )

                    self.ptype = SCPI
                    self.view_vol1 = True
                    self.view_cur1 = True
                    self.view_vol2 = False
                    self.view_cur2 = False
                    self.view_vbia = False
                    self.view_cps = False
                    self.view_leak_cur1 = False
                    self.view_leak_cur2 = False
                    
                else:
                    gen_port = dialog.ipaddress
                    self.ipport = int(dialog.ipport)
                    logging.info(gen_port)

                    self.client = device.connect_tcp(gen_port, self.ipport)
                    self.ptype = TCP

                logging.info(self.client)
                logging.info(self.ptype)

                # Graph
                self.graph_type = dialog.graph_type

                if self.client:
                    self.label_gen_port.setText(gen_port)
                    self.label_ptype.setText(self.ptype)
                    self.btn_com_gen.hide()
                    self.btn_pulldata.setEnabled(True)
                    self.btn_esc_run.setEnabled(True)
                    self.btn_params.setEnabled(True)
                    self.btn_inspect.setEnabled(True)
                    
                # self.data = device.read_setting_value(self.client, self.ptype)
                # if self.data:
                #     self.toggle = self.data['rd_toggle']

        else:
            logging.info("Open Dialog")

    # CheckBox clicked
    def checkBoxChanged(self, category):
        if category == 'VOLTAGE1':
            if self.cbx_vol1.isChecked() == True:
                self.view_vol1 = True
                self.graph_vol1.show()
            else:
                self.view_vol1 = False
                self.graph_vol1.hide()

        elif category == 'CURRENT1':
            if self.cbx_cur1.isChecked() == True:
                self.view_cur1 = True
                self.itemCurrent1.show()
            else:
                self.view_cur1 = False 
                self.itemCurrent1.hide()

        elif category == 'VOLTAGE2':
            if self.cbx_vol2.isChecked() == True:
                self.view_vol2 = True
                self.graph_vol2.show()
            else:
                self.view_vol2 = False 
                self.graph_vol2.hide()

        elif category == 'CURRENT2':
            if self.cbx_cur2.isChecked() == True:
                self.view_cur2 = True
                # self.axisB.addItem(self.itemCurrent2)
                self.itemCurrent2.show()
            else:
                self.view_cur2 = False
                # self.axisB.removeItem(self.itemCurrent2)
                self.itemCurrent2.hide()

        # elif category == 'VBIAS':
        #     if self.cbx_vbia.isChecked() == True:
        #         self.view_vbia = True
        #         self.axisC.addItem(self.itemVbia)
        #         self.axisC.show()
        #     else:
        #         self.view_vbia = False 
        #         self.axisC.removeItem(self.itemVbia)
                # self.axisC.hide()

        elif category == 'CPS':
            logging.info("CPS")
            if self.cbx_cps.isChecked() == True:
                self.view_cps = True
                # self.itemCps.addItem(self.itemCps)
                self.itemCps.show()
            else:
                self.view_cps = False  
                # self.itemCps.removeItem(self.itemCps)
                self.itemCps.hide()

        elif category == 'LEAK CURRENT1':
            if self.cbx_leak_cur1.isChecked() == True:
                self.view_leak_cur1 = True
                # self.axisD.addItem(self.itemLeakCur1)
                self.itemLeakCur1.show()
            else:
                self.view_leak_cur1 = False 
                # self.axisD.removeItem(self.itemLeakCur1)
                self.itemLeakCur1.hide()

        elif category == 'LEAK CURRENT2':
            if self.cbx_leak_cur2.isChecked() == True:
                self.view_leak_cur2 = True
                # self.axisD.addItem(self.itemLeakCur2)
                self.itemLeakCur2.show()
            else:
                self.view_leak_cur2 = False 
                # self.axisD.removeItem(self.itemLeakCur2)
                self.itemLeakCur2.hide()
        else:
            logging.info("No Category")

    # SAVE DATA as JSON FILE
    def save_data(self):
        data = {}
        
        FileSave = QFileDialog.getSaveFileName(self, '파일 저장', './data')
        file = FileSave[0]

        time_list = []
        dummy_list = []
        for tm in self.tmarks:
            if isinstance(tm, str):
                time_list.append(tm)
            else:
                tm = time.localtime(tm)
                string = time.strftime('%H:%M:%S', tm)
                time_list.append(string)
            dummy_list.append(0)


        if self.channelnum:
            dataset = pd.DataFrame({'indexs': self.indexs,
                    "times": time_list,
                    'vol1s': self.vol1s,
                    'cur1s(PHASE2)': self.cur1s,
                    'vol2s': self.vol2s,
                    'cur2s': self.cur2s,
                    'vbias(PHASE1)': self.vbias,
                    'VCS': self.vcss,
                    'ICS': self.icss,
                    'cpss': self.cpss,
                    'leak_cur1s': self.leak_cur1s,
                    'leak_cur2s': self.leak_cur2s,
            })
        else:
            dataset = pd.DataFrame({'indexs': self.indexs,
                    "times": time_list,
                    'vol1s': self.vol1s,
                    'cur1s(PHASE2)': self.cur1s,
                    'vol2s': dummy_list,
                    'cur2s': dummy_list,
                    'vbias(PHASE)': dummy_list,
                    'VCS': dummy_list,
                    'ICS': dummy_list,
                    'cpss': dummy_list,
                    'leak_cur1s': dummy_list,
                    'leak_cur2s': dummy_list,
            })

        product = 'PSES-50250PBN'
        user = 'USER'

        # CSS FILE
        csvfile = f"{file}.csv"
        dataset.to_csv(csvfile, sep=',', index=False)


        ## JSON   파일
        data['product'] = 'PSES-50250PBN'
        data['user'] = 'USER'
        start_time = datetime.strftime(self.start_time, '%Y-%m-%d %H:%M:%S')
        data['start_time'] = start_time

        if self.channelnum:
            data['indexs'] = self.indexs
            data['times'] = time_list
            data['vol1s'] = self.vol1s
            data['cur1s'] = self.cur1s
            data['vol2s'] = self.vol2s
            data['cur2s'] = self.cur2s
            data['vbias'] = self.vbias
            data['vcss'] = self.vcss
            data['icss'] = self.icss
            data['cpss'] = self.cpss
            data['leak_cur1s'] = self.leak_cur1s
            data['leak_cur2s'] = self.leak_cur2s
        else:
            data['indexs'] = self.indexs
            data['times'] = time_list
            data['vol1s'] = self.vol1s
            data['cur1s'] = self.cur1s
            data['vol2s'] = dummy_list
            data['cur2s'] = dummy_list
            data['vbias'] = dummy_list
            data['vcss'] = dummy_list
            data['icss'] = dummy_list
            data['cpss'] = dummy_list
            data['leak_cur1s'] = self.leak_cur1s
            data['leak_cur2s'] = dummy_list
        
        # JSON FILE
        jsonfile = f"{file}.json"
        with open(jsonfile, 'wb') as afile:
            json.dump(data, codecs.getwriter('utf-8')(afile))

    def regular_save_json(self):
        data = {}
        start_time = datetime.now()
        file = f"esc_{start_time.year:04d}_{start_time.month:02d}_{start_time.day:02d}_{start_time.hour:02d}_{start_time.minute:02d}_{start_time.second:02d}"
        
        start_time = datetime.strftime(self.start_time, '%Y-%m-%d %H:%M:%S')
        data['product'] = 'PSES-50250PBN'
        data['user'] = 'USER'
        data['start_time'] = start_time
        data['num'] = self.btn_num.text()

        time_list = []
        dummy_list = []
        for tm in self.tmarks:
            if isinstance(tm, str):
                time_list.append(tm)
            else:
                tm = time.localtime(tm)
                string = time.strftime('%H:%M:%S', tm)
                time_list.append(string)
            dummy_list.append(0)

        if self.channelnum:
            dataset = pd.DataFrame({'indexs': self.indexs,
                    "times": time_list,
                    'vol1s': self.vol1s,
                    'cur1s': self.cur1s,
                    'vol2s': self.vol2s,
                    'cur2s': self.cur2s,
                    'vbias': self.vbias,
                    'vcss': self.vcss,
                    'icss': self.icss,
                    'cpss': self.cpss,
                    'leak_cur1s': self.leak_cur1s,
                    'leak_cur2s': self.leak_cur2s,
            })
        else:
            dataset = pd.DataFrame({'indexs': self.indexs,
                    "times": time_list,
                    'vol1s': self.vol1s,
                    'cur1s': self.cur1s,
                    'vol2s': dummy_list,
                    'cur2s': dummy_list,
                    'vbias': dummy_list,
                    'vcss': dummy_list,
                    'icss': dummy_list,
                    'cpss': dummy_list,
                    'leak_cur1s': dummy_list,
                    'leak_cur2s': dummy_list,
            })

        # CSS FILE
        csvfile = f"data/{file}.csv"
        dataset.to_csv(csvfile, sep=',')
        
        if self.channelnum:
            data['indexs'] = self.indexs
            data['times'] = time_list
            data['vol1s'] = self.vol1s
            data['cur1s'] = self.cur1s
            data['vol2s'] = self.vol2s
            data['cur2s'] = self.cur2s
            data['vbias'] = self.vbias
            data['vcss'] = self.vcss
            data['icss'] = self.icss
            data['cpss'] = self.cpss
            data['leak_cur1s'] = self.leak_cur1s
            data['leak_cur2s'] = self.leak_cur2s
        
        else:
            data['indexs'] = self.indexs
            data['times'] = time_list
            data['vol1s'] = self.vol1s
            data['cur1s'] = self.cur1s
            data['vol2s'] = dummy_list
            data['cur2s'] = dummy_list
            data['vbias'] = dummy_list
            data['vcss'] = dummy_list
            data['icss'] = dummy_list
            data['cpss'] = dummy_list
            data['leak_cur1s'] = dummy_list
            data['leak_cur2s'] = dummy_list
        
        # JSON FILE
        jsonfile = f"data/{file}.json"
        with open(jsonfile, 'wb') as afile:
            json.dump(data, codecs.getwriter('utf-8')(afile))

        logging.info("########## regular_save_json :: {jsonfile}")

    # Read File DATA (JSON)
    def read_data(self):
        try:
            fname, _ = QFileDialog.getOpenFileName(self, 'Open file', 
             './data', "Json files (*.json)")

            with open(fname, 'r', encoding='utf-8') as json_file:
                data = json.load(json_file)
                self.indexs.extend(data['indexs'])
                logging.info("self.indexs :: ", len(self.indexs))

                if not self.start_time:
                    self.btn_savedata.setEnabled(True)
                    self.btn_zoomA.setEnabled(True)
                    # self.btn_zoom6.setEnabled(True)
                    
                    try:
                        start_time = data['start_time']
                        self.label_start_time.setText(start_time)
                        self.start_time = datetime.strptime(start_time, '%Y-%m-%d %H:%M:%S')
                    except Exception as e:
                        logging.info(e)

                    try:
                        self.btn_num.setText(str(data['num']))
                    except:
                        self.btn_num.setText(str(10))

                self.tmarks.extend(data['times'])
                self.vol1s.extend(data['vol1s'])

                try:
                    self.cur1s.extend(data['cur1s'])
                except:
                    self.cur1s.extend(data['curt1s'])

                self.vol2s.extend(data['vol2s'])

                try:
                    self.cur2s.extend(data['cur2s'])
                except:
                    self.cur2s.extend(data['curt2s'])

                self.vbias.extend(data['vbias'])
                self.vcss.extend(data['vcss'])
                self.icss.extend(data['icss'])
                self.cpss.extend(data['cpss'])

                try:
                    self.leak_cur1s.extend(data['leak_cur1s'])
                except:
                    self.leak_cur1s.extend(data['dechuck_starts'])
                
                try:
                    self.leak_cur2s.extend(data['leak_cur2s'])
                except:
                    self.leak_cur2s.extend(data['dechuck_ends'])

                s_time = self.label_start_time.text()
                if not s_time:
                    logging.info(data['start_time'])
                    try:
                        self.label_start_time.setText(data['start_time'])
                    except:
                        self.label_start_time.setText()

                product =  data.get('product')
                user =  data.get('user', '이름없음')
                title = "PRODUCT : {} / WRITER : {}".format(product, user)

                num = int(self.btn_num.text())
                self.time = int(len(self.indexs) / num)
                self.get_time()

            self.graphWidget.setTitle(title, color="b", size="20pt")

            self.draw_graph_full()

        except Exception as e:
            logging.info(e)

    def inspect_func(self):
        if not self.client:
            self.setting_device()
        else:
            Dialog = QDialog()
            dialog = InspectWindow(Dialog, self.client, self.channelnum)
            dialog.show()
            response = dialog.exec_()

            if response == QDialog.Accepted or response == QDialog.Rejected:
                self.graph_flag = False
      

if __name__ == "__main__":
    app = QApplication(sys.argv)
    form = MainWindow()
    form.show()
    sys.exit(app.exec_())